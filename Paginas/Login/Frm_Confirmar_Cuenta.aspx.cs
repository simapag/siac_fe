﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIAC.Cat_Clientes.Negocio;
using SIAC.Seguridad;
using SIAC.Envio_Email;
using System.Data;
using SIAC.Registro_Usuarios.Negocio;
using SIAC.Cat_Predios.Negocio;
using SIAC.Constantes;
using SIAC.Facturacion.Negocio;
using SIAC.Ciudades.Negocios;
using SIAC.Catalogo_Comercializacion_Usuarios.Negocio;
public partial class Paginas_Login_Frm_Confirmar_Cuenta : System.Web.UI.Page
{
    #region Page Load
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cls_Cat_Cor_Usuarios_Negocio Usuarios = new Cls_Cat_Cor_Usuarios_Negocio();
            Cls_Cat_Cor_Predios_Negocio Predios = new Cls_Cat_Cor_Predios_Negocio();
            DataTable Dt = new DataTable();
            string REF = Request.QueryString["ref"].ToString();
            Consultar_Estados();
            //Consultar_Ciudades();
            if (Request.QueryString.Count != 0)
            {
                Dt = Consultar_Registro_Usuario(REF);
                if (Dt.Rows.Count != 0)
                {
                    Txt_Email.Text = Dt.Rows[0]["Email"].ToString();
                    Hdd_Predio.Value = Dt.Rows[0][Cat_Registro_Usuarios.Campo_Predio_ID].ToString();
                    Predios.P_Predio_ID = Dt.Rows[0][Cat_Registro_Usuarios.Campo_Predio_ID].ToString();
                    DataTable Dt_Usuarios = Predios.Consulta_Usuarios();
                    Llenar_Datos(Dt_Usuarios);
                    //Habiltar_Campos();
                }
            }
        }
    }
    
    #endregion
    
    #region Metodos

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Llenar_Datos
    ///DESCRIPCIÓN: Carga los datos fiscales de los clientes.
    ///PARAMETROS:  Dt_Datos
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  04/Marzo/2016
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Llenar_Datos(DataTable Dt_Datos)
    {
        try
        {
            if (Dt_Datos.Rows.Count > 0)
            {
                Txt_Nombre.Text = Dt_Datos.Rows[0]["razon_social"].ToString();
                Txt_Calle.Text = Dt_Datos.Rows[0]["calle"].ToString();
                Txt_Numero_Exterior.Text = Dt_Datos.Rows[0]["numero_exterior"].ToString();
                Txt_Numero_Interior.Text = Dt_Datos.Rows[0]["numero_interior"].ToString();
                Txt_Colonia.Text = Dt_Datos.Rows[0]["colonia"].ToString();
                Txt_CP.Text = Dt_Datos.Rows[0]["cp"].ToString();
                Txt_Pais.Text = Dt_Datos.Rows[0]["pais"].ToString();
                Txt_Rfc.Text = Dt_Datos.Rows[0]["rfc"].ToString();
                Cmb_Estado.SelectedValue = Dt_Datos.Rows[0]["estado"].ToString();
                Consultar_Ciudades();
                Cmb_Ciudad.SelectedValue = Dt_Datos.Rows[0]["ciudad_id"].ToString();
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error("Error: " + Ex.Message);
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Habilitar_Campos
    ///DESCRIPCIÓN: Consulta los datos fiscales del cliente.
    ///PARAMETROS:  
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  04/Marzo/2016
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Habiltar_Campos()
    {
        try
        {
            Txt_Nombre.Enabled = String.IsNullOrEmpty(Txt_Nombre.Text.Trim()) ? true : false;
            Txt_Calle.Enabled = String.IsNullOrEmpty(Txt_Calle.Text.Trim()) ? true : false;
            Txt_Numero_Exterior.Enabled = String.IsNullOrEmpty(Txt_Numero_Exterior.Text.Trim()) ? true : false;
            Txt_Numero_Interior.Enabled = String.IsNullOrEmpty(Txt_Numero_Interior.Text.Trim()) ? true : false;
            Txt_Colonia.Enabled = String.IsNullOrEmpty(Txt_Colonia.Text.Trim()) ? true : false;
            Txt_CP.Enabled = String.IsNullOrEmpty(Txt_CP.Text.Trim()) ? true : false;
            Txt_Pais.Enabled = String.IsNullOrEmpty(Txt_Pais.Text.Trim()) ? true : false;
            Txt_Rfc.Enabled = String.IsNullOrEmpty(Txt_Rfc.Text.Trim()) ? true : false;
            Cmb_Estado.Enabled = Cmb_Estado.SelectedIndex > 0 ? false : true;
            Cmb_Ciudad.Enabled = Cmb_Ciudad.SelectedIndex > 0 ? false : true;
        }
        catch (Exception Ex)
        {
            Mensaje_Error("Error: " + Ex.Message);
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Alta_Cliente
    ///DESCRIPCIÓN: Alta de los datos fiscales del cliente.
    ///PARAMETROS:  Pass
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  4/Enero/2016
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private Boolean Alta_Cliente(String Pass)
    {
        Cls_Cat_Clientes_Negocio Cliente = new Cls_Cat_Clientes_Negocio();
        Cliente.P_Razon_Social = Txt_Nombre.Text.Trim().ToUpper();
        Cliente.P_Nombre_Corto = Txt_Nombre.Text.Trim().ToUpper();
        Cliente.P_Calle = Txt_Calle.Text.Trim().ToUpper();
        Cliente.P_Numero_Exterior = Txt_Numero_Exterior.Text.ToUpper();
        Cliente.P_Numero_Interior = Txt_Numero_Interior.Text.ToUpper();
        Cliente.P_Colonia = Txt_Colonia.Text.ToUpper();
        Cliente.P_Cp = Txt_CP.Text.Trim().ToUpper();
        Cliente.P_Rfc = Txt_Rfc.Text.Trim().ToUpper();
        Cliente.P_Ciudad = Cmb_Ciudad.Text.Trim();
        Cliente.P_Localidad = Cmb_Ciudad.Text.Trim();
        Cliente.P_Estado = Cmb_Estado.SelectedValue;
        Cliente.P_Pais = Txt_Pais.Text.Trim().ToUpper();
        Cliente.P_Contrasena = Cls_Seguridad.Encriptar(Pass.Trim());
        Cliente.P_Estatus = "ACTIVO";
        Cliente.P_Email = Txt_Email.Text.Trim();
        //Cliente.P_Curp = Txt_Curp.Text.Trim().ToUpper();
        Cliente.P_Predio_Id = Hdd_Predio.Value; // Consultar_Registro_Usuario(Request.QueryString["refe"].ToString()).Rows[0][Cat_Registro_Usuarios.Campo_Predio_ID].ToString();
        //Cliente.P_Cliente_Id = Cls_Seguridad.Desencriptar(Request.QueryString["refe"].ToString());
        return Cliente.Alta_Cliente();

    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Consultar_Estados
    ///DESCRIPCIÓN: Consulta y carga los estados.
    ///PARAMETROS:  
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  14/Diciembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Consultar_Estados()
    {
        Cls_Ope_Facturacion_Negocio Negocio = new Cls_Ope_Facturacion_Negocio();

        DataTable Dt_Datos;

        try
        {
            Cmb_Estado.DataValueField = Cat_Cor_Estados.Campo_Estado_ID;
            Cmb_Estado.DataTextField = Cat_Cor_Estados.Campo_Nombre;
            Dt_Datos = Negocio.Consultar_Estados();

            Cmb_Estado.DataSource = Dt_Datos;
            Cmb_Estado.DataBind();
            Cmb_Estado.Items.Insert(0, new ListItem("SELECCIONE", "0"));
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message.ToString());
        }

    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Consultar_Datos_Fiscales
    ///DESCRIPCIÓN: Consulta y carga las ciudades en el control
    ///PARAMETROS:  
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  14/Diciembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Consultar_Ciudades()
    {
        Cls_Cat_Cor_Ciudades_Negocio Negocio = new Cls_Cat_Cor_Ciudades_Negocio();

        DataTable Dt_Datos = new DataTable();
        try
        {
            Cmb_Ciudad.DataValueField = Cat_Cor_Ciudades.Campo_Ciudad_ID;
            Cmb_Ciudad.DataTextField = Cat_Cor_Ciudades.Campo_Nombre;
            if (Cmb_Estado.SelectedIndex != 0)
            {
                Negocio.P_Estado_ID = Cmb_Estado.SelectedValue;
                Dt_Datos = Negocio.Buscar_Ciudad();
            }



            Cmb_Ciudad.DataSource = Dt_Datos;
            Cmb_Ciudad.DataBind();
            Cmb_Ciudad.Items.Insert(0, new ListItem("SELECCIONE", "0"));
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message.ToString());
        }

    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Modificar_Estatus_Registro_Usuario
    ///DESCRIPCIÓN: Modifica el estatus del registro cuando se hace el alta del cliente.
    ///PARAMETROS:  
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  14/Diciembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Modificar_Estatus_Registro_Usuario()
    {
        Cls_Cat_Registro_Usuarios_Negocio Correo = new Cls_Cat_Registro_Usuarios_Negocio();
        Correo.P_Registro_Id = Request.QueryString["ref"].ToString();
        Correo.P_Estatus = "CONFIRMADO";
        Correo.Modificar_Registro_Usuarios();

    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Validar_Componentes
    ///DESCRIPCIÓN: Valida que los datos sean correctos.
    ///PARAMETROS:  
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  15/Diciembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private String Validar_Componentes()
    {
        bool Validacion = true;
        string Mensaje = "Favor de acudir a SIMAPAG para completar los siguientes datos: ";
        if (String.IsNullOrEmpty(Txt_Nombre.Text))
        {
            Mensaje += !Validacion ? ", " : "";
            Mensaje += "Razón Social";

            Validacion = false;
        }
        if (String.IsNullOrEmpty(Txt_Calle.Text))
        {
            Mensaje += !Validacion ? ", " : "";
            Mensaje += "Calle";

            Validacion = false;
        }
        if (String.IsNullOrEmpty(Txt_Numero_Exterior.Text))
        {
            Mensaje += !Validacion ? ", " : "";
            Mensaje += "Numero Ext";

            Validacion = false;
        }
        if (String.IsNullOrEmpty(Txt_Colonia.Text))
        {
            Mensaje += !Validacion ? ", " : "";
            Mensaje += "Colonia";

            Validacion = false;
        }
        if (String.IsNullOrEmpty(Txt_Rfc.Text))
        {
            Mensaje += !Validacion ? ", " : "";
            Mensaje += "RFC";

            Validacion = false;
        }
        if (String.IsNullOrEmpty(Txt_CP.Text))
        {
            Mensaje += !Validacion ? ", " : "";
            Mensaje += "CP";
            Validacion = false;
        }
        if (Cmb_Estado.SelectedIndex == 0)
        {
            Mensaje += !Validacion ? ", " : "";
            Mensaje += "Estado";
            Validacion = false;
        }
        if (String.IsNullOrEmpty(Cmb_Ciudad.Text))
        {
            Mensaje += !Validacion ? ", " : "";
            Mensaje += "Ciudad";
            Validacion = false;
        }
        if (String.IsNullOrEmpty(Txt_Pais.Text))
        {
            Mensaje += !Validacion ? ", " : "";
            Mensaje += "País";
            Validacion = false;
        }
        if (!Validacion)
        {
            return Mensaje;
        }
        else
            return "";


    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Crear_Password
    ///DESCRIPCIÓN: Crea la contraseña para ingresar al portal.
    ///PARAMETROS:  
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  14/Diciembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private String Crear_Password()
    {
        String Password = "";
        Password = Membership.GeneratePassword(8, 1);

        return Password;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Crear_Mensaje
    ///DESCRIPCIÓN: Crea el mensaje que se enviara al correo del cliente.
    ///PARAMETROS:  Usuario, Pass
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  15/Diciembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private string Crear_Mensaje(string Usuario, string Pass)
    {

        String Mensaje = "<font face='arial' size='4'> " +
            "<p><b>PORTAL DE FACTURACI&Oacute;N</b></p></br>" +
            "<p><b>Creación de la cuenta</b></p>" +
            "</font>" +
            "<font face='arial' size='3'>"+
            "<br><p>Se registro la cuenta al Portal de Facturación SIMAPAG. </p><br>"+
            "</font>"+
            "<font face='arial' size='3'>" +
            "<p> USUARIO: " + Usuario + "</p>" +
            "<p> CONTRASE&Ntilde;A: " + Pass + " </p>" +
            "</font>";
        return Mensaje;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Consultar_Registro_Usuario
    ///DESCRIPCIÓN: Consulta los datos del registro del usuario.
    ///PARAMETROS:  ID
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  14/Diciembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private DataTable Consultar_Registro_Usuario(String Id)
    {
        DataTable Dt_Registro = new DataTable();
        Cls_Cat_Registro_Usuarios_Negocio Cliente = new Cls_Cat_Registro_Usuarios_Negocio();
        Cliente.P_Registro_Id = Id;
        Dt_Registro = Cliente.Consultar_Registro_Usuario();

        return Dt_Registro;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Mensaje_Error
    ///DESCRIPCIÓN: Limpia el mensaje de error de la pantalla.
    ///PARAMETROS:  
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  15/Diciembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Mensaje_Error()
    {

        Lbl_Mensaje.Text = "";
        Lbl_Mensaje.Visible = false;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Mensaje_Error
    ///DESCRIPCIÓN: Muestra el mensaje de error en pantalla
    ///PARAMETROS:  
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  15/Diciembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Mensaje_Error(String Mensaje)
    {
        Lbl_Mensaje.Text = Mensaje;
        Lbl_Mensaje.Visible = true;
    }

    private Boolean Validar_Cuenta(String Predio_ID) {
        Cls_Cat_Clientes_Negocio Cliente_Negocio = new Cls_Cat_Clientes_Negocio();
        Boolean Valida = true;
        Cliente_Negocio.P_Predio_Id = Predio_ID.Trim();
        DataTable Dt = Cliente_Negocio.Consultar_Clientes();
        if (Dt.Rows.Count > 0)
        {
            if (Dt.Rows[0]["Estatus"].ToString() == "Activo")
            {
                Valida = false;
            }
        }

        return Valida;
    }

   
    #endregion

    #region Eventos

    protected void Btn_Confirmar_Click(object sender, EventArgs e)
    {
        Mensaje_Error();
        Lbl_Mensaje_Correcto.Text = "";
        Lbl_Mensaje_Correcto.Visible = false;

        try
        {
            if (Request.QueryString.Count != 0)
            {
                String id = Request.QueryString["ref"].ToString();
                DataTable Dt_Correos = Consultar_Registro_Usuario(id);

                String Pass = Crear_Password();
                //if (Validar_Componentes())
                //{
                if (Dt_Correos.Rows.Count > 0)
                {
                    if (Dt_Correos.Rows[0]["Estatus"].ToString().Trim() == "DESHABILITADO")
                    {
                        Hdd_Predio.Value = Dt_Correos.Rows[0][Cat_Registro_Usuarios.Campo_Predio_ID].ToString();
                        if (No_Existe_Cliente())
                        {
                            if (Alta_Cliente(Pass))
                            {
                                Modificar_Estatus_Registro_Usuario();
                                Cls_Enviar_Correo.Envia_Correo(Crear_Mensaje(Dt_Correos.Rows[0]["EMAIL"].ToString(), Pass), Dt_Correos.Rows[0]["EMAIL"].ToString(), "Confirmacion de cuenta");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Confirm", "showCorreo();", true);
                            }
                        }
                        else
                        {
                            Modifificar_Cliente(Pass);
                            Cls_Enviar_Correo.Envia_Correo(Crear_Mensaje(Dt_Correos.Rows[0]["EMAIL"].ToString(), Pass), Dt_Correos.Rows[0]["EMAIL"].ToString(), "Confirmacion de cuenta");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Confirm", "showCorreo();", true);
                        }
                    }
                    else
                    {
                        Mensaje_Error("La cuenta ya fue creada.");
                    }
                }
                else
                {
                    Mensaje_Error("Referencia no valida");
                }

            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    private void Modifificar_Cliente(String Pass)
    {
        Cls_Cat_Clientes_Negocio Cliente = new Cls_Cat_Clientes_Negocio();
        Cliente.P_Razon_Social = Txt_Nombre.Text.Trim().ToUpper();
        Cliente.P_Nombre_Corto = Txt_Nombre.Text.Trim().ToUpper();
        Cliente.P_Calle = Txt_Calle.Text.Trim().ToUpper();
        Cliente.P_Numero_Exterior = Txt_Numero_Exterior.Text.ToUpper();
        Cliente.P_Numero_Interior = Txt_Numero_Interior.Text.ToUpper();
        Cliente.P_Colonia = Txt_Colonia.Text.ToUpper();
        Cliente.P_Cp = Txt_CP.Text.Trim().ToUpper();
        Cliente.P_Rfc = Txt_Rfc.Text.Trim().ToUpper();
        Cliente.P_Ciudad = Cmb_Ciudad.Text.Trim();
        Cliente.P_Localidad = Cmb_Ciudad.Text.Trim();
        Cliente.P_Estado = Cmb_Estado.SelectedValue;
        Cliente.P_Pais = Txt_Pais.Text.Trim().ToUpper();
        Cliente.P_Contrasena = Cls_Seguridad.Encriptar(Pass.Trim());
        Cliente.P_Estatus = "ACTIVO";
        Cliente.P_Email = Txt_Email.Text.Trim();
        //Cliente.P_Curp = Txt_Curp.Text.Trim().ToUpper();
        Cliente.P_Predio_Id = Hdd_Predio.Value; // Consultar_Registro_Usuario(Request.QueryString["refe"].ToString()).Rows[0][Cat_Registro_Usuarios.Campo_Predio_ID].ToString();
        //Cliente.P_Cliente_Id = Cls_Seguridad.Desencriptar(Request.QueryString["refe"].ToString());
        Cliente.Modificar_Cliente_Por_Predio(); ;
    }

    private Boolean No_Existe_Cliente()
    {
        Boolean regresar = true;
        Cls_Cat_Clientes_Negocio Cliente = new Cls_Cat_Clientes_Negocio();
        //Cliente.P_Curp = Txt_Curp.Text.Trim().ToUpper();
        Cliente.P_Predio_Id = Hdd_Predio.Value; // Consultar_Registro_Usuario(Request.QueryString["refe"].ToString()).Rows[0][Cat_Registro_Usuarios.Campo_Predio_ID].ToString();
        Cliente.Consultar_Clientes();
        if (Cliente.Consultar_Clientes().Rows.Count > 0)
            regresar = false;
            
        return regresar;
    }

    protected void Btn_Regresar_Click(object sender, EventArgs e)
    {
        Response.Redirect("Frm_Login.aspx");
    }
    
    protected void Cmb_Estado_SelectedIndexChanged(object sender, EventArgs e)
    {
        Consultar_Ciudades();
    }
    
    #endregion

}