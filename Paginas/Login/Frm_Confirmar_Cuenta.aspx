﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Frm_Confirmar_Cuenta.aspx.cs" Inherits="Paginas_Login_Frm_Confirmar_Cuenta" %>

<%--<!DOCTYPE html>--%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="../../estilos/estilo_paginas.css" rel="stylesheet" />
    <link href="../../Estilos/css/bootstrap.css" rel="stylesheet" />
    <script src="../../JavaScript/jquery/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../JavaScript/js/bootstrap.js" type="text/javascript"></script>
    <title>Confirmar Cuenta</title>
    <script type="text/javascript">
        $(function () {
            $("[id$='Txt_Rfc']").blur(function () {
                $('input[id$=Txt_Rfc]').filter(function () {
                    if (this.value.length > 13 || this.value.length < 10)
                        $(this).val('');
                    });
            });
            $("[id$='Txt_Rfc']").keydown(function (event) {
                if (event.keyCode == 32) {
                    this.value = this.value.trim();
                    return false;
                }
            });
            $("[id$='Txt_CP']").blur(function () {
                $('input[id$=Txt_CP]').filter(function () {
                    if (!this.value.match(/^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/))
                        $(this).val('');
                });
            });

     

        });
        function showCorreo() {
            alert('Confirmación de la cuenta. Favor de revisar su correo.');
            window.location.href = 'Frm_Login.aspx';
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="Udp_Confirmar" runat="server">

            <ContentTemplate>
                <asp:UpdateProgress ID="Uprg_Comfirmar" runat="server" AssociatedUpdatePanelID="Udp_Confirmar" DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                        <div class="processMessage" id="div_progress">
                            <img alt="" src="../../imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <div class="navbar navbar-default">

                    <div class="container">
                        <div>&nbsp;</div>
                        <table>
                            <tr>
                                <td>
                                    <img src="../../imagenes/Logos/img-logo.png" alt=""/>
                                </td>
                                <td>
                                    <asp:Label ID="Lbl_Titulo" runat="server" Style="color: #FFF;"><h2 style="padding-left:20px">Confirmar Cuenta</h2></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <div>&nbsp;</div>
                    </div>
                </div>


                <div class="container">
                    <div class="form-group">
                        <asp:Label ID="Lbl_Mensaje" runat="server" CssClass="alert alert-danger center-block" Visible="false"></asp:Label>
                    <asp:Label ID="Lbl_Mensaje_Correcto" runat="server" CssClass="alert alert-success center-block" Visible="false"></asp:Label></div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">*Razón Social</label>
                        <div class="col-md-10">
                            <asp:TextBox ID="Txt_Nombre" runat="server" CssClass="form-control text-uppercase" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">*Calle</label>
                        <div class="col-md-10">
                            <asp:TextBox ID="Txt_Calle" runat="server" CssClass="form-control text-uppercase" MaxLength="255" Enabled="false"></asp:TextBox>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">*Numero Ext.</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="Txt_Numero_Exterior" runat="server" CssClass="form-control text-uppercase" MaxLength="50" Enabled="false"></asp:TextBox>
                        </div>
                        <label class="col-md-2 control-label">Numero Int.</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="Txt_Numero_Interior" runat="server" CssClass="form-control text-uppercase" MaxLength="50" Enabled="false"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">*Colonia</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="Txt_Colonia" runat="server" CssClass="form-control text-uppercase" MaxLength="100" Enabled="false"></asp:TextBox>
                        </div>
                        <label class="col-md-2 control-label">*C.P.</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="Txt_CP" runat="server" CssClass="form-control text-uppercase" MaxLength="5" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                    

                    <div class="form-group">
                        <label class="col-md-2 control-label">*Estado</label>
                        <div class="col-md-10">
                            <asp:DropDownList ID="Cmb_Estado" runat="server" CssClass="form-control text-uppercase" Enabled="false" OnSelectedIndexChanged="Cmb_Estado_SelectedIndexChanged" AutoPostBack="true">

                            </asp:DropDownList>
                        </div>
                       
                    </div>
                    <div class="form-group">
                         <label class="col-md-2 control-label">*Ciudad</label>
                        <div class="col-md-10">
                            <asp:DropDownList ID="Cmb_Ciudad" runat="server" CssClass="form-control text-uppercase" Enabled="false"></asp:DropDownList>
                        </div> </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">*Pa&iacute;s</label>
                        <div class="col-md-10">
                            <asp:TextBox ID="Txt_Pais" runat="server" CssClass="form-control text-uppercase" MaxLength="100" Enabled="false"></asp:TextBox>
                        </div>
                    </div><div class="form-group">
                        <label class="col-md-2 control-label">*RFC</label>
                        <div class="col-md-10">
                            <asp:TextBox ID="Txt_Rfc" runat="server" CssClass="form-control text-uppercase" MaxLength="20" Enabled="false"></asp:TextBox>
                        </div> </div>
                    <div class="form-group">
                        <%--<label class="col-md-2 control-label">CURP</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="Txt_Curp" runat="server" CssClass="form-control text-uppercase" MaxLength="18"></asp:TextBox>
                        </div>--%>
                        <label class="col-md-2 control-label">*Email</label>
                        <div class="col-md-10">
                            <asp:TextBox ID="Txt_Email" runat="server" CssClass="form-control" Enabled="false" ></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>&nbsp;</label>
                    </div>

                    <div class="col-lg-offset-2 col-lg-10">
                        <asp:Button ID="Btn_Confirmar" runat="server" Text="Confirmar" CssClass="btn btn-success" OnClick="Btn_Confirmar_Click" />
                        <asp:Button ID="Btn_Regresar" runat="server" Text="Regresar" CssClass="btn btn-success" OnClick="Btn_Regresar_Click" />
                    </div>

                </div>
                <asp:HiddenField ID ="Hdd_Predio" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>


    </form>
</body>
</html>
