﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Frm_Login.aspx.cs" Inherits="Paginas_Login_Frm_Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <script src="../../JavaScript/jquery/jquery-1.9.1.js"></script>
    <link href="../../Estilos/css/bootstrap.css" rel="stylesheet" />

    <link href="../../estilos/estilo_paginas.css" rel="stylesheet" />
    <script src="../../JavaScript/js/bootstrap.js"></script>
   
    <title>Portal Facturaci&oacute;n</title>
    <script>

        $(document).on('ready', function () {
            $('#Lnk_Ver').on('click', function (e) {
                e.preventDefault();
                var current = $(this).attr('action');
                if (current == 'hide') {
                    $('#Txt_Password').attr('type', 'text');
                    $(this).removeClass('glyphicon-eye-open').addClass('glyphicon-eye-close').attr('action', 'show');
                }
                if (current == 'show') {
                    $('#Txt_Password').attr('type', 'password');
                    $(this).removeClass('glyphicon-eye-close').addClass('glyphicon-eye-open').attr('action', 'hide');
                }
            })
        })



    </script>
</head>
<body style="background-color: #B7E0E4">
    <%--B7E0E4--%>
    <form id="form2" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager_Login" runat="server" />
        <asp:UpdatePanel ID="Udp_Login" runat="server">
            <Triggers>
                <asp:PostBackTrigger ControlID="Lnk_Reload"/>
                <asp:PostBackTrigger ControlID="Btn_Ingresar"/>
            </Triggers>
            <ContentTemplate>
                <asp:UpdateProgress ID="Uprg_Login" runat="server" AssociatedUpdatePanelID="Udp_Login" DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                        <div class="processMessage" id="div_progress">

                            <img alt="" src="../../imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <div class="navbar navbar-default">

                    <div class="container">
                        <div>&nbsp;</div>
                        <table>
                            <tr>
                                <td>
                                    <img src="../../imagenes/Logos/img-logo.png" />
                                </td>
                                <td style="text-align:left;padding-left:20px">
                                    <asp:Label ID="Lbl_Titulo" runat="server" Style="color: #FFF;"><h2>Portal de Facturaci&oacute;n SIMAPAG</h2></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <div>&nbsp;</div>
                    </div>
                </div>
                <div id="login">
                    <div class="container">
                        <asp:Label ID="Lbl_Mensaje" runat="server" CssClass="alert alert-danger center-block"></asp:Label>
                    </div>
                    <div class="container">

                        <div class="row">

                            <div class="col-md-4 col-md-offset-4">

                                <div class="panel panel-danger">
                                    <div class="panel-body">

                                        <div class="form-group">
                                            <asp:Label ID="Lbl_Correo" runat="server">Correo</asp:Label>
                                            <asp:TextBox ID="Txt_Correo" runat="server" CssClass="form-control" Requerido="true" TabIndex="1"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Lbl_Pass" runat="server">Contrase&ntilde;a</asp:Label>
                                            <table style="width: 100%;" border="0">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="Txt_Password" runat="server" TextMode="Password" CssClass="form-control" TabIndex="2" Requerido="true"></asp:TextBox>
                                                    </td>
                                                    <td class="form-group" style="width: 10%">
                                                        <span id="Lnk_Ver" action="hide" class="btn btn-info glyphicon glyphicon-eye-open text-right"></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="form-group">

                                            <table style="width: 100%;" border="0">
                                                <tr>
                                                    <td style="width: 90%">
                                                        <asp:Image ID="Img" runat="server" Width="100%" ImageUrl="CImage.aspx" CssClass="form-group" />
                                                    </td>
                                                    <td class="form-group" style="width: 10%">
                                                        <asp:LinkButton ID="Lnk_Reload" runat="server"  CssClass="btn btn-info glyphicon glyphicon-refresh"
                                                            CausesValidation="false" OnClick="Lnk_Reload_Click"></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>


                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Lbl_Capt" runat="server">Ingrese el codigo</asp:Label>
                                            <asp:TextBox ID="Txt_Img" runat="server" CssClass="form-control" TabIndex="3"></asp:TextBox>
                                        </div>


                                        <asp:Button ID="Btn_Ingresar" runat="server" class="btn btn-success" Style="width: 49%" TabIndex="4" Text="Ingresar"
                                            OnClick="Btn_Ingresar_Click" />
                                        <asp:Button ID="Btn_Registrarse" runat="server" class="btn btn-success" TabIndex="5" Style="width: 49%" Text="Registrarse"
                                            OnClick="Btn_Registrarse_Click" />
                                    </div>
                                </div>
                                <label id="Lbl_Extra">Si desea obtener su factura electr&oacute;nica sin registrarse, de clic <a href="../Paginas_Generales/Frm_Facturacion.aspx" class="alert-link">aqui</a></label>
                            </div>

                        </div>
                    </div>
                </div>
                <%--        <div id="footer">
            <p>
                &#169; 2013 CONTEL | Conectividad y Telecomunicaci&oacute;n S. A. de C. V.
                <br />
                Todos los derechos reservados.
            </p>
        </div>
        <br />
        <div id="Aviso">
            <p>
                "Se recomienda el uso de Google Chrome para el optimo desempeño del sistema"
            </p>
        </div>
        <div id="mensaje" style="display: block; width: 450px; height: 200px; margin: 0 auto;">
            <asp:ImageButton ID="Btn_Img_Mensaje" runat="server" ImageUrl="../../Imagenes/paginas/erp_warning.png" Style="visibility: hidden" />
            <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
        </div>--%>

                <asp:HiddenField ID="txt_cliente_temporal" runat="server" Value="" />

            </ContentTemplate>
        </asp:UpdatePanel>
        
    </form>
     
</body>
</html>
