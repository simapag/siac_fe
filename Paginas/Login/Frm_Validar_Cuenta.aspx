﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Frm_Validar_Cuenta.aspx.cs" Inherits="Paginas_Login_Frm_Validar_Cuenta"
    UICulture="es" Culture="es-MX" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="es-mx">
<head runat="server">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="../../estilos/estilo_paginas.css" rel="stylesheet" />
    <link href="../../Estilos/css/bootstrap.css" rel="stylesheet" />
    <script type="text/javascript" src="../../JavaScript/jquery/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="../../JavaScript/js/bootstrap.js"></script>
    <script src="../../Scripts/datepicker/js/bootstrap-datepicker.js"></script>
    <link href="../../Scripts/datepicker/css/datepicker.css" rel="stylesheet" />

    <title>Validar Cuenta</title>
    <link href="../../estilos/Frm_Ope_Facturacion.css" rel="stylesheet" />
    <script type="text/javascript">
        //
        $(function () {
            $("[id$='Txt_Total_A_Pagar']").blur(function () {
                $('input[id$=Txt_Total_A_Pagar]').filter(function () {
                    if (!this.value.match(/^\d+(\.\d{1,2})?$/))
                        $(this).val('');
                });
            });
            $("[id$='Txt_Correo_Electronico']").blur(function () {
                $('input[id$=Txt_Correo_Electronico]').filter(function () {
                    if (!this.value.match(/^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$/))
                        $(this).val('');

                });
            });
            $("[id$='Txt_Correo_Electronico']").keydown(function (event) {
                if (event.keyCode == 32) {
                    this.value = this.value.trim();
                    return false;
                }
            });

        });
        //
        function showCorreo() {
            alert('Correo Enviado. Favor de revisar su correo');
            window.location.href = 'Frm_Login.aspx';
        }

        //Seleccionar fecha 
        $(document).ready(function () {
            var dp = $('#<%=Txt_Fecha_Emision.ClientID%>');

            dp.datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "it"
            }).on('changeDate', function (ev) {
                $(this).blur();
                if (ev.viewMode === 'days') {
                    $(this).datepicker('hide');
                }
            });
        });
    </script>





</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"></asp:ScriptManager>
        <asp:UpdatePanel ID="Udp_Validar" runat="server">

            <ContentTemplate>
                <asp:UpdateProgress ID="Uprg_Validar" runat="server" AssociatedUpdatePanelID="Udp_Validar" DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                        <div class="processMessage" id="div_progress">
                            <img alt="" src="../../imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <div class="navbar navbar-default">

                    <div class="container">
                        <div>&nbsp;</div>
                        <table>
                            <tr>
                                <td>
                                    <img src="../../imagenes/Logos/img-logo.png" />
                                </td>
                                <td>
                                    <asp:Label ID="Lbl_Titulo" runat="server" Style="color: #FFF; text-align: left; padding-left:20px"><h2 style="padding-left:20px">Validar Cuenta</h2></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <div>&nbsp;</div>
                    </div>
                </div>

                <div id="Div_Datos" class="container">
                    <div class="form-group">
                        <asp:Label ID="Lbl_Mensaje" runat="server" CssClass="alert alert-danger center-block" Visible="false"></asp:Label>
                        <asp:Label ID="Lbl_Mensaje_Correcto" runat="server" CssClass="alert alert-success center-block" Visible="false"></asp:Label>

                    </div>

                    <%-- <div class="form-group">
                        <label class="col-md-2 control-label">No. Cuenta</label>
                        <div class="col-md-10">
                            <asp:TextBox ID="Txt_No_Cuenta" runat="server" CssClass="form-control" requerido="true" MaxLength="15"></asp:TextBox>
                        </div>
                    </div>--%>
                    <div class="form-group">
                        <label class="col-md-2 control-label">*RPU</label>
                        <div class="col-md-10">
                            <asp:TextBox ID="Txt_RPU" runat="server" CssClass="form-control" requerido="true" MaxLength="12"></asp:TextBox>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">*Fecha Emisi&oacute;n</label>
                        <div class="col-md-10">
                            <asp:TextBox ID="Txt_Fecha_Emision" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="dd/MM/yyyy"
                                requerido="true" MaxLength="10"></asp:TextBox>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">*Total a Pagar</label>
                        <div class="col-md-10">
                            <asp:TextBox ID="Txt_Total_A_Pagar" runat="server" CssClass="form-control" requerido="true" MaxLength="20"></asp:TextBox>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">*Correo Electr&oacute;nico</label>
                        <div class="col-md-10">
                            <asp:TextBox ID="Txt_Correo_Electronico" runat="server" CssClass="form-control" placeholder="ejemplo@dominio.com"
                                MaxLength="50" requerido="true"></asp:TextBox>
                        </div>

                    </div>
                    <div class="form-group">
                        <label>&nbsp;</label>
                    </div>

                    <div class="col-lg-offset-2 col-lg-10">
                        <asp:Button ID="Btn_Validar" runat="server" CssClass="btn btn-success" Text="Validar"
                            OnClick="Btn_Validar_Click" ToolTip="Validar"></asp:Button>

                        <asp:Button ID="Btn_Regresar" runat="server" Text="Regresar" CssClass="btn btn-success" OnClick="Btn_Regresar_Click" />

                        <%--<button class="btn btn-success">Elaborar solicitud TI  <span class="glyphicon  glyphicon-calendar "> </span> </button>--%>
                    </div>

                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>

</html>
