﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIAC.Cat_Clientes.Negocio;
using SIAC.Seguridad;
using SIAC.Constantes;
using Erp.Sesiones;
using System.Configuration;
using System.Web.Security;
using System.Collections;
using Cls_Cat_Parametros.Negocio;


public partial class Paginas_Login_Frm_Login : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
        if (!Page.IsPostBack)
        {
            Session.RemoveAll();
            Lbl_Mensaje.Visible = false;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Iniciar_Sesion
    ///DESCRIPCIÓN          : Valida el contrato, usuario y contraseña para poder
    ///                       acceder al sistema
    ///PARAMETROS           : 
    ///CREO                 : José Maldonado Méndez
    ///FECHA_CREO           : 04/Diciembre/2015
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Iniciar_Sesion()
    {
        Lbl_Mensaje.Text = null;
        try
        {
            if (Txt_Correo.Text.Trim() != "" & Txt_Password.Text.Trim() != "" & Txt_Img.Text.Trim() != "")
            {
                if (this.Txt_Img.Text == this.Session["CaptchaImageText"].ToString())
                {
                    if (VerificarUsuario())
                        Autentificacion(Txt_Correo.Text, Txt_Password.Text);
                    else
                    {
                        Lbl_Mensaje.Visible = true;
                        Lbl_Mensaje.Text = "El correo ingresado no es un usuario valido.";
                        Txt_Img.Text = "";
                    }
                }
                else
                {
                    Lbl_Mensaje.Visible = true;
                    Lbl_Mensaje.Text = "C&oacute;digo ingresado incorrecto.";
                    Txt_Img.Text = "";
                }
            }
            else
            {
                if (Txt_Correo.Text.Trim() == String.Empty)
                {
                    Lbl_Mensaje.Text = "Proporcione el Usuario para poder acceder al sistema.";
                    Lbl_Mensaje.Visible = true;
                    Txt_Correo.Focus();
                    return;
                }
                if (Txt_Password.Text.Trim() == String.Empty)
                {
                    Lbl_Mensaje.Visible = true;
                    Lbl_Mensaje.Text = "Proporcione la Contraseña para poder acceder al sistema.";
                    Txt_Password.Focus();
                    return;
                }
                if (Txt_Img.Text.Trim() == String.Empty)
                {
                    Lbl_Mensaje.Visible = true;
                    Lbl_Mensaje.Text = "Ingrese el C&oacute;digo de la imagen.";
                    Txt_Img.Focus();
                    return;
                }
            }
        }
        catch (Exception Ex) {
            Txt_Img.Text = "";
        }
    }

    private bool VerificarUsuario()
    {
        Cls_Cat_Clientes_Negocio Cliente = new Cls_Cat_Clientes_Negocio();
        bool usuario_valido = false;
        Cliente.P_Email = Txt_Correo.Text.Trim();
        DataTable dt = Cliente.VerificarEmail();
        if (dt.Rows.Count > 0)
        {
            txt_cliente_temporal.Value = dt.Rows[0]["Cliente_ID"].ToString().Trim();
            
            usuario_valido = true;
            if (dt.Rows[0][Cat_Adm_Clientes.Campo_Estatus].ToString() == "BLOQUEADO")
            {
                Lbl_Mensaje.Text = "La cuenta esta: Bloqueada espere 10 min";
                Lbl_Mensaje.Visible = true;
                Txt_Img.Text = "";  
            }
            else
            {
                Lbl_Mensaje.Text = "La cuenta esta: Bloqueada " + dt.Rows[0][Cat_Adm_Clientes.Campo_Estatus] + ", Para reactiva la cuenta.";
                Lbl_Mensaje.Visible = true;
                Txt_Img.Text = "";
            }

        }
        return usuario_valido;
    }

    /****************************************************************************************
     NOMBRE DE LA FUNCION: Autentificacion
     DESCRIPCION : Verificar que el usuario y password sean validos en el sistema para poder
                   acceder al mismo
     PARAMETROS  : Login: Nombre de usuario
                   Password: Contraseña
     CREO        : Yazmin A Delgado Gómez
     FECHA_CREO  : 14-Septiembre-2010
     MODIFICO          :
     FECHA_MODIFICO    :
     CAUSA_MODIFICACION:
    ****************************************************************************************/
    private void Autentificacion(String Usuario, String Password)
    {
        DataTable Dt_Cliente;
        Cls_Cat_Clientes_Negocio Cls_Cliente = new Cls_Cat_Clientes_Negocio();
        try
        {
            //Cls_Cliente.P_Estatus = "ACTIVO";
            Cls_Cliente.P_Email = Usuario.ToUpper();
            Cls_Cliente.P_Contrasena = Cls_Seguridad.Encriptar(Password.Trim());

            String Contrasenia = Cls_Seguridad.Desencriptar("A4+QG15mob0lpwfOZdk3rA==");

            Dt_Cliente = Cls_Cliente.Consultar_Clientes();
            if (Dt_Cliente.Rows.Count > 0)
            {
                if (Dt_Cliente.Rows[0][Cat_Adm_Clientes.Campo_Estatus].ToString() == "ACTIVO")
                {
                    Cls_Sessiones.Razon_Social = Dt_Cliente.Rows[0][Cat_Adm_Clientes.Campo_Razon_Social].ToString();
                    Cls_Sessiones.Cliente_ID = Dt_Cliente.Rows[0][Cat_Adm_Clientes.Campo_Cliente_Id].ToString();
                    Cls_Sessiones.Nombre_Corto = Dt_Cliente.Rows[0][Cat_Adm_Clientes.Campo_Nombre_Corto].ToString();
                    Cls_Sessiones.Predio_ID = Dt_Cliente.Rows[0][Cat_Adm_Clientes.Campo_Predio_Id].ToString();
                    Cls_Sessiones.Correo_Electronico = Dt_Cliente.Rows[0][Cat_Adm_Clientes.Campo_Email].ToString();
                    Cls_Sessiones.Gestor_Base_Datos = ConfigurationManager.ConnectionStrings["Irapuato"].ProviderName.ToString();
                    Cls_Sessiones.Rpu = Dt_Cliente.Rows[0][Cat_Cor_Predios.Campo_RPU].ToString();
                    FormsAuthentication.Initialize();
                    FormsAuthentication.RedirectFromLoginPage(Convert.ToString(Session[Cls_Sessiones.Empleado_Nombre]), false);
                    Cls_Sessiones.Mostrar_Menu = true;
                }
                else
                {
                    if (Dt_Cliente.Rows[0][Cat_Adm_Clientes.Campo_Estatus].ToString() == "BLOQUEADO")
                    {
                        VerificarCuentaBloqueado(Dt_Cliente, true);
                    }
                    else
                    {
                        Lbl_Mensaje.Text = "La cuenta esta Bloqueada " + Dt_Cliente.Rows[0][Cat_Adm_Clientes.Campo_Estatus] + ", Para reactiva la cuenta.";
                        Lbl_Mensaje.Visible = true;
                        Txt_Img.Text = "";
                    }
                }
            }
            else
            {
                Cls_Cat_Clientes_Negocio Cliente = new Cls_Cat_Clientes_Negocio();
                Cliente.P_Email = Txt_Correo.Text.Trim();
                DataTable dt = Cliente.VerificarEmail();
                if (dt.Rows[0][Cat_Adm_Clientes.Campo_Estatus].ToString() == "BLOQUEADO")
                {
                    VerificarCuentaBloqueado(dt, false);
                }
                else if (dt.Rows[0][Cat_Adm_Clientes.Campo_Estatus].ToString() == "PERMANENTE")
                {
                    Lbl_Mensaje.Text = "La cuenta esta: Bloqueada " + dt.Rows[0][Cat_Adm_Clientes.Campo_Estatus] + ", Para reactiva la cuenta.";
                    Lbl_Mensaje.Visible = true;
                    Txt_Img.Text = "";
                }
                else
                {
                    Deshabilitar_Usuario(txt_cliente_temporal.Value);
                }
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje.Visible = true;
            Lbl_Mensaje.Text = "Error: " + Ex.Message;
            Txt_Img.Text = "";
            
        }
    }

    private void VerificarCuentaBloqueado(DataTable Dt, bool ban)
    {
        List<Cls_Bloqueo> lista = new List<Cls_Bloqueo>();
        lista = SIAC.Metodos_Generales.Cls_Metodos_Generales.ConvertDataTableToList<Cls_Bloqueo>(Dt);

        String fecha_bloqueo = Convert.ToDateTime(lista[0].Fecha_Bloqueo_Temporal).ToString("ddMMyyyy");
        DateTime fecha_actual = DateTime.Now;
        TimeSpan hora = Convert.ToDateTime(lista[0].Fecha_Bloqueo_Temporal).TimeOfDay;
        TimeSpan hora_actual = DateTime.Now.TimeOfDay - hora;

        if (fecha_actual.ToString("ddMMyyyy") == fecha_bloqueo)
        {
            if (hora_actual.Minutes >= 10)
            {

                if (ban)
                {
                    ModificarDatosBloqueos(0, lista[0].No_Bloqueo_Temporal, txt_cliente_temporal.Value, "ACTIVO");

                    Cls_Sessiones.Razon_Social = Dt.Rows[0][Cat_Adm_Clientes.Campo_Razon_Social].ToString();
                    Cls_Sessiones.Cliente_ID = Dt.Rows[0][Cat_Adm_Clientes.Campo_Cliente_Id].ToString();
                    Cls_Sessiones.Nombre_Corto = Dt.Rows[0][Cat_Adm_Clientes.Campo_Nombre_Corto].ToString();
                    Cls_Sessiones.Predio_ID = Dt.Rows[0][Cat_Adm_Clientes.Campo_Predio_Id].ToString();
                    Cls_Sessiones.Correo_Electronico = Dt.Rows[0][Cat_Adm_Clientes.Campo_Email].ToString();
                    Cls_Sessiones.Gestor_Base_Datos = ConfigurationManager.ConnectionStrings["Irapuato"].ProviderName.ToString();
                    Cls_Sessiones.Rpu = Dt.Rows[0][Cat_Cor_Predios.Campo_RPU].ToString();
                    FormsAuthentication.Initialize();
                    FormsAuthentication.RedirectFromLoginPage(Convert.ToString(Session[Cls_Sessiones.Empleado_Nombre]), false);
                    Cls_Sessiones.Mostrar_Menu = true;
                }
                else
                {
                    ModificarDatosBloqueos(1, lista[0].No_Bloqueo_Temporal, txt_cliente_temporal.Value, "ACTIVO");
                    Lbl_Mensaje.Visible = true;
                    Lbl_Mensaje.Text = "Intente de nuevo, contraseña erronea. Numero de Intentos: " + 1;
                    Txt_Img.Text = "";
                }
            }
            else
            {
                Lbl_Mensaje.Visible = true;
                Lbl_Mensaje.Text = "La cuenta esta sido bloqueada. Espere " + (10 - hora_actual.Minutes) + " min para intentarlo de nuevo.";
                Txt_Img.Text = "";
            }
        }
        else {
            if (ban)
            {
                ModificarDatosBloqueos(0, lista[0].No_Bloqueo_Temporal, txt_cliente_temporal.Value, "ACTIVO");

                Cls_Sessiones.Razon_Social = Dt.Rows[0][Cat_Adm_Clientes.Campo_Razon_Social].ToString();
                Cls_Sessiones.Cliente_ID = Dt.Rows[0][Cat_Adm_Clientes.Campo_Cliente_Id].ToString();
                Cls_Sessiones.Nombre_Corto = Dt.Rows[0][Cat_Adm_Clientes.Campo_Nombre_Corto].ToString();
                Cls_Sessiones.Predio_ID = Dt.Rows[0][Cat_Adm_Clientes.Campo_Predio_Id].ToString();
                Cls_Sessiones.Correo_Electronico = Dt.Rows[0][Cat_Adm_Clientes.Campo_Email].ToString();
                Cls_Sessiones.Gestor_Base_Datos = ConfigurationManager.ConnectionStrings["Irapuato"].ProviderName.ToString();
                Cls_Sessiones.Rpu = Dt.Rows[0][Cat_Cor_Predios.Campo_RPU].ToString();
                FormsAuthentication.Initialize();
                FormsAuthentication.RedirectFromLoginPage(Convert.ToString(Session[Cls_Sessiones.Empleado_Nombre]), false);
                Cls_Sessiones.Mostrar_Menu = true;
            }
            else
            {
                ModificarDatosBloqueos(1, lista[0].No_Bloqueo_Temporal, txt_cliente_temporal.Value, "ACTIVO");
                Lbl_Mensaje.Visible = true;
                Lbl_Mensaje.Text = "Intente de nuevo, contraseña erronea. Numero de Intentos: " + 1;
                Txt_Img.Text = "";
            }
        }
    }

    private void ModificarDatosBloqueos(int numero_intento, int no_bloqueos, string cliente, string estatus) {

        Cls_Cat_Clientes_Negocio cliente_negocio = new Cls_Cat_Clientes_Negocio();
        cliente_negocio.Modificar_Datos_Bloqueos(numero_intento, no_bloqueos, cliente, estatus);

    
    }
    
    private Boolean Deshabilitar_Usuario(String Cliente_ID)
    {
        Cls_Cat_Clientes_Negocio Rs_Usuario_Estatus = new Cls_Cat_Clientes_Negocio();
        DataTable Dt_Resultado_Consulta = new DataTable();
        Boolean Activo = true;
        try
        {
            int Dias;
            DateTime Fecha_Ultimo_Acceso = new DateTime();
            DateTime Fecha_Actual = new DateTime();
            Rs_Usuario_Estatus.P_Cliente_Id = Cliente_ID;
            Dt_Resultado_Consulta = Rs_Usuario_Estatus.Consultar_Clientes();
            int Bloqueos = String.IsNullOrEmpty(Dt_Resultado_Consulta.Rows[0]["No_Bloqueo_Temporal"].ToString()) ? 0 : Convert.ToInt16(Dt_Resultado_Consulta.Rows[0]["No_Bloqueo_Temporal"].ToString());
            int intentos = String.IsNullOrEmpty(Dt_Resultado_Consulta.Rows[0]["No_Intentos_Temporal"].ToString()) ? 0 : Convert.ToInt16(Dt_Resultado_Consulta.Rows[0]["No_Intentos_Temporal"].ToString());
             
            if (!String.IsNullOrEmpty(Dt_Resultado_Consulta.Rows[0]["Fecha_Bloqueo_Temporal"].ToString()))
            {
                Fecha_Ultimo_Acceso = Convert.ToDateTime(Dt_Resultado_Consulta.Rows[0]["Fecha_Bloqueo_Temporal"].ToString());
            }
            else
            {
                Fecha_Ultimo_Acceso = DateTime.Now;
            }
            Fecha_Actual = DateTime.Now;
            Dias = (Fecha_Actual - Fecha_Ultimo_Acceso).Days;
            if (Dias >= 1)
            {
                if (Bloqueos == 3)
                {
                    ModificarDatosBloqueos(0, 0, Cliente_ID, "PERMANENTE");
                    Lbl_Mensaje.Visible = true;
                    Lbl_Mensaje.Text = "La Cuenta ha sido Bloqueada Permanentemente. Para reactivar ponerse en contacto con Atención de usuario";
                }
                else {
                    ModificarDatosBloqueos(1, Bloqueos + 1, Cliente_ID, "BLOQUEADO");
                    Lbl_Mensaje.Visible = true;
                    Lbl_Mensaje.Text = "La cuenta ha sido bloqueada. Espere 10 min para intentarlo de nuevo.";
                    Txt_Img.Text = "";
                }
            }
            else
            {
                if (Bloqueos == 3)
                {
                    ModificarDatosBloqueos(0, 0, Cliente_ID, "PERMANENTE");
                    Lbl_Mensaje.Visible = true;
                    Lbl_Mensaje.Text = "La Cuenta ha sido Bloqueada Permanentemente. Para reactivar ponerse en contacto con Atención de usuario";
                    Txt_Img.Text = "";
                }
                else
                {
                    if (intentos == 5)
                    {
                        ModificarDatosBloqueos(0, Bloqueos + 1, Cliente_ID, "BLOQUEADO");
                        Lbl_Mensaje.Visible = true;
                        Lbl_Mensaje.Text = "La cuenta ha sido bloqueada. Espere 10 min para intentarlo de nuevo.";
                        Txt_Img.Text = "";
                    }
                    else {
                        ModificarDatosBloqueos(intentos + 1, Bloqueos, Cliente_ID, "ACTIVO");
                        Lbl_Mensaje.Visible = true;
                        Lbl_Mensaje.Text = "Intente de nuevo, contraseña erronea. Numero de Intentos: " + (intentos + 1);
                        Txt_Img.Text = "";
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Deshabilitar_Usuario " + ex.Message.ToString(), ex);
        }
        return Activo;
    }

    #region Eventos

    protected void Btn_Registrarse_Click(object sender, EventArgs e)
    {
        Response.Redirect("Frm_Validar_Cuenta.aspx");
    }
    protected void Btn_Ingresar_Click(object sender, EventArgs e)
    {
        Lbl_Mensaje.Text = "";
        Lbl_Mensaje.Visible = false;
        Iniciar_Sesion();
        Txt_Password.Attributes.Add("value", Txt_Password.Text);

    }   
    protected void Lnk_Reload_Click(object sender, EventArgs e)
    {
        Txt_Password.Attributes.Add("value", Txt_Password.Text);
        Txt_Img.Text = "";
    }
    
    #endregion





}