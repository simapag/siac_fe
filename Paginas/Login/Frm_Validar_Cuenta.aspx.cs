﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIAC.Cat_Clientes.Negocio;
using SIAC.Constantes;
using SIAC.Envio_Email;
using SIAC.Ope_Facturacion_Recibos.Negocio;
using SIAC.Seguridad;
using SIAC.Registro_Usuarios.Negocio;

public partial class Paginas_Login_Frm_Validar_Cuenta : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
        }
    }
    #region Metodos 
    ///**************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Consultar_Facturas
    ///DESCRIPCIÓN         : Consulta si existe la factura con los datos del que se piden
    ///PARAMENTROS         :
    ///CREO                : Jose Maldonado Mendez
    ///FECHA_CREO          : 30/Noviembre/2015
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///*******************************************************************************
    public DataTable Consultar_Facturas()
    {
        Cls_Ope_Cor_Facturacion_Recibos_Negocio Rs_Factura = new Cls_Ope_Cor_Facturacion_Recibos_Negocio();
        DateTime Fecha_Emision;
        DateTime.TryParseExact(Txt_Fecha_Emision.Text.Trim(), "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out Fecha_Emision);
        Rs_Factura.P_Fecha_Emision = Fecha_Emision;
        Rs_Factura.P_RPU = Txt_RPU.Text.Trim();
        Rs_Factura.P_Total_Pagar = Convert.ToDecimal(Txt_Total_A_Pagar.Text);
        //Rs_Factura.P_No_Cuenta = Txt_No_Cuenta.Text.Trim();
        DataTable Dt = Rs_Factura.Consultar_Factura();
        
            return Dt;
    }
    ///**************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Componentes
    ///DESCRIPCIÓN         : Valida los componentes del formulario
    ///PARAMENTROS         :
    ///CREO                : Jose Maldonado Mendez
    ///FECHA_CREO          : 30/Noviembre/2015
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///*******************************************************************************
    private Boolean Validar_Componentes() {
        bool Validar = true;
        String Mensaje = "";
        String Mensaje2 = "";
        Lbl_Mensaje.Text = "";
        bool Validacion = true;


        //if (String.IsNullOrEmpty(Txt_No_Cuenta.Text))
        //{
        //    Mensaje += !Validar ? ", " : "";
        //    Mensaje += "No Cuenta";
        //    Validar = false;
        //}
        //else { 
        //    String Expresion = "^[+-]?\\d+?$";
        //    if(!Regex.IsMatch(Txt_No_Cuenta.Text,Expresion)){
        //        Mensaje2 += !Validacion ? ", " : "";
        //        Mensaje2 += "No Cuenta";
        //        Validacion = false;
        //    }

        //}
        if (String.IsNullOrEmpty(Txt_RPU.Text))
        {
            Mensaje += !Validar ? ", " : "";
            Mensaje += "RPU";

            Validar = false;
        }
        else
        {
            String Expresion = "^[+-]?\\d+?$";
            if (!Regex.IsMatch(Txt_RPU.Text, Expresion))
            {
                Mensaje2 += !Validacion ? ", " : "";
                Mensaje2 += "RPU";
                Validacion = false;
            }

        }
        if (String.IsNullOrEmpty(Txt_Fecha_Emision.Text))
        {
            Mensaje += !Validar ? ", " : "";
            Mensaje += "Fecha de Emisión";

            Validar = false;
        }
        else
        {
            DateTime Fecha;
            bool Fecha_Correcta = DateTime.TryParseExact(Txt_Fecha_Emision.Text.Trim(), "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out Fecha);
            if (!Fecha_Correcta) {
                Mensaje2 += !Validacion ? ", " : "";
                Mensaje2 += "Fecha de Emisión";

                Validacion = false;
            }
        }
        if (String.IsNullOrEmpty(Txt_Total_A_Pagar.Text))
        {
            Mensaje += !Validar ? ", " : "";
            Mensaje += "Total a Pagar";

            Validar = false;
        }
        else
        {
            string expresion = "^\\d+(\\.\\d{1,2})?$";
            if (!Regex.IsMatch(Txt_Total_A_Pagar.Text.Trim(), expresion))
            {
                Mensaje2 += !Validacion ? ", " : "";
                Mensaje2 += "Total a Pagar";

                Validacion = false;
            }
        }
        if (String.IsNullOrEmpty(Txt_Correo_Electronico.Text))
        {
            Mensaje += !Validar ? ", " : "";
            Mensaje += "Email";

            Validar = false;
        }
        else {
            String expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if(!Regex.IsMatch(Txt_Correo_Electronico.Text.Trim(),expresion)){
                Mensaje2 += !Validacion ? ", " : "";
                Mensaje2 += "Email";

                Validacion = false;
            }
        }
        if (!Validar || !Validacion)
        {
            String Mensajes = "";
            if (!Validar)
            {
                Mensajes += "Faltan los siguientes datos: "+ Mensaje;
                Mensajes += "<br>";
            }
            if (!Validacion)
                Mensajes += "Datos incorrectos: " + Mensaje2;
            Mensaje_Error(Mensajes);
            Validar = false;
        }

        return Validar;
    }
    ///**************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Mensaje_Error
    ///DESCRIPCIÓN         : Limpiar el mensaje de error
    ///PARAMENTROS         :
    ///CREO                : Jose Maldonado Mendez
    ///FECHA_CREO          : 30/Noviembre/2015
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///*******************************************************************************
    private void Mensaje_Error() {
        Lbl_Mensaje.Text = "";
        Lbl_Mensaje.Visible = false;
    }
    ///**************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Mensaje_Error
    ///DESCRIPCIÓN         : Consulta si existe la factura con los datos del que se piden
    ///PARAMENTROS         : Mensaje.- El mensaje de error que mostrara en la pantalla
    ///CREO                : Jose Maldonado Mendez
    ///FECHA_CREO          : 30/Noviembre/2015
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///*******************************************************************************
    private void Mensaje_Error(String Mensaje)
    {
        Lbl_Mensaje.Text = Mensaje;
        Lbl_Mensaje.Visible = true;
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Verificar_Correo
    ///DESCRIPCIÓN: Verifica si el correo existe el correo.
    ///PARAMETROS:  
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  30/Noviembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private bool Verificar_Correo()
    {
        bool validar = true;
        Cls_Cat_Clientes_Negocio Cliente = new Cls_Cat_Clientes_Negocio();
        Cls_Cat_Registro_Usuarios_Negocio Registro = new Cls_Cat_Registro_Usuarios_Negocio();
        //Registro.P_Email = Txt_Correo_Electronico.Text.Trim();
        //Registro.P_Estatus = "CONFIRMADO";
        //DataTable Dt_Registros = Registro.Consultar_Registro_Usuario();
        //if (Dt_Registros.Rows.Count > 0) {
        //    Mensaje_Error("El correo ya existe.");
        //    validar = false;
        //}
        Cliente.P_Email = Txt_Correo_Electronico.Text.Trim();
        Cliente.P_Estatus = "ACTIVO";

        DataTable Dt_Clientes = Cliente.Consultar_Clientes();
        if (Dt_Clientes.Rows.Count > 0)
        {
            Mensaje_Error("El correo ya existe.");
            validar = false;
        }

        return validar;
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Verificar_Predio
    ///DESCRIPCIÓN: Verifica si ya hay cliente utilizando el rpu
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  14/Diciembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private bool Verificar_Predio()
    {
        Cls_Cat_Clientes_Negocio Clientes = new Cls_Cat_Clientes_Negocio();
        bool Validar = true;
        Clientes.P_Predio_Id = Txt_RPU.Text.Trim();
        DataTable Dt_Predios = Clientes.Consultar_Predios();
        if (Dt_Predios.Rows.Count > 0)
        {
            Mensaje_Error("Esta utilizado el RPU.");
            Validar = false;
        }
        
        return Validar;
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Crear_Mensaje
    ///DESCRIPCIÓN: Crea el mensaje de la validacion de la cuenta.
    ///PARAMETROS:  ID
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  30/Noviembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private string Crear_Mensaje(string Id)
    {
        String Id_Cliente = Id;
        String[] Url_Segmento = Request.Url.Segments;
        String Url = Request.Url.Scheme + "://";
        Url += Request.Url.Authority;
        for (int i = 0; i < Url_Segmento.Length-1; i++)
        {
            Url += Url_Segmento[i];
        }
        String Mensaje = "<font face='arial' size='4'> " +
            "<p><b>PORTAL DE FACTURACI&Oacute;N SIMAPAG</b></p>" +
            "<p><b>Mensaje de confirmación de registro de cuenta</b></p>" +
            "</font>" +
            "<font face='arial' size='3'>" +
            "<p>Para continuar con el registro de la cuenta haga clic en el siguiente liga: " +
            "<P> <a href =" + Url + "Frm_Confirmar_Cuenta.aspx?ref=" + Id_Cliente + ">Confimar Cuenta</a></P>" +
            "</font>";
        Mensaje += "<P><font face='arial' size='2'><p>Si recibi&oacute; este correo por error, favor de ign&oacute;ralo.</p></font>";
        return Mensaje;
    ;
    }
    #endregion
    #region Eventos
    protected void Btn_Validar_Click(object sender, EventArgs e)
    {   
        Mensaje_Error();
        try
        {
            if (Validar_Componentes())
            {
                if (Verificar_Correo() && Verificar_Predio())
                {
                    DataTable Dt_Factura = Consultar_Facturas();
                    if (Dt_Factura.Rows.Count > 0)
                    {
                        Cls_Cat_Registro_Usuarios_Negocio Cliente = new Cls_Cat_Registro_Usuarios_Negocio();
                        DateTime fecha;
                        DateTime.TryParseExact(Txt_Fecha_Emision.Text, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out fecha);
                        Cliente.P_Email = Txt_Correo_Electronico.Text.Trim();
                        Cliente.P_Predio_Id = Dt_Factura.Rows[0][Ope_Cor_Facturacion_Recibos.Campo_Predio_Id].ToString();
                        Cliente.P_Estatus = "DESHABILITADO";
                        //Cliente.P_No_Cuenta = Txt_No_Cuenta.Text.Trim();
                        Cliente.P_Rpu = Txt_RPU.Text.Trim();
                        Cliente.P_Total_Pagar = Txt_Total_A_Pagar.Text;
                        Cliente.P_Fecha_Emision = fecha;
                        if (Cliente.Alta_Registro_Usuarios())
                        {
                            String Registro_ID = Cliente.P_Registro_Id.Trim();
                            String Mensaje = Crear_Mensaje(Registro_ID);
                            bool Correo_Enviado = Cls_Enviar_Correo.Envia_Correo(Mensaje, Txt_Correo_Electronico.Text.Trim(), "Validacion de cuenta");
                            if (!Correo_Enviado)
                            {
                                //Lbl_Mensaje_Correcto.Text = "Correo Enviado";
                                //Lbl_Mensaje_Correcto.Visible = true;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Confirm", "showCorreo();", true);
                                Limpiar_Controles();
                                //Response.Redirect("Frm_Login.aspx");
                            }
                            else
                            {
                                Cliente.P_Registro_Id = Registro_ID;
                                Cliente.Eliminar_Registro();
                                Mensaje_Error("Error al dar de alta la cuenta. Favor de intentar nuevamente.");
                            }
                        }
                    }
                    else
                    {
                        Mensaje_Error("No se encuentra datos de factura. Favor de verificar los datos.");
                    }
                }
            }   
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    private void Limpiar_Controles()
    {
        Txt_Correo_Electronico.Text = "";
        Txt_Fecha_Emision.Text = "";
        //Txt_No_Cuenta.Text = "";
        Txt_RPU.Text = "";
        Txt_Total_A_Pagar.Text = "";
    }
    protected void Btn_Regresar_Click(object sender, EventArgs e)
    {
        Response.Redirect("Frm_Login.aspx");
    }
    #endregion
}