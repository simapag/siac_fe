﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Erp.Sesiones;

public partial class Paginas_Paginas_Generales_MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            Btn_Salir.CausesValidation = false;

            if (Convert.ToBoolean(Cls_Sessiones.Mostrar_Menu))
            {
                Lbl_Nombre_Cliente.Text = Cls_Sessiones.Razon_Social;
                Hdf_Cliente.Value = Cls_Sessiones.Cliente_ID;
                Hdf_Rpu.Value = Cls_Sessiones.Rpu;
                Hdf_Predio.Value = Cls_Sessiones.Predio_ID;
                Hdf_Correo.Value = Cls_Sessiones.Correo_Electronico;
            }
            else
            {
                Response.Redirect("../Login/Frm_Login.aspx");
            }
           
        }
    }
    protected void Btn_Salir_Click(object sender, EventArgs e)
    {
        Session.RemoveAll();
        FormsAuthentication.SignOut();
        Session.Abandon();
        Response.Redirect("../Login/Frm_Login.aspx"); //Redireccionamos a la pagina de login

    }
}
