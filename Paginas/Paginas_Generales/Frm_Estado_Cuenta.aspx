﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true"
    CodeFile="Frm_Estado_Cuenta.aspx.cs" Inherits="Paginas_Paginas_Generales_Frm_Estado_Cuenta" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="../../JavaScript/jquery-easyiu-1.3.1/themes/default/easyui.css" rel="stylesheet" />
    <link href="../../Estilos/css/bootstrap.css" rel="stylesheet" />


    <script src="../../JavaScript/jquery/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../../JavaScript/Facturacion/Frm_Estado_Cuenta.js" type="text/javascript"></script>
    <script src="../../JavaScript/jquery-easyiu-1.3.1/jquery.easyui.min.js" type="text/javascript"></script>

    <title>Estado de Cuenta</title>
    <script type="text/javascript">
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantener_Session.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para mantener la sesión activa
        setInterval('MantenSesion()', '<%=(int)(0.9*(Session.Timeout * 60000))%>');


        function Abrir_Ventana_Modal(Url, Propiedades) {
            window.showModalDialog(Url, null, Propiedades);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Contenido" runat="Server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="Udp_Estado_Cuenta" runat="server">

        <ContentTemplate>
            <%--            <asp:UpdateProgress ID="Uprg_Estado_Cuenta" runat="server" AssociatedUpdatePanelID="Udp_Estado_Cuenta"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../../imagenes/paginas/Updating.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>--%>




            <div id="ventana_estado_cuenta">
                <div class="jumbotron">
                    <div class="container">
                        <asp:Label ID="Lbl_Titulo" runat="server"><h2>Estado de Cuenta</h2></asp:Label>
                    </div>
                </div>
                <div class="container">
                    <div class="form-group">
                        <asp:Label ID="Lbl_Mensaje" runat="server" CssClass="alert alert-danger center-block" Visible="false"></asp:Label>
                        <asp:Label ID="Lbl_Mensaje_Correcto" runat="server" CssClass="alert alert-success center-block" Visible="false"></asp:Label>

                    </div>

                    <div class="panel-primary col-md-7">
                        <div class="panel panel-heading">Datos Usuario</div>
                        <div class="panel panel-body">

                            <div class="form-group">
                                <div  class ="col-md-3" style="top:7px">
                                <label >No. Cuenta</label>
                                </div>
                                <div class="col-md-9">
                                    <asp:TextBox ID="Txt_No_Cuenta" name="Txt_No_Cuenta" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div  class ="col-md-3" style="top:7px">
                                    <label>RPU</label>
                                </div>
                                <div class="col-md-9">
                                    <asp:TextBox ID="Txt_RPU" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class ="col-md-3" style="top:7px">
                                    <label>Usuario</label>
                                </div>
                                <div class="col-md-9">
                                    <asp:TextBox ID="Txt_Usuario" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div  class ="col-md-3" style="top:7px">
                                    <label>Direcci&oacute;n</label>
                                </div>
                                <div class="col-md-9">
                                    <asp:TextBox ID="Txt_Direccion" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div  class ="col-md-3" style="top:7px">
                                    <label>Colonia</label>
                                </div>
                                <div class="col-md-9">
                                    <asp:TextBox ID="Txt_Colonia" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group" style="display: none;">
                                <div  class ="col-md-3" style="top:7px">
                                    <label>Direcci&oacute;n Corta</label>
                                </div>
                                <div class="col-md-9">
                                    <asp:TextBox ID="Txt_Direccion_Corta" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div  class ="col-md-3" style="top:7px">
                                    <label>Clasificaci&oacute;n</label>
                                </div>
                                <div class="col-md-9">
                                    <asp:TextBox ID="Txt_Clasificacion" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <%--<div class="form-group">
                                <label class="col-md-3 control-label">Referencia</label>
                                <div class="col-md-9">
                                    <asp:TextBox ID="Txt_Referencia" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                                </div>
                            </div>--%>
                        </div>
                    </div>

                    <div class="panel-primary col-md-5">
                        <div class="panel panel-heading">Datos Facturaci&oacute;n</div>
                        <div class="panel panel-body">

                            <div class="form-group">
                                <div  class ="col-md-6" style="top:7px">
                                    <label>&Uacute;ltimo Periodo Pagado</label>
                                </div>
                                <!--<label class="col-md-6 control-label">&Uacute;ltimo Periodo Pagado</label>-->
                                <div class="col-md-6">
                                    <asp:TextBox ID="Txt_Ultimo_Bimestre_Pagado" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div  class ="col-md-6" style="top:7px">
                                    <label>Fecha del Aviso</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:TextBox ID="Txt_Periodo_Aviso" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                                </div>

                            </div>
                            <div class="form-group">
                                <div  class ="col-md-6" style="top:7px">
                                    <label>Ciclo / Ruta</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:TextBox ID="Txt_Ruta" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                                </div>

                            </div>
                            <div class="form-group">
                                <div  class ="col-md-6" style="top:7px">
                                    <label>N&uacute;mero de Medidor</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:TextBox ID="Txt_No_Medidor" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                                </div>

                            </div>

                            <%--<div class="form-group">
                                <label class="col-md-6 control-label">Lectora</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="Txt_Lectura" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                                </div>

                            </div>--%>

                            <div class="form-group">
                                <div  class ="col-md-6" style="top:7px">
                                    <label>Lectura Anterior</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:TextBox ID="Txt_Lectura_Anterior" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                                </div>

                            </div>

                            <div class="form-group">
                                <div  class ="col-md-6" style="top:7px">
                                    <label>Lectura Actual</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:TextBox ID="Txt_Lectura_Actual" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                                </div>

                            </div>
                            <div class="form-group">
                                <div  class ="col-md-6" style="top:7px">
                                    <label>Consumo</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:TextBox ID="Txt_Consumo" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                                </div>

                            </div>

                            <div class="form-group">
                               <div  class ="col-md-6" style="top:7px">
                                    <label>Pagar Antes de</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:TextBox ID="Txt_Vence" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                                </div>

                            </div>
                            <div class="form-group">
                                <div  class ="col-md-6" style="top:7px">
                                    <label>Importe Periodo</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:TextBox ID="Txt_Importe_Bimestre" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div  class ="col-md-6" style="top:7px">
                                    <label>Adeudo Rezago</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:TextBox ID="Txt_Adeudo" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div  class ="col-md-6" style="top:7px">
                                    <label>Total a Pagar</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:TextBox ID="Txt_Total_Pagar" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div>
                        <%--<a id="Btn_Pagar" class="btn btn-success"  onclick="MostrarDatosPagos(); ">Pagar <span class='glyphicon glyphicon-ok'>
                        </span></a>--%>
                        <a id="Btn_Imprimir" class="btn btn-success" onclick="Consultar_Estado_Cuenta();">Imprimir <span class='glyphicon glyphicon-print'>
                        </span></a>
                    </div>
                </div>

            </div>


            <div id="ventanarealizarpago" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" onclick="cancelarPago();">&times;</button>
                            <h4 class="modal-title">Realizar Pago</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label>*Cantidad a Pagar</label>
                                <div>
                                    <span id="txt_monto_pagar" class="form-control"></span>
                                </div>
                            </div>
                            <%--                            <div class="form-group">
                                <label class="control-label">*Tipo de tarjeta</label>
                                <div class="">
                                    <asp:DropDownList ID="Cmb_Tipo_Tarjeta" runat="server" CssClass="form-control" requerido="true">
                                        <asp:ListItem Value="0">SELECCIONE</asp:ListItem>
                                        <asp:ListItem Value="CREDITO">CREDITO</asp:ListItem>
                                        <asp:ListItem Value="DEBITO">DEBITO</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>--%>
                            <div class="form-group">
                                <label class=" control-label">*No Tarjeta</label>
                                <div class="">
                                    <asp:TextBox ID="Txt_No_Tarjeta" runat="server" CssClass="form-control" group_pago="requerido" requerido="true"></asp:TextBox>
                                </div>
                                <label class="control-label">*No Aprobaci&oacute;n</label>
                                <div class="">
                                    <asp:TextBox ID="Txt_No_Aprobacion" runat="server" CssClass="form-control" group_pago="requerido" requerido="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">*Banco</label>
                                <div>
                                    <asp:DropDownList ID="Cmb_Banco" runat="server" CssClass="form-control" group_pago="requerido"></asp:DropDownList>
                                </div>
                            </div>

                            <%--<div class="form-group">
                                <label class="col-md-4">*Fecha de expiracion</label>
                                <div class="col-md-4">
                                    <asp:DropDownList ID="Cmb_Mes" runat="server" CssClass="form-control" requerido="true">
                                        <asp:ListItem Value="0">SELECCIONE</asp:ListItem>
                                        <asp:ListItem Value="01">01-ENERO</asp:ListItem>
                                        <asp:ListItem Value="02">02-FEBRERO</asp:ListItem>
                                        <asp:ListItem Value="03">03-MARZO</asp:ListItem>
                                        <asp:ListItem Value="04">04-ABRIL</asp:ListItem>
                                        <asp:ListItem Value="05">05-MAYO</asp:ListItem>
                                        <asp:ListItem Value="06">06-JUNIO</asp:ListItem>
                                        <asp:ListItem Value="07">07-JULIO</asp:ListItem>
                                        <asp:ListItem Value="08">08-AGOSTO</asp:ListItem>
                                        <asp:ListItem Value="09">09-SEPTIEMBRE</asp:ListItem>
                                        <asp:ListItem Value="10">10-OCTUBRE</asp:ListItem>
                                        <asp:ListItem Value="11">11-NOVIEMBRE</asp:ListItem>
                                        <asp:ListItem Value="12">12-DICIEMBRE</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <label class="col-md-1 control-label">*Año</label>
                                <div class="col-md-3">
                                    <asp:DropDownList ID="Cmb_Anio" runat="server" CssClass="form-control" requerido="true"> <asp:ListItem Value="12">2016</asp:ListItem></asp:DropDownList>
                                </div>
                            </div>--%>
                        </div>
                        <%--<div class="">
                                   <br /><br />
                               </div>--%>
                        <div class="modal-footer">

                            <a id="Btn_Realizar_Pago" class="btn btn-success" onclick="realizarPago();">Realizar pago <span class='glyphicon glyphicon-ok'>
                            </span></a>
                            <a id="Btn_Cancelar" class="btn btn-success" onclick="cancelarPago();">Cancelar <span class='glyphicon glyphicon-remove'>
                            </span></a>
                        </div>
                    </div>
                </div>
            </div>


            <div id="ventanamensaje" class="modal" style="position:fixed; top:38%; left:40%; padding:10px; width:14%; z-index:1001;">

                <img alt="" src="../../imagenes/paginas/Updating.gif" />

            </div>
       <div id="Div_Mensaje" class="modal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">SIMAPAG</h4>
                        </div>
                        <div class="modal-body">
                           <label id="Lbl_Mensajes"></label>

                        </div>
                        <div class="modal-footer">
                            <a id="Lnk_Aceptar" class="btn btn-success" onclick="cerrarVentana();">Aceptar <span class='glyphicon glyphicon-ok'></span></a>
                        </div>
                    </div>
                </div>
            </div>


        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

