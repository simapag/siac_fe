﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Inicio.aspx.cs" Inherits="Paginas_Paginas_Generales_Frm_Inicio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Inicio</title>
    <script type="text/javascript">
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantener_Session.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para mantener la sesión activa
        setInterval('MantenSesion()', '<%=(int)(0.9*(Session.Timeout * 60000))%>');


        function Abrir_Ventana_Modal(Url, Propiedades) {
            window.showModalDialog(Url, null, Propiedades);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Contenido" runat="Server">
    <div style="width: 98%; padding: 15px; height: 45%"></div>

</asp:Content>

