﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIAC.Facturacion.Negocio;
using Erp.Sesiones;
using System.Data;
public partial class Paginas_Paginas_Generales_Frm_Facturacion_Electronica : System.Web.UI.Page
{
    private DataTable S_Facturas {
        get { return (DataTable)Session["Dt_Facturas"]; }
        set { Session["Dt_Facturas"] = value; }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack) 
            //Buscar_Facturas();
    }

    #region Metodos

    /////******************************************************************************* 
    /////NOMBRE DE LA FUNCIÓN: Buscar_Facturas
    /////DESCRIPCIÓN: Consulta los recibos pagados del cliente.
    /////PARAMETROS:  
    /////CREO:        José Maldonado Méndez
    /////FECHA_CREO:  09/Enero/2016
    /////MODIFICO: 
    /////FECHA_MODIFICO:
    /////CAUSA_MODIFICACIÓN:
    /////*******************************************************************************
    //private void Buscar_Facturas()
    //{
    //    Cls_Ope_Facturacion_Negocio Facturas = new Cls_Ope_Facturacion_Negocio();
    //    Facturas.P_Predio_ID = Cls_Sessiones.Predio_ID;
    //    if (!String.IsNullOrEmpty(Txt_Folio.Text)) {
    //        Facturas.Codigo_Barras = Txt_Folio.Text.Trim();
    //    }
    //    if (Cmb_Facturas.SelectedIndex > 0) {
    //        Facturas.P_Estatus = Cmb_Facturas.SelectedValue;
    //    }
    //    S_Facturas = Facturas.Consultar_Facturas_Clientes();
    //    Cargar_Datos_Factura();
    //}

    /////******************************************************************************* 
    /////NOMBRE DE LA FUNCIÓN: Cargar_Datos_Factura
    /////DESCRIPCIÓN: Carga las facturas en el grid.
    /////PARAMETROS:  
    /////CREO:        José Maldonado Méndez
    /////FECHA_CREO:  09/Enero/2016
    /////MODIFICO: 
    /////FECHA_MODIFICO:
    /////CAUSA_MODIFICACIÓN:
    /////*******************************************************************************
    //private void Cargar_Datos_Factura(){
    //    Grid_Facturas.DataSource = S_Facturas;
    //    Grid_Facturas.DataBind();
    //}

    //#endregion

    //#region Eventos 

    //protected void Grid_Facturas_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        String S = Grid_Facturas.DataKeys[Grid_Facturas.SelectedRow.RowIndex].Values[0].ToString();
    //        Txt_Folio_Recibo.Value = S;
    //        //Grid_Facturas.SelectedIndex = -1;
    //        //Grid_Facturas.Attributes.Add("OnClick", "javascript:return Consultar_Folio()");
    //        //ClientScript.RegisterStartupScript(GetType(), "a", "Consultar_Folio();",true);
    //    }catch(Exception Ex){
            
    //    }
    //}
    //protected void Grid_Facturas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    Grid_Facturas.PageIndex = e.NewPageIndex;
    //    Grid_Facturas.DataSource = S_Facturas;
    //    Grid_Facturas.DataBind();
    //}
    //protected void Btn_Buscar_Click(object sender, EventArgs e)
    //{
    //    Buscar_Facturas();
    //}
    //protected void Btn_Limpiar_Click(object sender, EventArgs e)
    //{
    //    Txt_Folio.Text = "";
    //    Cmb_Facturas.SelectedIndex = 0;
    //}

    #endregion
    
}
