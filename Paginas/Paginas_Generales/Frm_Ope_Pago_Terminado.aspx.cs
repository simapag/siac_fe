﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIAC.Parametros_Pagos;
using System.Security.Cryptography;
using System.Web.Security;
using System.IO;
using System.Text;
using System.Data;
using System.Web.Services;
using System.Collections.Specialized;
using SIAC.Pagos_Internet.Datos;
using SIAC.Pagos_Internet.Bean;
using SIAC.Ope_Facturacion_Recibos.Negocio;
using System.ServiceModel;
using Newtonsoft.Json;

public partial class Paginas_Paginas_Generales_Frm_Ope_Pago_Terminado : System.Web.UI.Page
{
    private string ipaddress;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            Revisar_Peticion();
        }

    }

    public void Revisar_Peticion()
    {
        string s_transm;
        string c_referencia;
        string t_servicio;
        string t_importe;
        string sha1;
        string llave = "MmuulntiicpiapgioosmBEatnEcpoemCe2r0x12";
        string cadena_proteger = "";
        string cadena_hash = "";
        string autorizacion;
        string Origen_Peticion = "";
        string fecha_pago;
        string numero_tarjeta;
        string correo_electronico;
        string tipo_tarjeta;
        String Datos_Tarjeta;
        string banco;
        string banco_id;
        //Resultado ws del registro del pago
        string returnString;
        Cls_Pagos_Internet Obj_Pagos_Internet = new Cls_Pagos_Internet();
        NameValueCollection headers = Request.Headers;
        Cls_Bean_Pago_Internet Obj_Bean_Pago_Internet = new Cls_Bean_Pago_Internet();
        Cls_Ope_Cor_Facturacion_Recibos_Negocio Facturacion = new Cls_Ope_Cor_Facturacion_Recibos_Negocio();

        for (int i = 0; i < headers.Count; i++)
        {
            string key = headers.GetKey(i);
            string value = headers.Get(i);

            Origen_Peticion = "";
            if (key == "Origin" || key == "Origen")
            {
                Origen_Peticion = value;
                break;
            }

            //base.Response.Write(key + " = " + value + "<br/>");
        }


        var encoding = new System.Text.ASCIIEncoding();

        s_transm = Cls_Parametros_Pagos.s_transm;
        c_referencia = Cls_Parametros_Pagos.c_referencia;
        t_servicio = Cls_Parametros_Pagos.t_servicio;
        t_importe = Cls_Parametros_Pagos.t_importe;
        sha1 = Cls_Parametros_Pagos.sha1;
        autorizacion = Cls_Parametros_Pagos.autorizacion;
        fecha_pago = Cls_Parametros_Pagos.fecha_pago;
        numero_tarjeta = Cls_Parametros_Pagos.numero_tarjeta_credito;
        correo_electronico = Cls_Parametros_Pagos.correo_electronico;
        tipo_tarjeta = Cls_Parametros_Pagos.tipo_tarejeta;
        banco = Cls_Parametros_Pagos.banco;
        banco_id = Cls_Parametros_Pagos.banco_id;

        cadena_proteger = s_transm + c_referencia + t_importe + autorizacion;
        Datos_Tarjeta = "[" + Datos_Tarjetas(t_importe, autorizacion, numero_tarjeta, banco, banco_id) + "]";

        //if (sha1.Trim().Length == 0)
        //{
        //    lbl_Resultado.Text = "Solicitud de Pago en Internet Incorrecta";
        //    return;
        //}

        //byte[] keyByte = encoding.GetBytes(llave);
        //byte[] messageBytes = encoding.GetBytes(cadena_proteger);


        //HMACSHA1 hmacsha1 = new HMACSHA1(keyByte);
        //byte[] hashmessage1 = hmacsha1.ComputeHash(messageBytes);
        //cadena_hash = ByteToString(hashmessage1);

        //if (Origen_Peticion.Trim().Length == 0)
        //{
        //    lbl_Resultado.Text = "Origen de la petición incorrecta";
        //    return;
        //}

        //if (Obj_Pagos_Internet.Servidor_Autorizado(Origen_Peticion) == "no")
        //{
        //    lbl_Resultado.Text = "Origen de la petición incorrecta";
        //    return;
        //}


        //if (sha1 == cadena_hash.ToLower())
        //{
            Int64 ID;
            ID = Convert.ToInt64(s_transm);


            Obj_Bean_Pago_Internet.no_factura_recibo = String.Format("{0:0000000000}", ID);
            Obj_Bean_Pago_Internet.numero_autorizacion = autorizacion;
            Obj_Bean_Pago_Internet.referencia_bancaria = c_referencia;
            Obj_Bean_Pago_Internet.importe_total = Convert.ToDouble(t_importe);
            Obj_Bean_Pago_Internet.correo_electronico = correo_electronico;

            //if (tipo_tarjeta == "01")
            //{
            Obj_Bean_Pago_Internet.fecha_pago_banco = fecha_pago.Trim();
            Obj_Bean_Pago_Internet.numero_tarjeta = numero_tarjeta;
            //}
            //else
            //{
            //    Obj_Bean_Pago_Internet.fecha_pago_banco = Cls_Parametros_Pagos.fecha_paga_american_express;
            //    Obj_Bean_Pago_Internet.numero_tarjeta = Cls_Parametros_Pagos.numero_tarjeta_america_express;
            //}


            try
            {
                String No_Facturacion_Cadena =  String.Format("{0:0000000000}", ID);//"0000000000" + s_transm;
                //int Cant_Cadena = No_Facturacion_Cadena.Length - 10;
                //No_Facturacion_Cadena = No_Facturacion_Cadena.Substring(Cant_Cadena);
                Facturacion.P_No_Factura_Recibo = No_Facturacion_Cadena;
                DataTable Dt_Facturacion_Recibos = Facturacion.Consultar_Factura();
                String RPU = Dt_Facturacion_Recibos.Rows[0]["RPU"].ToString();


                //Registrar pago mediante Web Service
                //ServiceReference1.Servicio_PagosClient proxy = null;

                try
                {
                    //proxy = new ServiceReference1.Servicio_PagosClient();
                    //returnString = proxy.pago_normal(RPU, t_importe, Datos_Tarjeta);
                    //proxy.Close();
                }
                catch (CommunicationException ex)
                {
                    returnString = ex.Message;
                    //proxy.Abort();
                }
                catch (TimeoutException ex)
                {
                    returnString = ex.Message;
                    //proxy.Abort();
                }
                catch (Exception ex)
                {
                    returnString = ex.Message;
                    //proxy.Abort();
                    throw;
                }
                finally
                {
                //    if (proxy != null)
                        //proxy.Abort();
                }
                //if (!Es_Numero(returnString))
                //{
                //    lbl_Resultado.Text = returnString;
                //    return;
                //}
                Guardar_Pago(Obj_Bean_Pago_Internet, Obj_Pagos_Internet);
                lbl_Resultado.Text = "Proceso Realizado Exitosamente !!!";
            }
            catch (Exception ex)
            {
                lbl_Resultado.Text = "Error: " + ex.Message;
            }
        //}
        //else
        //{
        //    lbl_Resultado.Text = "Solicitud de Pago en Internet Incorrecta s";
        //}
    }

    private string Datos_Tarjetas(String Monto, String No_Aprobacion, String No_Tarjeta, String Banco, String Banco_Id)
    {
        StringBuilder Buffer = new StringBuilder();
        StringWriter Escritor = new StringWriter(Buffer);
        JsonWriter Escribir_Formato_JSON = new JsonTextWriter(Escritor);
        String Cadena_Resultado = "[]";
        Escribir_Formato_JSON.Formatting = Newtonsoft.Json.Formatting.None;
        Escribir_Formato_JSON.WriteStartObject();
        Escribir_Formato_JSON.WritePropertyName("monto");
        Escribir_Formato_JSON.WriteValue(Monto);

        Escribir_Formato_JSON.WritePropertyName("no_aprobacion");
        Escribir_Formato_JSON.WriteValue(No_Aprobacion);

        Escribir_Formato_JSON.WritePropertyName("no_tarjeta");
        Escribir_Formato_JSON.WriteValue(No_Tarjeta);

        Escribir_Formato_JSON.WritePropertyName("banco");
        Escribir_Formato_JSON.WriteValue(Banco);

        Escribir_Formato_JSON.WritePropertyName("banco_id");
        Escribir_Formato_JSON.WriteValue(Banco_Id);

        Escribir_Formato_JSON.WriteEndObject();
        Cadena_Resultado = Buffer.ToString();
        return Cadena_Resultado;
    }
    public void Guardar_Pago(Cls_Bean_Pago_Internet Obj_Bean_Pago_Internet, Cls_Pagos_Internet Obj_Pagos_Internet)
    {

        Obj_Pagos_Internet.Guardar_Pago_Linea(Obj_Bean_Pago_Internet);

    }

    public static string ByteToString(byte[] buff)
    {
        string sbinary = "";

        for (int i = 0; i < buff.Length; i++)
        {
            sbinary += buff[i].ToString("X2"); // hex format
        }
        return (sbinary);
    }

    public static string GetIPAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0)
            {
                return addresses[0];
            }
        }

        return context.Request.ServerVariables["remote_host"];

    }

    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }

}