﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Frm_Imprimir_Estado_Cuenta.aspx.cs" Inherits="Paginas_Paginas_Generales_Frm_Imprimir_Estado_Cuenta" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="../../estilos/estilo_paginas.css" rel="stylesheet" />
    <link href="../../Estilos/css/bootstrap.css" rel="stylesheet" />
    <script type="text/javascript" src="../../JavaScript/jquery/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="../../JavaScript/js/bootstrap.js"></script>


    <script src="../../JavaScript/Facturacion/Js_Imprimir_Estado_Cuenta.js" type="text/javascript"></script>
    <link href="../../estilos/Frm_Ope_Facturacion.css" rel="stylesheet" />
    <title>Estado de Cuenta</title>

</head>
<body>
    <form id="form1" runat="server">

        <div id="ventana_estado_cuenta">

            <div class="container">
                <asp:Label ID="Label1" runat="server"><h2>Estado de Cuenta</h2></asp:Label>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <input type="text" class="form-control" id="Hdf_Rpu"/>
                    <asp:HiddenField ID="Hdf_Correo" runat="server" Value="adas"/>
                </div>
                <div class="col-md-2">
                    <a id="Btn_Buscar" class="btn btn-success" onclick="buscarEstadoCuenta();">Buscar <span class='glyphicon glyphicon-search'></span></a>
                </div>
                <div class="col-md-3">
                    <a id="Btn_Imprimir" class="btn btn-success" onclick="Consultar_Estado_Cuenta();">Imprimir <span class='glyphicon glyphicon-print'></span></a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="form-group">
                <asp:Label ID="Lbl_Mensaje" runat="server" CssClass="alert alert-danger center-block" Visible="false"></asp:Label>
                <asp:Label ID="Lbl_Mensaje_Correcto" runat="server" CssClass="alert alert-success center-block" Visible="false"></asp:Label>
            </div>

            <div class="panel-primary col-md-7">
                <div class="panel panel-heading">Datos Usuario</div>
                <div class="panel panel-body">

                    <div class="form-group">
                        <div class="col-md-3" style="top: 7px">
                            <label>No. Cuenta</label>
                        </div>
                        <div class="col-md-9">
                            <asp:TextBox ID="Txt_No_Cuenta" name="Txt_No_Cuenta" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3" style="top: 7px">
                            <label>RPU</label>
                        </div>
                        <div class="col-md-9">
                            <asp:TextBox ID="Txt_RPU" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3" style="top: 7px">
                            <label>Usuario</label>
                        </div>
                        <div class="col-md-9">
                            <asp:TextBox ID="Txt_Usuario" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3" style="top: 7px">
                            <label>Direcci&oacute;n</label>
                        </div>
                        <div class="col-md-9">
                            <asp:TextBox ID="Txt_Direccion" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3" style="top: 7px">
                            <label>Colonia</label>
                        </div>
                        <div class="col-md-9">
                            <asp:TextBox ID="Txt_Colonia" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3" style="top: 7px">
                            <label>Direcci&oacute;n Corta</label>
                        </div>
                        <div class="col-md-9">
                            <asp:TextBox ID="Txt_Direccion_Corta" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3" style="top: 7px">
                            <label>Clasificaci&oacute;n</label>
                        </div>
                        <div class="col-md-9">
                            <asp:TextBox ID="Txt_Clasificacion" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-primary col-md-5">
                <div class="panel panel-heading">Datos Facturaci&oacute;n</div>
                <div class="panel panel-body">

                    <div class="form-group">
                        <div class="col-md-6" style="top: 7px">
                            <label>&Uacute;ltimo Periodo Pagado</label>
                        </div>

                        <div class="col-md-6">
                            <asp:TextBox ID="Txt_Ultimo_Bimestre_Pagado" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6" style="top: 7px">
                            <label>Fecha del Aviso</label>
                        </div>
                        <div class="col-md-6">
                            <asp:TextBox ID="Txt_Periodo_Aviso" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-md-6" style="top: 7px">
                            <label>Ciclo / Ruta</label>
                        </div>
                        <div class="col-md-6">
                            <asp:TextBox ID="Txt_Ruta" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-md-6" style="top: 7px">
                            <label>N&uacute;mero de Medidor</label>
                        </div>
                        <div class="col-md-6">
                            <asp:TextBox ID="Txt_No_Medidor" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                        </div>

                    </div>

                    <div class="form-group">
                        <div class="col-md-6" style="top: 7px">
                            <label>Lectura Anterior</label>
                        </div>
                        <div class="col-md-6">
                            <asp:TextBox ID="Txt_Lectura_Anterior" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                        </div>

                    </div>

                    <div class="form-group">
                        <div class="col-md-6" style="top: 7px">
                            <label>Lectura Actual</label>
                        </div>
                        <div class="col-md-6">
                            <asp:TextBox ID="Txt_Lectura_Actual" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-md-6" style="top: 7px">
                            <label>Consumo</label>
                        </div>
                        <div class="col-md-6">
                            <asp:TextBox ID="Txt_Consumo" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                        </div>

                    </div>

                    <div class="form-group">
                        <div class="col-md-6" style="top: 7px">
                            <label>Pagar Antes de</label>
                        </div>
                        <div class="col-md-6">
                            <asp:TextBox ID="Txt_Vence" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-md-6" style="top: 7px">
                            <label>Importe Periodo</label>
                        </div>
                        <div class="col-md-6">
                            <asp:TextBox ID="Txt_Importe_Bimestre" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6" style="top: 7px">
                            <label>Adeudo Rezago</label>
                        </div>
                        <div class="col-md-6">
                            <asp:TextBox ID="Txt_Adeudo" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6" style="top: 7px">
                            <label>Total a Pagar</label>
                        </div>
                        <div class="col-md-6">
                            <asp:TextBox ID="Txt_Total_Pagar" runat="server" CssClass="form-control" MaxLength="255" Enabled="false"></asp:TextBox>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </form>
</body>
</html>
