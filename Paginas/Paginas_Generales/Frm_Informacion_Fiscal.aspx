﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true"
    CodeFile="Frm_Informacion_Fiscal.aspx.cs" Inherits="Paginas_Paginas_Generales_Frm_Informacion_Fiscal"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Información Fiscal</title>

        <script src="../../JavaScript/jquery/jquery-1.7.1.js" type="text/javascript"></script>
    <script src="../../JavaScript/jquery/jquery-1.9.1.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantener_Session.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para mantener la sesión activa
        setInterval('MantenSesion()', '<%=(int)(0.9*(Session.Timeout * 60000))%>');


        function Abrir_Ventana_Modal(Url, Propiedades) {
            window.showModalDialog(Url, null, Propiedades);
        }

    </script>
      
    <script src="../../JavaScript/Js_Ventanas_Modal.js"></script>
    <style type="text/css"> .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
    position: relative;
    min-height: 1px;
    padding-right: 1px !important;
    padding-left: 0.5px !important;
}
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Contenido" runat="Server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="Udp_Confirmar" runat="server">

        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Comfirmar" runat="server" AssociatedUpdatePanelID="Udp_Confirmar" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../../imagenes/paginas/Updating.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="jumbotron">
                <div class="container">
                    <asp:Label ID="Lbl_Titulo" runat="server"><h2>Información Fiscal</h2></asp:Label>
                </div>
            </div>
            <div class="container">
                <div class="form-group">
                    <asp:Label ID="Lbl_Mensaje" runat="server" CssClass="alert alert-danger center-block" Visible="false"></asp:Label>

                    <asp:Label ID="Lbl_Mensaje_Correcto" runat="server" CssClass="alert alert-success center-block" Visible="false"></asp:Label>
                </div>
            </div>
            <div runat="server" class="container" id="Div_Principal">
                <div class="row">
                    <div  class ="col-md-2" style="top:7px">
                        <label>*Raz&oacute;n Social/Nombre</label>
                    </div>
                    <div class="col-md-10">
                        <asp:TextBox ID="Txt_Nombre" runat="server" CssClass="form-control text-uppercase" Enabled="false" TabIndex="1"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div  class ="col-md-2" style="top:7px">
                        <label>*Calle</label>
                    </div>
                    <div class="col-md-10">
                        <asp:TextBox ID="Txt_Calle" runat="server" CssClass="form-control text-uppercase" MaxLength="255" Enabled="false"
                            TabIndex="2"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div  class ="col-md-2" style="top:7px">
                        <label>*N&uacute;mero Ext.</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="Txt_Numero_Exterior" runat="server" CssClass="form-control text-uppercase" MaxLength="50"
                            Enabled="false" TabIndex="3"></asp:TextBox>
                    </div>
                    
                    <div class ="col-md-1" style="top:7px">
                        <label>*N&uacute;mero Int.</label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="Txt_Numero_Interior" runat="server" CssClass="form-control text-uppercase" MaxLength="50"
                            Enabled="false" TabIndex="4"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div  class ="col-md-2" style="top:7px">
                        <label>*Colonia</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="Txt_Colonia" runat="server" CssClass="form-control text-uppercase" MaxLength="100" Enabled="false"
                            TabIndex="5"></asp:TextBox>
                    </div>
                    <div  class ="col-md-1" style="top:7px">
                        <label>*C.P.</label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="Txt_CP" runat="server" CssClass="form-control text-uppercase" MaxLength="5" Enabled="false"
                            TabIndex="6"></asp:TextBox>
                    </div>
                </div>

                <div class="row">
                    <div  class ="col-md-2" style="top:7px">
                        <label>*Estado</label>
                    </div>
                    <div class="col-md-10">
                        <asp:DropDownList ID="Cmb_Estado" runat="server" AutoPostBack="true" TabIndex="7" OnSelectedIndexChanged="Cmb_Estado_SelectedIndexChanged"
                            CssClass="form-control text-uppercase" MaxLength="100" Enabled="false"></asp:DropDownList>
                    </div>
                </div>

                <div class="row">
                    <div  class ="col-md-2 control-label" style="top:7px">
                        <label>*Ciudad</label>
                    </div>
                    <div class="col-md-10">
                        <asp:DropDownList ID="Cmb_Ciudad" runat="server" CssClass="form-control text-uppercase" Enabled="false"
                            TabIndex="8"></asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <div  class ="col-md-2 control-label" style="top:7px">
                        <label>*Pa&iacute;s</label>
                    </div>
                    <div class="col-md-10">
                        <asp:TextBox ID="Txt_Pais" runat="server" CssClass="form-control text-uppercase" MaxLength="100" Enabled="false"
                            TabIndex="9"></asp:TextBox>
                    </div>
                </div>

                <div class="row">
                    <div  class ="col-md-2 control-label" style="top:7px">
                        <label>*RFC</label>
                    </div>
                    <div class="col-md-10">
                        <asp:TextBox ID="Txt_Rfc" runat="server" CssClass="form-control text-uppercase" MaxLength="20" Enabled="false"
                            TabIndex="10"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                <div  class ="col-md-2 control-label" style="top:7px">
                        <label>*Email Facturaci&oacute;n</label>
                    </div><div class="col-md-10">
                        <asp:TextBox ID="Txt_Email_Facturacion" runat="server" CssClass="form-control" Enabled="false" AutoCompleteType="Email"
                            TabIndex="11"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div  class ="col-md-2 control-label" style="top:7px">
                        <label>*Correo de Acceso</label>
                    </div>
                    <div class="col-md-10">
                        <asp:TextBox ID="Txt_Email" runat="server" CssClass="form-control" Enabled="false" AutoCompleteType="Email"
                            TabIndex="12"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <label>&nbsp;</label>
                </div>
                <div class="col-lg-10">
                    <asp:LinkButton ID="Btn_Modificar" runat="server" Text="Modificar <span class='glyphicon glyphicon-ok'></span>"
                        ToolTip="Modificar" CssClass="btn btn-success" OnClick="Btn_Modificar_Click"/>
                    <a id="Btn_Cambiar" runat="server" class="btn btn-success" onclick="mostrarVentana();">Cambiar Contraseña <span class='glyphicon glyphicon-pencil'></span></a>
                    <asp:LinkButton ID="Btn_Cancelar" runat="server" Text="Cancelar <span class='glyphicon glyphicon-remove'></span>"
                        ToolTip="Cancelar" CssClass="btn btn-success" Visible="false" OnClick="Btn_Cancelar_Click" />
                </div>


            </div>



            <div id="Div_Password" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" onclick="cancelarCambio();">&times;</button>
                            <h4 class="modal-title">Cambiar Contrase&ntilde;a</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label">Nueva Contrase&ntilde;a</label>
                                <div class="">
                                    <asp:TextBox ID="Txt_Contrasena_Nueva" runat="server" CssClass="form-control" TextMode="Password" MaxLength="20">
                                    </asp:TextBox>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label">Confirmar Contrase&ntilde;a</label>
                                <div class="">
                                    <asp:TextBox ID="Txt_Confirmar_Contrasena" runat="server" CssClass="form-control glyphicon-floppy-save"
                                        TextMode="Password"
                                        MaxLength="20"></asp:TextBox>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <a id="Lnk_Aceptar" class="btn btn-success" onclick="Modificar_Contrasena();">Modificar <span class='glyphicon glyphicon-ok'></span></a>
                            <a id="Lnk_Cancelar_Cambio" class="btn btn-success" onclick="cancelarCambio();">Cancelar <span class='glyphicon glyphicon-remove'></span></a>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

