﻿<%@ Page Language="C#" MasterPageFile="~/Paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true"
    CodeFile="Frm_Facturacion_Electronica.aspx.cs" Inherits="Paginas_Paginas_Generales_Frm_Facturacion_Electronica"
    EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="../../JavaScript/jquery-easyiu-1.3.1/themes/default/easyui.css" rel="stylesheet" />
    <link href="../../Estilos/css/bootstrap.css" rel="stylesheet" />
    <link href="../../estilos/estilo_paginas.css" rel="stylesheet" />
    <link href="../../estilos/Frm_Ope_Facturacion.css" rel="stylesheet" />
    <link href="../../JavaScript/src/bootstrap-table.css" rel="stylesheet" />

    <script src="../../JavaScript/jquery/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../../JavaScript/jquery-easyiu-1.3.1/jquery.easyui.min.js" type="text/javascript"></script>
    <style type="text/css">
        body {
            font-family: Arial;
            font-size: 10pt;
        }

        td {
            cursor: pointer;
        }

        .hover_row {
            background-color: #A1DCF2;
        }
    </style>
    
    <style type="text/css"> .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
    position: relative;
    min-height: 1px;
    padding-right: 1px !important;
    padding-left: 0.5px !important;
}
    </style>
    <title>Facturacion Electronica</title>
    <script type="text/javascript">
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantener_Session.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para mantener la sesión activa
        setInterval('MantenSesion()', '<%=(int)(0.9*(Session.Timeout * 60000))%>');


        function Abrir_Ventana_Modal(Url, Propiedades) {
            window.showModalDialog(Url, null, Propiedades);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Contenido" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="Upd_Folio" runat="server">
        <ContentTemplate>
            <div class="jumbotron">
                <div class="container" style="text-align: left;">
                    <asp:Label ID="Lbl_Titulo" runat="server"><h2>Facturaci&oacute;n Electrónica</h2></asp:Label>
                </div>
            </div>

            <div id="Div_Facturacion_Folio">
                <div class="container">
                    <div class="form-group col-md-12">
                        <div class="table-responsive">
                            <table id="table-pagination" class="table-condensed">
                            </table>
                        </div>
                    </div>
                </div>

                <asp:HiddenField ID="Txt_Folio_Recibo" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="Div_Factura_Datos" class="container" style="display: none; text-align: left;">
        <div class="container">
            <div class="form-group">
                <asp:Label ID="Lbl_Mensaje" runat="server" CssClass="alert alert-danger center-block" Visible="false"></asp:Label>
                <asp:Label ID="Lbl_Mensaje_Correcto" runat="server" CssClass="alert alert-success center-block" Visible="false"></asp:Label>

            </div>
            <div style="display: none">
                <input type="text" runat="server" group="Txt_Datos_Factura" id="Txt_Cliente_id" />

            </div>
                <div class="row">
                   <!-- <label class="col-md-3 control-label">*Nombre/Razón Social</label>-->
                   <div  class ="col-md-2" style="top:7px">
                        <label>*Raz&oacute;n Social/Nombre</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" runat="server" group="Txt_Datos_Factura Requerido" id="Txt_Razon" tabindex="1" class="form-control" />
                    </div>

                </div>
                <div class="row">
                    <div  class ="col-md-2" style="top:7px">
                        <label>*Calle</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" runat="server" group="Txt_Datos_Factura Requerido" id="Txt_Calle" tabindex="2" class="form-control" />

                    </div>
                </div>
                <div class="row">
                    <div  class ="col-md-2" style="top:7px">
                        <label>*N&uacute;mero Ext.</label>
                    </div>
                    <div class="col-md-5">
                        <input type="text" runat="server" group="Txt_Datos_Factura Requerido" id="Txt_Numero_Exterior" tabindex="3"
                            class="form-control" />
                    </div>
                    <div  class ="col-md-1" style="top:7px">
                        <label>*N&uacute;mero Int.</label>
                    </div>
                    <div class="col-md-4">
                        <input type="text" runat="server" group="Txt_Datos_Factura" id="Txt_Numero_Interior" tabindex="4" class="form-control" />
                    </div>
                </div>
                <div class="row">
                   <div  class ="col-md-2" style="top:7px">
                        <label>*Colonia</label>
                    </div>
                    <div class="col-md-5">
                        <input type="text" runat="server" group="Txt_Datos_Factura Requerido" id="Txt_Colonia" tabindex="5" class="form-control" />
                    </div>
                    <div  class ="col-md-1" style="top:7px">
                        <label>CP</label>
                    </div>
                    <div class="col-md-4">
                        <input type="text" runat="server" group="Txt_Datos_Factura" id="Txt_Codigo_Postal" tabindex="6" class="form-control" />
                    </div>
                </div>

                <div class="row">
                    <div  class ="col-md-2" style="top:7px">
                        <label>*Estado</label>
                    </div>
                    <div class="col-md-10">
                        <select id="Cmb_Estados" class="form-control" group="Txt_Datos_Factura" runat="server" tabindex="7">
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div  class ="col-md-2" style="top:7px">
                        <label>*Ciudad</label>
                    </div>
                    <div class="col-md-10">
                        <input id="Txt_Ciudad" type="text" group="Txt_Datos_Factura Requerido" tabindex="8" class="form-control" />
                    </div>
                </div>
                <div class="row">
                    <div  class ="col-md-2" style="top:7px">
                        <label>*Localdad/Delegaci&oacute;n</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" runat="server" group="Txt_Datos_Factura" id="Txt_Localidad" tabindex="9" class="form-control" />

                    </div>
                </div>
                <div class="row">
                    <div  class ="col-md-2" style="top:7px">
                        <label>*Pa&iacute;s</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" runat="server" group="Txt_Datos_Factura Requerido" id="Txt_Pais" tabindex="10" class="form-control" />
                    </div>
                </div>



                <div class="row">
                   <div  class ="col-md-2" style="top:7px">
                        <label>*RFC</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" runat="server" group="Txt_Datos_Factura Requerido" id="Txt_RFC" tabindex="11" class="form-control" />
                    </div>
                </div>
                <div class="row">
                    <div  class ="col-md-2" style="top:7px">
                        <label>*Email</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" runat="server" group="Txt_Datos_Factura Requerido" id="Txt_Email" tabindex="12" class="form-control" />
                    </div>
                </div>

            <div>
                &nbsp;
            </div>
            <div>
                <%--<a href="#" onclick="Modificar_Datos();" id="Lnk_Modificar" class="btn btn-success">
                    <asp:Label ID="Lbl_Modificar" runat="server" Text=" Modificar"></asp:Label>
                </a>--%>
                <%--<asp:LinkButton id ="Lnk_Modificar" runat ="server" CssClass="btn btn-success" Text="Modificar" OnClientClick="Modificar_Datos();"></asp:LinkButton>--%>
                <a href="#" onclick="Continuar_Detalles();" id="Lnk_Continuar_Detalles" class="btn btn-success">
                    <asp:Label ID="Lbl_Continuar_Detalles" runat="server" Text="Continuar"></asp:Label>
                </a>
                <a onclick="Cancelar_Modificacion();">
                    <asp:Label ID="Btn_Cancelar" runat="server" Text="Cancelar" CssClass="btn btn-success"></asp:Label>
                </a>
            </div>
        </div>
    </div>

    <div id="Div_Factura_Detalles" style="display: none; text-align: left;">
        <div class="container">
            <table style="width: 90%; margin-top: 5%; margin-left: 5%;">
                <tr>
                    <td style="width: 50%; text-align: right; font-size: 14px; font-family: Arial; font-weight: bold">&nbsp;Fecha
                        de pago
                    </td>
                    <td style="width: 50%; font-size: 14px; font-family: Arial; font-weight: bold; text-align: right;">
                        <asp:TextBox ID="Txt_Fecha_Pago" runat="server" CssClass="Txt_Detalle_Factura" Style="background-color: #ACDCF4;
                            width: 98%"
                            Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="display: none">
                        <input type="text" runat="server" group="Txt_Datos_Factura" id="Txt_No_Factura" />
                        <input type="text" runat="server" group="Txt_Datos_Factura" id="Txt_Subtotal" />
                        <input type="text" runat="server" group="Txt_Datos_Factura" id="Txt_Subtotal_Iva" />
                        <input type="text" runat="server" group="Txt_Datos_Factura" id="Txt_Total" />
                        <input type="text" runat="server" group="Txt_Datos_Factura" id="Txt_IvA_General" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" id="Tbl_Muestra_Facturado"></td>
                </tr>
                <tr style="Display: none">
                    <td><div class="table-responsive">
                        <table id="Tbl_Facturado" class="table-condensed">
                        </table></div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%">
                        <a href="#" class="btn btn-success" plain="true" onclick="Cancelar_Detalles()">Cancelar</a>
                    </td>
                    <td style="width: 50%">
                        <a href="#" class="btn btn-success" plain="true" onclick="Facturar()" id='Lnk_Facturar'>
                            <asp:Label runat="server" ID="Lbl_Facturar" Text="Facturar" />
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="ventana_mensaje" class="easyui-window" collapsible="false" minimizable="false"
        maximizable="false" closable="false" closed="true" modal="true" title="Facturando..."
        style="width: 120px; height: 120px;">
        <img src="../../imagenes/paginas/ajax-loader.gif" alt="." />
    </div>

    <script src="../../JavaScript/js/bootstrap.js"></script>
    <script src="../../JavaScript/src/bootstrap-table.js"></script>
    <script src="../../JavaScript/src/locale/bootstrap-table-es-MX.js"></script>
    <script src="../../JavaScript/Facturacion/Frm_Ope_Facturacion_Electronica.js" type="text/javascript"></script>
</asp:Content>

