﻿using Erp.Sesiones;
using SIAC.Cat_Clientes.Negocio;
using SIAC.Cat_Predios.Negocio;
using SIAC.Ciudades.Negocios;
using SIAC.Constantes;
using SIAC.Facturacion.Negocio;
using SIAC.Seguridad;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIAC.Catalogo_Comercializacion_Usuarios.Negocio;

public partial class Paginas_Paginas_Generales_Frm_Informacion_Fiscal : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Consultar_Estados();

            Llenar_Datos(Consultar_Datos_Fiscales());
            //Consultar_Ciudades();
        }
        if (!Convert.ToBoolean(Cls_Sessiones.Mostrar_Menu))
            Response.Redirect("../Login/Frm_Login.aspx");
    }

    #region Metodos

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Consultar_Datos_Fiscales
    ///DESCRIPCIÓN: Consulta los datos fiscales del cliente.
    ///PARAMETROS:  
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  14/Diciembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private DataTable Consultar_Datos_Fiscales()
    {
        Cls_Cat_Cor_Predios_Negocio neg_predio = new Cls_Cat_Cor_Predios_Negocio();

        neg_predio.P_Predio_ID = Cls_Sessiones.Predio_ID;
        DataTable Dt_Datos =  neg_predio.Consulta_Usuarios();
        
        //Cls_Cat_Clientes_Negocio Clientes = new Cls_Cat_Clientes_Negocio();
        //DataTable Dt = new DataTable();
        //Clientes.P_Cliente_Id = Cls_Sessiones.Cliente_ID;
        //if (!String.IsNullOrEmpty(Clientes.P_Cliente_Id))
        //    Dt = Clientes.Consultar_Clientes();

        return Dt_Datos;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Llenar_Datos
    ///DESCRIPCIÓN: Carga los datos del cliente en el formulario
    ///PARAMETROS:  Dt_Cliente
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  14/Diciembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Llenar_Datos(DataTable Dt_Cliente)
    {
        if (Dt_Cliente.Rows.Count > 0)
        {
            String estado = Dt_Cliente.Rows[0][Cat_Adm_Clientes.Campo_Estado].ToString();
            String ciudad =  Dt_Cliente.Rows[0]["ciudad_id"].ToString().Trim();
            Txt_Nombre.Text = Dt_Cliente.Rows[0][Cat_Adm_Clientes.Campo_Razon_Social].ToString();
            Txt_Calle.Text = Dt_Cliente.Rows[0][Cat_Adm_Clientes.Campo_Calle].ToString();
            Txt_Numero_Exterior.Text = Dt_Cliente.Rows[0][Cat_Adm_Clientes.Campo_Numero_Exterior].ToString();
            Txt_Numero_Interior.Text = Dt_Cliente.Rows[0][Cat_Adm_Clientes.Campo_Numero_Interior].ToString();
            Txt_Colonia.Text = Dt_Cliente.Rows[0][Cat_Adm_Clientes.Campo_Colonia].ToString();
            Txt_CP.Text = Dt_Cliente.Rows[0][Cat_Adm_Clientes.Campo_CP].ToString();
            Txt_Rfc.Text = Dt_Cliente.Rows[0][Cat_Adm_Clientes.Campo_RFC].ToString();
            Cmb_Estado.SelectedValue = String.IsNullOrEmpty(estado) ? "0" : estado;
            Consultar_Ciudades();
            Cmb_Ciudad.SelectedValue = String.IsNullOrEmpty(ciudad) ? "0" : ciudad;
            Txt_Pais.Text = Dt_Cliente.Rows[0][Cat_Adm_Clientes.Campo_Pais].ToString();
            //Txt_Curp.Text = Dt.Rows[0][Cat_Adm_Clientes.Campo_CURP].ToString();
            Txt_Email_Facturacion.Text = Dt_Cliente.Rows[0][Cat_Adm_Clientes.Campo_Email].ToString();
            Txt_Email.Text = Cls_Sessiones.Correo_Electronico;

        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Consultar_Estados
    ///DESCRIPCIÓN: Consulta y carga los estados en el control
    ///PARAMETROS:  
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  14/Diciembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Consultar_Estados()
    {
        Cls_Ope_Facturacion_Negocio Negocio = new Cls_Ope_Facturacion_Negocio();

        DataTable Dt_Datos;

        try
        {
            Cmb_Estado.DataValueField = Cat_Cor_Estados.Campo_Estado_ID;
            Cmb_Estado.DataTextField = Cat_Cor_Estados.Campo_Nombre;
            Dt_Datos = Negocio.Consultar_Estados();

            Cmb_Estado.DataSource = Dt_Datos;
            Cmb_Estado.DataBind();
            Cmb_Estado.Items.Insert(0, new ListItem("SELECCIONE", "0"));
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message.ToString(), true);
        }

    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Consultar_Ciudades
    ///DESCRIPCIÓN: Consulta y carga las ciudades en el control.
    ///PARAMETROS:  
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  04/Febrero/2016
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Consultar_Ciudades()
    {
        Cls_Cat_Cor_Ciudades_Negocio Negocio = new Cls_Cat_Cor_Ciudades_Negocio();

        DataTable Dt_Datos = new DataTable();
        try
        {
            Cmb_Ciudad.DataValueField = Cat_Cor_Ciudades.Campo_Ciudad_ID;
            Cmb_Ciudad.DataTextField = Cat_Cor_Ciudades.Campo_Nombre;
            if (Cmb_Estado.SelectedIndex != 0)
            {
                Negocio.P_Estado_ID = Cmb_Estado.SelectedValue;
                Dt_Datos = Negocio.Buscar_Ciudad();
            }



            Cmb_Ciudad.DataSource = Dt_Datos;
            Cmb_Ciudad.DataBind();
            Cmb_Ciudad.Items.Insert(0, new ListItem("SELECCIONE", "0"));
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message.ToString(), true);
        }

    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Modificar_Datos
    ///DESCRIPCIÓN: Modifica los datos fiscales del cliente.
    ///PARAMETROS:  
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  15/Diciembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Modificar_Datos()
    {
        Cls_Cat_Clientes_Negocio Clientes = new Cls_Cat_Clientes_Negocio();
        Cls_Cat_Cor_Usuarios_Negocio Usuarios = new Cls_Cat_Cor_Usuarios_Negocio();
        DataTable Dt_Usuarios = new DataTable();
        Dt_Usuarios = Consultar_Datos_Fiscales();
        Clientes.P_Cliente_Id = Cls_Sessiones.Cliente_ID;
        if (!String.IsNullOrEmpty(Clientes.P_Cliente_Id))
        {
            //Clientes.P_Razon_Social = Txt_Nombre.Text.ToUpper();
            //Clientes.P_Nombre_Corto = Txt_Nombre.Text.ToUpper();
            //Clientes.P_Calle = Txt_Calle.Text.ToUpper();
            //Clientes.P_Numero_Exterior = Txt_Numero_Exterior.Text.ToUpper();
            //Clientes.P_Numero_Interior = Txt_Numero_Interior.Text.ToUpper();
            //Clientes.P_Colonia = Txt_Colonia.Text.ToUpper();
            //Clientes.P_Cp = Txt_CP.Text.ToUpper();
            //Clientes.P_Rfc = Txt_Rfc.Text.ToUpper();
            //Clientes.P_Ciudad = Cmb_Ciudad.Text.ToUpper();
            //Clientes.P_Estado = Cmb_Estado.SelectedValue;
            //Clientes.P_Pais = Txt_Pais.Text.ToUpper();
            //Clientes.P_Curp = Txt_Curp.Text.ToUpper();
            Clientes.P_Email = Txt_Email.Text;
            //Clientes.P_Localidad = Cmb_Ciudad.SelectedItem.ToString();
            Clientes.Modificar_Cliente();
        }
        Usuarios.P_Usuario_Id = Dt_Usuarios.Rows[0]["Usuario_ID"].ToString().Trim();
        Usuarios.P_Correo_Electronico = Txt_Email_Facturacion.Text;
        Usuarios.Modifica_Usuarios();
        
        
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Validar_Contrasena
    ///DESCRIPCIÓN: Valida que la contraseña cumple con los parametros.
    ///PARAMETROS:  
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  22/Diciembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private Boolean Validar_Contrasena()
    {
        bool Validacion = true;
        if (Txt_Contrasena_Nueva.Text.Trim().Length < 8)
        {
            Validacion = false;
            Mensaje_Error("Longitud minima de 8 caracteres", true);
        }
        else
        {
            if ((String.IsNullOrEmpty(Txt_Confirmar_Contrasena.Text) && String.IsNullOrEmpty(Txt_Contrasena_Nueva.Text)))
            {
                Validacion = false;
                Mensaje_Error("Falta escribir las contraseñas", true);
            }
            if (!String.IsNullOrEmpty(Txt_Contrasena_Nueva.Text))
            {
                if (!String.IsNullOrEmpty(Txt_Confirmar_Contrasena.Text))
                {
                    if (Txt_Contrasena_Nueva.Text.Trim() != Txt_Confirmar_Contrasena.Text.Trim())
                    {
                        Validacion = false;
                        Mensaje_Error("Las contraseñas no coinciden.", true);
                    }
                }
                else
                {
                    Mensaje_Error("Falta confirmar contraseña.", true);
                    Validacion = false;
                }
            }
        }


        return Validacion;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Validar_Componentes
    ///DESCRIPCIÓN: Valida que los componentes esten correctos .
    ///PARAMETROS:  
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  14/Diciembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private Boolean Validar_Componentes()
    {
        bool Validacion = true;
        string Mensaje = "Falta los siguientes datos: ";
        //if (String.IsNullOrEmpty(Txt_Nombre.Text))
        //{
        //    Mensaje += !Validacion ? ", " : "";
        //    Mensaje += "Nombre";

        //    Validacion = false;
        //}
        //if (String.IsNullOrEmpty(Txt_Calle.Text))
        //{
        //    Mensaje += !Validacion ? ", " : "";
        //    Mensaje += "Calle";

        //    Validacion = false;
        //}
        //if (String.IsNullOrEmpty(Txt_Numero_Exterior.Text))
        //{
        //    Mensaje += !Validacion ? ", " : "";
        //    Mensaje += "Numero Ext";

        //    Validacion = false;
        //}
        //if (String.IsNullOrEmpty(Txt_Colonia.Text))
        //{
        //    Mensaje += !Validacion ? ", " : "";
        //    Mensaje += "Colonia";

        //    Validacion = false;
        //}
        //if (String.IsNullOrEmpty(Txt_Rfc.Text))
        //{
        //    Mensaje += !Validacion ? ", " : "";
        //    Mensaje += "RFC";

        //    Validacion = false;
        //}
        //if (String.IsNullOrEmpty(Txt_CP.Text))
        //{
        //    Mensaje += !Validacion ? ", " : "";
        //    Mensaje += "CP";
        //    Validacion = false;
        //}

        //if (Cmb_Ciudad.SelectedIndex == 0)
        //{
        //    Mensaje += !Validacion ? ", " : "";
        //    Mensaje += "Ciudad";
        //    Validacion = false;
        //}
        //if (Cmb_Estado.SelectedIndex == 0)
        //{
        //    Mensaje += !Validacion ? ", " : "";
        //    Mensaje += "Estado";
        //    Validacion = false;
        //}
        //if (String.IsNullOrEmpty(Txt_Pais.Text))
        //{
        //    Mensaje += !Validacion ? ", " : "";
        //    Mensaje += "País";
        //    Validacion = false;
        //}
        if (String.IsNullOrEmpty(Txt_Email.Text))
        {
            Mensaje += !Validacion ? ", " : "";
            Mensaje += "Email";

            Validacion = false;
        }
        else
        {
            String expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (!Regex.IsMatch(Txt_Email.Text.Trim(), expresion))
            {
                //Mensaje += !Validacion ? ", " : "";
                Mensaje += "<br>Datos Incorrectos: ";
                Mensaje += "Email.";

                Validacion = false;
            }
            else
            {
                Cls_Cat_Clientes_Negocio Cliente = new Cls_Cat_Clientes_Negocio();
                Cliente.P_Cliente_Id = Cls_Sessiones.Cliente_ID;
                Cliente.P_Email = Txt_Email.Text.Trim();
                DataTable Dt = Cliente.Verificar_Email();
                if (Dt.Rows.Count > 0)
                {
                    Mensaje += "<br>Email utilizado. Favor de utilizar otro correo electronico.";
                    Validacion = false;
                }
            }
        }
        if (!Validacion)
        {
            Mensaje_Error(Mensaje, true);
        }

        return Validacion;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Configurar_Formulario
    ///DESCRIPCIÓN: Habilita o deshabilita los campos.
    ///PARAMETROS:  Opcion
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  28/Diciembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Configurar_Formulario(String Opcion)
    {
        switch (Opcion)
        {
            case "Inicial":
                Txt_Nombre.Enabled = false;
                Txt_Calle.Enabled = false;
                Cmb_Ciudad.Enabled = false;
                Txt_Colonia.Enabled = false;
                Txt_CP.Enabled = false;
                //Txt_Contrasena_Nueva.Enabled = false;
                //Txt_Curp.Enabled = false;
                Txt_Email.Enabled = false;
                Cmb_Estado.Enabled = false;
                //Txt_Confirmar_Contrasena.Enabled = false;
                Txt_Numero_Exterior.Enabled = false;
                Txt_Numero_Interior.Enabled = false;
                Txt_Pais.Enabled = false;
                Txt_Rfc.Enabled = false;
                Btn_Modificar.Text = "Modificar <span class='glyphicon glyphicon-ok'></span>";
                Btn_Modificar.ToolTip = "Modificar";
                Btn_Cancelar.Visible = false;
                Btn_Cambiar.Visible = true;
                break;

            case "Modificar":
                //Txt_Nombre.Enabled = true;
                //Txt_Calle.Enabled = true;
                //Cmb_Ciudad.Enabled = true;
                //Txt_Colonia.Enabled = true;
                //Txt_CP.Enabled = true;
                //Txt_Contrasena_Nueva.Enabled = true;
                //Txt_Curp.Enabled = true;
                Txt_Email_Facturacion.Enabled = true;
                //Txt_Email_Facturacion.Enabled = true;
                //Cmb_Estado.Enabled = true;
                ////Txt_Confirmar_Contrasena.Enabled = true;
                //Txt_Numero_Exterior.Enabled = true;
                //Txt_Numero_Interior.Enabled = true;
                //Txt_Pais.Enabled = true;
                //Txt_Rfc.Enabled = true;
                Btn_Modificar.Text = "Actualizar  <span class='glyphicon glyphicon-floppy-saved'></span>";
                Btn_Modificar.ToolTip = "Actualizar";
                Btn_Cancelar.Visible = true;
                Btn_Cambiar.Visible = false;
                break;
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Mensaje_Error
    ///DESCRIPCIÓN: Muestra los mensajes de error en pantalla 
    ///PARAMETROS:  Mensaje, Visible
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  28/Diciembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Mensaje_Error(String Mensaje, Boolean Visible)
    {
        Lbl_Mensaje.Text = Mensaje;
        Lbl_Mensaje.Visible = Visible;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Mensaje_Correcto
    ///DESCRIPCIÓN: Muestra el mensaje correcto.
    ///PARAMETROS:  Mensaje, Visible
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  28/Diciembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Mensaje_Correcto(String Mensaje, bool Visible)
    {
        Lbl_Mensaje_Correcto.Text = Mensaje;
        Lbl_Mensaje_Correcto.Visible = Visible;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Modificar_Contrasenia
    ///DESCRIPCIÓN: Modifica la contraseña.
    ///PARAMETROS:  
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  14/Diciembre/2015
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private bool Modificar_Contrasenia()
    {
        Cls_Cat_Clientes_Negocio Clientes = new Cls_Cat_Clientes_Negocio();
        bool Cambio = false;
        Clientes.P_Cliente_Id = Cls_Sessiones.Cliente_ID;
        if (!String.IsNullOrEmpty(Clientes.P_Cliente_Id))
        {
            Clientes.P_Contrasena = Cls_Seguridad.Encriptar(Txt_Contrasena_Nueva.Text.Trim());
            Clientes.Modificar_Cliente();
            Cambio = true;
        }
        return Cambio;
    }

    #endregion

    #region Eventos

    protected void Btn_Modificar_Click(object sender, EventArgs e)
    {
        Mensaje_Error("", false);
        Mensaje_Correcto("", false);
        try
        {
            if (Btn_Modificar.ToolTip == "Modificar")
            {
                Configurar_Formulario("Modificar");
            }
            else
            {
                if (Validar_Componentes())
                {
                    Modificar_Datos();
                    Configurar_Formulario("Inicial");
                    Mensaje_Correcto("Datos Modificados", true);
                    Cls_Sessiones.Correo_Electronico = Txt_Email.Text.Trim(); 

                }

            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message, true);
        }
    }

    protected void Btn_Cancelar_Click(object sender, EventArgs e)
    {
        Mensaje_Error("", false);
        Mensaje_Correcto("", false);
        try
        {
            //Llenar_Datos(Consultar_Datos_Fiscales());
            Configurar_Formulario("Inicial");
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message, true);
        }
    }

    protected void Lnk_Aceptar_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validar_Contrasena())
            {
                if (Modificar_Contrasenia())
                {
                    Mensaje_Correcto("Contraseña Modificada", true);
                }
                else
                    Mensaje_Error("Error al cambiar la contraseña", true);
            }
            else
            {
                Txt_Contrasena_Nueva.Attributes.Add("value", Txt_Contrasena_Nueva.Text);
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message, true);
        }
    }

    protected void Cmb_Estado_SelectedIndexChanged(object sender, EventArgs e)
    {
        Consultar_Ciudades();
    }

    #endregion
}