﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Frm_Facturacion.aspx.cs" Inherits="Paginas_Paginas_Generales_Frm_Facturacion"
    EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="../../estilos/estilo_paginas.css" rel="stylesheet" />
    <link href="../../JavaScript/jquery-easyiu-1.3.1/themes/default/easyui.css" rel="stylesheet" />
    <link href="../../Estilos/css/bootstrap.css" rel="stylesheet" />
    <link href="../../estilos/Frm_Ope_Facturacion.css" rel="stylesheet" />
    <script src="../../JavaScript/jquery/jquery-1.7.1.js" type="text/javascript"></script>
    <script src="../../JavaScript/jquery/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../../JavaScript/js/bootstrap.js" type="text/javascript"></script>
    <script src="../../JavaScript/Facturacion/Frm_Fac.js" type="text/javascript"></script>
    <script src="../../JavaScript/jquery-easyiu-1.3.1/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../../JavaScript/jquery/json_parse.js" type="text/javascript"></script>

    <style type="text/css"> .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
    position: relative;
    min-height: 1px;
    padding-right: 1px !important;
    padding-left: 0.5px !important;
}
    </style>

    <title>Facturacion</title>
</head>
<body>
    <form id="form1" runat="server">

        <div class="navbar navbar-default">

            <div class="container">
                <div>&nbsp;</div>
                <table>
                    <tr>
                        <td>
                            <img src="../../imagenes/Logos/img-logo.png" />
                        </td>
                        <td>
                            <asp:Label ID="Lbl_Titulo" runat="server" Style="color: #FFF;"><h2>&nbsp;&nbsp;Facturación Electrónica</h2></asp:Label>
                        </td>
                    </tr>
                </table>
                <div>&nbsp;</div>
            </div>
        </div>
        <div id="Div_Facturacion_Folio" style="text-align: left;">
            <div class="container">

                <div class="row">

                    <div class="col-md-4 col-md-offset-4">

                        <div class="panel panel-default">
                            <div class="panel-body">

                                <div class="form-group">
                                    <label id="Lbl_Codigo" class="control-label">C&oacute;digo de Facturaci&oacute;n</label>
                                    <asp:TextBox ID="Txt_Folio_Recibo" runat="server" CssClass="form-control" Requerido="true"></asp:TextBox>

                                </div>

                                <a onclick="Consultar_Folio()">
                                    <asp:Label ID="Btn_Aceptar" runat="server" Text="Aceptar" CssClass="btn btn-success" Width="49%"></asp:Label>
                                </a>

                                <asp:Button ID="Btn_Regresar" runat="server" class="btn btn-success" Style="width: 49%" Text="Cancelar"
                                    OnClick="Btn_Regresar_Click" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div id="Div_Factura_Datos" class="container" style="text-align: left">
            <div class="container">
                <div class="form-group">
                    <asp:Label ID="Lbl_Mensaje" runat="server" CssClass="alert alert-danger center-block" Visible="false"></asp:Label>
                    <asp:Label ID="Lbl_Mensaje_Correcto" runat="server" CssClass="alert alert-success center-block" Visible="false"></asp:Label>

                </div>
                <div style="display: none">
                    <asp:TextBox runat="server" group="Txt_Datos_Factura" ID="Txt_Cliente_ID" />

                </div>
                
                   <div class="row">
                        <label class="col-md-3 control-label">*Nombre/Razón Social</label>
                        <div class="col-md-9">
                            <span  group="Txt_Datos_Factura Requerido" id="Txt_Razon" tabindex="1" class="form-control text-uppercase"></span>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-3 control-label">*Calle</label>
                        <div class="col-md-9">
                            <span  group="Txt_Datos_Factura Requerido" id="Txt_Calle" tabindex="2" class="form-control text-uppercase" />

                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-3 control-label">*Número Exterior</label>
                        <div class="col-md-4">
                            <span  group="Txt_Datos_Factura Requerido" id="Txt_Numero_Exterior" tabindex="3" class="form-control text-uppercase" />
                        </div>
                        <label class="col-md-2 control-label">Número Interior</label>
                        <div class="col-md-3">
                            <span group="Txt_Datos_Factura" id="Txt_Numero_Interior" tabindex="4" class="form-control text-uppercase" />
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-3 control-label">*Colonia</label>
                        <div class="col-md-4">
                            <span runat="server" group="Txt_Datos_Factura Requerido" id="Txt_Colonia" tabindex="5" class="form-control text-uppercase" />
                        </div>

                        <label class="col-md-2 control-label">*CP</label>
                        <div class="col-md-3">
                            <span group="Txt_Datos_Factura Requerido" id="Txt_Codigo_Postal" tabindex="6" class="form-control text-uppercase" MaxLength="5"/>
                        </div>
                    </div>


                    <div class="row">
                        <label class="col-md-3 control-label">*Estado</label>
                        <div class="col-md-9">
                            <select id="Cmb_Estados" onchange="Consultar_Ciudades('','','')" class="form-control text-uppercase" tabindex="7" group="Txt_Datos_Factura Requerido"></select>
                           <%-- <input type="text" id="Cmb_Estados"  class="form-control text-uppercase" tabindex="7" group="Txt_Datos_Factura Requerido"/>--%>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-3 control-label">*Ciudad</label>
                        <div class="col-md-9">
                            <select id="Cmb_Ciudad" group="Txt_Datos_Factura Requerido" tabindex="8" onchange="Localidad();" class="form-control text-uppercase"></select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-3 control-label">Localidad/Delegación</label>
                        <div class="col-md-9">
                            <span runat="server" group="Txt_Datos_Factura" id="Txt_Localidad" tabindex="9" class="form-control text-uppercase" />

                        </div>
                    </div>

                    <div class="row">
                        <label class="col-md-3 control-label">*País</label>
                        <div class="col-md-9">
                            <span group="Txt_Datos_Factura Requerido" id="Txt_Pais" tabindex="10" class="form-control text-uppercase" />
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-3 control-label">*RFC</label>
                        <div class="col-md-9">
                            <span runat="server" group="Txt_Datos_Factura Requerido" id ="Txt_RFC" tabindex="11" class="form-control text-uppercase" />
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-md-3 control-label">*Email</label>
                        <div class="col-md-9">
                            <asp:TextBox runat="server" group="Txt_Datos_Factura Requerido" ID="Txt_Email" TabIndex="12" CssClass="form-control" />
                        </div>
                    </div>

                

                <div>
                    &nbsp;
                </div>
                <div>
                    <a href="#" onclick="Modificar_Datos();" id="Lnk_Modificar" class="btn btn-success">
                        <asp:Label ID="Lbl_Modificar" runat="server" Text="Modificar"></asp:Label>
                    </a>
                    <%--<asp:LinkButton ID ="Lnk_Modificar" runat ="server" CssClass="btn btn-success" Text="Modificar" OnClientClick="Modificar_Datos();"></asp:LinkButton>--%>
                    <a href="#" onclick="Continuar_Detalles();" id="Lnk_Continuar_Detalles" class="btn btn-success">
                        <asp:Label ID="Lbl_Continuar_Detalles" runat="server" Text="Continuar"></asp:Label>
                    </a>
                    <a onclick="Cancelar_Modificacion();">
                        <asp:Label ID="Btn_Cancelar" runat="server" Text="Cancelar" CssClass="btn btn-success"></asp:Label>
                    </a>
                </div>
            </div>
        </div>

        <div id="Div_Factura_Detalles" style="display: none; text-align: left">
            <div class="container">
                <table style="width: 90%; margin-top: 5%; margin-left: 5%;">
                    <tr>
                        <td style="width: 50%; text-align: right; font-size: 14px; font-family: Arial; font-weight: bold">&nbsp;Fecha pago
                        </td>
                        <td style="width: 50%; font-size: 14px; font-family: Arial; font-weight: bold; text-align: right;">
                            <asp:TextBox ID="Txt_Fecha_Pago" runat="server" class="Txt_Detalle_Factura" Width="98%" Style="background-color: #ACDCF4" Enabled="false" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="display: none">
                            <asp:TextBox runat="server" group="Txt_Datos_Factura" ID="Txt_No_Factura" />
                            <asp:TextBox runat="server" group="Txt_Datos_Factura" ID="Txt_Subtotal" />
                            <asp:TextBox runat="server" group="Txt_Datos_Factura" ID="Txt_Subtotal_Iva" />
                            <asp:TextBox runat="server" group="Txt_Datos_Factura" ID="Txt_Total" />
                            <asp:TextBox runat="server" group="Txt_Datos_Factura" ID="Txt_IvA_General" />
                             <input type="hidden" group="Txt_Datos_Factura" id ="Txt_Usuario_ID"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="Tbl_Muestra_Facturado"></td>
                    </tr>
                    <tr style="Display: none">
                        <td>
                            <table id="Tbl_Facturado">
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%">
                            <a href="#" class="btn btn-success" plain="true" onclick="Cancelar_Detalles()">Cancelar</a>
                        </td>
                        <td style="width: 50%">
                            <a href="#" class="btn btn-success" plain="true" onclick="Facturar()" id='Lnk_Facturar'>
                                <asp:Label runat="server" ID="Lbl_Facturar" Text="Facturar" />
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="ventana_mensaje" class="easyui-window" collapsible="false" minimizable="false"
            maximizable="false" closable="false" closed="true" modal="true" title="Facturando..."
            style="width: 110px; height: 110px;">
            <img src="../../imagenes/paginas/ajax-loader.gif" alt="." />
        </div>

    </form>
</body>
</html>
