﻿using Erp.Sesiones;
using Newtonsoft.Json;
using SIAC.Cat_Clientes.Negocio;
using SIAC.Seguridad;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_Paginas_Generales_Frm_Controlador_Password : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Controlador(this.Response, this.Request);
    }

    public void Controlador(HttpResponse response, HttpRequest request)
    {
        String Accion;
        String Json = "{[]}";
        Accion = HttpContext.Current.Request["opcion"];
        try
        {
            switch (Accion)
            {
                case "Actualizar_Password":
                    Json = Actualizar_Password();
                    break;

            }
        }
        catch (Exception Ex)
        {
            Json = Respuesta(Ex.Message);
        }
        response.Clear();
        response.ContentType = "application/json";
        response.Flush();
        response.Write(Json);
        response.End();
    }

    public String Respuesta(String Respuesta)
    {
        StringBuilder Buffer = new StringBuilder();
        StringWriter Escritor = new StringWriter(Buffer);
        JsonWriter Escribir_Formato_JSON = new JsonTextWriter(Escritor);
        String Cadena_Resultado = "[]";
        Escribir_Formato_JSON.Formatting = Newtonsoft.Json.Formatting.None;
        Escribir_Formato_JSON.WriteStartObject();
        Escribir_Formato_JSON.WritePropertyName("Mensaje");
        Escribir_Formato_JSON.WriteValue(Respuesta);
        Escribir_Formato_JSON.WriteEndObject();
        Cadena_Resultado = Buffer.ToString();
        return Cadena_Resultado;
    }

    private string Actualizar_Password()
    {
        Cls_Cat_Clientes_Negocio Clientes = new Cls_Cat_Clientes_Negocio();
        String pass = HttpContext.Current.Request["password"];
        bool Cambio = false;
        Clientes.P_Cliente_Id = Cls_Sessiones.Cliente_ID;
        if (!String.IsNullOrEmpty(Clientes.P_Cliente_Id))
        {
            Clientes.P_Contrasena = Cls_Seguridad.Encriptar(pass);
            Clientes.Modificar_Cliente();
            Cambio = true;
        }
        return Respuesta("ok");
    }

}