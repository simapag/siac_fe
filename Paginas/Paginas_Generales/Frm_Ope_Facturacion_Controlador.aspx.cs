﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using SIAC.Facturacion.Negocio;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Transactions;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.Xml;
using System.Xml.Linq;
using SIAC.Metodos_Generales;
using SIAC_Apl_Parametros_Facturacion.Negocio;
using SIAC.Ope_Facturas_Series.Negocio;
using SIAC_Ope_Facturas_Detalles.Negocio;
using SIAC.Timbrado;
using SIAC.Seguridad;
using SIAC.Constantes;
using SIAC.Envio_Email;

using System.Web.Security;
using System.Web.Services;
using SIAC.Mensaje_Servidor;
using SIAC.Ayudante_JQuery;
using System.Security.Cryptography;
using SIAC.Pagos_Internet.Datos;
using SIAC.Ciudades.Negocios;
using SIAC.Cat_Predios.Negocio;
using SIAC.Cat_Metodos_Pago.Negocio;
using SIAC.Facturacion.Reportes.Entidades;
using SIAC.Facturacion.Reportes;
using System.Data.SqlClient;

public partial class Paginas_Facturacion_Frm_Ope_Facturacion_Controlador : System.Web.UI.Page
{
    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN:   Page_Load()
    //DESCRIPCIÓN:      funcion que se carga al inicio de la pagina
    //PARÁMETROS:       
    //CREO:             Ramón Baeza Yépez
    //FECHA_CREO:       05/12/2013
    //*******************************************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Controlador(this.Response, this.Request);
    }
    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN:   Consultar_Folio()
    //DESCRIPCIÓN:      Consulta los folios facturados
    //PARÁMETROS:       Request; parametros del HttpContext
    //CREO:             Ramón Baeza Yépez
    //FECHA_CREO:       05/12/2013
    //*******************************************************************************************************
    public void Controlador(HttpResponse response, HttpRequest request)
    {
        String Accion;
        String Json = "{[]}";
        Accion = HttpContext.Current.Request["Accion"];
        try
        {
            switch (Accion)
            {
                case "Consultar_Datos_Factura":
                    Json = Consultar_Datos_Factura(Request);
                    break;
                case "Consultar_Estado_Cuenta":
                    Json = Consultar_Estado_Cuenta(request);
                    break;
                case "Consultar_Datos_Cliente":
                    Json = Consultar_Datos_Cliente(Request);
                    break;
                case "Modificar_Datos_Cliente":
                    Json = Modificar_Datos_Cliente(Request);
                    break;
                case "Consultar_Detalles_Pago":
                    Json = Consultar_Detalles_Pago(Request);
                    break;
                case "Consultar_Detalles_Pago_Facturado":
                    Json = Consultar_Detalles_Pago_Facturado(Request);
                    break;
                case "Consultar_Estados":
                    Json = Consultar_Estados(Request);
                    break;
                case "Imprimir_Factura":
                    Json = Imprimir_Factura(Request);
                    break;
                case "Alta_Datos_Cliente":
                    Json = Alta_Datos_Cliente(Request);
                    break;
                case "Consultar_Ciudades":
                    Json = Consultar_Ciudades(Request);
                    break;
                case "Consultar_Predio":
                    Json = Consultar_Predio(Request);
                    break;
                case "Modificar_Datos_Usuarios":
                    Json = Modificar_Datos_Usuarios(Request);
                    break;
                case "Buscar_Bancos":
                    Json = Buscar_Bancos();
                    break;
                case "Consultar_Facturas":
                    Json = Buscar_Facturas();
                    break;
                case "Consultar_Rezagos":
                    Json = Consultar_Rezagos(Request);
                    break;

            }
        }
        catch (Exception Ex)
        {
            Json = Respuesta(Ex.Message);
        }
        response.Clear();
        response.ContentType = "application/json";
        response.Flush();
        response.Write(Json);
        response.End();
    }

    private string Buscar_Facturas()
    {
        Cls_Ope_Facturacion_Negocio Negocio = new Cls_Ope_Facturacion_Negocio();
        String Cadena_Resultado = "{[]}";
        DataTable Dt_Datos;

        try
        {
            Negocio.P_Predio_ID = HttpContext.Current.Request["Predio"].ToString().Trim();
            Dt_Datos = Negocio.Consultar_Facturas_Clientes();
            Cadena_Resultado = SIAC.Ayudante_JQuery.Ayudante_JQuery.dataTableToJSONsintotalrenglones(Dt_Datos);
        }
        catch (Exception Ex)
        {
            String Mensaje_Error = Ex.Message.ToString();
        }
        return Cadena_Resultado;
    }

    private string Buscar_Bancos()
    {
        Cls_Ope_Facturacion_Negocio Negocio = new Cls_Ope_Facturacion_Negocio();
        String Cadena_Resultado = "{[]}";
        DataTable Dt_Datos;

        try
        {
            Dt_Datos = Negocio.Consultar_Bancos();
            Cadena_Resultado = SIAC.Ayudante_JQuery.Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt_Datos);
        }
        catch (Exception Ex)
        {
            String Mensaje_Error = Ex.Message.ToString();
        }
        return Cadena_Resultado;
    }


    private string Consultar_Rezagos(HttpRequest Request)
    {
        Cls_Ope_Facturacion_Negocio Negocio = new Cls_Ope_Facturacion_Negocio();
        String RPU = HttpContext.Current.Request["RPU"];
        String Cadena_Resultado = "{[]}";
        DataTable Dt_Datos;
        Cls_Mensajes_Servidor Mensaje = new Cls_Mensajes_Servidor();
        try
        {
            //Dt_Datos = Negocio.Consultar_Bancos();
            Negocio.P_Rpu = RPU.Trim();
            //Dt_Datos = Negocio.Consultar_Existe_Rezago();

            //if (Dt_Datos.Rows.Count > 0)
            //{
            //    Cadena_Resultado = Respuesta("success");
            //}
            //else
            //{
            //    Cadena_Resultado = Respuesta("sin_rezago");
            //}

            if (RPU.Length > 0 && Double.Parse(RPU) > 0)
            {
                Cadena_Resultado = Respuesta("success");
            }
            else
            {
                Cadena_Resultado = Respuesta("sin_rezago");
            }
        }
        catch (Exception Ex)
        {
            String Mensaje_Error = Ex.Message.ToString();
        }
        return Cadena_Resultado;
    }
    private string Modificar_Datos_Usuarios(HttpRequest Request)
    {
        Cls_Ope_Facturacion_Negocio Negocio = new Cls_Ope_Facturacion_Negocio();
        String Cadena_Resultado = "[]";
        String Datos = HttpContext.Current.Request["Datos"];
        Datos = Datos.Replace("ctl00_", "");
        Datos = Datos.Replace("Cph_Contenido_", "");
        Datos = Datos.Replace("Txt_", "");
        Datos = Datos.Replace("Cmb_", "");
        DataTable Dt_Cliente;

        try
        {
            Dt_Cliente = SIAC.Ayudante_JQuery.Ayudante_JQuery.Json_A_DataTable(Datos);
            if (Dt_Cliente.Rows.Count > 0)
            {
                Negocio.P_Razon_Social_Facturacion = Dt_Cliente.Rows[0]["Razon"].ToString().Trim();
                Negocio.P_Rfc_Facturacion = Dt_Cliente.Rows[0]["RFC"].ToString().Trim();
                Negocio.P_Email_Facturacion = Dt_Cliente.Rows[0]["Email"].ToString().Trim();
                Negocio.P_Calle_Facturacion = Dt_Cliente.Rows[0]["Calle"].ToString().Trim();
                Negocio.P_Colonia_Facturacion = Dt_Cliente.Rows[0]["Colonia"].ToString().Trim();
                Negocio.P_Cp_Facturacion = Dt_Cliente.Rows[0]["Codigo_Postal"].ToString().Trim();
                Negocio.P_Ciudad_Facturacion = Dt_Cliente.Rows[0]["Ciudad"].ToString().Trim();
                Negocio.P_Estado_Facturacion = Dt_Cliente.Rows[0]["Estado"].ToString().Trim();
                Negocio.P_Numero_Exterior_Facturacion = Dt_Cliente.Rows[0]["Numero_Exterior"].ToString().Trim();
                Negocio.P_Numero_Interior_Facturacion = Dt_Cliente.Rows[0]["Numero_Interior"].ToString().Trim();
                Negocio.P_Estado_Fiscal_Id = Dt_Cliente.Rows[0]["Estado_ID"].ToString().Trim();
                Negocio.P_Ciudad_Fiscal_ID = Dt_Cliente.Rows[0]["Ciudad_ID"].ToString().Trim();

                Negocio.P_Usuario_Id = Dt_Cliente.Rows[0]["Usuario_ID"].ToString().Trim();
                if (!String.IsNullOrEmpty(Negocio.P_Usuario_Id))
                    Negocio.Modificar_Datos_Usuarios();
                Cadena_Resultado = Respuesta("Modificacion");
                //Session["Dt_Cliente"] = Dt_Cliente;
            }
        }
        catch (Exception Ex)
        {
            Cadena_Resultado = Respuesta(Ex.Message);
        }
        return Cadena_Resultado;
    }


    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN:   Consultar_Datos_Factura()
    //DESCRIPCIÓN:      Consulta los datos para verificar si ya se realizo la facturacion
    //PARÁMETROS:       Request; parametros del HttpContext
    //CREO:             Ramón Baeza Yépez
    //FECHA_CREO:       05/12/2013
    //*******************************************************************************************************
    public string Consultar_Datos_Factura(HttpRequest Request)
    {
        Cls_Ope_Facturacion_Negocio Negocio = new Cls_Ope_Facturacion_Negocio();
        String No_Folio = HttpContext.Current.Request["No_Folio"];
        String Cadena_Resultado = "[]";
        DataTable Dt_Datos;
        try
        {
            Negocio.No_Folio = No_Folio;
            Dt_Datos = Negocio.Consultar_Datos_Factura_Global();

            if (Dt_Datos.Rows.Count <= 0)
            {
                Dt_Datos = Negocio.Consultar_Datos_Factura();
                if (Dt_Datos.Rows.Count > 0)
                    Session["No_Factura_Correo"] = Dt_Datos.Rows[0]["No_Factura"].ToString();
            }
            else if (Dt_Datos.Rows[0][0].ToString() == "NC" || Dt_Datos.Rows[0][0].ToString() == "CANCELADA")
            {
                Dt_Datos.Clear();
            }
            Cadena_Resultado = SIAC.Ayudante_JQuery.Ayudante_JQuery.dataTableToJSON(Dt_Datos);
        }
        catch (Exception Ex)
        {
            Cadena_Resultado = Respuesta(Ex.Message);
        }
        return Cadena_Resultado;
    }

    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN:   Consultar_Estado_Cuenta()
    //DESCRIPCIÓN:      Consulta el estado de cuenta de una cuenta
    //PARÁMETROS:       Request; parametros del HttpContext
    //CREO:             Fernando Gonzalez B.
    //FECHA_CREO:       05/12/2013
    //*******************************************************************************************************
    public string Consultar_Estado_Cuenta(HttpRequest Request)
    {
        Cls_Ope_Facturacion_Negocio Obj_Negocio = new Cls_Ope_Facturacion_Negocio();
        Ds_Impresion_Aviso_Pago Obj_Dataset = new Ds_Impresion_Aviso_Pago();
        String No_Cuenta = HttpContext.Current.Request["No_Cuenta"];
        String Cadena_Resultado = "[]";
        DataTable Dt_Datos;
        DataRow Rw_Renglos_Ds = null;
        DateTime fecha_limite;
        try
        {
            Obj_Negocio.No_Cuenta = No_Cuenta;
            //Dt_Datos = Obj_Negocio.Consultar_Datos_Estado_Cuenta();
            Dt_Datos = Consultar_Estado_Cuenta(No_Cuenta);
            foreach (DataRow Rw_Aux in Dt_Datos.Rows)
            {
                DateTime.TryParseExact(Rw_Aux["Fecha_Limite_Pago"].ToString(), "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out fecha_limite);
                Rw_Renglos_Ds = Obj_Dataset.Dt_Impresion_Recibos.NewRow();
                Rw_Renglos_Ds["No_Cuenta"] = Rw_Aux["No_Cuenta"];
                Rw_Renglos_Ds["Tarifa"] = Rw_Aux["Tarifa"];
                //Rw_Renglos_Ds["No_Padron"] = Rw_Aux["No_Padron"];
                Rw_Renglos_Ds["Usuario"] = Rw_Aux["Usuario"];
                Rw_Renglos_Ds["Colonia"] = Rw_Aux["Colonia"];
                Rw_Renglos_Ds["Direccion"] = Rw_Aux["Direccion"];
                Rw_Renglos_Ds["Direccion_Corta"] = Rw_Aux["Direccion_Corta"];
                if (!String.IsNullOrEmpty(Rw_Aux["Codigo_Barras"].ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    System.Drawing.Bitmap Imagen_cb = crearCodigo(Rw_Aux["Codigo_Barras"].ToString());
                    Imagen_cb.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

                    Rw_Renglos_Ds["Codigo_Barras"] = ms.ToArray();
                }
                Rw_Renglos_Ds["ultimo_pagado"] = Rw_Aux["ultimo_pagado"];
                Rw_Renglos_Ds["Referencia"] = Rw_Aux["Referencia"];
                Rw_Renglos_Ds["Fecha_Emision"] = Rw_Aux["Fecha_Emision"];
                Rw_Renglos_Ds["Periodo_Facturacion"] = Rw_Aux["Periodo_Facturacion"];
                Rw_Renglos_Ds["Ruta_Reparto"] = Rw_Aux["Ruta_Reparto"];
                Rw_Renglos_Ds["NO_MEDIDOR"] = Rw_Aux["MEDIDOR_ID"];
                Rw_Renglos_Ds["Lectora"] = Rw_Aux["Lectora"];
                Rw_Renglos_Ds["Lectura_Anterior"] = Rw_Aux["Lectura_Anterior"];
                Rw_Renglos_Ds["Lectura_Actual"] = Rw_Aux["Lectura_Actual"];
                Rw_Renglos_Ds["Consumo"] = Rw_Aux["Consumo"];
                Rw_Renglos_Ds["Fecha_Limite_Pago"] = fecha_limite;
                Rw_Renglos_Ds["Importe_Bimestre"] = Rw_Aux["Importe_Bimestre"];
                Rw_Renglos_Ds["Adeudo"] = Rw_Aux["Adeudo"];
                Rw_Renglos_Ds["Total_Pagar"] = Rw_Aux["Total_Pagar"];
                Rw_Renglos_Ds["Numero_Codigo_Barras"] = Rw_Aux["Numero_Codigo_Barras"];
                Rw_Renglos_Ds["RPU"] = Rw_Aux["RPU"];
                Rw_Renglos_Ds["Mes_Facturado"] = Rw_Aux["Mes_Facturado"];
                Rw_Renglos_Ds["Meses_Adeudos"] = Rw_Aux["Meses_Adeudo"];
                Rw_Renglos_Ds["Adeudo_Anterior"] = Rw_Aux["Adeudo_Anterior"];
                Rw_Renglos_Ds["Fecha_Suspension"] = Rw_Aux["Suspension"];
                Rw_Renglos_Ds["Salvo_A_Favor"] = Rw_Aux["Saldo_Favor"];

                Obj_Dataset.Dt_Impresion_Recibos.Rows.Add(Rw_Renglos_Ds);
            }
            DataTable Dt_Detalles = Obj_Negocio.Consultar_Detalles_Recibo(No_Cuenta);
            Dt_Detalles = AgruparDetalle(Dt_Detalles);
            DataRow Rw_Aux1;
            foreach (DataRow DR in Dt_Detalles.Rows)
            {
                Rw_Aux1 = Obj_Dataset.Dt_Detalles.NewRow();
                Rw_Aux1[0] = DR["concepto_id"];
                Rw_Aux1[1] = DR["concepto"];
                Rw_Aux1[2] = DR["total_saldo"];
                Obj_Dataset.Dt_Detalles.Rows.Add(Rw_Aux1);
            }
            //Cls_Metodos_Generales.Generar_Reporte(Obj_Dataset, "Rpt_Ope_Cor_Impresion_Recibos", this.Response);
            //Obj_Dataset.Dt_Detalles = Dt_Detalles;
            Cls_Metodos_Generales.Generar_Reporte(Obj_Dataset, "Rpt_Aviso_Pago", this.Response);
        }
        catch (Exception Ex)
        {
            Cadena_Resultado = Respuesta(Ex.Message);
        }
        return Cadena_Resultado;
    }

    private DataTable AgruparDetalle(DataTable Dt_Detalle)
    {


        for (int Indice_Pago = 0; Indice_Pago < Dt_Detalle.Rows.Count; Indice_Pago++)
        {
            for (int Indice_Pago_Siguiente = Indice_Pago + 1; Indice_Pago_Siguiente < Dt_Detalle.Rows.Count; Indice_Pago_Siguiente++)
            {
                if (Dt_Detalle.Rows[Indice_Pago]["concepto_id"].ToString().Trim().Equals(Dt_Detalle.Rows[Indice_Pago_Siguiente]["concepto_id"].ToString().Trim()))
                {
                    Dt_Detalle.Rows[Indice_Pago]["total_saldo"] = Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago]["total_saldo"]) + Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago_Siguiente]["total_saldo"]);
                    Dt_Detalle.Rows.RemoveAt(Indice_Pago_Siguiente);
                    Indice_Pago_Siguiente--;
                }
            }
        }
        return Dt_Detalle;
    }

    public Bitmap crearCodigo(string Code)
    {
        try
        {
            string text = code128(Code);
            Bitmap bitmap = new Bitmap(1, 1);
            Font font = new Font("Code 128", 50, FontStyle.Regular, GraphicsUnit.Pixel);
            Graphics graphics = Graphics.FromImage(bitmap);
            int width = (int)graphics.MeasureString(text, font).Width;
            int height = (int)graphics.MeasureString(text, font).Height;
            bitmap = new Bitmap(bitmap, new Size(width, height));
            graphics = Graphics.FromImage(bitmap);
            graphics.Clear(Color.White);
            graphics.SmoothingMode = SmoothingMode.AntiAlias;
            graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
            graphics.DrawString(text, font, new SolidBrush(Color.FromArgb(0, 0, 0)), 0, 0);
            graphics.Flush();

            return bitmap;
        }
        catch (Exception Ex)
        {
            throw new Exception("Error: " + Ex.Message);
        }
    }

    public string code128(string chaine)
    {
        string respuesta = "";
        char letra;
        int i = 0;
        long checksum = 0;
        int mini = 0;
        int dummy = 0;
        bool tableB = false;

        if (chaine.Length > 0)
        {

            //Vérifier si caractères valides
            //Check for valid characters
            for (i = 0; i < chaine.Length; i++)
            { //inicio for
                letra = Convert.ToChar(chaine.Substring(i, 1));

                if ((letra >= 32 && letra <= 126) || letra == 203)
                {

                }
                else
                {
                    i = 0;
                    break;
                }
            } //fin for

            respuesta = "";
            tableB = true;

            if (i > 0)
            {
                i = 0;
                while (i < chaine.Length)
                {

                    if (tableB)
                    {

                        mini = (i == 0 || i + 3 == chaine.Length ? 4 : 6);
                        testnum(ref mini, chaine, i);
                        if (mini < 0)
                        {
                            if (i == 0)
                                respuesta = Convert.ToString(Convert.ToChar(210));
                            else
                                respuesta = respuesta + Convert.ToString(Convert.ToChar(204));

                            tableB = false;
                        }
                        else
                        {
                            if (i == 0)
                                respuesta = Convert.ToString(Convert.ToChar(209));
                        }

                    } // fin table b  

                    if (!tableB)
                    {
                        mini = 2;
                        testnum(ref mini, chaine, i);
                        if (mini < 0)
                        {
                            dummy = Convert.ToInt32(chaine.Substring(i, 2));
                            dummy = (dummy < 95 ? dummy + 32 : dummy + 105);
                            respuesta = respuesta + Convert.ToString(Convert.ToChar(dummy));
                            i = i + 2;
                        }
                        else
                        {
                            respuesta = respuesta + Convert.ToString(Convert.ToChar(205));
                            tableB = true;
                        }


                    } // fin table b negado


                    if (tableB)
                    {
                        respuesta = respuesta + chaine.Substring(i, 1);
                        i = i + 1;
                    }



                } // fin while


                for (i = 0; i < respuesta.Length; i++)
                {

                    dummy = Convert.ToChar(respuesta.Substring(i, 1));
                    dummy = (dummy < 127 ? dummy - 32 : dummy - 105);
                    if (i == 0)
                        checksum = dummy;
                    checksum = (checksum + (i) * dummy) % 103;

                }

                checksum = (checksum < 95 ? checksum + 32 : checksum + 105);

                respuesta = respuesta + Convert.ToString(Convert.ToChar(checksum)) + Convert.ToString(Convert.ToChar(211));

            } // fin segundo if


        }// fin primer if
        return respuesta;
    }

    public void testnum(ref int mini, string chaine, int i)
    {
        char letra;
        mini = mini - 1;
        if (i + mini < chaine.Length)
        {
            while (mini >= 0)
            {
                letra = Convert.ToChar(chaine.Substring(i + mini, 1));
                if (letra < 48 || letra > 57)
                {
                    break;
                }

                mini = mini - 1;
            }
        }
    }

    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN:   Consultar_Datos_Cliente()
    //DESCRIPCIÓN:      Consulta los datos para la facturacion
    //PARÁMETROS:       Request; parametros del HttpContext
    //CREO:             Ramón Baeza Yépez
    //FECHA_CREO:       05/12/2013
    //*******************************************************************************************************
    public string Consultar_Datos_Cliente(HttpRequest Request)
    {
        Cls_Ope_Facturacion_Negocio Negocio = new Cls_Ope_Facturacion_Negocio();
        Cls_Cat_Cor_Predios_Negocio Negocio_Predio = new Cls_Cat_Cor_Predios_Negocio();
        String No_Folio = HttpContext.Current.Request["No_Folio"];
        String Cadena_Resultado = "[]";
        String rfc;
        DataTable Dt_Datos;
        DataTable Dt_Datos_Clientes;
        try
        {
            Negocio.Codigo_Barras = No_Folio;
            Dt_Datos = Negocio.Consultar_No_Cuenta();
            rfc = Negocio.Consultar_Diverso_RFC(No_Folio);

            Negocio.P_Rpu = Dt_Datos.Rows[0]["RPU"].ToString().Trim();

            if (!String.IsNullOrEmpty(Negocio.P_Rpu) && Negocio.P_Rpu != "0")
            {
                Negocio_Predio.P_RPU = Negocio.P_Rpu;
                Dt_Datos = Negocio_Predio.Consulta_Usuarios();
                if (Dt_Datos.Rows.Count > 0)
                {
                    Dt_Datos_Clientes = Negocio.Consultar_Datos_Cliente();
                    if (Dt_Datos_Clientes.Rows.Count > 0)
                    {
                        Dt_Datos.Rows[0]["cliente_id"] = Dt_Datos_Clientes.Rows[0]["cliente_id"];
                        Dt_Datos.Rows[0]["estatus"] = Dt_Datos_Clientes.Rows[0]["estatus"];
                    }

                }
            }
            else //if (rfc.Trim().Length > 0)
            {
                //Dt_Datos = Negocio.Consultar_Datos_Cliente(rfc);
                Dt_Datos = Negocio.Consultar_Diverso_Codigo_Barras(No_Folio);

                if (Dt_Datos.Rows.Count > 0 && !String.IsNullOrEmpty(Dt_Datos.Rows[0]["Usuario_No_Registrado_ID"].ToString().Trim()))
                {
                    //Negocio.P_Usuario_Id = Dt_Datos.Rows[0]["Usuario_ID"].ToString().Trim();
                    Dt_Datos = Negocio.Consultar_Datos_Usuarios_No_Registrados(Dt_Datos.Rows[0]["Usuario_No_Registrado_ID"].ToString().Trim());
                }

                else
                {
                    Dt_Datos = Negocio.Consultar_Sancion_Codigo_Barras(No_Folio);

                    if(Dt_Datos.Rows.Count > 0 && !String.IsNullOrEmpty(Dt_Datos.Rows[0]["Usuario_No_Registrado_ID"].ToString().Trim()))
                        Dt_Datos = Negocio.Consultar_Datos_Usuarios_No_Registrados(Dt_Datos.Rows[0]["Usuario_No_Registrado_ID"].ToString().Trim());
                }
            }

            Cadena_Resultado = SIAC.Ayudante_JQuery.Ayudante_JQuery.dataTableToJSON(Dt_Datos);
            Session["Folio_Pago"] = Negocio.P_Rpu;
        }
        catch (Exception Ex)
        {
            Cadena_Resultado = Respuesta(Ex.Message);
        }
        return Cadena_Resultado;
    }
    [WebMethod]
    public static string Consultar_Detalles_Recibo(String No_Folio)
    {
        Cls_Ope_Facturacion_Negocio Negocio = new Cls_Ope_Facturacion_Negocio();
        Cls_Apl_Parametros_Facturacion_Negocio Rs_Parametros_Facturacion = new Cls_Apl_Parametros_Facturacion_Negocio();
        String Cadena_Resultado = "[]";
        DataTable Dt_Recibo;
        DataTable Dt_Detalle;
        DataTable Dt_Costos_Factores = new DataTable();
        DataTable Dt_Parametros_Factura = new DataTable();
        DataTable Dt_Nota_Credito = new DataTable();
        DataSet Ds_Detalles_Pagos = new DataSet();
        String CODIGO_BARRAS = String.Empty;
        String NO_CUENTA = String.Empty;
        String FECHA_MOVIMIENTO = String.Empty;
        String CLAVE_CONCEPTO = String.Empty;
        String IVA = String.Empty;
        DateTime FECHA_TOLERANCIA;
        Int64 TIEMPO_TOLERANCIA = 0;
        string Permitir_Facturas_Anteriores = "NO";
        //---------------------------------------------------------------------
        Cls_Mensajes_Servidor objMensajeServidor = new Cls_Mensajes_Servidor();
        JsonSerializerSettings configuracionJson = new JsonSerializerSettings();
        configuracionJson.NullValueHandling = NullValueHandling.Ignore;
        //---------------------------------------------------------------------
        string strrespuesta;
        try
        {
            Dt_Costos_Factores = Negocio.Consultar_Costos_Factores();
            Dt_Parametros_Factura = Rs_Parametros_Facturacion.Consultar_Parametros();

            Negocio.No_Folio = No_Folio.Replace(';', ' ');
            Dt_Recibo = Negocio.Consultar_Recibo();

            if (Dt_Recibo.Rows.Count == 0)
            {
                objMensajeServidor.mensaje = "No se encontró el folio";
                objMensajeServidor.dato1 = "[]";
                strrespuesta = JsonConvert.SerializeObject(objMensajeServidor, Newtonsoft.Json.Formatting.Indented, configuracionJson);
                return strrespuesta;
            }

            if (Dt_Recibo.Rows.Count > 1)
            {
                objMensajeServidor.mensaje = "El folio se encuentra duplicado";
                objMensajeServidor.dato1 = "[]";
                strrespuesta = JsonConvert.SerializeObject(objMensajeServidor, Newtonsoft.Json.Formatting.Indented, configuracionJson);
                return strrespuesta;
            }

            Dt_Detalle = Negocio.Consultar_Detalles_Pago_PDF();
            Ds_Detalles_Pagos.Tables.Add(Dt_Detalle.Copy());

            TIEMPO_TOLERANCIA = Convert.ToInt32(Dt_Parametros_Factura.Rows[0]["DIAS_TOLERANCIA"].ToString());
            FECHA_TOLERANCIA = Convert.ToDateTime(Dt_Recibo.Rows[0]["FECHA"]).AddDays(TIEMPO_TOLERANCIA);
            Permitir_Facturas_Anteriores = Dt_Parametros_Factura.Rows[0]["PERMITIR_FACTURAS_ANTERIORES"].ToString();

            Negocio.Codigo_Barras = No_Folio.Replace(';', ' ');

            if (Dt_Recibo.Rows[0]["No_Factura"].ToString().Trim() == "NC" || Dt_Recibo.Rows[0]["No_Factura"].ToString().Trim() == "CANCELADA" || Dt_Recibo.Rows[0]["No_Factura"].ToString().Trim() == "0")
            {
                int count = Dt_Detalle.Rows.Count;
                objMensajeServidor.mensaje = "bien";
                objMensajeServidor.dato1 = SIAC.Ayudante_JQuery.Ayudante_JQuery.onDataGrid(Dt_Detalle, 1, count);
            }
            else
            {
                objMensajeServidor.mensaje = "Este pago ya se encuentra timbrado con el folio" + Dt_Recibo.Rows[0]["No_Factura"].ToString();
                objMensajeServidor.dato1 = "[]";
            }
            strrespuesta = JsonConvert.SerializeObject(objMensajeServidor, Newtonsoft.Json.Formatting.Indented, configuracionJson);
            return strrespuesta;
        }
        catch (Exception Ex)
        {
            //Cadena_Resultado = Respuesta(Ex.Message);
        }
        //return Cadena_Resultado;
        strrespuesta = JsonConvert.SerializeObject(objMensajeServidor, Newtonsoft.Json.Formatting.Indented, configuracionJson);
        return strrespuesta;

    }
    [WebMethod]
    public static string Consultar_Generales_Pago(string No_Folio)
    {
        DataTable Dt_Recibo;
        string str_respuesta;
        Cls_Ope_Facturacion_Negocio Negocio;
        //-------------------------------------------------
        Cls_Mensajes_Servidor objMensajeServidor = new Cls_Mensajes_Servidor();
        JsonSerializerSettings configuracionJson = new JsonSerializerSettings();
        configuracionJson.NullValueHandling = NullValueHandling.Ignore;
        //------------------------------------------------------

        try
        {
            Negocio = new Cls_Ope_Facturacion_Negocio();
            Negocio.No_Folio = No_Folio.Replace(';', ' ');
            Dt_Recibo = Negocio.Consultar_Recibo();
            str_respuesta = Ayudante_JQuery.dataTableToJSONsintotalrenglones(Dt_Recibo);
            objMensajeServidor.mensaje = "bien";
            objMensajeServidor.dato1 = str_respuesta;

        }
        catch (Exception ex)
        {
            objMensajeServidor.mensaje = ex.Message;
        }

        str_respuesta = JsonConvert.SerializeObject(objMensajeServidor, Newtonsoft.Json.Formatting.Indented, configuracionJson);
        return str_respuesta;

    }
    
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_Detalles_Pago()
        //DESCRIPCIÓN:      Consulta los folios facturados
        //PARÁMETROS:       Request; parametros del HttpContext
        //CREO:             Ramón Baeza Yépez
        //FECHA_CREO:       05/12/2013
        //*******************************************************************************************************
        public string Consultar_Detalles_Pago(HttpRequest Request)
    {
        Cls_Ope_Facturacion_Negocio Negocio = new Cls_Ope_Facturacion_Negocio();
        Cls_Apl_Parametros_Facturacion_Negocio Rs_Parametros_Facturacion = new Cls_Apl_Parametros_Facturacion_Negocio();
        String No_Folio = HttpContext.Current.Request["No_Folio"];
        String Cadena_Resultado = "[]";
        DataTable Dt_Recibo;
        DataTable Dt_Detalle;
        DataTable Dt_Costos_Factores = new DataTable();
        DataTable Dt_Parametros_Factura = new DataTable();
        DataTable Dt_Nota_Credito = new DataTable();
        DataSet Ds_Detalles_Pagos = new DataSet();
        String CODIGO_BARRAS = String.Empty;
        String NO_CUENTA = String.Empty;
        String FECHA_MOVIMIENTO = String.Empty;
        String CLAVE_CONCEPTO = String.Empty;
        String IVA = String.Empty;
        DateTime FECHA_TOLERANCIA;
        Int64 TIEMPO_TOLERANCIA = 0;
        string Permitir_Facturas_Anteriores = "NO";

        //---------------------------------------------------------------------
        Cls_Mensajes_Servidor objMensajeServidor = new Cls_Mensajes_Servidor();
        JsonSerializerSettings configuracionJson = new JsonSerializerSettings();
        configuracionJson.NullValueHandling = NullValueHandling.Ignore;
        //---------------------------------------------------------------------
        try
        {
            Dt_Costos_Factores = Negocio.Consultar_Costos_Factores();
            Dt_Parametros_Factura = Rs_Parametros_Facturacion.Consultar_Parametros();

            Negocio.No_Folio = No_Folio.Replace(';', ' ');
            Dt_Recibo = Negocio.Consultar_Recibo();

            if (Dt_Recibo.Rows.Count == 0)
                throw new Exception("No se encontró el folio");

            if (Dt_Recibo.Rows.Count > 1)
                throw new Exception("El folio se encuentra duplicado");

            Dt_Detalle = Negocio.Consultar_Detalles_Pago_PDF();
            Ds_Detalles_Pagos.Tables.Add(Dt_Detalle.Copy());

            TIEMPO_TOLERANCIA = Convert.ToInt32(Dt_Parametros_Factura.Rows[0]["DIAS_TOLERANCIA"].ToString());
            FECHA_TOLERANCIA = Convert.ToDateTime(Dt_Recibo.Rows[0]["FECHA"]).AddDays(TIEMPO_TOLERANCIA);
            Permitir_Facturas_Anteriores = Dt_Parametros_Factura.Rows[0]["PERMITIR_FACTURAS_ANTERIORES"].ToString();

            Negocio.Codigo_Barras = No_Folio.Replace(';', ' ');

            if (Dt_Recibo.Rows[0]["No_Factura"].ToString().Trim() == "NC" || Dt_Recibo.Rows[0]["No_Factura"].ToString().Trim() == "CANCELADA" || Dt_Recibo.Rows[0]["No_Factura"].ToString().Trim() == "0")
            {
                int count = Dt_Detalle.Rows.Count;
                Cadena_Resultado = SIAC.Ayudante_JQuery.Ayudante_JQuery.onDataGrid(Dt_Detalle, 1, count);
            }
            else
            {
                throw new Exception("Este pago ya se encuentra timbrado con el folio " + Dt_Recibo.Rows[0]["No_Factura"].ToString() + " - " + Dt_Recibo.Rows[0]["Codigo_Barras"].ToString());
            }
            return Cadena_Resultado;

            //Desde aqui no 
            //Dt_Nota_Credito = Negocio.Consultar_No_Cuenta();

            if (Dt_Nota_Credito.Rows[0]["No_Factura"].ToString().Trim() == "NC" || Dt_Nota_Credito.Rows[0]["No_Factura"].ToString().Trim() == "CANCELADA")
            {
                if (Convert.ToString((Convert.ToDateTime(Dt_Nota_Credito.Rows[0]["FECHA"]).Month) + "/" + (Convert.ToDateTime(Dt_Nota_Credito.Rows[0]["FECHA"]).Year))
                    == (Convert.ToString(DateTime.Now.Month.ToString()) + "/" + Convert.ToString(DateTime.Now.Year.ToString())))
                {
                    Dt_Detalle.Rows.Clear();
                    foreach (DataTable Dt_Detalles_Pagos in Ds_Detalles_Pagos.Tables)
                    {
                        // For each row, print the values of each column.
                        foreach (DataRow row in Dt_Detalles_Pagos.Rows)
                        {
                            DataRow Dr_Detalle = Dt_Detalle.NewRow();
                            Dr_Detalle = Dt_Detalle.NewRow();
                            Dr_Detalle["CODIGO_BARRAS"] = row["CODIGO_BARRAS"].ToString();
                            if (CODIGO_BARRAS == "") { CODIGO_BARRAS = row["CODIGO_BARRAS"].ToString(); }
                            if (row["NO_CUENTA"].ToString() == "")
                            {
                                Dr_Detalle["NO_CUENTA"] = 0;
                            }
                            else
                            {
                                Dr_Detalle["NO_CUENTA"] = row["NO_CUENTA"].ToString();
                            }

                            if (NO_CUENTA == "") { NO_CUENTA = row["NO_CUENTA"].ToString(); }
                            Dr_Detalle["CLAVE_CONCEPTO"] = row["CLAVE_CONCEPTO"].ToString();
                            if (CLAVE_CONCEPTO == "") { CLAVE_CONCEPTO = row["CLAVE_CONCEPTO"].ToString(); }
                            Dr_Detalle["DESCRIPCION"] = row["DESCRIPCION"].ToString();
                            Dr_Detalle["VALOR_UNITARIO"] = row["VALOR_UNITARIO"].ToString();
                            Dr_Detalle["IMPORTE"] = row["IMPORTE"].ToString();
                            Dr_Detalle["Porcentaje_IVA"] = row["Porcentaje_IVA"].ToString();
                            Dr_Detalle["IVA"] = row["IVA"].ToString();
                            Dr_Detalle["ivaCorrecto"] = row["ivaCorrecto"].ToString();
                            if (IVA == "0.0") { IVA = row["IVA"].ToString(); }
                            Dr_Detalle["TOTAL"] = row["TOTAL"].ToString();
                            Dr_Detalle["FECHA_MOVIMIENTO"] = row["FECHA_MOVIMIENTO"].ToString();
                            if (FECHA_MOVIMIENTO == "") { FECHA_MOVIMIENTO = row["FECHA_MOVIMIENTO"].ToString(); }
                            Dr_Detalle["UNIDAD_DE_MEDIDA"] = row["UNIDAD_DE_MEDIDA"].ToString();
                            Dr_Detalle["NO_CUENTA"] = row["NO_CUENTA"].ToString();
                            Dr_Detalle["PERIODO_PAGO"] = row["PERIODO_PAGO"].ToString();
                            Dr_Detalle["TIPO_SERVICIO"] = row["TIPO_SERVICIO"].ToString();

                            Dr_Detalle["totalGlobal"] = row["totalGlobal"].ToString();
                            Dr_Detalle["impuestoGlobal"] = row["impuestoGlobal"].ToString();
                            Dr_Detalle["subtotalGlobal"] = row["subtotalGlobal"].ToString();

                            Dt_Detalle.Rows.Add(Dr_Detalle);
                        }
                    }

                    //Agrupamos los pagos de los conceptos
                    Dt_Detalle.Columns.Add("IMPORTE_String", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("IVA_String", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("TOTAL_String", System.Type.GetType("System.String"));

                    for (int Indice_Pago = 0; Indice_Pago < Dt_Detalle.Rows.Count; Indice_Pago++)
                    {
                        for (int Indice_Pago_Siguiente = Indice_Pago + 1; Indice_Pago_Siguiente < Dt_Detalle.Rows.Count; Indice_Pago_Siguiente++)
                        {
                            if (Dt_Detalle.Rows[Indice_Pago]["DESCRIPCION"].ToString().Trim().Equals(Dt_Detalle.Rows[Indice_Pago_Siguiente]["DESCRIPCION"].ToString().Trim()))
                            {
                                Dt_Detalle.Rows[Indice_Pago]["IMPORTE"] = Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago]["IMPORTE"].ToString().Replace("$", "").Replace(",", "")) +
                                Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago_Siguiente]["IMPORTE"]);
                                Dt_Detalle.Rows[Indice_Pago]["IVA"] = Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago]["IVA"].ToString().Replace("$", "").Replace(",", "")) +
                                Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago_Siguiente]["IVA"]);
                                Dt_Detalle.Rows[Indice_Pago]["TOTAL"] = Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago]["TOTAL"].ToString().Replace("$", "").Replace(",", "")) +
                                Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago_Siguiente]["TOTAL"]);

                                Dt_Detalle.Rows.RemoveAt(Indice_Pago_Siguiente);
                                Indice_Pago_Siguiente--;
                            }
                        }

                        Dt_Detalle.Rows[Indice_Pago]["IMPORTE_String"] = String.Format("{0:C}", (Dt_Detalle.Rows[Indice_Pago]["IMPORTE"]));
                        Dt_Detalle.Rows[Indice_Pago]["IVA_String"] = String.Format("{0:C}", Dt_Detalle.Rows[Indice_Pago]["IVA"]);
                        Dt_Detalle.Rows[Indice_Pago]["TOTAL_String"] = String.Format("{0:C}", Dt_Detalle.Rows[Indice_Pago]["TOTAL"]);
                    }

                    Session["Dt_Detalle"] = Dt_Detalle;
                    int count = Dt_Detalle.Rows.Count;
                    Cadena_Resultado = SIAC.Ayudante_JQuery.Ayudante_JQuery.onDataGrid(Dt_Detalle, 1, count);
                }
                else if (Permitir_Facturas_Anteriores == "SI")
                {
                    Dt_Detalle.Rows.Clear();
                    foreach (DataTable Dt_Detalles_Pagos in Ds_Detalles_Pagos.Tables)
                    {
                        // For each row, print the values of each column.
                        foreach (DataRow row in Dt_Detalles_Pagos.Rows)
                        {
                            DataRow Dr_Detalle = Dt_Detalle.NewRow();
                            Dr_Detalle = Dt_Detalle.NewRow();
                            Dr_Detalle["CODIGO_BARRAS"] = row["CODIGO_BARRAS"].ToString();
                            if (CODIGO_BARRAS == "") { CODIGO_BARRAS = row["CODIGO_BARRAS"].ToString(); }
                            if (row["NO_CUENTA"].ToString() == "")
                            {
                                Dr_Detalle["NO_CUENTA"] = 0;
                            }
                            else
                            {
                                Dr_Detalle["NO_CUENTA"] = row["NO_CUENTA"].ToString();
                            }

                            if (NO_CUENTA == "") { NO_CUENTA = row["NO_CUENTA"].ToString(); }
                            Dr_Detalle["CLAVE_CONCEPTO"] = row["CLAVE_CONCEPTO"].ToString();
                            if (CLAVE_CONCEPTO == "") { CLAVE_CONCEPTO = row["CLAVE_CONCEPTO"].ToString(); }
                            Dr_Detalle["DESCRIPCION"] = row["DESCRIPCION"].ToString();
                            Dr_Detalle["VALOR_UNITARIO"] = row["VALOR_UNITARIO"].ToString();
                            Dr_Detalle["IMPORTE"] = row["IMPORTE"].ToString();
                            //Dr_Detalle["lleva_iva"] = row["lleva_IVA"].ToString();
                            //if (row["lleva_IVA"].ToString() == "NO")
                            //    Dr_Detalle["Porcentaje_IVA"] = "0.0";
                            //else
                            Dr_Detalle["Porcentaje_IVA"] = row["Porcentaje_IVA"].ToString();
                            Dr_Detalle["IVA"] = row["IVA"].ToString();
                            Dr_Detalle["ivaCorrecto"] = row["ivaCorrecto"].ToString();
                            if (IVA == "0.0") { IVA = row["IVA"].ToString(); }
                            Dr_Detalle["TOTAL"] = row["TOTAL"].ToString();
                            Dr_Detalle["FECHA_MOVIMIENTO"] = row["FECHA_MOVIMIENTO"].ToString();
                            if (FECHA_MOVIMIENTO == "") { FECHA_MOVIMIENTO = row["FECHA_MOVIMIENTO"].ToString(); }
                            Dr_Detalle["UNIDAD_DE_MEDIDA"] = row["UNIDAD_DE_MEDIDA"].ToString();
                            Dr_Detalle["NO_CUENTA"] = row["NO_CUENTA"].ToString();
                            Dr_Detalle["PERIODO_PAGO"] = row["PERIODO_PAGO"].ToString();
                            Dr_Detalle["TIPO_SERVICIO"] = row["TIPO_SERVICIO"].ToString();

                            Dr_Detalle["totalGlobal"] = row["totalGlobal"].ToString();
                            Dr_Detalle["impuestoGlobal"] = row["impuestoGlobal"].ToString();
                            Dr_Detalle["subtotalGlobal"] = row["subtotalGlobal"].ToString();

                            Dt_Detalle.Rows.Add(Dr_Detalle);
                        }
                    }

                    //Agrupamos los pagos de los conceptos
                    Dt_Detalle.Columns.Add("IMPORTE_String", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("IVA_String", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("TOTAL_String", System.Type.GetType("System.String"));

                    for (int Indice_Pago = 0; Indice_Pago < Dt_Detalle.Rows.Count; Indice_Pago++)
                    {
                        for (int Indice_Pago_Siguiente = Indice_Pago + 1; Indice_Pago_Siguiente < Dt_Detalle.Rows.Count; Indice_Pago_Siguiente++)
                        {
                            if (Dt_Detalle.Rows[Indice_Pago]["DESCRIPCION"].ToString().Trim().Equals(Dt_Detalle.Rows[Indice_Pago_Siguiente]["DESCRIPCION"].ToString().Trim()))
                            {
                                Dt_Detalle.Rows[Indice_Pago]["IMPORTE"] = Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago]["IMPORTE"].ToString().Replace("$", "").Replace(",", "")) +
                                Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago_Siguiente]["IMPORTE"]);
                                Dt_Detalle.Rows[Indice_Pago]["IVA"] = Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago]["IVA"].ToString().Replace("$", "").Replace(",", "")) +
                                Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago_Siguiente]["IVA"]);
                                Dt_Detalle.Rows[Indice_Pago]["TOTAL"] = Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago]["TOTAL"].ToString().Replace("$", "").Replace(",", "")) +
                                Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago_Siguiente]["TOTAL"]);

                                Dt_Detalle.Rows.RemoveAt(Indice_Pago_Siguiente);
                                Indice_Pago_Siguiente--;
                            }
                        }

                        Dt_Detalle.Rows[Indice_Pago]["IMPORTE_String"] = String.Format("{0:C}", (Dt_Detalle.Rows[Indice_Pago]["IMPORTE"]));
                        Dt_Detalle.Rows[Indice_Pago]["IVA_String"] = String.Format("{0:C}", Dt_Detalle.Rows[Indice_Pago]["IVA"]);
                        Dt_Detalle.Rows[Indice_Pago]["TOTAL_String"] = String.Format("{0:C}", Dt_Detalle.Rows[Indice_Pago]["TOTAL"]);
                    }

                    Session["Dt_Detalle"] = Dt_Detalle;
                    int count = Dt_Detalle.Rows.Count;
                    Cadena_Resultado = SIAC.Ayudante_JQuery.Ayudante_JQuery.onDataGrid(Dt_Detalle, 1, count);
                }
                else
                {
                    Cadena_Resultado = "error_fecha";
                }
            }
            else
            {
                //if (Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy")) <= (Convert.ToDateTime(String.Format("{0:dd/MM/yyyy}", FECHA_TOLERANCIA))))
                if ((DateTime.Now) <= (FECHA_TOLERANCIA))
                {
                    Dt_Detalle.Rows.Clear();
                    foreach (DataTable Dt_Detalles_Pagos in Ds_Detalles_Pagos.Tables)
                    {
                        // For each row, print the values of each column.
                        foreach (DataRow row in Dt_Detalles_Pagos.Rows)
                        {
                            DataRow Dr_Detalle = Dt_Detalle.NewRow();
                            Dr_Detalle = Dt_Detalle.NewRow();
                            Dr_Detalle["CODIGO_BARRAS"] = row["CODIGO_BARRAS"].ToString();
                            if (CODIGO_BARRAS == "") { CODIGO_BARRAS = row["CODIGO_BARRAS"].ToString(); }
                            if (row["NO_CUENTA"].ToString() == "")
                            {
                                Dr_Detalle["NO_CUENTA"] = 0;
                            }
                            else
                            {
                                Dr_Detalle["NO_CUENTA"] = row["NO_CUENTA"].ToString();
                            }

                            if (NO_CUENTA == "") { NO_CUENTA = row["NO_CUENTA"].ToString(); }
                            Dr_Detalle["CLAVE_CONCEPTO"] = row["CLAVE_CONCEPTO"].ToString();
                            if (CLAVE_CONCEPTO == "") { CLAVE_CONCEPTO = row["CLAVE_CONCEPTO"].ToString(); }
                            Dr_Detalle["DESCRIPCION"] = row["DESCRIPCION"].ToString();
                            Dr_Detalle["VALOR_UNITARIO"] = row["VALOR_UNITARIO"].ToString();
                            Dr_Detalle["IMPORTE"] = row["IMPORTE"].ToString();
                            Dr_Detalle["IVA"] = row["IVA"].ToString();
                            //Dr_Detalle["lleva_iva"] = row["lleva_IVA"].ToString();
                            //if (row["lleva_IVA"].ToString() == "NO")
                            //    Dr_Detalle["Porcentaje_IVA"] = "0.0";
                            //else
                            Dr_Detalle["Porcentaje_IVA"] = row["Porcentaje_IVA"].ToString();
                            if ((IVA == "0.0") || (IVA == "")) { IVA = row["IVA"].ToString(); }
                            Dr_Detalle["TOTAL"] = row["TOTAL"].ToString();
                            Dr_Detalle["FECHA_MOVIMIENTO"] = row["FECHA_MOVIMIENTO"].ToString();
                            if (FECHA_MOVIMIENTO == "") { FECHA_MOVIMIENTO = row["FECHA_MOVIMIENTO"].ToString(); }
                            Dr_Detalle["UNIDAD_DE_MEDIDA"] = row["UNIDAD_DE_MEDIDA"].ToString();
                            Dr_Detalle["NO_CUENTA"] = row["NO_CUENTA"].ToString();
                            Dr_Detalle["PERIODO_PAGO"] = row["PERIODO_PAGO"].ToString();
                            Dr_Detalle["TIPO_SERVICIO"] = row["TIPO_SERVICIO"].ToString();
                            Dr_Detalle["ivaCorrecto"] = row["ivaCorrecto"].ToString();

                            Dr_Detalle["totalGlobal"] = row["totalGlobal"].ToString();
                            Dr_Detalle["impuestoGlobal"] = row["impuestoGlobal"].ToString();
                            Dr_Detalle["subtotalGlobal"] = row["subtotalGlobal"].ToString();

                            Dt_Detalle.Rows.Add(Dr_Detalle);
                        }
                    }

                    //Agrupamos los pagos de los conceptos
                    Dt_Detalle.Columns.Add("IMPORTE_String", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("IVA_String", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("TOTAL_String", System.Type.GetType("System.String"));

                    for (int Indice_Pago = 0; Indice_Pago < Dt_Detalle.Rows.Count; Indice_Pago++)
                    {
                        for (int Indice_Pago_Siguiente = Indice_Pago + 1; Indice_Pago_Siguiente < Dt_Detalle.Rows.Count; Indice_Pago_Siguiente++)
                        {
                            if (Dt_Detalle.Rows[Indice_Pago]["DESCRIPCION"].ToString().Trim().Equals(Dt_Detalle.Rows[Indice_Pago_Siguiente]["DESCRIPCION"].ToString().Trim()))
                            {
                                Dt_Detalle.Rows[Indice_Pago]["IMPORTE"] = Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago]["IMPORTE"].ToString().Replace("$", "").Replace(",", "")) +
                                Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago_Siguiente]["IMPORTE"]);
                                Dt_Detalle.Rows[Indice_Pago]["IVA"] = Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago]["IVA"].ToString().Replace("$", "").Replace(",", "")) +
                                Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago_Siguiente]["IVA"]);
                                Dt_Detalle.Rows[Indice_Pago]["TOTAL"] = Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago]["TOTAL"].ToString().Replace("$", "").Replace(",", "")) +
                                Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago_Siguiente]["TOTAL"]);

                                Dt_Detalle.Rows.RemoveAt(Indice_Pago_Siguiente);
                                Indice_Pago_Siguiente--;
                            }
                        }
                        Dt_Detalle.Rows[Indice_Pago]["IMPORTE_String"] = String.Format("{0:C}", (Dt_Detalle.Rows[Indice_Pago]["IMPORTE"]));
                        Dt_Detalle.Rows[Indice_Pago]["IVA_String"] = String.Format("{0:C}", Dt_Detalle.Rows[Indice_Pago]["IVA"]);
                        Dt_Detalle.Rows[Indice_Pago]["TOTAL_String"] = String.Format("{0:C}", Dt_Detalle.Rows[Indice_Pago]["TOTAL"]);
                    }

                    Session["Dt_Detalle"] = Dt_Detalle;
                    int count = Dt_Detalle.Rows.Count;
                    Cadena_Resultado = SIAC.Ayudante_JQuery.Ayudante_JQuery.onDataGrid(Dt_Detalle, 1, count);
                }
                else
                {
                    DataTable Dt_Factura_Usuario = new DataTable();
                    Negocio.P_No_Factura = Dt_Nota_Credito.Rows[0]["No_Factura"].ToString().Trim();
                    Dt_Factura_Usuario = Negocio.Consultar_Datos_Factura();
                    if (Dt_Factura_Usuario.Rows.Count > 0)
                    {
                        if (!String.IsNullOrEmpty(Dt_Factura_Usuario.Rows[0]["Folio_Pago"].ToString()))
                        {
                            Dt_Detalle.Rows.Clear();

                            foreach (DataTable Dt_Detalles_Pagos in Ds_Detalles_Pagos.Tables)
                            {
                                // For each row, print the values of each column.
                                foreach (DataRow row in Dt_Detalles_Pagos.Rows)
                                {
                                    DataRow Dr_Detalle = Dt_Detalle.NewRow();
                                    Dr_Detalle = Dt_Detalle.NewRow();
                                    Dr_Detalle["CODIGO_BARRAS"] = row["CODIGO_BARRAS"].ToString();
                                    if (CODIGO_BARRAS == "") { CODIGO_BARRAS = row["CODIGO_BARRAS"].ToString(); }
                                    if (row["NO_CUENTA"].ToString() == "")
                                    {
                                        Dr_Detalle["NO_CUENTA"] = 0;
                                    }
                                    else
                                    {
                                        Dr_Detalle["NO_CUENTA"] = row["NO_CUENTA"].ToString();
                                    }

                                    if (NO_CUENTA == "") { NO_CUENTA = row["NO_CUENTA"].ToString(); }
                                    Dr_Detalle["CLAVE_CONCEPTO"] = row["CLAVE_CONCEPTO"].ToString();
                                    if (CLAVE_CONCEPTO == "") { CLAVE_CONCEPTO = row["CLAVE_CONCEPTO"].ToString(); }
                                    Dr_Detalle["DESCRIPCION"] = row["DESCRIPCION"].ToString();
                                    Dr_Detalle["VALOR_UNITARIO"] = row["VALOR_UNITARIO"].ToString();
                                    Dr_Detalle["IMPORTE"] = row["IMPORTE"].ToString();
                                    Dr_Detalle["IVA"] = row["IVA"].ToString();
                                    //Dr_Detalle["lleva_iva"] = row["lleva_IVA"].ToString();
                                    //if (row["lleva_IVA"].ToString() == "NO")
                                    //    Dr_Detalle["Porcentaje_IVA"] = "0.0";
                                    //else
                                    Dr_Detalle["Porcentaje_IVA"] = row["Porcentaje_IVA"].ToString();
                                    if ((IVA == "0.0") || (IVA == "")) { IVA = row["IVA"].ToString(); }
                                    Dr_Detalle["TOTAL"] = row["TOTAL"].ToString();
                                    Dr_Detalle["FECHA_MOVIMIENTO"] = row["FECHA_MOVIMIENTO"].ToString();
                                    if (FECHA_MOVIMIENTO == "") { FECHA_MOVIMIENTO = row["FECHA_MOVIMIENTO"].ToString(); }
                                    Dr_Detalle["UNIDAD_DE_MEDIDA"] = row["UNIDAD_DE_MEDIDA"].ToString();
                                    Dr_Detalle["NO_CUENTA"] = row["NO_CUENTA"].ToString();
                                    Dr_Detalle["PERIODO_PAGO"] = row["PERIODO_PAGO"].ToString();
                                    Dr_Detalle["TIPO_SERVICIO"] = row["TIPO_SERVICIO"].ToString();

                                    Dr_Detalle["totalGlobal"] = row["totalGlobal"].ToString();
                                    Dr_Detalle["impuestoGlobal"] = row["impuestoGlobal"].ToString();
                                    Dr_Detalle["subtotalGlobal"] = row["subtotalGlobal"].ToString();

                                    Dt_Detalle.Rows.Add(Dr_Detalle);
                                }
                            }

                            //Agrupamos los pagos de los conceptos
                            Dt_Detalle.Columns.Add("IMPORTE_String", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("IVA_String", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("TOTAL_String", System.Type.GetType("System.String"));

                            for (int Indice_Pago = 0; Indice_Pago < Dt_Detalle.Rows.Count; Indice_Pago++)
                            {
                                for (int Indice_Pago_Siguiente = Indice_Pago + 1; Indice_Pago_Siguiente < Dt_Detalle.Rows.Count; Indice_Pago_Siguiente++)
                                {
                                    if (Dt_Detalle.Rows[Indice_Pago]["DESCRIPCION"].ToString().Trim().Equals(Dt_Detalle.Rows[Indice_Pago_Siguiente]["DESCRIPCION"].ToString().Trim()))
                                    {
                                        Dt_Detalle.Rows[Indice_Pago]["IMPORTE"] = Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago]["IMPORTE"].ToString().Replace("$", "").Replace(",", "")) +
                                        Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago_Siguiente]["IMPORTE"]);
                                        Dt_Detalle.Rows[Indice_Pago]["IVA"] = Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago]["IVA"].ToString().Replace("$", "").Replace(",", "")) +
                                        Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago_Siguiente]["IVA"]);
                                        Dt_Detalle.Rows[Indice_Pago]["TOTAL"] = Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago]["TOTAL"].ToString().Replace("$", "").Replace(",", "")) +
                                            Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago_Siguiente]["TOTAL"]);
                                        Dt_Detalle.Rows.RemoveAt(Indice_Pago_Siguiente);
                                        Indice_Pago_Siguiente--;
                                    }
                                }

                                Dt_Detalle.Rows[Indice_Pago]["IMPORTE_String"] = String.Format("{0:C}", (Dt_Detalle.Rows[Indice_Pago]["IMPORTE"]));
                                Dt_Detalle.Rows[Indice_Pago]["IVA_String"] = String.Format("{0:C}", Dt_Detalle.Rows[Indice_Pago]["IVA"]);
                                Dt_Detalle.Rows[Indice_Pago]["TOTAL_String"] = String.Format("{0:C}", Dt_Detalle.Rows[Indice_Pago]["TOTAL"]);
                            }

                            Session["Dt_Detalle"] = Dt_Detalle;
                            int count = Dt_Detalle.Rows.Count;
                            Cadena_Resultado = SIAC.Ayudante_JQuery.Ayudante_JQuery.onDataGrid(Dt_Detalle, 1, count);

                        }
                        else
                        {
                            Cadena_Resultado = "error_fecha";
                        }
                    }
                    else
                    {
                        Cadena_Resultado = "error_fecha";
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            Cadena_Resultado = Ex.Message;
        }
        //return Cadena_Resultado;
        return Cadena_Resultado;
    }

    public string Consultar_Detalles_Pago_Facturado(HttpRequest Request)
    {
        Cls_Ope_Facturacion_Negocio Negocio = new Cls_Ope_Facturacion_Negocio();
        Cls_Apl_Parametros_Facturacion_Negocio Rs_Parametros_Facturacion = new Cls_Apl_Parametros_Facturacion_Negocio();
        String No_Folio = HttpContext.Current.Request["No_Folio"];
        String Cadena_Resultado = "[]";
        DataTable Dt_Detalle;
        DataTable Dt_Costos_Factores = new DataTable();
        DataTable Dt_Parametros_Factura = new DataTable();
        DataTable Dt_Nota_Credito = new DataTable();
        DataSet Ds_Detalles_Pagos = new DataSet();
        String CODIGO_BARRAS = String.Empty;
        String NO_CUENTA = String.Empty;
        String FECHA_MOVIMIENTO = String.Empty;
        String CLAVE_CONCEPTO = String.Empty;
        String IVA = String.Empty;
        DateTime FECHA_TOLERANCIA;
        Int64 TIEMPO_TOLERANCIA = 0;
        string Permitir_Facturas_Anteriores = "NO";

        try
        {
            Dt_Costos_Factores = Negocio.Consultar_Costos_Factores();
            Dt_Parametros_Factura = Rs_Parametros_Facturacion.Consultar_Parametros();

            Negocio.No_Folio = No_Folio.Replace(';', ' ');
            Dt_Detalle = Negocio.Consultar_Detalles_Pago_PDF();
            Ds_Detalles_Pagos.Tables.Add(Dt_Detalle.Copy());

            TIEMPO_TOLERANCIA = Convert.ToInt32(Dt_Parametros_Factura.Rows[0]["DIAS_TOLERANCIA"].ToString());
            FECHA_TOLERANCIA = Convert.ToDateTime(Dt_Detalle.Rows[0]["FECHA_MOVIMIENTO"]).AddDays(TIEMPO_TOLERANCIA);
            Permitir_Facturas_Anteriores = Dt_Parametros_Factura.Rows[0]["PERMITIR_FACTURAS_ANTERIORES"].ToString();

            Negocio.Codigo_Barras = No_Folio.Replace(';', ' ');
            Dt_Nota_Credito = Negocio.Consultar_No_Cuenta();

            DataTable Dt_Factura_Usuario = new DataTable();
            Negocio.P_No_Factura = Dt_Nota_Credito.Rows[0]["No_Factura"].ToString().Trim();
            Dt_Factura_Usuario = Negocio.Consultar_Datos_Factura();

            if (Dt_Factura_Usuario.Rows.Count > 0)
            {
                if (!String.IsNullOrEmpty(Dt_Factura_Usuario.Rows[0]["Folio_Pago"].ToString()))
                {
                    Dt_Detalle.Rows.Clear();
                    Dt_Detalle = Negocio.Consultar_Detalles_Pago_Facturado();
                    if(Dt_Detalle.Rows.Count<=0)
                        throw new Exception("Error al obtener número de factura");
                    //Agrupamos los pagos de los conceptos
                    Dt_Detalle.Columns.Add("IMPORTE_String", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("IVA_String", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("TOTAL_String", System.Type.GetType("System.String"));

                    for (int Indice_Pago = 0; Indice_Pago < Dt_Detalle.Rows.Count; Indice_Pago++)
                    {
                        for (int Indice_Pago_Siguiente = Indice_Pago + 1; Indice_Pago_Siguiente < Dt_Detalle.Rows.Count; Indice_Pago_Siguiente++)
                        {
                            if (Dt_Detalle.Rows[Indice_Pago]["DESCRIPCION"].ToString().Trim().Equals(Dt_Detalle.Rows[Indice_Pago_Siguiente]["DESCRIPCION"].ToString().Trim()))
                            {
                                Dt_Detalle.Rows[Indice_Pago]["IMPORTE"] = Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago]["IMPORTE"].ToString().Replace("$", "").Replace(",", "")) +
                                Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago_Siguiente]["IMPORTE"]);
                                Dt_Detalle.Rows[Indice_Pago]["IVA"] = Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago]["IVA"].ToString().Replace("$", "").Replace(",", "")) +
                                Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago_Siguiente]["IVA"]);
                                Dt_Detalle.Rows[Indice_Pago]["TOTAL"] = Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago]["TOTAL"].ToString().Replace("$", "").Replace(",", "")) +
                                    Convert.ToDouble(Dt_Detalle.Rows[Indice_Pago_Siguiente]["TOTAL"]);
                                Dt_Detalle.Rows.RemoveAt(Indice_Pago_Siguiente);
                                Indice_Pago_Siguiente--;
                            }
                        }

                        Dt_Detalle.Rows[Indice_Pago]["IMPORTE_String"] = String.Format("{0:C}", (Dt_Detalle.Rows[Indice_Pago]["IMPORTE"]));
                        Dt_Detalle.Rows[Indice_Pago]["IVA_String"] = String.Format("{0:C}", Dt_Detalle.Rows[Indice_Pago]["IVA"]);
                        Dt_Detalle.Rows[Indice_Pago]["TOTAL_String"] = String.Format("{0:C}", Dt_Detalle.Rows[Indice_Pago]["TOTAL"]);
                    }

                    Session["Dt_Detalle"] = Dt_Detalle;
                    int count = Dt_Detalle.Rows.Count;
                    Cadena_Resultado = SIAC.Ayudante_JQuery.Ayudante_JQuery.onDataGrid(Dt_Detalle, 1, count);

                }
                else
                {
                    Cadena_Resultado = "error_fecha";
                }
            }
            else
            {
                Cadena_Resultado = "error_fecha";
            }
        }
        catch (Exception Ex)
        {
            Cadena_Resultado = Ex.Message;
        }
        return Cadena_Resultado;
    }


    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN:   Alta_Datos_Cliente()
    //DESCRIPCIÓN:      modifica los datosa del cliente para la facturacion
    //PARÁMETROS:       Request; parametros del HttpContext
    //CREO:             Miguel Angel Alvarado Enriquez
    //FECHA_CREO:       19/Feb/2014
    //*******************************************************************************************************
    public string Alta_Datos_Cliente(HttpRequest Request)
    {
        Cls_Ope_Facturacion_Negocio Negocio = new Cls_Ope_Facturacion_Negocio();

        String Cadena_Resultado = "[]";
        String Datos = HttpContext.Current.Request["Datos"];
        DataTable Dt_Predios = new DataTable();
        int pos;
        pos = Datos.IndexOf(',');

        if (pos != -1)
        {
            Datos = Datos.Replace("Txt_", "").Replace(',', ';');
        }
        else
        {
            Datos = Datos.Replace("Txt_", "").Replace(';', ' ');
        }

        DataTable Dt_Cliente;
        try
        {
            Dt_Cliente = SIAC.Ayudante_JQuery.Ayudante_JQuery.Json_A_DataTable(Datos);
            if (Dt_Cliente.Rows.Count > 0)
            {
                Negocio.Estado_ID = Dt_Cliente.Rows[0]["Estado_ID"].ToString().Trim();
                Negocio.No_Cuenta = Convert.ToInt32(Dt_Cliente.Rows[0]["No_Folio"].ToString().Trim().Substring(0, 6)).ToString(); ;
                Negocio.Nombre = Dt_Cliente.Rows[0]["Razon"].ToString().Trim();
                Negocio.Rfc = Dt_Cliente.Rows[0]["RFC"].ToString().Trim();
                Negocio.Pais = Dt_Cliente.Rows[0]["Pais"].ToString().Trim();
                Negocio.Estado = Dt_Cliente.Rows[0]["Estado"].ToString().Trim();
                Negocio.Ciudad = Dt_Cliente.Rows[0]["Ciudad"].ToString().Trim();
                Negocio.Localidad = Dt_Cliente.Rows[0]["Localidad"].ToString().Trim();
                Negocio.Colonia = Dt_Cliente.Rows[0]["Colonia"].ToString().Trim();
                Negocio.Calle = Dt_Cliente.Rows[0]["Calle"].ToString().Trim();
                Negocio.Numero_Exterior = Dt_Cliente.Rows[0]["Numero_Exterior"].ToString().Trim();
                Negocio.Numero_Interior = Dt_Cliente.Rows[0]["Numero_Interior"].ToString().Trim();
                Negocio.Codigo_Postal = Dt_Cliente.Rows[0]["Codigo_Postal"].ToString().Trim();
                Negocio.P_Predio_ID = Dt_Cliente.Rows[0]["Predio_ID"].ToString();
                Negocio.Email = Dt_Cliente.Rows[0]["Email"].ToString().Trim();
                Negocio.Alta_Cliente();
                String Cliente_ID = Negocio.Cliente_ID;
                Cadena_Resultado = Respuesta("Alta", Cliente_ID);
            }
        }
        catch (Exception Ex)
        {
            Cadena_Resultado = Respuesta(Ex.Message);
        }
        return Cadena_Resultado;
    }
    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN:   Modificar_Datos_Cliente()
    //DESCRIPCIÓN:      modifica los datosa del cliente para la facturacion
    //PARÁMETROS:       Request; parametros del HttpContext
    //CREO:             Ramón Baeza Yépez
    //FECHA_CREO:       05/12/2013
    //*******************************************************************************************************
    public string Modificar_Datos_Cliente(HttpRequest Request)
    {
        Cls_Ope_Facturacion_Negocio Negocio = new Cls_Ope_Facturacion_Negocio();
        String Cadena_Resultado = "[]";
        String Datos = HttpContext.Current.Request["Datos"];
        Datos = Datos.Replace("ctl00_", "");
        Datos = Datos.Replace("Cph_Contenido_", "");
        Datos = Datos.Replace("Txt_", "");
        Datos = Datos.Replace("Cmb_", "");
        DataTable Dt_Cliente;

        try
        {
            Dt_Cliente = SIAC.Ayudante_JQuery.Ayudante_JQuery.Json_A_DataTable(Datos);
            if (Dt_Cliente.Rows.Count > 0)
            {
                Negocio.Cliente_ID = Dt_Cliente.Rows[0]["Cliente_ID"].ToString().Trim();
                Negocio.Estado_ID = Dt_Cliente.Rows[0]["Estado_ID"].ToString().Trim();
                Negocio.No_Cuenta = Convert.ToInt32(Dt_Cliente.Rows[0]["No_Folio"].ToString().Trim().Substring(0, 6)).ToString();
                Negocio.Nombre = Dt_Cliente.Rows[0]["Razon"].ToString().Trim();
                Negocio.Rfc = Dt_Cliente.Rows[0]["RFC"].ToString().Trim();
                Negocio.Pais = Dt_Cliente.Rows[0]["Pais"].ToString().Trim();
                Negocio.Estado = Dt_Cliente.Rows[0]["Estado"].ToString().Trim();
                Negocio.Ciudad = Dt_Cliente.Rows[0]["Ciudad"].ToString().Trim();
                Negocio.Localidad = Dt_Cliente.Rows[0]["Localidad"].ToString().Trim();
                Negocio.Colonia = Dt_Cliente.Rows[0]["Colonia"].ToString().Trim();
                Negocio.Calle = Dt_Cliente.Rows[0]["Calle"].ToString().Trim();
                Negocio.Numero_Exterior = Dt_Cliente.Rows[0]["Numero_Exterior"].ToString().Trim();
                Negocio.Numero_Interior = Dt_Cliente.Rows[0]["Numero_Interior"].ToString().Trim();
                Negocio.Codigo_Postal = Dt_Cliente.Rows[0]["Codigo_Postal"].ToString().Trim();
                Negocio.Email = Dt_Cliente.Rows[0]["Email"].ToString().Trim();
                Negocio.No_Folio = Dt_Cliente.Rows[0]["No_Folio"].ToString().Trim();
                Negocio.Modificar_Datos_Cliente();
                Cadena_Resultado = Respuesta("Modificacion");
                Session["Dt_Cliente"] = Dt_Cliente;
            }
        }
        catch (Exception Ex)
        {
            Cadena_Resultado = Respuesta(Ex.Message);
        }
        return Cadena_Resultado;
    }
    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN:   Consultar_Estados()
    //DESCRIPCIÓN:      Consulta los estados para la facturacion
    //PARÁMETROS:       Request; parametros del HttpContext
    //CREO:             Ramón Baeza Yépez
    //FECHA_CREO:       05/12/2013
    //*******************************************************************************************************
    public string Consultar_Estados(HttpRequest Request)
    {
        Cls_Ope_Facturacion_Negocio Negocio = new Cls_Ope_Facturacion_Negocio();
        String Cadena_Resultado = "{[]}";
        DataTable Dt_Datos;

        try
        {
            Dt_Datos = Negocio.Consultar_Estados();
            Cadena_Resultado = SIAC.Ayudante_JQuery.Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt_Datos);
        }
        catch (Exception Ex)
        {
            String Mensaje_Error = Ex.Message.ToString();
        }
        return Cadena_Resultado;
    }
    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN:   Respuesta()
    //DESCRIPCIÓN:      Genera la cadena en formato json con la respuesta 
    //PARÁMETROS:       Respuesta; mensaje con la respuesta a ser transmitida
    //CREO:             Ramón Baeza Yépez
    //FECHA_CREO:       05/12/2013
    //*******************************************************************************************************
    public String Respuesta(String Respuesta)
    {
        StringBuilder Buffer = new StringBuilder();
        StringWriter Escritor = new StringWriter(Buffer);
        JsonWriter Escribir_Formato_JSON = new JsonTextWriter(Escritor);
        String Cadena_Resultado = "[]";
        Escribir_Formato_JSON.Formatting = Newtonsoft.Json.Formatting.None;
        Escribir_Formato_JSON.WriteStartObject();
        Escribir_Formato_JSON.WritePropertyName("Mensaje");
        Escribir_Formato_JSON.WriteValue(Respuesta);
        Escribir_Formato_JSON.WriteEndObject();
        Cadena_Resultado = Buffer.ToString();
        return Cadena_Resultado;
    }
    public String Respuesta(String Respuesta, String Cliente_ID)
    {
        StringBuilder Buffer = new StringBuilder();
        StringWriter Escritor = new StringWriter(Buffer);
        JsonWriter Escribir_Formato_JSON = new JsonTextWriter(Escritor);
        String Cadena_Resultado = "[]";
        Escribir_Formato_JSON.Formatting = Newtonsoft.Json.Formatting.None;
        Escribir_Formato_JSON.WriteStartObject();
        Escribir_Formato_JSON.WritePropertyName("Mensaje");
        Escribir_Formato_JSON.WriteValue(Respuesta);

        Escribir_Formato_JSON.WritePropertyName("Cliente_Id");
        Escribir_Formato_JSON.WriteValue(Cliente_ID);

        Escribir_Formato_JSON.WriteEndObject();
        Cadena_Resultado = Buffer.ToString();
        return Cadena_Resultado;
    }
    public String Respuesta(String Respuesta, String Ruta_Pdf, String Ruta_Corta_Pdf)
    {
        StringBuilder Buffer = new StringBuilder();
        StringWriter Escritor = new StringWriter(Buffer);
        JsonWriter Escribir_Formato_JSON = new JsonTextWriter(Escritor);
        String Cadena_Resultado = "[]";
        Escribir_Formato_JSON.Formatting = Newtonsoft.Json.Formatting.None;
        Escribir_Formato_JSON.WriteStartObject();
        Escribir_Formato_JSON.WritePropertyName("Mensaje");
        Escribir_Formato_JSON.WriteValue(Respuesta);

        Escribir_Formato_JSON.WritePropertyName("Ruta");
        Escribir_Formato_JSON.WriteValue(Ruta_Pdf);

        Escribir_Formato_JSON.WritePropertyName("Ruta_Corta");
        Escribir_Formato_JSON.WriteValue(Ruta_Corta_Pdf);

        Escribir_Formato_JSON.WriteEndObject();
        Cadena_Resultado = Buffer.ToString();
        return Cadena_Resultado;
    }

    public void Obtener_Datos_Sat(DataTable detalles)
    {
        DataColumn columnaUnidadSat = new DataColumn("unidad_sat");
        DataColumn columnaConceptoSat = new DataColumn("concepto_sat");
        detalles.Columns.Add(columnaUnidadSat);
        detalles.Columns.Add(columnaConceptoSat);

        foreach (DataRow fila in detalles.Rows)
        {
            DataTable datos = new Cls_Apl_Parametros_Facturacion_Negocio().ObtenerUnidadConceptoSat(fila["Clave_Concepto"].ToString());

            fila["unidad_sat"] = datos.Rows[0]["unidad_sat"].ToString();
            fila["concepto_sat"] = datos.Rows[0]["concepto_sat"].ToString();
        }
    }

    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN:   Imprimir_Factura()
    //DESCRIPCIÓN:      imprime la factura 
    //PARÁMETROS:       Request; parametros del HttpContext
    //CREO:             Ramón Baeza Yépez
    //FECHA_CREO:       05/12/2013
    //*******************************************************************************************************
    public string Imprimir_Factura(HttpRequest Request)
    {
        Cls_Ope_Facturacion_Negocio Negocio = new Cls_Ope_Facturacion_Negocio();
        Cls_Cat_Cor_Predios_Negocio Negocio_Predios = new Cls_Cat_Cor_Predios_Negocio();
        Cls_Apl_Parametros_Facturacion_Negocio Rs_Parametros_Facturacion = new Cls_Apl_Parametros_Facturacion_Negocio();
        StringBuilder Buffer = new StringBuilder();
        StringWriter Escritor = new StringWriter(Buffer);
        JsonWriter Escribir_Formato_JSON = new JsonTextWriter(Escritor);
        String Cadena_Resultado = "{[]}";
        // Datatables
        DataTable Dt_Datos_Clientes;             //Datos del cliente
        DataTable Dt_Detalles_Pago;              //Detalle del pago
        DataTable Dt_Datos_Fiscales;             //Datos fiscales del organismo
        DataTable Dt_Parametros_Facturacion;     //Parametros para la Facturación
        DataTable Dt_Impuestos = Crea_Tabla_Impuestos();
        DataTable Dt_Factura = new DataTable();
        DataTable Dt_Datos = new DataTable();

        // Parametros de facturacion
        String Regimen_Fiscal = String.Empty;
        String Expedida_En = String.Empty;
        String Timbre_Version = String.Empty;
        String Archivo_Cer = String.Empty;
        String Archivo_Key = String.Empty;
        String Password_Llave = String.Empty;

        // Ruta de archivos
        String Ruta_Xml = String.Empty;
        String Ruta_Qr = String.Empty;
        String Ruta_Cert = String.Empty;
        String Ruta_Key = String.Empty;
        String Ruta_Pdf = String.Empty;
        String Ruta_Externa_Xml = String.Empty;
        String Ruta_Externa_Pdf = String.Empty;
        String Ruta_Pdf_A_Enviar = String.Empty;
        String Ruta_Corta_Pdf = String.Empty;
        String Tipo_Comprobante = String.Empty;
        String[] Ruta_Imagen_Empresa = new string[4];
        //String Ruta_Imagen_Empresa_2 = String.Empty;

        // Datos del correo
        String Mensaje = String.Empty;
        String Envia = String.Empty;
        String Mensaje_Error_Correo = String.Empty;
        Boolean Envio_Correo = false;

        // Complementos de factura
        String Cadena_Original = String.Empty;
        String Cadena_UTF = String.Empty;
        String Cadena_Sello = String.Empty;

        Double Subtotal;
        Double Iva;
        Double Total;
        //Double Iva_General;
        Int32 Quitar_Tiempo = 0;
        String Id_Creado = String.Empty;
        String Codigo_Bidimensional = String.Empty;
        String rfc;
        decimal subTotalBueno = 0;
        double iva_recalculado_sin_redondeos = 0;
        double descuentos_facturados = 0;
        double recibo_descuadre_iva = 0;
        double total_ajuste_descuadre = 0;
        double redondeosNegativos = 0;
        decimal totalRedondeado = 0;
        decimal tasa_iva_general = 0;
        Boolean factura_generada = false;
        // Dataset de la factura
        DataSet Ds_Factura_Pdf = new DataSet();
        Boolean Bln_Ya_Se_Aplico_Descuadre_Iva_Conceptos_Sin_Iva = false;
        // Documento Xml
        XDocument Xml_Documento = new XDocument(new XDeclaration("1.0", "utf-8", "yes"));
        DataTable Dt_registro = new DataTable();        
        try
        {
            Negocio.Codigo_Barras = HttpContext.Current.Request["No_Folio"].Replace(';', ' ');
            Dt_Datos = Negocio.Consultar_No_Cuenta();
            Negocio.P_Tipo_Servicio = Dt_Datos.Rows[0]["Tipo_Servicio"].ToString();
            Negocio.P_Periodo_Pago = Dt_Datos.Rows[0]["Periodo"].ToString();
            string lugar_Expedicion = Negocio.Consultar_Lugar_Expedicion(Dt_Datos.Rows[0]["no_recibo"].ToString());

            Negocio.No_Folio = HttpContext.Current.Request["No_Folio"].Replace(';', ' ');
            Dt_Factura = Negocio.Consultar_Datos_Factura();

            if (Dt_Datos.Rows[0]["No_Factura"].ToString().Trim() != "NC" && Dt_Datos.Rows[0]["No_Factura"].ToString().Trim() != "CANCELADA" && !string.IsNullOrEmpty(Dt_Datos.Rows[0]["No_Factura"].ToString().Trim()))
            {
                throw new Exception("Factura_Generada");
            }
            Consulta_Maximo_Serie("FACTURA");
            Dt_Parametros_Facturacion = Rs_Parametros_Facturacion.Consultar_Parametros();
            if (Dt_Parametros_Facturacion.Rows.Count > 0)
            {
                tasa_iva_general = Math.Round( Convert.ToDecimal( Dt_Parametros_Facturacion.Rows[0]["tasa_iva_general"].ToString() ),4);
            }
            else
            {
                throw new Exception("No se encontraron parámetros de facturación");
            }
            rfc = Negocio.Consultar_Diverso_RFC(Negocio.Codigo_Barras);
            Negocio.P_Rpu = Dt_Datos.Rows[0]["RPU"].ToString();
            Negocio_Predios.P_RPU = Dt_Datos.Rows[0]["RPU"].ToString();
            String Cliente_ID = HttpContext.Current.Request["Cliente_Id"].ToString();

            if (!String.IsNullOrEmpty(Negocio_Predios.P_RPU))
            {
                if (Negocio.P_Rpu.Trim() != "0")
                {
                    Dt_Datos_Clientes = Negocio_Predios.Consulta_Usuarios();
                    if (Dt_Datos_Clientes.Rows.Count == 0)
                    {
                        if (String.IsNullOrEmpty(Cliente_ID))
                        {
                            Negocio.Cliente_ID = Cliente_ID;
                            Dt_Datos_Clientes = Negocio.Consultar_Datos_Cliente();
                        }
                        else
                            Dt_Datos_Clientes = Negocio.Consultar_Datos_Cliente(rfc);
                    }
                }
                else
                {
                    Dt_Datos_Clientes = Negocio.Consultar_Diverso_Codigo_Barras(HttpContext.Current.Request["No_Folio"].Replace(';', ' ').Trim());
                    if (Dt_Datos_Clientes.Rows.Count > 0 && !String.IsNullOrEmpty(Dt_Datos_Clientes.Rows[0]["Usuario_No_Registrado_ID"].ToString().Trim()))
                    {
                        Dt_Datos_Clientes = Negocio.Consultar_Datos_Usuarios_No_Registrados(Dt_Datos_Clientes.Rows[0]["Usuario_No_Registrado_ID"].ToString().Trim());

                    }
                    else
                    {
                        Dt_Datos_Clientes = Negocio.Consultar_Sancion_Codigo_Barras(HttpContext.Current.Request["No_Folio"].Replace(';', ' ').Trim());

                        if (Dt_Datos_Clientes.Rows.Count > 0 && !String.IsNullOrEmpty(Dt_Datos_Clientes.Rows[0]["Usuario_No_Registrado_ID"].ToString().Trim()))
                            Dt_Datos_Clientes = Negocio.Consultar_Datos_Usuarios_No_Registrados(Dt_Datos_Clientes.Rows[0]["Usuario_No_Registrado_ID"].ToString().Trim());
                    }
                }
            }
            else
            {
                if (String.IsNullOrEmpty(Cliente_ID))
                {
                    Negocio.Cliente_ID = Cliente_ID;
                    Dt_Datos_Clientes = Negocio.Consultar_Datos_Cliente();
                    if (Dt_Datos_Clientes.Rows.Count == 0)
                        Dt_Datos_Clientes = Negocio.Consultar_Datos_Cliente(rfc);
                }
                else
                    Dt_Datos_Clientes = Negocio.Consultar_Datos_Cliente(rfc);
            }

            //Dt_Detalles_Pago = (DataTable)Session["Dt_Detalle"];
            string jsonDetalles = HttpContext.Current.Request.Form["detalles"];
            Dt_Detalles_Pago = (DataTable)JsonConvert.DeserializeObject(jsonDetalles, (typeof(DataTable)));

            //Calcular_Iva_Sat(Negocio, Dt_Detalles_Pago, Convert.ToDouble(HttpContext.Current.Request["Iva_General"]) / 10);

            var detallesOriginales = Dt_Detalles_Pago.Copy();
            Obtener_Datos_Sat(detallesOriginales);
            DataColumn columnaDescuento = new DataColumn("descuento");
            columnaDescuento.DefaultValue = "0";
            Dt_Detalles_Pago.Columns.Add(columnaDescuento);

            Negocio.P_Usuario_Creo = HttpContext.Current.Request["empleado"];
            Negocio.Empresa_ID = "00001";
            Dt_Datos_Fiscales = Negocio.Consultar_Datos_Fiscales();
            Subtotal = Convert.ToDouble(HttpContext.Current.Request["Subtotal"]);
            //Subtotal_Iva = Convert.ToDouble(HttpContext.Current.Request["Subtotal_Iva"]);
            Total = Convert.ToDouble(HttpContext.Current.Request["Total"]);
            Iva = Convert.ToDouble(HttpContext.Current.Request["Iva_General"]);

            //Subtotal_Iva = Convert.ToDouble(Negocio.P_Iva);
            //Subtotal = Total - Subtotal_Iva;

            Ajustar_Redondeos(Dt_Detalles_Pago, Total, Iva);
            Obtener_Datos_Sat(Dt_Detalles_Pago);
            

            for (int i = 0; i < Dt_Detalles_Pago.Rows.Count; i++)
            {
                var fila = Dt_Detalles_Pago.Rows[i];
                var importe = Convert.ToDouble(fila["importe"]);
                var concepto_id = Convert.ToDouble(fila["clave_concepto"]);

                if (importe < 0 )
                {
                    importe = Math.Abs(importe);

                    var conceptoDescuento = Negocio.Verificar_Es_Descuento(fila["Clave_Concepto"].ToString());

                    if (conceptoDescuento != "00000")
                    {
                        bool banEliminado = false;

                        foreach (DataRow registro in Dt_Detalles_Pago.Rows)
                        {
                            if (registro["Clave_Concepto"].ToString() == conceptoDescuento)
                            {
                                double descuento = Convert.ToDouble(registro["descuento"]);
                                descuento += importe;
                                descuentos_facturados += descuento;
                                registro["descuento"] = descuento;
                                banEliminado = true;
                                break;
                            }
                        }

                        if (!banEliminado && (fila["Clave_Concepto"].ToString() == "00170" || fila["descripcion"].ToString() == "CONDONACION SANCIONES"))
                        {
                            foreach (DataRow concepto in Dt_Detalles_Pago.Rows)
                            {
                                var conceptoID = concepto["Clave_Concepto"].ToString();

                                if (conceptoID == "00170")
                                    continue;
                                DataTable sanciones = Negocio.Verificar_Es_Sancion(conceptoID);

                                if (sanciones.Rows.Count > 0)
                                {
                                    DataRow sancion = sanciones.Rows[0];

                                    if (Convert.ToDouble(concepto["total"]) >= (Convert.ToDouble(fila["total"])))
                                    {
                                        double descuento = Convert.ToDouble(concepto["descuento"]);
                                        descuento += importe;
                                        concepto["descuento"] = descuento;
                                        descuentos_facturados += descuento;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    else
                    {
                        //var cantidad = Math.Abs(importe);
                        redondeosNegativos += importe;
                        //Subtotal += cantidad;
                        //Dt_Detalles_Pago.Rows.Remove(Dt_Detalles_Pago.Rows[i]);
                        //i--;
                    }

                    //Subtotal += importe;
                    Dt_Detalles_Pago.Rows.Remove(Dt_Detalles_Pago.Rows[i]);
                    i--;
                }
                else
                {
                    totalRedondeado += Convert.ToDecimal(Math.Round(Convert.ToDecimal(fila["total"]), 2));
                    subTotalBueno += Math.Round(Convert.ToDecimal(fila["importe"]), 2,MidpointRounding.AwayFromZero);
                    if (Convert.ToDouble(fila["ivaCorrecto"]) > 0 || Convert.ToDouble(fila["iva"]) > 0)
                        iva_recalculado_sin_redondeos += Convert.ToDouble(fila["ivaCorrecto"]);
                }
            }          

            foreach (DataRow fila in Dt_Detalles_Pago.Rows)
            {
                double descuento = Convert.ToDouble(fila["descuento"]);
                double impuesto = Convert.ToDouble(fila["ivaCorrecto"]);

                if (redondeosNegativos > 0 && Convert.ToDouble(fila["importe"]) > redondeosNegativos + descuento )
                {
                    descuento += redondeosNegativos;
                    fila["descuento"] = descuento;
                    redondeosNegativos = 0;
                }

                Negocio.P_Total_Descuento += descuento;
            }
            total_ajuste_descuadre = Convert.ToDouble(subTotalBueno) + Math.Round(iva_recalculado_sin_redondeos,2);
            recibo_descuadre_iva = Total - total_ajuste_descuadre + Negocio.P_Total_Descuento ;
            recibo_descuadre_iva = Math.Round(recibo_descuadre_iva, 4);
            if (recibo_descuadre_iva < 0)
            {
                Negocio.P_Total_Descuento += Math.Abs(recibo_descuadre_iva);

                foreach (DataRow fila in Dt_Detalles_Pago.Rows)
                {
                    double descuento = Convert.ToDouble(fila["descuento"]) + Math.Abs(recibo_descuadre_iva);
                    
                    var conceptoDescuento = Negocio.Verificar_Es_Descuento(fila["Clave_Concepto"].ToString());

                    if (conceptoDescuento == "00000")
                    {
                        if (Convert.ToDouble(fila["total"]) > descuento)
                        {
                            fila["descuento"] = descuento;
                            break;
                        }
                    }
                }
            }
            else
            {
                foreach (DataRow fila in Dt_Detalles_Pago.Rows)
                {
                    double impuesto_temp = Convert.ToDouble(fila["ivaCorrecto"]);
                    double importe_temp = Convert.ToDouble(fila["importe"]);
                    double total_temp = importe_temp + recibo_descuadre_iva;
                    if (impuesto_temp == 0)
                    {
                        fila["importe"] = total_temp;
                        fila["total"] = total_temp;
                        subTotalBueno += Convert.ToDecimal( recibo_descuadre_iva );
                        Bln_Ya_Se_Aplico_Descuadre_Iva_Conceptos_Sin_Iva = true;
                        break;
                    }
                }
                if (!Bln_Ya_Se_Aplico_Descuadre_Iva_Conceptos_Sin_Iva)
                {
                    Negocio.P_Total_Descuento -= Math.Abs(recibo_descuadre_iva);

                    foreach (DataRow fila in Dt_Detalles_Pago.Rows)
                    {
                        double descuento = Convert.ToDouble(fila["descuento"]) - Math.Abs(recibo_descuadre_iva);

                        var conceptoDescuento = Negocio.Verificar_Es_Descuento(fila["Clave_Concepto"].ToString());

                        if (conceptoDescuento == "00000")
                        {
                            if (Convert.ToDouble(fila["total"]) > descuento)
                            {
                                fila["descuento"] = descuento;
                                break;
                            }
                        }
                    }
                }
            }
            //Negocio.P_Total_Descuento += descuentos_facturados;
            //Subtotal = (Double)totalRedondeado;

            Negocio.P_SubTotal_No_Descuentos = String.Format("{0:#0.00}", Math.Round(subTotalBueno, 2));

            // Lee los parametros de facturacion
            Dt_Parametros_Facturacion = Rs_Parametros_Facturacion.Consultar_Parametros();

            if (!String.IsNullOrEmpty(Convert.ToString(Dt_Parametros_Facturacion.Rows[0]["QUITAR_TIEMPO_TIMBRADO"])))
            {
                Quitar_Tiempo = (Convert.ToInt32(Dt_Parametros_Facturacion.Rows[0]["QUITAR_TIEMPO_TIMBRADO"]) * (-1));
            }
            else
            {
                Quitar_Tiempo = 0;
            }

            //Datos Generales de la Factura.
            //Id_Creado = Session["Factura_Serie"].ToString();
            //Negocio.P_No_Factura = Session["No_Factura"].ToString();
            Negocio.P_Serie = Session["Serie"].ToString();
            Negocio.P_Fecha_Creo_Xml_Factura = DateTime.Now.AddMinutes(Quitar_Tiempo);
            Negocio.P_Fecha_Creo_Xml = Negocio.P_Fecha_Creo_Xml_Factura.ToString("dd/MM/yyyy HH:mm:ss");
            Negocio.P_Fecha_Creo_Xml_Factura = DateTime.Now.AddMinutes(Quitar_Tiempo);
            Negocio.P_Cliente_Id = !String.IsNullOrEmpty(Cliente_ID) ? Cliente_ID : Dt_Datos_Clientes.Rows[0]["Cliente_Id"].ToString().Trim();
            Negocio.P_Email_Facturacion = HttpContext.Current.Request["Email"].ToString().Trim();
            Negocio.P_Orden_Compra = "";
            Negocio.P_Forma_Pago = "PUE";
            Negocio.P_Tipo_Comprobante = "I";

            Double Total_Efectivo = 0;
            Double Total_Tranferencia = 0;
            Double Total_Cheque = 0;
            Double Total_Tarjetas = 0;
            if (Dt_Datos != null && Dt_Datos.Rows.Count > 0)
            {
                DataRow Dr_Detalles = Dt_Datos.Rows[0];
                if (!String.IsNullOrEmpty(Dr_Detalles["PAGO_EFECTIVO"].ToString()))
                    Total_Efectivo += Double.Parse(Dr_Detalles["PAGO_EFECTIVO"].ToString());
                if (!String.IsNullOrEmpty(Dr_Detalles["TRANSFERENCIA"].ToString()))
                    Total_Tranferencia += Double.Parse(Dr_Detalles["TRANSFERENCIA"].ToString());
                if (!String.IsNullOrEmpty(Dr_Detalles["PAGO_CHEQUE"].ToString()))
                    Total_Cheque += Double.Parse(Dr_Detalles["PAGO_CHEQUE"].ToString());
                if (!String.IsNullOrEmpty(Dr_Detalles["PAGO_TARJETA"].ToString()))
                    Total_Tarjetas += Double.Parse(Dr_Detalles["PAGO_TARJETA"].ToString());
            }

            Negocio.P_Metodo_Pago = Obtener_Pagos_Orden(Total_Efectivo, Total_Cheque, Total_Tranferencia, Total_Tarjetas, "XML", Dt_Datos.Rows[0]["no_recibo"].ToString());
            Negocio.P_Condiciones = "NO IDENTIFICADO";
            //Negocio.P_No_Factura = Session["No_Factura"].ToString();
            Negocio.P_Fecha_Emision = String.Format("{0:dd/MM/yyyy HH:mm:ss}", DateTime.Now);
            Negocio.P_Fecha_Vencimiento = String.Format("{0:dd/MM/yyyy HH:mm:ss}", DateTime.Now); ;
            Negocio.P_Tipo_Factura = "NORMAL";
            Negocio.P_Tipo_Moneda = "MXN";
            Negocio.P_Tipo_Cambio = "";
            Negocio.P_Comentarios = "";

            // Guarda los totales
            if (Negocio.P_Tipo_Factura.Equals("NORMAL"))
            {
                Negocio.P_Subtotal = Convert.ToString(Subtotal).ToString().Replace("$", "");
                Negocio.P_Subtotal_Cero = "0";
                Negocio.P_Iva = Convert.ToString(Iva).ToString().Replace("$", "");
                Negocio.P_Descuento = "0";
                Negocio.P_Total = Convert.ToString(Total).ToString().Replace("$", "");
            }

            // Datos del cliente.
            Negocio.P_Razon_Social_Facturacion = Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Datos_Clientes.Rows[0]["RAZON_SOCIAL"].ToString());
            Negocio.P_Rfc_Facturacion = Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Datos_Clientes.Rows[0]["RFC"].ToString().ToUpper());
            Negocio.P_Calle_Facturacion = Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Datos_Clientes.Rows[0]["CALLE"].ToString().ToUpper());
            Negocio.P_Numero_Interior_Facturacion = Dt_Datos_Clientes.Rows[0]["NUMERO_INTERIOR"].ToString();
            Negocio.P_Numero_Exterior_Facturacion = Dt_Datos_Clientes.Rows[0]["NUMERO_EXTERIOR"].ToString();
            Negocio.P_Colonia_Facturacion = Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Datos_Clientes.Rows[0]["COLONIA"].ToString().ToUpper());
            Negocio.P_Cp = Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Datos_Clientes.Rows[0]["CP"].ToString());
            Negocio.P_Localidad_Facturacion = Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Datos_Clientes.Rows[0]["LOCALIDAD"].ToString().ToUpper());
            Negocio.P_Ciudad_Facturacion = Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Datos_Clientes.Rows[0]["CIUDAD"].ToString().ToUpper());
            Negocio.P_Estado_Facturacion = Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Datos_Clientes.Rows[0]["NOMBRE_ESTADO"].ToString().ToUpper());
            Negocio.P_Pais_Facturacion = Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Datos_Clientes.Rows[0]["PAIS"].ToString().ToUpper());

            Negocio.Numero_Exterior = Dt_Datos_Clientes.Rows[0]["Numero_Exterior"].ToString().ToUpper();
            Negocio.Numero_Interior = Dt_Datos_Clientes.Rows[0]["Numero_Interior"].ToString().ToUpper();

            Negocio.P_Dias_Credito = "0";
            Negocio.P_Porcentaje_Descuento = "0";

            try
            {
                //var clave = Dt_Datos_Clientes.Rows[0]["uso_cfdi"].ToString();
                var clave = HttpContext.Current.Request["uso_cfdi"].ToString();

                if(string.IsNullOrEmpty(clave.Trim()))
                    Negocio.UsoCFDI = "G03";
                else
                    Negocio.UsoCFDI = clave;
            }
            catch (Exception)
            {
                Negocio.UsoCFDI = "G03";
            }



            // Datos de pago
            Negocio.P_Saldo = Total.ToString();
            Negocio.P_Abono = "0";
            Negocio.P_Pagada = "NO";
            Negocio.P_Estatus = "ACTIVA";
            Negocio.P_Cancelada = "NO";
            


            if (Dt_Parametros_Facturacion != null)
            {
                if (Dt_Parametros_Facturacion.Rows.Count > 0)
                {
                    //Registra los impuestos
                    DataRow Dr_Impuestos = Dt_Impuestos.NewRow();
                    if (Double.Parse((String.IsNullOrEmpty(Negocio.P_Iva) ? "0" : Negocio.P_Iva)) > 0)
                    {
                        Dr_Impuestos["IMPUESTO"] = "IVA";
                        Dr_Impuestos["TASA"] = String.Format("{0:#0.0000}", tasa_iva_general );
                        Dr_Impuestos["IMPORTE"] = Negocio.P_Iva;
                    }
                    else
                    {
                        Dr_Impuestos["IMPUESTO"] = "IVA";
                        Dr_Impuestos["TASA"] = "0";
                        Dr_Impuestos["IMPORTE"] = "0";
                    }
                    Dt_Impuestos.Rows.Add(Dr_Impuestos);
                }
            }

            //Asigna los datos de los parametros de la factura
            //Regimen_Fiscal = Dt_Parametros_Facturacion.Rows[0]["Regimen_Fiscal"].ToString().ToUpper();
            Regimen_Fiscal = Dt_Parametros_Facturacion.Rows[0]["regimen_fical_clave"].ToString().ToUpper();
            //Expedida_En = (Dt_Datos_Fiscales.Rows[0]["Ciudad"].ToString() + ", " + Dt_Datos_Fiscales.Rows[0]["Estado"].ToString()).ToUpper();
            Expedida_En = Dt_Datos_Fiscales.Rows[0]["CP"].ToString();
            Expedida_En = lugar_Expedicion;
            Timbre_Version = "3.3";
            Password_Llave = Cls_Seguridad.Desencriptar(Dt_Parametros_Facturacion.Rows[0]["Password_Llave"].ToString());
            // Complementa la factura.
            Negocio.P_Timbre_Version = Timbre_Version;
            

            //Aqui se inicia proceso de escritura en base de datos
            Cls_Ope_Facturacion_Negocio Rs_Facturas = new Cls_Ope_Facturacion_Negocio();
            TransactionScope ts = new TransactionScope();
            String strsql = "";
            try
            {
                //Transaccion para insertar factura por 1era ves
                using (ts)
                {
                    using (SqlConnection Conexion = new SqlConnection(Cls_Constantes.Str_Conexion))
                    {
                        Conexion.Open();
                        strsql = "set language español";
                        using (SqlCommand Obj_Comando = Conexion.CreateCommand())
                        {
                            Obj_Comando.CommandText = strsql;
                            Obj_Comando.ExecuteNonQuery();
                        }

                        String temp = Rs_Facturas.InsertarFacturaParaObtenerElConsecutivo(Negocio, Conexion).ToString();

                        //Si se insertó factura se timbra
                        if (!String.IsNullOrEmpty(temp))
                        {
                            temp = "0000000000" + temp;                            
                            Negocio.P_No_Factura = temp.Substring(temp.Length - 10, 10);

                            // Ruta de los archivos
                            Ruta_Xml = Dt_Parametros_Facturacion.Rows[0]["Ruta_Xml"].ToString().Replace(@"\", "/") + "CFDI_" + Negocio.P_No_Factura + ".xml";
                            Ruta_Qr = Dt_Parametros_Facturacion.Rows[0]["Ruta_Xml"].ToString().Replace(@"\", "/") + "CFDI_" + Negocio.P_No_Factura + ".png";
                            Ruta_Cert = Dt_Parametros_Facturacion.Rows[0]["Ruta_Certificado"].ToString().Replace(@"\", "/");
                            Ruta_Key = Dt_Parametros_Facturacion.Rows[0]["Ruta_Llave"].ToString().Replace(@"\", "/");

                            Ruta_Externa_Xml = Dt_Parametros_Facturacion.Rows[0]["Ruta_Externa_Xml"].ToString().Replace(@"\", "/");
                            //Ruta_Externa_Xml = @"C:\repositorios\simapag\siac\Facturacion\Facturacion\XML\".Replace(@"\", "/");//
                            Ruta_Externa_Pdf = Dt_Parametros_Facturacion.Rows[0]["Ruta_Externa_Pdf"].ToString().Replace(@"\", "/");
                            //Ruta_Externa_Pdf = @"C:\repositorios\simapag\siac\Facturacion\Facturacion\PDF\".Replace(@"\", "/");//

                            Ruta_Pdf = Dt_Parametros_Facturacion.Rows[0]["Ruta_Pdf"].ToString().Replace(@"\", "/") + "FAC_" + Negocio.P_No_Factura + ".pdf";
                            Ruta_Corta_Pdf = Ruta_Pdf;
                            Negocio.P_No_Certificado = Cls_Timbrado.Consulta_Serie_Certificado(Server.MapPath(Ruta_Cert));
                            Negocio.P_Certificado = Cls_Timbrado.Consulta_Certificado(Server.MapPath(Ruta_Cert));
                            #region armado de xml
                            Cadena_Original = Cls_Timbrado.Genera_Cadena_Original(Negocio, Expedida_En, Regimen_Fiscal, Dt_Datos_Fiscales,Dt_Detalles_Pago, Dt_Impuestos, Conexion);
                            Cadena_UTF = Cls_Timbrado.Valida_Caracteres_UTF(Cadena_Original);
                            Cadena_Sello = Cls_Timbrado.Genera_Sello(Cadena_UTF, Server.MapPath(Ruta_Key), Password_Llave);

                            Negocio.P_Timbre_Sello_Cfd = Cadena_Sello;

                            // Crea el documento XML
                            Xml_Documento = Crea_Xml_CFDI(ref Negocio, Dt_Detalles_Pago, Dt_Datos_Fiscales, Dt_Parametros_Facturacion, Dt_Impuestos, Regimen_Fiscal, Subtotal, Expedida_En, Conexion);
                            // Guarda el documento XML.
                            Xml_Documento.Save(Server.MapPath(Ruta_Xml));
                            //if (!String.IsNullOrEmpty(Ruta_Externa_Xml))
                            //{
                            //    // Guarda el documento XML.
                            //    Xml_Documento.Save(Ruta_Externa_Xml + "CFDI_" + Negocio.P_No_Factura + ".xml");
                            //}
                            Xml_Documento = null;
                            // Genera el codigo bidimensional
                            Codigo_Bidimensional = "?re=" + Dt_Datos_Fiscales.Rows[0]["RFC"].ToString() + "&rr=" + Negocio.P_Rfc_Facturacion
                                + "&tt=" + String.Format("{0:0000000000.000000}", Double.Parse(Negocio.P_Total)) + "&id=" + Negocio.P_Timbre_Uuid;
                            Negocio.P_Ruta_Codigo_Bd = Ruta_Qr;

                            Negocio.P_Cadena_Original = "||" + Negocio.P_Timbre_Version.Trim();
                            Negocio.P_Cadena_Original += "|" + Negocio.P_Timbre_Uuid.Trim();
                            Negocio.P_Cadena_Original += "|" + String.Format("{0:yyyy-MM-dd}", DateTime.Parse(Negocio.P_Timbre_Fecha_Timbrado));
                            Negocio.P_Cadena_Original += "T" + String.Format("{0:HH:mm:ss}", DateTime.Parse(Negocio.P_Timbre_Fecha_Timbrado));
                            Negocio.P_Cadena_Original += "|" + Negocio.P_Timbre_Sello_Cfd.Trim();
                            Negocio.P_Cadena_Original += "|" + Negocio.P_Timbre_No_Certificado_Sat.Trim() + "||";
                            Negocio.P_Cp_Lugar_Expedicion = lugar_Expedicion;

                            Negocio.P_Condiciones = "NO IDENTIFICADO";
                            Negocio.P_Metodo_Pago = Obtener_Pagos_Orden(Total_Efectivo, Total_Cheque, Total_Tranferencia, Total_Tarjetas, "PDF", Dt_Datos.Rows[0]["no_recibo"].ToString(),Conexion);

                            Dt_Detalles_Pago = detallesOriginales;

                    #endregion armado de xml
                        }
                        else
                            return Respuesta("Ocurrio un error al dar de alta factura, no se timbró ingreso. \n Intente de nuevo por favor." + Negocio.P_No_Factura, Ruta_Pdf, Ruta_Corta_Pdf);
                    }
                    ts.Complete();
                }
            }
            catch (Exception e)
            {
                ts.Dispose();

                try
                {
                    string ruta = Server.MapPath(string.Format("../../Facturacion/Facturacion/logXML/{0}.txt", Negocio.No_Folio));
                    if (!File.Exists(ruta))
                    {
                        using (StreamWriter sw = File.CreateText(ruta))
                        {
                            if (!String.IsNullOrEmpty(Negocio.P_No_Factura))
                                sw.WriteLine("Ocurrio un error al actualizar los datos de la factura: " + Negocio.P_No_Factura);
                            if (!String.IsNullOrEmpty(Negocio.P_Timbre_Uuid))
                                sw.WriteLine("Folio fiscal: " + Negocio.P_Timbre_Uuid);
                            sw.WriteLine("Detalles del error[" + e.Message.ToString().Trim() + "]");
                        }
                    }
                }
                catch { }
                if(String.IsNullOrEmpty(Negocio.P_Timbre_Uuid))
                    return Respuesta("Ocurrio un error al dar de alta factura, no se timbró ingreso. \n Intente de nuevo por favor. \n" + e.Message, Ruta_Pdf, Ruta_Corta_Pdf);
                else {
                    
                    return Respuesta("Ocurrio un error al escribir en la base de datos. \n la factura se timbró con el folio.  \n" + Negocio.P_Timbre_Uuid, Ruta_Pdf, Ruta_Corta_Pdf);
                }

            }
            //otra transaccion para actualizar datos a la factura
            try 
            {
                ts = new TransactionScope();
                using (ts)
                {
                    using (SqlConnection Conexion = new SqlConnection(Cls_Constantes.Str_Conexion))
                    {
                        Conexion.Open();
                        #region actualizar factura con los datos timbrados
                        if (Negocio.Alta_Factura(Dt_Detalles_Pago, Conexion))
                        {
                            //Actualiza el estatus de la serie
                            Actualiza_Estatus_Serie(Negocio.P_Serie, Negocio.P_No_Factura, false, Conexion);
                            
                            ts.Complete();
                            factura_generada = true;                                                      
                        }
                        #endregion alta factura
                    }
                }
            }
            catch(Exception ex)
            {
                try
                {
                    string ruta = Server.MapPath(string.Format("../../Facturacion/Facturacion/logXML/{0}.txt", Negocio.No_Folio));
                    if (!File.Exists(ruta))
                    {
                        using (StreamWriter sw = File.CreateText(ruta))
                        {
                            if (!String.IsNullOrEmpty(Negocio.P_No_Factura))
                                sw.WriteLine("Ocurrio un error al actualizar los datos de la factura: " + Negocio.P_No_Factura);
                            if (!String.IsNullOrEmpty(Negocio.P_Timbre_Uuid))
                                sw.WriteLine("Folio fiscal: " + Negocio.P_Timbre_Uuid);
                            sw.WriteLine("Detalles del error[" + ex.Message.ToString().Trim() + "]");
                        }
                    }
                }
                catch { }

                if (!factura_generada)
                {
                    if (!String.IsNullOrEmpty(Negocio.P_Timbre_Uuid))
                    {
                        return Respuesta("Ocurrio un error al actualizar en la base de datos. \n la factura se timbró con el folio. \n" + Negocio.P_Timbre_Uuid, Ruta_Pdf, Ruta_Corta_Pdf);
                    }
                }
            }
            
            
            // Genera y guarda el codigo QR.
            Cls_Timbrado.Generar_Codigo_QR(Codigo_Bidimensional, Server.MapPath(Ruta_Qr), "CFDI_" + Negocio.P_No_Factura + ".png", Ruta_Externa_Xml);

            Ruta_Imagen_Empresa[0] = Server.MapPath(Dt_Parametros_Facturacion.Rows[0]["Imagen_Factura"].ToString().Replace(@"\", "/"));
            Ruta_Imagen_Empresa[1] = Server.MapPath(Dt_Parametros_Facturacion.Rows[0]["Imagen_Factura_2"].ToString().Replace(@"\", "/"));
            Ruta_Imagen_Empresa[2] = Server.MapPath(Dt_Parametros_Facturacion.Rows[0]["Imagen_Factura_3"].ToString().Replace(@"\", "/"));
            Ruta_Imagen_Empresa[3] = Server.MapPath(Dt_Parametros_Facturacion.Rows[0]["Imagen_Factura_4"].ToString().Replace(@"\", "/"));

            // Crea y guarda el PDF
            Tipo_Comprobante = "INGRESO";
            Regimen_Fiscal = "Régimen Fiscal " + Dt_Parametros_Facturacion.Rows[0]["regimen_fical_clave"].ToString() + " " + Dt_Parametros_Facturacion.Rows[0]["regimen_fiscal_descripcion"].ToString().ToUpper();
            Dt_registro = Negocio.Consultar_Factura();
            Ds_Factura_Pdf = Crea_Pdf(Negocio, Regimen_Fiscal, Ruta_Imagen_Empresa, Tipo_Comprobante, Dt_Impuestos, Dt_Datos_Fiscales, Dt_Detalles_Pago, lugar_Expedicion);

            Cls_Factura_Pdf Factura_Pdf = Crea_Obj_Pdf(Negocio, Regimen_Fiscal, Ruta_Imagen_Empresa, Tipo_Comprobante, Dt_Parametros_Facturacion, Dt_Impuestos, Dt_Datos_Fiscales, Dt_Detalles_Pago, new DataTable(), null);
            Factura_Pdf.Lugar_Expedicion = Expedida_En;
            Factura_Pdf.RPU = Dt_registro.Rows[0]["rpu"].ToString();
            Factura_Pdf.Periodo_Pago = Dt_registro.Rows[0]["periodo_pago"].ToString();
            Factura_Pdf.Tipo_Servicio = Negocio.P_Tipo_Servicio.Trim();
            Factura_Pdf.Codigo_Barras = Dt_registro.Rows[0]["folio_pago"].ToString();
            Factura_Pdf.No_Recibo = Dt_Datos.Rows[0]["no_recibo"].ToString();
            //Factura_Pdf.SubTotal = Negocio.P_SubTotal_No_Descuentos;
            Factura_Pdf.Descuento = String.Format("{0:#0.00}", Negocio.P_Total_Descuento);
            Factura_Pdf.Uso_Cfdi = Negocio.UsoCFDI;
            Cls_Factura_Individual_Pdf Obj_Reporte_Factura = new Cls_Factura_Individual_Pdf(Server.MapPath(Ruta_Pdf), Factura_Pdf);
            Obj_Reporte_Factura.Iniciar_Reporte();
            File.Copy(Server.MapPath(Ruta_Pdf), Path.Combine(Ruta_Externa_Pdf, "FAC_" + Negocio.P_No_Factura + ".pdf"));

            //Cls_Metodos_Generales.Generar_Reporte(ref Ds_Factura_Pdf, "FAC_" + Negocio.P_No_Factura + ".pdf", "../Rpt/Facturacion/", "Rpt_Ope_Facturacion_Electronica.rpt", Ruta_Pdf, "Portal de Facturación OPDAPAS", this, this.GetType(), Ruta_Externa_Pdf);

            //Envia el correo cliente                        
            if (!String.IsNullOrEmpty(Dt_Parametros_Facturacion.Rows[0]["Envio_Automatico_Correo"].ToString()))
            {
                // Forma el cuerpo del mensaje
                Mensaje = "<font face='arial' size='4'> " +
                    "<b>Envío de Factura Autom&aacute;tico</b>" +
                    "</font>" +
                    "<font face='arial' size='3'>" +
                    "<p> " + Dt_Datos_Fiscales.Rows[0]["Nombre_Corto"] + " le agradece su pago de los servgicios que le proporcionamos y le anexa la Factura Electrónica solicitada con FOLIO "
                    + Negocio.P_No_Factura + ". </p>" +
                    "</font>";
                Ruta_Xml = Server.MapPath(Ruta_Xml);
                Ruta_Pdf = Server.MapPath(Ruta_Pdf);

                Envia = Dt_Parametros_Facturacion.Rows[0]["Envio_Automatico_Correo"].ToString();
                if (Envia.Equals("SI"))
                {
                    try
                    {
                        Cls_Enviar_Correo.Enviar_Correo_Archivos(new String[] { Ruta_Pdf, Ruta_Xml }, Mensaje, HttpContext.Current.Request["Email"], Dt_Parametros_Facturacion.Rows[0]["Email_Origen_Correos"].ToString(), "Envío de Factura Electrónica");

                        Envio_Correo = true;
                    }
                    catch (Exception ExC)
                    {
                        Mensaje_Error_Correo = ExC.Message;
                        Envio_Correo = false;
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            if (Ex.Message.Equals("Factura_Generada"))
            {
                //Negocio.P_No_Factura = (String)Session["No_Factura_Correo"];
                Negocio.P_No_Factura = HttpContext.Current.Request["No_Factura_Correo"];
                Negocio.P_Email_Facturacion = HttpContext.Current.Request["Email"];
                if(!(Negocio.P_No_Factura == "undefined")) Negocio.Modificar_Email();

                bool Reenvio_Correo = Enviar_Correo(HttpContext.Current.Request["Email"], Dt_Factura.Rows[0][Ope_Facturas.Campo_No_Factura].ToString(), out Ruta_Pdf_A_Enviar, out Ruta_Corta_Pdf);
                if (Reenvio_Correo)
                    return Respuesta("La Factura se reenvio al correo " + HttpContext.Current.Request["Email"], Ruta_Pdf_A_Enviar, Ruta_Corta_Pdf);
                else
                    return Respuesta("La Factura no se reenvio al correo " + HttpContext.Current.Request["Email"], "", "");

            }
            
            return Respuesta(Ex.Message.ToString());
        }
        if (Envia.Equals("SI"))
        {
            if (Envio_Correo)
            {
                return Respuesta("Se dio de alta correctamente la Factura No " + Negocio.P_No_Factura + " y se envió al correo " + Negocio.P_Email_Facturacion, Ruta_Pdf, Ruta_Corta_Pdf);
            }
            else
            {
                return Respuesta("Se dio de alta correctamente la Factura No " + Negocio.P_No_Factura + " y No se pudo enviar el correo . Error: " + Mensaje_Error_Correo, Ruta_Pdf, Ruta_Corta_Pdf);
            }
        }
        else
        {
            return Respuesta("Se dio de alta correctamente la Factura No " + Negocio.P_No_Factura, Ruta_Pdf, Ruta_Corta_Pdf);
        }
    }

    //public void Calcular_Iva_Sat(Cls_Ope_Facturacion_Negocio Rs_Factura, DataTable Dt_Factura_Detalles, Double Iva_General)
    //{
    //    bool seRedondeoUnIva = false;
    //    decimal ivaDefinitivo = 0;

    //    foreach (DataRow Fila_Tabla in Dt_Factura_Detalles.Rows)
    //    {

    //        if (Rs_Factura.Verificar_Importe_Iva(Fila_Tabla["Clave_Concepto"].ToString().Trim()))
    //        {
    //            double iva;
    //            var importe = Convert.ToDouble(Fila_Tabla["ivaCorrecto"]);

    //            var baseImporte = Convert.ToDouble(Fila_Tabla["Total"].ToString());
    //            var tasa = Iva_General / 100;

    //            var limiteInferior = (baseImporte - Math.Pow(10, -2) / 2) * tasa;
    //            var limiteTruncado = Math.Truncate(limiteInferior * 100) / 100;

    //            var limiteSuperior = (baseImporte - Math.Pow(10, -2) / 2 - Math.Pow(10, -12)) * tasa;
    //            var limiteSuperiorRedondeaco = Math.Round(limiteSuperior, 2);

    //            if (!seRedondeoUnIva)
    //            {
    //                var iva1 = Math.Truncate(importe * 100) / 100;
    //                var iva2 = Convert.ToDouble(String.Format("{0:#0.00}", importe));


    //                if (iva1 != iva2)
    //                {
    //                    seRedondeoUnIva = true;
    //                    importe = iva2;
    //                }
    //                else
    //                    importe = Math.Truncate(importe * 100) / 100;
    //            }
    //            else
    //                importe = Math.Truncate(importe * 100) / 100;

    //            if (importe >= limiteTruncado && importe <= limiteSuperiorRedondeaco)
    //                iva = importe;
    //            else if (Math.Round(Math.Abs(importe - limiteTruncado), 2) == 0.01 || Math.Round(Math.Abs(importe - limiteSuperiorRedondeaco), 2) == 0.01)
    //                iva = importe;
    //            else
    //            {
    //                //iva = Math.Truncate(limiteInferior * 100) / 100;
    //                //seRedondeoUnIva = false;
    //                if (!seRedondeoUnIva & limiteTruncado != limiteSuperiorRedondeaco)
    //                {
    //                    iva = limiteSuperiorRedondeaco;
    //                    seRedondeoUnIva = true;
    //                }
    //                else
    //                    iva = limiteTruncado;
    //            }
    //            ivaDefinitivo += Convert.ToDecimal(iva);

    //        }
    //    }

    //    Rs_Factura.P_Iva = ivaDefinitivo.ToString();
    //}


    private void Ajustar_Redondeos(DataTable detalles, double subtotal, double iva)
    {
        Decimal subtotalReal = 0m;

        foreach (DataRow fila in detalles.Rows)
        {
            Decimal valor = Convert.ToDecimal(fila["importe"]) + Convert.ToDecimal(fila["ivaCorrecto"]);
            subtotalReal += Math.Round(valor, 2);
        }

        decimal diferencia = (decimal)subtotal - subtotalReal;

        if(diferencia != 0)
        {
            foreach (DataRow fila in detalles.Rows)
            {
                if(fila["clave_concepto"].ToString() == "00013" || fila["clave_concepto"].ToString() == "00155")
                {
                    decimal valorAnterior = Convert.ToDecimal(fila["importe"]);
                    fila["total"] = valorAnterior + diferencia;
                    fila["importe"] = valorAnterior + diferencia;
                    diferencia = 0;
                    break;
                }
            }

            if(diferencia != 0)
            {
                DataRow nuevaFila = detalles.NewRow();
                nuevaFila["no_cuenta"] = detalles.Rows[0]["no_cuenta"].ToString();
                nuevaFila["clave_concepto"] = "00013";
                nuevaFila["Cantidad"] = "1";
                nuevaFila["descripcion"] = "REDONDEO ACTUAL";
                nuevaFila["valor_unitario"] = diferencia.ToString();
                nuevaFila["importe"] = diferencia.ToString();
                nuevaFila["iva"] = detalles.Rows[0]["iva"].ToString();
                nuevaFila["total"] = diferencia.ToString();
                nuevaFila["FECHA_MOVIMIENTO"] = detalles.Rows[0]["FECHA_MOVIMIENTO"].ToString();
                nuevaFila["Unidad_de_Medida"] = detalles.Rows[0]["Unidad_de_Medida"].ToString();
                nuevaFila["iva"] = "0";
                nuevaFila["ivaCorrecto"] = "0";
                nuevaFila["Tasa"] = "0.0000";
                nuevaFila["Descuento"] = "0";
                detalles.Rows.Add(nuevaFila);
            }
        }
    }



    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Enviar_Correo
    ///DESCRIPCIÓN: Envia el correo con la reimpresion de la factura.
    ///PARAMETROS:  Correo: correo al que se envia el documento.
    ///             Factura: direccion de la factura en el servidor
    ///CREO:        Ramón Baeza Yépez
    ///FECHA_CREO:  30/Diciembre/2013
    ///*******************************************************************************
    private Boolean Enviar_Correo(String Correo, String No_Factura, out String pRuta_PDF, out String pRuta_Corta)
    {
        Cls_Apl_Parametros_Facturacion_Negocio Rs_Parametros_Facturacion = new Cls_Apl_Parametros_Facturacion_Negocio();
        Cls_Ope_Facturacion_Negocio Negocio = new Cls_Ope_Facturacion_Negocio();
        String Mensaje;
        Boolean Envio_Correo = false;
        String Ruta_Pdf = String.Empty;
        String Ruta_Xml = String.Empty;
        String Envia = String.Empty;
        String Mensaje_Error_Correo = String.Empty;
        DataTable Dt_Parametros_Facturacion;
        DataTable Dt_Datos_Fiscales;
        pRuta_PDF = "";
        pRuta_Corta = "";

        try
        {
            Dt_Parametros_Facturacion = Rs_Parametros_Facturacion.Consultar_Parametros();
            Dt_Datos_Fiscales = Negocio.Consultar_Datos_Fiscales();
            //Envia el correo cliente                        
            if (!String.IsNullOrEmpty(Dt_Parametros_Facturacion.Rows[0]["Envio_Automatico_Correo"].ToString()))
            {
                // Forma el cuerpo del mensaje
                Mensaje = "<font face='arial' size='4'> " +
                    "<b>Reenvio de Factura </b>" +
                    "</font>" +
                    "<font face='arial' size='3'>" +
                    "<p> " + Dt_Datos_Fiscales.Rows[0]["Nombre_Corto"] + " le agradece su pago de los servicios que le proporcionamos y le anexa la Factura Electrónica solicitada con FOLIO "
                    + No_Factura + ". </p>" +
                    "</font>";
                Ruta_Xml = Server.MapPath("../../Facturacion/Facturacion/XML/CFDI_" + No_Factura + ".xml");
                Ruta_Pdf = Server.MapPath("../../Facturacion/Facturacion/PDF/FAC_" + No_Factura + ".pdf");

                pRuta_PDF = Server.MapPath("../../Facturacion/Facturacion/PDF/FAC_" + No_Factura + ".pdf");
                pRuta_Corta = "../../Facturacion/Facturacion/PDF/FAC_" + No_Factura + ".pdf";

                Envia = Dt_Parametros_Facturacion.Rows[0]["Envio_Automatico_Correo"].ToString();
                if (Envia.Equals("SI"))
                {
                    try
                    {
                        Cls_Enviar_Correo.Enviar_Correo_Archivos(new String[] { Ruta_Pdf, Ruta_Xml }, Mensaje, Correo, Dt_Parametros_Facturacion.Rows[0]["Email_Origen_Correos"].ToString(), "Reenvio de Factura Electrónica");
                        Envio_Correo = true;
                    }
                    catch (Exception ExC)
                    {
                        Mensaje_Error_Correo = ExC.Message;
                        Envio_Correo = false;
                    }
                }
            }
        }
        catch (Exception Ex) { }
        return Envio_Correo;
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Crea_Xml_CFDI
    ///DESCRIPCIÓN: Crea el archivo Xml de la factura.
    ///PARAMETROS:  Rs_Facturas, 
    ///CREO:        Luis Alberto Salas Garcia
    ///FECHA_CREO:  23/Mayo/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private XDocument Crea_Xml_CFDI(ref Cls_Ope_Facturacion_Negocio Rs_Facturas, DataTable Dt_Factura_Detalles, DataTable Dt_Datos_Fiscales,
        DataTable Dt_Parametros_Facturacion, DataTable Dt_Impuestos, String Regimen_Fiscal, Double SubTotal, string lugar_expedicion, SqlConnection objConexion)
    {
        // Declaracion de Documento XML
        XDocument Xml_Documento = new XDocument(new XDeclaration("1.0", "utf-8", "yes"));

        // Nodo donde se almacena la respuesta
        XmlDocument Xml_Respuesta = new XmlDocument();
        XmlNode Nodo_Respuesta;
        XElement Nodo_Timbrado;

        // Declaracion de los nodos del xml
        XElement Nodo_Raiz;
        XElement Nodo_Emisor;
        XElement Nodo_Emisor_Domicilio;
        XElement Nodo_Regimen_Fiscal;
        XElement Nodo_Receptor;
        XElement Nodo_Receptor_Domicilio;
        XElement Nodo_Conceptos;
        XElement Nodo_Cuenta_Predial;
        XElement Nodo_Concepto;
        XElement Nodo_Impuestos;
        XElement Nodo_Retenciones;
        XElement Nodo_Traslados;
        XElement Nodo_Complemento;

        // Variables del timbre
        String Xml = String.Empty;
        String Timbrado_VersionSat = String.Empty;
        String Timbrado_UUID = String.Empty;
        String Timbrado_FechaTimbrado = String.Empty;
        String Timbrado_selloCFD = String.Empty;
        String Timbrado_noCertificadoSAT = String.Empty;
        String Timbrado_selloSAT = String.Empty;
        String Timbrado_Ambiente = String.Empty;
        String Respuesta = String.Empty;
        String Codigo_Bidimensional = String.Empty;

        // Parametros de timbrado
        String Codigo_Usuario = String.Empty;
        String Codigo_Proveedor = String.Empty;
        String Sucursal_Id = String.Empty;
        String Version_Timbrado = String.Empty;

        // Variables para Impuestos
        Double Impuestos_Retenidos = 0;
        Double Impuestos_Trasladados = 0;
        Double Impuestos_Locales;
        Double Sumatoria_Iva_Recalculado = 0;
        // Fecha Timbrado
        DateTime Fecha_Timbrado;

        // Declaracion de NameSpace
        XNamespace cfdi = "http://www.sat.gob.mx/cfd/3";
        XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
        XNamespace implocal = "http://www.sat.gob.mx/implocal";
        XNamespace tfd = "http://www.sat.gob.mx/TimbreFiscalDigital";

        //DataTable Dt_Parametros_Facturacion = new DataTable();
        try
        {
            DateTime Fecha = DateTime.Now.AddMinutes(-10);
            //DateTime Fecha = DateTime.Now;

            // Crea Nodo Raiz
            Nodo_Raiz = new XElement(cfdi + "Comprobante");
            if (Rs_Facturas.P_Tipo_Factura.Equals("ARRENDAMIENTOS") || Rs_Facturas.P_Tipo_Factura.Equals("HONORARIOS"))
            {
                Nodo_Raiz.SetAttributeValue(xsi + "schemaLocation", "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd http://www.sat.gob.mx/implocal http://www.sat.gob.mx/sitio_internet/cfd/implocal/implocal.xsd");
                Nodo_Raiz.SetAttributeValue(XNamespace.Xmlns + "implocal", "http://www.sat.gob.mx/implocal");
            }
            else
            {
                Nodo_Raiz.SetAttributeValue(xsi + "schemaLocation", "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd");
            }
            Nodo_Raiz.SetAttributeValue(XNamespace.Xmlns + "xsi", "http://www.w3.org/2001/XMLSchema-instance");
            Nodo_Raiz.SetAttributeValue(XNamespace.Xmlns + "cfdi", "http://www.sat.gob.mx/cfd/3");
            Nodo_Raiz.SetAttributeValue("Version", "3.3");
            if (!String.IsNullOrEmpty(Rs_Facturas.P_Serie))
            {
                Nodo_Raiz.SetAttributeValue("Serie", Rs_Facturas.P_Serie);
            }
            Nodo_Raiz.SetAttributeValue("Folio", Convert.ToInt32(Rs_Facturas.P_No_Factura).ToString());
            Nodo_Raiz.SetAttributeValue("Fecha", String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(Rs_Facturas.P_Fecha_Creo_Xml)) + "T" + String.Format("{0:HH:mm:ss}", Convert.ToDateTime(Rs_Facturas.P_Fecha_Creo_Xml)));

            Nodo_Raiz.SetAttributeValue("Sello", Rs_Facturas.P_Timbre_Sello_Cfd);
            Nodo_Raiz.SetAttributeValue("NoCertificado", Rs_Facturas.P_No_Certificado);
            Nodo_Raiz.SetAttributeValue("Certificado", Rs_Facturas.P_Certificado);

            Nodo_Raiz.SetAttributeValue("CondicionesDePago", Rs_Facturas.P_Condiciones);
            //Nodo_Raiz.SetAttributeValue("SubTotal", String.Format("{0:#0.00}", Convert.ToDouble(Rs_Facturas.P_Subtotal)));
            Nodo_Raiz.SetAttributeValue("SubTotal", Rs_Facturas.P_SubTotal_No_Descuentos);
            Nodo_Raiz.SetAttributeValue("Total", String.Format("{0:#0.00}", Convert.ToDouble(Rs_Facturas.P_Total)));
            Nodo_Raiz.SetAttributeValue("LugarExpedicion", lugar_expedicion);
            if (Rs_Facturas.P_Tipo_Moneda.Equals("MXN"))
            {
                Nodo_Raiz.SetAttributeValue("Moneda", Rs_Facturas.P_Tipo_Moneda);
            }
            else
            {
                Nodo_Raiz.SetAttributeValue("TipoCambio", String.Format("{0:#0.00}", Convert.ToDouble(Rs_Facturas.P_Tipo_Cambio)));
                Nodo_Raiz.SetAttributeValue("Moneda", Rs_Facturas.P_Tipo_Moneda);
            }
            Nodo_Raiz.SetAttributeValue("TipoDeComprobante", Rs_Facturas.P_Tipo_Comprobante);


            //if (Double.Parse((String.IsNullOrEmpty(Rs_Facturas.P_Descuento) ? "0" : Rs_Facturas.P_Descuento)) > 0)
            //{
            //    Nodo_Raiz.SetAttributeValue("descuento", String.Format("{0:#0.00}", Convert.ToDouble(Rs_Facturas.P_Descuento)));
            //}

            //Nodo_Raiz.SetAttributeValue("metodoDePago", Rs_Facturas.P_Metodo_Pago);
            Nodo_Raiz.SetAttributeValue("FormaPago", Rs_Facturas.P_Metodo_Pago);

            if (!String.IsNullOrEmpty(Rs_Facturas.P_No_Cuenta_Pago))
            {
                Nodo_Raiz.SetAttributeValue("NumCtaPago", Rs_Facturas.P_No_Cuenta_Pago);
            }

            //Nodo_Raiz.SetAttributeValue("formaDePago", Rs_Facturas.P_Forma_Pago);
            Nodo_Raiz.SetAttributeValue("MetodoPago", Rs_Facturas.P_Forma_Pago);

            if (Rs_Facturas.P_Total_Descuento > 0)
                Nodo_Raiz.SetAttributeValue("Descuento", String.Format("{0:#0.00}", Convert.ToDouble(Rs_Facturas.P_Total_Descuento)));


            // Crea Nodo Emisor        
            if (Dt_Datos_Fiscales != null)
            {
                Nodo_Emisor = new XElement(cfdi + "Emisor");
                Timbrado_Ambiente = Dt_Parametros_Facturacion.Rows[0]["Ambiente_Timbrado"].ToString();
                if (Timbrado_Ambiente == "PRUEBA")
                    Nodo_Emisor.SetAttributeValue("Rfc", "AAA010101AAA");
                else
                Nodo_Emisor.SetAttributeValue("Rfc", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Datos_Fiscales.Rows[0]["RFC"].ToString()));
                if (!String.IsNullOrEmpty(Dt_Datos_Fiscales.Rows[0]["Razon_Social"].ToString()))
                {
                    Nodo_Emisor.SetAttributeValue("Nombre", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Datos_Fiscales.Rows[0]["Razon_Social"].ToString()));
                }

                Nodo_Emisor.SetAttributeValue("RegimenFiscal", Dt_Parametros_Facturacion.Rows[0]["regimen_fical_clave"].ToString());

                // Crea nodo de domicilio fiscal
                Nodo_Emisor_Domicilio = new XElement(cfdi + "DomicilioFiscal");
                Nodo_Emisor_Domicilio.SetAttributeValue("Calle", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Datos_Fiscales.Rows[0]["Calle"].ToString()));
                if (!String.IsNullOrEmpty(Dt_Datos_Fiscales.Rows[0]["Numero_Exterior"].ToString()))
                {
                    Nodo_Emisor_Domicilio.SetAttributeValue("NoExterior", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Datos_Fiscales.Rows[0]["Numero_Exterior"].ToString()));
                }
                if (!String.IsNullOrEmpty(Dt_Datos_Fiscales.Rows[0]["Numero_Interior"].ToString()))
                {
                    Nodo_Emisor_Domicilio.SetAttributeValue("NoInterior", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Datos_Fiscales.Rows[0]["Numero_Interior"].ToString()));
                }
                if (!String.IsNullOrEmpty(Dt_Datos_Fiscales.Rows[0]["Colonia"].ToString()))
                {
                    Nodo_Emisor_Domicilio.SetAttributeValue("Colonia", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Datos_Fiscales.Rows[0]["Colonia"].ToString()));
                }
                if (!String.IsNullOrEmpty(Dt_Datos_Fiscales.Rows[0]["Localidad"].ToString()))
                {
                    Nodo_Emisor_Domicilio.SetAttributeValue("Localidad", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Datos_Fiscales.Rows[0]["Localidad"].ToString()));
                }
                Nodo_Emisor_Domicilio.SetAttributeValue("Municipio", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Datos_Fiscales.Rows[0]["Ciudad"].ToString()));
                Nodo_Emisor_Domicilio.SetAttributeValue("Estado", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Datos_Fiscales.Rows[0]["Estado"].ToString()));
                Nodo_Emisor_Domicilio.SetAttributeValue("Pais", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Datos_Fiscales.Rows[0]["Pais"].ToString()));
                Nodo_Emisor_Domicilio.SetAttributeValue("CodigoPostal", Dt_Datos_Fiscales.Rows[0]["CP"].ToString());

                //Nodo_Emisor.Add(Nodo_Emisor_Domicilio);

                //// Crea nodo de regimen fiscal
                //Nodo_Regimen_Fiscal = new XElement(cfdi + "RegimenFiscal");
                //Nodo_Regimen_Fiscal.SetAttributeValue("Regimen", Regimen_Fiscal);
                //Nodo_Emisor.Add(Nodo_Regimen_Fiscal);

                Nodo_Raiz.Add(Nodo_Emisor);
            }

            // Crea nodo receptor
            Nodo_Receptor = new XElement(cfdi + "Receptor");
            if (!String.IsNullOrEmpty(Rs_Facturas.P_Rfc_Facturacion))
            {
                Nodo_Receptor.SetAttributeValue("Rfc", Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Facturas.P_Rfc_Facturacion));
            }
            if (!String.IsNullOrEmpty(Rs_Facturas.P_Razon_Social_Facturacion))
            {
                Nodo_Receptor.SetAttributeValue("Nombre", Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Facturas.P_Razon_Social_Facturacion));
            }
            if (!String.IsNullOrEmpty(Rs_Facturas.UsoCFDI))
            {
                Nodo_Receptor.SetAttributeValue("UsoCFDI", Rs_Facturas.UsoCFDI);
            }

            // Crea nodo de domicilio del receptor
            Nodo_Receptor_Domicilio = new XElement(cfdi + "Domicilio");
            Nodo_Receptor_Domicilio.SetAttributeValue("Calle", Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Facturas.P_Calle_Facturacion));
            if (!String.IsNullOrEmpty(Dt_Datos_Fiscales.Rows[0]["Numero_Exterior"].ToString()))
            {
                Nodo_Receptor_Domicilio.SetAttributeValue("NoExterior", Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Facturas.P_Numero_Exterior_Facturacion));
            }
            if (!String.IsNullOrEmpty(Rs_Facturas.P_Numero_Interior_Facturacion))
            {
                Nodo_Receptor_Domicilio.SetAttributeValue("NoInterior", Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Facturas.P_Numero_Interior_Facturacion));
            }
            if (!String.IsNullOrEmpty(Rs_Facturas.P_Colonia_Facturacion))
            {
                Nodo_Receptor_Domicilio.SetAttributeValue("Colonia", Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Facturas.P_Colonia_Facturacion));
            }

            Nodo_Receptor_Domicilio.SetAttributeValue("Municipio", Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Facturas.P_Ciudad_Facturacion));
            Nodo_Receptor_Domicilio.SetAttributeValue("Estado", Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Facturas.P_Estado_Facturacion));
            Nodo_Receptor_Domicilio.SetAttributeValue("Pais", Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Facturas.P_Pais_Facturacion));
            Nodo_Receptor_Domicilio.SetAttributeValue("CodigoPostal", Rs_Facturas.P_Cp);

            //Nodo_Receptor.Add(Nodo_Receptor_Domicilio);
            Nodo_Raiz.Add(Nodo_Receptor);

            bool seRedondeoUnIva = false;

            // Crea nodo Conceptos
            Nodo_Conceptos = new XElement(cfdi + "Conceptos");
            Sumatoria_Iva_Recalculado = 0;
            foreach (DataRow Fila_Tabla in Dt_Factura_Detalles.Rows)
            {
                // Valida que el concepto tenga cantidad y total
                if (Math.Round( Convert.ToDouble(Fila_Tabla["Importe"]),2 ) > 0)
                {
                    Nodo_Concepto = new XElement(cfdi + "Concepto");
                    Nodo_Concepto.SetAttributeValue("Cantidad", String.Format("{0:#0.00}", Convert.ToDouble(Fila_Tabla["Cantidad"].ToString())));
                    Nodo_Concepto.SetAttributeValue("Unidad", Fila_Tabla["Unidad_de_Medida"].ToString());
                    if (!String.IsNullOrEmpty(Fila_Tabla["Clave_Concepto"].ToString()))
                    {
                        Nodo_Concepto.SetAttributeValue("NoIdentificacion", Fila_Tabla["Clave_Concepto"].ToString());
                    }
                    Nodo_Concepto.SetAttributeValue("Descripcion", Cls_Metodos_Generales.CFD_Elimina_Espacios(Fila_Tabla["Descripcion"].ToString().Trim()));
                    Nodo_Concepto.SetAttributeValue("ValorUnitario", String.Format("{0:#0.00}", Convert.ToDouble(Fila_Tabla["Importe"].ToString())));
                    Nodo_Concepto.SetAttributeValue("Importe", String.Format("{0:#0.00}", Convert.ToDouble(Fila_Tabla["Importe"].ToString())));
                    //Nodo_Concepto.SetAttributeValue("ClaveProdServ", "01010101");
                    Nodo_Concepto.SetAttributeValue("ClaveProdServ", Fila_Tabla["concepto_sat"].ToString());
                    //Nodo_Concepto.SetAttributeValue("ClaveUnidad", "E48");
                    Nodo_Concepto.SetAttributeValue("ClaveUnidad", Fila_Tabla["unidad_sat"].ToString());

                    if (Convert.ToDouble(Fila_Tabla["descuento"].ToString()) > 0)
                        Nodo_Concepto.SetAttributeValue("Descuento", String.Format("{0:#0.00}", Convert.ToDouble(Fila_Tabla["descuento"].ToString())));
                    //Nodo_Concepto.SetAttributeValue("Descuento", Fila_Tabla["descuento"].ToString());

                    //if (Convert.ToDouble(Fila_Tabla["iva"].ToString()) > 0)
                    if (Rs_Facturas.Verificar_Importe_Iva(Fila_Tabla["Clave_Concepto"].ToString().Trim(),objConexion))
                    {
                        var nodoImpuestos = new XElement(cfdi + "Impuestos");
                        var nodoTraslados = new XElement(cfdi + "Traslados");
                        var nodoTraslado = new XElement(cfdi + "Traslado");

                        var baseImporte = Math.Round(Convert.ToDouble(Fila_Tabla["importe"].ToString()),4);
                        var tasa = Double.Parse(Fila_Tabla["tasa"].ToString());
                        nodoTraslado.SetAttributeValue("Base", String.Format("{0:#0.00}",baseImporte));
                        nodoTraslado.SetAttributeValue("Impuesto", "002");
                        nodoTraslado.SetAttributeValue("TipoFactor", "Tasa");
                        nodoTraslado.SetAttributeValue("TasaOCuota", String.Format("{0:#0.000000}", tasa));

                        double iva;
                        var importe = Convert.ToDouble(Fila_Tabla["ivaCorrecto"]);


                        //var limiteInferior = (baseImporte - Math.Pow(10, -2) / 2) * tasa;
                        //var limiteTruncado = Math.Truncate(limiteInferior * 100) / 100;

                        //var limiteSuperior = (baseImporte - Math.Pow(10, -2) / 2 - Math.Pow(10, -12)) * tasa;
                        //var limiteSuperiorRedondeaco = Math.Round(limiteSuperior, 2);

                        if(!seRedondeoUnIva)
                        {
                            var iva1 = Math.Truncate(importe * 100) / 100;
                            var iva2 = Convert.ToDouble(String.Format("{0:#0.00}", importe));


                            if (iva1 != iva2)
                            {
                                seRedondeoUnIva = true;
                                importe = iva2;
                            } else
                                importe = Math.Truncate(importe * 100) / 100;
                        } else
                            importe = Math.Truncate(importe * 100) / 100;



                        //if (importe >= limiteTruncado && importe <= limiteSuperiorRedondeaco)
                        //    iva = importe;
                        //else if (Math.Round(Math.Abs(importe - limiteTruncado), 2) == 0.01 || Math.Round(Math.Abs(importe - limiteSuperiorRedondeaco), 2) == 0.01)
                        //    iva = importe;
                        //else
                        //{
                        //    if (!seRedondeoUnIva & limiteTruncado != limiteSuperiorRedondeaco)
                        //    {
                        //        iva = limiteSuperiorRedondeaco;
                        //        seRedondeoUnIva = true;
                        //    }
                        //    else
                        //        iva = limiteTruncado;
                        //}

                        iva =  Math.Round(baseImporte,2) * tasa;
                        iva = Math.Round(iva,4);
                        //calcular limite inferior
                        //nodoTraslado.SetAttributeValue("Importe", Fila_Tabla["iva"].ToString());

                        //if(seRedondeoUnIva)
                        //    nodoTraslado.SetAttributeValue("Importe", String.Format("{0:#0.00}", iva));
                        //else
                        //{
                        //    var ivaTmp = Math.Truncate(iva * 100) / 100;
                        //    nodoTraslado.SetAttributeValue("Importe", String.Format("{0:#0.00}", ivaTmp));
                        //}
                        //else
                        //{
                        
                        nodoTraslado.SetAttributeValue("Importe", String.Format("{0:#0.0000}", Math.Round(iva, 4)));
                        //}



                        Sumatoria_Iva_Recalculado += iva;
                        nodoTraslados.Add(nodoTraslado);
                        nodoImpuestos.Add(nodoTraslados);
                        Nodo_Concepto.Add(nodoImpuestos);
                    }

                    Nodo_Conceptos.Add(Nodo_Concepto);
                }
            }

            Nodo_Raiz.Add(Nodo_Conceptos);

            // Crea nodo impuestos
            Nodo_Impuestos = new XElement(cfdi + "Impuestos");
            Impuestos_Retenidos = 0;
            Impuestos_Trasladados = 0;

            if (Impuestos_Retenidos > 0)
            {
                Nodo_Impuestos.SetAttributeValue("totalImpuestosRetenidos", String.Format("{0:#0.00}", Impuestos_Retenidos));
                //Nodo_Impuestos.SetAttributeValue("totalImpuestosTrasladados", String.Format("{0:#0.00}", Impuestos_Trasladados));
                Nodo_Impuestos.SetAttributeValue("totalImpuestosTrasladados", String.Format("{0:#0.00}", Sumatoria_Iva_Recalculado));

                // Crea nodo retenciones
                Nodo_Retenciones = new XElement(cfdi + "Retenciones");

                // Crea un nodo por cada retencion
                XElement Nodo_Retencion = new XElement(cfdi + "Retencion");
                Nodo_Retencion.SetAttributeValue("impuesto", 0);
                Nodo_Retencion.SetAttributeValue("importe", String.Format("{0:#0.00}", Double.Parse("0")));

                Nodo_Retenciones.Add(Nodo_Retencion);

                Nodo_Impuestos.Add(Nodo_Retenciones);
            }
            else
            {
                if (Dt_Impuestos.Rows.Count > 0 && Double.Parse(Dt_Impuestos.Rows[0]["IMPORTE"].ToString()) > 0)
                {
                    foreach (DataRow Fila_Tabla in Dt_Impuestos.Rows)
                    {
                        //Nodo_Impuestos.SetAttributeValue("TotalImpuestosTrasladados", String.Format("{0:#0.00}", Double.Parse(Fila_Tabla["IMPORTE"].ToString())));
                        Nodo_Impuestos.SetAttributeValue("TotalImpuestosTrasladados", String.Format("{0:#0.00}", Sumatoria_Iva_Recalculado));

                        Nodo_Traslados = new XElement(cfdi + "Traslados");

                        // Crea nodo de traslado por cada impuesto
                        XElement Nodo_Traslado = new XElement(cfdi + "Traslado");
                        //Nodo_Traslado.SetAttributeValue("Impuesto", Fila_Tabla["IMPUESTO"].ToString());
                        Nodo_Traslado.SetAttributeValue("Impuesto", "002");
                        Nodo_Traslado.SetAttributeValue("TipoFactor", "Tasa");
                        Nodo_Traslado.SetAttributeValue("TasaOCuota", String.Format("{0:#0.000000}", Double.Parse(Fila_Tabla["TASA"].ToString()) ));
                        //Nodo_Traslado.SetAttributeValue("Importe", String.Format("{0:#0.00}", Double.Parse(Fila_Tabla["IMPORTE"].ToString())));
                        Nodo_Traslado.SetAttributeValue("Importe", String.Format("{0:#0.00}", Sumatoria_Iva_Recalculado));

                        Nodo_Traslados.Add(Nodo_Traslado);
                        Nodo_Impuestos.Add(Nodo_Traslados);
                    }
                    Nodo_Raiz.Add(Nodo_Impuestos);
                }
            }

            //Nodo_Impuestos.Add(Nodo_Traslados);
            //Nodo_Raiz.Add(Nodo_Impuestos);

            // Crea nodo Complementos
            Nodo_Complemento = new XElement(cfdi + "Complemento");
            Impuestos_Locales = 0;
            Impuestos_Locales += Double.Parse("0");

            if (Impuestos_Locales > 0)
            {
                XElement Nodo_Impuesto_Local = new XElement(implocal + "ImpuestosLocales");

                Nodo_Impuesto_Local.SetAttributeValue("version", "1.0");
                Nodo_Impuesto_Local.SetAttributeValue("TotaldeRetenciones", String.Format("{0:#0.00}", Impuestos_Locales));
                Nodo_Impuesto_Local.SetAttributeValue("TotaldeTraslados", String.Format("{0:#0.00}", 0));
                Nodo_Impuesto_Local.SetAttributeValue(XNamespace.Xmlns + "implocal", "http://www.sat.gob.mx/implocal");
                Nodo_Complemento.Add(Nodo_Impuesto_Local);

                XElement Nodo_Elemento = new XElement(implocal + "RetencionesLocales");

                Nodo_Elemento.SetAttributeValue("ImpLocRetenido", String.Format("{0:#0.00}", Double.Parse("0")));
                Nodo_Elemento.SetAttributeValue("TasadeRetencion", String.Format("{0:#0.00}", Double.Parse("0")));
                Nodo_Elemento.SetAttributeValue("Importe", String.Format("{0:#0.00}", Double.Parse("0")));

                Nodo_Impuesto_Local.Add(Nodo_Elemento);
                Nodo_Complemento.Add(Nodo_Impuesto_Local);
            }
            Nodo_Raiz.Add(Nodo_Complemento);
            Xml_Documento.Add(Nodo_Raiz);

            // Se inicializan las variables del timbre
            Timbrado_VersionSat = String.Empty;
            Timbrado_UUID = String.Empty;
            Timbrado_FechaTimbrado = String.Empty;
            Timbrado_selloCFD = String.Empty;
            Timbrado_noCertificadoSAT = String.Empty;
            Timbrado_selloSAT = String.Empty;
            Session["Codigo_UUID"] = String.Empty;
            // Parametros del timbrado
            if (Dt_Parametros_Facturacion != null)
            {
                if (Dt_Parametros_Facturacion.Rows.Count > 0)
                {
                    Version_Timbrado = Dt_Parametros_Facturacion.Rows[0]["Version"].ToString();
                    Timbrado_Ambiente = Dt_Parametros_Facturacion.Rows[0]["Ambiente_Timbrado"].ToString();
                    Sucursal_Id = Dt_Parametros_Facturacion.Rows[0]["Sucursal_Id"].ToString();
                    Codigo_Usuario = Dt_Parametros_Facturacion.Rows[0]["Codigo_Usuario"].ToString();
                    Codigo_Proveedor = Dt_Parametros_Facturacion.Rows[0]["Codigo_Usuario_Proveedor"].ToString();



                    //!Timbrado_Ambiente.Equals("PRUEBA")
                    try
                    {
                        Respuesta = Cls_Timbrado.Generar_Timbrado_v3(!Timbrado_Ambiente.Equals("PRUEBA"), Version_Timbrado, Codigo_Proveedor, Codigo_Usuario, Sucursal_Id, Xml_Documento.ToString(SaveOptions.DisableFormatting));

                        Xml_Respuesta.LoadXml(Respuesta);

                        XmlNamespaceManager ns = new XmlNamespaceManager(Xml_Respuesta.NameTable);
                        ns.AddNamespace("tfd", "http://www.sat.gob.mx/TimbreFiscalDigital");

                        Nodo_Respuesta = Xml_Respuesta.SelectSingleNode("//tfd:TimbreFiscalDigital", ns);

                        if (Nodo_Respuesta != null)
                        {
                            Timbrado_VersionSat = Nodo_Respuesta.Attributes["Version"].Value;//Nodo_Respuesta.Attributes[0].InnerText;
                            Timbrado_UUID = Nodo_Respuesta.Attributes["UUID"].Value;//Nodo_Respuesta.Attributes[1].InnerText;
                            Timbrado_FechaTimbrado = Nodo_Respuesta.Attributes["FechaTimbrado"].Value;//Nodo_Respuesta.Attributes[2].InnerText;
                            Timbrado_selloCFD = Nodo_Respuesta.Attributes["SelloCFD"].Value;//Respuesta.Attributes[3].InnerText;
                            Timbrado_noCertificadoSAT = Nodo_Respuesta.Attributes["NoCertificadoSAT"].Value;//Nodo_Respuesta.Attributes[4].InnerText;
                            Timbrado_selloSAT = Nodo_Respuesta.Attributes["SelloSAT"].Value;//Nodo_Respuesta.Attributes[5].InnerText;
                            Session["Codigo_UUID"] = Timbrado_UUID;

                            // Complementa la factura
                            Timbrado_FechaTimbrado = Timbrado_FechaTimbrado.Replace('T', ' ');
                            Fecha_Timbrado = Convert.ToDateTime(Timbrado_FechaTimbrado);
                            Rs_Facturas.P_Timbre_Version = Timbrado_VersionSat;
                            Rs_Facturas.P_Timbre_Uuid = Timbrado_UUID;
                            Rs_Facturas.P_Timbre_Fecha_Timbrado = Fecha_Timbrado.ToString();
                            Rs_Facturas.P_Timbre_Sello_Cfd = Timbrado_selloCFD;
                            Rs_Facturas.P_Timbre_No_Certificado_Sat = Timbrado_noCertificadoSAT;
                            Rs_Facturas.P_Timbre_Sello_Sat = Timbrado_selloSAT;
                        }
                        else
                        {
                            Nodo_Respuesta = Xml_Respuesta.SelectSingleNode("//Error");

                            if (Nodo_Respuesta != null)
                            {
                                throw new Exception("ErrorTimbrado  Error: " + Nodo_Respuesta.Attributes[0].InnerText + " " + Nodo_Respuesta.Attributes[1].InnerText + " " + String.Format("{0:yyyy-MMM-dd}", Convert.ToDateTime(Rs_Facturas.P_Fecha_Creo_Xml)) + "   " + String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(Rs_Facturas.P_Fecha_Creo_Xml)) + "T" + String.Format("{0:HH:mm:ss}", Convert.ToDateTime(Rs_Facturas.P_Fecha_Creo_Xml)));
                            }
                            else
                            {
                                throw new Exception("ErrorTimbrado Error: El Servidor de Timbrado no respondio");
                            }
                        }
                    }
                    catch (Exception Ex_Respuesta)
                    {
                        string ruta = Server.MapPath(string.Format("../../Facturacion/Facturacion/logXML/{0}.xml", DateTime.Now.ToString("yyyyMMddHHmmss")));
                        Xml_Documento.Save(ruta);
                        throw new Exception(Ex_Respuesta.Message);
                    }

                    return XDocument.Parse(Respuesta);
                    
                }
                else
                {
                    throw new Exception("No se encontraron los par&aacute;metros del timbrado.");
                }
            }
            else
            {
                throw new Exception("No se encontraron los par&aacute;metros del timbrado.");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Crea_Xml_CFDI Error: " + Ex.Message);
        }
        return Xml_Documento;
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Crea_Tabla_Impuestos
    ///DESCRIPCIÓN: Crea una tabla con la estructura de impuestos.
    ///PARAMETROS:   
    ///CREO:        MiguelAngel Alvarado Enriquez
    ///FECHA_CREO:  19/Diciembre/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private DataTable Crea_Tabla_Impuestos()
    {
        DataTable Dt_Partida;
        Dt_Partida = new DataTable();
        Dt_Partida.Columns.Add("IMPUESTO");
        Dt_Partida.Columns.Add("TASA");
        Dt_Partida.Columns.Add("IMPORTE");
        return Dt_Partida;
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Actualiza_Estatus_Serie
    ///DESCRIPCIÓN: Actualiza el estatus de la serie.
    ///PARAMETROS:  
    ///             Serie, Nombre de la serie a actualizar.
    ///             Consecutivo, Timbre actual.
    ///CREO:        Miguel Angel Alvarado Enriquez
    ///FECHA_CREO:  26/Diciembre/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Actualiza_Estatus_Serie(String Serie, String Consecutivo, Boolean Cancelado, SqlConnection objConexion)
    {
        int Timbre;
        int Timbre_Final;
        int Timbre_Inicial;
        int Timbre_Usados;
        int Facturas_Canceladas;
        String Estatus = String.Empty;
        DataTable Dt_Series = new DataTable();
        DataTable Dt_Ultima_Factura = new DataTable();

        Cls_Ope_Facturas_Series_Negocio Rs_Serie = new Cls_Ope_Facturas_Series_Negocio();
        Cls_Ope_Facturacion_Negocio Rs_Facturas = new Cls_Ope_Facturacion_Negocio();

        Rs_Serie.P_Estatus = "ACTIVO";
        Rs_Serie.P_Serie = Serie;
        Dt_Series = Rs_Serie.Consultar_Serie(objConexion);

        Facturas_Canceladas = Int32.Parse(Dt_Series.Rows[0]["Cancelaciones"].ToString());

        if (Cancelado == true)
        {
            Rs_Serie.P_Cancelaciones = Convert.ToString(Facturas_Canceladas + 1);
            Rs_Serie.P_Serie_Id = Dt_Series.Rows[0]["Serie_Id"].ToString();
            Rs_Serie.Modificar_Serie(objConexion);

            Rs_Facturas.P_Serie = Serie;
            Dt_Ultima_Factura = Rs_Facturas.Consultar_Ultima_Factura(objConexion);
            Timbre = Int32.Parse(Dt_Ultima_Factura.Rows[0]["No_Factura"].ToString());
        }
        else
        {
            Timbre = Int32.Parse(Consecutivo);
        }

        Rs_Serie.P_Estatus = "ACTIVO";
        Rs_Serie.P_Serie = Serie;
        Dt_Series = Rs_Serie.Consultar_Serie(objConexion);

        Timbre_Final = Int32.Parse(Dt_Series.Rows[0]["Timbre_Final"].ToString());
        Timbre_Inicial = Int32.Parse(Dt_Series.Rows[0]["Timbre_Inicial"].ToString());
        Facturas_Canceladas = Int32.Parse(Dt_Series.Rows[0]["Cancelaciones"].ToString());
        Estatus = Dt_Series.Rows[0]["Estatus"].ToString();
        Rs_Serie.P_Serie_Id = Dt_Series.Rows[0]["Serie_Id"].ToString();
        Rs_Serie.P_Serie = String.Empty;

        Timbre_Usados = Timbre + Facturas_Canceladas;

        if (Timbre_Usados >= Timbre_Final)
        {
            Rs_Serie.P_Estatus = "TERMINADO";
            Rs_Serie.Modificar_Serie();

            Rs_Serie.P_Tipo_Serie = "FACTURA";
            Rs_Serie.P_Tipo_Default = "NO";
            Rs_Serie.P_Estatus = "PENDIENTE";
            Dt_Series = Rs_Serie.Consultar_Serie_Activa_Pendiente(objConexion);
            if (Dt_Series != null && Dt_Series.Rows.Count > 0)
            {
                Rs_Serie.P_Estatus = "ACTIVO";
                Rs_Serie.P_Serie_Id = Dt_Series.Rows[0]["Serie_ID"].ToString();
                Rs_Serie.Modificar_Serie(objConexion);
            }

        }

    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Consulta_Maximo_Serie
    ///DESCRIPCIÓN: Consulta el consecutivo de la serie.
    ///PARAMETROS:  Tipo_Serie, Cadena que contiene el tipo de serie a consultar.
    ///CREO:        Miguel Angel Alvarado Enriquez
    ///FECHA_CREO:  28/Diciembre/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Consulta_Maximo_Serie(String Tipo_Serie)
    {
        String Str_Consecutivo = "0";
        String Serie_Id = "0";
        String Serie = String.Empty;
        DataTable Dt_Facturas = new DataTable();
        DataTable Dt_Series = new DataTable();

        Cls_Ope_Facturacion_Negocio Rs_Facturas = new Cls_Ope_Facturacion_Negocio();
        Cls_Ope_Facturas_Series_Negocio Rs_Series = new Cls_Ope_Facturas_Series_Negocio();

        // Busca si hay series del tipo de serie y por default
        Rs_Series.P_Tipo_Serie = Tipo_Serie;
        Rs_Series.P_Tipo_Default = "SI";
        Rs_Series.P_Estatus = "ACTIVO";
        Dt_Series = Rs_Series.Consultar_Serie_Activa_Pendiente();
        if (Dt_Series != null && Dt_Series.Rows.Count > 0)
        {
            Serie_Id = Dt_Series.Rows[0]["Serie_Id"].ToString().Trim();
            Serie = Dt_Series.Rows[0]["Serie"].ToString().Trim();
        }
        else
        {
            // En caso de que el tipo de serie no tenga un default, se consulta la serie
            // que no este por default, pero que sea de ese tipo de serie.
            Rs_Series.P_Tipo_Serie = Tipo_Serie;
            Rs_Series.P_Tipo_Default = "NO";
            Rs_Series.P_Estatus = "ACTIVO";
            Dt_Series = Rs_Series.Consultar_Serie_Activa_Pendiente();
            if (Dt_Series != null && Dt_Series.Rows.Count > 0)
            {
                Serie_Id = Dt_Series.Rows[0]["Serie_Id"].ToString().Trim();
                Serie = Dt_Series.Rows[0]["Serie"].ToString().Trim();
            }
            else
            {
                // Busca cualquier otra serie que este activa o pendiente.
                Rs_Series.P_Tipo_Serie = Tipo_Serie;
                Rs_Series.P_Tipo_Default = String.Empty;
                Rs_Series.P_Estatus = String.Empty;

                Dt_Series = Rs_Series.Consultar_Serie_Activa_Pendiente();
                if (Dt_Series != null && Dt_Series.Rows.Count > 0)
                {
                    Serie_Id = Dt_Series.Rows[0]["Serie_Id"].ToString().Trim();
                    Serie = Dt_Series.Rows[0]["Serie"].ToString().Trim();

                    if (Dt_Series.Rows[0]["Estatus"].ToString().Trim() == "Pendiente")
                    {
                        Rs_Series.P_Serie_Id = Serie_Id;
                        Rs_Series.P_Estatus = "ACTIVO";
                        Rs_Series.Modificar_Serie();
                    }
                }
                else
                {
                    throw new Exception("No hay series activas o pendientes!!!");
                }
            }
        }

        Dt_Facturas = Rs_Facturas.Consultar_Ultima_Factura();

        /*if (!String.IsNullOrEmpty(Serie.Trim()))
        {
            if (String.IsNullOrEmpty(Dt_Facturas.Rows[0]["NO_FACTURA"].ToString()))
            {
                Str_Consecutivo = Convert.ToString(Convert.ToInt32(Dt_Series.Rows[0]["Timbre_Inicial"].ToString()).ToString("D10"));
            }
            else
            {
                Str_Consecutivo = Cls_Metodos_Generales.Obtener_ID_Consecutivo_Tabla();
                //Str_Consecutivo = "0000010190";
                //Str_Consecutivo = Cls_Metodos_Generales.Obtener_ID_Consecutivo(Ope_Facturas.Tabla_Ope_Facturas, Ope_Facturas.Campo_No_Factura, Ope_Facturas.Campo_Serie + "= '" + Serie + "'", 10);
            }*/
            //Session["No_Factura"] = Str_Consecutivo;
            //Session["Factura_Serie"] = Serie + ", " + Str_Consecutivo;
            Session["Serie"] = Serie;
        //}
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Mostrar_PDF
    ///DESCRIPCIÓN: Crea y muestra el pdf en una ventana del navegador.
    ///PARAMETROS:  No_Factura, Numero de la factura a crear.
    ///             No_Serie, Serie de la factura a crear.
    ///CREO:        Miguel Angel Alvarado Enriquez
    ///FECHA_CREO:  12/Diciembre/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private DataSet Crea_Pdf(Cls_Ope_Facturacion_Negocio Rs_Factura, String Regimen_Fiscal, String[] Ruta_Imagen_Factura, String Tipo_Comprobante, DataTable Dt_Impuestos, DataTable Dt_Datos_Fiscales, DataTable Dt_Detalles_Pago, string lugar_expedicion)
    {
        DataSet Ds_Factura_Electronica = new DataSet();
        String Cadena_Original = String.Empty;
        try
        {
            Cls_Ope_Facturas_Detalles_Negocio Rs_Factura_Detalles = new Cls_Ope_Facturas_Detalles_Negocio();
            Rs_Factura_Detalles.P_No_Factura = Rs_Factura.P_No_Factura;
            Rs_Factura_Detalles.P_Serie = Rs_Factura.P_Serie;

            DataTable Dt_Factura = new DataTable();
            DataTable Dt_Factura_Detalles = new DataTable();
            DataTable Dt_Recibo_Pagos = new DataTable();
            DataTable Dt_Empresa = Dt_Datos_Fiscales;
            DataTable Dt_Impuestos_Retenidos = Crea_Tabla_Impuestos();
            DataTable Dt_Impuestos_Locales = Crea_Tabla_Impuestos();

            Dt_Factura = Rs_Factura.Consultar_Factura();
            var formaPago = Dt_Factura.Rows[0]["metodo_pago"].ToString();
            var metodoPago = Dt_Factura.Rows[0]["forma_pago"].ToString();
            Dt_Factura.Rows[0]["metodo_pago"] = metodoPago;
            Dt_Factura.Rows[0]["forma_pago"] = formaPago;
            Dt_Factura_Detalles = Rs_Factura_Detalles.Consultar_Factura();

            Rs_Factura.No_Folio = Dt_Factura.Rows[0]["FOLIO_PAGO"].ToString();
            Dt_Recibo_Pagos = Rs_Factura.Consultar_No_Cuenta();

            Dt_Factura.Columns.Add("NO_RECIBO", typeof(String));
            Dt_Factura.Rows[0]["NO_RECIBO"] = Dt_Recibo_Pagos.Rows[0]["NO_RECIBO"].ToString();

            String Tipo = Dt_Factura.Rows[0][Ope_Facturas.Campo_Tipo_Factura].ToString();
            String Expedida_En = String.Empty;
            if (Tipo.Equals("NORMAL"))
            {
                Rs_Factura.P_Subtotal = Dt_Factura.Rows[0][Ope_Facturas.Campo_Subtotal].ToString();
                Rs_Factura.P_Subtotal_Cero = Dt_Factura.Rows[0][Ope_Facturas.Campo_Subtotal_Cero].ToString();
                Rs_Factura.P_Iva = Dt_Factura.Rows[0][Ope_Facturas.Campo_Iva].ToString();
                Rs_Factura.P_Descuento = Dt_Factura.Rows[0][Ope_Facturas.Campo_Descuento].ToString();
                Rs_Factura.P_Total = Dt_Factura.Rows[0][Ope_Facturas.Campo_Total].ToString();
            }

            // Rellena la tabla de Parametros facturacion
            Cls_Apl_Parametros_Facturacion_Negocio Rs_Impuestos = new Cls_Apl_Parametros_Facturacion_Negocio();
            DataTable Dt_Parametros_Factura = Rs_Impuestos.Consultar_Parametros();

            Expedida_En = (Dt_Datos_Fiscales.Rows[0]["Ciudad"].ToString() + ", " + Dt_Datos_Fiscales.Rows[0]["Estado"].ToString()).ToUpper();
            Expedida_En = lugar_expedicion;
            //Cadena_Original = Cls_Timbrado.Genera_Cadena_Original(Rs_Factura, Expedida_En, Regimen_Fiscal, Dt_Datos_Fiscales,
            //Dt_Detalles_Pago, Dt_Impuestos);
            Cadena_Original = Rs_Factura.P_Cadena_Original;

            // Renombra las tablas
            Dt_Factura.TableName = "DT_FACTURAS";
            Dt_Factura_Detalles.TableName = "DT_FACTURAS_DETALLES";
            Dt_Empresa.TableName = "CAT_EMPRESAS";
            Dt_Parametros_Factura.TableName = "APL_CAT_PARAMETROS_IMPUESTOS";

            // Agrega otros campos que se van a enviar.
            // Encabezado factura
            Dt_Factura.Columns.Add("TASA_IVA_GENERAL", typeof(Double));
            Dt_Factura.Columns.Add("IMPORTE_LETRA", typeof(String));
            Dt_Factura.Columns.Add("IMAGEN_QR", typeof(Byte[]));
            //Dt_Factura.Columns.Add("NO_CUENTA", typeof(String));
            //Dt_Factura.Columns.Add("PERIODO_PAGO", typeof(String));
            //Dt_Factura.Columns.Add("TIPO_SERVICIO", typeof(String));

            // Datos de la Empresa
            if (!Dt_Empresa.Columns.Contains("IMAGEN"))
            {
                Dt_Empresa.Columns.Add("IMAGEN", typeof(Byte[]));
            }
            if (!Dt_Empresa.Columns.Contains("IMAGEN_2"))
            {
                Dt_Empresa.Columns.Add("IMAGEN_2", typeof(Byte[]));
            }
            if (!Dt_Empresa.Columns.Contains("IMAGEN_3"))
            {
                Dt_Empresa.Columns.Add("IMAGEN_3", typeof(Byte[]));
            }
            if (!Dt_Empresa.Columns.Contains("IMAGEN_4"))
            {
                Dt_Empresa.Columns.Add("IMAGEN_4", typeof(Byte[]));
            }
            if (!Dt_Empresa.Columns.Contains("REGIMEN_FISCAL"))
            {
                Dt_Empresa.Columns.Add("REGIMEN_FISCAL", typeof(String));
            }
            if (!Dt_Empresa.Columns.Contains("EXPEDIDO_EN"))
            {
                Dt_Empresa.Columns.Add("EXPEDIDO_EN", typeof(String));
            }

            // Rellena los nuevos campos
            // Encabezado factura
            Dt_Factura.Rows[0]["TASA_IVA_GENERAL"] = Convert.ToDouble(Dt_Impuestos.Rows[0]["Tasa"].ToString());
            Dt_Factura.Rows[0]["IMPORTE_LETRA"] = Cls_Metodos_Generales.Convertir_Cantidad_Letras(Convert.ToDecimal(Dt_Factura.Rows[0][Ope_Facturas.Campo_Total].ToString()), Dt_Factura.Rows[0][Ope_Facturas.Campo_Tipo_Moneda].ToString());
            Dt_Factura.Rows[0]["CADENA_ORIGINAL"] = Cadena_Original;
            Dt_Factura.Rows[0]["IMAGEN_QR"] = Cls_Metodos_Generales.Convertir_Imagen_Bytes(Server.MapPath(Dt_Factura.Rows[0][Ope_Facturas.Campo_Ruta_Codigo_Bd].ToString()), 1.0, 300);
            //Dt_Factura.Rows[0]["NO_CUENTA"] = Rs_Factura.P_Rpu;//Dt_Detalles_Pago.Rows[0]["No_Cuenta"].ToString();
            Dt_Factura.Rows[0]["NO_CUENTA"] = Dt_Detalles_Pago.Rows[0]["No_Cuenta"].ToString();
            //Dt_Factura.Rows[0]["TIPO_SERVICIO"] = //Dt_Detalles_Pago.Rows[0]["Tipo_Servicio"].ToString();
            Dt_Factura.Rows[0]["PERIODO_PAGO"] = Rs_Factura.P_Periodo_Pago.Trim();
            Dt_Factura.Rows[0]["TIPO_SERVICIO"] = Rs_Factura.P_Tipo_Servicio.Trim();

            // Datos de la Empresa
            Dt_Empresa.Rows[0]["REGIMEN_FISCAL"] = Regimen_Fiscal;
            Dt_Empresa.Rows[0]["EXPEDIDO_EN"] = Expedida_En;
            Dt_Empresa.Rows[0]["IMAGEN"] = Cls_Metodos_Generales.Convertir_Imagen_Bytes(Ruta_Imagen_Factura[0], 161, 172);
            Dt_Empresa.Rows[0]["IMAGEN_2"] = Cls_Metodos_Generales.Convertir_Imagen_Bytes(Ruta_Imagen_Factura[1], 161, 172);
            Dt_Empresa.Rows[0]["IMAGEN_3"] = Cls_Metodos_Generales.Convertir_Imagen_Bytes(Ruta_Imagen_Factura[2], 161, 172);
            Dt_Empresa.Rows[0]["IMAGEN_4"] = Cls_Metodos_Generales.Convertir_Imagen_Bytes(Ruta_Imagen_Factura[3], 161, 172);

            foreach (DataRow fila in Dt_Factura_Detalles.Rows)
            {
                fila["codigo"] = new Cls_Apl_Parametros_Facturacion_Negocio().ObtenerUnidadConceptoSat(fila["codigo"].ToString()).Rows[0]["concepto_sat"].ToString();
            }

            Ds_Factura_Electronica.Tables.Add(Dt_Factura.Copy());
            Ds_Factura_Electronica.Tables.Add(Dt_Factura_Detalles.Copy());
            Ds_Factura_Electronica.Tables.Add(Dt_Empresa.Copy());
            Ds_Factura_Electronica.Tables.Add(Dt_Parametros_Factura.Copy());

            //Cls_Metodos_Generales.Generar_Reporte(ref Ds_Factura_Electronica, "../Rpt/Facturacion/", "Rpt_Ope_Facturacion_Electronica.rpt", Ruta_Pdf, "Facturaci&oacuten Electr&oacute;nica", this, this.GetType());
        }
        catch (Exception Ex)
        {
            throw new Exception("Crea_Pdf Error:[" + Ex.Message + "]");
        }

        return Ds_Factura_Electronica;
    }

    private Cls_Factura_Pdf Crea_Obj_Pdf(Cls_Ope_Facturacion_Negocio Rs_Factura, string Regimen_Fiscal, string[] Ruta_Imagen_Factura, string Tipo_Comprobante, DataTable Dt_Parametros_Facturacion, System.Data.DataTable Dt_Impuestos, System.Data.DataTable Dt_Datos_Fiscales, System.Data.DataTable Dt_Totales, System.Data.DataTable Dt_Cfdi_Relacionados, String Recibos)
    {
        Cls_Factura_Pdf Obj_Factura_Electronica = new Cls_Factura_Pdf();
        List<Cls_Cfdi_Relacionados> Lst_Cfdi_Relacionados;
        Cls_Datos_Clientes Datos_Clientes;
        List<Cls_Datos_Conceptos> Lst_Conceptos = new List<Cls_Datos_Conceptos>();
        List<Cls_Datos_Impuestos> Lst_Impuestos = new List<Cls_Datos_Impuestos>();
        Cls_Metodos_Pago Metodos_Pago;

        //DataSet Ds_Factura_Electronica = new Ds_Ope_Fe_Factura_Pdf();
        String Cadena_Original = String.Empty;
        String Numeros_Recibo = String.Empty;

        Double Subtotal_Impuesto_Traslado = 0;
        Double Subtotal_Impuesto_Retencion = 0;

        DataTable Dt_Datos_Generales = new DataTable();
        DataTable Dt_Factura = new DataTable();
        DataTable Dt_Factura_Detalles = new DataTable();
        DataTable Dt_Empresa = Dt_Datos_Fiscales;
        DataTable Dt_Impuestos_Retenidos = Crea_Tabla_Impuestos();
        DataTable Dt_Impuestos_Locales = Crea_Tabla_Impuestos();

        Cls_Ope_Facturas_Detalles_Negocio Rs_Factura_Detalles = new Cls_Ope_Facturas_Detalles_Negocio();
        Rs_Factura_Detalles.P_No_Factura = Rs_Factura.P_No_Factura;
        Rs_Factura_Detalles.P_Serie = Rs_Factura.P_Serie;

        Dt_Datos_Generales = Rs_Factura.Consultar_Factura();
        //Dt_Datos_Generales = Obj_Facturacion_Dao.Consultar_Factura(Rs_Factura.P_No_Factura, Rs_Factura.P_Serie);

        if (Dt_Datos_Generales != null)
        {
            var Enumerable_Facturas = from Factura in Dt_Datos_Generales.AsEnumerable()
                                      select Factura;
            Enumerable_Facturas.ToList<DataRow>().ForEach(Factura =>
            {
                Factura["CONDICIONES"] = Factura["CONDICIONES"] + "|" + Rs_Factura.P_Leyenda_Fiscal; Factura["NO_CUENTA_PAGO"] = Rs_Factura.P_No_Cuenta_Pago;
                if (Factura["LOCALIDAD"].ToString().Trim() != "")
                {
                    Factura["CIUDAD"] = Factura["CIUDAD"] + ", " + Factura["LOCALIDAD"];
                }
            });
        }


        //Dt_Factura_Detalles = Obj_Facturacion_Dao.Consultar_Detalles_Factura(Rs_Factura.P_No_Factura, Rs_Factura.P_Serie);
        Dt_Factura_Detalles = Rs_Factura_Detalles.Consultar_Factura();


        Recibos = "";
        Dt_Datos_Generales.Columns.Add("NO_RECIBO", typeof(String));

        Dt_Datos_Generales.Rows[0]["NO_RECIBO"] = Recibos;
        Dt_Datos_Generales.Columns.Add("TIPO_CFDI", typeof(String));
        Dt_Datos_Generales.Rows[0]["TIPO_CFDI"] = Rs_Factura.P_Tipo_Comprobante;
        //Dt_Factura_Detalles.Rows.Clear();

        //String Abreviatura_Unidad = Dt_Parametros_Facturacion.Rows[0]["ABREVIATURA_UNIDAD"].ToString().ToUpper();

        //foreach (DataRow row in Dt_Totales.Rows)
        //{
        //    DataRow Dr_Detalle = Dt_Factura_Detalles.NewRow();
        //    Dr_Detalle["NO_FACTURA"] = Dt_Datos_Generales.Rows[0]["NO_FACTURA"].ToString();
        //    Dr_Detalle["FOLIO_PAGO"] = row["CODIGO_BARRAS"].ToString();
        //    Dr_Detalle["SERIE"] = Dt_Datos_Generales.Rows[0]["SERIE"].ToString();
        //    Dr_Detalle["CANTIDAD"] = 1;
        //    Dr_Detalle["CODIGO"] = row["NO_RECIBO"].ToString();
        //    Dr_Detalle["DESCRIPCION"] = row["DESCRIPCION"].ToString();
        //    Dr_Detalle["UNIDAD"] = row["Unidad_Desc"].ToString();
        //    //Dr_Detalle["PORCENTAJE_IMPUESTO"] = row["IVA"].ToString();
        //    Dr_Detalle["SUBTOTAL"] = row["Subtotal"].ToString();
        //    Dr_Detalle["PRECIO_UNITARIO"] = row["valorUnitario"].ToString();
        //    Dr_Detalle["TOTAL"] = row["Total"].ToString();
        //    Dr_Detalle["IVA"] = row["Impuesto_Importe"].ToString();
        //    Dt_Factura_Detalles.Rows.Add(Dr_Detalle);
        //}

        String Tipo = Dt_Datos_Generales.Rows[0]["TIPO_FACTURA"].ToString();
        String Expedida_En = String.Empty;
        if (Tipo.Equals("NORMAL") || Tipo.Equals("GLOBAL"))
        {
            Decimal Subtotal = 0;
            Decimal Subtotal_Cero = 0;
            Decimal Iva = 0;
            Decimal Descuento = 0;
            Decimal Total = 0;

            Decimal.TryParse(Dt_Datos_Generales.Rows[0]["SUBTOTAL"].ToString(), out Subtotal);
            Decimal.TryParse(Dt_Datos_Generales.Rows[0]["SUBTOTAL_CERO"].ToString(), out Subtotal_Cero);
            Decimal.TryParse(Dt_Datos_Generales.Rows[0]["IVA"].ToString(), out Iva);
            Decimal.TryParse(Dt_Datos_Generales.Rows[0]["DESCUENTO"].ToString(), out Descuento);
            Decimal.TryParse(Dt_Datos_Generales.Rows[0]["TOTAL"].ToString(), out Total);
            Rs_Factura.P_Subtotal = Subtotal.ToString();// Dt_Factura.Rows[0]["SUB_TOTAL"].ToString();
            Rs_Factura.P_Subtotal_Cero = Subtotal_Cero.ToString();// Dt_Factura.Rows[0]["SUB_TOTAL_CERO"].ToString();
            Rs_Factura.P_Iva = Iva.ToString();// Dt_Factura.Rows[0]["IVA"].ToString();
            Rs_Factura.P_Descuento = Descuento.ToString();// Dt_Factura.Rows[0]["DESCUENTO"].ToString();
            Rs_Factura.P_Total = Total.ToString();// Dt_Factura.Rows[0]["TOTAL"].ToString();
        }
        Expedida_En = (Dt_Datos_Fiscales.Rows[0]["Ciudad"].ToString() + ", " + Dt_Datos_Fiscales.Rows[0]["Estado"].ToString()).ToUpper();
        Cadena_Original = Rs_Factura.P_Cadena_Original;
        // Renombra las tablas
        Dt_Datos_Generales.TableName = "DT_FACTURAS";
        Dt_Factura_Detalles.TableName = "DT_FACTURAS_DETALLES";
        Dt_Empresa.TableName = "CAT_EMPRESAS";
        Dt_Parametros_Facturacion.TableName = "APL_CAT_PARAMETROS_IMPUESTOS";
        // Agrega otros campos que se van a enviar.
        // Encabezado factura
        Dt_Datos_Generales.Columns.Add("TASA_IVA_GENERAL", typeof(Double));
        Dt_Datos_Generales.Columns.Add("IMPORTE_LETRA", typeof(String));
        Dt_Datos_Generales.Columns.Add("IMAGEN_QR", typeof(Byte[]));

        //Dt_Factura.Columns.Add("CADENA_ORIGINAL", typeof(String));

        // Datos de la Empresa
        if (!Dt_Empresa.Columns.Contains("IMAGEN"))
        {
            Dt_Empresa.Columns.Add("IMAGEN", typeof(Byte[]));
        }
        if (!Dt_Empresa.Columns.Contains("IMAGEN_2"))
        {
            Dt_Empresa.Columns.Add("IMAGEN_2", typeof(Byte[]));
        }
        if (!Dt_Empresa.Columns.Contains("IMAGEN_3"))
        {
            Dt_Empresa.Columns.Add("IMAGEN_3", typeof(Byte[]));
        }
        if (!Dt_Empresa.Columns.Contains("IMAGEN_4"))
        {
            Dt_Empresa.Columns.Add("IMAGEN_4", typeof(Byte[]));
        }
        if (!Dt_Empresa.Columns.Contains("REGIMEN_FISCAL"))
        {
            Dt_Empresa.Columns.Add("REGIMEN_FISCAL", typeof(String));
        }
        if (!Dt_Empresa.Columns.Contains("EXPEDIDO_EN"))
        {
            Dt_Empresa.Columns.Add("EXPEDIDO_EN", typeof(String));
        }

        // Rellena los nuevos campos
        // Encabezado factura
        //if (Dt_Impuestos.Rows.Count > 0)
        //    Dt_Datos_Generales.Rows[0]["TASA_IVA_GENERAL"] = Convert.ToDouble(Dt_Impuestos.Rows[0]["TasaOcuota"].ToString());
        //else
        //    Dt_Datos_Generales.Rows[0]["TASA_IVA_GENERAL"] = 0;
        Dt_Datos_Generales.Rows[0]["IMPORTE_LETRA"] = Cls_Metodos_Generales.Convertir_Cantidad_Letras(Convert.ToDecimal(Dt_Datos_Generales.Rows[0]["TOTAL"].ToString()), Dt_Datos_Generales.Rows[0]["TIPO_MONEDA"].ToString());
        Dt_Datos_Generales.Rows[0]["CADENA_ORIGINAL"] = Cadena_Original;
        Dt_Datos_Generales.Rows[0]["IMAGEN_QR"] = Cls_Metodos_Generales.Convertir_Imagen_Bytes(Dt_Datos_Generales.Rows[0]["RUTA_CODIGO_BD"].ToString(), 1.0, 300);

        Dt_Empresa.Rows[0]["REGIMEN_FISCAL"] = Regimen_Fiscal;
        Dt_Empresa.Rows[0]["EXPEDIDO_EN"] = Expedida_En;

        Dt_Empresa.Rows[0]["IMAGEN"] = null;
        Dt_Empresa.Rows[0]["IMAGEN_2"] = null;
        Dt_Empresa.Rows[0]["IMAGEN_3"] = null;
        Dt_Empresa.Rows[0]["IMAGEN_4"] = null;

        #region armado de tabla encabezado factura
        Obj_Factura_Electronica.Imagen_Qr = Cls_Metodos_Generales.Convertir_Imagen_Bytes(Dt_Datos_Generales.Rows[0]["RUTA_CODIGO_BD"].ToString(), 1.0, 300);
        Obj_Factura_Electronica.Imagen_Qr = Cls_Metodos_Generales.Convertir_Imagen_Bytes(Server.MapPath(Dt_Datos_Generales.Rows[0]["RUTA_CODIGO_BD"].ToString()), 1.0, 300);
        Obj_Factura_Electronica.Razon_Social_Empresa = Dt_Empresa.Rows[0]["RAZON_SOCIAL"].ToString();
        Obj_Factura_Electronica.Rfc_Empresa = Dt_Empresa.Rows[0]["RFC"].ToString();
        Obj_Factura_Electronica.Direccion_Empresa = Dt_Empresa.Rows[0]["CALLE"].ToString() + " " + Dt_Empresa.Rows[0]["NUMERO_EXTERIOR"].ToString() + " C.P. " + Dt_Empresa.Rows[0]["CP"].ToString();
        Obj_Factura_Electronica.Codigo_Postal_Empresa = Dt_Empresa.Rows[0]["CP"].ToString();
        Obj_Factura_Electronica.Colonia_Empresa = Dt_Empresa.Rows[0]["COLONIA"].ToString();
        Obj_Factura_Electronica.Ciudad_Empresa = Dt_Empresa.Rows[0]["CIUDAD"].ToString();
        Obj_Factura_Electronica.Estado_Empresa = Dt_Empresa.Rows[0]["ESTADO"].ToString();
        Obj_Factura_Electronica.Pais_Empresa = Dt_Empresa.Rows[0]["PAIS"].ToString();
        Obj_Factura_Electronica.Regimen_Fiscal = Regimen_Fiscal;
        Obj_Factura_Electronica.Tipo_CFDI = Rs_Factura.P_Tipo_Comprobante;
        Obj_Factura_Electronica.No_certificado = Dt_Datos_Generales.Rows[0]["NO_CERTIFICADO"].ToString();
        Obj_Factura_Electronica.No_certificado_Sat = Dt_Datos_Generales.Rows[0]["TIMBRE_NO_CERTIFICADO_SAT"].ToString();
        Obj_Factura_Electronica.Fecha_Timbrado = Dt_Datos_Generales.Rows[0]["FECHA_CREO_XML"].ToString();
        Obj_Factura_Electronica.Uuid = Dt_Datos_Generales.Rows[0]["TIMBRE_UUID"].ToString();
        Obj_Factura_Electronica.Serie = Dt_Datos_Generales.Rows[0]["SERIE"].ToString();
        Obj_Factura_Electronica.Folio = Dt_Datos_Generales.Rows[0]["NO_FACTURA"].ToString();
        Obj_Factura_Electronica.Moneda = Dt_Datos_Generales.Rows[0]["TIPO_MONEDA"].ToString();
        Obj_Factura_Electronica.Tipo_Cambio = "0";// Dt_Parametros_Facturacion.Rows[0]["TIPO_CAMBIO"].ToString();
        Obj_Factura_Electronica.Fecha_Emision = Dt_Datos_Generales.Rows[0]["FECHA_EMISION"].ToString();
        Obj_Factura_Electronica.Lugar_Expedicion = Dt_Datos_Generales.Rows[0]["CODIGO_POSTAL_EXPEDICION"].ToString();
        Obj_Factura_Electronica.Forma_Pago = Rs_Factura.P_Forma_Pago;
        Obj_Factura_Electronica.Metodo_Pago = Rs_Factura.P_Metodo_Pago;
        Obj_Factura_Electronica.Condiciones_Pago = Rs_Factura.P_Condiciones;
        Obj_Factura_Electronica.Importe_Letra = Dt_Datos_Generales.Rows[0]["IMPORTE_LETRA"].ToString();
        Obj_Factura_Electronica.SubTotal = Dt_Datos_Generales.Rows[0]["SUBTOTAL"].ToString();
        Obj_Factura_Electronica.Total = Dt_Datos_Generales.Rows[0]["TOTAL"].ToString();
        Obj_Factura_Electronica.Descuento = Dt_Datos_Generales.Rows[0]["DESCUENTO"].ToString();
        Obj_Factura_Electronica.Version_Cfdi = Rs_Factura.P_Timbre_Version;
        Obj_Factura_Electronica.Cadena_Original = Cadena_Original;
        Obj_Factura_Electronica.Timbre_Sello_CFD = Dt_Datos_Generales.Rows[0]["TIMBRE_sELLO_CFD"].ToString();
        Obj_Factura_Electronica.Timbre_Sello_SAT = Dt_Datos_Generales.Rows[0]["TIMBRE_sELLO_SAT"].ToString();
        Obj_Factura_Electronica.Leyenda_Fiscal = "leyenda";
        //Obj_Factura_Electronica.Recibos_Incluidos = Rs_Factura.RecibosRelacionados;

        //Datos Cliente    
        Datos_Clientes = new Cls_Datos_Clientes();
        Datos_Clientes.Calle = Dt_Datos_Generales.Rows[0]["CALLE"].ToString();
        //Datos_Clientes.Ciudad = Dt_Datos_Generales.Rows[0]["CIUDAD"].ToString();
        Datos_Clientes.Ciudad = Dt_Datos_Generales.Rows[0]["localidad"].ToString();
        Datos_Clientes.Colonia = Dt_Datos_Generales.Rows[0]["COLONIA"].ToString();
        Datos_Clientes.Comentarios = "";
        Datos_Clientes.CP = Dt_Datos_Generales.Rows[0]["CP"].ToString();
        Datos_Clientes.Cuenta_Pago = Dt_Datos_Generales.Rows[0]["NO_CUENTA_PAGO"].ToString();
        Datos_Clientes.Estado = Dt_Datos_Generales.Rows[0]["ESTADO"].ToString();
        Datos_Clientes.Razon_Social = Dt_Datos_Generales.Rows[0]["RAZON_SOCIAL"].ToString();
        Datos_Clientes.Numero_Exterior = Dt_Datos_Generales.Rows[0]["NUMERO_EXTERIOR"].ToString();
        Datos_Clientes.Numero_Interior = Dt_Datos_Generales.Rows[0]["NUMERO_INTERIOR"].ToString();
        Datos_Clientes.Pais = Dt_Datos_Generales.Rows[0]["PAIS"].ToString();
        Datos_Clientes.RFC = Dt_Datos_Generales.Rows[0]["RFC"].ToString();
        Datos_Clientes.UsoCFDI = Rs_Factura.UsoCFDI;
        Obj_Factura_Electronica.Datos_Clientes = Datos_Clientes;

        foreach (DataRow Dr_Renglon in Dt_Totales.Rows)
        {
            Cls_Datos_Conceptos Concepto = new Cls_Datos_Conceptos();
            Concepto.Base = Convert.ToDouble(Dr_Renglon["importe"].ToString());
            Concepto.Cantidad ="1.00";
            Concepto.ClaveProdServ = "10121804"; //Dr_Renglon["ClaveProdServ"].ToString();//codigo del sat
            Concepto.ClaveUnidad = "codigin"; //Dr_Renglon["ClaveUnidad"].ToString();//codigo de cat_fe_unidades
            Concepto.Descripcion = Dr_Renglon["descripcion"].ToString();
            Concepto.Descripcion_Cat = "aqui va la descrición";// Dr_Renglon["Descripcion_Cat"].ToString();//descripcion del cat_fe_productos_serv
            Concepto.Descuento = 0; //Convert.ToDouble(Dr_Renglon["Descuento"].ToString());//<- total del descuento
            Concepto.Fecha_Recibo = "10/10/2017";// Dr_Renglon["Fecha_Recibo"].ToString();//fecha recibo
            Concepto.Folio_Pago = "0000";// Dr_Renglon["Folio_Pago"].ToString();//nada
            //Concepto.Importe = Convert.ToDouble(Dr_Renglon["total"].ToString());//unitario x cantidad
            Concepto.Importe = Convert.ToDouble(String.Format("{0:#0.00}", Convert.ToDouble(Dr_Renglon["total"])));
            Concepto.Impuesto = "002";
            Concepto.Impuesto_Importe = 0;// Convert.ToDouble(Dr_Renglon["Impuesto_Importe"].ToString());//cantidad impuesto
            Concepto.Nodo = "TRASLADO";
            Concepto.NoIdentificacion = Dr_Renglon["clave_concepto"].ToString();
            Concepto.No_Pago = "1";
            Concepto.No_Recibo = "2";// Dr_Renglon["No_Recibo"].ToString();//    <---
            Concepto.TasaOCuota = "0.16";
            //Concepto.Subtotal = Convert.ToDouble(Dr_Renglon["total"].ToString());
            Concepto.Subtotal = Convert.ToDouble(String.Format("{0:#0.00}", Convert.ToDouble(Dr_Renglon["importe"])));
            Concepto.TipoFactor = "TASA";
            //Concepto.Total = Convert.ToDouble(Dr_Renglon["total"].ToString());
            Concepto.Total = Convert.ToDouble(String.Format("{0:#0.00}", Convert.ToDouble(Dr_Renglon["total"])));
            Concepto.Unidad = Dr_Renglon["unidad_de_medida"].ToString();
            //Concepto.Unidad_Desc = "E48-Unidad de servicio";// Dr_Renglon["Unidad_Desc"].ToString();//descripcion de cat_fe_unidades
            Concepto.Unidad_Desc = Dr_Renglon["concepto_sat"].ToString();
            Concepto.valorUnitario = String.Format("{0:#0.00}", Convert.ToDouble(Dr_Renglon["importe"]));
            Lst_Conceptos.Add(Concepto);
        }
        Obj_Factura_Electronica.Lst_Conceptos = Lst_Conceptos;

        Cls_Datos_Impuestos Impuesto;

        foreach (DataRow Dr_Renglon in Dt_Impuestos.Rows)
        {
            Impuesto = new Cls_Datos_Impuestos();
            //Impuesto.Base = Convert.ToDouble(Dr_Renglon["Base"].ToString())
            Impuesto.Importe = Convert.ToDouble(Dr_Renglon["Importe"].ToString());
            Impuesto.Impuesto = string.Empty;// Dr_Renglon["Impuesto"].ToString();//002
            Impuesto.Nodo = "Traslado";
            Impuesto.TasaOcuota = Convert.ToDouble(Dr_Renglon["tasa"].ToString()) / 100;//0.16
            Impuesto.TipoFactor = "Tasa";// Dr_Renglon["TipoFactor"].ToString();//"TASA"
            Lst_Impuestos.Add(Impuesto);
        }
        Obj_Factura_Electronica.Lst_Impuestos = Lst_Impuestos;
        double Impuesto_Retenido = 0;
        double Impuesto_Traslado = 0;

        Impuesto_Traslado = (from arreglo in Lst_Impuestos
                             where arreglo.Nodo == "Traslado"
                             select arreglo.Importe).Sum();

        Impuesto_Retenido = (from arreglo in Lst_Impuestos
                             where arreglo.Nodo == "Retencion"
                             select arreglo.Importe).Sum();

        Obj_Factura_Electronica.Impuesto_Retenido = Impuesto_Retenido.ToString();
        Obj_Factura_Electronica.Impuesto_Traslado = String.Format("{0:#0.00}",Impuesto_Traslado);

        Obj_Factura_Electronica.Logo = Ruta_Imagen_Factura[0];

        #endregion

        return Obj_Factura_Electronica;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Consultar_Ciudades
    ///DESCRIPCIÓN: Consulta las ciudades
    ///PARAMETROS:  Request
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  25/febrero/2016
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public string Consultar_Ciudades(HttpRequest Request)
    {
        Cls_Cat_Cor_Ciudades_Negocio Negocio = new Cls_Cat_Cor_Ciudades_Negocio();
        String Cadena_Resultado = "{[]}";
        DataTable Dt_Datos = new DataTable();

        try
        {
            String Estado = HttpContext.Current.Request["Estado_Id"];

            if (!String.IsNullOrEmpty(Estado) && Estado.ToLower() != "seleccione" && Estado.ToLower() != "null")
            {
                Negocio.P_Estado_ID = Estado;
            }
            else
            {
                Negocio.P_Estado_ID = "";
            }
            Dt_Datos = Negocio.Buscar_Ciudad();

            Cadena_Resultado = SIAC.Ayudante_JQuery.Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt_Datos);
        }
        catch (Exception Ex)
        {
            String Mensaje_Error = Ex.Message.ToString();
        }
        return Cadena_Resultado;
    }


    [WebMethod]
    public static string Buscar_Correo(string numero_cuenta)
    {

        string str_respuesta;
        //-------------------------------------------------
        Cls_Mensajes_Servidor objMensajeServidor = new Cls_Mensajes_Servidor();
        JsonSerializerSettings configuracionJson = new JsonSerializerSettings();
        configuracionJson.NullValueHandling = NullValueHandling.Ignore;
        //------------------------------------------------------
        Cls_Pagos_Internet Obj_Pagos_Internet = new Cls_Pagos_Internet();
        //--------------------------------------------------
        string correo;

        try
        {

            correo = Obj_Pagos_Internet.Buscar_Correo_Electronico(numero_cuenta);
            objMensajeServidor.mensaje = "bien";
            objMensajeServidor.dato1 = correo;

        }
        catch (Exception ex)
        {
            objMensajeServidor.mensaje = ex.Message;
        }


        str_respuesta = JsonConvert.SerializeObject(objMensajeServidor, Newtonsoft.Json.Formatting.Indented, configuracionJson);
        return str_respuesta;

    }


    [WebMethod]
    public static string Elaborar_Refactura(string no_cuenta, string predio_id)
    {

        string str_respuesta;
        //-------------------------------------------------
        Cls_Mensajes_Servidor objMensajeServidor = new Cls_Mensajes_Servidor();
        JsonSerializerSettings configuracionJson = new JsonSerializerSettings();
        configuracionJson.NullValueHandling = NullValueHandling.Ignore;
        //---------------------------------------------------
        Cls_Pagos_Internet Obj_Pagos_Internet = new Cls_Pagos_Internet();
        //--------------------------------------------------
        DataTable Dt_Datos;
        //--------------------------------------------------
        string resultado;
        //--------------------------------------------------

        int year;
        int bimestre;


        try
        {

            Dt_Datos = Obj_Pagos_Internet.Obtener_Bimestre(Convert.ToInt32(no_cuenta));

            for (int Indice_Periodos = 0; Indice_Periodos < Dt_Datos.Rows.Count; Indice_Periodos++)
            {
                for (int Indice_Periodo_Siguiente = Indice_Periodos + 1; Indice_Periodo_Siguiente < Dt_Datos.Rows.Count; Indice_Periodo_Siguiente++)
                {
                    if (Dt_Datos.Rows[Indice_Periodos]["Año"].ToString().Equals(Dt_Datos.Rows[Indice_Periodo_Siguiente]["Año"].ToString())
                        && Dt_Datos.Rows[Indice_Periodos]["Bimestre"].ToString().Equals(Dt_Datos.Rows[Indice_Periodo_Siguiente]["Bimestre"].ToString()))
                    {
                        Dt_Datos.Rows.RemoveAt(Indice_Periodo_Siguiente);
                        Indice_Periodo_Siguiente--;
                    }
                }
            }


            if (Dt_Datos.Rows.Count > 0)
            {
                year = Convert.ToInt16(Dt_Datos.Rows[0]["Año"].ToString());
                bimestre = Convert.ToInt16(Dt_Datos.Rows[0]["Bimestre"].ToString());

            }
            else
            {
                objMensajeServidor.mensaje = "No se puede refacturar al período actual, debido a que no hay bimestres disponibles para la cuenta: " + no_cuenta;
                str_respuesta = JsonConvert.SerializeObject(objMensajeServidor, Newtonsoft.Json.Formatting.Indented, configuracionJson);
                return str_respuesta;

            }

            resultado = Obj_Pagos_Internet.Elaborar_Refacturacion(predio_id, year, bimestre, "pago en internet");

            if (resultado.Length == 10)
            {
                objMensajeServidor.mensaje = "bien";
                objMensajeServidor.dato1 = year.ToString();
                objMensajeServidor.dato2 = bimestre.ToString();
            }
            else
            {
                resultado = resultado.Replace("'", "");
                objMensajeServidor.mensaje = resultado;
            }




        }
        catch (Exception ex)
        {
            objMensajeServidor.mensaje = ex.Message;
        }

        str_respuesta = JsonConvert.SerializeObject(objMensajeServidor, Newtonsoft.Json.Formatting.Indented, configuracionJson);
        return str_respuesta;


    }


    [WebMethod]
    public static string Buscar_Estado_Cuenta(string rpu, string correo_electronico)
    {

        string str_respuesta;
        string no_padron_recuperado;
        string str_informacion;
        //-------------------------------------------------
        Cls_Mensajes_Servidor objMensajeServidor = new Cls_Mensajes_Servidor();
        JsonSerializerSettings configuracionJson = new JsonSerializerSettings();
        configuracionJson.NullValueHandling = NullValueHandling.Ignore;
        //---------------------------------------------------
        Cls_Ope_Facturacion_Negocio Obj_Negocio = new Cls_Ope_Facturacion_Negocio();
        //---------------------------------------------------
        DataTable Dt_Datos;
        //------------------------------------------------------
        Cls_Pagos_Internet Obj_Pagos_Internet = new Cls_Pagos_Internet();
        //--------------------------------------------------

        try
        {
            Obj_Negocio.P_Rpu = rpu;
            //Dt_Datos = Obj_Negocio.Consultar_Datos_Estado_Cuenta();
            Dt_Datos = Consultar_Estado_Cuenta(rpu);


            if (Dt_Datos == null || Dt_Datos.Rows.Count == 0)
            {

                objMensajeServidor.mensaje = "Contrato no encontrado ";
                str_respuesta = JsonConvert.SerializeObject(objMensajeServidor, Newtonsoft.Json.Formatting.Indented, configuracionJson);
                return str_respuesta;

            }
            else
            {
                //no_padron_recuperado = Dt_Datos.Rows[0]["No_Padron"].ToString();

                //if (no_padron != no_padron_recuperado.Trim())
                //{
                //    objMensajeServidor.mensaje = "Número de Patrón Incorrecto ";
                //    str_respuesta = JsonConvert.SerializeObject(objMensajeServidor, Newtonsoft.Json.Formatting.Indented, configuracionJson);
                //    return str_respuesta;
                //}

                //Obj_Pagos_Internet.Actualizar_Correo_Electronico(rpu, correo_electronico);

                str_informacion = Ayudante_JQuery.dataTableToJSONsintotalrenglones(Dt_Datos);
                string no_factura_recibo;
                string referencia;
                decimal total_pagar;
                int numero_factura;

                no_factura_recibo = Dt_Datos.Rows[0]["No_Factura_Recibo"].ToString();
                referencia = Dt_Datos.Rows[0]["Referencia"].ToString();
                total_pagar = Convert.ToDecimal(Dt_Datos.Rows[0]["Total_Pagar"].ToString());
                numero_factura = Int32.Parse(no_factura_recibo);

                var encoding = new System.Text.ASCIIEncoding();

                string llave = "MmuulntiicpiapgioosmBEatnEcpoemCe2r0x12";
                string cadena_proteger = "";
                string cadena_hash;

                cadena_proteger = numero_factura.ToString() + referencia + total_pagar.ToString();

                byte[] keyByte = encoding.GetBytes(llave);
                byte[] messageBytes = encoding.GetBytes(cadena_proteger);

                HMACSHA1 hmacsha1 = new HMACSHA1(keyByte);
                byte[] hashmessage1 = hmacsha1.ComputeHash(messageBytes);
                cadena_hash = ByteToString(hashmessage1);

                objMensajeServidor.dato1 = str_informacion;
                objMensajeServidor.dato2 = cadena_hash.ToLower();
                objMensajeServidor.mensaje = "bien";
            }
        }
        catch (Exception ex)
        {
            objMensajeServidor.mensaje = ex.Message;
        }

        str_respuesta = JsonConvert.SerializeObject(objMensajeServidor, Newtonsoft.Json.Formatting.Indented, configuracionJson);

        return str_respuesta;

    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Consultar_Estado_Cuenta
    ///DESCRIPCIÓN: Consulta los recibos por pagar y los datos del usuario
    ///PARAMETROS:  RPU
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  15/febrero/2016
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private static DataTable Consultar_Estado_Cuenta(string rpu)
    {
        Cls_Ope_Facturacion_Negocio Obj_Negocio = new Cls_Ope_Facturacion_Negocio();

        DataTable Dt_Datos = Crear_Tabla_Estado_Cuenta();
        DataRow Dr_Datos;
        Obj_Negocio.P_Rpu = rpu;
        Decimal Total_Importe = 0;
        Decimal Adeudo = 0;
        Double Total_Pagar = 0;
        DateTime fecha_corte;
        try
        {
            DataTable Dt_Datos_Facturas = Obj_Negocio.Consultar_Facturas_Recibos();
            DataTable Dt_Medidores = Obj_Negocio.Consultar_Medidores();
            DataTable Dt_Usuario = Obj_Negocio.Consultar_Usuarios();
            DataTable Dt_Total = Obj_Negocio.Consultar_Totales();
            Double Periodos_Adeudos = Obj_Negocio.consultar_periodos_Adeudos(rpu);
            double saldo_favor = Obj_Negocio.Obtener_saldo_favor(rpu);
            foreach (DataRow Dr in Dt_Total.Rows)
            {
                Total_Pagar += Convert.ToDouble(Dr["total_saldo"]);
            }
            string dTotal_Pagar = String.Format("{0:0.00}", Math.Round(Total_Pagar, 4));
            DateTime.TryParseExact(Dt_Datos_Facturas.Rows[0]["fecha_corte"].ToString(), "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out fecha_corte);
            Dr_Datos = Dt_Datos.NewRow();
            Dr_Datos[0] = Dt_Datos_Facturas.Rows[0]["No_Cuenta"];
            Dr_Datos[1] = Dt_Usuario.Rows[0]["tarifa"];
            Dr_Datos[2] = Dt_Usuario.Rows[0]["usuario"];
            Dr_Datos[3] = Dt_Usuario.Rows[0]["colonia"];
            Dr_Datos[4] = Dt_Usuario.Rows[0]["calle"] + " No. Ext. " + Dt_Usuario.Rows[0]["exterior"];
            Dr_Datos[5] = Dt_Usuario.Rows[0]["calle"];
            Dr_Datos[6] = Dt_Datos_Facturas.Rows[0]["Codigo_Barras"];
            Dr_Datos[7] = Dt_Usuario.Rows[0]["Ultimo_Bim_Pagado"];
            Dr_Datos[8] = Dt_Datos_Facturas.Rows[0]["referencia"];
            Dr_Datos[9] = Dt_Datos_Facturas.Rows[0]["fecha_emision"];
            Dr_Datos[10] = Dt_Datos_Facturas.Rows[0]["periodo_facturacion"];
            Dr_Datos[11] = Dt_Usuario.Rows[0]["no_sector"] + "/" + Dt_Usuario.Rows[0]["no_ruta"];
            Dr_Datos[12] = Dt_Medidores.Rows[0]["no_medidor"];
            Dr_Datos[13] = Dt_Medidores.Rows[0]["lectura_inicial"];
            Dr_Datos[14] = Dt_Datos_Facturas.Rows[0]["Lectura_Anterior"];
            Dr_Datos[15] = Dt_Datos_Facturas.Rows[0]["Lectura_Actual"];
            Dr_Datos[16] = Dt_Datos_Facturas.Rows[0]["Consumo"];
            Dr_Datos[17] = Dt_Datos_Facturas.Rows[0]["fecha_limite_pago"];
            foreach (DataRow Dr_Aux in Dt_Datos_Facturas.Rows)
            {
                Adeudo += Convert.ToDecimal(Dr_Aux["Adeudo"]);
            }

            //Dr_Datos[18] = String.Format("{0:0.00}", Total_Pagar-Convert.ToDouble(Adeudo));//Dt_Datos_Facturas.Rows[0]["Total_Pagar"];

            Dr_Datos[18] = String.Format("{0:0.00}", Dt_Datos_Facturas.Rows[0]["Total_Importe"]);//Dt_Datos_Facturas.Rows[0]["Total_Pagar"];
            Dr_Datos[19] = Adeudo;
            //Dr_Datos[20] = dTotal_Pagar;
            Dr_Datos[20] = String.Format("{0:0.00}", new SIAC.Facturacion.Datos.Cls_Ope_Cor_Consulta_Usuario().Obtener_Saldo_Facturacion(Dt_Usuario.Rows[0]["no_cuenta"].ToString()));//(Convert.ToDecimal(Dt_Datos_Facturas.Rows[0]["Total_Importe"])) + (Adeudo));
            Dr_Datos[21] = Dt_Datos_Facturas.Rows[0]["Codigo_Barras"];
            Dr_Datos[22] = Dt_Datos_Facturas.Rows[0]["rpu"];
            Dr_Datos[23] = Dt_Datos_Facturas.Rows[0]["no_factura_recibo"];
            Dr_Datos[24] = Dt_Datos_Facturas.Rows[0]["vigente"];
            Dr_Datos[25] = Dt_Usuario.Rows[0]["Predio_id"];
            Dr_Datos[26] = Dt_Datos_Facturas.Rows[0]["bimestre"].ToString() + "-" + Dt_Datos_Facturas.Rows[0]["anio"].ToString();
            //Dr_Datos[27] = fecha_limite.AddDays(10).ToString("dd-MMM-yyyy").ToLower();
            Dr_Datos[27] = fecha_corte.ToString("dd-MMM-yyyy").ToLower();
            Dr_Datos[28] = saldo_favor;//Dt_Usuario.Rows[0]["Saldo_Favor"];
            Dr_Datos[29] = Periodos_Adeudos.ToString();//Dt_Usuario.Rows[0]["Meses_Adeudo"];
            Dr_Datos[30] = "0";//Dt_Usuario.Rows[0]["Adeudo_Anterior"];
            Dt_Datos.Rows.Add(Dr_Datos);
        }
        catch (Exception ex)
        {
            return Dt_Datos = null;

        }
        return Dt_Datos;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Crear_Tabla_Estado_Cuenta
    ///DESCRIPCIÓN: Crear el datatable con los campos para el estado de cuenta
    ///PARAMETROS:  RPU
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  15/febrero/2016
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public static DataTable Crear_Tabla_Estado_Cuenta()
    {
        DataTable Dt_Estado_Cuenta = new DataTable();

        Dt_Estado_Cuenta.Columns.Add("No_Cuenta"); //0
        Dt_Estado_Cuenta.Columns.Add("Tarifa");//1
        Dt_Estado_Cuenta.Columns.Add("Usuario");//2
        Dt_Estado_Cuenta.Columns.Add("Colonia");//3
        Dt_Estado_Cuenta.Columns.Add("Direccion");//4
        Dt_Estado_Cuenta.Columns.Add("Direccion_Corta");//5
        Dt_Estado_Cuenta.Columns.Add("Codigo_Barras");//6
        Dt_Estado_Cuenta.Columns.Add("ultimo_pagado");//7
        Dt_Estado_Cuenta.Columns.Add("Referencia");//8
        Dt_Estado_Cuenta.Columns.Add("Fecha_Emision");//9
        Dt_Estado_Cuenta.Columns.Add("Periodo_Facturacion");//10
        Dt_Estado_Cuenta.Columns.Add("Ruta_Reparto");//11
        Dt_Estado_Cuenta.Columns.Add("MEDIDOR_ID");//12
        Dt_Estado_Cuenta.Columns.Add("Lectora");//13
        Dt_Estado_Cuenta.Columns.Add("Lectura_Anterior");//14
        Dt_Estado_Cuenta.Columns.Add("Lectura_Actual");//15
        Dt_Estado_Cuenta.Columns.Add("Consumo");//16
        Dt_Estado_Cuenta.Columns.Add("Fecha_Limite_Pago");//17
        Dt_Estado_Cuenta.Columns.Add("Importe_Bimestre");//18
        Dt_Estado_Cuenta.Columns.Add("Adeudo");//19
        Dt_Estado_Cuenta.Columns.Add("Total_Pagar");//20
        Dt_Estado_Cuenta.Columns.Add("Numero_Codigo_Barras");//21
        Dt_Estado_Cuenta.Columns.Add("RPU");//22
        Dt_Estado_Cuenta.Columns.Add("No_Factura_Recibo");//23
        Dt_Estado_Cuenta.Columns.Add("Vigente"); //24
        Dt_Estado_Cuenta.Columns.Add("Predio_Id");//25
        Dt_Estado_Cuenta.Columns.Add("Mes_Facturado");//26
        Dt_Estado_Cuenta.Columns.Add("Suspension");//27
        Dt_Estado_Cuenta.Columns.Add("Saldo_Favor");//28
        Dt_Estado_Cuenta.Columns.Add("Meses_Adeudo");//29
        Dt_Estado_Cuenta.Columns.Add("Adeudo_Anterior");//30

        return Dt_Estado_Cuenta;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Consultar_Predio
    ///DESCRIPCIÓN: Consulta el predio del usuario para verificar si se puede generar factura.
    ///PARAMETROS:  Request
    ///CREO:        José Maldonado Méndez
    ///FECHA_CREO:  30/Marzo/2016
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private string Consultar_Predio(HttpRequest Request)
    {

        Cls_Ope_Facturacion_Negocio Negocio = new Cls_Ope_Facturacion_Negocio();
        Cls_Cat_Cor_Predios_Negocio Negocio_Predio = new Cls_Cat_Cor_Predios_Negocio();
        String No_Folio = HttpContext.Current.Request["No_Folio"];
        String Cadena_Resultado = "[]";
        DataTable Dt_Datos;

        try
        {
            Negocio.Codigo_Barras = No_Folio;
            Dt_Datos = Negocio.Consultar_Rpu();
            if (Dt_Datos.Rows.Count > 0)
            {
                Negocio_Predio.P_RPU = Dt_Datos.Rows[0]["RPU"].ToString().Trim();
                DataTable Dt_Datos_Predios = Negocio_Predio.Consultar_Predios_Factura();
                Cadena_Resultado = Ayudante_JQuery.Crear_Tabla_Formato_JSON(Dt_Datos_Predios);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
        return Cadena_Resultado;
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Obtener_Pagos_Orden
    ///DESCRIPCIÓN: Ordenas los montos de los agos y consulta la clave
    ///PARAMETROS:  Total_Efectivo, el monto total del Efectivo.
    ///             Total_Cheque, el monto total de los Cheques.
    ///             Total_Tranferencia, el monto total de las Transferencias.
    ///             Total_Tarjetas, el monto total de las Tarjetas.
    ///             Tipo, el tipo para generar la cadena (PDF o XML)
    ///CREO:        Ana Laura Huichapa Ramírez
    ///FECHA_CREO:  19/Septiembre/2016
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************

    private string Obtener_Pagos_Orden(double Total_Efectivo, double Total_Cheque, double Total_Tranferencia, double Total_Tarjetas, String Tipo, string noRecibo)
    {
        Cls_Cat_Metodos_Pago_Negocio Rs_Negocio = new Cls_Cat_Metodos_Pago_Negocio();
        Rs_Negocio.P_Tipo_Default = "Consultar_Por_Like_Concepto";
        String Regresar = "";
        DataTable Dt_Datos = new DataTable();
        Dt_Datos.Columns.Add("FORMA DE PAGO");
        Dt_Datos.Columns.Add("METODO DE PAGO");
        Dt_Datos.Columns.Add("MONTO");
        Dt_Datos.AcceptChanges();
        DataTable Dt_Claves_Metodo_Pago = new DataTable();
        for (int i = 1; i <= 4; i++)
        {
            DataRow Dr_Datos = Dt_Datos.NewRow();
            switch (i)
            {
                case 1:
                    Dr_Datos["FORMA DE PAGO"] = "EFECTIVO";
                    Dr_Datos["MONTO"] = Total_Efectivo.ToString();
                    Rs_Negocio.P_Nombre = "EFECTIVO";
                    Dt_Claves_Metodo_Pago = Rs_Negocio.Consultar_Metodos_Pago();
                    Dr_Datos["METODO DE PAGO"] = Dt_Claves_Metodo_Pago.Rows[0]["Clave"].ToString();
                    Dt_Datos.Rows.Add(Dr_Datos);
                    Dt_Datos.AcceptChanges();
                    break;
                case 2:
                    Dr_Datos["FORMA DE PAGO"] = "CHEQUE";
                    Dr_Datos["MONTO"] = Total_Cheque.ToString();
                    Rs_Negocio.P_Nombre = "CHEQUE";
                    Dt_Claves_Metodo_Pago = Rs_Negocio.Consultar_Metodos_Pago();
                    Dr_Datos["METODO DE PAGO"] = Dt_Claves_Metodo_Pago.Rows[0]["Clave"].ToString();
                    Dt_Datos.Rows.Add(Dr_Datos);
                    Dt_Datos.AcceptChanges();
                    break;
                case 3:
                    Dr_Datos["FORMA DE PAGO"] = "TRANSFERENCIA";
                    Dr_Datos["MONTO"] = Total_Tranferencia.ToString();
                    Rs_Negocio.P_Nombre = "TRANSFERENCIA";
                    Dt_Claves_Metodo_Pago = Rs_Negocio.Consultar_Metodos_Pago();
                    Dr_Datos["METODO DE PAGO"] = Dt_Claves_Metodo_Pago.Rows[0]["Clave"].ToString();
                    Dt_Datos.Rows.Add(Dr_Datos);
                    Dt_Datos.AcceptChanges();
                    break;
                case 4:
                    Dr_Datos["FORMA DE PAGO"] = "TARJETAS";
                    Dr_Datos["MONTO"] = Total_Tarjetas.ToString();
                    Rs_Negocio.P_No_Recibo = noRecibo;
                    Dt_Claves_Metodo_Pago = Rs_Negocio.Consultar_Metodos_Pago_Tarjetas();

                    if(Dt_Claves_Metodo_Pago.Rows.Count > 0)
                        Dr_Datos["METODO DE PAGO"] = Dt_Claves_Metodo_Pago.Rows[0]["Clave"].ToString();
                    else
                        Dr_Datos["METODO DE PAGO"] = "04";
                    Dt_Datos.Rows.Add(Dr_Datos);
                    Dt_Datos.AcceptChanges();
                    break;
            }
        }
        DataView Dv_Datos = Dt_Datos.DefaultView;
        Dv_Datos.Sort = "MONTO desc";
        Dt_Datos = Dv_Datos.ToTable();

        decimal metodoDePagoMayor = 0;
        foreach (DataRow Dr_Datos in Dt_Datos.Rows)
        {
            if (Double.Parse(Dr_Datos["MONTO"].ToString()) > 0)
            {
                if (Tipo.Equals("XML"))
                {
                    decimal metodoDePagoActual = Decimal.Parse(Dr_Datos["monto"].ToString() );

                    if (metodoDePagoActual > metodoDePagoMayor)
                    {
                        metodoDePagoMayor = metodoDePagoActual;
                        Regresar = Dr_Datos["METODO DE PAGO"].ToString();
                    }

                    //if (Regresar.Length <= 0)
                    //{ Regresar += Dr_Datos["METODO DE PAGO"].ToString(); }
                    //else
                    //{ Regresar += ", " + Dr_Datos["METODO DE PAGO"].ToString(); }

                }
                else if (Tipo.Equals("PDF"))
                {
                    if (Regresar.Length <= 0)
                    {
                        Regresar += Dr_Datos["METODO DE PAGO"].ToString() + " " + Dr_Datos["FORMA DE PAGO"].ToString();
                    }
                    else
                    { Regresar += ", " + Dr_Datos["METODO DE PAGO"].ToString() + " " + Dr_Datos["FORMA DE PAGO"].ToString(); }
                }
            }
        }
        return Regresar;
    }

    private string Obtener_Pagos_Orden(double Total_Efectivo, double Total_Cheque, double Total_Tranferencia, double Total_Tarjetas, String Tipo, string noRecibo, SqlConnection objConexion)
    {
        Cls_Cat_Metodos_Pago_Negocio Rs_Negocio = new Cls_Cat_Metodos_Pago_Negocio();
        Rs_Negocio.P_Tipo_Default = "Consultar_Por_Like_Concepto";
        String Regresar = "";
        DataTable Dt_Datos = new DataTable();
        Dt_Datos.Columns.Add("FORMA DE PAGO");
        Dt_Datos.Columns.Add("METODO DE PAGO");
        Dt_Datos.Columns.Add("MONTO");
        Dt_Datos.AcceptChanges();
        DataTable Dt_Claves_Metodo_Pago = new DataTable();
        for (int i = 1; i <= 4; i++)
        {
            DataRow Dr_Datos = Dt_Datos.NewRow();
            switch (i)
            {
                case 1:
                    Dr_Datos["FORMA DE PAGO"] = "EFECTIVO";
                    Dr_Datos["MONTO"] = Total_Efectivo.ToString();
                    Rs_Negocio.P_Nombre = "EFECTIVO";
                    Dt_Claves_Metodo_Pago = Rs_Negocio.Consultar_Metodos_Pago(objConexion);
                    Dr_Datos["METODO DE PAGO"] = Dt_Claves_Metodo_Pago.Rows[0]["Clave"].ToString();
                    Dt_Datos.Rows.Add(Dr_Datos);
                    Dt_Datos.AcceptChanges();
                    break;
                case 2:
                    Dr_Datos["FORMA DE PAGO"] = "CHEQUE";
                    Dr_Datos["MONTO"] = Total_Cheque.ToString();
                    Rs_Negocio.P_Nombre = "CHEQUE";
                    Dt_Claves_Metodo_Pago = Rs_Negocio.Consultar_Metodos_Pago(objConexion);
                    Dr_Datos["METODO DE PAGO"] = Dt_Claves_Metodo_Pago.Rows[0]["Clave"].ToString();
                    Dt_Datos.Rows.Add(Dr_Datos);
                    Dt_Datos.AcceptChanges();
                    break;
                case 3:
                    Dr_Datos["FORMA DE PAGO"] = "TRANSFERENCIA";
                    Dr_Datos["MONTO"] = Total_Tranferencia.ToString();
                    Rs_Negocio.P_Nombre = "TRANSFERENCIA";
                    Dt_Claves_Metodo_Pago = Rs_Negocio.Consultar_Metodos_Pago(objConexion);
                    Dr_Datos["METODO DE PAGO"] = Dt_Claves_Metodo_Pago.Rows[0]["Clave"].ToString();
                    Dt_Datos.Rows.Add(Dr_Datos);
                    Dt_Datos.AcceptChanges();
                    break;
                case 4:
                    Dr_Datos["FORMA DE PAGO"] = "TARJETAS";
                    Dr_Datos["MONTO"] = Total_Tarjetas.ToString();
                    Rs_Negocio.P_No_Recibo = noRecibo;
                    Dt_Claves_Metodo_Pago = Rs_Negocio.Consultar_Metodos_Pago_Tarjetas(objConexion);

                    if (Dt_Claves_Metodo_Pago.Rows.Count > 0)
                        Dr_Datos["METODO DE PAGO"] = Dt_Claves_Metodo_Pago.Rows[0]["Clave"].ToString();
                    else
                        Dr_Datos["METODO DE PAGO"] = "04";
                    Dt_Datos.Rows.Add(Dr_Datos);
                    Dt_Datos.AcceptChanges();
                    break;
            }
        }
        DataView Dv_Datos = Dt_Datos.DefaultView;
        Dv_Datos.Sort = "MONTO desc";
        Dt_Datos = Dv_Datos.ToTable();

        decimal metodoDePagoMayor = 0;
        foreach (DataRow Dr_Datos in Dt_Datos.Rows)
        {
            if (Double.Parse(Dr_Datos["MONTO"].ToString()) > 0)
            {
                if (Tipo.Equals("XML"))
                {
                    decimal metodoDePagoActual = Decimal.Parse(Dr_Datos["monto"].ToString());

                    if (metodoDePagoActual > metodoDePagoMayor)
                    {
                        metodoDePagoMayor = metodoDePagoActual;
                        Regresar = Dr_Datos["METODO DE PAGO"].ToString();
                    }

                    //if (Regresar.Length <= 0)
                    //{ Regresar += Dr_Datos["METODO DE PAGO"].ToString(); }
                    //else
                    //{ Regresar += ", " + Dr_Datos["METODO DE PAGO"].ToString(); }

                }
                else if (Tipo.Equals("PDF"))
                {
                    if (Regresar.Length <= 0)
                    {
                        Regresar += Dr_Datos["METODO DE PAGO"].ToString() + " " + Dr_Datos["FORMA DE PAGO"].ToString();
                    }
                    else
                    { Regresar += ", " + Dr_Datos["METODO DE PAGO"].ToString() + " " + Dr_Datos["FORMA DE PAGO"].ToString(); }
                }
            }
        }
        return Regresar;
    }

    public static string ByteToString(byte[] buff)
    {
        string sbinary = "";

        for (int i = 0; i < buff.Length; i++)
        {
            sbinary += buff[i].ToString("X2"); // hex format
        }
        return (sbinary);
    }


}