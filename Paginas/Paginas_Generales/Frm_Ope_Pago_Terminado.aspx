﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Pago_Terminado.aspx.cs" Inherits="Paginas_Paginas_Generales_Frm_Ope_Pago_Terminado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Confirmación</title>
    <script type="text/javascript">
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantener_Session.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para mantener la sesión activa
        setInterval('MantenSesion()', '<%=(int)(0.9*(Session.Timeout * 60000))%>');


        function Abrir_Ventana_Modal(Url, Propiedades) {
            window.showModalDialog(Url, null, Propiedades);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Contenido" runat="Server">
    <div>
        <div class="jumbotron">
            <div class="container" style="text-align: left;">
                <asp:Label ID="Lbl_Titulo" runat="server"><h2>Confirmación del Pago en línea</h2></asp:Label>
            </div>
        </div>
        <div class="container">

            <div class="row">

                <div class="col-md-4 col-md-offset-4">

                    <div class="panel panel-danger">
                        <div class="panel-body center-block">
                            <asp:Label ID="lbl_Resultado" Font-Bold="true" Font-Size="Medium" runat="server"> </asp:Label>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>

