﻿<%@ Page Language="C#"  AutoEventWireup="true"  CodeFile="Frm_Facturacion.aspx.cs"  Inherits="Paginas_Paginas_Generales_Frm_Facturacion"
    EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="../../estilos/estilo_paginas.css" rel="stylesheet" />
    <link href="../../JavaScript/jquery-easyiu-1.3.1/themes/default/easyui.css" rel="stylesheet" />
    <link href="../../Estilos/css/bootstrap.css" rel="stylesheet" />
    <link href="../../estilos/Frm_Ope_Facturacion.css" rel="stylesheet" />
    <%--<script src="../../JavaScript/jquery/jquery-1.7.1.js" type="text/javascript"></script>--%>

    <title>Facturacion</title>
</head>
 
<body>
    <form id="form1" runat="server">

        <div class="navbar navbar-default">

            <div class="container">
                <div>&nbsp;</div>
                <table>
                    <tr>
                        <td>
                            <img src="../../imagenes/Logos/img-logo.png" />
                        </td>
                        <td>
                            <asp:Label ID="Lbl_Titulo" runat="server" Style="color: #FFF;">
                                <h2>&nbsp;&nbsp;Facturación Electrónica</h2>
                            </asp:Label>
                        </td>
                    </tr>
                </table>
                <div>&nbsp;</div>
            </div>
        </div>
        <div id="Div_Facturacion_Folio" style="text-align: left;">
            <div class="container">

                <div class="row">

                    <div class="col-md-4 col-md-offset-4">

                        <div class="panel panel-default">
                            <div class="panel-body">

                                <div class="form-group">
                                    <label id="Lbl_Codigo" class="control-label">C&oacute;digo de Facturaci&oacute;n</label>
                                    <asp:TextBox ID="Txt_Folio_Recibo" runat="server" CssClass="form-control" Requerido="true"></asp:TextBox>

                                </div>

                                <a onclick="Consultar_Folio()">
                                    <asp:Label ID="Btn_Aceptar" runat="server" Text="Aceptar" CssClass="btn btn-success" Width="49%"></asp:Label>
                                </a>

                                <asp:Button ID="Btn_Regresar" runat="server" class="btn btn-success" Style="width: 49%" Text="Cancelar"
                                    OnClick="Btn_Regresar_Click" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div id="Div_Factura_Datos" class="container" style="display: none; text-align: left">
            <div class="container">
                <div class="form-group">
                    <asp:Label ID="Lbl_Mensaje" runat="server" CssClass="alert alert-danger center-block" Visible="false"></asp:Label>
                    <asp:Label ID="Lbl_Mensaje_Correcto" runat="server" CssClass="alert alert-success center-block" Visible="false"></asp:Label>

                </div>
                <div style="display: none">
                    <asp:TextBox runat="server" group="Txt_Datos_Factura" ID="Txt_Cliente_ID" />
                    <asp:TextBox runat="server" ID="Txt_Predio_ID" group="Txt_Datos_Factura"></asp:TextBox>
                    <asp:TextBox runat="server" ID="Txt_Estado_ID" group="Txt_Datos_Factura"></asp:TextBox>

                </div>
                <div class="form-group">
                    <div class="form-group">
                        <label class="col-md-3 control-label">*Nombre/Razón Social</label>
                        <div class="col-md-9">
                            <asp:TextBox group="Txt_Datos_Factura Requerido" ID="Txt_Razon" runat="server" TabIndex="1" CssClass="form-control text-uppercase"></asp:TextBox>
                        </div>

                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label">*Calle</label>
                        <div class="col-md-9">
                            <asp:TextBox runat="server" group="Txt_Datos_Factura Requerido" ID="Txt_Calle" TabIndex="2" CssClass="form-control text-uppercase" />

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">*Número Exterior</label>
                        <div class="col-md-4">
                            <asp:TextBox runat="server" group="Txt_Datos_Factura Requerido" ID="Txt_Numero_Exterior" TabIndex="3" CssClass="form-control text-uppercase" />
                        </div>
                        <label class="col-md-2 control-label">Número Interior</label>
                        <div class="col-md-3">
                            <asp:TextBox runat="server" group="Txt_Datos_Factura" ID="Txt_Numero_Interior" TabIndex="4" CssClass="form-control text-uppercase" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">*Colonia</label>
                        <div class="col-md-4">
                            <asp:TextBox runat="server" group="Txt_Datos_Factura Requerido" ID="Txt_Colonia" TabIndex="5" CssClass="form-control text-uppercase" />
                        </div>

                        <label class="col-md-2 control-label">*CP</label>
                        <div class="col-md-3">
                            <asp:TextBox runat="server" group="Txt_Datos_Factura Requerido" ID="Txt_Codigo_Postal" TabIndex="6" CssClass="form-control text-uppercase" MaxLength="5" />
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label">*Estado</label>
                        <div class="col-md-9">
                            <%-- <select id="Cmb_Estados" onchange="Consultar_Ciudades('','','')" class="form-control text-uppercase" tabindex="7" group="Txt_Datos_Factura Requerido"></select>--%>
                            <input type="text" id="Cmb_Estados" class="form-control text-uppercase" tabindex="7" group="Txt_Datos_Factura Requerido" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">*Ciudad</label>
                        <div class="col-md-9">
                            <%--<select id="Cmb_Ciudad" group="Txt_Datos_Factura Requerido" tabindex="8" onchange="Localidad();" class="form-control text-uppercase"></select>--%>
                            <input type="text" id="Cmb_Ciudad" class="form-control text-uppercase" tabindex="8" group="Txt_Datos_Factura Requerido" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Localidad/Delegación</label>
                        <div class="col-md-9">
                            <asp:TextBox runat="server" group="Txt_Datos_Factura" ID="Txt_Localidad" TabIndex="9" CssClass="form-control text-uppercase" />

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">*País</label>
                        <div class="col-md-9">
                            <asp:TextBox runat="server" group="Txt_Datos_Factura Requerido" ID="Txt_Pais" TabIndex="10" CssClass="form-control text-uppercase" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">*RFC</label>
                        <div class="col-md-9">
                            <asp:TextBox runat="server" group="Txt_Datos_Factura" ID="Txt_RFC" TabIndex="11" CssClass="form-control text-uppercase" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">*Email</label>
                        <div class="col-md-9">
                            <asp:TextBox runat="server" group="Txt_Datos_Factura" ID="Txt_Email" TabIndex="12" CssClass="form-control" />
                        </div>
                    </div>

                </div>

                <div>
                    &nbsp;
                </div>
                <div>
                    <%--<a href="#" onclick="Modificar_Datos();" id="Lnk_Modificar" class="btn btn-success">
                        <asp:Label ID="Lbl_Modificar" runat="server" Text="Modificar"></asp:Label>
                    </a>--%>
                    <%--<asp:LinkButton ID ="Lnk_Modificar" runat ="server" CssClass="btn btn-success" Text="Modificar" OnClientClick="Modificar_Datos();"></asp:LinkButton>--%>
                    <a href="#" onclick="Continuar_Detalles();" id="Lnk_Continuar_Detalles" class="btn btn-success">
                        <asp:Label ID="Lbl_Continuar_Detalles" runat="server" Text="Continuar"></asp:Label>
                    </a>
                    <a onclick="Cancelar_Modificacion();">
                        <asp:Label ID="Btn_Cancelar" runat="server" Text="Cancelar" CssClass="btn btn-success"></asp:Label>
                    </a>
                </div>
            </div>
        </div>

        <div id="Div_Factura_Detalles" style="display: none; text-align: left">
            <div class="container">
                <table style="width: 90%; margin-top: 5%; margin-left: 5%;">
                    <tr id="tr_folio_factura">
                        <td style="width: 50%; text-align: right; font-size: 14px; font-family: Arial; font-weight: bold">&nbsp;Folio&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td style="width: 50%; font-size: 14px; font-family: Arial; font-weight: bold; text-align: right;">
                            <asp:TextBox ID="Txt_Folio_Factura" runat="server" class="Txt_Detalle_Factura" Width="98%" Style="background-color: #ACDCF4" Enabled="false" />
                        </td>
                    </tr>
                    <tr id="tr_fecha_pago">
                        <td style="width: 50%; text-align: right; font-size: 14px; font-family: Arial; font-weight: bold">&nbsp;Fecha pago
                        </td>
                        <td style="width: 50%; font-size: 14px; font-family: Arial; font-weight: bold; text-align: right;">
                            <asp:TextBox ID="Txt_Fecha_Pago" runat="server" class="Txt_Detalle_Factura" Width="98%" Style="background-color: #ACDCF4" Enabled="false" />
                        </td>
                    </tr>
                    <tr id="tr_uso_cfdi">
                        <td style="width: 50%; text-align: right; font-size: 14px; font-family: Arial; font-weight: bold">&nbsp;Uso CFDI
                        </td>
                        <td style="width: 50%; font-size: 14px; font-family: Arial; font-weight: bold; text-align: right;">
                            <select name="cars" style="width: 98%" id="select_uso_cfdi">
                                <option disabled selected value> -- Sin uso -- </option>
                                <option value="G01">Adquisición de mercancias</option>
                                <option value="G02">Devoluciones, descuentos o bonificaciones</option>
                                <option value="G03">Gastos en general</option>
                                <option value="I01">Construcciones</option>
                                <option value="I02">Mobilario y equipo de oficina por inversiones</option>
                                <option value="I03">Equipo de transporte</option>
                                <option value="I04">Equipo de computo y accesorios</option>
                                <option value="I05">Dados, troqueles, moldes, matrices y herramental</option>
                                <option value="I06">Comunicaciones telefónicas</option>
                                <option value="I07">Comunicaciones satelitales</option>
                                <option value="I08">Otra maquinaria y equipo</option>
                                <option value="D01">Honorarios médicos, dentales y gastos hospitalarios.</option>
                                <option value="D02">Gastos médicos por incapacidad o discapacidad</option>
                                <option value="D03">Gastos funerales.</option>
                                <option value="D04">Donativos.</option>
                                <option value="D05">Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).</option>
                                <option value="D06">Aportaciones voluntarias al SAR.</option>
                                <option value="D07">Primas por seguros de gastos médicos.</option>
                                <option value="D08">Gastos de transportación escolar obligatoria.</option>
                                <option value="D09">Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.</option>
                                <option value="D10">Pagos por servicios educativos (colegiaturas)</option>
                                <option value="P01">Por definir</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="display: none">
                            <asp:TextBox runat="server" group="Txt_Datos_Factura" ID="Txt_No_Factura" />
                            <asp:TextBox runat="server" group="Txt_Datos_Factura" ID="Txt_Subtotal" />
                            <asp:TextBox runat="server" group="Txt_Datos_Factura" ID="Txt_Subtotal_Iva" />
                            <asp:TextBox runat="server" group="Txt_Datos_Factura" ID="Txt_Total" />
                            <asp:TextBox runat="server" group="Txt_Datos_Factura" ID="Txt_IvA_General" />
                            <input type="hidden" group="Txt_Datos_Factura" id="Txt_Usuario_ID" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="Tbl_Muestra_Facturado"></td>
                    </tr>
                    <tr style="Display: none">
                        <td>
                            <table id="Tbl_Facturado">
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%">
                            <a href="#" class="btn btn-success" plain="true" onclick="Cancelar_Detalles()">Cancelar</a>
                        </td>
                        <td style="width: 50%">
                            <a href="#" class="btn btn-success" plain="true" onclick="Facturar()" id='Lnk_Facturar'>
                                <asp:Label runat="server" ID="Lbl_Facturar" Text="Facturar" />
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
         <%--<asp:ScriptManager ID="ScriptManager1" runat="server" />
        <div id="ventana_mensaje"  class="easyui-window"  collapsible="false" " minimizable="false"
             title="Procesanso Informacion..." maximizable="false"  closable="false"  modal="true" style="text-align: center">
			<asp:UpdatePanel ID="UpdatePanel1" runat="server">
			<ContentTemplate>
			<asp:Timer ID="pollTimer"  Interval="50" runat="server" />
			<img src="../../imagenes/paginas/Updating.gif" alt="The spinner should be here" />
			</ContentTemplate>
			

			</asp:UpdatePanel>
        

        </div>--%>
        
       <%-- <div id="ventana_mensaje" class="easyui-window" collapsible="false" minimizable="false"
            maximizable="false" closable="false" closed="true" modal="true" title="Facturando..."
            style="width: 110px; height: 110px;">
            <img src="../../imagenes/paginas/ajax-loader.gif" alt="." />
        </div>
        <div id="Div_Mensaje" class="easyui-window" collapsible="false" minimizable="false"
            maximizable="false" closable="false" closed="true" modal="true" title="Procesando Información..." style="width: 180px; height: 180px;">
            <img src="../../imagenes/paginas/Updating.gif" alt="." />
        </div>--%>


    </form>
  

         <div id="progressBackgroundFilter" class="progressBackgroundFilter">
            <div  class="processMessage" id="div_progress">
                <img alt="" src="../../imagenes/paginas/Updating.gif" />
            </div>
        </div>
         

    <script src="../../JavaScript/jquery-1.9.1.min.js"></script>  
    <script src="../../JavaScript/js/bootstrap.js" type="text/javascript"></script>
    <script src="../../JavaScript/jquery-easyiu-1.3.1/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../../JavaScript/jquery/json_parse.js" type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="../../JavaScript/Facturacion/Frm_Ope_Facturacion.js" type="text/javascript"></script>
</body>
   
</html>
