﻿var no_cuenta;
var no_padron;
var obj_estado_cuenta;
var estatus_recibo;
var predio_id;
var correo_electronico;

$(function () {

    buscarEstadoCuenta();
    $("#ventanarealizarpago").modal({ backdrop: "static" });
    $("#ventanarealizarpago").modal('hide');
    Bancos();
    //$('.validacion').blur(function () { // inicio evento blur
    //    var campo = $(this);
    //    var tipo = $.trim(campo.attr('tipo'));
    //    var valores = $.trim(campo.attr('valores'));

    //    //validacion numeros
    //    if (tipo == "numeros") {
    //        var respuesta;
    //        respuesta = obtenerCadenaNumeros($.trim(campo.val()), valores);
    //        campo.val(respuesta);
    //    }


    //    //validacion de correo electrónico
    //    if (tipo == "correo") {
    //        if (validarEmail($.trim(campo.val())) == 'incorrecto') {
    //            campo.val('');
    //        }

    //    }



    //    //eventos de foco
    //    $('.validacion').focus(function () { // inicio 
    //        var campo = $(this);
    //        $(campo).css('border-style', 'groove');
    //        $(campo).css('border-color', '');
    //    }); // fin del evento focus


     

    //    });


    // buscar correo electrónico
    //$('#Txt_No_Cuenta').blur(function () {

    //    var numero_cuenta = $(this).val();
    //    alert(numero_cuenta);
    //    if (numero_cuenta.length > 0) {
    //        Buscar_Correo_Electronico(numero_cuenta);
    //    }



    //}); // fin





});

//-----------------------------------------------------------------------------------

function validarEmail(email) { 
var respuesta="incorrecto";

var expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

  if (expr.test(email)) {
      respuesta = "correcto";
  }

  return respuesta;
}

function Consultar_Estado_Cuenta() {
    var Txt_No_Cuenta = $("[id$='Hdf_Rpu']").val();
    var Datos;
    var Total = 0;
    var Obj_Json;
    var Total = $("[id$='Cph_Contenido_Txt_Total_Pagar']").val();
    if (Total == '0' || Total == '0.00' || Total.length == 0) {
        $.messager.alert("SIAC", "Importe en cero", "info");
        return;
    }
    $("#ventanamensaje").modal();
    if (Txt_No_Cuenta.length > 0) {
        window.location = "Frm_Ope_Facturacion_Controlador.aspx?Accion=Consultar_Estado_Cuenta&No_Cuenta=" + Txt_No_Cuenta;
        //        $.ajax({
        //            type: "POST",
        //            url: 'Frm_Ope_Facturacion_Controlador.aspx?Accion=Consultar_Estado_Cuenta&No_Cuenta=' + Txt_No_Cuenta,
        //            contentType: "application/json; charset=utf-8",
        //            dataType: "json",
        //            async: false,
        //            success: function (Datos) {
        //                
        //            },
        //            error: function (Datos) {
        //                Obj_Json = eval("(" + Datos.d + ")");
        //            }
        //        });
        $("#ventanamensaje").modal('hide');
    }
    else {
        $("#ventanamensaje").modal('hide');
        $.messager.alert("SIAC", 'Debes Ingresar un numero de cuenta.');
    }
}

//------------------------------------------------------------------------------------

function obtenerCadenaNumeros(cadena, cadendaExtra) {

    var respuesta = "";
    var letra = "";
    var enter = "\n";

    var caracteres, i;
    caracteres = "1234567890" + enter + cadendaExtra;
    for (i = 0; i < cadena.length; i++) {
        letra = cadena.substring(i, i + 1);
        if (caracteres.indexOf(letra) != -1) {
            respuesta = respuesta + letra
        }
    }
    return respuesta;


}


//-----------------------------------------------------------------------------------

function buscarEstadoCuenta() {

    var rpu = $.trim($("[id$='Hdf_Rpu']").val());
    correo_electronico = $.trim($("[id$='Hdf_Correo']").val());

    if (rpu.length == 0) {
        $.messager.alert('SIAC', 'Escribe un número de Cuenta', 'info');
        return;
    }
    if (correo_electronico.length == 0) {
        $.messager.alert('SIAC', 'Escribe un correo electrónico', 'info');
        return;
    }
    $("#ventanamensaje").modal();
    ////$('#ventana_mensaje').window('open');
    $.ajax({
        type: "POST",
        url: "Frm_Ope_Facturacion_Controlador.aspx/Buscar_Estado_Cuenta",
        data: "{'rpu':'" + rpu + "','correo_electronico':'" + correo_electronico+ "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $("#ventanamensaje").modal('hide');
            //$('#ventana_mensaje').window('close');
            var respuesta = eval("(" + response.d + ")");
            if (respuesta.mensaje == "bien") {
                busquedaExitosa(respuesta);
            }
            else {
                $.messager.alert('SIAC', '' + respuesta.mensaje, 'info');

            }

        },
        error: function (result) {
            $("#ventanamensaje").modal('hide');
            //$('#ventana_mensaje').window('close');
            $.messager.alert('SIAC', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');
        }

    });
}

//-----------------------------------------------------------------------------------

function busquedaExitosa(respuesta) {

    obj_estado_cuenta = json_parse(respuesta.dato1);
    limpiarCajas();

    llenarDatosPagos(obj_estado_cuenta, respuesta);


    $("[id$='Cph_Contenido_Txt_No_Cuenta']").val(obj_estado_cuenta[0].no_cuenta);
    $("[id$='Cph_Contenido_Txt_RPU']").val(obj_estado_cuenta[0].rpu);
    $("[id$='Cph_Contenido_Txt_Usuario']").val(obj_estado_cuenta[0].usuario);
    $("[id$='Cph_Contenido_Txt_Direccion']").val(obj_estado_cuenta[0].direccion);
    $("[id$='Cph_Contenido_Txt_Colonia']").val(obj_estado_cuenta[0].colonia);
    $("[id$='Cph_Contenido_Txt_Direccion_Corta']").val(obj_estado_cuenta[0].direccion_corta);
    $("[id$='Cph_Contenido_Txt_Clasificacion']").val(obj_estado_cuenta[0].tarifa);
    $("[id$='Cph_Contenido_Txt_Referencia']").val(obj_estado_cuenta[0].referencia);

    $("[id$='Cph_Contenido_Txt_Ultimo_Bimestre_Pagado']").val(obj_estado_cuenta[0].ultimo_pagado);
    $("[id$='Cph_Contenido_Txt_Periodo_Aviso']").val(obj_estado_cuenta[0].fecha_emision);
    $("[id$='Cph_Contenido_Txt_Ruta']").val(obj_estado_cuenta[0].ruta_reparto);
    $("[id$='Cph_Contenido_Txt_No_Medidor']").val(obj_estado_cuenta[0].medidor_id);
    $("[id$='Cph_Contenido_Txt_Lectura']").val(obj_estado_cuenta[0].lectora);
    $("[id$='Cph_Contenido_Txt_Lectura_Anterior']").val(obj_estado_cuenta[0].lectura_anterior);
    $("[id$='Cph_Contenido_Txt_Lectura_Actual']").val(obj_estado_cuenta[0].lectura_actual);
    $("[id$='Cph_Contenido_Txt_Consumo']").val(obj_estado_cuenta[0].consumo);
    $("[id$='Cph_Contenido_Txt_Vence']").val(obj_estado_cuenta[0].fecha_limite_pago);
    $("[id$='Cph_Contenido_Txt_Importe_Bimestre']").val(obj_estado_cuenta[0].importe_bimestre);
    $("[id$='Cph_Contenido_Txt_Adeudo']").val(obj_estado_cuenta[0].adeudo);
    $("[id$='Cph_Contenido_Txt_Total_Pagar']").val(obj_estado_cuenta[0].total_pagar);

    estatus_recibo = obj_estado_cuenta[0].vigente;
    predio_id = obj_estado_cuenta[0].predio_id;
    no_cuenta = obj_estado_cuenta[0].no_cuenta;

    //if (estatus_recibo == 'vencido') {
    //    Obtener_Facturacion();
    //}
    //else {
      // $('#ventana_estado_cuenta').window('open');
    //}





}

//-----------------------------------------------------------------------------------------

function buscarEstadoCuenta_segunda() {

    no_cuenta = $.trim($("[id$='Hdf_Predio']").val());
    //no_padron = $.trim($('#Txt_No_Padron').val());
    correo_electronico = $.trim($("[id$='Hdf_Correo']").val());


    if (no_cuenta.length == 0) {
        $.messager.alert('SIAC', 'Escribe un número de Cuenta', 'info');
        return;
    }

    //if (no_padron.length == 0) {
    //    $.messager.alert('SIAC', 'Escribe un número de padrón', 'info');
    //    return;
    //}

    if (correo_electronico.length == 0) {
        $.messager.alert('SIAC', 'Escribe un correo electrónico', 'info');
        return;
    }


    //$('#ventana_mensaje').window('open');
    $.ajax({
        type: "POST",
        url: "Frm_Ope_Facturacion_Controlador.aspx/Buscar_Estado_Cuenta",
        data: "{'no_cuenta':'" + no_cuenta + "','no_padron':'" + "no_padron" + "','correo_electronico':'" + correo_electronico + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //$('#ventana_mensaje').window('close');
            var respuesta = eval("(" + response.d + ")");
            if (respuesta.mensaje == "bien") {
                busquedaExitosa_segunda_parte(respuesta);
            }
            else {
                $.messager.alert('SIAC', ' ' + respuesta.mensaje, 'info');

            }

        },
        error: function (result) {
            //$('#ventana_mensaje').window('close');
            $.messager.alert('SIAC', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

        }

    });


}

//----------------------------------------------------------------------------------------

function busquedaExitosa_segunda_parte(respuesta) {

    obj_estado_cuenta = json_parse(respuesta.dato1);
    limpiarCajas();

    llenarDatosPagos(obj_estado_cuenta, respuesta);

    $("[id$='Cph_Contenido_Txt_No_Cuenta']").val(obj_estado_cuenta[0].no_cuenta);
    $("[id$='Cph_Contenido_Txt_RPU']").val(obj_estado_cuenta[0].rpu);
    $("[id$='Cph_Contenido_Txt_Usuario']").val(obj_estado_cuenta[0].usuario);
    $("[id$='Cph_Contenido_Txt_Direccion']").val(obj_estado_cuenta[0].direccion);
    $("[id$='Cph_Contenido_Txt_Colonia']").val(obj_estado_cuenta[0].colonia);
    $("[id$='Cph_Contenido_Txt_Direccion_Corta']").val(obj_estado_cuenta[0].direccion_corta);
    $("[id$='Cph_Contenido_Txt_Clasificacion']").val(obj_estado_cuenta[0].tarifa);
    $("[id$='Cph_Contenido_Txt_Referencia']").val(obj_estado_cuenta[0].referencia);

    $("[id$='Cph_Contenido_Txt_Ultimo_Bimestre_Pagado']").val(obj_estado_cuenta[0].ultimo_pagado);
    $("[id$='Cph_Contenido_Txt_Periodo_Aviso']").val(obj_estado_cuenta[0].fecha_emision);
    $("[id$='Cph_Contenido_Txt_Ruta']").val(obj_estado_cuenta[0].ruta_reparto);
    $("[id$='Cph_Contenido_Txt_No_Medidor']").val(obj_estado_cuenta[0].medidor_id);
    $("[id$='Cph_Contenido_Txt_Lectura']").val(obj_estado_cuenta[0].lectora);
    $("[id$='Cph_Contenido_Txt_Lectura_Anterior']").val(obj_estado_cuenta[0].lectura_anterior);
    $("[id$='Cph_Contenido_Txt_Lectura_Actual']").val(obj_estado_cuenta[0].lectura_actual);
    $("[id$='Cph_Contenido_Txt_Consumo']").val(obj_estado_cuenta[0].consumo);
    $("[id$='Cph_Contenido_Txt_Vence']").val(obj_estado_cuenta[0].fecha_limite_pago_formato);
    $("[id$='Cph_Contenido_Txt_Importe_Bimestre']").val(obj_estado_cuenta[0].importe_bimestre);
    $("[id$='Cph_Contenido_Txt_Adeudo']").val(obj_estado_cuenta[0].adeudo);
    $("[id$='Cph_Contenido_Txt_Total_Pagar']").val(obj_estado_cuenta[0].total_pagar);

    estatus_recibo = obj_estado_cuenta[0].vigente;
    predio_id = obj_estado_cuenta[0].predio_id;


    //$('#ventana_estado_cuenta').window('open');

}



//-----------------------------------------------------------------------------------------

function limpiarCajas() {
    $("[id$='Cph_Contenido_Txt_No_Cuenta']").val('');
    $("[id$='Cph_Contenido_Txt_RPU']").val('');
    $("[id$='Cph_Contenido_Txt_Usuario']").val('');
    //$('#span_usuario').val('');
    $("[id$='Cph_Contenido_Txt_Direccion']").val('');
    $("[id$='Cph_Contenido_Txt_Colonia']").val('');
    $("[id$='Cph_Contenido_Txt_Direccion_Corta']").val('');
    $("[id$='Cph_Contenido_Txt_Clasificacion']").val('');
    $("[id$='Cph_Contenido_Txt_Referencia']").val('');



    $("[id$='Cph_Contenido_Txt_Ultimo_Bimestre_Pagado']").val('');
    $("[id$='Cph_Contenido_Txt_Periodo_Aviso']").val('');
    $("[id$='Cph_Contenido_Txt_Ruta']").val('');
    $("[id$='Cph_Contenido_Txt_No_Medidor']").val('');
    $("[id$='Cph_Contenido_Txt_Lectura']").val('');
    $("[id$='Cph_Contenido_Txt_Lectura_Anterior']").val('');
    $("[id$='Cph_Contenido_Txt_Lectura_Actual']").val('');
    $("[id$='Cph_Contenido_Txt_Consumo']").val('');
    $("[id$='Cph_Contenido_Txt_Vence']").val('');

    $("[id$='Cph_Contenido_Txt_Importe_Bimestre']").val('');
    $("[id$='Cph_Contenido_Txt_Adeudo']").val('');
    $("[id$='Cph_Contenido_Txt_Total_Pagar']").val('');
    
  
   
}

//--------------------------------------------------------------------------------------

function cerrarEstadoCuenta() {
    $('#Txt_No_Cuenta').val('');
    $('#Txt_No_Padron').val('');
    $('#Txt_Correo_Electronico').val('');
    $('#ventana_estado_cuenta').window('close');

}

//--------------------------------------------------------------------------------------

function llenarDatosPagos(obj_estado_cuenta,respuesta) {
    $('#s_transm').val( parseInt(obj_estado_cuenta[0].no_factura_recibo));
    $('#c_referencia').val(obj_estado_cuenta[0].referencia);
    $('#t_importe').val(obj_estado_cuenta[0].total_pagar);
   
    $('#val_2').val(obj_estado_cuenta[0].usuario);
    //hash
    $('#val_13').val(respuesta.dato2);
    $('#txt_monto_pagar').html(obj_estado_cuenta[0].total_pagar);
    $('#val_11').val($.trim($("[id$='Hdf_Correo']").val()));
}

//--------------------------------------------------------------------------------------

function realizarPago() {
    if (Datos_Validos_pagos()) {
        LlenarDatosTarjeta();
        var Total = $("[id$='Cph_Contenido_Txt_Total_Pagar']").val();

        Total = $.trim(Total);

        var referencia = parseInt($.trim($('#c_referencia').val()));
        if (referencia == 0) {
            $.messager.alert('SIAC', 'Favor de acudir a nuestras instalaciones para revisar sus consumos', 'info');
            return;
        }
        if (Total == '0' || Total == '0.00' || Total.length == 0) {
            $.messager.alert("SIAC", "Importe en cero", "info");
            return;
        }
        if (estatus_recibo == "vencido") {
            $.messager.alert('Siac', 'No se puede pagar porque esta vencido', 'info');
        } else {
            $('#frm_enviar_pagos').submit();
        }
    } else
        return;
}

//---------------------------------------------------------------------------------------

function Obtener_Facturacion(){

    //$('#ventana_mensaje').window('open');
    $.ajax({
        type: "POST",
        url: "Frm_Ope_Facturacion_Controlador.aspx/Elaborar_Refactura",
        data: "{'no_cuenta':'" + no_cuenta + "','predio_id':'" + predio_id + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //$('#ventana_mensaje').window('close');
            var respuesta = eval("(" + response.d + ")");
            if (respuesta.mensaje == "bien") {
                facturacion_exitosa(respuesta);
            }
            else {
                $.messager.alert('SIAC', respuesta.mensaje, 'error', function () {
                    //$('#ventana_estado_cuenta').window('open');
                });

            }

        },
        error: function (result) {
            //$('#ventana_mensaje').window('close');
            $.messager.alert('SIAC', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

        }

    });

}

//---------------------------------------------------------------------

function facturacion_exitosa(respuesta) {
    buscarEstadoCuenta_segunda();

}


//-------------------------------------------------------------------------

function Buscar_Correo_Electronico(numero_cuenta) {

    //$('#ventana_mensaje').window('open');
    $.ajax({
        type: "POST",
        url: "Frm_Ope_Facturacion_Controlador.aspx/Buscar_Correo",
        data: "{'numero_cuenta':'" + numero_cuenta + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //$('#ventana_mensaje').window('close');
            var respuesta = eval("(" + response.d + ")");
            if (respuesta.mensaje == "bien") {
                Busqueda_Exitosa_Correo_Electronico(respuesta);
            }
            else {
                $.messager.alert('SIAC', respuesta.mensaje, 'error', function () {
                    //$('#ventana_estado_cuenta').window('open');
                });

            }

        },
        error: function (result) {
            //$('#ventana_mensaje').window('close');
            $.messager.alert('SIAC', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

        }

    });

}

function Busqueda_Exitosa_Correo_Electronico(respuesta) {
    var correo_electronico;

    correo_electronico = $.trim(respuesta.dato1);

    if(validarEmail(correo_electronico)== 'correcto')
        $('#Txt_Correo_Electronico').val(correo_electronico);

   

}

function MostrarDatosPagos() {
    var Total = $("[id$='Cph_Contenido_Txt_Total_Pagar']").val();

    if (Total == '0' || Total == '0.00' || Total.length == 0) {
        $.messager.alert("SIAC", "Importe en cero", "info");
    } else {
        if (Rezagos_existentes()) {
            $("#ventanarealizarpago").modal();
        } else {
            alert("Cuenta con rezagos, no es posible pagar");
        }
    }
}

function cancelarPago() {
    Limpiar_Datos_Pagos();
    $("#ventanarealizarpago").modal('hide');
}

function Limpiar_Datos_Pagos() {
    $("[id$='Cph_Contenido_Txt_No_Tarjeta']").val('');
    $("[id$='Cph_Contenido_Txt_CVV']").val('');
    $("[id$='Cph_Contenido_txt_monto_pagar']").val('');
    $("[id$='Cph_Contenido_Cmb_Mes']").val(0);
    $("[id$='Cph_Contenido_Cmb_Anio']").val(0);
}

function Bancos() {
    $.ajax({
        url: "Frm_Ope_Facturacion_Controlador.aspx?Accion=Buscar_Bancos",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (Datos) {
            if (Datos != null) {
                //var select = $('#ctl00_Cph_Contenido_Cmb_Banco');
                var select = $("[id$='Cmb_Banco']");
                $('option', select).remove();
                var options = '<option value="Seleccione"><-SELECCIONE-></option>';
                for (var Indice_Banco = 0; Indice_Banco < Datos.length; Indice_Banco++) {
                    options += '<option value="' + Datos[Indice_Banco].BANCO_ID + '">' + Datos[Indice_Banco].NOMBRE.toUpperCase() + '</option>';
                }
                select.append(options);
            }
        },
        error: function (result) {
        }

    });
}

function Datos_Validos_pagos() {
    var Controles = $('[group_pago*=requerido]');
    var Datos_Faltantes = '';
    var Foco = false;
    for (var Indice_Datos = 0; Indice_Datos < Controles.length; Indice_Datos++) {
        if (Indice_Datos < 2) {
            if (Controles[0].value == '' && Controles[1].value == '') {
                Datos_Faltantes += Datos_Faltantes.length < 1 ? Controles[Indice_Datos].id.substring(24, Controles[Indice_Datos].id.length) : ', ' + Controles[Indice_Datos].id.substring(24, Controles[Indice_Datos].id.length);
            }
        }
        else if (Controles[Indice_Datos].value == '' || Controles[Indice_Datos].value == 'Seleccione') {
            if (Controles[Indice_Datos].id == $("[id$='Cmb_Banco']")) {
                Datos_Faltantes += Datos_Faltantes.length < 1 ? 'Banco' : ', ' + 'Banco';
            }
            else {
                Datos_Faltantes += Datos_Faltantes.length < 1 ? 'Banco' : ', ' + 'Banco';
            }
            if (Foco == false) {
                Controles[Indice_Datos].focus();
                Foco = true;
            }
        }
        //else
        //    Mostrar_Imagen_Error(Controles[Indice_Datos].id == 'Cmb_Estados' ? "Img_Wrn_Estado" : "Img_Wrn_" + Controles[Indice_Datos].id.substring(4, Controles[Indice_Datos].id.length), '');
    }// for

    if (Datos_Faltantes.length > 0) {
        $.messager.alert("SIAC", 'Favor de ingresar los siguientes datos:\n' + Datos_Faltantes);
        return false;
    }

    return true;
}

function LlenarDatosTarjeta() {
    var numero_cuenta = $("[id$='Txt_No_Tarjeta']").val();
    var banco_id = $("[id$='Cmb_Banco']").find('option:selected')[0].value.trim();
    var banco = $("[id$='Cmb_Banco']").find('option:selected')[0].innerHTML.trim();
    var autorizar = $("[id$='Txt_No_Aprobacion']").val();
    $('#val_9').val(numero_cuenta);
    $('#n_autoriz').val(autorizar);
    $('#n_banco').val(banco_id);
    $('#banco').val(banco);
}
function Rezagos_existentes() {
    var validacion = true;

    try {

        $.ajax({
            //url: "Frm_Ope_Facturacion_Controlador.aspx?Accion=Consultar_Rezagos&RPU= " + $("[id$='Txt_RPU']").val(),
            url: "Frm_Ope_Facturacion_Controlador.aspx?Accion=Consultar_Rezagos&RPU= " + $("[id$='Txt_Adeudo']").val(),
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Datos) {

                if (Datos.Mensaje == "success") {
                    validacion = false;
                }
            },
            error: function (result) {
                
            }

        });
    } catch (e) {
        
    }
    return validacion;
}