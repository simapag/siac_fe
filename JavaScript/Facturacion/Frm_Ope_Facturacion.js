﻿var queryString = {};
var iva_correcto = 0;
var iva_redondeado = 0;
$(document).ready(function () {
    Habilitar_Controles("Inicial");
    Inicializar_Controles();
    Limpiar_Controles();
    //Consultar_Ciudades();
    //buscarEstadoCuenta();


    if (location.search) location.search.substr(1).split("&").forEach(
        function (item) {
            var s = item.split("="), k = s[0], v = s[1] && decodeURIComponent(s[1]);
            (queryString[k] = queryString[k] || []).push(v);
        });

    if (queryString.codigo_barras) {
        $("[id$='Txt_Folio_Recibo']").val(queryString.codigo_barras);
        Consultar_Folio();
    }

    // var codigo = window.location.search;
    // if (codigo != "")
    // {
    //     var codigo_separado = codigo.split("?codigo_barras=");
    //     $("[id$='Txt_Folio_Recibo']").val(codigo_separado[1]);
    //     Consultar_Folio();
    // }
    //var codigo_separado = codigo.split("codigo_barras");

    jQuery('.progressBackgroundFilter').hide();
    jQuery('.progressBackgroundFilter')
    //.ajaxStart(function () {
    $(document).bind("ajaxStart.mine", function () {
        jQuery('.progressBackgroundFilter').show();
    })
    //.ajaxStop(function () {
    $(document).bind("ajaxStop.mine", function () {
        jQuery('.progressBackgroundFilter').hide();
    });


});


//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Consultar_Ciudades
//DESCRIPCIÓN:      Consulta la ciudades 
//CREO:             José Maldonado Méndez
//FECHA_CREO:       05/02/2016
//*******************************************************************************************************
function Consultar_Ciudades(Estado, ciudad_id, estatus) {

    if (Estado.length == 0) {
        Estado = $('#Cmb_Estados').val();
    }
    $.ajax({
        url: "Frm_Ope_Facturacion_Controlador.aspx?Accion=Consultar_Ciudades&Estado_Id=" + Estado,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (Datos) {
            if (Datos != null) {
                var select = $('#Cmb_Ciudad');
                $('option', select).remove();
                var options = '<option value="Seleccione"><-SELECCIONE-></option>';
                for (var Indice_Ciudades = 0; Indice_Ciudades < Datos.length; Indice_Ciudades++) {
                    options += '<option value="' + Datos[Indice_Ciudades].CIUDAD_ID + '">' + Datos[Indice_Ciudades].NOMBRE.toUpperCase() + '</option>';
                }
                select.append(options);
                if (ciudad_id.length != 0) {

                    if (estatus == 'Facturado') {
                        for (var Indice_Ciudades = $('#Cmb_Ciudad').length; Indice_Ciudades > 0; Indice_Ciudades++) {
                            if ($('#Cmb_Ciudad')[0][Indice_Ciudades].innerHTML.trim() == ciudad_id) {
                                $('#Cmb_Ciudad').val($('#Cmb_Ciudad')[0][Indice_Ciudades].value.trim());
                                break;
                            }
                        }
                    } else
                        $('#Cmb_Ciudad').val(ciudad_id);
                }
            }
        },
        error: function (result) {
        }
    });


}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Inicializar_Controles
//DESCRIPCIÓN:      inicializa los controles de la pagina
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Inicializar_Controles() {
    //    $.ajax({
    //        url: "Frm_Ope_Facturacion_Controlador.aspx?Accion=Consultar_Estados",
    //        type: 'POST',
    //        contentType: "application/json; charset=utf-8",
    //        dataType: "json",
    //        success: function (Datos) {
    //            if (Datos != null) {
    //                var select = $('#Cmb_Estados');
    //                $('option', select).remove();
    //                var options = '<option value="Seleccione"><-SELECCIONE-></option>';
    //                for (var Indice_Estados = 0; Indice_Estados < Datos.length; Indice_Estados++) {
    //                    options += '<option value="' + Datos[Indice_Estados].ESTADO_ID + '">' + Datos[Indice_Estados].NOMBRE.toUpperCase() + '</option>';
    //                }
    //                select.append(options);
    //            }
    //        },
    //        error: function (result) {
    //        }

    //    });

    //Consultar_Ciudades('', '');

    $("[id$='Txt_RFC']").blur(function () {
        $('input[id$=Txt_RFC]').filter(function () {
            $(this).val(obtenerCadenaPermitida($(this).val().toUpperCase(), ''));
            if (this.value.length > 13 || this.value.length < 10)
                $(this).val('');
            else {
                $(this).val(obtenerCadenaPermitida($(this).val().toUpperCase(), ''));
                Mostrar_Imagen_Error("Img_Wrn_" + this.id.substring(4, this.id.length), '');
            }
        });
    });
    $("[id$='Txt_RFC']").keydown(function (event) {
        if (event.keyCode == 32) {
            this.value = this.value.trim();
            return false;
        }
    });
    $("[id$='Txt_Curp']").blur(function () {
        $('input[id$=Txt_Curp]').filter(function () {
            if (!this.value.match(/^[a-zA-Z]{4}(\d{6})([a-zA-Z]{6})(\d{2})?$/))
                $(this).val('');
            else {
                $(this).val($(this).val().toUpperCase());
                Mostrar_Imagen_Error("Img_Wrn_" + this.id.substring(4, this.id.length), '');
            }
        });
    });
    $("[id$='Txt_Curp']").keydown(function (event) {
        if (event.keyCode == 32) {
            this.value = this.value.trim();
            return false;
        }
    });
    $("[id$='Txt_Codigo_Postal']").blur(function () {
        $('input[id$=Txt_Codigo_Postal]').filter(function () {
            if (!this.value.match(/^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/))
                $(this).val('');
        });
    });

    //$("[id$='Txt_Fax']").blur(function () {
    //    $('input[id$=Txt_Fax]').filter(function () {
    //        if (!this.value.match(/^[0-9]{2,3}-? ?[0-9]{6,8}$/))
    //            $(this).val('');
    //        else
    //            Mostrar_Imagen_Error("Img_Wrn_" + this.id.substring(4, this.id.length), '');
    //    });
    //});

    //$("[id$='Txt_Telefono_1']").blur(function () {
    //    $('input[id$=Txt_Telefono_1]').filter(function () {
    //        if (!this.value.match(/^[0-9]{2,3}-? ?[0-9]{5,8}$/))
    //            $(this).val('');
    //        else
    //            Mostrar_Imagen_Error("Img_Wrn_" + this.id.substring(4, this.id.length), '');
    //    });
    //});

    //$("[id$='Txt_Telefono_2']").blur(function () {
    //    $('input[id$=Txt_Telefono_2]').filter(function () {
    //        if (!this.value.match(/^[0-9]{2,3}-? ?[0-9]{5,8}$/))
    //            $(this).val('');
    //        else
    //            Mostrar_Imagen_Error("Img_Wrn_" + this.id.substring(4, this.id.length), '');
    //    });
    //});
    $("[id$='Txt_Email']").blur(function () {
        // $('input[id$=Txt_Email]').filter(function () {
        //     if (!this.value.match(/^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$/))
        //         $(this).val('');
        //     else
        //         Mostrar_Imagen_Error("Img_Wrn_" + this.id.substring(4, this.id.length), '');
        // });

        $('input[id$=Txt_Email]').filter(function () {
            var correos = this.value.split(/,|;/);
            var exito = true;

            for (var i = 0; i < correos.length; i++) {
                var correo = correos[i];

                if (!correo.match(/^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$/))
                    exito = false;
            }

            if (exito)
                Mostrar_Imagen_Error("Img_Wrn_" + this.id.substring(4, this.id.length), '');
            else
                $(this).val('');
        });
    });
    $("[id$='Txt_Email']").keydown(function (event) {
        if (event.keyCode == 32) {
            this.value = this.value.trim();
            return false;
        }

       if (event.keyCode == 13) {
            event.preventDefault();
        }

    });
    Mostrar_Imagen_Error("Img_Wrn_", '');
    $("[id$='Txt_Folio_Recibo']").focus();
    $("[id$='Txt_Folio_Recibo']").keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            Consultar_Folio();
        }
    });
    $('[group*=Txt_Datos_Factura]').keydown(function (event) {
        if (event.keyCode == 13) {
            if (parseFloat(event.currentTarget.tabIndex) == 20)
                Modificar_Datos();
            else
                $('[group*=Txt_Datos_Factura]')[parseFloat(event.currentTarget.tabIndex) + 1].focus();
        }
    });
    //$('[group*=Txt_Datos_Factura]').blur(function () {
    //    this.value = this.value.toUpperCase();
    //});
}

//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Limpiar_Controles
//DESCRIPCIÓN:      Limpia los controles de la pagina
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Limpiar_Controles() {
    $('#Txt_Folio_Recibo').val('');
    $('#Txt_No_Cuenta').val('');
    $('#Txt_No_Factura').val('');
    $('.Txt_Datos_Factura').val('');
    $('.Txt_Detalle_Factura').val('');
    $('#Txt_Usuario_ID').val('');
    var Controles = $('input[group*=Txt_Datos_Factura]');
    $('select[group*=Txt_Datos_Factura]').val("Seleccione");
    Controles.val('');
}

//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Mostrar_Imagen_Error
//DESCRIPCIÓN:      Muestra u oculta la imagen de error en la validacion de los datos
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Mostrar_Imagen_Error(Control, Imagen) {
    $("[id*='" + Control + "']").attr('src', Imagen);
    if (Imagen.length < 1)
        $("[id*='" + Control + "']").attr("width", "0%");
    else
        $("[id*='" + Control + "']").attr("width", "30%");
}

//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Habilitar_Controles
//DESCRIPCIÓN:      habilita los controles de la pagina
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Habilitar_Controles(Accion) {
    var Habilitados = false;
    $('Div[id$=Div_Facturacion_Folio]').css("display", "none");
    $('Div[id$=Div_Factura_Datos]').css("display", "none");
    $('Div[id$=Div_Factura_Detalles]').css("display", "none");
    $('#Lnk_Continuar_Detalles').removeClass('btn btn-danger').addClass('btn btn-success');
    //$('#Lnk_Modificar').removeClass('btn btn-danger').addClass('btn btn-success');
    switch (Accion) {
        case "Inicial":
            $('Div[id$=Div_Facturacion_Folio]').css("display", "Block");
            //$('#Lnk_Modificar').linkbutton({ disabled: false });
            break;
        case "Datos":
            $('Div[id$=Div_Factura_Datos]').css("display", "Block");
            $('#Lnk_Continuar_Detalles').linkbutton({ disabled: false });
            //$("#Lbl_Modificar").text("Modificar");
            //$('#Lnk_Modificar').linkbutton({ disabled: false });
            //$('#Lnk_Modificar').removeClass('btn btn-danger').addClass('btn btn-success');
            $('#Txt_Razon').css("display", "Block");
            break;
        case "Modificar":
            $('Div[id$=Div_Factura_Datos]').css("display", "Block");
            $('#Lnk_Continuar_Detalles').linkbutton({ disabled: true });
            $('#Lnk_Continuar_Detalles').removeClass('btn btn-success').addClass('btn btn-danger');
            //$("#Lbl_Modificar").text("Guardar");
            Habilitados = true;
            break;
        case "Detalles":
            $('Div[id$=Div_Factura_Detalles]').css("display", "Block");
            $("#Lbl_Facturar").text("Facturar");
            break;
    }
    var Controles = $('input[group*=Txt_Datos_Factura]');
    Controles.attr("disabled", !Habilitados);
    $('select[group*=Txt_Datos_Factura]').attr("disabled", !Habilitados);
    if ($('#Txt_No_Factura').val() != '') {
        //$('#Lnk_Modificar').linkbutton({ disabled: true });
        //$('#Lnk_Modificar').removeClass('btn btn-success').addClass('btn btn-danger');

        $("#Lbl_Facturar").text("Reenviar");
    }
    $('#Txt_Email').attr("disabled", false);
}

//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Datos_Validos
//DESCRIPCIÓN:      Funcion que valida que se ingresen todos los datos requeridos
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Datos_Validos() {
    var Controles = $('[group*=Requerido]');
    var Datos_Faltantes = '';
    var Foco = false;
    for (var Indice_Datos = 0; Indice_Datos < Controles.length; Indice_Datos++) {
        if (Indice_Datos < 2) {
            if (Controles[0].value == '' && Controles[1].value == '') {
                Datos_Faltantes += Datos_Faltantes.length < 1 ? Controles[Indice_Datos].id.substring(4, Controles[Indice_Datos].id.length) : ', ' + Controles[Indice_Datos].id.substring(4, Controles[Indice_Datos].id.length);
                Mostrar_Imagen_Error("Img_Wrn_" + Controles[Indice_Datos].id.substring(4, Controles[Indice_Datos].id.length), '../../JavaScript/jquery-easyiu-1.3.1/themes/gray/images/validatebox_warning.png');
            }
        }
        else if (Controles[Indice_Datos].value == '' || Controles[Indice_Datos].value == 'Seleccione') {
            if (Controles[Indice_Datos].id == 'Cmb_Estados') {
                Datos_Faltantes += Datos_Faltantes.length < 1 ? 'Estado' : ', ' + 'Estado';
                Mostrar_Imagen_Error("Img_Wrn_Estado", '../../JavaScript/jquery-easyiu-1.3.1/themes/gray/images/validatebox_warning.png');
            }
            else {
                Datos_Faltantes += Datos_Faltantes.length < 1 ? Controles[Indice_Datos].id.substring(4, Controles[Indice_Datos].id.length) : ', ' + Controles[Indice_Datos].id.substring(4, Controles[Indice_Datos].id.length);
                Mostrar_Imagen_Error("Img_Wrn_" + Controles[Indice_Datos].id.substring(4, Controles[Indice_Datos].id.length), '../../JavaScript/jquery-easyiu-1.3.1/themes/gray/images/validatebox_warning.png');
            }
            if (Foco == false) {
                Controles[Indice_Datos].focus();
                Foco = true;
            }
        }
        else
            Mostrar_Imagen_Error(Controles[Indice_Datos].id == 'Cmb_Estados' ? "Img_Wrn_Estado" : "Img_Wrn_" + Controles[Indice_Datos].id.substring(4, Controles[Indice_Datos].id.length), '');
    }// for
    var txt_rfc = $.trim($('#Txt_RFC').val());

    if (txt_rfc.length < 12 || txt_rfc.length > 13) {

        //Datos_Faltantes += ' La longitud del RFC es incorrecta';
    }
    var txt_correo = $.trim($('#Txt_Email').val());

    if (Datos_Faltantes.length > 0) {
        Swal.fire({            
            icon: 'info',
            html:
                '<strong>Ingresa al siguiente sitio, ' +
                '<b><a href="http://187.174.176.237/tramites">Trámites</a></b> ' +
                'para actualizar y/o registrar tus datos<strong>',
            showCloseButton: false,
            showCancelButton: false,
            focusConfirm: true            
        })
            .then((result) => {
                if (result.isConfirmed) {
                    window.open('http://187.174.176.237/tramites');
                }
            })
            ;
       
        //$.messager.alert("SIAC", 'Favor de ingresar los siguientes datos:\n' + Datos_Faltantes);
        //$.messager.alert("SIAC", 'Ingresa al siguiente sitio 187.174.176.237/tramites para actualizar y/o registrar tus datos');
        return false;
    }
    if (txt_correo.length == 0) {
        $.messager.alert("SIAC", 'Favor de ingresar el correo electronico');
        return false;
    }
    return true;
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Consultar_Folio
//DESCRIPCIÓN:      Consulta los folios facturados
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
var no_factura_correo;

function Consultar_Folio() {

    var Txt_No_Folio = $('#Txt_Folio_Recibo').val();
    var Datos;
    var Total = 0;
    var Obj_Json;
    var Iva_Concepto;
    if (Txt_No_Folio.length >= 1) {
        //$('#Div_Mensaje').window('open');
        //window.print();
        //$.ajax({
        //    type: "POST",
        //    url: 'Frm_Ope_Facturacion_Controlador.aspx?Accion=Consultar_Predio&No_Folio=' + Txt_No_Folio,
        //    contentType: "application/json; charset=utf-8",
        //    dataType: "json",
        //    async: false,
        //    success: function (Datos) {
        //        if (Datos.TOTAL > 0) {
        //            if (Datos.Table[0].Facturar == "SI") {
        //window.showModalDialog('#Div_Mensaje', 'Frm_Faturacion.aspx', '');

        $.ajax({
            type: "POST",
            url: 'Frm_Ope_Facturacion_Controlador.aspx?Accion=Consultar_Datos_Factura&No_Folio=' + Txt_No_Folio,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (Datos) {

                if (Datos.total === undefined)
                {
                    alert(Datos.Mensaje);   
                    return;
                }
                    
                if (Datos.total > 0) {
                    no_factura_correo = Datos.rows[0].no_factura;

                    if (Datos.rows[0].factura == 'USUARIO') {
                        //$('#Div_Mensaje').window('close');

                    }
                    if (Datos.rows[0].factura == 'GLOBAL') {
                        alert('El folio se encuentra dentro de una factura global');
                        $('#Div_Mensaje').window('close');

                    }
                    else {
                        $('#Div_Mensaje').window('close');
                        alert('La factura ya no puede ser generada');
                        Cargar_Datos(Datos, Txt_No_Folio, "Facturado");

                    }

                }
                else {
                    if (Datos.Mensaje != undefined) {
                        var n = Datos.Mensaje.includes("Error relacionado con la red o específico de la instancia mientras se establecía una conexión con el servidor");
                        if (n)
                            alert(Datos.Mensaje);
                        
                    }
                    else
                        Consultar_Datos_Clientes(Txt_No_Folio);
                    //$('#Div_Mensaje').window('close');
                }

            },
            error: function (Datos) {
                //$('#Div_Mensaje').window('close');
                Obj_Json = eval("(" + Datos.d + ")");
            }
        });
        //        }
        //    },
        //    error: function (result) {
        //    }

        //});
        //$('#Div_Mensaje').window('close');
    } else {
        //$('#Div_Mensaje').window('close');
        $.messager.alert("SIAC", 'No se puede generar la factura.');
    }
    //} else {
    //    $.messager.alert('SIAC','No se puede generar la factura');
    //}

    //    },
    //    error: function (Datos) {
    //        Obj_Json = eval("(" + Datos.d + ")");
    //    }
    //});  
    //}
    //else {
    //    $.messager.alert("SIAC", 'El Codigo de facturacion no cumple con la longitud necesaria.');
    //}

}

//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Consultar_Datos_Clientes
//DESCRIPCIÓN:      Consultar los datos fiscales de los usuarios.
//CREO:             Fernando Gonzalez B.
//FECHA_CREO:       24/04/2014
//*******************************************************************************************************
function Consultar_Datos_Clientes(Txt_No_Folio) {
    $.ajax({
        type: "POST",
        url: 'Frm_Ope_Facturacion_Controlador.aspx?Accion=Consultar_Datos_Cliente&No_Folio=' + Txt_No_Folio,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (Datos) {

            if (Datos.total > 0) {
                //if (Datos.rows[0].estatus == "ACTIVO") {
                //    $.messager.alert("SIAC", "El folio ingresado le pertenece a una cuenta. Favor de ingresar al portal.", 'info', function () { window.location.href = '../Login/Frm_Login.aspx'; });
                //} else {
                Cargar_Datos(Datos, Txt_No_Folio, "No_Facturado");
                //}
            }

            //if (Datos.total > 0) {
            //    if (Datos.rows[0].cliente_id != "NO") {
            //        //if (Datos.rows[0].estatus != "ACTIVO") {
            //        //    $('#Txt_Usuario_ID').val(Datos.rows[0].usuario_id);
            //        //    Cargar_Datos(Datos, Txt_No_Folio, "No_Facturado");
            //        //} else {
            //        alert("El folio ingresado le pertenece a una cuenta. Favor de ingresar al portal.", 'info');
            //        window.location.href = '../Login/Frm_Login.aspx';
            //        //}
            //    } else {
            //        $('#Txt_Usuario_ID').val(Datos.rows[0].usuario_id);
            //        Cargar_Datos(Datos, Txt_No_Folio, "No_Facturado");

            //    }
            //}
            else {
                if (Datos.total == 0) {
                    Cargar_Datos_Nuevos(Datos, Txt_No_Folio, "No_Facturado");
                }
                else {
                    $.messager.alert("SIAC", 'El codigo de facturación ingresado no existe, favor de verificarlo.');
                }
            }
        },
        error: function (Datos) {
            Obj_Json = eval("(" + Datos.d + ")");
        }
    });
}


//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Consultar_Estado_Cuenta
//DESCRIPCIÓN:      Consultar el ultimo recibo
//CREO:             Fernando Gonzalez B.
//FECHA_CREO:       24/04/2014
//*******************************************************************************************************
function Consultar_Estado_Cuenta() {
    var Txt_No_Cuenta = $("[id$='Cph_Contenido_Hdf_Rpu']").val();
    var Datos;
    var Total = 0;
    var Obj_Json;
    var Total = $("[id$='Cph_Contenido_Txt_Total_Pagar']").val();
    if (Total == '0' || Total == '0.00') {
        $.messager.alert("SIAC", "Importe en cero", "info");
        return;
    }
    if (Txt_No_Cuenta.length > 0) {
        window.location = "Frm_Ope_Facturacion_Controlador.aspx?Accion=Consultar_Estado_Cuenta&No_Cuenta=" + Txt_No_Cuenta;
        //        $.ajax({
        //            type: "POST",
        //            url: 'Frm_Ope_Facturacion_Controlador.aspx?Accion=Consultar_Estado_Cuenta&No_Cuenta=' + Txt_No_Cuenta,
        //            contentType: "application/json; charset=utf-8",
        //            dataType: "json",
        //            async: false,
        //            success: function (Datos) {
        //                
        //            },
        //            error: function (Datos) {
        //                Obj_Json = eval("(" + Datos.d + ")");
        //            }
        //        });
    }
    else {
        $.messager.alert("SIAC", 'Debes Ingresar un numero de cuenta.');
    }
}

//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Cargar_Datos
//DESCRIPCIÓN:      permite la modificacion de los datos de facturacion
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
var detalles;

function Cargar_Datos(Datos, Txt_No_Folio, Estatus) {
    Datos = Datos.rows[0];
    
    var Tabla;
    var Lineas;
    var Porcetaje_Iva = 0.0;
    var Es_cliente = Datos.cliente_id;
    var mensaje;
    var Url;
    $('#tr_uso_cfdi').show();
    $('#select_uso_cfdi').val(Datos.uso_cfdi);



    if (Datos.cliente_id != 'NO')
        $('#Txt_Cliente_ID').val(Datos.cliente_id);
    if (Estatus == 'Facturado') {
        $('#Txt_Localidad').val(Datos.localidad.toUpperCase());
        //for (var Indice_Estados = $('#Cmb_Estados').length; Indice_Estados > 0; Indice_Estados++) {
        //    if ($('#Cmb_Estados')[0][Indice_Estados].innerHTML.trim() == Datos.estado.trim()) {
        //        $('#Cmb_Estados').val($('#Cmb_Estados')[0][Indice_Estados].value.trim());
        //        break;
        //    }
        //}
        //AnaConsultar_Ciudades(Datos.estado, Datos.ciudad, 'Facturado');Consultar_Folio
        $('#Cmb_Ciudad').val(Datos.ciudad);
        $('#Txt_No_Factura').val(Datos.no_factura.toUpperCase());
        $('#Txt_Folio_Factura').val(Datos.no_factura.toUpperCase());
        $('#tr_fecha_pago').hide();
        $('#tr_uso_cfdi').hide();
        $('#tr_folio_factura').show();
        $('#Cmb_Estados').val(Datos.estado);
    }
    else {

        //AnaConsultar_Ciudades(Datos.estado, Datos.ciudad_id, '');
        $('#Cmb_Ciudad').val(Datos.ciudad);
        $('#Cmb_Estados').val(Datos.nombre_estado);
        $('#Txt_No_Factura').val('');
        $('#tr_folio_factura').hide();
        $('#tr_fecha_pago').show();
    }
    $('#Txt_Estado_ID').val(Datos.estado);
    $('#Txt_Predio_ID').val(Datos.predio_id);
    $('#Txt_Razon').val(Datos.razon_social);
    $('#Txt_RFC').val(obtenerCadenaPermitida(Datos.rfc.toUpperCase(), ''));
    $('#Txt_Pais').val(Datos.pais.toUpperCase());
    $('#Txt_Colonia').val(obtenerCadenaPermitida(Datos.colonia.toUpperCase(), ''));
    $('#Txt_Localidad').val(obtenerCadenaPermitida(Datos.ciudad.toUpperCase()), '');
    $('#Txt_Calle').val(obtenerCadenaPermitida(Datos.calle.toUpperCase()), '');
    $('#Txt_Numero_Exterior').val(Datos.numero_exterior);
    $('#Txt_Numero_Interior').val(Datos.numero_interior);
    $('#Txt_Codigo_Postal').val(Datos.cp);
    $('#Txt_Email').val(Datos.email);


    //una consulta_servidor;

    try {

        $('#Tbl_Facturado').datagrid({
        url: "Frm_Ope_Facturacion_Controlador.aspx",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            pageNumber: 1,
        queryParams: {
            Accion: $('#Txt_No_Factura').val() != '' ? 'Consultar_Detalles_Pago_Facturado' : 'Consultar_Detalles_Pago',
            // Accion: 'Consultar_Detalles_Pago',
            No_Folio: Txt_No_Folio
        },
        onLoadSuccess: function (data) {
            mensaje = data.mensaje;
            detalles = data.rows;
            Datos = $('#Tbl_Facturado').datagrid('getRows');
            if (Datos.length > 0) {
                Tabla = $('#Tbl_Muestra_Facturado');
                $('table', Tabla).remove();
                Porcetaje_Iva = '0';
                Lineas = "<table style='width:100%'>";
                Lineas += "<tr style='background-color: #011826; border-color:#ccc;color:#fff; width:100%;font-size:14px;font-family:Arial;font-weight:bold'>";
                Lineas += "<td style='text-align:center;width:15%'>Unidad de Medida</td><td style='text-align:center;width:15%'>Clave</td><td colspan='2' style='text-align:center;width:52%'>Concepto</td><td style='text-align:center;width:10%'>Total</td></tr>";
                for (var Indice_Datos = 0; Indice_Datos < Datos.length; Indice_Datos++) {
                    iva_correcto += parseFloat(Datos[Indice_Datos].ivaCorrecto);
                    iva_redondeado += parseFloat(Datos[Indice_Datos].iva);
                    //if (Datos[Indice_Datos].clave_concepto == '00159') {
                    //    Iva += parseFloat(Datos[Indice_Datos].importe);
                    //    continue;
                    //}

                    // if(parseFloat(Datos[Indice_Datos].importe) === 0)
                    //     continue;

                    Lineas += '<tr style="font-size:14px;font-family:Arial;text-align:center;color:#000" ><td style="text-align:center">' +
                        Datos[Indice_Datos].Unidad_de_Medida + '</td><td colspan="2" style="text-align:center">' + Datos[Indice_Datos].clave_concepto + '</td><td>' + Datos[Indice_Datos].descripcion +
                        '</td><td style="text-align:right">';
                    Lineas += formatNumber.new(parseFloat(Datos[Indice_Datos].importe).toFixed(2), '$') + '</td></tr>';
                    //Subtotal += parseFloat(Datos[Indice_Datos].importe);
                    //Iva_Concepto = (parseFloat(Datos[Indice_Datos].iva) * 100) / parseFloat(Datos[Indice_Datos].importe);
                    //if (Datos[Indice_Datos].iva != 0 && Iva_Concepto > parseFloat(Porcetaje_Iva))
                    //  Porcetaje_Iva = Iva_Concepto;
                    //if ($('#Txt_Fecha_Pago').val() == "")
                        //$('#Txt_Fecha_Pago').val(Datos[Indice_Datos].FECHA_MOVIMIENTO);
                    //if (Datos[Indice_Datos].lleva_iva == "SI")
                    //Porcetaje_Iva = Datos[Indice_Datos].Porcentaje_IVA;
                    //Total_Factura += parseFloat(Datos[Indice_Datos].importe);

                    //Iva = parseFloat(Datos[Indice_Datos].impuestoGlobal);
                    

                }
                
                //Tabla.append(Lineas);
                





                //Generales
                //Consultar_Generales_Pago
                $.ajax({ //inicio
                    type: "POST",
                    url: "Frm_Ope_Facturacion_Controlador.aspx/Consultar_Generales_Pago",
                    data: "{'No_Folio':'" + Txt_No_Folio + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        $('#Div_Mensaje').window('close');
                        var respuesta = eval("(" + response.d + ")");
                        if (respuesta.mensaje == "bien") {
                            Mostrar_Generales_Recibo(Lineas,respuesta);
                            Habilitar_Controles("Datos");
                            $('#Lnk_Continuar_Detalles').focus();

                        } else {
                            $('#ventana_mensaje').window('close');
                            $.messager.alert('SIAC', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                        }
                    },
                    error: function (result) {
                        $('#Div_Mensaje').window('close');
                        alert("ERROR " + result.status + ' ' + result.statusText);

                    }
                });  //fin peticion ajax
            }
        },
        onLoadError: function (Datos, textStatus, errorThrown) {
            $('#Div_Mensaje').window('close');
            var mensaje = $.trim(Datos.responseText);

            if (mensaje == "error_fecha") {
                $.messager.alert('SIAC', 'El periodo de facturación ha expirado', 'info');

            } else {
                $.messager.alert('SIAC', mensaje, 'info');
                //Obj_Json = eval("(" + Datos.d + ")");
            }
        }
            });        
    }
    catch (err) {
        $.messager.alert('SIAC', err, 'info');
    }
}

function Mostrar_Generales_Recibo(Lineas, respuesta) {
    var Subtotal = 0;
    var Iva = 0;
    var Total = 0;
    var Porcentaje_Iva = 0;
    Tabla = $('#Tbl_Muestra_Facturado');
    var Obj_Json = eval(respuesta.dato1);
    $('#Txt_Fecha_Pago').val(Obj_Json[0].fecha);

    Subtotal = parseFloat(Obj_Json[0].subtotal);
    Total = parseFloat(Obj_Json[0].total);
    Iva = iva_redondeado;//parseFloat(Obj_Json[0].iva);
    Porcentaje_Iva = Obj_Json[0].porcentaje_iva;
    ////$('#Tbl_Facturado').css("display", "none");
    //$('#Txt_Subtotal_Iva').val(Iva.toFixed(2));
    
    //Subtotal = 0;
    //Iva = 0;
    //Total_Factura = 0;

    //Subtotal = Total_Factura - Iva;
    //Subtotal = RedondearDecimales(Subtotal);
    //Iva = RedondearDecimales(Iva);
    //Total_Factura = parseFloat(Subtotal + Iva);
    Lineas += "<tr style='width:100%; font-size: 14px; font-family: Arial; text-align: center; color: #000'> <td></td> <td></td> <td></td> <td></td> <td></td> </tr>";
    Lineas += "<tr style='font-size:14px;font-family:Arial;font-weight:bold'>";
    Lineas += "<td></td> <td></td> <td></td> <td style='text-align:right'>Subtotal: </td> <td colspan='2' style='text-align:right'>" + formatNumber.new(Subtotal.toFixed(2), '$') + "</td> </tr>";
    Lineas += "<tr style='font-size:14px;font-family:Arial;font-weight:bold'>";
    Lineas += "<td></td> <td></td> <td></td> <td style='text-align:right'>" + Porcentaje_Iva + " % iva:</td><td colspan='2' style='text-align:right'>" + formatNumber.new(Iva.toFixed(2), '$') + "</td> </tr>";
    Lineas += "<tr style='font-size:14px;font-family:Arial;font-weight:bold'>";
    Lineas += "<td></td> <td></td> <td></td> <td style='text-align:right'>Total: </td> <td colspan='2' style='text-align:right'>" + formatNumber.new(Total.toFixed(2), '$') + "</td> </tr>";
    Lineas += '</table>';
    Tabla.append(Lineas);
    Lineas = "";
    //Se guardan los valores en cajas de texto ocultas
    $('#Txt_Subtotal').val(Subtotal);
    $('#Txt_Total').val(Total);
    $('#Txt_IvA_General').val(iva_correcto);
    iva_correcto = 0;
    iva_redondeado = 0;
}

//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Cargar_Datos_Nuevos
//DESCRIPCIÓN:      permite la modificacion de los datos de facturacion
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Cargar_Datos_Nuevos(Datos, Txt_No_Folio, Estatus) {
    var Subtotal = 0;
    var Iva = 0;
    var Total_Factura = 0;
    var Tabla;
    var Lineas;
    var Porcetaje_Iva = 0.0;
    Datos = Datos.rows[0];
    $('#Txt_Cliente_ID').val();
    if (Estatus == 'Facturado') {
        $('#Txt_Localidad').val();
        for (var Indice_Estados = $('#Cmb_Estados').length; Indice_Estados > 0; Indice_Estados++) {
            if ($('#Cmb_Estados')[0][Indice_Estados].innerHTML.trim() == Datos.estado.trim()) {
                $('#Cmb_Estados').val($('#Cmb_Estados')[0][Indice_Estados].value.trim());
                break;
            }
        }
        //AnaConsultar_Ciudades(Datos.estado, Datos.ciudad, 'Facturado');
        $('#Txt_No_Factura').val();
    }
    else {
        //$('#Txt_Nombre').val();
        $('#Cmb_Estados').val();
        //AnaConsultar_Ciudades(Datos.estado, Datos.ciudad, '');
    }
    $('#Txt_Razon').val();
    $('#Txt_RFC').val();
    $('#Txt_Curp').val();
    $('#Txt_Pais').val();
    $('#Cmb_Ciudad').val();
    $('#Txt_Colonia').val();
    $('#Txt_Localidad').val();
    $('#Txt_Calle').val();
    $('#Txt_Numero_Exterior').val();
    $('#Txt_Numero_Interior').val();
    $('#Txt_Codigo_Postal').val();
    //$('#Txt_Extension').val();
    //$('#Txt_Nextel').val();
    $('#Txt_Email').val();
    //$('#Txt_Fax').val();
    //$('#Txt_Telefono_1').val();
    //$('#Txt_Telefono_2').val();
    //$('#Txt_Sitio_Web').val();
    $('#Tbl_Facturado').datagrid({
        url: 'Frm_Ope_Facturacion_Controlador.aspx',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        pageNumber: 1,
        queryParams: {
            Accion: 'Consultar_Detalles_Pago',
            No_Folio: Txt_No_Folio
        },
        onLoadSuccess: function (data) {
            Datos = $('#Tbl_Facturado').datagrid('getRows');
            if (Datos.length > 0) {
                Tabla = $('#Tbl_Muestra_Facturado');
                $('table', Tabla).remove();
                Porcetaje_Iva = '16';
                Lineas = "<table>";
                Lineas += "<tr style='background-color: #011826; border-color:#ccc;color:#fff;width:100%;font-size:14px;font-family:Arial;font-weight:bold'>";
                Lineas += "<td style='text-align:center;width:15%'>Unidad de Medida</td><td style='text-align:center;width:15%'>Clave</td><td style='text-align:center;width:52%'>Concepto</td><td style='text-align:center;width:10%'>Total</td></tr>";
                for (var Indice_Datos = 0; Indice_Datos < Datos.length; Indice_Datos++) {
                    Lineas += '<tr style="font-size:14px;font-family:Arial;text-align:center"><td style="text-align:center">' +
                        Datos[Indice_Datos].Unidad_de_Medida + '</td><td style="text-align:center">' +
                        Datos[Indice_Datos].clave_concepto + '</td><td>' +
                        Datos[Indice_Datos].descripcion +
                        '</td><td style="text-align:right">';
                    Lineas += formatNumber.new(parseFloat(Datos[Indice_Datos].importe).toFixed(2), '$') + '</td>'
                    //'<td style="text-align:right">' +
                    //formatNumber.new(parseFloat(Datos[Indice_Datos].iva), '$') + '</td><td style="text-align:right">' +
                    //formatNumber.new(parseFloat(Datos[Indice_Datos].total), '$') + '</td>
                    Lineas += '</tr>';
                    Subtotal += parseFloat(Datos[Indice_Datos].total);
                    Iva = parseFloat(Datos[Indice_Datos].iva);
                    Iva_Concepto = (parseFloat(Datos[Indice_Datos].iva) * 100) / parseFloat(Datos[Indice_Datos].importe);
                    //                    if (Datos[Indice_Datos].iva != 0 && Iva_Concepto > parseFloat(Porcetaje_Iva))
                    //                        Porcetaje_Iva = Iva_Concepto;
                    if ($('#Txt_Fecha_Pago').val() == "")
                        $('#Txt_Fecha_Pago').val(Datos[Indice_Datos].FECHA_MOVIMIENTO);
                    //if (Datos[Indice_Datos].lleva_iva == "SI")
                    Porcetaje_Iva = Datos[Indice_Datos].Porcentaje_IVA;
                }
                Subtotal = Subtotal;
                Iva = RedondearDecimales(Iva);
                Total_Factura = Subtotal + Iva;

                Lineas += "<tr><td></td> <td></td> <td></td> <td></td> <td></td></tr>";
                Lineas += "<tr style='font-size:12px;font-family:Arial;font-weight:bold'>";
                Lineas += "<td></td><td></td><td></td><td style='text-align:right'>Subtotal: </td><td colspan='2' style='text-align:right'>" + formatNumber.new(Subtotal, '$') + "</td></tr>";
                Lineas += "<tr style='font-size:12px;font-family:Arial;font-weight:bold'>";
                Lineas += "<td></td><td></td><td></td><td style='text-align:right'> " + Porcetaje_Iva + "% iva: </td><td colspan='2' style='text-align:right'>" + formatNumber.new(Iva, '$') + "</td></tr>";
                Lineas += "<tr style='font-size:12px;font-family:Arial;font-weight:bold'>";
                Lineas += "<td></td><td></td><td></td><td style='text-align:right'>Total: </td><td colspan='2' style='text-align:right'>" + formatNumber.new(Total_Factura.toFixed(2), '$') + "</td></tr>";
                Lineas += '</table>';
                Tabla.append(Lineas);
                $('#Tbl_Facturado').css("display", "none");
                $('#Txt_Subtotal').val(Subtotal);
                $('#Txt_Subtotal_Iva').val(Iva);
                $('#Txt_Total').val(Total_Factura.toFixed(2));
                $('#Txt_IvA_General').val(Porcetaje_Iva);
                Subtotal = 0;
                Iva = 0;
                Total_Factura = 0;
                Habilitar_Controles("Modificar");
                $('#Lnk_Continuar_Detalles').focus();
            }
        },
        onLoadError: function (Datos) {
            $.messager.alert('SIAC', "El folio " + Txt_No_Folio + " ingresado no existe");
            Obj_Json = eval("(" + Datos.d + ")");
        }
    });
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   RedondearDecimales
//DESCRIPCIÓN:      Redondea los numeros a dos decimales 
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function RedondearDecimales(Numero) {
    return +(Math.round(Numero + "e+2") + "e-2");
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Modificar_Datos
//DESCRIPCIÓN:      permite la modificacion de los datos de facturacion
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Modificar_Datos() {
    //var Accion = $("#Lnk_Modificar").text();
    var Controles;
    var Datos;
    //if (Accion == "Modificar") {
    //    Habilitar_Controles("Modificar");
    //    //$("[id$='Txt_Nombre']").focus();
    //}
    //else {
    if ($('#Txt_Cliente_ID').val() != "") {
        if (Datos_Validos() == true) {
            Controles = $('input[group*=Txt_Datos_Factura]');
            Datos = "{[";
            for (var Indice_Datos = 0; Indice_Datos < Controles.length; Indice_Datos++) {
                Datos += Controles[Indice_Datos].id + ":" + Controles[Indice_Datos].value.replace(';', '') + ";";
            }
            Datos += "No_Folio:" + $('#Txt_Folio_Recibo').val() + ";";
            Datos += "Estado_ID:" + $('#Txt_Estado_ID').val() + ";";
            Datos += "Estado:" + $('#Cmb_Estados').val() + ";";
            //Datos += "Ciudad:" + $('#Cmb_Ciudad').find('option:selected')[0].value.trim();
            Datos += "Ciudad:" + $('#Cmb_Ciudad').val();
            Datos += "]}";
            $.ajax({
                type: "POST",
                url: 'Frm_Ope_Facturacion_Controlador.aspx?Accion=Modificar_Datos_Cliente&Datos=' + Datos,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (Respuesta) {
                    if (Respuesta.Mensaje == 'Modificacion') {
                        //$.messager.alert("SIAC", "La modificación de los datos se realizó correctamente");
                        //Habilitar_Controles("Datos");
                        //$('#Lnk_Continuar_Detalles').focus();
                    }
                    else {
                        $.messager.alert("SIAC", Respuesta.Mensaje)
                    }
                },
                error: function (Respuesta) {
                    $.messager.alert("SIAC", "La modificación de los datos no se realizó");
                }
            });
        }
        //}
        //else {
        //    Alta_Cliente()
        //}
        //if ($('#Txt_Usuario_ID').val().trim() != '')
        //    Modificar_Datos_Usuarios();
    }
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Alta_Cliente
//DESCRIPCIÓN:      permite la modificacion de los datos de facturacion
//CREO:             Miguel Angel Alvarado Enriquez
//FECHA_CREO:       18/Feb/2014
//*******************************************************************************************************
function Alta_Cliente() {
    //var Accion = $("#Lbl_Modificar").text();
    var Controles;
    var Datos;
    if (Datos_Validos() == true) {
        Controles = $('input[group*=Txt_Datos_Factura]');
        Datos = "{[";
        for (var Indice_Datos = 0; Indice_Datos < Controles.length; Indice_Datos++) {
            Datos += Controles[Indice_Datos].id + ":" + Controles[Indice_Datos].value + ",";
        }
        Datos += "No_Folio : " + $('#Txt_Folio_Recibo').val() + ",";
        Datos += "Estado_ID : " + $('#Txt_Estado_ID').val() + ",";
        Datos += "Estado : " + $('#Cmb_Estados').val() + ",";
        //Datos += "Ciudad : " + $('#Cmb_Ciudad').find('option:selected')[0].value.trim();
        Datos += "Ciudad : " + $('#Cmb_Ciudad').val();
        Datos += "]}";
        $.ajax({
            type: "POST",
            url: 'Frm_Ope_Facturacion_Controlador.aspx?Accion=Alta_Datos_Cliente&Datos=' + Datos,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Respuesta) {
                if (Respuesta.Mensaje == 'Alta') {
                    $('#Txt_Cliente_ID').val(Respuesta.Cliente_Id);
                    //$.messager.alert("SIAC", "Los datos se han guardado correctamente");
                    //Habilitar_Controles("Datos");
                    //$('#Lnk_Continuar_Detalles').focus();
                }
            },
            error: function (Respuesta) {
                $.messager.alert("SIAC", "Los datos se han guardado correctamente");
            }
        });
    }
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Cancelar_Modificacion
//DESCRIPCIÓN:      permite cancelar la modificacion de los datos del cliente
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Cancelar_Modificacion() {
    //var Accion = $("#Lbl_Modificar").text();
    //if (Accion == "Guardar") {
    //    Consultar_Folio();
    //    Mostrar_Imagen_Error("Img_Wrn_", '');
    //}
    //else {
    Limpiar_Controles();
    Habilitar_Controles("Inicial");
    $("[id$='Txt_Folio_Recibo']").focus();
    //}
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Continuar_Detalles
//DESCRIPCIÓN:      permite avanzar al div que muestra la informacion detallada de la factura
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Continuar_Detalles() {

    if (Datos_Validos() == true) {
        Habilitar_Controles("Detalles");
        var Txt_No_Folio = $('#Txt_Folio_Recibo').val();
        var Txt_factura = $('#Txt_No_Factura').val();
        var Txt_Cliente = $('#Txt_Cliente_ID').val();
        if (Txt_factura.length == 0) {
            if (Txt_Cliente.length == 0)
                Alta_Cliente();
            else
                Modificar_Datos();

        }
        $('#Lnk_Facturar').focus();


    }
    //else
    //    $('#Lnk_Modificar').focus();

}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Continuar_Detalles
//DESCRIPCIÓN:      permite avanzar al div que muestra la informacion detallada de la factura
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Cancelar_Detalles() {
    Habilitar_Controles("Datos");
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   obtenerCadenaPermitida
//DESCRIPCIÓN:      Obtiene la cadena con el formato permitido
//CREO:             Miguel AnAlvarado Enriquez
//FECHA_CREO:       31/Marzo/2014
//*******************************************************************************************************
function obtenerCadenaPermitida(cadena, cadendaExtra) {

    var respuesta = "";
    var letra = "";
    var enter = "\n";

    var caracteres, i;
    caracteres = "abcdefghijklmnopqrstuvwxyz1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZ áéíóúñ ÁÉÍÓÚÑ" + enter + cadendaExtra;
    for (i = 0; i < cadena.length; i++) {
        letra = cadena.substring(i, i + 1);
        if (caracteres.indexOf(letra) != -1) {
            respuesta = respuesta + letra
        }
    }
    return respuesta;

}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Inicializar_Controles
//DESCRIPCIÓN:      inicializa los controles de la pagina
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Facturar() {
    var Txt_No_Folio = $('#Txt_Folio_Recibo').val();
    var Ruta_PDF_Enviar = "";

    var uso_cfdi = $('#select_uso_cfdi').val();

    if (!uso_cfdi) {
        alert('Seleccione el uso que se le dará al CFDI');
        return;
    }

    //$('#ventana_mensaje').window('open');
    $.ajax({
        url: "Frm_Ope_Facturacion_Controlador.aspx?Accion=Imprimir_Factura&No_Folio=" + Txt_No_Folio + "&Subtotal=" + $('#Txt_Subtotal').val() + "&Nombre=" + $('#Txt_Razon').val() + "&RFC=" + obtenerCadenaPermitida($('#Txt_RFC').val(), '') +
        "&Subtotal_Iva=" + $('#Txt_Subtotal_Iva').val() + "&Total=" + $('#Txt_Total').val() + "&Iva_General=" + $('#Txt_IvA_General').val() + "0" + "&Email=" + $('#Txt_Email').val() +
        "&Cliente_Id=" + $('#Txt_Cliente_ID').val() + "&No_Factura_Correo=" + no_factura_correo + "&empleado=" + (queryString.empleado ? queryString.empleado : '') + "&uso_cfdi=" + uso_cfdi,
        type: 'POST',
        data: { detalles: JSON.stringify(detalles) },
        // contentType: "application/json; charset=utf-8",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        success: function (Datos) {
            if (Datos != null) {
                if (Datos.Mensaje.includes('Ocurrio un error al dar de alta factura')) {
                    $.messager.alert('SIAC', Datos.Mensaje, 'error');
                    return;
                }
                //$('#ventana_mensaje').window('close');
                Habilitar_Controles("Inicial");
                Inicializar_Controles();
                Limpiar_Controles();
                Ruta_PDF_Enviar = Datos.Ruta_Corta;

                //alert(Datos.Mensaje);
                $.messager.alert('SIAC', Datos.Mensaje, 'info', function () {
                    if (Ruta_PDF_Enviar === undefined) {

                    } else {
                        window.open(Ruta_PDF_Enviar, "resizeable,scrollbar");

                    }
                });
            }
        },
        error: function (result) {
        }

    });
}
var formatNumber = {
    separador: ",", // separador para los miles
    sepDecimal: '.', // separador para los decimales
    formatear: function (num) {
        num += '';
        var splitStr = num.split('.');
        var splitLeft = splitStr[0];
        var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
        var regx = /(\d+)(\d{3})/;
        while (regx.test(splitLeft)) {
            splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
        }
        return this.simbol + splitLeft + splitRight;
    },
    new: function (num, simbol) {
        this.simbol = simbol || '';
        return this.formatear(num);
    }
}
function Localidad() {
    var Ciudad = $('#Cmb_Ciudad option:selected').text();
    $('#Txt_Localidad').val(Ciudad);
}

//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Modificar_Datos_Usuarios
//DESCRIPCIÓN:      permite la modificacion de los datos de usuarios de facturacion
//CREO:             José Maldonado Méndez
//FECHA_CREO:       04/Abril/2016
//*******************************************************************************************************
function Modificar_Datos_Usuarios() {
    var Accion = $("#Lnk_Modificar").text();
    var Controles;
    var Datos;
    if (Accion == "Modificar") {
        Habilitar_Controles("Modificar");
        //$("[id$='Txt_Nombre']").focus();
    }
    else {
        if ($('#Txt_Cliente_ID').val() != "") {
            if (Datos_Validos() == true) {
                Controles = $('input[group*=Txt_Datos_Factura]');
                Datos = "{[";
                for (var Indice_Datos = 0; Indice_Datos < Controles.length; Indice_Datos++) {
                    Datos += Controles[Indice_Datos].id + ":" + Controles[Indice_Datos].value.replace(';', '') + ";";
                }
                Datos += "No_Folio:" + $('#Txt_Folio_Recibo').val() + ";";
                Datos += "Estado_ID:" + $('#Cmb_Estados').find('option:selected')[0].value.trim() + ";";
                Datos += "Estado:" + $('#Cmb_Estados').find('option:selected')[0].innerHTML.trim() + ";";
                Datos += "Ciudad_ID:" + $('#Cmb_Ciudad').find('option:selected')[0].value.trim() + ";";
                Datos += "Ciudad:" + $('#Cmb_Ciudad').find('option:selected')[0].innerHTML.trim();
                Datos += "]}";
                $.ajax({
                    type: "POST",
                    url: 'Frm_Ope_Facturacion_Controlador.aspx?Accion=Modificar_Datos_Usuarios&Datos=' + Datos,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    success: function (Respuesta) {
                        if (Respuesta.Mensaje == 'Modificacion') {
                            $.messager.alert("SIAC", "La modificación de los datos se realizó correctamente");
                            Habilitar_Controles("Datos");
                            $('#Lnk_Continuar_Detalles').focus();
                        }
                        else {
                            $.messager.alert("SIAC", Respuesta.Mensaje)
                        }
                    },
                    error: function (Respuesta) {
                        $.messager.alert("SIAC", "La modificación de los datos no se realizó");
                    }
                });
            }
        }
    }
}

