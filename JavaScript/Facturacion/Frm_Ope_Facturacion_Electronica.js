﻿$(function () {
    Habilitar_Controles("Inicial");
    Inicializar_Controles();
    Limpiar_Controles();
    crearTablaFacturas();
    tablaFacturasSelectRow();
});

function tablaFacturasSelectRow() {
    $('#table-pagination').on('click-row.bs.table', function (e, row, $element) {

        $("[id$='Cph_Contenido_Txt_Folio_Recibo']").val(row.codigo_barras);
        Consultar_Folio();
    });
}

function crearTablaFacturas() {
    var predio = $("[id$='Hdf_Predio']").val();
    $('#table-pagination').bootstrapTable({
        url: 'Frm_Ope_Facturacion_Controlador.aspx?Accion=Consultar_Facturas&Predio=' + predio,
        pagination: true,
        cache: false,
        height: 400,
        striped: true,
        pagination: true,
        pageSize: 10,
        search: true,
        pageList: [10, 25, 50, 100, 200],
        columns: [{ field: 'no_recibo', title: 'No Recibo', sortable: "true", width: "5%" },
        { field: 'no_factura', title: 'No Factura', sortable: "true", width: "10%" },
        { field: 'codigo_barras', title: 'Folio', sortable: "true", width: "20%" },
        { field: 'fecha', title: 'Fecha', sortable: "true", width: "20%" },
        { field: 'estatus', title: 'Estatus', sortable: "true" }
    ]});
}


//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Inicializar_Controles
//DESCRIPCIÓN:      inicializa los controles de la pagina
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Inicializar_Controles() {
    $.ajax({
        url: "Frm_Ope_Facturacion_Controlador.aspx?Accion=Consultar_Estados",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (Datos) {
            if (Datos != null) {
                var select = $("[id$='Cph_Contenido_Cmb_Estados']");
                $('option', select).remove();
                var options = '<option value="Seleccione"><-SELECCIONE-></option>';
                for (var Indice_Estados = 0; Indice_Estados < Datos.length; Indice_Estados++) {
                    options += '<option value="' + Datos[Indice_Estados].ESTADO_ID + '">' + Datos[Indice_Estados].NOMBRE.toUpperCase() + '</option>';
                }
                select.append(options);
            }
        },
        error: function (result) {
        }

    });
    $("[id$='Cph_Contenido_Txt_RFC']").blur(function () {
        $('input[id$=Cph_Contenido_Txt_RFC]').filter(function () {
            $(this).val(obtenerCadenaPermitida($(this).val().toUpperCase(), ''));
            if (this.value.length > 13 || this.value.length < 10)
                $(this).val('');
            else {
                $(this).val(obtenerCadenaPermitida($(this).val().toUpperCase(), ''));
                Mostrar_Imagen_Error("Img_Wrn_" + this.id.substring(4, this.id.length), '');
            }
        });
    });
    $("[id$='Cph_Contenido_Txt_RFC']").keydown(function (event) {
        if (event.keyCode == 32) {
            this.value = this.value.trim();
            return false;
        }
    });
    //$("[id$='Txt_Curp']").blur(function () {
    //    $('input[id$=Txt_Curp]').filter(function () {
    //        if (!this.value.match(/^[a-zA-Z]{4}(\d{6})([a-zA-Z]{6})(\d{2})?$/))
    //            $(this).val('');
    //        else {
    //            $(this).val($(this).val().toUpperCase());
    //            Mostrar_Imagen_Error("Img_Wrn_" + this.id.substring(4, this.id.length), '');
    //        }
    //    });
    //});
    //$("[id$='Txt_Curp']").keydown(function (event) {
    //    if (event.keyCode == 32) {
    //        this.value = this.value.trim();
    //        return false;
    //    }
    //});
    $("[id$='Cph_Contenido_Txt_Codigo_Postal']").blur(function () {
        $('input[id$=Cph_Contenido_Txt_Codigo_Postal]').filter(function () {
            if (!this.value.match(/^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/))
                $(this).val('');
        });
    });

    //$("[id$='Txt_Fax']").blur(function () {
    //    $('input[id$=Txt_Fax]').filter(function () {
    //        if (!this.value.match(/^[0-9]{2,3}-? ?[0-9]{6,8}$/))
    //            $(this).val('');
    //        else
    //            Mostrar_Imagen_Error("Img_Wrn_" + this.id.substring(4, this.id.length), '');
    //    });
    //});

    //$("[id$='Txt_Telefono_1']").blur(function () {
    //    $('input[id$=Txt_Telefono_1]').filter(function () {
    //        if (!this.value.match(/^[0-9]{2,3}-? ?[0-9]{5,8}$/))
    //            $(this).val('');
    //        else
    //            Mostrar_Imagen_Error("Img_Wrn_" + this.id.substring(4, this.id.length), '');
    //    });
    //});

    //$("[id$='Txt_Telefono_2']").blur(function () {
    //    $('input[id$=Txt_Telefono_2]').filter(function () {
    //        if (!this.value.match(/^[0-9]{2,3}-? ?[0-9]{5,8}$/))
    //            $(this).val('');
    //        else
    //            Mostrar_Imagen_Error("Img_Wrn_" + this.id.substring(4, this.id.length), '');
    //    });
    //});
    $("[id$='Cph_Contenido_Txt_Email']").blur(function () {
        $('input[id$=Cph_Contenido_Txt_Email]').filter(function () {
           // if (!this.value.match(/^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$/))
             //   $(this).val('');
           // else
                Mostrar_Imagen_Error("Img_Wrn_" + this.id.substring(4, this.id.length), '');
        });
    });
    $("[id$='Cph_Contenido_Txt_Email']").keydown(function (event) {
        if (event.keyCode == 32) {
            this.value = this.value.trim();
            return false;
        }
    });
    Mostrar_Imagen_Error("Img_Wrn_", '');
    $("[id$='Cph_Contenido_Txt_Folio_Recibo']").focus();
    $("[id$='Cph_Contenido_Txt_Folio_Recibo']").keydown(function (event) {
        if (event.keyCode == 13) {
            Consultar_Folio();
        }
    });
    $('[group*=Txt_Datos_Factura]').keydown(function (event) {
        if (event.keyCode == 13) {
            if (parseFloat(event.currentTarget.tabIndex) == 20)
                Modificar_Datos();
            else
                $('[group*=Txt_Datos_Factura]')[parseFloat(event.currentTarget.tabIndex) + 1].focus();
        }
    });
    //$('[group*=Txt_Datos_Factura]').blur(function () {
    //    this.value = this.value.toUpperCase();
    //});
}

//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Limpiar_Controles
//DESCRIPCIÓN:      Limpia los controles de la pagina
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Limpiar_Controles() {
    $("[id$='Cph_Contenido_Txt_Folio_Recibo']").val('');
    $("[id$='Cph_Contenido_Txt_No_Cuenta']").val('');
    $("[id$='Cph_Contenido_Txt_No_Factura']").val('');
    $('.Txt_Datos_Factura').val('');
    $('.Txt_Detalle_Factura').val('');
    $("[id*=Grid_Facturas] td").closest("tr").removeClass('GridSelected');
}

//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Mostrar_Imagen_Error
//DESCRIPCIÓN:      Muestra u oculta la imagen de error en la validacion de los datos
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Mostrar_Imagen_Error(Control, Imagen) {
    $("[id*='" + Control + "']").attr('src', Imagen);
    if (Imagen.length < 1)
        $("[id*='" + Control + "']").attr("width", "0%");
    else
        $("[id*='" + Control + "']").attr("width", "30%");
}

//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Habilitar_Controles
//DESCRIPCIÓN:      habilita los controles de la pagina
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Habilitar_Controles(Accion) {
    var Habilitados = false;
    $('Div[id$=Div_Facturacion_Folio]').css("display", "none");
    $('Div[id$=Div_Factura_Datos]').css("display", "none");
    $('Div[id$=Div_Factura_Detalles]').css("display", "none");
    $("[id$='Cph_Contenido_Lbl_Facturar']").text("Facturar");
    $('#Lnk_Continuar_Detalles').removeClass('btn btn-danger').addClass('btn btn-success');
    switch (Accion) {
        case "Inicial":
            $('Div[id$=Div_Facturacion_Folio]').css("display", "Block");
            break;
        case "Datos":
            $('Div[id$=Div_Factura_Datos]').css("display", "Block");
            $('#Lnk_Continuar_Detalles').linkbutton({ disabled: false });
            //$("#Lnk_Modificar").text("Modificar");
            //$("#Lnk_Modificar").attr("iconcls", "icon-edit");
            //$('#Lnk_Modificar').linkbutton({ disabled: false });
            //$('#Lnk_Modificar').removeClass('btn btn-danger').addClass('btn btn-success');
            $("[id$='Cph_Contenido_Txt_Razon']").css("display", "Block");
            //$("[id$='Cph_Contenido_Txt_Nombre').css("display", "Block");
            //if ($("[id$='Cph_Contenido_Txt_Nombre').val().length > 0)
            //    $("[id$='Cph_Contenido_Txt_Razon').css("display", "none");
            //else
            //    $("[id$='Cph_Contenido_Txt_Nombre').css("display", "none");
            
            break;
        case "Modificar":
            $('Div[id$=Div_Factura_Datos]').css("display", "Block");
            $('#Lnk_Continuar_Detalles').linkbutton({ disabled: true });
            $('#Lnk_Continuar_Detalles').removeClass('btn btn-success').addClass('btn btn-danger');
            //$("#Lnk_Modificar").text("Guardar");
            //$("#Lnk_Modificar").attr("iconcls", "icon-ok");
            Habilitados = true;
            break;
        case "Detalles":
            $('Div[id$=Div_Factura_Detalles]').css("display", "Block");
            $("[id$='Cph_Contenido_Lbl_Facturar']").text("Facturar");
            break;
    }
    var Controles = $('input[group*=Txt_Datos_Factura]');
    Controles.attr("disabled", !Habilitados);
    $('select[group*=Txt_Datos_Factura]').attr("disabled", !Habilitados);
    if ($("[id$='Cph_Contenido_Txt_No_Factura']").val() != '') {
        //$('#Lnk_Modificar').linkbutton({ disabled: true });
        //$('#Lnk_Modificar').removeClass('btn btn-success').addClass('btn btn-danger');
        $("[id$='Cph_Contenido_Txt_Email']").attr("disabled", false);
        $("[id$='Cph_Contenido_Lbl_Facturar']").text("Reenviar");
    } else {
        $("[id$='Cph_Contenido_Txt_Email']").attr("disabled", false);
    }
}

//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Datos_Validos
//DESCRIPCIÓN:      Funcion que valida que se ingresen todos los datos requeridos
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Datos_Validos() {
    var Controles = $('[group*=Requerido]');
    var Datos_Faltantes = '';
    var Foco = false;
    for (var Indice_Datos = 0; Indice_Datos < Controles.length; Indice_Datos++) {
        if (Indice_Datos < 2) {
            if (Controles[0].value == '' && Controles[1].value == '') {
                Datos_Faltantes += Datos_Faltantes.length < 1 ? Controles[Indice_Datos].id.substring(24, Controles[Indice_Datos].id.length) : ', ' + Controles[Indice_Datos].id.substring(24, Controles[Indice_Datos].id.length);
                Mostrar_Imagen_Error("Img_Wrn_" + Controles[Indice_Datos].id.substring(24, Controles[Indice_Datos].id.length), '../../JavaScript/jquery-easyiu-1.3.1/themes/gray/images/validatebox_warning.png');
            }
        }
        else if (Controles[Indice_Datos].value == '' || Controles[Indice_Datos].value == 'Seleccione') {
            if (Controles[Indice_Datos].id == "[id$='Cph_Contenido_Cmb_Estados']") {
                Datos_Faltantes += Datos_Faltantes.length < 1 ? 'Estado' : ', ' + 'Estado';
                Mostrar_Imagen_Error("Img_Wrn_Estado", '../../JavaScript/jquery-easyiu-1.3.1/themes/gray/images/validatebox_warning.png');
            }
            else {
                Datos_Faltantes += Datos_Faltantes.length < 1 ? Controles[Indice_Datos].id.substring(24, Controles[Indice_Datos].id.length) : ', ' + Controles[Indice_Datos].id.substring(24, Controles[Indice_Datos].id.length);
                Mostrar_Imagen_Error("Img_Wrn_" + Controles[Indice_Datos].id.substring(24, Controles[Indice_Datos].id.length), '../../JavaScript/jquery-easyiu-1.3.1/themes/gray/images/validatebox_warning.png');
            }
            if (Foco == false) {
                Controles[Indice_Datos].focus();
                Foco = true;
            }
        }
        else
            Mostrar_Imagen_Error(Controles[Indice_Datos].id == "[id$='Cph_Contenido_Cmb_Estados']" ? "Img_Wrn_Estado" : "Img_Wrn_" + Controles[Indice_Datos].id.substring(24, Controles[Indice_Datos].id.length), '');
    }// for

    var txt_rfc = $.trim($("[id$='Cph_Contenido_Txt_RFC']").val());

    if (txt_rfc.length < 12 || txt_rfc.length > 13) {

        Datos_Faltantes += ' La longitud del RFC es incorrecta';
    }

    if (Datos_Faltantes.length > 0) {
        $.messager.alert('SIAC','Favor de ingresar los siguientes datos:\n' + Datos_Faltantes);
        return false;
    }
    return true;
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Consultar_Folio
//DESCRIPCIÓN:      Consulta los folios facturados
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Consultar_Folio() {
    var Txt_No_Folio = $("[id$='Cph_Contenido_Txt_Folio_Recibo']").val();
    var Datos;
    var Total = 0;
    var Obj_Json;
    var Iva_Concepto;
    if (Txt_No_Folio.length >= 1) {
        $.ajax({
            type: "POST",
            url: 'Frm_Ope_Facturacion_Controlador.aspx?Accion=Consultar_Datos_Factura&No_Folio=' + Txt_No_Folio,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Datos) {
                if (Datos.total > 0) {
                    if (Datos.rows[0].folio_pago == '0') {
                    }
                    if (Datos.rows[0].factura == 'USUARIO')
                       alert('La factura correspondiente al folio "' + Txt_No_Folio + '" ya fue generada con anterioridad se puede reenviar la factura si asi lo requiere');
                    else
                        $.messager.alert('SIAC','La factura ya no puede ser generada');
                    Cargar_Datos(Datos, Txt_No_Folio, "Facturado");
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: 'Frm_Ope_Facturacion_Controlador.aspx?Accion=Consultar_Datos_Cliente&No_Folio=' + Txt_No_Folio,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: false,
                        success: function (Datos) {
                            if (Datos.total > 0) {
                                Cargar_Datos(Datos, Txt_No_Folio, "No_Facturado");
                            }
                            else {
                                if (Datos.total == 0) {
                                    Cargar_Datos_Nuevos(Datos, Txt_No_Folio, "No_Facturado");
                                }
                                else {
                                    $.messager.alert('SIAC','El codigo de facturación ingresado no existe, favor de verificarlo.');
                                }
                            }
                        },
                        error: function (Datos) {
                            Obj_Json = eval("(" + Datos.d + ")");
                        }
                    });
                }
            },
            error: function (Datos) {
                Obj_Json = eval("(" + Datos.d + ")");
            }
        });
    }
    else {
        $.messager.alert('SIAC','El Codigo de facturacion no cumple con la longitud necesaria.');
    }
}

//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Consultar_Estado_Cuenta
//DESCRIPCIÓN:      Consultar el ultimo recibo
//CREO:             Fernando Gonzalez B.
//FECHA_CREO:       24/04/2014
//*******************************************************************************************************
function Consultar_Estado_Cuenta() {
    var Txt_No_Cuenta = $("[id$='Cph_Contenido_Hdf_Predio']").val();
    var Datos;
    var Total = 0;
    var Obj_Json;

    if (Txt_No_Cuenta.length > 0) {
        window.location = "Frm_Ope_Facturacion_Controlador.aspx?Accion=Consultar_Estado_Cuenta&No_Cuenta=" + Txt_No_Cuenta;
        //        $.ajax({
        //            type: "POST",
        //            url: 'Frm_Ope_Facturacion_Controlador.aspx?Accion=Consultar_Estado_Cuenta&No_Cuenta=' + Txt_No_Cuenta,
        //            contentType: "application/json; charset=utf-8",
        //            dataType: "json",
        //            async: false,
        //            success: function (Datos) {
        //                
        //            },
        //            error: function (Datos) {
        //                Obj_Json = eval("(" + Datos.d + ")");
        //            }
        //        });
    }
    else {
        $.messager.alert('SIAC','Debes Ingresar un numero de cuenta.')
    }
}

//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Cargar_Datos
//DESCRIPCIÓN:      permite la modificacion de los datos de facturacion
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Cargar_Datos(Datos, Txt_No_Folio, Estatus) {
    var Subtotal = 0;
    var Iva = 0;
    var Total_Factura = 0;
    var Tabla;
    var Lineas;
    var Porcetaje_Iva = 0.0;
    Datos = Datos.rows[0];
    $("[id$='Cph_Contenido_Txt_Cliente_id']").val(Datos.cliente_id);
    if (Estatus == 'Facturado') {
        $("[id$='Cph_Contenido_Txt_Localidad']").val(Datos.localidad.toUpperCase());
        for (var Indice_Estados = $("[id$='Cph_Contenido_Cmb_Estados']").length; Indice_Estados > 0; Indice_Estados++) {
            if ($("[id$='Cph_Contenido_Cmb_Estados']")[0][Indice_Estados].innerHTML.trim() == Datos.estado.trim()) {
                $("[id$='Cph_Contenido_Cmb_Estados']").val($("[id$='Cph_Contenido_Cmb_Estados']")[0][Indice_Estados].value.trim());
                break;
            }
        }
        $("[id$='Cph_Contenido_Txt_No_Factura']").val(Datos.no_factura.toUpperCase());
    }
    else {
        $("[id$='Cph_Contenido_Txt_Nombre']").val(Datos.nombre_corto);
        $("[id$='Cph_Contenido_Cmb_Estados']").val(Datos.estado);
    }
    $("[id$='Cph_Contenido_Txt_Razon']").val(Datos.razon_social);
    $("[id$='Cph_Contenido_Txt_RFC']").val(obtenerCadenaPermitida(Datos.rfc.toUpperCase(), ''));
    //$('#Txt_Curp').val(Datos.curp.toUpperCase());
    $("[id$='Cph_Contenido_Txt_Pais']").val(Datos.pais.toUpperCase());
    $('#Txt_Ciudad').val(Datos.ciudad.toUpperCase());
    $("[id$='Cph_Contenido_Txt_Colonia']").val(obtenerCadenaPermitida(Datos.colonia.toUpperCase(), ''));
    $("[id$='Cph_Contenido_Txt_Localidad']").val(obtenerCadenaPermitida(Datos.ciudad.toUpperCase()), '');
    $("[id$='Cph_Contenido_Txt_Calle']").val(obtenerCadenaPermitida(Datos.calle.toUpperCase()), '');
    $("[id$='Cph_Contenido_Txt_Numero_Exterior']").val(Datos.numero_exterior);
    $("[id$='Cph_Contenido_Txt_Numero_Interior']").val(Datos.numero_interior);
    $("[id$='Cph_Contenido_Txt_Codigo_Postal']").val(Datos.cp);
    //$('#Txt_Extension').val(Datos.extension);
    //$('#Txt_Nextel').val(Datos.nextel);
    $("[id$='Cph_Contenido_Txt_Email']").val(Datos.email);
    //$('#Txt_Fax').val(Datos.fax);
    //$('#Txt_Telefono_1').val(Datos.telefono_1);
    //$('#Txt_Telefono_2').val(Datos.telefono_2);
    //$('#Txt_Sitio_Web').val(Datos.sitio_web);
    $('#Tbl_Facturado').datagrid({
        url: 'Frm_Ope_Facturacion_Controlador.aspx',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        pageNumber: 1,
        queryParams: {
            Accion: 'Consultar_Detalles_Pago',
            No_Folio: Txt_No_Folio
        },
        onLoadSuccess: function (data) {
            Datos = $('#Tbl_Facturado').datagrid('getRows');
            if (Datos.length > 0) {
                Tabla = $('#Tbl_Muestra_Facturado');
                $('table', Tabla).remove();
                Porcetaje_Iva = '0';
                Lineas = "<table style='width:100%'>";
                Lineas += "<tr style='background-color: #011826; border-color:#ccc;color:#fff; width:100%;font-size:14px;font-family:Arial;font-weight:bold'>";
                Lineas += "<td style='text-align:center;width:5%'>Cantidad</td><td style='text-align:center;width:15%'>Unidad de Medida</td><td style='text-align:center;width:15%'>Clave</td><td style='text-align:center;width:52%'>Concepto</td><td style='text-align:center;width:10%'>Total</td></tr>";
                for (var Indice_Datos = 0; Indice_Datos < Datos.length; Indice_Datos++) {
                    Lineas += '<tr style="font-size:14px;font-family:Arial;text-align:center;color:#000" ><td style="text-align:center">' + Datos[Indice_Datos].valor_unitario + '</td><td style="text-align:center">' +
                    Datos[Indice_Datos].Unidad_de_Medida + '</td><td style="text-align:center">' + Datos[Indice_Datos].clave_concepto + '</td><td>' + Datos[Indice_Datos].descripcion +
                    '</td><td style="text-align:right">';
                    Lineas += formatNumber.new(parseFloat(Datos[Indice_Datos].importe).toFixed(2), '$') + '</td></tr>';
                    Subtotal += parseFloat(Datos[Indice_Datos].importe);
                    Iva += parseFloat(Datos[Indice_Datos].iva);
                    Iva_Concepto = (parseFloat(Datos[Indice_Datos].iva) * 100) / parseFloat(Datos[Indice_Datos].importe);
                    //                    if (Datos[Indice_Datos].iva != 0 && Iva_Concepto > parseFloat(Porcetaje_Iva))
                    //                        Porcetaje_Iva = Iva_Concepto;
                    if ($("[id$='Cph_Contenido_Txt_Fecha_Pago']").val() == "")
                        $("[id$='Cph_Contenido_Txt_Fecha_Pago']").val(Datos[Indice_Datos].FECHA_MOVIMIENTO);
                    //if (Datos[Indice_Datos].lleva_iva == "SI")
                        Porcetaje_Iva = Datos[Indice_Datos].Porcentaje_IVA;

                }
                Subtotal = RedondearDecimales(Subtotal);
                Iva = RedondearDecimales(Iva);
                Total_Factura = parseFloat(Subtotal + Iva);
                if (Iva == "0.00")
                    Porcetaje_Iva = "0";
                Lineas += "<tr> <td></td> <td></td> <td></td> <td></td> <td></td> </tr>";
                Lineas += "<tr style='font-size:14px;font-family:Arial;font-weight:bold'>";
                Lineas += "<td></td> <td></td> <td></td> <td style='text-align:right'>Subtotal: </td> <td colspan='2' style='text-align:right'>" + formatNumber.new(Subtotal.toFixed(2), '$') + "</td> </tr>";
                Lineas += "<tr style='font-size:14px;font-family:Arial;font-weight:bold'>";
                Lineas += "<td></td> <td></td> <td></td> <td style='text-align:right'> " + Porcetaje_Iva + "% iva: </td><td colspan='2' style='text-align:right'>" + formatNumber.new(Iva.toFixed(2), '$') + "</td> </tr>";
                Lineas += "<tr style='font-size:14px;font-family:Arial;font-weight:bold'>";
                Lineas += "<td></td> <td></td> <td></td> <td style='text-align:right'>Total: </td> <td colspan='2' style='text-align:right'>" + formatNumber.new(Total_Factura.toFixed(2), '$') + "</td> </tr>";
                Lineas += '</table>';
                Tabla.append(Lineas);
                $('#Tbl_Facturado').css("display", "none");
                $("[id$='Cph_Contenido_Txt_Subtotal']").val(Subtotal);
                $("[id$='Cph_Contenido_Txt_Subtotal_Iva']").val(Iva);
                $("[id$='Cph_Contenido_Txt_Total']").val(Total_Factura.toFixed(2));
                $("[id$='Cph_Contenido_Txt_IvA_General']").val(Porcetaje_Iva);
                Subtotal = 0;
                Iva = 0;
                Total_Factura = 0;
                Habilitar_Controles("Datos");
                $('#Lnk_Continuar_Detalles').focus();
            }
        },
        onLoadError: function (Datos) {

            var mensaje = $.trim(Datos.responseText);
            var mensaje1 = $.trim(Datos.dato1);

            if (mensaje == "error_fecha") {
                $.messager.alert('SIAC', 'El período de facturación ha expirado', 'info');

            }
            else if (mensaje.includes("Este pago ya se encuentra timbrado con el folio"))
            {
                window.location.href = "/paginas/Paginas_Generales/Frm_Facturacion.aspx?codigo_barras=" + mensaje.split(" - ")[1];
            }
            else {
                $.messager.alert('SIAC', "El folio " + Txt_No_Folio + " ingresado no existe", 'info');
                Obj_Json = eval("(" + Datos.d + ")");
            }
            
        }
    });
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Cargar_Datos_Nuevos
//DESCRIPCIÓN:      permite la modificacion de los datos de facturacion
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Cargar_Datos_Nuevos(Datos, Txt_No_Folio, Estatus) {
    var Subtotal = 0;
    var Iva = 0;
    var Total_Factura = 0;
    var Tabla;
    var Lineas;
    var Porcetaje_Iva = 0.0;
    Datos = Datos.rows[0];
    $("[id$='Cph_Contenido_Txt_Cliente_id']").val();
    if (Estatus == 'Facturado') {
        $("[id$='Cph_Contenido_Txt_Localidad']").val();
        for (var Indice_Estados = $("[id$='Cph_Contenido_Cmb_Estados']").length; Indice_Estados > 0; Indice_Estados++) {
            if ($("[id$='Cph_Contenido_Cmb_Estados']")[0][Indice_Estados].innerHTML.trim() == Datos.estado.trim()) {
                $("[id$='Cph_Contenido_Cmb_Estados']").val($("[id$='Cph_Contenido_Cmb_Estados']")[0][Indice_Estados].value.trim());
                break;
            }
        }
        $("[id$='Cph_Contenido_Txt_No_Factura']").val();
    }
    else {
        $("[id$='Cph_Contenido_Txt_Nombre']").val();
        $("[id$='Cph_Contenido_Cmb_Estados']").val();
    }
    $("[id$='Cph_Contenido_Txt_Razon']").val();
    $("[id$='Cph_Contenido_Txt_RFC']").val();
    $("[id$='Cph_Contenido_Txt_Curp']").val();
    $("[id$='Cph_Contenido_Txt_Pais']").val();
    $('#Txt_Ciudad').val();
    $("[id$='Cph_Contenido_Txt_Colonia']").val();
    $("[id$='Cph_Contenido_Txt_Localidad']").val();
    $("[id$='Cph_Contenido_Txt_Calle']").val();
    $("[id$='Cph_Contenido_Txt_Numero_Exterior']").val();
    $("[id$='Cph_Contenido_Txt_Numero_Interior']").val();
    $("[id$='Cph_Contenido_Txt_Codigo_Postal']").val();
    //$('#Txt_Extension').val();
    //$('#Txt_Nextel').val();
    $('#Txt_Email').val();
    //$('#Txt_Fax').val();
    //$('#Txt_Telefono_1').val();
    //$('#Txt_Telefono_2').val();
    //$('#Txt_Sitio_Web').val();
    $('#Tbl_Facturado').datagrid({
        url: 'Frm_Ope_Facturacion_Controlador.aspx',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        pageNumber: 1,
        queryParams: {
            Accion: 'Consultar_Detalles_Pago',
            No_Folio: Txt_No_Folio
        },
        onLoadSuccess: function (data) {
            Datos = $('#Tbl_Facturado').datagrid('getRows');
            if (Datos.length > 0) {
                Tabla = $('#Tbl_Muestra_Facturado');
                $('table', Tabla).remove();
                Porcetaje_Iva = '0';
                Lineas = "<table>";
                Lineas += "<tr style='background-color: #f0f0f0; border-color:#ccc;width:100%;font-size:12px;font-family:Arial;font-weight:bold'>";
                Lineas += "<td style='text-align:center;width:5%'>Cantidad</td><td style='text-align:center;width:15%'>Unidad de Medida</td><td style='text-align:center;width:15%'>Clave</td><td style='text-align:center;width:52%'>Concepto</td><td style='text-align:center;width:10%'>Total</td></tr>";
                for (var Indice_Datos = 0; Indice_Datos < Datos.length; Indice_Datos++) {
                    Lineas += '<tr style="font-size:12px;font-family:Arial;text-align:center"><td style="text-align:center">' + Datos[Indice_Datos].valor_unitario + '</td><td style="text-align:center">' +
                    Datos[Indice_Datos].Unidad_de_Medida + '</td><td style="text-align:center">' + Datos[Indice_Datos].clave_concepto + '</td><td>' + Datos[Indice_Datos].descripcion +
                    '</td><td style="text-align:right">';
                    Lineas += formatNumber.new(parseFloat(Datos[Indice_Datos].importe).toFixed(2), '$') + '</td><td style="text-align:right">' +
                        formatNumber.new(parseFloat(Datos[Indice_Datos].iva), '$') + '</td><td style="text-align:right">' + formatNumber.new(parseFloat(Datos[Indice_Datos].total), '$') + '</td></tr>';
                    Subtotal += parseFloat(Datos[Indice_Datos].total);
                    Iva = parseFloat(Datos[Indice_Datos].iva);
                    Iva_Concepto = (parseFloat(Datos[Indice_Datos].iva) * 100) / parseFloat(Datos[Indice_Datos].importe);
                    //                    if (Datos[Indice_Datos].iva != 0 && Iva_Concepto > parseFloat(Porcetaje_Iva))
                    //                        Porcetaje_Iva = Iva_Concepto;
                    if ($("[id$='Cph_Contenido_Txt_Fecha_Pago']").val() == "")
                        $("[id$='Cph_Contenido_Txt_Fecha_Pago']").val(Datos[Indice_Datos].FECHA_MOVIMIENTO);
                    //if (Datos[Indice_Datos].lleva_iva == "SI")
                        Porcetaje_Iva = Datos[Indice_Datos].Porcentaje_IVA;
                }
                Subtotal = RedondearDecimales(Subtotal);
                Iva = RedondearDecimales(Iva);
                Total_Factura = Subtotal + Iva;

                Lineas += "<tr><td></td><td></td><td></td><td></td><td><td></td></td><td></td></tr>";
                Lineas += "<tr style='font-size:12px;font-family:Arial;font-weight:bold'>";
                Lineas += "<td></td><td></td><td></td><td style='text-align:right'>Subtotal: </td><td colspan='2' style='text-align:right'>" + formatNumber.new(Subtotal, '$') + "</td></tr>";
                Lineas += "<tr style='font-size:12px;font-family:Arial;font-weight:bold'>";
                Lineas += "<td></td><td></td><td></td><td style='text-align:right'> " + Porcetaje_Iva + "% iva: </td><td colspan='2' style='text-align:right'>" + formatNumber.new(Iva, '$') + "</td></tr>";
                Lineas += "<tr style='font-size:12px;font-family:Arial;font-weight:bold'>";
                Lineas += "<td></td><td></td><td></td><td style='text-align:right'>Total: </td><td colspan='2' style='text-align:right'>" + formatNumber.new(Total_Factura, '$') + "</td></tr>";
                Lineas += '</table>';
                Tabla.append(Lineas);
                $('#Tbl_Facturado').css("display", "none");
                $("[id$='Cph_Contenido_Txt_Subtotal']").val(Subtotal);
                $("[id$='Cph_Contenido_Txt_Subtotal_Iva']").val(Iva);
                $("[id$='Cph_Contenido_Txt_Total']").val(Total_Factura);
                $("[id$='Cph_Contenido_Txt_IvA_General']").val(Porcetaje_Iva);
                Subtotal = 0;
                Iva = 0;
                Total_Factura = 0;
                Habilitar_Controles("Modificar");
                $('#Lnk_Continuar_Detalles').focus();
            }
        },
        onLoadError: function (Datos) {
            $.messager.alert('SIAC','SIAC', "El folio " + Txt_No_Folio + " ingresado no existe");
            Obj_Json = eval("(" + Datos.d + ")");
        }
    });
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   RedondearDecimales
//DESCRIPCIÓN:      Redondea los numeros a dos decimales 
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function RedondearDecimales(Numero) {
    return +(Math.round(Numero + "e+2") + "e-2");
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Modificar_Datos
//DESCRIPCIÓN:      permite la modificacion de los datos de facturacion
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Modificar_Datos() {
    var Accion = $("#Lnk_Modificar").text();
    var Controles;
    var Datos;

    if (Accion == "Modificar") {
        Habilitar_Controles("Modificar");
        $("[id$='Cph_Contenido_Txt_Nombre']").focus();
    }
    else {
        if ($("[id$='Cph_Contenido_Txt_Cliente_id']").val() != "") {
            if (Datos_Validos() == true) {
                Controles = $('input[group*=Txt_Datos_Factura]');
                Datos = "{[";
                for (var Indice_Datos = 0; Indice_Datos < Controles.length; Indice_Datos++) {
                    Datos += Controles[Indice_Datos].id + ":" + Controles[Indice_Datos].value.replace(';', '') + ";";
                }
                Datos += "No_Folio:" + $("[id$='Cph_Contenido_Txt_Folio_Recibo']").val() + ";";
                Datos += "Estado_ID:" + $("[id$='Cph_Contenido_Cmb_Estados']").find('option:selected')[0].value.trim() + ";";
                Datos += "Estado:" + $("[id$='Cph_Contenido_Cmb_Estados']").find('option:selected')[0].innerHTML.trim();
                Datos += "]}";
                $.ajax({
                    type: "POST",
                    url: 'Frm_Ope_Facturacion_Controlador.aspx?Accion=Modificar_Datos_Cliente&Datos=' + Datos,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    success: function (Respuesta) {
                        if (Respuesta.Mensaje == 'Modificacion') {
                            $.messager.alert('SIAC',"La modificación de los datos se realizó correctamente");
                            Habilitar_Controles("Datos");
                            $('#Lnk_Continuar_Detalles').focus();
                        }
                        else {
                            $.messager.alert('SIAC',Respuesta.Mensaje)
                        }
                    },
                    error: function (Respuesta) {
                        $.messager.alert('SIAC',"La modificación de los datos no se realizó");
                    }
                });
            }
        }
        else {
            Alta_Cliente()
        }
    }
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Alta_Cliente
//DESCRIPCIÓN:      permite la modificacion de los datos de facturacion
//CREO:             Miguel Angel Alvarado Enriquez
//FECHA_CREO:       18/Feb/2014"[id$='
//*******************************************************************************************************
function Alta_Cliente() {
    var Accion = $("[id$='Cph_Contenido_Lbl_Modificar']").text();
    var Controles;
    var Datos;
    if (Datos_Validos() == true) {
        Controles = $('input[group*=Txt_Datos_Factura]');
        Datos = "{[";
        for (var Indice_Datos = 0; Indice_Datos < Controles.length; Indice_Datos++) {
            Datos += Controles[Indice_Datos].id + ":" + Controles[Indice_Datos].value + ",";
        }
        Datos += "No_Folio : " + $("[id$='Cph_Contenido_Txt_Folio_Recibo']").val() + ",";
        Datos += "Estado_ID : " + $("[id$='Cph_Contenido_Cmb_Estados']").find('option:selected')[0].value.trim() + ",";
        Datos += "Estado : " + $("[id$='Cph_Contenido_Cmb_Estados']").find('option:selected')[0].innerHTML.trim();
        Datos += "]}";
        $.ajax({
            type: "POST",
            url: 'Frm_Ope_Facturacion_Controlador.aspx?Accion=Alta_Datos_Cliente&Datos=' + Datos,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Respuesta) {
                if (Respuesta.Mensaje == 'Alta') {
                    $.messager.alert('SIAC',"Los datos se han guardado correctamente");
                    Habilitar_Controles("Datos");
                    $('#Lnk_Continuar_Detalles').focus();
                }
            },
            error: function (Respuesta) {
                $.messager.alert('SIAC',"Los datos se han guardado correctamente");
            }
        });
    }
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Cancelar_Modificacion
//DESCRIPCIÓN:      permite cancelar la modificacion de los datos del cliente
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Cancelar_Modificacion() {
    var Accion = $("#Lnk_Modificar").text();
    if (Accion == "Guardar") {
        Consultar_Folio();
        Mostrar_Imagen_Error("Img_Wrn_", '');
    }
    else {
        Limpiar_Controles();
        Habilitar_Controles("Inicial");
        $("[id$='Cph_Contenido_Txt_Folio_Recibo']").focus();
    }
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Continuar_Detalles
//DESCRIPCIÓN:      permite avanzar al div que muestra la informacion detallada de la factura
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Continuar_Detalles() {
    if (Datos_Validos() == true) {
        Habilitar_Controles("Detalles");
        var Txt_No_Folio = $("[id$='Cph_Contenido_Txt_Folio_Recibo']").val();
        $('#Lnk_Facturar').focus();
    }
    //else
    //    $('#Lnk_Modificar').focus();

}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Continuar_Detalles
//DESCRIPCIÓN:      permite avanzar al div que muestra la informacion detallada de la factura
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Cancelar_Detalles() {
    Habilitar_Controles("Datos");
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   obtenerCadenaPermitida
//DESCRIPCIÓN:      Obtiene la cadena con el formato permitido
//CREO:             Miguel AnAlvarado Enriquez
//FECHA_CREO:       31/Marzo/2014
//*******************************************************************************************************
function obtenerCadenaPermitida(cadena, cadendaExtra) {

    var respuesta = "";
    var letra = "";
    var enter = "\n";

    var caracteres, i;
    caracteres = "abcdefghijklmnopqrstuvwxyz1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZáéíóúÁÉÍÓÚ" + enter + cadendaExtra;
    for (i = 0; i < cadena.length; i++) {
        letra = cadena.substring(i, i + 1);
        if (caracteres.indexOf(letra) != -1) {
            respuesta = respuesta + letra
        }
    }
    return respuesta;

}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN:   Inicializar_Controles
//DESCRIPCIÓN:      inicializa los controles de la pagina
//CREO:             Ramón Baeza Yépez
//FECHA_CREO:       05/12/2013
//*******************************************************************************************************
function Facturar() {
    return;
    var Txt_No_Folio = $("[id$='Cph_Contenido_Txt_Folio_Recibo']").val();
    var Ruta_PDF_Enviar = "";

    $('#ventana_mensaje').window('open');
    $.ajax({
        url: "Frm_Ope_Facturacion_Controlador.aspx?Accion=nada&No_Folio=" + Txt_No_Folio + "&Subtotal=" + $("[id$='Cph_Contenido_Txt_Subtotal']").val() + "&Nombre=" + $("[id$='Cph_Contenido_Txt_Nombre']").val() + "&RFC=" + obtenerCadenaPermitida($("[id$='Cph_Contenido_Txt_RFC']").val(), '') +
            "&Subtotal_Iva=" + $("[id$='Cph_Contenido_Txt_Subtotal_Iva']").val() + "&Total=" + $("[id$='Cph_Contenido_Txt_Total']").val() + "&Iva_General=" + $("[id$='Cph_Contenido_Txt_IvA_General']").val() + "0" + "&Email=" + $("[id$='Cph_Contenido_Txt_Email']").val() + "&Cliente_Id=" + $("[id$='Cph_Contenido_Txt_Cliente_id']").val(),
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (Datos) {
            if (Datos != null) {
                $('#ventana_mensaje').window('close');

                debugger;
                if (Datos.Mensaje.includes('Ocurrio un error al dar de alta factura'))
                {
                    $.messager.alert('SIAC', Datos.Mensaje, 'error');
                    return;
                }
                else
                {
                    Habilitar_Controles("Inicial");
                    Inicializar_Controles();
                    Limpiar_Controles();
                    Ruta_PDF_Enviar = Datos.Ruta_Corta;

                    //alert(Datos.Mensaje);
                    $.messager.alert('SIAC', Datos.Mensaje, 'info', function () {
                        if (Ruta_PDF_Enviar === undefined) {

                        } else {
                            window.open(Ruta_PDF_Enviar, "resizeable,scrollbar");
                            window.location.href = 'Frm_Facturacion_Electronica.aspx';
                        }
                    });
                }
            }
        },
        error: function (result) {
            $.messager.alert('SIAC', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');
        }

    });
   
}
var formatNumber = {
    separador: ",", // separador para los miles
    sepDecimal: '.', // separador para los decimales
    formatear: function (num) {
        num += '';
        var splitStr = num.split('.');
        var splitLeft = splitStr[0];
        var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
        var regx = /(\d+)(\d{3})/;
        while (regx.test(splitLeft)) {
            splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
        }
        return this.simbol + splitLeft + splitRight;
    },
    new: function (num, simbol) {
        this.simbol = simbol || '';
        return this.formatear(num);
    }
}