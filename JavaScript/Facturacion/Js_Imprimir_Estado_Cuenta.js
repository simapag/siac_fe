﻿$(function () { });

function buscarEstadoCuenta() {

    var rpu = $.trim($("[id$='Hdf_Rpu']").val());

    if (rpu.length == 0) {
        $.messager.alert('SIAC', 'Escribe un número de Cuenta', 'info');
        return;
    }

    $.ajax({
        type: "POST",
        url: "Frm_Ope_Facturacion_Controlador.aspx/Buscar_Estado_Cuenta",
        data: "{'rpu':'" + rpu + "','correo_electronico':'pruebas@contel.com.mx'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var respuesta = eval("(" + response.d + ")");
            if (respuesta.mensaje == "bien") {
                busquedaExitosa(respuesta);
            }
                   },
        error: function (result) {
            alert('Ocurrio un Error!' + result.status + ' ' + result.statusText);
        }

    });
}

function Consultar_Estado_Cuenta() {
    var Txt_No_Cuenta = $("[id$='Hdf_Rpu']").val();
    var Datos;
    var Total = 0;
    var Obj_Json;
    //var Total = $("[id$='Txt_Total_Pagar']").val();
    //if (Total == '0' || Total == '0.00' || Total.length == 0) {
    //    $.messager.alert("SIAC", "Importe en cero", "info");
    //    return;
    //}
    if (Txt_No_Cuenta.length > 0) {
        window.location = "Frm_Ope_Facturacion_Controlador.aspx?Accion=Consultar_Estado_Cuenta&No_Cuenta=" + Txt_No_Cuenta;
    }
    else {

        alert('Debes Ingresar un numero de cuenta.');
    }
}

function busquedaExitosa(respuesta) {

   var obj_estado_cuenta = JSON.parse(respuesta.dato1);
    limpiarCajas();

    $("[id$='Txt_No_Cuenta']").val(obj_estado_cuenta[0].no_cuenta);
    $("[id$='Txt_RPU']").val(obj_estado_cuenta[0].rpu);
    $("[id$='Txt_Usuario']").val(obj_estado_cuenta[0].usuario);
    $("[id$='Txt_Direccion']").val(obj_estado_cuenta[0].direccion);
    $("[id$='Txt_Colonia']").val(obj_estado_cuenta[0].colonia);
    $("[id$='Txt_Direccion_Corta']").val(obj_estado_cuenta[0].direccion_corta);
    $("[id$='Txt_Clasificacion']").val(obj_estado_cuenta[0].tarifa);
    $("[id$='Txt_Referencia']").val(obj_estado_cuenta[0].referencia);

    $("[id$='Txt_Ultimo_Bimestre_Pagado']").val(obj_estado_cuenta[0].ultimo_pagado);
    $("[id$='Txt_Periodo_Aviso']").val(obj_estado_cuenta[0].fecha_emision);
    $("[id$='Txt_Ruta']").val(obj_estado_cuenta[0].ruta_reparto);
    $("[id$='Txt_No_Medidor']").val(obj_estado_cuenta[0].medidor_id);
    $("[id$='Txt_Lectura']").val(obj_estado_cuenta[0].lectora);
    $("[id$='Txt_Lectura_Anterior']").val(obj_estado_cuenta[0].lectura_anterior);
    $("[id$='Txt_Lectura_Actual']").val(obj_estado_cuenta[0].lectura_actual);
    $("[id$='Txt_Consumo']").val(obj_estado_cuenta[0].consumo);
    $("[id$='Txt_Vence']").val(obj_estado_cuenta[0].fecha_limite_pago);
    $("[id$='Txt_Importe_Bimestre']").val(obj_estado_cuenta[0].importe_bimestre);
    $("[id$='Txt_Adeudo']").val(obj_estado_cuenta[0].adeudo);
    $("[id$='Txt_Total_Pagar']").val(obj_estado_cuenta[0].total_pagar);

    //estatus_recibo = obj_estado_cuenta[0].vigente;
    //predio_id = obj_estado_cuenta[0].predio_id;
    //no_cuenta = obj_estado_cuenta[0].no_cuenta;

    //if (estatus_recibo == 'vencido') {
    //    Obtener_Facturacion();
    //}
    //else {
    // $('#ventana_estado_cuenta').window('open');
    //}





}

function limpiarCajas() {
    $("[id$='Txt_No_Cuenta']").val('');
    $("[id$='Txt_RPU']").val('');
    $("[id$='Txt_Usuario']").val('');
    //$('#span_usuario').val('');
    $("[id$='Txt_Direccion']").val('');
    $("[id$='Txt_Colonia']").val('');
    $("[id$='Txt_Direccion_Corta']").val('');
    $("[id$='Txt_Clasificacion']").val('');
    $("[id$='Txt_Referencia']").val('');



    $("[id$='Txt_Ultimo_Bimestre_Pagado']").val('');
    $("[id$='Txt_Periodo_Aviso']").val('');
    $("[id$='Txt_Ruta']").val('');
    $("[id$='Txt_No_Medidor']").val('');
    $("[id$='Txt_Lectura']").val('');
    $("[id$='Txt_Lectura_Anterior']").val('');
    $("[id$='Txt_Lectura_Actual']").val('');
    $("[id$='Txt_Consumo']").val('');
    $("[id$='Txt_Vence']").val('');

    $("[id$='Txt_Importe_Bimestre']").val('');
    $("[id$='Txt_Adeudo']").val('');
    $("[id$='Txt_Total_Pagar']").val('');



}
