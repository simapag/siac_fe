﻿///***************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Validar_Campos
///DESCRIPCIÓN          : Función que suma las columna del Grid de los Gastos
///PARAMETROS           : Div: Es contenedor que tiene las cajas de texto y los combos
///CREO                 : Armando Zavala Moreno
///FECHA_CREO           : 18/Febrero/2013 12:42:00 p.m
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///***************************************************************************************************
function Validar_Campos(Div) {
    var Control = $("[id$='" + Div + "']").attr('id');
    var Elementos = $('#' + Control + ' :input[id][Requerido$="true"]');
    var Caja_Texto;
    var Tipo_Control;
    var Nombre_Control;
    var Valor_Control;
    var Mensaje_Error = '';
    var Faltan_Datos = false;

    if (Div == "Div_Nuevo_Cliente") {
        Faltan_Datos = Validar_Campos_Cliente_Nuevo(Div);
    }
    else {
        for (var i = 0; i < Elementos.length; i++) {
            Caja_Texto = Elementos[i];

            Tipo_Control = $(Caja_Texto).attr('type');
            Nombre_Control = $(Caja_Texto).get(0).tagName;

            if (Nombre_Control == "INPUT" || Nombre_Control == "TEXTAREA") {
                Valor_Control = $.trim($(Caja_Texto).val());
                if (Valor_Control.length == 0 || Valor_Control == "Dia/Mes/Año") {
                    $(Caja_Texto).css('border-style', 'solid');
                    $(Caja_Texto).css('border-color', 'red');
                    Faltan_Datos = true;
                }
                else {
                    $(Caja_Texto).css('border-style', '');
                    $(Caja_Texto).css('border-color', '');
                }
            }

            if (Nombre_Control == "SELECT") {
                var selectedIndex = $(Caja_Texto)[0].selectedIndex;
                Valor_Control = $(Caja_Texto).val();
                
                if (selectedIndex == 0) {
                    if (Valor_Control == "-1" || Valor_Control == "SELECCIONE" || Valor_Control == "0") {
                        $(Caja_Texto).css('border-style', 'solid');
                        $(Caja_Texto).css('border-color', 'red');
                        Faltan_Datos = true;
                    }
                }
                else {
                    $(Caja_Texto).css('border-style', '');
                    $(Caja_Texto).css('border-color', '');
                }

            }
        }
    }

    Mensaje_Error = (Faltan_Datos) ? " Favor de revisar los campos en color rojo. <br/>" : "";
    return Mensaje_Error;
}
///***************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Obtener_Fecha
///DESCRIPCIÓN          : Convierte una cadena con formato dd/MM/yyyy a Date
///PARAMETROS           : Cadena: Es una cadena de texto que se convertira a tipo Date
///CREO                 : Luis Salas
///FECHA_CREO           : 17/Mayo/2013 11:42:00 a.m
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///***************************************************************************************************
function Obtener_Fecha(Cadena) {
    // Se manda a llamar de la siguiente manera
    // La cadena debe tener el formato dd/MM/yyyy
    /*
        
        var Fecha = Obtener_Fecha($("[id$='Txt_Fecha_Emision_Inicial']").val());
    */
    var Fecha = new Array();
    var Meses = ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"];
    Fecha = Cadena.split("/");

    var Fecha_Convertida = new Date(Fecha[2], Meses.indexOf(Fecha[1]), Fecha[0]);

    return Fecha_Convertida;
}
///***************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Validar_NumeroDecimal_Formateado
///DESCRIPCIÓN          : Función que suma las columna del Grid de los Gastos
///PARAMETROS           : Numero_Evaluado: Es el numero que va hacer evaluado, para ver si es un decimal
///                                        valido y asi formatearlo a tipo moneda
///CREO                 : Armando Zavala Moreno
///FECHA_CREO           : 25/Febrero/2013 04:34:00 p.m
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///***************************************************************************************************
function Validar_NumeroDecimal_Formateado(Numero_Evaluado) {
    var decimals = 2;
    var Punto_Decimal = ".";
    var Precision = 2;
    var Separador = ",";
    var Resultado = '';
    var Numero_Valido;

    Numero_Evaluado = (Numero_Evaluado + '').replace(/[^0-9+\-Ee.]/g, ''); //Se eliminan los caracters que no sean numeros
    var n = !isFinite(+Numero_Evaluado) ? 0 : +Numero_Evaluado;


    toFixedFix = function (n, Precision) {
        var k = Math.pow(10, Precision);
        return '' + Math.round(n * k) / k;
    };

    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    Resultado = (Precision ? toFixedFix(n, Precision) : '' + Math.round(n)).split('.');
    if (Resultado[0].length > 3) {
        Resultado[0] = Resultado[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, Separador);
    }
    if ((Resultado[1] || '').length < Precision) {
        Resultado[1] = Resultado[1] || '';
        Resultado[1] += new Array(Precision - Resultado[1].length + 1).join('0');
    }
    Numero_Valido = "$" + Resultado.join(Punto_Decimal);
    return Numero_Valido
}
///***************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Validar_NumeroDecimal_Formateado_SinSigno_Pesos
///DESCRIPCIÓN          : Función que suma las columna del Grid de los Gastos
///PARAMETROS           : Numero_Evaluado: Es el numero que va hacer evaluado, para ver si es un decimal
///                                        valido y asi formatearlo a tipo moneda
///CREO                 : Armando Zavala Moreno
///FECHA_CREO           : 25/Febrero/2013 04:34:00 p.m
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///***************************************************************************************************
function Validar_NumeroDecimal_Formateado_SinSigno_Pesos(Numero_Evaluado) {
    var decimals = 2;
    var Punto_Decimal = ".";
    var Precision = 2;
    var Separador = ",";
    var Resultado = '';
    var Numero_Valido;

    Numero_Evaluado = (Numero_Evaluado + '').replace(/[^0-9+\-Ee.]/g, ''); //Se eliminan los caracters que no sean numeros
    var n = !isFinite(+Numero_Evaluado) ? 0 : +Numero_Evaluado;


    toFixedFix = function (n, Precision) {
        var k = Math.pow(10, Precision);
        return '' + Math.round(n * k) / k;
    };

    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    Resultado = (Precision ? toFixedFix(n, Precision) : '' + Math.round(n)).split('.');
    if (Resultado[0].length > 3) {
        Resultado[0] = Resultado[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, Separador);
    }
    if ((Resultado[1] || '').length < Precision) {
        Resultado[1] = Resultado[1] || '';
        Resultado[1] += new Array(Precision - Resultado[1].length + 1).join('0');
    }
    Numero_Valido = Resultado.join(Punto_Decimal);
    return Numero_Valido
}
///***************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Validar_Email
///DESCRIPCIÓN          : Función que suma las columna del Grid de los Gastos
///PARAMETROS           : Email: Es el numero que va hacer evaluado, para ver si es un decimal
///CREO                 : Armando Zavala Moreno
///FECHA_CREO           : 01/Marzo/2013 04:34:00 p.m
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///***************************************************************************************************
function Validar_Email(Email) {
    // creamos nuestra regla con expresiones regulares.
    var Filtro = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
    // utilizamos test para comprobar si el parametro valor cumple la regla
    if (Filtro.test(Email))
        return true;
    else
        return false;
}
///***************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Validar_Texbox_Multiline
///DESCRIPCIÓN          : Función que suma las columna del Grid de los Gastos
///PARAMETROS           : Tamaño_Permitido: Es el numero maximo caractes permitido de la caja de texto
///                       Tamaño_Cadena: Es el tamaño actual de la caja de texto
///CREO                 : Armando Zavala Moreno
///FECHA_CREO           : 01/Marzo/2013 04:34:00 p.m
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///***************************************************************************************************
function Validar_Texbox_Multiline(Tamaño_Permitido, Tamaño_Cadena) {
    if (Tamaño_Cadena > Tamaño_Permitido) {
        this.value = this.value.substring(0, Tamaño_Permitido);
    }
}
///***************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Limpiar_Cajas
///DESCRIPCIÓN          : Función que limpia las cajas de texto y los combos
///PARAMETROS           : Div_Contenedor: Es el Div donde se encuntran nuestros contoles
///CREO                 : Armando Zavala Moreno
///FECHA_CREO           : 11/Abril/2013 11:00:00 a.m
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///***************************************************************************************************
function Limpiar_Cajas(Div_Contenedor) {
    var Control = $("[id$='" + Div_Contenedor + "']").attr('id');
    $('#' + Control + ' input:text').each(function () {
        $(this).val('');
    });
    $('#' + Control + ' select').each(function () {
        $(this).find('option:first').prop('selected', 'selected');
    });
    $('#' + Control + ' textarea').each(function () {
        $(this).val('');
    });
}
///*******************************************************************************
///DESCRIPCIÓN          : Valida que solo se tecleen numeros, un punto y con maximo
///                       dos decimales.
///PARAMETROS           :
///CREO                 : Armando Zavala Moreno
///FECHA_CREO           : 20/Abril/2013 12:22 P.M.
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
function Valida_Solo_Numeros(Control, e) {
    // Se manda a llamar de la siguiente manera
    /*
        $("[id$='Nombre_Caja']").keypress(function (e) {
            return Valida_Solo_Numeros(this, e);
        });
    */
    var Tecla;
    var Tecla_String;
    var Caja_Evaluada = $(Control).attr('id'); // Se obtiene el nombre real del control donde se presiono la tecla (por lo del updatepanel)
    var Cadena_de_la_Caja = $(Control).val(); //Obtiene el valor del control
    var Arreglo_Caja = Cadena_de_la_Caja.split('.');
    var Dos_Puntos = "";

    if (window.event) {
        Tecla = window.event.keyCode; //Obtiene el numero que pertenece a la tecla que fue presionada
    }
    else if (e) {
        Tecla = e.which;
    }
    else {
        return true;
    }
    Tecla_String = String.fromCharCode(Tecla); //Obtiene el caracter que pertenece de acuerdo al numero de tecla presionada


    if ((Tecla == null) || (Tecla == 0) || (Tecla == 8) || (Tecla == 9) || (Tecla == 13) || (Tecla == 27)) {//Permite el espacio, backspace, Tab, Enter y Esc
        return true;
    }
    else if (Cadena_de_la_Caja.length > 0) {
        if (Cadena_de_la_Caja.indexOf(".") > -1) {
            if (Tecla_String == ".") {
                return false;
            }
            else {
                Dos_Puntos = Arreglo_Caja[1].toString();
                if (Dos_Puntos.length > 1) {
                    return false;
                }
            }
        }
        else if (Cadena_de_la_Caja == "0" && Tecla_String != ".") {
            return false;
        }
        else if ((("0123456789").indexOf(Tecla_String) > -1)) {
            return true;
        }
        else if (Cadena_de_la_Caja.indexOf(".") > -1 && (Tecla_String == ".")) {//Si la cadena ya tiene un punto impide que se capture otro
            return false;
        }
        else if (Tecla_String == ".") {//Permite el punto
            return true;
        }
        else
            return false;
    }
    else {
        if ((("0123456789").indexOf(Tecla_String) > -1)) {// Si la tecla presionada es uno de los caracteres "0123456789$" es correcta
            return true;
        }
        else {
            return false;
        }
    }
}



function Validar_Campos_Cliente_Nuevo(Div) {
    var Control = $("[id$='" + Div + "']").attr('id');
    var Elementos = $('#' + Control + ' :input[id][Requerido$="true"]');
    var Caja_Texto;
    var Tipo_Control;
    var Nombre_Control;
    var Valor_Control;
    var Mensaje_Error;
    var Faltan_Datos = false;

    for (var i = 0; i < Elementos.length; i++) {
        Caja_Texto = Elementos[i];

        Tipo_Control = $(Caja_Texto).attr('type');
        Nombre_Control = $(Caja_Texto).get(0).tagName;

        if ($(Caja_Texto).attr('title') == "Cmb_Nuevo_Metodo_Pago" || $(Caja_Texto).attr('title') == "Txt_Nuevo_No_Cuenta" 
            || $(Caja_Texto).attr('title') == "Txt_Nuevo_Banco") {
            
//            if (Nombre_Control == "SELECT") {
//                var selectedIndex = $(Caja_Texto)[0].selectedIndex;
//                Valor_Control = $(Caja_Texto).val();

//                if (selectedIndex == 0) {
//                    if (Valor_Control == "-1" || Valor_Control == "SELECCIONE" || Valor_Control == "0") {
//                        $(Caja_Texto).css('border-style', 'solid');
//                        $(Caja_Texto).css('border-color', 'red');
//                        Faltan_Datos = true;
//                    }
//                }
//                else {
//                    $(Caja_Texto).css('border-style', '');
//                    $(Caja_Texto).css('border-color', '');
//                }
//            }

            if (Nombre_Control == "INPUT" || Nombre_Control == "TEXTAREA") {
                Valor_Control = $.trim($(Caja_Texto).val());

                if ($("select[id$=Cmb_Nuevo_Metodo_Pago] option:selected").val() > 0) {
                    if ($("select[id$=Cmb_Nuevo_Metodo_Pago] option:selected").text() != "NO IDENTIFICADO"
                        && $("select[id$=Cmb_Nuevo_Metodo_Pago] option:selected").text() != "EFECTIVO") {
                        if (Valor_Control.length == 0 || Valor_Control == "Dia/Mes/Año") {
                            $(Caja_Texto).css('border-style', 'solid');
                            $(Caja_Texto).css('border-color', 'red');
                            Faltan_Datos = true;
                        }
                        else {
                            $(Caja_Texto).css('border-style', '');
                            $(Caja_Texto).css('border-color', '');
                        }
                    } else {
                        $(Caja_Texto).css('border-style', '');
                        $(Caja_Texto).css('border-color', '');
                    }
                } 
                else {
                    $(Caja_Texto).css('border-style', '');
                    $(Caja_Texto).css('border-color', '');
                }

                
            }

        } else {

            if (Nombre_Control == "INPUT" || Nombre_Control == "TEXTAREA") {
                Valor_Control = $.trim($(Caja_Texto).val());
                if (Valor_Control.length == 0 || Valor_Control == "Dia/Mes/Año") {
                    $(Caja_Texto).css('border-style', 'solid');
                    $(Caja_Texto).css('border-color', 'red');
                    Faltan_Datos = true;
                }
                else {
                    $(Caja_Texto).css('border-style', '');
                    $(Caja_Texto).css('border-color', '');
                }
            }

            if (Nombre_Control == "SELECT") {
                var selectedIndex = $(Caja_Texto)[0].selectedIndex;
                Valor_Control = $(Caja_Texto).val();

                if (selectedIndex == 0) {
                    if (Valor_Control == "-1" || Valor_Control == "SELECCIONE" || Valor_Control == "0") {
                        $(Caja_Texto).css('border-style', 'solid');
                        $(Caja_Texto).css('border-color', 'red');
                        Faltan_Datos = true;
                    }
                }
                else {
                    $(Caja_Texto).css('border-style', '');
                    $(Caja_Texto).css('border-color', '');
                }
            }
         }

    }

    return Faltan_Datos;
}