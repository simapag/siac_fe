﻿$(function () {
    $("#Div_Password").modal({ backdrop: "static" });
    $("#Div_Password").modal('hide');

});

function mostrarVentana() {
    $("[id$='Div_Password']").modal();
}

function cancelarCambio() {
    $("[id$='Div_Password']").modal('hide');
    $("[id$='Txt_Contrasena_Nueva']").val('');
    $("[id$='Txt_Confirmar_Contrasena']").val('');
}

function Modificar_Contrasena() {
    var pass1 = $("[id$='Txt_Contrasena_Nueva']").val();
    var pass2 = $("[id$='Txt_Confirmar_Contrasena']").val();
    //checkForm(pass1, pass2);
    if (Validar_Pass(pass1, pass2)) {
        Actualizar_Password(pass1);
    }
}

function Validar_Pass(pass1, pass2) {
    var espacios = false;
    var cont = 0;

    while (!espacios && (cont < pass1.length)) {
        if (pass1.charAt(cont) == " ")
            espacios = true;
        cont++;
    }

    if (espacios) {
        alert("La contraseña no puede contener espacios en blanco");
        return false;
    }


    if (pass1.length == 0 || pass2.length == 0) {
        alert("Los campos de la password no pueden quedar vacios");
        return false;
    }
    if (pass1.length < 8 || pass2.length < 8) {
        alert("Minimo 8 caracteres");
        return false;
    }
    if (pass1 != pass2) {
        alert("Las contraseñas no coindiciden");
        return false;
    } else {
        return true;
    }
}

function checkForm(pass1, pass2) {
 

    if (pass1 != "" && pass1 == pass2) {
        if (pass1.length < 8) {
            alert("Error: Contraseña debe tener al menos 8 caracteres");
            pass1.focus();
            return false;
        }
        re = /[0-9]/;
        if (!re.test(pass1)) {
            alert("Error: Contraseña debe tener al menos un numero (0-9)");
            pass1.focus();
            return false;
        }
        re = /[a-z]/;
        if (!re.test(pass1)) {
            alert("Error: Contraseña debe tener al menos una letra minuscula (a-z)!");
            pass1.focus();
            return false;
        }
        re = /[A-Z]/;
        if (!re.test(pass1)) {
            alert("Error: Contraseña debe tener al menos una letra mayuscula (A-Z)!");
            pass1.focus();
            return false;
        }
    } else {
        alert("Error: Compruebe que ha introducido y confirmado su contraseña");
        pass1.focus();
        return false;
    }
    return true;
}

function Actualizar_Password(Password) {
    if (Password != "") {
        $.ajax({
            url: "Frm_Controlador_Password.aspx?Opcion=Actualizar_Password&password=" + Password,
            type: 'POST',
            async: false,
            cache: false,
            success: function (respuesta) {
                if (respuesta.Mensaje != "ok") {
                    alert("La contraseña no se ha actualizado.");
                } else {
                    alert("La contraseña se ha actualizado.");
                    cancelarCambio();
                }
            }
        });
    }
}
