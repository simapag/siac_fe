﻿using System;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using SIAC.Constantes;
using System.Collections.Generic;
using SharpContent.ApplicationBlocks.Data;
using SIAC_Apl_Parametros_Facturacion.Negocio;
using System.Text;
using SIAC.Metodos_Generales;

/// <summary>
/// Summary description for Cls_Apl_Parametros_Facturacion_Datos
/// </summary>
/// 

namespace SIAC_Apl_Parametros_Facturacion.Datos
{
    public class Cls_Apl_Parametros_Facturacion_Datos
    {

	 #region MÉTODOS
        /// ***********************************************************************************
        /// Nombre de la Función: Alta_Parametros_Facturacion
        /// Descripción         : Da de alta en la Base de Datos un nuevo Parametro
        /// Parámetros          : 
        /// Usuario Creo        : Alejandro Leyva Alvarado.
        /// Fecha Creó          : 09/Mayo/2013 18:124 p.m.
        /// Usuario Modifico    :
        /// Fecha Modifico      :
        /// ***********************************************************************************
    public static Boolean Alta_Parametros_Facturacion(Cls_Apl_Parametros_Facturacion_Negocio P_Parametros)
        {
            Boolean Alta = false;

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            StringBuilder Mi_sql = new StringBuilder();
            String Parametro_ID = "";
           
            try
            {
                Parametro_ID = Cls_Metodos_Generales.Obtener_ID_Consecutivo(Apl_Parametros_Facturacion.Tabla_Apl_Cat_Parametros_Facturacion, Apl_Parametros_Facturacion.Campo_Parametro_Factura_Id, "", 5);

                Mi_sql.Append("INSERT INTO " + Apl_Parametros_Facturacion.Tabla_Apl_Cat_Parametros_Facturacion + "(");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Parametro_Factura_Id + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Interes_Pagare + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_IVA + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Retencion_Fletes + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Retencion_IVA + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Retencion_Cedular + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Retencion_ISR  + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Email_Origen_Correos + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Mensaje_Factura + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Regimen_Fiscal + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Version + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Codigo_Usuario + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Codigo_Usuario_Proveedor + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Sucursal_Id + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Ambiente_Timbrado + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Aviso_Expira_Folio + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Aviso_Expira_Vigencia + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Vigencia_Certificado_Inicio + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Vigencia_Certificado_Fin + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Password_Llave + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Ruta_Certificado + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Ruta_Llave + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Ruta_Pdf + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Ruta_Xml + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Imagen_Factura + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Envio_Automatico_Correo + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Usuario_Creo + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Ip_Creo + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Equipo_Creo + ", ");
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Fecha_Creo);
                Mi_sql.Append(") VALUES ('");
                Mi_sql.Append(Parametro_ID + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Interes_Pagare + "', ");
                Mi_sql.Append("'" + P_Parametros.P_IVA + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Retencion_Fletes + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Retencion_Iva + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Retencion_Cedular + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Retencion_ISR + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Email_Origen_Correos + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Mensaje_Factura + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Regimen_Fiscal + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Version + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Codigo_Usuario + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Codigo_Usuario_Proveedor + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Sucursal_Id + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Ambiente_Timbrado + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Expira_Folio + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Expira_Vigencia + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Vigencia_Inicial + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Vigencia_Final + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Password_Llave + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Ruta_Certificado + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Ruta_Llave + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Ruta_Pdf + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Ruta_Xmls + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Imagen_Factura + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Envio_Automatico_Correo + "', ");
                Mi_sql.Append(")");

                Obj_Comando.CommandText = Mi_sql.ToString();
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

                Alta = true;
            }
            catch (SqlException Ex) 
            { 
                throw new Exception("Error: " + Ex.Message); 
            }
            catch (DBConcurrencyException Ex) 
            { 
                throw new Exception("Error: " + Ex.Message); 
            }
            catch (Exception Ex) 
            { 
                throw new Exception("Error: " + Ex.Message); 
            }
            return Alta;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Parametro
        ///DESCRIPCIÓN         : Modifica en la Base de Datos un nuevo Parametro
        ///PARAMENTROS         : P_Parametros.- Instancia de la Clase de Negocio de Parametros con los datos que van a ser
        ///                                   modificados.
        ///CREO                : Miguel Angel Alvarado Enriquez.
        ///FECHA_CREO          : 11/Marzo/2013 11:12 a.m. 
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
    public static Boolean Modificar_Parametro_Facturacion(Cls_Apl_Parametros_Facturacion_Negocio P_Parametros)
        {
            Boolean Modificado = false;
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            StringBuilder Mi_sql = new StringBuilder();
            try
            {
                Mi_sql.Append("UPDATE " + Apl_Parametros_Facturacion.Tabla_Apl_Cat_Parametros_Facturacion + " SET ");
                if (!String.IsNullOrEmpty(P_Parametros.P_Interes_Pagare))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Interes_Pagare + " = '" + P_Parametros.P_Interes_Pagare + "', ");
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_IVA))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_IVA + " = '" + P_Parametros.P_IVA + "', ");
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Retencion_Fletes))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Retencion_Fletes + " = '" + P_Parametros.P_Retencion_Fletes + "', ");
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Retencion_Iva))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Retencion_IVA + " = '" + P_Parametros.P_Retencion_Iva + "', ");
                }

                if (!String.IsNullOrEmpty(P_Parametros.P_Retencion_Cedular))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Retencion_Cedular + " = '" + P_Parametros.P_Retencion_Cedular + "', ");
                }


                if (!String.IsNullOrEmpty(P_Parametros.P_Retencion_ISR))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Retencion_ISR + " = '" + P_Parametros.P_Retencion_ISR + "', ");
                }

                if (!String.IsNullOrEmpty(P_Parametros.P_Password_Llave))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Password_Llave + " = '" + P_Parametros.P_Password_Llave + "', ");
                }

                if (!String.IsNullOrEmpty(P_Parametros.P_Email_Origen_Correos))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Email_Origen_Correos + " = '" + P_Parametros.P_Email_Origen_Correos + "', ");
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Mensaje_Factura))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Mensaje_Factura + " = '" + P_Parametros.P_Mensaje_Factura + "', ");
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Regimen_Fiscal))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Regimen_Fiscal + " = '" + P_Parametros.P_Regimen_Fiscal + "', ");
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Version))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Version + " = '" + P_Parametros.P_Version + "', ");
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Codigo_Usuario))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Codigo_Usuario + " = '" + P_Parametros.P_Codigo_Usuario + "', ");
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Codigo_Usuario_Proveedor))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Codigo_Usuario_Proveedor + " = '" + P_Parametros.P_Codigo_Usuario_Proveedor + "', ");
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Sucursal_Id))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Sucursal_Id + " = '" + P_Parametros.P_Sucursal_Id + "', ");
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Ambiente_Timbrado))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Ambiente_Timbrado + " = '" + P_Parametros.P_Ambiente_Timbrado + "', ");
                }

                if (!String.IsNullOrEmpty(P_Parametros.P_Ruta_Certificado))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Ruta_Certificado + " = '" + P_Parametros.P_Ruta_Certificado + "', ");
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Ruta_Llave))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Ruta_Llave + " = '" + P_Parametros.P_Ruta_Llave + "', ");
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Ruta_Pdf))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Ruta_Pdf + " = '" + P_Parametros.P_Ruta_Pdf + "', ");
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Ruta_Xmls))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Ruta_Xml + " = '" + P_Parametros.P_Ruta_Xmls + "', ");
                }

                if (!String.IsNullOrEmpty(P_Parametros.P_Imagen_Factura))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Imagen_Factura + " = '" + P_Parametros.P_Imagen_Factura + "', ");  
                }

                if (!String.IsNullOrEmpty(P_Parametros.P_Vigencia_Inicial))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Vigencia_Certificado_Inicio + " = " + "convert(datetime,'" + P_Parametros.P_Vigencia_Inicial + "', 105)" + ", ");
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Vigencia_Final))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Vigencia_Certificado_Fin + " = " + "convert(datetime,'" + P_Parametros.P_Vigencia_Final + "', 105)" + ", "); 
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Expira_Vigencia))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Aviso_Expira_Vigencia + " = '" + P_Parametros.P_Expira_Vigencia + "', ");
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Expira_Folio))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Aviso_Expira_Folio + " = '" + P_Parametros.P_Expira_Folio + "', ");
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Envio_Automatico_Correo))
                {
                    Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Envio_Automatico_Correo + " = '" + P_Parametros.P_Envio_Automatico_Correo + "', ");
                }
                Mi_sql.Append(Apl_Parametros_Facturacion.Campo_Fecha_Modifico + " = GETDATE()");
                Mi_sql.Append(" WHERE " + Apl_Parametros_Facturacion.Campo_Parametro_Factura_Id + " = '" + P_Parametros.P_Parametro_Factura_Id + "'");

                Obj_Comando.CommandText = Mi_sql.ToString();
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
                Modificado = true;

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Modificado;
        }
        ///**************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Parametros
        ///DESCRIPCIÓN         : Consulta los Pametros
        ///PARAMENTROS         : P_Parametros.- Instancia de la Clase de Negocio de Empresas con los datos que servirán de
        ///                                   filtro.
        ///CREO                : Miguel Angel Alvarado Eniquez.
        ///FECHA_CREO          : 15/Febrero/2013 
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
    public static DataTable Consultar_Parametros_Facturacion(Cls_Apl_Parametros_Facturacion_Negocio P_Parametros)
        {
            DataTable Tabla = new DataTable();
            
            String Aux_Filtros = "";
            Boolean Segundo_Filtro = false;

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            StringBuilder Mi_SQL = new StringBuilder();

            try
            {
                Mi_SQL.Append("SELECT *, rf.CLAVE as regimen_fical_clave, rf.descripcion as regimen_fiscal_descripcion,( SELECT isnull(Porcentaje_IVA,0.16)/100 from Cat_Cor_Parametros  ) as [tasa_iva_general] FROM  " + Apl_Parametros_Facturacion.Tabla_Apl_Cat_Parametros_Facturacion + " as p inner join CAT_FE_REGIMEN_FISCAL as rf on p.REGIMEN_FISCAL_ID = rf.REGIMEN_FISCAL_ID");
                if (!String.IsNullOrEmpty(P_Parametros.P_Interes_Pagare))
                {
                    Mi_SQL.Append(Segundo_Filtro ? " AND " : " WHERE ");
                    Mi_SQL.Append(Apl_Parametros_Facturacion.Campo_Interes_Pagare + " = '" + P_Parametros.P_Interes_Pagare + "' ");
                    Segundo_Filtro = true;
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_IVA))
                {
                    Mi_SQL.Append(Segundo_Filtro ? " AND " : " WHERE ");
                    Mi_SQL.Append(Apl_Parametros_Facturacion.Campo_IVA + " = '" + P_Parametros.P_IVA + "' ");
                    Segundo_Filtro = true;
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Parametro_Factura_Id))
                {
                    Mi_SQL.Append(Segundo_Filtro ? " AND " : " WHERE ");
                    Mi_SQL.Append(Apl_Parametros_Facturacion.Campo_Parametro_Factura_Id + " = '" + P_Parametros.P_Parametro_Factura_Id + "'  ");
                    Segundo_Filtro = true;
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Retencion_Fletes))
                {
                    Mi_SQL.Append(Segundo_Filtro ? " AND " : " WHERE ");
                    Mi_SQL.Append(Apl_Parametros_Facturacion.Campo_Retencion_Fletes + " = '" + P_Parametros.P_Retencion_Fletes + "' ");
                    Segundo_Filtro = true;
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Retencion_Iva))
                {
                    Mi_SQL.Append(Segundo_Filtro ? " AND " : " WHERE ");
                    Mi_SQL.Append(Apl_Parametros_Facturacion.Campo_Retencion_IVA + " = '" + P_Parametros.P_Retencion_Iva + "' ");
                    Segundo_Filtro = true;
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Retencion_Cedular))
                {
                    Mi_SQL.Append(Segundo_Filtro ? " AND " : " WHERE ");
                    Mi_SQL.Append(Apl_Parametros_Facturacion.Campo_Retencion_Cedular + " = '" + P_Parametros.P_Retencion_Cedular + "' ");
                    Segundo_Filtro = true;
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Retencion_ISR))
                {
                    Mi_SQL.Append(Segundo_Filtro ? " AND " : " WHERE ");
                    Mi_SQL.Append(Apl_Parametros_Facturacion.Campo_Retencion_ISR + " = '" + P_Parametros.P_Retencion_ISR + "' ");
                    Segundo_Filtro = true;
                }
                if (Mi_SQL.ToString().EndsWith(" AND "))
                {
                    Aux_Filtros = Mi_SQL.ToString().Substring(0, Mi_SQL.Length - 5);
                    Mi_SQL.Length = 0;
                    Mi_SQL.Append(Aux_Filtros);
                }
                if (Mi_SQL.ToString().EndsWith(" WHERE "))
                {
                    Aux_Filtros = Mi_SQL.ToString().Substring(0, Mi_SQL.Length - 7);
                    Mi_SQL.Length = 0;
                    Mi_SQL.Append(Aux_Filtros);
                }

                // agregar filtro y orden a la consulta
                Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }

        public static DataTable ObtenerUnidadConceptoSat(String concepto)
        {
            DataTable Tabla = new DataTable();

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            string Mi_SQL;

            try
            {
                Mi_SQL = "select unidad_sat, concepto_sat from Cat_Cor_Conceptos_Cobros WHERE Concepto_ID = '{0}'";

                // agregar filtro y orden a la consulta
                Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, string.Format(Mi_SQL, concepto)).Tables[0];

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }
        #endregion

    }
}