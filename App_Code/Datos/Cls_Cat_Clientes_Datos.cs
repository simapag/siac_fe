﻿using System;
using System.Text;
using System.Data;
using SIAC.Cat_Clientes.Negocio;
using SharpContent.ApplicationBlocks.Data;
using SIAC.Constantes;
//using SIAC.Sesiones;

using SIAC.Metodos_Generales;
using System.IO;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using System.Data.SqlClient;
using Erp.Sesiones;
using System.Collections.Generic;

namespace SIAC.Cat_Clientes.Datos
{
    public class Cls_Cat_Clientes_Datos
    {
        /// ***********************************************************************************
        /// Nombre de la Función: Alta_Cliente
        /// Descripción         : Da de alta en la Base de Datos un nuevo Cliente
        /// Parámetros          : 
        /// Usuario Creo        : Jose Maldonado Mendez.
        /// Fecha Creó          : 30/Noviembre/2015
        /// Usuario Modifico    :
        /// Fecha Modifico      :
        /// ***********************************************************************************
        public static Boolean Alta_Clientes(Cls_Cat_Clientes_Negocio P_Parametros)
        {
            Boolean Alta = false;

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            String Mi_sql = "";
            String Parametro_ID = "";

            try
            {
                Parametro_ID = Cls_Metodos_Generales.Obtener_ID_Consecutivo(Cat_Adm_Clientes.Tabla_Cat_Adm_Clientes, Cat_Adm_Clientes.Campo_Cliente_Id, "", 10);
                P_Parametros.P_Cliente_Id = Parametro_ID;
                Mi_sql = "INSERT INTO " + Cat_Adm_Clientes.Tabla_Cat_Adm_Clientes + "(";
                Mi_sql += Cat_Adm_Clientes.Campo_Cliente_Id + ", ";
                Mi_sql += Cat_Adm_Clientes.Campo_Nombre_Corto + ", ";
                Mi_sql += Cat_Adm_Clientes.Campo_Razon_Social + ", ";
                Mi_sql += Cat_Adm_Clientes.Campo_RFC + ", ";
                Mi_sql += Cat_Adm_Clientes.Campo_CURP + ", ";
                Mi_sql += Cat_Adm_Clientes.Campo_Pais + ", ";
                Mi_sql += Cat_Adm_Clientes.Campo_Localidad + ", ";
                Mi_sql += Cat_Adm_Clientes.Campo_Colonia + ", ";
                Mi_sql += Cat_Adm_Clientes.Campo_Ciudad + ", ";
                Mi_sql += Cat_Adm_Clientes.Campo_Calle + ", ";
                Mi_sql += Cat_Adm_Clientes.Campo_Numero_Exterior + ", ";
                Mi_sql += Cat_Adm_Clientes.Campo_Numero_Interior + ", ";
                Mi_sql += Cat_Adm_Clientes.Campo_CP + ", ";
                Mi_sql += Cat_Adm_Clientes.Campo_Email + ", ";
                Mi_sql += Cat_Adm_Clientes.Campo_Estatus + ", ";
                Mi_sql += Cat_Adm_Clientes.Campo_Predio_Id + ", ";
                Mi_sql += Cat_Adm_Clientes.Campo_Estado + ", ";
                Mi_sql += Cat_Adm_Clientes.Campo_Contrasena + ", ";
                Mi_sql += Cat_Adm_Clientes.Campo_Usuario_Creo + ", ";
                Mi_sql += Cat_Adm_Clientes.Campo_Fecha_Creo;
                Mi_sql += ") VALUES (";
                Mi_sql += "'" + P_Parametros.P_Cliente_Id + "', ";
                Mi_sql += "'" + P_Parametros.P_Nombre_Corto + "', ";
                Mi_sql += "'" + P_Parametros.P_Razon_Social + "', ";
                Mi_sql += "'" + P_Parametros.P_Rfc.ToUpper() + "', ";
                Mi_sql += "'" + P_Parametros.P_Curp.ToUpper() + "', ";
                Mi_sql += "'" + P_Parametros.P_Pais + "', ";
                Mi_sql += "'" + P_Parametros.P_Localidad + "', ";
                Mi_sql += "'" + P_Parametros.P_Colonia + "', ";
                Mi_sql += "'" + P_Parametros.P_Ciudad + "', ";
                Mi_sql += "'" + P_Parametros.P_Calle + "', ";
                Mi_sql += "'" + P_Parametros.P_Numero_Exterior + "', ";
                Mi_sql += "'" + P_Parametros.P_Numero_Interior + "', ";
                Mi_sql += "'" + P_Parametros.P_Cp + "', ";
                Mi_sql += "'" + P_Parametros.P_Email + "', ";
                Mi_sql += "'" + P_Parametros.P_Estatus + "', ";
                Mi_sql += "'" + P_Parametros.P_Predio_Id + "', ";
                Mi_sql += "'" + P_Parametros.P_Estado + "', ";
                Mi_sql += "'" + P_Parametros.P_Contrasena + "', ";
                Mi_sql += "'" + "" + "', ";
                Mi_sql += "GETDATE())";


                Obj_Comando.CommandText = Mi_sql.ToString();
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

                Alta = true;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Alta;
        }

        ///**************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Predios
        ///DESCRIPCIÓN         : Consulta los clientes
        ///PARAMENTROS         : P_Parametros.- Instancia de la Clase de Negocio de Clientes con los datos que servirán de
        ///                                   filtro.
        ///CREO                : José Maldonado Méndez
        ///FECHA_CREO          : 30/Noviembre/2015
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        public static DataTable Consultar_Clientes(Cls_Cat_Clientes_Negocio P_Parametros)
        {
            DataTable Tabla = new DataTable();

            String Aux_Filtros = "";
            Boolean Segundo_Filtro = false;
try
            {
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            String Mi_SQL = "";

            
                Mi_SQL += "SELECT *, Cat_Cor_Predios.RPU, isnull(Cat_Cor_Usuarios.NOMBRE, '') + ' ' + isnull(Cat_Cor_Usuarios.APELLIDO_PATERNO, '') + ' ' + isnull(Cat_Cor_Usuarios.APELLIDO_MATERNO, '') AS [usuario]  FROM  " + Cat_Adm_Clientes.Tabla_Cat_Adm_Clientes;
                Mi_SQL += " INNER JOIN Cat_Cor_Predios ON CAT_ADM_CLIENTES.PREDIO_ID = Cat_Cor_Predios.Predio_ID";
                Mi_SQL += " INNER JOIN Cat_Cor_Usuarios ON Cat_Cor_Usuarios.USUARIO_ID = Cat_Cor_Predios.Usuario_ID";
                if (!String.IsNullOrEmpty(P_Parametros.P_Cliente_Id))
                {
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE ";
                    Mi_SQL += Cat_Adm_Clientes.Campo_Cliente_Id + " = '" + P_Parametros.P_Cliente_Id + "'";
                    Segundo_Filtro = true;
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Email))
                {
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE ";
                    Mi_SQL += Cat_Adm_Clientes.Campo_Email + " = '" + P_Parametros.P_Email + "'";
                    Segundo_Filtro = true;
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Contrasena))
                {
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE ";
                    Mi_SQL += Cat_Adm_Clientes.Campo_Contrasena + " = '" + P_Parametros.P_Contrasena + "'";
                    Segundo_Filtro = true;
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Estatus)) {
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE "; Segundo_Filtro = true;
                    Mi_SQL += Cat_Adm_Clientes.Tabla_Cat_Adm_Clientes + "." + Cat_Adm_Clientes.Campo_Estatus + " = '" + P_Parametros.P_Estatus + "'";
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Predio_Id)) {
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE "; Segundo_Filtro = true;
                    Mi_SQL += Cat_Adm_Clientes.Tabla_Cat_Adm_Clientes + "." + Cat_Adm_Clientes.Campo_Predio_Id + " = '" + P_Parametros.P_Predio_Id + "'";
                }
                // agregar filtro y orden a la consulta
                Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }
        
        ///**************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Cliente
        ///DESCRIPCIÓN         : Modifica los datos del cliente
        ///PARAMENTROS         : P_Parametros.- Instancia de la Clase de Negocio de Clientes con los datos que se modificaran.
        ///CREO                : José Maldonado Méndez
        ///FECHA_CREO          : 02/Diciembre/2015
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        public static Boolean Modificar_Cliente(Cls_Cat_Clientes_Negocio P_Parametros)
        {
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            Boolean Modificar = false;
            String Mi_Sql = "";
            try
            {
                Mi_Sql = "UPDATE " + Cat_Adm_Clientes.Tabla_Cat_Adm_Clientes + " SET ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Nombre_Corto))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Nombre_Corto + " = '" + P_Parametros.P_Nombre_Corto + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Razon_Social))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Razon_Social + " = '" + P_Parametros.P_Razon_Social + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Rfc))
                    Mi_Sql += Cat_Adm_Clientes.Campo_RFC + " = '" + P_Parametros.P_Rfc.ToUpper() + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Curp))
                    Mi_Sql += Cat_Adm_Clientes.Campo_CURP + " = '" + P_Parametros.P_Curp.ToUpper() + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Pais))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Pais + " = '" + P_Parametros.P_Pais + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Localidad))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Localidad + " = '" + P_Parametros.P_Localidad + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Colonia))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Colonia + " = '" + P_Parametros.P_Colonia + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Ciudad))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Ciudad + " = '" + P_Parametros.P_Ciudad + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Calle))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Calle + " = '" + P_Parametros.P_Calle + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Numero_Exterior))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Numero_Exterior + " = '" + P_Parametros.P_Numero_Exterior + "', ";
                //if (!String.IsNullOrEmpty(P_Parametros.P_Numero_Interior))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Numero_Interior + " = '" + P_Parametros.P_Numero_Interior + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Cp))
                    Mi_Sql += Cat_Adm_Clientes.Campo_CP + " = '" + P_Parametros.P_Cp + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Email))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Email + " = '" + P_Parametros.P_Email + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Estatus))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Estatus + " = '" + P_Parametros.P_Estatus + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Predio_Id))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Predio_Id + " = '" + P_Parametros.P_Predio_Id + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Estado))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Estado + " = '" + P_Parametros.P_Estado + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Contrasena))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Contrasena + " = '" + P_Parametros.P_Contrasena + "', ";
                Mi_Sql += Cat_Adm_Clientes.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Corto + "', ";
                Mi_Sql += Cat_Adm_Clientes.Campo_Fecha_Modifico + " = GETDATE()";

                Mi_Sql += " WHERE " + Cat_Adm_Clientes.Campo_Cliente_Id + " = '" + P_Parametros.P_Cliente_Id + "'";
                Obj_Comando.CommandText = Mi_Sql;
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

                Modificar = true;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Modificar;
        }

        ///**************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Cliente_Por_Predio
        ///DESCRIPCIÓN         : Modifica los datos del cliente
        ///PARAMENTROS         : P_Parametros.- Instancia de la Clase de Negocio de Clientes con los datos que se modificaran.
        ///CREO                : Ana Laura Huichapa Ramirez
        ///FECHA_CREO          : 26/Octubre/2016
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        public static Boolean Modificar_Cliente_Por_Predio(Cls_Cat_Clientes_Negocio P_Parametros)
        {
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            Boolean Modificar = false;
            String Mi_Sql = "";
            try
            {
                Mi_Sql = "UPDATE " + Cat_Adm_Clientes.Tabla_Cat_Adm_Clientes + " SET ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Nombre_Corto))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Nombre_Corto + " = '" + P_Parametros.P_Nombre_Corto + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Razon_Social))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Razon_Social + " = '" + P_Parametros.P_Razon_Social + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Rfc))
                    Mi_Sql += Cat_Adm_Clientes.Campo_RFC + " = '" + P_Parametros.P_Rfc.ToUpper() + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Curp))
                    Mi_Sql += Cat_Adm_Clientes.Campo_CURP + " = '" + P_Parametros.P_Curp.ToUpper() + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Pais))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Pais + " = '" + P_Parametros.P_Pais + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Localidad))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Localidad + " = '" + P_Parametros.P_Localidad + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Colonia))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Colonia + " = '" + P_Parametros.P_Colonia + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Ciudad))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Ciudad + " = '" + P_Parametros.P_Ciudad + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Calle))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Calle + " = '" + P_Parametros.P_Calle + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Numero_Exterior))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Numero_Exterior + " = '" + P_Parametros.P_Numero_Exterior + "', ";
                //if (!String.IsNullOrEmpty(P_Parametros.P_Numero_Interior))
                Mi_Sql += Cat_Adm_Clientes.Campo_Numero_Interior + " = '" + P_Parametros.P_Numero_Interior + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Cp))
                    Mi_Sql += Cat_Adm_Clientes.Campo_CP + " = '" + P_Parametros.P_Cp + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Email))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Email + " = '" + P_Parametros.P_Email + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Estatus))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Estatus + " = '" + P_Parametros.P_Estatus + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Estado))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Estado + " = '" + P_Parametros.P_Estado + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Contrasena))
                    Mi_Sql += Cat_Adm_Clientes.Campo_Contrasena + " = '" + P_Parametros.P_Contrasena + "', ";
                Mi_Sql += Cat_Adm_Clientes.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Corto + "', ";
                Mi_Sql += Cat_Adm_Clientes.Campo_Fecha_Modifico + " = GETDATE()";

                Mi_Sql += " WHERE " + Cat_Adm_Clientes.Campo_Predio_Id + " = '" + P_Parametros.P_Predio_Id + "'";
                Obj_Comando.CommandText = Mi_Sql;
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

                Modificar = true;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Modificar;
        }

        public static DataTable Consultar_Predios(Cls_Cat_Clientes_Negocio P_Parametros)
        {
            DataTable Tabla = new DataTable();

            String Aux_Filtros = "";
            Boolean Segundo_Filtro = false;

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            String Mi_SQL = "";

            try
            {
                Mi_SQL += "SELECT * FROM CAT_ADM_CLIENTES INNER JOIN Cat_Cor_Predios ON Cat_Cor_Predios.Predio_ID = CAT_ADM_CLIENTES.PREDIO_ID";

                if (!String.IsNullOrEmpty(P_Parametros.P_Predio_Id))
                {
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE "; Segundo_Filtro = true;
                    Mi_SQL += "RPU = '" + P_Parametros.P_Predio_Id + "'";
                }

                Mi_SQL += Segundo_Filtro ? " AND " : " WHERE "; Segundo_Filtro = true;
                Mi_SQL += Cat_Adm_Clientes.Tabla_Cat_Adm_Clientes + "." + Cat_Adm_Clientes.Campo_Estatus + " = 'ACTIVO'";

                // agregar filtro y orden a la consulta
                Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }

        public static DataTable Verificar_Email(Cls_Cat_Clientes_Negocio Parametros)
        {

            DataTable Tabla = new DataTable();
            Boolean Segundo_Filtro = false;

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            String Mi_SQL = "";

            try
            {
                Mi_SQL += "SELECT * FROM CAT_ADM_CLIENTES";

                if (!String.IsNullOrEmpty(Parametros.P_Cliente_Id))
                {
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE "; Segundo_Filtro = true;
                    Mi_SQL += "Cliente_Id != '" + Parametros.P_Cliente_Id + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Email)) {
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE "; Segundo_Filtro = true;
                    Mi_SQL += "Email = '" + Parametros.P_Email.Trim() + "'";
                }

                Mi_SQL += Segundo_Filtro ? " AND " : " WHERE "; Segundo_Filtro = true;
                Mi_SQL += Cat_Adm_Clientes.Tabla_Cat_Adm_Clientes + "." + Cat_Adm_Clientes.Campo_Estatus + " = 'ACTIVO'";

                // agregar filtro y orden a la consulta
                Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }

        public static DataTable VerificarEmail(Cls_Cat_Clientes_Negocio Datos)
        {
            string sql = "";
            DataTable dt_cliente = new DataTable();
            //sql = "SELECT * FROM CAT_ADM_CLIENTES WHERE EMAIL = '" + Datos.P_Email + "' AND ESTATUS IN ('ACTIVO', 'BLOQUEADO', 'PERMANENTE') AND CONTRASEÑA IS NOT NULL";
            sql = "SELECT CAT_ADM_CLIENTES.*, isnull(Cat_Cor_Usuarios.NOMBRE, '') + ' ' + isnull(Cat_Cor_Usuarios.APELLIDO_PATERNO, '') + ' ' + isnull(Cat_Cor_Usuarios.APELLIDO_MATERNO, '') AS [usuario] FROM CAT_ADM_CLIENTES INNER JOIN Cat_Cor_Predios ON CAT_ADM_CLIENTES.PREDIO_ID = Cat_Cor_Predios.Predio_ID INNER JOIN Cat_Cor_Usuarios ON Cat_Cor_Usuarios.USUARIO_ID = Cat_Cor_Predios.Usuario_ID WHERE CAT_ADM_CLIENTES.EMAIL = '" + Datos.P_Email + "' AND CAT_ADM_CLIENTES.ESTATUS IN ('ACTIVO', 'BLOQUEADO', 'PERMANENTE') AND CAT_ADM_CLIENTES.CONTRASEÑA IS NOT NULL";

            dt_cliente = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, sql).Tables[0];

            return dt_cliente;
        }

        internal static List<Cls_Bloqueo> ConsultarDatosBloqueos(string cliente_id)
        {
            List<Cls_Bloqueo> lista = new List<Cls_Bloqueo>();
            String sql = string.Empty;
            sql = "SELECT CLIENTE_ID, ESTATUS, No_Intentos_Temporal, No_Bloqueo_Temporal, Fecha_Bloqueo_Temporal FROM CAT_ADM_CLIENTES ";
            sql += "Where CLIENTE_ID = '" + cliente_id + "'";

            DataTable Dt = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, sql).Tables[0];


            lista = Cls_Metodos_Generales.ConvertDataTableToList<Cls_Bloqueo>(Dt);

            return lista;
        }

        public static void Modificar_Datos_Bloqueos(int numero_intento, int no_bloqueos, string cliente,string estatus)
        {
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            Boolean Modificar = false;
            String Mi_Sql = "";
            try
            {
                Mi_Sql = "UPDATE " + Cat_Adm_Clientes.Tabla_Cat_Adm_Clientes + " SET ";
                Mi_Sql += " ESTATUS = '" + estatus + "',";
                Mi_Sql += " No_Intentos_Temporal = '" + numero_intento + "',";
                Mi_Sql += " No_Bloqueo_Temporal = '" + no_bloqueos +"',";
                Mi_Sql += " Fecha_Bloqueo_Temporal = getdate()"; 

                Mi_Sql += " WHERE " + Cat_Adm_Clientes.Campo_Cliente_Id + " = '" + cliente + "'";
                Obj_Comando.CommandText = Mi_Sql;
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

            }

            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

        }
    }
}
