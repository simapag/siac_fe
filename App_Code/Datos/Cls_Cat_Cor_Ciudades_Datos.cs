﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIAC.Constantes;
using SharpContent.ApplicationBlocks.Data;
using SIAC.Ciudades.Negocios;

/// <summary>
/// Summary description for Cls_Cat_Cor_Ciudades_Datos
/// </summary>
/// 
namespace SIAC.Ciudades.Datos
{
    public class Cls_Cat_Cor_Ciudades_Datos
    {
        #region Métodos
        public Cls_Cat_Cor_Ciudades_Datos() { }

        ///****************************************************************************************
        //    NOMBRE_FUNCION : Insertar_Ciudad
        //    DESCRIPCION    : Insertar un registro de un Ciudad a la base de datos           
        //    PARAMETROS     : El objeto Datos que se encarga con la comunicación de la clase de Negocio
        //    CREO           : Sergio Ulises Durán Hérnández
        //    FECHA_CREO     : 18-Agosto-2011
        //    MODIFICO       :
        //    FECHA_MODIFICO :
        //    CAUSA_MODIFICO :
        //****************************************************************************************/

        public static void Insertar_Ciudad(Cls_Cat_Cor_Ciudades_Negocio Datos)
        {
            //Declaración de las variables
            SqlTransaction Obj_Transaccion;
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            SqlCommand Obj_Comando = new SqlCommand();
            String Mi_SQL;
            Object ID_Consecutivo;
            Object Ultima_Clave;

            try
            {
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Query para traer el último ID
                Mi_SQL = "SELECT ISNULL( MAX(" + Cat_Cor_Ciudades.Campo_Ciudad_ID + "), '00000' ) " +
                         " FROM " + Cat_Cor_Ciudades.Tabla_Cat_Cor_Ciudades;

                ID_Consecutivo = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                if (!Convert.IsDBNull(ID_Consecutivo))      //Si la consulta no regresa nulo, obtener el nuevo ID
                {
                    Datos.P_Ciudad_ID = string.Format("{0:00000}", Convert.ToInt32(ID_Consecutivo) + 1);

                    //Query para obtener la última clave de la tabla del campo Ciudad_ID
                    Mi_SQL = "SELECT TOP(1) " + Cat_Cor_Ciudades.Campo_Ciudad_ID +
                                " FROM " + Cat_Cor_Ciudades.Tabla_Cat_Cor_Ciudades +
                                " ORDER BY " + Cat_Cor_Ciudades.Campo_Ciudad_ID + " DESC";

                    Ultima_Clave = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                    if (Ultima_Clave != null)    // Si en la base de datos se encontró una clave con el Ciudad proporcionado,
                    {                                       // sumar 1 al consecutivo para obtener la nueva clave
                        Ultima_Clave = Ultima_Clave.ToString().Substring(Ultima_Clave.ToString().Length - 5);  //Ultimos 5 caracteres del dato obtenido
                        Datos.P_Ciudad_ID = String.Format("{0:00000}", Convert.ToInt32(Ultima_Clave) + 1);
                    }
                    else                    // Si no se encontraron datos, asignar el primer para la región
                    {
                        Datos.P_Ciudad_ID = "00001";
                    }
                }
                else
                {
                    Datos.P_Ciudad_ID = "00001";
                }

                //Query para la inserción de una ciudad
                Mi_SQL = "INSERT INTO " + Cat_Cor_Ciudades.Tabla_Cat_Cor_Ciudades +
                          "(" + Cat_Cor_Ciudades.Campo_Ciudad_ID + "," + Cat_Cor_Ciudades.Campo_Estado_ID + "," + Cat_Cor_Ciudades.Campo_Clave + "," + Cat_Cor_Ciudades.Campo_Estatus + "," +
                          Cat_Cor_Ciudades.Campo_Nombre + "," + Cat_Cor_Ciudades.Campo_Usuario_Creo + "," + Cat_Cor_Ciudades.Campo_Fecha_Creo + ") " +
                          "VALUES('" + Datos.P_Ciudad_ID + "','" + Datos.P_Estado_ID + "','" + Datos.P_Clave + "','" + Datos.P_Estatus + "','" +
                          Datos.P_Nombre + "','" + Datos.P_Usuario_Creo + "',GETDATE())";

                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        ///****************************************************************************************
        //    NOMBRE_FUNCION : Modificar_Ciudad
        //    DESCRIPCION    : Modificar un registro de un Ciudad de la base de datos           
        //    PARAMETROS     : El objeto Datos que se encarga con la comuniación de la clase de Negocio
        //    CREO           : Sergio Ulises Durán Hernández
        //    FECHA_CREO     : 18-Agosto-2011
        //    MODIFICO       :
        //    FECHA_MODIFICO :
        //    CAUSA_MODIFICO :
        //****************************************************************************************/

        public static void Modificar_Ciudad(Cls_Cat_Cor_Ciudades_Negocio Datos)
        {
            //Declaración de las variables
            SqlTransaction Obj_Transaccion;
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            SqlCommand Obj_Comando = new SqlCommand();
            String Mi_SQL;

            try
            {
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Query para modificar un Ciudad de acuerdo a su ID
                Mi_SQL = "UPDATE " + Cat_Cor_Ciudades.Tabla_Cat_Cor_Ciudades +
                         " SET " + Cat_Cor_Ciudades.Campo_Estado_ID + " = '" + Datos.P_Estado_ID + "'" +
                         " , " + Cat_Cor_Ciudades.Campo_Clave + " = '" + Datos.P_Clave + "'" +
                         " , " + Cat_Cor_Ciudades.Campo_Estatus + " = '" + Datos.P_Estatus + "'" +
                         " , " + Cat_Cor_Ciudades.Campo_Nombre + " = '" + Datos.P_Nombre + "'" +
                         " , " + Cat_Cor_Ciudades.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Modifico + "'" +
                         " , " + Cat_Cor_Ciudades.Campo_Fecha_Modifico + " = GETDATE() " +
                         " WHERE " + Cat_Cor_Ciudades.Campo_Ciudad_ID + " = '" + Datos.P_Ciudad_ID + "'";

                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        ///****************************************************************************************
        //    NOMBRE_FUNCION : Eliminar_Ciudad
        //    DESCRIPCION    : Eliminar un registro de un Ciudad de la base de datos           
        //    PARAMETROS     : El objeto Datos que se encarga con la comunicación de la clase de Negocio
        //    CREO           : Sergio Ulises Durán Hernández
        //    FECHA_CREO     : 18-Agosto-2011
        //    MODIFICO       :
        //    FECHA_MODIFICO :
        //    CAUSA_MODIFICO :
        //****************************************************************************************/

        public static void Eliminar_Ciudad(Cls_Cat_Cor_Ciudades_Negocio Datos)
        {
            //Declaración de las variables
            SqlTransaction Obj_Transaccion;
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            SqlCommand Obj_Comando = new SqlCommand();
            String Mi_SQL;

            try
            {
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Query para eliminar una Ciudad de la BD de acuerdo a su ID
                Mi_SQL = "DELETE FROM " + Cat_Cor_Ciudades.Tabla_Cat_Cor_Ciudades +
                         " WHERE " + Cat_Cor_Ciudades.Campo_Ciudad_ID + " = '" + Datos.P_Ciudad_ID + "'";

                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }


        public static DataTable obtenerCiudad(string estado_id) {

            String Mi_SQL;
            DataTable Dt_Temporal;

            Mi_SQL = " SELECT CIUDAD_ID as [id],NOMBRE as [name] " +
                     "FROM CAT_COR_CIUDADES " +
                     "where ESTADO_ID='" + estado_id + "' " +
                     "ORDER by NOMBRE";

            Dt_Temporal = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Temporal;
        
        }


        ///****************************************************************************************
        //    NOMBRE_FUNCION : Buscar_Ciudad
        //    DESCRIPCION    : Buscar un registro de un Ciudad de la base de datos           
        //    PARAMETROS     : El objeto Datos que se encarga con la comuniación de la clase de Negocio
        //    CREO           : Sergio Ulises Durán Hernández
        //    FECHA_CREO     : 18-Agosto-2011
        //    MODIFICO       :
        //    FECHA_MODIFICO :
        //    CAUSA_MODIFICO :
        //****************************************************************************************/

        public static DataTable Buscar_Ciudad(Cls_Cat_Cor_Ciudades_Negocio Datos)
        {
            //Declaración de las variables
            String Mi_SQL;
            DataTable Dt_Temporal;

            try
            {
                //Query para buscar un Ciudad de acuerdo a su nombre
                Mi_SQL = "SELECT c." + Cat_Cor_Ciudades.Campo_Ciudad_ID
                       + " , c." + Cat_Cor_Estados.Campo_Estado_ID
                       + " , c." + Cat_Cor_Estados.Campo_Nombre + " AS Estado"
                       + " ,c." + Cat_Cor_Ciudades.Campo_Clave
                       + " , UPPER(c." + Cat_Cor_Ciudades.Campo_Nombre + ") AS NOMBRE"
                       + " , c." + Cat_Cor_Ciudades.Campo_Estatus
                       + " FROM " + Cat_Cor_Ciudades.Tabla_Cat_Cor_Ciudades + " c"
                       + " LEFT OUTER JOIN CAT_COR_ESTADOS e ON c.ESTADO_ID = e.ESTADO_ID";
                if (!String.IsNullOrEmpty(Datos.P_Estado_ID))
                    Mi_SQL += " WHERE e.ESTADO_ID = '" + Datos.P_Estado_ID + "' or e.NOMBRE = '" + Datos.P_Estado_ID + "'";

                Dt_Temporal = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                return Dt_Temporal;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        ///****************************************************************************************
        //    NOMBRE_FUNCION : Llenar_Grid_Ciudad
        //    DESCRIPCION    : Llenar el Grid del Formulario Frm_Cat_Ciudades
        //    PARAMETROS     : El objeto Datos que se encarga con la comuniación de la clase de Negocio
        //    CREO           : Sergio Ulises Durán Hernández
        //    FECHA_CREO     : 19-Agosto-2011
        //    MODIFICO       :
        //    FECHA_MODIFICO :
        //    CAUSA_MODIFICO :
        //****************************************************************************************/
        public static DataTable Llenar_Grid_Ciudad(Cls_Cat_Cor_Ciudades_Negocio Datos)
        {
            //Declaración de las variables
            String Mi_SQL;
            DataTable Dt_Temporal;

            try
            {
                //Query para llenar el grid del formulario
                Mi_SQL = "SELECT " + Cat_Cor_Ciudades.Tabla_Cat_Cor_Ciudades + "." + Cat_Cor_Ciudades.Campo_Ciudad_ID
                       + " , " + Cat_Cor_Ciudades.Tabla_Cat_Cor_Ciudades + "." + Cat_Cor_Estados.Campo_Estado_ID
                       + " , " + Cat_Cor_Estados.Tabla_Cat_Cor_Estados + "." + Cat_Cor_Estados.Campo_Nombre + " AS Estado"
                       + " , " + Cat_Cor_Ciudades.Tabla_Cat_Cor_Ciudades + "." + Cat_Cor_Ciudades.Campo_Clave
                       + " , " + Cat_Cor_Ciudades.Tabla_Cat_Cor_Ciudades + "." + Cat_Cor_Ciudades.Campo_Nombre
                       + " , " + Cat_Cor_Ciudades.Tabla_Cat_Cor_Ciudades + "." + Cat_Cor_Ciudades.Campo_Estatus
                       + " FROM " + Cat_Cor_Ciudades.Tabla_Cat_Cor_Ciudades + "," + Cat_Cor_Estados.Tabla_Cat_Cor_Estados
                       + " WHERE " + Cat_Cor_Ciudades.Tabla_Cat_Cor_Ciudades + "." + Cat_Cor_Ciudades.Campo_Estado_ID + "=" + Cat_Cor_Estados.Tabla_Cat_Cor_Estados + "." + Cat_Cor_Estados.Campo_Estado_ID;

                Dt_Temporal = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                return Dt_Temporal;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        ///*******************************************************************************
        /// NOMBRE_FUNCION : Consulta_Estados
        /// DESCRIPCION    : Consulta los estados de la base de datos
        /// PARAMETROS     : Datos: Variable de la capa de negocios que contiene los datos a modificar
        /// CREO           : Sergio Ulises Durán Hernández
        /// FECHA_CREO     : 18-Agosto-2011
        /// MODIFICO       :
        /// FECHA_MODIFICO :
        /// CAUSA_MODIFICO :
        ///*******************************************************************************/
        public static DataTable Consulta_Estados(Cls_Cat_Cor_Ciudades_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;

            try
            {
                //Asignar consulta para los datos
                Mi_SQL = "SELECT " + Cat_Cor_Estados.Campo_Estado_ID + "," + Cat_Cor_Estados.Campo_Nombre;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Cor_Estados.Tabla_Cat_Cor_Estados;
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Cor_Estados.Campo_Nombre;

                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        
        ///*******************************************************************************
        /// NOMBRE_FUNCION : Valida_Ciudades
        /// DESCRIPCION    : Consulta si existe la ciudad o clave
        /// PARAMETROS     : Datos: Variable de la capa de negocios que contiene los datos a modificar
        /// CREO           : David Herrera Rincón
        /// FECHA_CREO     : 29-Junio-2012
        /// MODIFICO       :
        /// FECHA_MODIFICO :
        /// CAUSA_MODIFICO :
        ///*******************************************************************************/
        public static DataTable Valida_Ciudades(Cls_Cat_Cor_Ciudades_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;
            DataTable Dt_Temporal;
            try
            {
                //Asignar consulta para los datos
                Mi_SQL = "SELECT " + Cat_Cor_Ciudades.Campo_Clave + "," + Cat_Cor_Ciudades.Campo_Nombre;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Cor_Ciudades.Tabla_Cat_Cor_Ciudades;
                Mi_SQL = Mi_SQL + " WHERE Nombre='" + Datos.P_Nombre + "' OR Clave='" + Datos.P_Clave + "'";

                //Entregar resultado
                Dt_Temporal = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                return Dt_Temporal;
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        #endregion
    }
}