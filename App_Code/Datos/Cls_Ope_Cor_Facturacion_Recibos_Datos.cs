﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SharpContent.ApplicationBlocks.Data;
using SIAC.Constantes;
using SIAC.Ope_Facturacion_Recibos.Negocio;
/// <summary>
/// Descripción breve de Cls_Ope_Cor_Facturacion_Recibos_Datos
/// </summary>
/// 
namespace SIAC.Ope_Facturacion_Recibos.Datos{
    public class Cls_Ope_Cor_Facturacion_Recibos_Datos
    {
        #region Metodos
        ///**************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Facturacion_Recibos
        ///DESCRIPCIÓN         : Consulta la facturacion de recibos
        ///PARAMENTROS         : P_Parametros.- Instancia de la Clase de Negocio de Facturacion Recibos con los datos que servirán de
        ///                                   filtro.
        ///CREO                : Jose Maldonado Mendez
        ///FECHA_CREO          : 28/Noviembre/2015
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        public static DataTable Consultar_Facturacion_Recibos(Cls_Ope_Cor_Facturacion_Recibos_Negocio P_Parametros)
        {
            DataTable Tabla = new DataTable();

            Boolean Segundo_Filtro = false;

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            String Mi_SQL = "";

            try
            {
                Mi_SQL += "SELECT TOP 1 * FROM  " + Ope_Cor_Facturacion_Recibos.Tabla_Ope_Cor_Facturacion_Recibos;
                if (!String.IsNullOrEmpty(P_Parametros.P_No_Factura_Recibo)) { 
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE ";
                    Mi_SQL += Ope_Cor_Facturacion_Recibos.Campo_No_Factura_Recibo + " = '" + P_Parametros.P_No_Factura_Recibo + "'";
                    Segundo_Filtro = true;
                }

                if (!String.IsNullOrEmpty(P_Parametros.P_No_Cuenta))
                {
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE ";
                    Mi_SQL += Ope_Cor_Facturacion_Recibos.Campo_No_Cuenta + " = '" + P_Parametros.P_No_Cuenta + "'";
                    Segundo_Filtro = true;
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_RPU))
                {
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE ";
                    Mi_SQL += Ope_Cor_Facturacion_Recibos.Campo_RPU + " = '" + P_Parametros.P_RPU + "'";
                    Segundo_Filtro = true;
                }
                if (P_Parametros.P_Total_Pagar != Decimal.MinValue) {
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE ";
                    Mi_SQL += Ope_Cor_Facturacion_Recibos.Campo_Total_Pagar + " = '" + P_Parametros.P_Total_Pagar + "'";
                    Segundo_Filtro = true;
                }
                if (P_Parametros.P_Fecha_Emision != DateTime.MinValue)
                {
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE ";
                    Mi_SQL += Ope_Cor_Facturacion_Recibos.Campo_Fecha_Emision + " >= '" + P_Parametros.P_Fecha_Emision.ToString("yyyyMMdd") + "'";
                    Mi_SQL += " AND " + Ope_Cor_Facturacion_Recibos.Campo_Fecha_Emision + " < '" + P_Parametros.P_Fecha_Emision.AddDays(1).ToString("yyyyMMdd") + "'";
                    Segundo_Filtro = true;
                }
                Mi_SQL += " ORDER BY " + Ope_Cor_Facturacion_Recibos.Campo_Fecha_Emision + " DESC";


                // agregar filtro y orden a la consulta
                Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }

        #endregion
    }
}