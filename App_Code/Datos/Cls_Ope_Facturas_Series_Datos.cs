﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using SIAC.Constantes;
using System.Data.SqlClient;
using SIAC.Ope_Facturas_Series.Negocio;
using System.Collections.Generic;
using SharpContent.ApplicationBlocks.Data;
using SIAC.Metodos_Generales;

namespace SIAC.Ope_Facturas_Series.Datos
{
    public class Cls_Ope_Facturas_Series_Datos
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Serie
        ///DESCRIPCIÓN: Da de alta en la Base de Datos una nueva Serie
        ///PARAMENTROS:     
        ///             1. P_Series.        Instancia de la Clase de Negocio de Series 
        ///                                 con los datos del que van a ser
        ///                                 dados de Alta.
        ///CREO: Miguel Angel Alvarado Enriquez
        ///FECHA_CREO: 26/Diciembre/2013
        ///MODIFICO:       
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static Boolean Alta_Serie(Cls_Ope_Facturas_Series_Negocio P_Series)
        {
            Boolean Alta = false;

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            StringBuilder Mi_sql = new StringBuilder();
            try
            {
                Mi_sql.Append("INSERT INTO " + Cat_Facturas_Series.Tabla_Cat_Facturas_Series + "(");
                Mi_sql.Append(Cat_Facturas_Series.Campo_Serie_Id + ", ");
                Mi_sql.Append(Cat_Facturas_Series.Campo_Serie + ", ");
                Mi_sql.Append(Cat_Facturas_Series.Campo_Descripcion + ", ");
                Mi_sql.Append(Cat_Facturas_Series.Campo_Tipo_Serie + ", ");
                Mi_sql.Append(Cat_Facturas_Series.Campo_Total_Timbres + ", ");
                Mi_sql.Append(Cat_Facturas_Series.Campo_Timbre_Inicial + ", ");
                Mi_sql.Append(Cat_Facturas_Series.Campo_Timbre_Final + ", ");
                Mi_sql.Append(Cat_Facturas_Series.Campo_Cancelaciones + ", ");
                Mi_sql.Append(Cat_Facturas_Series.Campo_Tipo_Default + ", ");
                Mi_sql.Append(Cat_Facturas_Series.Campo_Estatus);
                Mi_sql.Append(") VALUES (");
                Mi_sql.Append("'" + P_Series.P_Serie_Id + "', ");
                Mi_sql.Append("'" + P_Series.P_Serie + "', ");
                Mi_sql.Append("'" + P_Series.P_Descripcion + "', ");
                Mi_sql.Append("'" + P_Series.P_Tipo_Serie + "', ");
                Mi_sql.Append("" + P_Series.P_Total_Timbres + ", ");
                Mi_sql.Append("" + P_Series.P_Timbre_Inicial + ", ");
                Mi_sql.Append("" + P_Series.P_Timbre_Final + ", ");
                Mi_sql.Append("" + P_Series.P_Cancelaciones + ", ");
                Mi_sql.Append("'" + P_Series.P_Tipo_Default + "', ");
                Mi_sql.Append("'" + P_Series.P_Estatus);
                Mi_sql.Append(")");

                Obj_Comando.CommandText = Mi_sql.ToString();
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

                Alta = true;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Alta;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Serie
        ///DESCRIPCIÓN: Modifica en la Base de Datos una Serie
        ///PARAMENTROS:     
        ///             1. P_Series.        Instancia de la Clase de Negocio de Serie 
        ///                                 con los datos del que van a ser
        ///                                 modificados.
        ///CREO: Luis Alberto Salas García
        ///FECHA_CREO: 12 Abril 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static Boolean Modificar_Serie(Cls_Ope_Facturas_Series_Negocio P_Series)
        {
            Boolean Modificado = false;
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            StringBuilder Mi_sql = new StringBuilder();

            try
            {
                Mi_sql.Append("UPDATE " + Cat_Facturas_Series.Tabla_Cat_Facturas_Series + " SET ");
                if (P_Series.P_Serie_Id != null && P_Series.P_Serie_Id.Trim() != "")
                    Mi_sql.Append(Cat_Facturas_Series.Campo_Serie_Id + " = '" + P_Series.P_Serie_Id + "', ");
                if (P_Series.P_Serie != null && P_Series.P_Serie.Trim() != "")
                    Mi_sql.Append(Cat_Facturas_Series.Campo_Serie + " = '" + P_Series.P_Serie + "', ");
                if (P_Series.P_Descripcion != null && P_Series.P_Descripcion.Trim() != "")
                    Mi_sql.Append(Cat_Facturas_Series.Campo_Descripcion + " = '" + P_Series.P_Descripcion + "', ");
                if (P_Series.P_Tipo_Serie != null && P_Series.P_Tipo_Serie.Trim() != "")
                    Mi_sql.Append(Cat_Facturas_Series.Campo_Tipo_Serie + " = '" + P_Series.P_Tipo_Serie + "', ");
                if (P_Series.P_Total_Timbres != null && P_Series.P_Total_Timbres.Trim() != "")
                    Mi_sql.Append(Cat_Facturas_Series.Campo_Total_Timbres + " = '" + P_Series.P_Total_Timbres + "', ");
                if (P_Series.P_Timbre_Inicial != null && P_Series.P_Timbre_Inicial.Trim() != "")
                    Mi_sql.Append(Cat_Facturas_Series.Campo_Timbre_Inicial  + " = '" + P_Series.P_Timbre_Inicial + "', ");
                if (P_Series.P_Timbre_Final != null && P_Series.P_Timbre_Final.Trim() != "")
                    Mi_sql.Append(Cat_Facturas_Series.Campo_Timbre_Final  + " = '" + P_Series.P_Timbre_Final + "', ");
                if (P_Series.P_Cancelaciones != null && P_Series.P_Cancelaciones.Trim() != "")
                    Mi_sql.Append(Cat_Facturas_Series.Campo_Cancelaciones + " = '" + P_Series.P_Cancelaciones + "', ");
                if (P_Series.P_Tipo_Default != null && P_Series.P_Tipo_Default.Trim() != "")
                    Mi_sql.Append(Cat_Facturas_Series.Campo_Tipo_Default + " = '" + P_Series.P_Tipo_Default + "', ");
                if (P_Series.P_Estatus != null && P_Series.P_Estatus.Trim() != "")
                    Mi_sql.Append(Cat_Facturas_Series.Campo_Estatus + " = '" + P_Series.P_Estatus + "'");
                Mi_sql.Append(" WHERE " + Cat_Facturas_Series.Campo_Serie_Id + " = '" + P_Series.P_Serie_Id + "'");
                Obj_Comando.CommandText = Mi_sql.ToString();
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
                Modificado = true;

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Modificado;
        }
        public static Boolean Modificar_Serie(Cls_Ope_Facturas_Series_Negocio P_Series, SqlConnection objConexion)
        {
            Boolean Modificado = false;
            StringBuilder Mi_sql = new StringBuilder();

            try
            {
                Mi_sql.Append("UPDATE " + Cat_Facturas_Series.Tabla_Cat_Facturas_Series + " SET ");
                if (P_Series.P_Serie_Id != null && P_Series.P_Serie_Id.Trim() != "")
                    Mi_sql.Append(Cat_Facturas_Series.Campo_Serie_Id + " = '" + P_Series.P_Serie_Id + "', ");
                if (P_Series.P_Serie != null && P_Series.P_Serie.Trim() != "")
                    Mi_sql.Append(Cat_Facturas_Series.Campo_Serie + " = '" + P_Series.P_Serie + "', ");
                if (P_Series.P_Descripcion != null && P_Series.P_Descripcion.Trim() != "")
                    Mi_sql.Append(Cat_Facturas_Series.Campo_Descripcion + " = '" + P_Series.P_Descripcion + "', ");
                if (P_Series.P_Tipo_Serie != null && P_Series.P_Tipo_Serie.Trim() != "")
                    Mi_sql.Append(Cat_Facturas_Series.Campo_Tipo_Serie + " = '" + P_Series.P_Tipo_Serie + "', ");
                if (P_Series.P_Total_Timbres != null && P_Series.P_Total_Timbres.Trim() != "")
                    Mi_sql.Append(Cat_Facturas_Series.Campo_Total_Timbres + " = '" + P_Series.P_Total_Timbres + "', ");
                if (P_Series.P_Timbre_Inicial != null && P_Series.P_Timbre_Inicial.Trim() != "")
                    Mi_sql.Append(Cat_Facturas_Series.Campo_Timbre_Inicial + " = '" + P_Series.P_Timbre_Inicial + "', ");
                if (P_Series.P_Timbre_Final != null && P_Series.P_Timbre_Final.Trim() != "")
                    Mi_sql.Append(Cat_Facturas_Series.Campo_Timbre_Final + " = '" + P_Series.P_Timbre_Final + "', ");
                if (P_Series.P_Cancelaciones != null && P_Series.P_Cancelaciones.Trim() != "")
                    Mi_sql.Append(Cat_Facturas_Series.Campo_Cancelaciones + " = '" + P_Series.P_Cancelaciones + "', ");
                if (P_Series.P_Tipo_Default != null && P_Series.P_Tipo_Default.Trim() != "")
                    Mi_sql.Append(Cat_Facturas_Series.Campo_Tipo_Default + " = '" + P_Series.P_Tipo_Default + "', ");
                if (P_Series.P_Estatus != null && P_Series.P_Estatus.Trim() != "")
                    Mi_sql.Append(Cat_Facturas_Series.Campo_Estatus + " = '" + P_Series.P_Estatus + "'");
                Mi_sql.Append(" WHERE " + Cat_Facturas_Series.Campo_Serie_Id + " = '" + P_Series.P_Serie_Id + "'");
                using (SqlCommand Obj_Comando = objConexion.CreateCommand())
                {
                    Obj_Comando.CommandText = Mi_sql.ToString();
                    Obj_Comando.ExecuteNonQuery();
                }

                Modificado = true;

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Modificado;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Eliminar_Serie
        ///DESCRIPCIÓN: Modifica en la Base de Datos una Serie
        ///PARAMENTROS:     
        ///             1. P_Series.        Instancia de la Clase de Negocio de Series 
        ///                                 con los datos del que van a ser
        ///                                 modificados.
        ///CREO: Luis Alberto Salas García
        ///FECHA_CREO: 12 Abril 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static Boolean Eliminar_Serie(Cls_Ope_Facturas_Series_Negocio P_Series)
        {
            Boolean Eliminado = false;
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            StringBuilder Mi_sql = new StringBuilder();

            try
            {
                Mi_sql.Append("UPDATE " + Cat_Facturas_Series.Tabla_Cat_Facturas_Series + " SET ");
                Mi_sql.Append(Cat_Facturas_Series.Campo_Estatus + " = 'ELIMINADO', ");
                Mi_sql.Append(" WHERE " + Cat_Facturas_Series.Campo_Serie_Id + " = '" + P_Series.P_Serie_Id + "'");

                Obj_Comando.CommandText = Mi_sql.ToString();
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
                Eliminado = true;

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Eliminado;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Serie
        ///DESCRIPCIÓN: Consulta las Series
        ///PARAMENTROS:     
        ///             1. P_Series.        Instancia de la Clase de Negocio de Series 
        ///                                 con los datos que servirán de
        ///                                 filtro.
        ///CREO: Luis Alberto Salas García
        ///FECHA_CREO: 12 Abril 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Serie(Cls_Ope_Facturas_Series_Negocio P_Series)
        {
            DataTable Tabla = new DataTable();
            Boolean Entro_Where = false;

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            StringBuilder Mi_SQL = new StringBuilder();

            try
            {
                Mi_SQL.Append("SELECT * FROM " + Cat_Facturas_Series.Tabla_Cat_Facturas_Series);
                if (!String.IsNullOrEmpty(P_Series.P_Serie_Id))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Serie_Id + " = '" + P_Series.P_Serie_Id + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Serie))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Serie + " = '" + P_Series.P_Serie + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Descripcion))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Descripcion + " = '" + P_Series.P_Descripcion + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Tipo_Serie))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Tipo_Serie + " = '" + P_Series.P_Tipo_Serie + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Tipo_Default))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Tipo_Default + " = '" + P_Series.P_Tipo_Default + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Total_Timbres))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Total_Timbres + " = '" + P_Series.P_Total_Timbres + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Timbre_Inicial ))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Timbre_Inicial  + " = '" + P_Series.P_Timbre_Inicial + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Timbre_Final ))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Timbre_Final + " = '" + P_Series.P_Timbre_Final + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Estatus))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Estatus + " = '" + P_Series.P_Estatus + "'");
                }
                if (Entro_Where)
                {
                    Mi_SQL.Append(" AND " + Cat_Facturas_Series.Campo_Estatus + " != 'ELIMINADO'"); 
                }
                else
                {
                    Mi_SQL.Append(" WHERE " + Cat_Facturas_Series.Campo_Estatus + " != 'ELIMINADO'"); 
                }

                // agregar filtro y orden a la consulta
                Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }
        public static DataTable Consultar_Serie(Cls_Ope_Facturas_Series_Negocio P_Series, SqlConnection objConexion)
        {
            DataTable Tabla = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();
            Boolean Entro_Where = false;
            StringBuilder Mi_SQL = new StringBuilder();

            try
            {
                Mi_SQL.Append("SELECT * FROM " + Cat_Facturas_Series.Tabla_Cat_Facturas_Series);
                if (!String.IsNullOrEmpty(P_Series.P_Serie_Id))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Serie_Id + " = '" + P_Series.P_Serie_Id + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Serie))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Serie + " = '" + P_Series.P_Serie + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Descripcion))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Descripcion + " = '" + P_Series.P_Descripcion + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Tipo_Serie))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Tipo_Serie + " = '" + P_Series.P_Tipo_Serie + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Tipo_Default))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Tipo_Default + " = '" + P_Series.P_Tipo_Default + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Total_Timbres))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Total_Timbres + " = '" + P_Series.P_Total_Timbres + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Timbre_Inicial))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Timbre_Inicial + " = '" + P_Series.P_Timbre_Inicial + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Timbre_Final))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Timbre_Final + " = '" + P_Series.P_Timbre_Final + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Estatus))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Estatus + " = '" + P_Series.P_Estatus + "'");
                }
                if (Entro_Where)
                {
                    Mi_SQL.Append(" AND " + Cat_Facturas_Series.Campo_Estatus + " != 'ELIMINADO'");
                }
                else
                {
                    Mi_SQL.Append(" WHERE " + Cat_Facturas_Series.Campo_Estatus + " != 'ELIMINADO'");
                }
                using (SqlCommand Obj_Comando = objConexion.CreateCommand())
                {
                    Obj_Comando.CommandText = Mi_SQL.ToString();
                    adapter.SelectCommand = Obj_Comando;
                    adapter.Fill(Tabla);
                }


            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Serie_Activa_Pendiente
        ///DESCRIPCIÓN: Consulta la serie, tomando en cuenta que este activa o pendiente.
        ///PARAMENTROS:     
        ///             1. P_Series.        Instancia de la Clase de Negocio de Series 
        ///                                 con los datos que servirán de
        ///                                 filtro.
        ///CREO: Luis Alberto Salas García
        ///FECHA_CREO: 12 Abril 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Serie_Activa_Pendiente(Cls_Ope_Facturas_Series_Negocio P_Series)
        {
            DataTable Tabla = new DataTable();
            StringBuilder Mi_SQL = new StringBuilder();
            Boolean Entro_Where = false;
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;

            try
            {
                Mi_SQL.Append("SELECT * FROM " + Cat_Facturas_Series.Tabla_Cat_Facturas_Series);
                if (!String.IsNullOrEmpty(P_Series.P_Serie_Id))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Serie_Id + " = '" + P_Series.P_Serie_Id + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Serie))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Serie + " = '" + P_Series.P_Serie + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Descripcion))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Descripcion + " = '" + P_Series.P_Descripcion + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Tipo_Serie))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Tipo_Serie + " = '" + P_Series.P_Tipo_Serie + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Tipo_Default))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Tipo_Default + " = '" + P_Series.P_Tipo_Default + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Total_Timbres))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Total_Timbres + " = '" + P_Series.P_Total_Timbres + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Timbre_Inicial))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Timbre_Inicial + " = '" + P_Series.P_Timbre_Inicial + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Timbre_Final))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Timbre_Final + " = '" + P_Series.P_Timbre_Final + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Estatus))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Estatus + " = '" + P_Series.P_Estatus + "'");
                }
                Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                Mi_SQL.Append(" (" + Cat_Facturas_Series.Campo_Estatus + " != 'ELIMINADO'");
                Mi_SQL.Append(" AND " + Cat_Facturas_Series.Campo_Estatus + " != 'CANCELADO'");
                Mi_SQL.Append(" AND " + Cat_Facturas_Series.Campo_Estatus + " != 'TERMINADO')");

                // agregar filtro y orden a la consulta
                Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }
        public static DataTable Consultar_Serie_Activa_Pendiente(Cls_Ope_Facturas_Series_Negocio P_Series, SqlConnection objConexion)
        {
            DataTable Tabla = new DataTable();
            StringBuilder Mi_SQL = new StringBuilder();
            Boolean Entro_Where = false;
            SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {
                Mi_SQL.Append("SELECT * FROM " + Cat_Facturas_Series.Tabla_Cat_Facturas_Series);
                if (!String.IsNullOrEmpty(P_Series.P_Serie_Id))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Serie_Id + " = '" + P_Series.P_Serie_Id + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Serie))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Serie + " = '" + P_Series.P_Serie + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Descripcion))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Descripcion + " = '" + P_Series.P_Descripcion + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Tipo_Serie))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Tipo_Serie + " = '" + P_Series.P_Tipo_Serie + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Tipo_Default))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Tipo_Default + " = '" + P_Series.P_Tipo_Default + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Total_Timbres))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Total_Timbres + " = '" + P_Series.P_Total_Timbres + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Timbre_Inicial))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Timbre_Inicial + " = '" + P_Series.P_Timbre_Inicial + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Timbre_Final))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Timbre_Final + " = '" + P_Series.P_Timbre_Final + "'");
                }
                if (!String.IsNullOrEmpty(P_Series.P_Estatus))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Facturas_Series.Campo_Estatus + " = '" + P_Series.P_Estatus + "'");
                }
                Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                Mi_SQL.Append(" (" + Cat_Facturas_Series.Campo_Estatus + " != 'ELIMINADO'");
                Mi_SQL.Append(" AND " + Cat_Facturas_Series.Campo_Estatus + " != 'CANCELADO'");
                Mi_SQL.Append(" AND " + Cat_Facturas_Series.Campo_Estatus + " != 'TERMINADO')");

                using (SqlCommand Obj_Comando = objConexion.CreateCommand())
                {
                    Obj_Comando.CommandText = Mi_SQL.ToString();
                    adapter.SelectCommand = Obj_Comando;
                    adapter.Fill(Tabla);
                }
            }   
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }
    }
}