﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using SIAC.Constantes;
using SIAC_Ope_Facturas_Detalles.Negocio;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using SIAC.Metodos_Generales;

namespace SIAC_Ope_Facturas_Detalles.Datos
{
    public class Cls_Ope_Facturas_Detalles_Datos
    {

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Factura
        ///DESCRIPCIÓN: Da de alta en la Base de Datos una nueva Factura
        ///PARAMENTROS:     
        ///             1. P_Detalles_Facturas.      Instancia de la Clase de Negocio de Facturas 
        ///                                 con los datos del que van a ser
        ///                                 dados de Alta.
        ///CREO: Miguel Angel Alvarado Enriquez
        ///FECHA_CREO: 26/Diciembre/2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static Boolean Alta_Detalle_Factura(Cls_Ope_Facturas_Detalles_Negocio P_Detalles_Facturas, ref SqlCommand Obj_Comando)
        {
            Boolean Alta = false;
            StringBuilder Mi_sql = new StringBuilder(); ;
            
            try
            {
                Mi_sql.Append("INSERT INTO " + Ope_Facturas_Detalle.Tabla_Ope_Facturas_Detalle + "(");
                Mi_sql.Append(Ope_Facturas_Detalle.Campo_No_Factura + ", ");
           
                Mi_sql.Append(Ope_Facturas_Detalle.Campo_Serie+ ", ");
                Mi_sql.Append(Ope_Facturas_Detalle.Campo_Cantidad + ", ");
                if (!String.IsNullOrEmpty(P_Detalles_Facturas.P_Codigo)) Mi_sql.Append(Ope_Facturas_Detalle.Campo_Codigo + ", ");
                Mi_sql.Append(Ope_Facturas_Detalle.Campo_Descripcion + ", ");
                Mi_sql.Append(Ope_Facturas_Detalle.Campo_Unidad + ", ");
                Mi_sql.Append(Ope_Facturas_Detalle.Campo_Subtotal + ", ");
                Mi_sql.Append(Ope_Facturas_Detalle.Campo_Precio_Unitario + ", ");
                Mi_sql.Append(Ope_Facturas_Detalle.Campo_Iva + ", ");
                Mi_sql.Append(Ope_Facturas_Detalle.Campo_Porcentaje_Impuesto + ", ");
                //Mi_sql.Append(Ope_Facturas_Detalle.Campo_No_Recibo + ", ");
                Mi_sql.Append(Ope_Facturas_Detalle.Campo_Total);
                Mi_sql.Append(") VALUES (");
                Mi_sql.Append("'" + P_Detalles_Facturas.P_No_Factura + "', ");
                Mi_sql.Append("'" + P_Detalles_Facturas.P_Serie + "', ");
                Mi_sql.Append(P_Detalles_Facturas.P_Cantidad + ", ");
                if (!String.IsNullOrEmpty(P_Detalles_Facturas.P_Codigo)) Mi_sql.Append("'" + P_Detalles_Facturas.P_Codigo + "', ");
                Mi_sql.Append("'" + P_Detalles_Facturas.P_Descripcion + "', ");
                Mi_sql.Append("'" + P_Detalles_Facturas.P_Unidad + "', ");
                Mi_sql.Append(P_Detalles_Facturas.P_Subtotal + ", ");
                Mi_sql.Append(P_Detalles_Facturas.P_Precio + ", ");
                Mi_sql.Append(P_Detalles_Facturas.P_Iva + ", ");
                Mi_sql.Append(P_Detalles_Facturas.P_Porcentaje + ", ");
                //Mi_sql.Append(P_Detalles_Facturas.P_No_Recibo + ", ");
                Mi_sql.Append(P_Detalles_Facturas.P_Total);
                Mi_sql.Append(")");

                Obj_Comando.CommandText = Mi_sql.ToString();
                Obj_Comando.ExecuteNonQuery();

                Alta = true;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Alta;
        }

        public static Boolean Alta_Detalle_Factura(Cls_Ope_Facturas_Detalles_Negocio P_Detalles_Facturas, SqlConnection objConexion)
        {
            Boolean Alta = false;
            StringBuilder Mi_sql = new StringBuilder(); ;

            try
            {
                Mi_sql.Append("INSERT INTO " + Ope_Facturas_Detalle.Tabla_Ope_Facturas_Detalle + "(");
                Mi_sql.Append(Ope_Facturas_Detalle.Campo_No_Factura + ", ");

                Mi_sql.Append(Ope_Facturas_Detalle.Campo_Serie + ", ");
                Mi_sql.Append(Ope_Facturas_Detalle.Campo_Cantidad + ", ");
                if (!String.IsNullOrEmpty(P_Detalles_Facturas.P_Codigo)) Mi_sql.Append(Ope_Facturas_Detalle.Campo_Codigo + ", ");
                Mi_sql.Append(Ope_Facturas_Detalle.Campo_Descripcion + ", ");
                Mi_sql.Append(Ope_Facturas_Detalle.Campo_Unidad + ", ");
                Mi_sql.Append(Ope_Facturas_Detalle.Campo_Subtotal + ", ");
                Mi_sql.Append(Ope_Facturas_Detalle.Campo_Precio_Unitario + ", ");
                Mi_sql.Append(Ope_Facturas_Detalle.Campo_Iva + ", ");
                Mi_sql.Append(Ope_Facturas_Detalle.Campo_Porcentaje_Impuesto + ", ");
                //Mi_sql.Append(Ope_Facturas_Detalle.Campo_No_Recibo + ", ");
                Mi_sql.Append(Ope_Facturas_Detalle.Campo_Total);
                Mi_sql.Append(") VALUES (");
                Mi_sql.Append("'" + P_Detalles_Facturas.P_No_Factura + "', ");
                Mi_sql.Append("'" + P_Detalles_Facturas.P_Serie + "', ");
                Mi_sql.Append(P_Detalles_Facturas.P_Cantidad + ", ");
                if (!String.IsNullOrEmpty(P_Detalles_Facturas.P_Codigo)) Mi_sql.Append("'" + P_Detalles_Facturas.P_Codigo + "', ");
                Mi_sql.Append("'" + P_Detalles_Facturas.P_Descripcion + "', ");
                Mi_sql.Append("'" + P_Detalles_Facturas.P_Unidad + "', ");
                Mi_sql.Append(P_Detalles_Facturas.P_Subtotal + ", ");
                Mi_sql.Append(P_Detalles_Facturas.P_Precio + ", ");
                Mi_sql.Append(P_Detalles_Facturas.P_Iva + ", ");
                Mi_sql.Append(P_Detalles_Facturas.P_Porcentaje + ", ");
                //Mi_sql.Append(P_Detalles_Facturas.P_No_Recibo + ", ");
                Mi_sql.Append(P_Detalles_Facturas.P_Total);
                Mi_sql.Append(")");
                using (SqlCommand Obj_Comando = objConexion.CreateCommand())
                {
                    Obj_Comando.CommandText = Mi_sql.ToString();
                    Obj_Comando.ExecuteNonQuery();
                }

                Alta = true;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Alta;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Factura
        ///DESCRIPCIÓN: Consulta las facturas
        ///PARAMENTROS:     
        ///             1. P_Detalles_Facturas.      Instancia de la Clase de Negocio de Facturas 
        ///                                 con los datos que servirán de
        ///                                 filtro.
        ///CREO: Miguel Angel Alvarado Enriquez
        ///FECHA_CREO: 26/Diciembre/2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Detalle_Factura(Cls_Ope_Facturas_Detalles_Negocio P_Detalles_Facturas)
        {
            DataTable Tabla = new DataTable();
            StringBuilder Mi_SQL = new StringBuilder();
            Boolean Entro_Where = false;

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;

            try
            {
                Mi_SQL.Append("SELECT * FROM " + Ope_Facturas_Detalle.Tabla_Ope_Facturas_Detalle);
                if (!String.IsNullOrEmpty(P_Detalles_Facturas.P_No_Factura) && !String.IsNullOrEmpty(P_Detalles_Facturas.P_Serie))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Facturas_Detalle.Campo_No_Factura + " = '" + P_Detalles_Facturas.P_No_Factura + "' AND ");
                    //Mi_SQL.Append(Ope_Facturas_Detalle.Campo_Serie + " = '" + P_Detalles_Facturas.P_Serie + "'");
                    Mi_SQL.Append(Ope_Facturas_Detalle.Campo_Serie + " = '" + P_Detalles_Facturas.P_Serie + "' AND CODIGO <> '00159'");
                }
                Mi_SQL.Append(" ORDER BY " + Ope_Facturas_Detalle.Campo_No_Factura + ", " + Ope_Facturas_Detalle.Campo_Serie);

                // agregar filtro y orden a la consulta
                Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }
    }
}