﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SIAC.Constantes;
using SIAC.Registro_Usuarios.Negocio;
using SIAC.Metodos_Generales;
using System.Data;
using SharpContent.ApplicationBlocks.Data;

/// <summary>
/// Descripción breve de Cls_Cat_Cliente_Temporal_Datos
/// </summary>
/// 
namespace SIAC.Registro_Usuarios.Datos
{
    public class Cls_Cat_Registro_Usuarios_Datos
    {
        #region Metodos
        /// ***********************************************************************************
        /// Nombre de la Función: Alta_Registro_Usuario
        /// Descripción         : Da de alta en la Base de Datos para la validacion de correo
        /// Parámetros          : 
        /// Usuario Creo        : Jose Maldonado Mendez.
        /// Fecha Creó          : 03/Diciembre/2015
        /// Usuario Modifico    :
        /// Fecha Modifico      :
        /// ***********************************************************************************
        public static Boolean Alta_Registro_Usuario(Cls_Cat_Registro_Usuarios_Negocio P_Parametros)
        {
            Boolean Alta = false;

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            String Mi_sql = "";
            String Parametro_ID = "";

            try
            {
                Parametro_ID = Cls_Metodos_Generales.Obtener_ID_Consecutivo(Cat_Registro_Usuarios.Tabla_Cat_Registro_Usuarios, Cat_Registro_Usuarios.Campo_Registro_ID, "", 10);
                P_Parametros.P_Registro_Id = Parametro_ID;

                Mi_sql = "INSERT INTO " + Cat_Registro_Usuarios.Tabla_Cat_Registro_Usuarios + "(";
                Mi_sql += Cat_Registro_Usuarios.Campo_Registro_ID + ", ";
                Mi_sql += Cat_Registro_Usuarios.Campo_Email + ", ";
                Mi_sql += Cat_Registro_Usuarios.Campo_No_Cuenta + ", ";
                Mi_sql += Cat_Registro_Usuarios.Campo_RPU + ", ";
                Mi_sql += Cat_Registro_Usuarios.Campo_Fecha_Emision + ", ";
                Mi_sql += Cat_Registro_Usuarios.Campo_Predio_ID + ", ";
                Mi_sql += Cat_Registro_Usuarios.Campo_Total_Pagar + ", ";
                Mi_sql += Cat_Registro_Usuarios.Campo_Estatus + ", ";
                Mi_sql += Cat_Registro_Usuarios.Campo_Fecha_Creo;
                Mi_sql += ") VALUES (";
                Mi_sql += "'" + P_Parametros.P_Registro_Id + "', ";
                Mi_sql += "'" + P_Parametros.P_Email + "', ";
                Mi_sql += "'" + P_Parametros.P_No_Cuenta + "', ";
                Mi_sql += "'" + P_Parametros.P_Rpu + "', ";
                Mi_sql += "'" + P_Parametros.P_Fecha_Emision.ToString("yyyyMMdd") + "', ";
                Mi_sql += "'" + P_Parametros.P_Predio_Id + "', ";
                Mi_sql += P_Parametros.P_Total_Pagar + ", ";
                Mi_sql += "'" + P_Parametros.P_Estatus + "', ";
                Mi_sql += "GETDATE())";
                Obj_Comando.CommandText = Mi_sql.ToString();
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

                Alta = true;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Alta;
        }

        ///**************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Registro_Usuarios
        ///DESCRIPCIÓN         : Consulta el registro de los usuarios
        ///PARAMENTROS         : P_Parametros.- Instancia de la Clase de Negocio de Registro_Usuarios con los datos que servirán de
        ///                                   filtro.
        ///CREO                : José Maldonado Méndez
        ///FECHA_CREO          : 30/Noviembre/2015
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        public static DataTable Consultar_Registro_Usuarios(Cls_Cat_Registro_Usuarios_Negocio P_Parametros)
        {
            DataTable Tabla = new DataTable();
            Boolean Segundo_Filtro = false;

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            String Mi_SQL = "";

            try
            {
                Mi_SQL += "SELECT * FROM  " + Cat_Registro_Usuarios.Tabla_Cat_Registro_Usuarios;
                if (!String.IsNullOrEmpty(P_Parametros.P_Registro_Id))
                {
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE ";
                    Mi_SQL += Cat_Registro_Usuarios.Campo_Registro_ID + " = '" + P_Parametros.P_Registro_Id + "'";
                    Segundo_Filtro = true;
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Email))
                {
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE ";
                    Mi_SQL += Cat_Registro_Usuarios.Campo_Email + " = '" + P_Parametros.P_Email + "'";
                    Segundo_Filtro = true;
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Rpu))
                {
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE ";
                    Mi_SQL += Cat_Registro_Usuarios.Campo_RPU + " = '" + P_Parametros.P_Rpu + "'";
                    Segundo_Filtro = true;
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Estatus))
                {
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE ";
                    Mi_SQL += Cat_Registro_Usuarios.Campo_Estatus + " = '" + P_Parametros.P_Estatus + "'";
                    Segundo_Filtro = true;
                }

                // agregar filtro y orden a la consulta
                Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }

        ///**************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Registro_Usuarios
        ///DESCRIPCIÓN         : Modifica el registro de los clientes
        ///PARAMENTROS         : P_Parametros.- Instancia de la Clase de Negocio de Clientes con los datos que se modificaran.
        ///CREO                : José Maldonado Méndez
        ///FECHA_CREO          : 02/Diciembre/2015
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        public static Boolean Modificar_Registro_Usuarios(Cls_Cat_Registro_Usuarios_Negocio P_Parametros)
        {
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            Boolean Modificar = false;
            String Mi_Sql = "";
            try
            {
                Mi_Sql = "UPDATE " + Cat_Registro_Usuarios.Tabla_Cat_Registro_Usuarios + " SET ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Email))
                    Mi_Sql += Cat_Registro_Usuarios.Campo_Email + " = '" + P_Parametros.P_Email + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_No_Cuenta))
                    Mi_Sql += Cat_Registro_Usuarios.Campo_No_Cuenta + " = '" + P_Parametros.P_No_Cuenta + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Rpu))
                    Mi_Sql += Cat_Registro_Usuarios.Campo_RPU + " = '" + P_Parametros.P_Rpu + "', ";
                if (P_Parametros.P_Fecha_Emision != DateTime.MinValue)
                    Mi_Sql += Cat_Registro_Usuarios.Campo_Fecha_Emision + " = '" + P_Parametros.P_Fecha_Emision.ToString("yyyyMMdd HH:mm:ss") + "', ";
                if (!String.IsNullOrEmpty(P_Parametros.P_Total_Pagar))
                    Mi_Sql += Cat_Registro_Usuarios.Campo_Total_Pagar + " = " + P_Parametros.P_Total_Pagar + "'";
                if (!String.IsNullOrEmpty(P_Parametros.P_Estatus))
                    Mi_Sql += Cat_Registro_Usuarios.Campo_Estatus + " = '" + P_Parametros.P_Estatus + "', ";

                //Mi_Sql += Cat_Registro_Usuarios.Campo_Usuario_Modifico + " = '" + "'";

                Mi_Sql += Cat_Registro_Usuarios.Campo_Fecha_Modifico + " = GETDATE()";

                Mi_Sql += " WHERE " + Cat_Registro_Usuarios.Campo_Registro_ID + " = '" + P_Parametros.P_Registro_Id + "'";
                Obj_Comando.CommandText = Mi_Sql;
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

                Modificar = true;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Modificar;
        }
  
        public static void Eliminar_Registro(Cls_Cat_Registro_Usuarios_Negocio P_Parametros)
        {
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
          
            String Mi_Sql = "";
            try
            {
                Mi_Sql = "DELETE " + Cat_Registro_Usuarios.Tabla_Cat_Registro_Usuarios;
                Mi_Sql += " WHERE " + Cat_Registro_Usuarios.Campo_Registro_ID + " = '" + P_Parametros.P_Registro_Id + "'";

                Obj_Comando.CommandText = Mi_Sql;
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            

        }
        
        #endregion

    }
}