﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using SIAC.Constantes;
using System.Data.SqlClient;
using SIAC.Cat_Metodos_Pago.Negocio;
using System.Collections.Generic;
using SharpContent.ApplicationBlocks.Data;
using SIAC.Metodos_Generales;

namespace SIAC.Cat_Metodos_Pago.Datos
{
    public class Cls_Cat_Metodos_Pago_Datos
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Serie
        ///DESCRIPCIÓN: Da de alta en la Base de Datos una nueva Serie
        ///PARAMENTROS:     
        ///             1. P_Series.        Instancia de la Clase de Negocio de Series 
        ///                                 con los datos del que van a ser
        ///                                 dados de Alta.
        ///CREO: Miguel Angel Alvarado Enriquez
        ///FECHA_CREO: 26/Diciembre/2013
        ///MODIFICO:       
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Serie        /////*******************************************************************************
        //public static Boolean Alta_Metodo_Pago(Cls_Cat_Metodos_Pago_Negocio P_Metodos_Pago)
        //{
        //    Boolean Alta = false;

        //    SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
        //    Obj_Conexion.Open();
        //    SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
        //    SqlCommand Obj_Comando = new SqlCommand();
        //    Obj_Comando.Transaction = Obj_Transaccion;
        //    Obj_Comando.Connection = Obj_Conexion;
        //    String Mi_sql = "";
        //    try
        //    {
        //        Mi_sql = "INSERT INTO " + Cat_Cor_Metodos_Pago.Tabla_Cat_Metodos_Pago + "(";
        //        Mi_sql += Cat_Cor_Metodos_Pago.Campo_Metodo_Pago_Id + ", ";
        //        Mi_sql += Cat_Cor_Metodos_Pago.Campo_Nombre + ", ";
        //        Mi_sql += Cat_Cor_Metodos_Pago.Campo_Clave + ", ";
        //        Mi_sql += Cat_Cor_Metodos_Pago.Campo_Estatus+", ";
        //        Mi_sql += Cat_Cor_Metodos_Pago.Campo_Usuario_Creo+", ";
        //        Mi_sql += Cat_Cor_Metodos_Pago.Campo_Fecha_Creo;
        //        Mi_sql += ") VALUES (";
        //        Mi_sql +="'" + P_Metodos_Pago.P_Metodo_Pago_Id + "', ";
        //        Mi_sql += "'" + P_Metodos_Pago.P_Nombre + "', ";
        //        Mi_sql += "'" + P_Metodos_Pago.P_Clave + "', ";
        //        Mi_sql += "'" + P_Metodos_Pago.P_Estatus + "', ";
        //        Mi_sql += "'" + SIAC.Sessiones.Cls_Sessiones.Nombre_Empleado + "', ";
        //        Mi_sql += "GETDATE()";
        //        Mi_sql += ")";

        //        Obj_Comando.CommandText = Mi_sql.ToString();
        //        Obj_Comando.ExecuteNonQuery();
        //        Obj_Transaccion.Commit();
        //        Obj_Conexion.Close();

        //        Alta = true;
        //    }
        //    catch (SqlException Ex)
        //    {
        //        throw new Exception("Error: " + Ex.Message);
        //    }
        //    catch (DBConcurrencyException Ex)
        //    {
        //        throw new Exception("Error: " + Ex.Message);
        //    }
        //    catch (Exception Ex)
        //    {
        //        throw new Exception("Error: " + Ex.Message);
        //    }
        //    return Alta;
        //}
        ///DESCRIPCIÓN: Modifica en la Base de Datos una Serie
        ///PARAMENTROS:     
        ///             1. P_Series.        Instancia de la Clase de Negocio de Serie 
        ///                                 con los datos del que van a ser
        ///                                 modificados.
        ///CREO: Luis Alberto Salas García
        ///FECHA_CREO: 12 Abril 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        //public static Boolean Modificar_Metodos_Pago(Cls_Cat_Metodos_Pago_Negocio P_Metodo_Pago)
        //{
        //    Boolean Modificado = false;
        //    SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
        //    Obj_Conexion.Open();
        //    SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
        //    SqlCommand Obj_Comando = new SqlCommand();
        //    Obj_Comando.Transaction = Obj_Transaccion;
        //    Obj_Comando.Connection = Obj_Conexion;
        //    String Mi_sql = "";

        //    try
        //    {
        //        Mi_sql = "UPDATE " + Cat_Cor_Metodos_Pago.Tabla_Cat_Metodos_Pago + " SET ";

        //        if (P_Metodo_Pago.P_Clave != null && P_Metodo_Pago.P_Clave.Trim() != "")
        //            Mi_sql += Cat_Cor_Metodos_Pago.Campo_Clave + " = '" + P_Metodo_Pago.P_Clave + "', ";
        //        if (P_Metodo_Pago.P_Nombre != null && P_Metodo_Pago.P_Nombre.Trim() != "")
        //            Mi_sql += Cat_Cor_Metodos_Pago.Campo_Nombre + " = '" + P_Metodo_Pago.P_Nombre + "', ";
        //        if (P_Metodo_Pago.P_Estatus != null && P_Metodo_Pago.P_Estatus.Trim() != "")
        //        { 
        //            Mi_sql += Cat_Cor_Metodos_Pago.Campo_Estatus + " = '" + P_Metodo_Pago.P_Estatus + "', ";
        //        }
        //            Mi_sql += Cat_Cor_Metodos_Pago.Campo_Usuario_Modifico + " = '" + SIAC.Sessiones.Cls_Sessiones.Nombre_Empleado + "', ";
        //            Mi_sql += Cat_Cor_Metodos_Pago.Campo_Fecha_Modifico + " = GETDATE() ";
                
        //        Mi_sql += "WHERE " + Cat_Cor_Metodos_Pago.Campo_Metodo_Pago_Id+ " = '" + P_Metodo_Pago.P_Metodo_Pago_Id + "'";

        //        Obj_Comando.CommandText = Mi_sql; 
        //        Obj_Comando.ExecuteNonQuery();
        //        Obj_Transaccion.Commit();
        //        Obj_Conexion.Close();
        //        Modificado = true;
        //    }
        //    catch (SqlException Ex)
        //    {
        //        throw new Exception("Error: " + Ex.Message);
        //    }
        //    catch (DBConcurrencyException Ex)
        //    {
        //        throw new Exception("Error: " + Ex.Message);
        //    }
        //    catch (Exception Ex)
        //    {
        //        throw new Exception("Error: " + Ex.Message);
        //    }
        //    return Modificado;
        //}

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Eliminar_Serie
        ///DESCRIPCIÓN: Modifica en la Base de Datos una Serie
        ///PARAMENTROS:     
        ///             1. P_Series.        Instancia de la Clase de Negocio de Series 
        ///                                 con los datos del que van a ser
        ///                                 modificados.
        ///CREO: Luis Alberto Salas García
        ///FECHA_CREO: 12 Abril 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        //public static Boolean Eliminar_Metodo_Pago(Cls_Cat_Metodos_Pago_Negocio P_Metodos)
        //{
        //    Boolean Eliminado = false;
        //    SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
        //    Obj_Conexion.Open();
        //    SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
        //    SqlCommand Obj_Comando = new SqlCommand();
        //    Obj_Comando.Transaction = Obj_Transaccion;
        //    Obj_Comando.Connection = Obj_Conexion;
        //    StringBuilder Mi_sql = new StringBuilder();

        //    try
        //    {
        //        Mi_sql.Append("UPDATE " + Cat_Cor_Metodos_Pago.Tabla_Cat_Metodos_Pago + " SET ");
        //        Mi_sql.Append(Cat_Cor_Metodos_Pago.Campo_Estatus + " = 'ELIMINADO' ");
        //        Mi_sql.Append(" WHERE " + Cat_Cor_Metodos_Pago.Campo_Metodo_Pago_Id + " = '" + P_Metodos.P_Metodo_Pago_Id + "'");

        //        Obj_Comando.CommandText = Mi_sql.ToString();
        //        Obj_Comando.ExecuteNonQuery();
        //        Obj_Transaccion.Commit();
        //        Obj_Conexion.Close();
        //        Eliminado = true;

        //    }
        //    catch (SqlException Ex)
        //    {
        //        throw new Exception("Error: " + Ex.Message);
        //    }
        //    catch (DBConcurrencyException Ex)
        //    {
        //        throw new Exception("Error: " + Ex.Message);
        //    }
        //    catch (Exception Ex)
        //    {
        //        throw new Exception("Error: " + Ex.Message);
        //    }
        //    return Eliminado;
        //}


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Metodos_Pago
        ///DESCRIPCIÓN: Consulta los métodos de pago
        ///PARAMENTROS:     
        ///             1. P_Metodos.        Instancia de la Clase de Negocio de Métodos de pago
        ///                                 con los datos que servirán de
        ///                                 filtro.
        ///CREO: Ana Laura Huichapa Ramírez
        ///FECHA_CREO: 12 Abril 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Metodos_Pago(Cls_Cat_Metodos_Pago_Negocio P_Metodos)
        {
            DataTable Tabla = new DataTable();
            Boolean Entro_Where = false;

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            String Mi_SQL = "";

            try
            {
               
                if (P_Metodos.P_Tipo_Default.Equals("Consultar_Por_Like_Concepto"))
                {
                    //Mi_SQL = "SELECT * FROM Cat_Cor_Metodos_Pago WHERE Nombre LIKE '%" + P_Metodos.P_Nombre + "%'"; ;
                    Mi_SQL = "select * from CAT_FE_FORMAS_PAGO WHERE DESCRIPCION LIKE '%" + P_Metodos.P_Nombre + "%'";
                    //Mi_SQL += " AND Estatus = 'ACTIVO'";
                }
                // agregar filtro y orden a la consulta
                Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }
        public static DataTable Consultar_Metodos_Pago(Cls_Cat_Metodos_Pago_Negocio P_Metodos, SqlConnection objConexion)
        {
            DataTable Tabla = new DataTable();
            Boolean Entro_Where = false;
            String Mi_SQL = "";
            SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {

                if (P_Metodos.P_Tipo_Default.Equals("Consultar_Por_Like_Concepto"))
                {
                    //Mi_SQL = "SELECT * FROM Cat_Cor_Metodos_Pago WHERE Nombre LIKE '%" + P_Metodos.P_Nombre + "%'"; ;
                    Mi_SQL = "select * from CAT_FE_FORMAS_PAGO WHERE DESCRIPCION LIKE '%" + P_Metodos.P_Nombre + "%'";
                    //Mi_SQL += " AND Estatus = 'ACTIVO'";
                    using (SqlCommand Obj_Comando = objConexion.CreateCommand())
                    {
                        Obj_Comando.CommandText = Mi_SQL;
                        adapter.SelectCommand = Obj_Comando;
                        adapter.Fill(Tabla);
                    }
                }
                


            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }
        public static DataTable Consultar_Metodos_Pago_Tarjetas(Cls_Cat_Metodos_Pago_Negocio P_Metodos)
        {
            DataTable Tabla = new DataTable();

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            String Mi_SQL = "";

            try
            {

                if (P_Metodos.P_Tipo_Default.Equals("Consultar_Por_Like_Concepto"))
                {
                    //Mi_SQL = "SELECT * FROM Cat_Cor_Metodos_Pago WHERE Nombre LIKE '%" + P_Metodos.P_Nombre + "%'"; ;
                    Mi_SQL = @"select *
                            from CAT_FE_FORMAS_PAGO
                            WHERE DESCRIPCION LIKE '%' + 
                            (
                                select top 1 ta.Tipo
	                            from Ope_Cor_Caj_Recibos_Cobros as rc
		                            inner join ope_cor_caj_tarjetas as ta
			                            on rc.NO_RECIBO = ta.no_recibo
	                            WHERE rc.NO_RECIBO = '{0}'
	                            ORDER by ta.tarjeta_id desc
                            ) + '%'";
                    //Mi_SQL += " AND Estatus = 'ACTIVO'";
                }
                Mi_SQL = string.Format(Mi_SQL, P_Metodos.P_No_Recibo);
                // agregar filtro y orden a la consulta
                Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }
        public static DataTable Consultar_Metodos_Pago_Tarjetas(Cls_Cat_Metodos_Pago_Negocio P_Metodos,SqlConnection objConexion)
        {
            DataTable Tabla = new DataTable();           
            String Mi_SQL = "";
            SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {

                if (P_Metodos.P_Tipo_Default.Equals("Consultar_Por_Like_Concepto"))
                {
                    //Mi_SQL = "SELECT * FROM Cat_Cor_Metodos_Pago WHERE Nombre LIKE '%" + P_Metodos.P_Nombre + "%'"; ;
                    Mi_SQL = @"select *
                            from CAT_FE_FORMAS_PAGO
                            WHERE DESCRIPCION LIKE '%' + 
                            (
                                select top 1 ta.Tipo
	                            from Ope_Cor_Caj_Recibos_Cobros as rc
		                            inner join ope_cor_caj_tarjetas as ta
			                            on rc.NO_RECIBO = ta.no_recibo
	                            WHERE rc.NO_RECIBO = '{0}'
	                            ORDER by ta.tarjeta_id desc
                            ) + '%'";
                    //Mi_SQL += " AND Estatus = 'ACTIVO'";
                }
                Mi_SQL = string.Format(Mi_SQL, P_Metodos.P_No_Recibo);
                using(SqlCommand Obj_Comando = objConexion.CreateCommand())
                {
                    Obj_Comando.CommandText = Mi_SQL.ToString();
                    adapter.SelectCommand = Obj_Comando;
                    adapter.Fill(Tabla);
                }

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }

    }
}