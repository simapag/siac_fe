﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using SIAC.Constantes;
using System.Data;
using SharpContent.ApplicationBlocks.Data;

namespace SIAC.Facturacion.Datos
{
    public class Cls_Ope_Cor_Consulta_Usuario
    {
        public Cls_Ope_Cor_Consulta_Usuario()
        {
            //
            // TODO: Agregar aquí la lógica del constructor
            //
        }

        /// <summary>
        /// Obteners the saldo facturacion.
        /// </summary>
        /// <param name="rpu">The rpu.</param>
        /// <returns>System.Double.</returns>
        public double Obtener_Saldo_Facturacion(string rpu)
        {

            double respuesta = 0;
            DataTable Dt_Consulta;

            string strsql = "SELECT SUM(fd.Total) - ISNULL(( " +
                     "   SELECT sum(mc.total) " +
                     "   FROM Ope_Cor_Caj_Movimientos_Cobros_Espejo mc " +
                     "   WHERE mc.No_Movimiento_Facturacion = fd.No_Movimiento " +
                     "       AND mc.estado_concepto_cobro <> 'CANCELADO' " +
                     "   ), 0)  as [total] " +
           " FROM Ope_Cor_Facturacion_Recibos f " +
           " JOIN Ope_Cor_Facturacion_Recibos_Detalles fd ON f.No_Factura_Recibo = fd.No_Factura_Recibo " +
           " WHERE f.RPU = '" + rpu + "'  AND fd.Estatus <> 'CANCELADO'  " +
           "     AND f.Estatus_Recibo IN ( " +
           "         'PENDIENTE' " +
           "        ,'PARCIAL' " +
           "         ) " +
           " GROUP BY f.RPU " +
           "     ,fd.No_Movimiento " +
           " HAVING ( " +
           "         SUM(fd.Total) - ISNULL(( " +
           "                 SELECT sum(mc.total) " +
           "                 FROM Ope_Cor_Caj_Movimientos_Cobros_Espejo mc " +
           "                 WHERE mc.No_Movimiento_Facturacion = fd.No_Movimiento " +
           "	                    AND mc.estado_concepto_cobro <> 'CANCELADO' " +
           "                 ), 0) " +
           "         ) <> 0 ";



            Dt_Consulta = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, strsql).Tables[0];

            if (Dt_Consulta.Rows.Count > 0)
                respuesta = Convert.ToDouble(Dt_Consulta.Compute("sum(total)", ""));

            return respuesta;
        }
    }
}