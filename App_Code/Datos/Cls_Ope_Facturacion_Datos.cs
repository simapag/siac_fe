﻿using System;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using SIAC.Constantes;
using System.Collections.Generic;
using SharpContent.ApplicationBlocks.Data;
using SIAC.Facturacion.Negocio;
using SIAC_Ope_Facturas_Detalles.Negocio;
using System.Text;
using System.Text.RegularExpressions;
using SIAC.Metodos_Generales;

namespace SIAC.Facturacion.Datos
{
    public class Cls_Ope_Facturacion_Datos
    {
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_Datos_Factura()
        //DESCRIPCIÓN:      consulta los datos para la facturacion
        //PARÁMETROS:       Cls_Ope_Facturacion_Negocio: Datos para realizar la busqueda
        //CREO:             Ramón Baeza Yépez
        //FECHA_CREO:       05/12/2013
        //*******************************************************************************************************
        public DataTable Consultar_Datos_Factura(Cls_Ope_Facturacion_Negocio Datos)
        {
            DataTable Dt_Datos = new DataTable();
            String Mi_SQL;
            try
            {
                Mi_SQL = "select *, Factura='USUARIO' from OPE_FACTURAS where FOLIO_PAGO = '" + Datos.No_Folio + "' and CANCELADA != 'SI' order by Fecha_Emision Desc";
                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }


        public DataTable Consultar_Datos_Cliente_Por_RFC(string rfc)
        {

            DataTable Dt_Datos = new DataTable();
            String Mi_SQL;
            try
            {
                Mi_SQL = "SELECT ";
                Mi_SQL += "c.CLIENTE_ID ";
                Mi_SQL += ",c.NOMBRE_CORTO ";
                Mi_SQL += ",c.RAZON_SOCIAL ";
                Mi_SQL += ",c.CALLE ";
                Mi_SQL += ",c.NUMERO_EXTERIOR ";
                Mi_SQL += ",c.NUMERO_INTERIOR ";
                Mi_SQL += ",c.COLONIA ";
                Mi_SQL += ",c.CP ";
                Mi_SQL += ",c.ESTADO ";
                Mi_SQL += ",c.CIUDAD AS CIUDAD_ID ";
                Mi_SQL += ",c.LOCALIDAD ";
                Mi_SQL += ",c.PAIS ";
                Mi_SQL += ",c.RFC ";
                Mi_SQL += ",c.EMAIL ";
                Mi_SQL += ",c.Estatus ";
                Mi_SQL += ",e.NOMBRE AS NOMBRE_ESTADO ";
                Mi_SQL += ",ci.NOMBRE AS CIUDAD ";
                Mi_SQL += "FROM CAT_ADM_CLIENTES AS c ";
                Mi_SQL += "LEFT OUTER JOIN CAT_COR_ESTADOS AS e ";
                Mi_SQL += "ON c.ESTADO = e.ESTADO_ID ";
                Mi_SQL += "LEFT OUTER JOIN CAT_COR_CIUDADES AS ci ";
                Mi_SQL += "ON c.CIUDAD = ci.CIUDAD_ID " +
                         "WHERE " +
                         "c.RFC='" + rfc + "' AND c.ESTATUS IS NULL AND c.PREDIO_ID IS NULL ORDER BY c.CLIENTE_ID DESC ";
                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;

        }



        public DataTable Consultar_Datos_Usuarios_No_Registrados(string Usuario_ID)
        {

            DataTable Dt_Datos = new DataTable();
            String Mi_SQL;
            try
            {
                Mi_SQL = "SELECT ";
                Mi_SQL += "c.Usuario_No_Registrado_ID,  ";
                Mi_SQL += " '' as Cliente_ID,  ";
                Mi_SQL += "c.Nombre as NOMBRE_CORTO,  ";
                Mi_SQL += "c.Razon_Social as RAZON_SOCIAL,  ";
                Mi_SQL += "c.Calle_Factura as CALLE,  ";
                Mi_SQL += "c.Numero_Factura as NUMERO_EXTERIOR,  ";
                Mi_SQL += "'' as NUMERO_INTERIOR,  ";
                Mi_SQL += "c.Colonia_Factura as COLONIA,  ";
                Mi_SQL += " c.Codigo_Postal_Factura as CP,  ";
                Mi_SQL += "c.Estado_Factura as ESTADO,  ";
                Mi_SQL += "c.Ciudad_Factura AS CIUDAD_ID,  ";
                Mi_SQL += "c.Ciudad_Factura as LOCALIDAD,  ";
                Mi_SQL += "'MEXICO' AS PAIS, ";
                Mi_SQL += "c.RFC_Factura AS RFC,  ";
                Mi_SQL += "c.Correo_Electronico_Factura AS EMAIL,  ";
                Mi_SQL += "c.Estatus AS Estatus,  ";
                Mi_SQL += "c.Estado_Factura AS NOMBRE_ESTADO,  ";
                Mi_SQL += "c.Ciudad_Factura AS CIUDAD ";
                Mi_SQL += "FROM Cat_Cor_Usuarios_No_Registrados AS c ";
                Mi_SQL += "WHERE c.Usuario_No_Registrado_ID = '" + Usuario_ID + "' ";
                Mi_SQL += "ORDER BY c.Usuario_No_Registrado_ID DESC ";
                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;

        }

        public DataTable Consultar_Datos_Diversos_Por_Codigo_Barras(string codigo_barras)
        {

            DataTable Dt_Datos = new DataTable();
            String Mi_SQL;
            try
            {
                Mi_SQL = "SELECT * from Ope_Cor_Diversos where Codigo_Barras = '" + codigo_barras + "'"; ;
                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;

        }

        public DataTable Consultar_Datos_Sanciones_Por_Codigo_Barras(string codigo_barras)
        {

            DataTable Dt_Datos = new DataTable();
            String Mi_SQL;
            try
            {
                Mi_SQL = "SELECT * from Ope_Cor_Sanciones where Codigo_Barras = '" + codigo_barras + "'"; ;
                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;

        }


        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_Datos_Cliente()
        //DESCRIPCIÓN:      consulta los datos para la facturacion
        //PARÁMETROS:       Cls_Ope_Facturacion_Negocio: Datos para realizar la busqueda
        //CREO:             Ramón Baeza Yépez
        //FECHA_CREO:       05/12/2013
        //*******************************************************************************************************
        public DataTable Consultar_Datos_Cliente(Cls_Ope_Facturacion_Negocio Datos)
        {
            DataTable Dt_Datos = new DataTable();
            String Mi_SQL;
            try
            {
                //Mi_SQL = "select Cli.*, Est.NOMBRE as NOMBRE_ESTADO from CAT_ADM_CLIENTES as Cli left outer join CAT_COR_ESTADOS as Est on Cli.ESTADO_ID = Est.ESTADO_ID where Cli.USUARIO_ID " +
                //         "in ( select Usuarios.USUARIO_ID AS CLIENTE_ID from cat_cor_usuarios Usuarios LEFT OUTER JOIN Cat_Cor_Predios_Usuarios Padron ON Padron.Usuario_ID = " +
                //          "Usuarios.USUARIO_ID WHERE Padron.No_Cuenta = '" + Datos.No_Cuenta + "');";

                Mi_SQL = "SELECT ";
                Mi_SQL += "c.CLIENTE_ID ";
                Mi_SQL += ",c.NOMBRE_CORTO ";
                Mi_SQL += ",c.RAZON_SOCIAL ";
                Mi_SQL += ",c.CALLE ";
                Mi_SQL += ",c.NUMERO_EXTERIOR ";
                Mi_SQL += ",c.NUMERO_INTERIOR ";
                Mi_SQL += ",c.COLONIA ";
                Mi_SQL += ",c.CP ";
                Mi_SQL += ",c.ESTADO ";
                Mi_SQL += ",c.CIUDAD AS CIUDAD_ID ";
                Mi_SQL += ",c.LOCALIDAD ";
                Mi_SQL += ",c.PAIS ";
                Mi_SQL += ",c.RFC ";
                Mi_SQL += ",c.EMAIL ";
                Mi_SQL += ",c.Estatus ";
                Mi_SQL += ",e.NOMBRE AS NOMBRE_ESTADO ";
                Mi_SQL += ",ci.NOMBRE AS CIUDAD ";
                Mi_SQL += "FROM CAT_ADM_CLIENTES AS c ";
                Mi_SQL += "LEFT OUTER JOIN CAT_COR_ESTADOS AS e ";
                Mi_SQL += "ON c.ESTADO = e.ESTADO_ID ";
                Mi_SQL += "LEFT OUTER JOIN CAT_COR_CIUDADES AS ci ";
                Mi_SQL += "ON c.CIUDAD = ci.CIUDAD_ID ";
                Mi_SQL += "WHERE c.CLIENTE_ID ";
                if (!String.IsNullOrEmpty(Datos.Cliente_ID))
                    Mi_SQL += "= '" + Datos.Cliente_ID + "'";
                else
                {
                    Mi_SQL += "IN (SELECT ";
                    Mi_SQL += "usuarios.CLIENTE_ID AS CLIENTE_ID ";
                    Mi_SQL += "FROM CAT_ADM_CLIENTES usuarios ";
                    Mi_SQL += "LEFT OUTER JOIN Cat_Cor_Predios p ";
                    Mi_SQL += "ON p.Predio_ID = Usuarios.PREDIO_ID ";
                    Mi_SQL += "WHERE p.RPU = '" + Datos.P_Rpu + "')";
                }
                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_Tabla_Cliente()
        //DESCRIPCIÓN:      consulta los datos para la facturacion
        //PARÁMETROS:       Cls_Ope_Facturacion_Negocio: Datos para realizar la busqueda
        //CREO:             Miguel Angel Alvarado Enriquez
        //FECHA_CREO:       19/Feb/2014
        //*******************************************************************************************************
        public DataTable Consultar_Tabla_Cliente(Cls_Ope_Facturacion_Negocio Datos)
        {
            DataTable Dt_Datos = new DataTable();
            bool Entro_Where = false;
            String Mi_SQL;
            try
            {
                Mi_SQL = "SELECT Cli.*, Est.NOMBRE AS NOMBRE_ESTADO FROM CAT_ADM_CLIENTES AS Cli ";
                Mi_SQL += "LEFT OUTER JOIN CAT_COR_ESTADOS AS Est ON Cli.ESTADO = Est.ESTADO_ID ";
                if (!String.IsNullOrEmpty(Datos.Nombre))
                {
                    Mi_SQL += Entro_Where ? " AND " : " WHERE "; Entro_Where = true;
                    Mi_SQL += "RAZON_SOCIAL = '" + Datos.Nombre + "'";
                }
                Mi_SQL += Entro_Where ? " AND " : " WHERE "; Entro_Where = true;
                Mi_SQL += "RFC = '" + Datos.Rfc + "';";

                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }


        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_Tabla_Cliente()
        //DESCRIPCIÓN:      consulta los datos para la facturacion
        //PARÁMETROS:       Cls_Ope_Facturacion_Negocio: Datos para realizar la busqueda
        //CREO:             Miguel Angel Alvarado Enriquez
        //FECHA_CREO:       19/Feb/2014
        //*******************************************************************************************************
        public DataTable Consultar_Existe_Rezago(Cls_Ope_Facturacion_Negocio Datos)
        {
            DataTable Dt_Datos = new DataTable();
            bool Entro_Where = false;
            String Mi_SQL;
            try
            {
                Mi_SQL = "SELECT fc.Fecha_Limite_Pago, fd.* ";
                Mi_SQL += "FROM Ope_Cor_Facturacion_Recibos fc, Ope_Cor_Facturacion_Recibos_Detalles fd ";
                Mi_SQL += "WHERE fc.RPU = '"  +Datos.P_Rpu+ "' and fc.No_Factura_Recibo = fd.No_Factura_Recibo ";
                Mi_SQL += "and fd.Estatus <> 'PAGADO' and fd.No_Movimiento NOT IN (select mc.No_Movimiento_Facturacion from Ope_Cor_Caj_Movimientos_Cobros_Espejo mc ";
                Mi_SQL += "where mc.estado_concepto_cobro = 'PORAPLICAR'  and mc.No_Movimiento_Facturacion = fd.No_Movimiento and (fd.Total_Saldo - mc.total) <= 0)";
                Mi_SQL += " AND fd.Total_Saldo > 0 ";

                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }


        public string Consultar_Diverso_RFC(string codigo_barras)
        {
            string respuesta = "";
            String Mi_SQL;
            Mi_SQL = "SELECT isnull( RFC,'') as [rfc] FROM Ope_Cor_Diversos WHERE Codigo_Barras='" + codigo_barras + "'";
            respuesta = Convert.ToString(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL));

            return respuesta;
        }

        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_Datos_Cliente()
        //DESCRIPCIÓN:      consulta los datos para la facturacion
        //PARÁMETROS:       Cls_Ope_Facturacion_Negocio: Datos para realizar la busqueda
        //CREO:             Ramón Baeza Yépez
        //FECHA_CREO:       05/12/2013
        //*******************************************************************************************************
        public DataTable Consultar_No_Cuenta(Cls_Ope_Facturacion_Negocio Datos)
        {
            DataTable Dt_Datos = new DataTable();
            String Mi_SQL;
            try
            {
                Mi_SQL = "select rc.*,CASE substring(rc.CODIGO_BARRAS, LEN(rc.CODIGO_BARRAS), LEN(rc.CODIGO_BARRAS))" +
        "WHEN 'D' "+
            "THEN CASE rc.RPU "+
                    "WHEN 0 "+
                        "THEN CASE rc.total_iva_cobrado " +
                                "WHEN 0 " +
                                    "THEN 'DOMESTICO' " +
                                "ELSE 'NO DOMESTICO' " +
                                "END " +
                        "ELSE( " +
                            "SELECT " +
                            "g.Nombre_Giro " +
                            "FROM Cat_Cor_Predios p " +
                            "JOIN Cat_Cor_Tarifas t ON t.Tarifa_ID = p.Tarifa_ID " +
                            "JOIN Cat_Cor_Giros g ON g.GIRO_ID = t.Giro_ID " +
                                "AND p.RPU = rc.RPU " +
                            ") " +
                    "END " +
        "ELSE( " +
                "SELECT g.Nombre_Giro " +
                "FROM Cat_Cor_Predios p " +
                "JOIN Cat_Cor_Tarifas t ON t.Tarifa_ID = p.Tarifa_ID " +
                "JOIN Cat_Cor_Giros g ON g.GIRO_ID = t.Giro_ID " +
                    "AND p.RPU = rc.RPU " +
                ") " +
        "END AS Tipo_Servicio from Ope_Cor_Caj_Recibos_Cobros rc " +
                    "WHERE rc.CODIGO_BARRAS = '" + Datos.Codigo_Barras + "' " +
                    " AND (rc.estado_Recibo='ACTIVO' OR rc.estado_recibo = 'PORAPLICAR') ";
                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                //Dt_Datos = SqlHelper.ExecuteDataset("Data Source=187.174.176.231;Initial Catalog=Simapag;User ID=dbcajas;Password=TellerMachine01", CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }

        public string Consultar_Lugar_Expedicion(string noRecibo)
        {
            string respuesta = "";
            String Mi_SQL;
            Mi_SQL = @"select '36000' as lugar_expedicion
                from Ope_Cor_Caj_Recibos_Cobros as rc
                    inner join Ope_Cor_Cajas as c on c.OPERACION_CAJA_ID = rc.OPERACION_CAJA_ID
                    INNER join Cat_Cor_Sucursales as su on su.SUCURSAL_ID = c.SUCURSAL_ID
                WHERE rc.NO_RECIBO = {0}";
            respuesta = Convert.ToString(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, string.Format(Mi_SQL, noRecibo)));

            //return respuesta;
            return "36000";
        }

        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_Datos_Pago()
        //DESCRIPCIÓN:      consulta los datos de la forma de pago ya sea cheque o tarjeta
        //PARÁMETROS:       Cls_Ope_Facturacion_Negocio: Datos para realizar la busqueda
        //CREO:             Fernando Gonzalez
        //FECHA_CREO:       04/06/2014
        //*******************************************************************************************************
        public DataTable Consultar_Datos_Pago(Cls_Ope_Facturacion_Negocio Datos)
        {
            DataTable Dt_Datos = new DataTable();
            String Mi_SQL = "";
            try
            {
                if (Datos.P_Tipo_Pago == "TARJETA")
                {
                    Mi_SQL = "select * FROM ope_cor_caj_tarjetas where No_Recibo='" + Datos.P_No_Factura + "' AND Estado_Tarjeta='ACTIVO'";
                }
                else
                {
                    Mi_SQL = "select Ope_Cor_Caj_Cheques.*,Cat_Cor_Bancos.Nombre as Nombre_Banco from Ope_Cor_Caj_Cheques,Cat_Cor_Bancos where Ope_Cor_Caj_Cheques.Banco_ID=Cat_Cor_Bancos.Banco_ID and No_Recibo='" + Datos.P_No_Factura + "'";
                }

                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION: Consultar_Detalles_Pago
        ///DESCRIPCION         : Consulta los datos de los conceptos a facturar de las tablas 
        ///                     Ope_Cor_Caj_Movimientos_Cobros_Espejo y Ope_Cor_Caj_Recibos_Cobros
        ///PARAMETROS          : Cls_Ope_Facturacion_Negocio: Datos para realizar la busqueda    
        ///CREO                : Jesus Toledo Rodriguez
        ///FECHA_CREO          : 12/Diciembre/2013
        ///MODIFICO            :                
        ///FECHA_MODIFICO      :          
        ///CAUSA_MODIFICACION  :      
        ///*******************************************************************************
        public DataTable Consultar_Detalles_Pago(Cls_Ope_Facturacion_Negocio Datos)
        {
            DataTable Dt_Datos = null;
            String Mi_SQL;
            String ID_Iva;
            Double Importe_Iva = 0.0;
            try
            {
                //Mi_SQL = "SELECT r.CODIGO_BARRAS AS [codigo_barras] " +
                //           ",r.No_Cuenta AS [no_cuenta] " +
                //           ",m.CONCEPTO_ID AS [clave_concepto] " +
                //           ",c.Nombre AS [descripcion] " +
                //           ",1 AS [valor_unitario] " +
                //           ",sum(m.IMPORTE) AS [importe] " +
                //           ",0.0 AS [iva] " +
                //           ",sum(m.IMPORTE) AS [total] " +
                //           ",max(m.FECHA_MOVIMIENTO) as [FECHA_MOVIMIENTO] " +
                //           ",'SERVICIOS' as Unidad_de_Medida ,c.lleva_iva, (select isnull(Porcentaje_IVA,0) from Cat_Cor_Parametros where Parametro_ID='00001') as Porcentaje_IVA " +
                //           ",r.Periodo as Periodo_Pago," +
                //           "case substring(r.CODIGO_BARRAS,LEN(r.CODIGO_BARRAS),LEN(r.CODIGO_BARRAS)) " +
                //           "WHEN 'D' THEN CASE r.No_Cuenta WHEN 0 then case r.total_iva_cobrado WHEN 0 THEN 'DOMESTICO' ELSE 'NO DOMESTICO' END " +
                //            "else (select g.Nombre_Giro from Cat_Cor_Predios p JOIN Cat_Cor_Tarifas t ON t.Tarifa_ID = p.Tarifa_ID " +
                //            "JOIN Cat_Cor_Giros g ON g.GIRO_ID = t.Giro_ID  and p.No_Cuenta = r.No_Cuenta ) end " +
                //            "ELSE (select g.Nombre_Giro from Cat_Cor_Predios p JOIN Cat_Cor_Tarifas t ON t.Tarifa_ID = p.Tarifa_ID " +
                //            "JOIN Cat_Cor_Giros g ON g.GIRO_ID = t.Giro_ID and p.No_Cuenta = r.No_Cuenta ) END as Tipo_Servicio " +
                //        "FROM Ope_Cor_Caj_Recibos_Cobros r " +
                //        "JOIN Ope_Cor_Caj_Movimientos_Cobros_Espejo m ON r.NO_RECIBO = m.NO_RECIBO " +
                //        "JOIN Cat_Cor_Conceptos_Cobros c ON m.CONCEPTO_ID = c.CONCEPTO_ID " +
                //        "WHERE estado_recibo = 'ACTIVO' ";

                //if (!String.IsNullOrEmpty(Datos.No_Folio))
                //    Mi_SQL += " AND CODIGO_BARRAS = '" + Datos.No_Folio + "' ";
                //Mi_SQL += "GROUP by m.Concepto_ID,c.Nombre,r.CODIGO_BARRAS,r.No_Cuenta,m.NO_MOVIMIENTO,c.lleva_iva,r.Periodo,r.total_iva_cobrado";

                Mi_SQL = " SELECT r.CODIGO_BARRAS AS [codigo_barras]";
                Mi_SQL += "	,r.RPU AS [no_cuenta]";
                Mi_SQL += "	,m.CONCEPTO_ID AS [clave_concepto]";
                Mi_SQL += "	,c.Nombre AS [descripcion]";
                Mi_SQL += "	,1 AS [valor_unitario]";
                Mi_SQL += "	,sum(m.IMPORTE) AS [importe]";
                Mi_SQL += "	,0.0 AS [iva]";
                Mi_SQL += "	,sum(m.IMPORTE) AS [total]";
                Mi_SQL += "	,max(m.FECHA_MOVIMIENTO) AS [FECHA_MOVIMIENTO]";
                Mi_SQL += "	,'SERVICIOS' AS Unidad_de_Medida";
                Mi_SQL += "	,c.impuesto_id";
                Mi_SQL += "	,(SELECT isnull(Porcentaje_IVA, 0)";
                Mi_SQL += "		FROM Cat_Cor_Parametros";
                Mi_SQL += "		WHERE Parametro_ID = '00001'";
                Mi_SQL += ") AS Porcentaje_IVA";
                Mi_SQL += "	,r.Periodo AS Periodo_Pago";
                Mi_SQL += "	,CASE substring(r.CODIGO_BARRAS, LEN(r.CODIGO_BARRAS), LEN(r.CODIGO_BARRAS))";
                Mi_SQL += "	WHEN 'D'";
                Mi_SQL += "	THEN CASE r.RPU";
                Mi_SQL += "	WHEN 0";
                Mi_SQL += "	THEN CASE r.total_iva_cobrado";
                Mi_SQL += "	WHEN 0";
                Mi_SQL += "	THEN 'DOMESTICO'";
                Mi_SQL += "	ELSE 'NO DOMESTICO'";
                Mi_SQL += "	END";
                Mi_SQL += "	ELSE (";
                Mi_SQL += "	SELECT g.Nombre_Giro";
                Mi_SQL += "	FROM Cat_Cor_Predios p";
                Mi_SQL += "	JOIN Cat_Cor_Tarifas t ON t.Tarifa_ID = p.Tarifa_ID";
                Mi_SQL += "	JOIN Cat_Cor_Giros g ON g.GIRO_ID = t.Giro_ID";
                Mi_SQL += "	AND p.RPU = r.RPU)";
                Mi_SQL += "	END";
                Mi_SQL += "	ELSE (";
                Mi_SQL += "		SELECT g.Nombre_Giro";
                Mi_SQL += "	FROM Cat_Cor_Predios p";
                Mi_SQL += "	JOIN Cat_Cor_Tarifas t ON t.Tarifa_ID = p.Tarifa_ID";
                Mi_SQL += "	JOIN Cat_Cor_Giros g ON g.GIRO_ID = t.Giro_ID";
                Mi_SQL += "	AND p.RPU = r.RPU)";
                Mi_SQL += "	END AS Tipo_Servicio";
                Mi_SQL += "	FROM Ope_Cor_Caj_Recibos_Cobros r";
                Mi_SQL += "	JOIN Ope_Cor_Caj_Movimientos_Cobros_Espejo m ON r.NO_RECIBO = m.NO_RECIBO";
                Mi_SQL += "	JOIN Cat_Cor_Conceptos_Cobros c ON m.CONCEPTO_ID = c.CONCEPTO_ID";
                Mi_SQL += "	WHERE (estado_recibo = 'ACTIVO' OR Estado_Recibo='PORAPLICAR')";
                Mi_SQL += " AND c.Facturable = 'SI'";
                if (!String.IsNullOrEmpty(Datos.No_Folio))
                    Mi_SQL += " AND CODIGO_BARRAS = '" + Datos.No_Folio + "'";
                Mi_SQL += "	GROUP BY m.Concepto_ID";
                Mi_SQL += "		,c.Nombre";
                Mi_SQL += "		,r.CODIGO_BARRAS";
                Mi_SQL += "		,r.RPU";
                Mi_SQL += "		,m.NO_MOVIMIENTO";
                Mi_SQL += "		,c.impuesto_id";
                Mi_SQL += "		,r.Periodo";
                Mi_SQL += "		,r.total_iva_cobrado";



                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                if (Dt_Datos.Rows.Count > 0)
                {
                    Mi_SQL = "select Concepto_Iva_ID from Cat_Cor_Costos_Factores where Año='" + ((DateTime)Dt_Datos.Rows[0]["FECHA_MOVIMIENTO"]).Year + "'";
                    ID_Iva = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).ToString();
                    if (!String.IsNullOrEmpty(ID_Iva))
                    {
                        for (int Indice = 0; Indice < Dt_Datos.Rows.Count; Indice++)
                        {
                            if (Dt_Datos.Rows[Indice]["clave_concepto"].ToString().Equals(ID_Iva))
                            {
                                Importe_Iva = Convert.ToDouble(Dt_Datos.Rows[Indice]["importe"].ToString());
                                Dt_Datos.Rows.RemoveAt(Indice);
                                Dt_Datos.Rows[Dt_Datos.Rows.Count - 1]["iva"] = Importe_Iva;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;

        }
        public DataTable Consultar_Recibo(Cls_Ope_Facturacion_Negocio Datos)
        {
            String Mi_SQL;
            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado            
            SqlConnection Obj_Conexion = null;
            SqlCommand Obj_Comando = null;
            SqlDataReader Dr_Lector;
            //Asignar conexion
            Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            Obj_Comando = Obj_Conexion.CreateCommand();

            try
            {
                Mi_SQL = "SELECT RPU AS [no_cuenta], TOTAL_RECIBOS as [total],total_iva_cobrado as [iva],(TOTAL_RECIBOS - isnull(total_iva_cobrado,0)) as [subtotal],Fecha,Periodo,isnull(nullif(No_Factura,''),0) as [No_Factura],(SELECT isnull(Porcentaje_IVA,16) from Cat_Cor_Parametros) as [porcentaje_iva]";
                Mi_SQL += ",Codigo_Barras FROM Ope_Cor_Caj_Recibos_Cobros";
                Mi_SQL += " WHERE CODIGO_BARRAS = @cb";
                Mi_SQL += " AND estado_Recibo in ('ACTIVO','PORAPLICAR')";
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.Parameters.Add("@cb", SqlDbType.Char, 50).Value = Datos.No_Folio;
                Obj_Comando.CommandType = CommandType.Text;
                //Ejecutar consulta
                Dr_Lector = Obj_Comando.ExecuteReader();
                Dt_Resultado.Load(Dr_Lector);
                //Cerrar conexion
                Dr_Lector.Close();
                Obj_Conexion.Close();
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Resultado;
        }
                public DataTable Consultar_Detalles_Pago_PDF(Cls_Ope_Facturacion_Negocio Datos)
        {
            DataTable Dt_Datos = null;
            String Mi_SQL;
            String ID_Iva;
            Decimal Importe_Iva = 0.0M;

            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado            
            SqlConnection Obj_Conexion = null;
            SqlCommand Obj_Comando = null;
            SqlDataReader Dr_Lector;
            //Asignar conexion
            Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            Obj_Comando = Obj_Conexion.CreateCommand();

            try
            {
                Mi_SQL = "SELECT mc.RPU AS [no_cuenta],mc.CONCEPTO_ID AS [clave_concepto],cc.Nombre AS [descripcion],'1' as [Cantidad], sum(mc.IMPORTE) as [importe], sum(mc.IMPORTE) as [Valor_Unitario], ";
                Mi_SQL += "case when sum(mc.impuesto) = 0 THEN " +
                    "CONVERT(FLOAT, ROUND(sum(CASE " +
                    "WHEN i.PORCENTAJE_IMPUESTO > 0 " +
                    "THEN (mc.IMPORTE * isnull(i.PORCENTAJE_IMPUESTO, 0) / 100 ) " +
                    "ELSE 0 " +
                    "END), 2)) " +
                    "ELSE sum(mc.impuesto) END AS [iva]" +
                    ",CONVERT(FLOAT, ROUND(sum(CASE " +
                    "WHEN i.PORCENTAJE_IMPUESTO > 0 " +
                    "THEN (ROUND(mc.IMPORTE,2) * isnull(i.PORCENTAJE_IMPUESTO, 0) / 100 ) " +
                    "ELSE 0 END), 4)) AS [ivaCorrecto],";

                //Mi_SQL += "sum(mc.impuesto) as [iva],CONVERT(float,ROUND(sum(case when mc.impuesto > 0 then (mc.IMPORTE * 0.16) ELSE 0 END),4)) as [ivaCorrecto],";

                Mi_SQL += " sum(mc.total) as [total],";
                Mi_SQL += "'SERVICIOS' as [Unidad_de_Medida],isnull(i.PORCENTAJE_IMPUESTO,0)/100 as [tasa],MAX(mc.FECHA_MOVIMIENTO) as FECHA_MOVIMIENTO ";
                Mi_SQL += "FROM Ope_Cor_Caj_Movimientos_Cobros_Espejo mc ";
                Mi_SQL += "JOIN Cat_Cor_Conceptos_Cobros cc ON mc.CONCEPTO_ID = cc.Concepto_ID ";
                Mi_SQL += "left outer JOIN CAT_COM_IMPUESTOS i ON cc.impuesto_id = i.IMPUESTO_ID ";
                Mi_SQL += " WHERE NO_RECIBO = (SELECT TOP 1 NO_RECIBO from Ope_Cor_Caj_Recibos_Cobros where CODIGO_BARRAS = @cb AND estado_recibo in ('PORAPLICAR','ACTIVO')) ";
                Mi_SQL += " AND upper(cc.Nombre) NOT LIKE ('IVA') AND upper(cc.Nombre) NOT LIKE ('IVA ANTICIPO') ";
                Mi_SQL += " GROUP BY mc.Concepto_ID,mc.RPU,cc.Nombre,i.PORCENTAJE_IMPUESTO";

                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.Parameters.Add("@cb", SqlDbType.Char, 50).Value = Datos.No_Folio;
                Obj_Comando.CommandType = CommandType.Text;
                //Ejecutar consulta
                Dr_Lector = Obj_Comando.ExecuteReader();
                Dt_Resultado.Load(Dr_Lector);
                //Cerrar conexion
                Dr_Lector.Close();
                Obj_Conexion.Close();
                //Entrega de resultado
                //////Mi_SQL = "SELECT r.CODIGO_BARRAS AS [codigo_barras] " +
                //////           ",r.No_Cuenta AS [no_cuenta] " +
                //////           ",m.CONCEPTO_ID AS [clave_concepto] " +
                //////           ",c.Nombre AS [descripcion] " +
                //////           ",1 AS [valor_unitario] " +
                //////           ",sum(m.IMPORTE) AS [importe] " +
                //////           ",0.0 AS [iva] " +
                //////           ",sum(m.IMPORTE) AS [total] " +
                //////           ",max(m.FECHA_MOVIMIENTO) as [FECHA_MOVIMIENTO] " +
                //////           ",'SERVICIOS' as Unidad_de_Medida ,c.lleva_iva, (select isnull(Porcentaje_IVA,0) from Cat_Cor_Parametros where Parametro_ID='00001') as Porcentaje_IVA " +
                //////           ",r.Periodo as Periodo_Pago," +
                //////           "case substring(r.CODIGO_BARRAS,LEN(r.CODIGO_BARRAS),LEN(r.CODIGO_BARRAS)) " +
                //////           "WHEN 'D' THEN CASE r.No_Cuenta WHEN 0 then case r.total_iva_cobrado WHEN 0 THEN 'DOMESTICO' ELSE 'NO DOMESTICO' END " +
                //////            "else (select g.Nombre_Giro from Cat_Cor_Predios p JOIN Cat_Cor_Tarifas t ON t.Tarifa_ID = p.Tarifa_ID " +
                //////            "JOIN Cat_Cor_Giros g ON g.GIRO_ID = t.Giro_ID  and p.No_Cuenta = r.No_Cuenta ) end " +
                //////            "ELSE (select g.Nombre_Giro from Cat_Cor_Predios p JOIN Cat_Cor_Tarifas t ON t.Tarifa_ID = p.Tarifa_ID " +
                //////            "JOIN Cat_Cor_Giros g ON g.GIRO_ID = t.Giro_ID and p.No_Cuenta = r.No_Cuenta ) END as Tipo_Servicio " +
                //////        "FROM Ope_Cor_Caj_Recibos_Cobros r " +
                //////        "JOIN Ope_Cor_Caj_Movimientos_Cobros_Espejo m ON r.NO_RECIBO = m.NO_RECIBO " +
                //////        "JOIN Cat_Cor_Conceptos_Cobros c ON m.CONCEPTO_ID = c.CONCEPTO_ID " +
                //////        "WHERE estado_recibo = 'ACTIVO' ";

                //////if (!String.IsNullOrEmpty(Datos.No_Folio))
                //////    Mi_SQL += " AND CODIGO_BARRAS = '" + Datos.No_Folio + "' ";
                //////Mi_SQL += "GROUP by m.Concepto_ID,c.Nombre,r.CODIGO_BARRAS,r.No_Cuenta,m.NO_MOVIMIENTO,c.lleva_iva,r.Periodo,r.total_iva_cobrado";

                //Mi_SQL = " SELECT r.CODIGO_BARRAS AS [codigo_barras]";
                //Mi_SQL += "	,r.RPU AS [no_cuenta]";
                //Mi_SQL += "	,m.CONCEPTO_ID AS [clave_concepto]";
                //Mi_SQL += "	,c.Nombre AS [descripcion]";
                //Mi_SQL += ", c.Orden";
                //Mi_SQL += "	,1 AS [valor_unitario]";
                //Mi_SQL += "	,sum(m.IMPORTE) AS [importe]";
                //Mi_SQL += "	,0.0 AS [iva]";
                //Mi_SQL += "	,m.impuesto AS [ivaCorrecto]";
                //Mi_SQL += "	,sum(m.IMPORTE) AS [total]";
                //Mi_SQL += "	,max(m.FECHA_MOVIMIENTO) AS [FECHA_MOVIMIENTO]";
                //Mi_SQL += "	,'SERVICIOS' AS Unidad_de_Medida";
                //Mi_SQL += "	,c.impuesto_id";
                //Mi_SQL += "	,(SELECT isnull(Porcentaje_IVA, 0)";
                //Mi_SQL += "		FROM Cat_Cor_Parametros";
                //Mi_SQL += "		WHERE Parametro_ID = '00001'";
                //Mi_SQL += ") AS Porcentaje_IVA";
                //Mi_SQL += "	,r.Periodo AS Periodo_Pago";
                //Mi_SQL += "	,CASE substring(r.CODIGO_BARRAS, LEN(r.CODIGO_BARRAS), LEN(r.CODIGO_BARRAS))";
                //Mi_SQL += "	WHEN 'D'";
                //Mi_SQL += "	THEN CASE r.RPU";
                //Mi_SQL += "	WHEN 0";
                //Mi_SQL += "	THEN CASE r.total_iva_cobrado";
                //Mi_SQL += "	WHEN 0";
                //Mi_SQL += "	THEN 'DOMESTICO'";
                //Mi_SQL += "	ELSE 'NO DOMESTICO'";
                //Mi_SQL += "	END";
                //Mi_SQL += "	ELSE (";
                //Mi_SQL += "	SELECT g.Nombre_Giro";
                //Mi_SQL += "	FROM Cat_Cor_Predios p";
                //Mi_SQL += "	JOIN Cat_Cor_Tarifas t ON t.Tarifa_ID = p.Tarifa_ID";
                //Mi_SQL += "	JOIN Cat_Cor_Giros g ON g.GIRO_ID = t.Giro_ID";
                //Mi_SQL += "	AND p.RPU = r.RPU)";
                //Mi_SQL += "	END";
                //Mi_SQL += "	ELSE (";
                //Mi_SQL += "		SELECT g.Nombre_Giro";
                //Mi_SQL += "	FROM Cat_Cor_Predios p";
                //Mi_SQL += "	JOIN Cat_Cor_Tarifas t ON t.Tarifa_ID = p.Tarifa_ID";
                //Mi_SQL += "	JOIN Cat_Cor_Giros g ON g.GIRO_ID = t.Giro_ID";
                //Mi_SQL += "	AND p.RPU = r.RPU)";
                //Mi_SQL += "	END AS Tipo_Servicio";
                //if (!String.IsNullOrEmpty(Datos.No_Folio))
                //{
                //    Mi_SQL += string.Format(" ,(SELECT SUM(IMPORTE) FROM Ope_Cor_Caj_Movimientos_Cobros_Espejo WHERE NO_RECIBO = (SELECT NO_RECIBO from Ope_Cor_Caj_Recibos_Cobros WHERE CODIGO_BARRAS = '{0}')) as totalGlobal", Datos.No_Folio);
                //    Mi_SQL += string.Format(" ,(SELECT ROUND(SUM(impuesto),2, 1) FROM Ope_Cor_Caj_Movimientos_Cobros_Espejo WHERE NO_RECIBO = (SELECT NO_RECIBO from Ope_Cor_Caj_Recibos_Cobros WHERE CODIGO_BARRAS = '{0}')) as impuestoGlobal", Datos.No_Folio);
                //    Mi_SQL += string.Format(" ,(SELECT SUM(IMPORTE) - ROUND(SUM(impuesto),2, 1) FROM Ope_Cor_Caj_Movimientos_Cobros_Espejo WHERE NO_RECIBO = (SELECT NO_RECIBO from Ope_Cor_Caj_Recibos_Cobros WHERE CODIGO_BARRAS = '{0}')) as subtotalGlobal", Datos.No_Folio);
                //}
                //Mi_SQL += "	FROM Ope_Cor_Caj_Recibos_Cobros r";
                //Mi_SQL += "	JOIN Ope_Cor_Caj_Movimientos_Cobros_Espejo m ON r.NO_RECIBO = m.NO_RECIBO";
                //Mi_SQL += "	JOIN Cat_Cor_Conceptos_Cobros c ON m.CONCEPTO_ID = c.CONCEPTO_ID";
                //Mi_SQL += "	WHERE (estado_recibo = 'ACTIVO' OR Estado_Recibo='PORAPLICAR')";
                //Mi_SQL += " AND c.Facturable = 'SI'";
                //if (!String.IsNullOrEmpty(Datos.No_Folio))
                //    Mi_SQL += " AND CODIGO_BARRAS = '" + Datos.No_Folio + "'";
                //Mi_SQL += " AND round(importe, 2) <> 0";
                //Mi_SQL += "	GROUP BY m.Concepto_ID";
                //Mi_SQL += "		,c.Nombre";
                //Mi_SQL += "     , c.Orden";
                //Mi_SQL += "		,r.CODIGO_BARRAS";
                //Mi_SQL += "		,r.RPU";
                ////Mi_SQL += "		,m.NO_MOVIMIENTO";
                //Mi_SQL += "		,c.impuesto_id";
                //Mi_SQL += "		,r.Periodo";
                //Mi_SQL += "		,r.total_iva_cobrado,m.impuesto";
                ////Mi_SQL += "         Order By c.Orden";
                //Mi_SQL += " HAVING sum(m.IMPORTE) <> 0 Order By m.CONCEPTO_ID";



                //Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                ////Dt_Datos = SqlHelper.ExecuteDataset("Data Source=187.174.176.231;Initial Catalog=Simapag;User ID=dbcajas;Password=TellerMachine01", CommandType.Text, Mi_SQL).Tables[0];

                //if (Dt_Datos.Rows.Count > 0)
                //{
                //    Mi_SQL = "select Concepto_Iva_ID from Cat_Cor_Costos_Factores where Año='" + ((DateTime)Dt_Datos.Rows[0]["FECHA_MOVIMIENTO"]).Year + "'";
                //    ID_Iva = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).ToString();
                //    if (!String.IsNullOrEmpty(ID_Iva))
                //    {
                //        for (int Indice = 0; Indice < Dt_Datos.Rows.Count; Indice++)
                //        {

                //            if (Dt_Datos.Rows[Indice]["clave_concepto"].ToString().Equals(ID_Iva))
                //            {
                //                Importe_Iva = Convert.ToDecimal(Dt_Datos.Rows[Indice]["importe"].ToString());
                //                Importe_Iva = Importe_Iva - (Importe_Iva % 0.01M);

                //                Dt_Datos.Rows.RemoveAt(Indice);
                //                //Dt_Datos.Rows[Dt_Datos.Rows.Count - 1]["iva"] = Importe_Iva;

                //                //Decimal totalGlobal = Convert.ToDecimal(Dt_Datos.Rows[Dt_Datos.Rows.Count - 1]["totalGlobal"]);
                //                //Dt_Datos.Rows[Dt_Datos.Rows.Count - 1]["impuestoGlobal"] = Importe_Iva;

                //                //Dt_Datos.Rows[Dt_Datos.Rows.Count - 1]["subtotalGlobal"] = totalGlobal - Importe_Iva;
                //                break;
                //            }
                //        }
                //    }
                //}
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Resultado;

        }
        /// <summary>
        /// Inserta una factura para obtener el consecutivo
        /// </summary>
        /// <param name="negocio">Clase facturacion negocio.</param>
        /// <param name="Obj_Conexion">The object conexion.</param>
        /// <usuario_creo>Jesus Toledo</usuario_creo>
        /// <fecha_creo>8/10/2020</fecha_creo>
        /// <returns>System.String</returns>
        public static Int64 InsertarFacturaParaObtenerElConsecutivo(Cls_Ope_Facturacion_Negocio negocio, SqlConnection Obj_Conexion)
        {
            Int64 intRespuesta = 0;
            string Mi_SQL = "";
            try
            {
                using (SqlCommand Obj_Comando = Obj_Conexion.CreateCommand())
                {
                    Mi_SQL = "INSERT INTO OPE_FACTURAS_CONSECUTIVOS DEFAULT VALUES; SET @no_factura = SCOPE_IDENTITY();";

                    SqlParameter parametro_no_recibo = new SqlParameter("@no_factura", SqlDbType.Int);
                    parametro_no_recibo.Direction = ParameterDirection.Output;
                    Obj_Comando.Parameters.Add(parametro_no_recibo);
                    Obj_Comando.CommandText = Mi_SQL;
                    Obj_Comando.ExecuteNonQuery();
                    intRespuesta = Convert.ToInt64(parametro_no_recibo.Value);
                    String temp = ("0000000000" + intRespuesta.ToString());

                    Mi_SQL = "INSERT INTO OPE_FACTURAS (NO_FACTURA,SERIE,FOLIO_PAGO,FECHA_CREO,NUMERO_EXTERIOR,ESTATUS, PAGADA, CANCELADA) VALUES(  @consecutivo,@serie,@codigo_barras,getdate(),'0','ACTIVA','NO','NO'  ) ";
                    Obj_Comando.Parameters.Add("@consecutivo", SqlDbType.VarChar).Value = temp.Substring(temp.Length - 10, 10); ;
                    Obj_Comando.Parameters.Add("@serie", SqlDbType.VarChar).Value = negocio.P_Serie;
                    Obj_Comando.Parameters.Add("@codigo_barras", SqlDbType.VarChar).Value = negocio.No_Folio;
                    Obj_Comando.CommandText = Mi_SQL;
                    Obj_Comando.ExecuteNonQuery();
                }
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            
            return intRespuesta;
        }

        public DataTable Consultar_Detalles_Pago_Facturado(Cls_Ope_Facturacion_Negocio Datos)
        {
            DataTable Dt_Datos = null;
            String Mi_SQL;
            String ID_Iva;
            Double Importe_Iva = 0.0;
            try
            {
                //Mi_SQL = "SELECT r.CODIGO_BARRAS AS [codigo_barras] " +
                //           ",r.No_Cuenta AS [no_cuenta] " +
                //           ",m.CONCEPTO_ID AS [clave_concepto] " +
                //           ",c.Nombre AS [descripcion] " +
                //           ",1 AS [valor_unitario] " +
                //           ",sum(m.IMPORTE) AS [importe] " +
                //           ",0.0 AS [iva] " +
                //           ",sum(m.IMPORTE) AS [total] " +
                //           ",max(m.FECHA_MOVIMIENTO) as [FECHA_MOVIMIENTO] " +
                //           ",'SERVICIOS' as Unidad_de_Medida ,c.lleva_iva, (select isnull(Porcentaje_IVA,0) from Cat_Cor_Parametros where Parametro_ID='00001') as Porcentaje_IVA " +
                //           ",r.Periodo as Periodo_Pago," +
                //           "case substring(r.CODIGO_BARRAS,LEN(r.CODIGO_BARRAS),LEN(r.CODIGO_BARRAS)) " +
                //           "WHEN 'D' THEN CASE r.No_Cuenta WHEN 0 then case r.total_iva_cobrado WHEN 0 THEN 'DOMESTICO' ELSE 'NO DOMESTICO' END " +
                //            "else (select g.Nombre_Giro from Cat_Cor_Predios p JOIN Cat_Cor_Tarifas t ON t.Tarifa_ID = p.Tarifa_ID " +
                //            "JOIN Cat_Cor_Giros g ON g.GIRO_ID = t.Giro_ID  and p.No_Cuenta = r.No_Cuenta ) end " +
                //            "ELSE (select g.Nombre_Giro from Cat_Cor_Predios p JOIN Cat_Cor_Tarifas t ON t.Tarifa_ID = p.Tarifa_ID " +
                //            "JOIN Cat_Cor_Giros g ON g.GIRO_ID = t.Giro_ID and p.No_Cuenta = r.No_Cuenta ) END as Tipo_Servicio " +
                //        "FROM Ope_Cor_Caj_Recibos_Cobros r " +
                //        "JOIN Ope_Cor_Caj_Movimientos_Cobros_Espejo m ON r.NO_RECIBO = m.NO_RECIBO " +
                //        "JOIN Cat_Cor_Conceptos_Cobros c ON m.CONCEPTO_ID = c.CONCEPTO_ID " +
                //        "WHERE estado_recibo = 'ACTIVO' ";

                //if (!String.IsNullOrEmpty(Datos.No_Folio))
                //    Mi_SQL += " AND CODIGO_BARRAS = '" + Datos.No_Folio + "' ";
                //Mi_SQL += "GROUP by m.Concepto_ID,c.Nombre,r.CODIGO_BARRAS,r.No_Cuenta,m.NO_MOVIMIENTO,c.lleva_iva,r.Periodo,r.total_iva_cobrado";

                Mi_SQL = " select 1 AS [valor_unitario], 'SERVICIOS' AS Unidad_de_Medida, codigo AS [clave_concepto], " +
                    "DESCRIPCION AS[descripcion], total AS[importe], iva AS[iva], porcentaje_impuesto AS Porcentaje_IVA, " +
                    "getdate() AS[FECHA_MOVIMIENTO], total AS [total]";


                if (!String.IsNullOrEmpty(Datos.No_Folio))
                    Mi_SQL += " ,(SELECT TOTAL from OPE_FACTURAS WHERE NO_FACTURA = '{0}') as totalGlobal ,(SELECT IVA from OPE_FACTURAS WHERE NO_FACTURA = '{0}') as impuestoGlobal ,(SELECT SUBTOTAL from OPE_FACTURAS WHERE NO_FACTURA = '{0}') as subtotalGlobal ";

                Mi_SQL += "  from OPE_FACTURAS_DETALLES ";

                if (!String.IsNullOrEmpty(Datos.No_Folio))
                    Mi_SQL += " where NO_FACTURA = '{0}'";

                Mi_SQL = String.Format(Mi_SQL, Datos.P_No_Factura);

                //Mi_SQL = " select 1 AS [valor_unitario], 'SERVICIOS' AS Unidad_de_Medida, codigo AS [clave_concepto], " +
                //    "DESCRIPCION AS[descripcion], total AS[importe], iva AS[iva], porcentaje_impuesto AS Porcentaje_IVA, " +
                //    "getdate() AS[FECHA_MOVIMIENTO], total AS [total] ";
                //Mi_SQL += "	from OPE_FACTURAS_DETALLES";
                //if (!String.IsNullOrEmpty(Datos.No_Folio))
                //    Mi_SQL += " where NO_FACTURA = '" + Datos.P_No_Factura + "'";

                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }

        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Modificar_Datos_Cliente()
        //DESCRIPCIÓN:      modifica los datos de los clientes
        //PARÁMETROS:       Cls_Ope_Facturacion_Negocio: Datos para realizar la busqueda
        //CREO:             Ramón Baeza Yépez
        //FECHA_CREO:       05/12/2013
        //*******************************************************************************************************
        public void Modificar_Datos_Cliente(Cls_Ope_Facturacion_Negocio Datos)
        {
            //Declaración de las variables
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            String Mi_SQL;
            try
            {
                //Query para modificar un Banco de acuerdo a su ID
                Mi_SQL = "UPDATE CAT_ADM_CLIENTES set ";
                Mi_SQL += "NOMBRE_CORTO = '" + Datos.Nombre + "', ";
                Mi_SQL += "RAZON_SOCIAL = '" + Datos.Nombre + "', ";
                Mi_SQL += "Rfc = '" + Datos.Rfc + "', ";
                Mi_SQL += "Curp = '" + Datos.Curp + "', ";
                Mi_SQL += "Pais = '" + Datos.Pais + "', ";
                Mi_SQL += "Estado = '" + Datos.Estado_ID + "', ";
                Mi_SQL += "Ciudad = '" + Datos.Ciudad + "', ";
                Mi_SQL += "Localidad = '" + Datos.Localidad + "', ";
                Mi_SQL += "Colonia = '" + Datos.Colonia + "', ";
                Mi_SQL += "Calle = '" + Datos.Calle + "', ";
                Mi_SQL += "Numero_Exterior = '" + Datos.Numero_Exterior + "', ";
                Mi_SQL += "Numero_Interior = '" + Datos.Numero_Interior + "', ";
                Mi_SQL += "CP = '" + Datos.Codigo_Postal + "', ";
                //Mi_SQL += "Fax = '" + Datos.Fax + "', ";
                //Mi_SQL += "Telefono_1 = '" + Datos.Telefono_1 + "', ";
                //Mi_SQL += "Telefono_2 = '" + Datos.Telefono_2 + "', ";
                //Mi_SQL += "Extension = '" + Datos.Extension + "', ";
                //Mi_SQL += "Nextel = '" + Datos.Nextel + "', ";
                Mi_SQL += "Email = '" + Datos.Email + "' ";
                //Mi_SQL += "Sitio_Web = '" + Datos.Sitio_Web + "' ";
                Mi_SQL += " Where Cliente_ID = '" + Datos.Cliente_ID + "' ";

                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                Mi_SQL += "UPDATE Ope_Cor_Diversos SET RFC='" + Datos.Rfc + "' WHERE Codigo_Barras='" + Datos.No_Folio + "'";

                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex) { throw new Exception("Error: " + Ex.Message); }
            catch (DBConcurrencyException Ex) { throw new Exception("Error: " + Ex.Message); }
            catch (Exception Ex) { throw new Exception("Error: " + Ex.Message); }
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_Estados()
        //DESCRIPCIÓN:      consulta los estados
        //PARÁMETROS:       Cls_Ope_Facturacion_Negocio: Datos para realizar la busqueda
        //CREO:             Ramón Baeza Yépez
        //FECHA_CREO:       05/12/2013
        //*******************************************************************************************************
        public DataTable Consultar_Estados(Cls_Ope_Facturacion_Negocio Datos)
        {
            String Mi_SQL;
            DataTable Dt_Datos = new DataTable();
            try
            {
                //Query para buscar una Región de acuerdo a su número de región
                Mi_SQL = "select * from CAT_COR_ESTADOS";

                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_Datos_Fiscales()
        //DESCRIPCIÓN:      consulta los estados
        //PARÁMETROS:       Cls_Ope_Facturacion_Negocio: Datos para realizar la busqueda
        //CREO:             Ramón Baeza Yépez
        //FECHA_CREO:       05/12/2013
        //*******************************************************************************************************
        public DataTable Consultar_Datos_Fiscales(Cls_Ope_Facturacion_Negocio Datos)
        {
            String Mi_SQL;
            DataTable Dt_Datos = new DataTable();
            try
            {
                //Query para buscar una Región de acuerdo a su número de región
                Mi_SQL = "select * from CAT_EMPRESAS";

                if (!String.IsNullOrEmpty(Datos.Empresa_ID))
                {
                    Mi_SQL += " WHERE Empresa_ID = '" + Datos.Empresa_ID + "'";
                }

                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }

        public static Boolean Alta_Factura(Cls_Ope_Facturacion_Negocio P_Facturas, DataTable Dt_Detalle_Factura, SqlConnection objConexion)
        {
            Boolean Alta = false;
            StringBuilder Mi_sql = new StringBuilder();

            try
            {
                Mi_sql.Append("UPDATE OPE_FACTURAS SET");

                //if (!String.IsNullOrEmpty(P_Facturas.P_No_Factura)) Mi_sql.Append("'" + P_Facturas.P_No_Factura + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Serie)) Mi_sql.Append(" SERIE = '" + P_Facturas.P_Serie + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Cliente_Id)) Mi_sql.Append("CLIENTE_ID = '" + P_Facturas.P_Cliente_Id + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.No_Folio)) Mi_sql.Append("FOLIO_PAGO = '" + P_Facturas.No_Folio + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Email_Facturacion)) Mi_sql.Append("EMAIL='" + P_Facturas.P_Email_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Razon_Social_Facturacion)) Mi_sql.Append("RAZON_SOCIAL = '" + P_Facturas.P_Razon_Social_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Rfc_Facturacion)) Mi_sql.Append("RFC = '" + P_Facturas.P_Rfc_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Pais_Facturacion)) Mi_sql.Append("PAIS = '" + P_Facturas.P_Pais_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Estado_Facturacion)) Mi_sql.Append("ESTADO = '" + P_Facturas.P_Estado_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Localidad_Facturacion)) Mi_sql.Append("LOCALIDAD = '" + P_Facturas.P_Localidad_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Colonia_Facturacion)) Mi_sql.Append("COLONIA = '" + P_Facturas.P_Colonia_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Ciudad_Facturacion)) Mi_sql.Append("CIUDAD = '" + P_Facturas.P_Ciudad_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Calle_Facturacion)) Mi_sql.Append("CALLE = '" + P_Facturas.P_Calle_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Numero_Exterior_Facturacion)) Mi_sql.Append("NUMERO_EXTERIOR = '" + P_Facturas.P_Numero_Exterior_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Numero_Interior_Facturacion)) Mi_sql.Append("NUMERO_INTERIOR = '" + P_Facturas.P_Numero_Interior_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Cp)) Mi_sql.Append("CP = '" + P_Facturas.P_Cp + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Forma_Pago)) Mi_sql.Append("FORMA_PAGO = '" + P_Facturas.P_Forma_Pago + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Condiciones)) Mi_sql.Append("CONDICIONES = '" + P_Facturas.P_Condiciones + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_No_Cuenta_Pago)) Mi_sql.Append("NO_CUENTA_PAGO = '" + P_Facturas.P_No_Cuenta_Pago + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Metodo_Pago)) Mi_sql.Append("METODO_PAGO = '" + P_Facturas.P_Metodo_Pago + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Banco_Pago)) Mi_sql.Append("BANCO_PAGO = '" + P_Facturas.P_Banco_Pago + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Emision)) Mi_sql.Append("FECHA_EMISION = '" + String.Format("{0:yyyyMMdd HH:mm:ss}", Convert.ToDateTime(P_Facturas.P_Fecha_Emision)) + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Vencimiento)) Mi_sql.Append("FECHA_VENCIMIENTO = '" + String.Format("{0:yyyyMMdd HH:mm:ss}", Convert.ToDateTime(P_Facturas.P_Fecha_Vencimiento)) + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Pago)) Mi_sql.Append("FECHA_PAGO = '" + String.Format("{0:yyyyMMdd HH:mm:ss}", Convert.ToDateTime(P_Facturas.P_Fecha_Pago)) + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Tipo_Factura)) Mi_sql.Append("TIPO_FACTURA = '" + P_Facturas.P_Tipo_Factura + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Tipo_Moneda)) Mi_sql.Append("TIPO_MONEDA = '" + P_Facturas.P_Tipo_Moneda + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Tipo_Cambio)) Mi_sql.Append("TIPO_CAMBIO = " + P_Facturas.P_Tipo_Cambio + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Subtotal)) Mi_sql.Append("SUBTOTAL = " + P_Facturas.P_Subtotal + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Subtotal_Cero)) Mi_sql.Append("SUBTOTAL_CERO = " + P_Facturas.P_Subtotal_Cero + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Retencion_Cedular)) Mi_sql.Append("RETENCION_CEDULAR = " + P_Facturas.P_Retencion_Cedular + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Retencion_Isr)) Mi_sql.Append("RETENCION_ISR = " + P_Facturas.P_Retencion_Isr + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Retencion_Iva)) Mi_sql.Append("RETENCION_IVA = " + P_Facturas.P_Retencion_Iva + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Retencion_Flete)) Mi_sql.Append("RETENCION_FLETE = " + P_Facturas.P_Retencion_Flete + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Iva)) Mi_sql.Append("IVA = " + P_Facturas.P_Iva + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Descuento)) Mi_sql.Append("DESCUENTO = " + P_Facturas.P_Descuento + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Total)) Mi_sql.Append("TOTAL = " + P_Facturas.P_Total + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Saldo)) Mi_sql.Append("SALDO = " + P_Facturas.P_Saldo + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Abono)) Mi_sql.Append("ABONO = " + P_Facturas.P_Abono + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Pagada)) Mi_sql.Append("PAGADA = '" + P_Facturas.P_Pagada + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Cancelada)) Mi_sql.Append("CANCELADA = '" + P_Facturas.P_Cancelada + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Usuario_Cancelacion)) Mi_sql.Append("USUARIO_CANCELACION = '" + P_Facturas.P_Usuario_Cancelacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Cancelacion)) Mi_sql.Append("FECHA_CANCELACION = '" + P_Facturas.P_Fecha_Cancelacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Motivo_Cancelacion)) Mi_sql.Append("MOTIVO_CANCELACION = '" + P_Facturas.P_Motivo_Cancelacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Comentarios)) Mi_sql.Append("COMENTARIOS = '" + P_Facturas.P_Comentarios + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Certificado)) Mi_sql.Append("CERTIFICADO = '" + P_Facturas.P_Certificado + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_No_Certificado)) Mi_sql.Append("NO_CERTIFICADO = '" + P_Facturas.P_No_Certificado + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_No_Autorizacion)) Mi_sql.Append("NO_AUTORIZACION = '" + P_Facturas.P_No_Autorizacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Anio_Autorizacion)) Mi_sql.Append("ANIO_AUTORIZACION = " + P_Facturas.P_Anio_Autorizacion + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Genero_Informe)) Mi_sql.Append("GENERO_INFORME = '" + P_Facturas.P_Genero_Informe + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Informe_Sat)) Mi_sql.Append("FECHA_INFORME_SAT = '" + P_Facturas.P_Fecha_Informe_Sat + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Usuario_Informe_Sat)) Mi_sql.Append("USUARIO_INFORME_SAT = '" + P_Facturas.P_Usuario_Informe_Sat + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Creo_Xml)) Mi_sql.Append("FECHA_CREO_XML = '" + String.Format("{0:yyyyMMdd HH:mm:ss}", Convert.ToDateTime(P_Facturas.P_Fecha_Creo_Xml)) + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Usuario_Autorizo_Regenerar)) Mi_sql.Append("USUARIO_AUTORIZO_REGENERAR = '" + P_Facturas.P_Usuario_Autorizo_Regenerar + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Autorizo_Regenerar)) Mi_sql.Append("FECHA_AUTORIZO_REGENERAR = '" + P_Facturas.P_Fecha_Autorizo_Regenerar + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Usuario_Autorizo_Informe)) Mi_sql.Append("USUARIO_AUTORIZO_INFORME = '" + P_Facturas.P_Usuario_Autorizo_Informe + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Autorizo_Informe)) Mi_sql.Append("FECHA_AUTORIZO_INFORME = '" + P_Facturas.P_Fecha_Autorizo_Informe + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Timbre_Version)) Mi_sql.Append("TIMBRE_VERSION = '" + P_Facturas.P_Timbre_Version + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Timbre_Uuid)) Mi_sql.Append("TIMBRE_UUID = '" + P_Facturas.P_Timbre_Uuid + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Timbre_Fecha_Timbrado)) Mi_sql.Append("TIMBRE_FECHA_TIMBRADO = '" + String.Format("{0:yyyyMMdd HH:mm:ss}", Convert.ToDateTime(P_Facturas.P_Timbre_Fecha_Timbrado)) + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Timbre_Sello_Cfd)) Mi_sql.Append("TIMBRE_SELLO_CFD = '" + P_Facturas.P_Timbre_Sello_Cfd + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Timbre_No_Certificado_Sat)) Mi_sql.Append("TIMBRE_NO_CERTIFICADO_SAT = '" + P_Facturas.P_Timbre_No_Certificado_Sat + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Timbre_Sello_Sat)) Mi_sql.Append("TIMBRE_SELLO_SAT = '" + P_Facturas.P_Timbre_Sello_Sat + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Ruta_Codigo_Bd)) Mi_sql.Append("RUTA_CODIGO_BD = '" + P_Facturas.P_Ruta_Codigo_Bd + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Ruta_Codigo_Bd)) Mi_sql.Append( string.IsNullOrEmpty(P_Facturas.P_Usuario_Creo) ? "USUARIO_CREO = NULL, " : (" USUARIO_CREO = '" + P_Facturas.P_Usuario_Creo + "', "));
                if (!String.IsNullOrEmpty(P_Facturas.Curp)) Mi_sql.Append("CURP = '" + P_Facturas.Curp + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.Fax)) Mi_sql.Append("FAX = '" + P_Facturas.Fax + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.Telefono_1)) Mi_sql.Append("TELEFONO_1 = '" + P_Facturas.Telefono_1 + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.Telefono_2)) Mi_sql.Append("TELEFONO_2 = '" + P_Facturas.Telefono_2 + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.Extension)) Mi_sql.Append("EXTENSION = '" + P_Facturas.Extension + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.Nextel)) Mi_sql.Append("NEXTEL = '" + P_Facturas.Nextel + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.Sitio_Web)) Mi_sql.Append("SITIO_WEB = '" + P_Facturas.Sitio_Web + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Estatus)) Mi_sql.Append("ESTATUS = '" + P_Facturas.P_Estatus + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_No_Cuenta)) Mi_sql.Append("NO_CUENTA = '" + P_Facturas.P_No_Cuenta + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Rpu)) Mi_sql.Append("RPU = '" + P_Facturas.P_Rpu + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Periodo_Pago)) Mi_sql.Append("PERIODO_PAGO = '" + P_Facturas.P_Periodo_Pago + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Tipo_Servicio)) Mi_sql.Append("TIPO_SERVICIO = '" + P_Facturas.P_Tipo_Servicio + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Cp_Lugar_Expedicion)) Mi_sql.Append("codigo_postal_expedicion = '" + P_Facturas.P_Cp_Lugar_Expedicion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Cp_Lugar_Expedicion)) Mi_sql.Append("uso_cfdi = '" + P_Facturas.UsoCFDI + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Cadena_Original)) Mi_sql.Append("CADENA_ORIGINAL = '" + P_Facturas.P_Cadena_Original + "'");
                Mi_sql.Append(" WHERE NO_FACTURA = '" + P_Facturas.P_No_Factura + "'");

                using (SqlCommand Obj_Comando = objConexion.CreateCommand())
                {
                    Obj_Comando.CommandText = Mi_sql.ToString();
                    Obj_Comando.ExecuteNonQuery();
                }

                // Da de alta los detalles de la factura.
                Cls_Ope_Facturas_Detalles_Negocio Rs_Detalle_Factura = new Cls_Ope_Facturas_Detalles_Negocio();
                Rs_Detalle_Factura.P_No_Factura = P_Facturas.P_No_Factura;
                Rs_Detalle_Factura.P_Serie = P_Facturas.P_Serie;

                foreach (DataRow Dr_Fila in Dt_Detalle_Factura.Rows)
                {
                    Rs_Detalle_Factura.P_Cantidad = Dr_Fila["Valor_Unitario"].ToString();
                    Rs_Detalle_Factura.P_Codigo = Dr_Fila["Clave_Concepto"].ToString();
                    //Rs_Detalle_Factura.P_No_Recibo = Dr_Fila["No_Recibo"].ToString();
                    Rs_Detalle_Factura.P_Descripcion = Dr_Fila[Ope_Facturas_Detalle.Campo_Descripcion].ToString();
                    Rs_Detalle_Factura.P_Unidad = Dr_Fila["Unidad_de_Medida"].ToString();
                    Rs_Detalle_Factura.P_Subtotal = Regex.Replace(Dr_Fila["Importe"].ToString(), @"[$,]", "");
                    Rs_Detalle_Factura.P_Precio = Regex.Replace(Dr_Fila["Importe"].ToString(), @"[$,]", "");
                    Rs_Detalle_Factura.P_Porcentaje = String.Format("{0:#0.0000}", Convert.ToDouble(Dr_Fila["tasa"].ToString()) * 100);
                    Rs_Detalle_Factura.P_Iva = Regex.Replace(Dr_Fila["ivaCorrecto"].ToString(), @"[$,]", "");
                    Rs_Detalle_Factura.P_Total = Regex.Replace(Dr_Fila[Ope_Facturas_Detalle.Campo_Total].ToString(), @"[$,]", "");
                    Rs_Detalle_Factura.Alta_Detalle_Factura(objConexion);
                }

                Mi_sql = new StringBuilder();
                Mi_sql.Append("UPDATE Ope_Cor_Caj_Recibos_Cobros set ");
                Mi_sql.Append("NO_FACTURA = '" + P_Facturas.P_No_Factura + "' ");
                Mi_sql.Append("Where CODIGO_BARRAS = '" + P_Facturas.No_Folio + "' ");
                using (SqlCommand Obj_Comando = objConexion.CreateCommand())
                {
                    Obj_Comando.CommandText = Mi_sql.ToString();
                    Obj_Comando.ExecuteNonQuery();
                }

                Alta = true;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Alta;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Factura
        ///DESCRIPCIÓN: Da de alta en la Base de Datos una nueva Factura
        ///PARAMENTROS:     
        ///             1. P_Facturas.      Instancia de la Clase de Negocio de Facturas 
        ///                                 con los datos del que van a ser
        ///                                 dados de Alta.
        ///CREO: Luis Alberto Salas García
        ///FECHA_CREO: 8 Abril 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static Boolean Alta_Factura_Deprecated(Cls_Ope_Facturacion_Negocio P_Facturas, DataTable Dt_Detalle_Factura)
        {
            Boolean Alta = false;
            StringBuilder Mi_sql = new StringBuilder();
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;

            try
            {
                Mi_sql.Append("INSERT INTO " + Ope_Facturas.Tabla_Ope_Facturas + "(");

                if (!String.IsNullOrEmpty(P_Facturas.P_No_Factura)) Mi_sql.Append(Ope_Facturas.Campo_No_Factura + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Serie)) Mi_sql.Append(Ope_Facturas.Campo_Serie + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Cliente_Id)) Mi_sql.Append(Ope_Facturas.Campo_Cliente_Id + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.No_Folio)) Mi_sql.Append(Ope_Facturas.Campo_Folio_Pago + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Email_Facturacion)) Mi_sql.Append(Ope_Facturas.Campo_Email + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Razon_Social_Facturacion)) Mi_sql.Append(Ope_Facturas.Campo_Razon_Social + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Rfc_Facturacion)) Mi_sql.Append(Ope_Facturas.Campo_Rfc + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Pais_Facturacion)) Mi_sql.Append(Ope_Facturas.Campo_Pais + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Estado_Facturacion)) Mi_sql.Append(Ope_Facturas.Campo_Estado + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Localidad_Facturacion)) Mi_sql.Append(Ope_Facturas.Campo_Localidad + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Colonia_Facturacion)) Mi_sql.Append(Ope_Facturas.Campo_Colonia + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Ciudad_Facturacion)) Mi_sql.Append(Ope_Facturas.Campo_Ciudad + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Calle_Facturacion)) Mi_sql.Append(Ope_Facturas.Campo_Calle + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Numero_Exterior_Facturacion)) Mi_sql.Append(Ope_Facturas.Campo_Numero_Exterior + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Numero_Interior_Facturacion)) Mi_sql.Append(Ope_Facturas.Campo_Numero_Interior + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Cp)) Mi_sql.Append(Ope_Facturas.Campo_Cp + ", ");
                //if (!String.IsNullOrEmpty(P_Facturas.P_Dias_Credito)) Mi_sql.Append(Ope_Facturas.Campo_Dias_Credito + ", ");
                //if (!String.IsNullOrEmpty(P_Facturas.P_Porcentaje_Descuento)) Mi_sql.Append(Ope_Facturas.Campo_Porcentaje_Descuento + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Orden_Compra)) Mi_sql.Append(Ope_Facturas.Campo_Orden_Compra + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Forma_Pago)) Mi_sql.Append(Ope_Facturas.Campo_Forma_Pago + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Condiciones)) Mi_sql.Append(Ope_Facturas.Campo_Condiciones + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_No_Cuenta_Pago)) Mi_sql.Append(Ope_Facturas.Campo_No_Cuenta_Pago + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Metodo_Pago)) Mi_sql.Append(Ope_Facturas.Campo_Metodo_Pago + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Banco_Pago)) Mi_sql.Append(Ope_Facturas.Campo_Banco_Pago + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Emision)) Mi_sql.Append(Ope_Facturas.Campo_Fecha_Emision + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Vencimiento)) Mi_sql.Append(Ope_Facturas.Campo_Fecha_Vencimiento + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Pago)) Mi_sql.Append(Ope_Facturas.Campo_Fecha_Pago + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Tipo_Factura)) Mi_sql.Append(Ope_Facturas.Campo_Tipo_Factura + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Tipo_Moneda)) Mi_sql.Append(Ope_Facturas.Campo_Tipo_Moneda + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Tipo_Cambio)) Mi_sql.Append(Ope_Facturas.Campo_Tipo_Cambio + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Subtotal)) Mi_sql.Append(Ope_Facturas.Campo_Subtotal + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Subtotal_Cero)) Mi_sql.Append(Ope_Facturas.Campo_Subtotal_Cero + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Retencion_Cedular)) Mi_sql.Append(Ope_Facturas.Campo_Retencion_Cedular + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Retencion_Isr)) Mi_sql.Append(Ope_Facturas.Campo_Retencion_Isr + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Retencion_Iva)) Mi_sql.Append(Ope_Facturas.Campo_Retencion_Iva + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Retencion_Flete)) Mi_sql.Append(Ope_Facturas.Campo_Retencion_Flete + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Iva)) Mi_sql.Append(Ope_Facturas.Campo_Iva + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Descuento)) Mi_sql.Append(Ope_Facturas.Campo_Descuento + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Total)) Mi_sql.Append(Ope_Facturas.Campo_Total + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Saldo)) Mi_sql.Append(Ope_Facturas.Campo_Saldo + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Abono)) Mi_sql.Append(Ope_Facturas.Campo_Abono + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Pagada)) Mi_sql.Append(Ope_Facturas.Campo_Pagada + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Cancelada)) Mi_sql.Append(Ope_Facturas.Campo_Cancelada + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Usuario_Cancelacion)) Mi_sql.Append(Ope_Facturas.Campo_Usuario_Cancelacion + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Cancelacion)) Mi_sql.Append(Ope_Facturas.Campo_Fecha_Cancelacion + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Motivo_Cancelacion)) Mi_sql.Append(Ope_Facturas.Campo_Motivo_Cancelacion + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Comentarios)) Mi_sql.Append(Ope_Facturas.Campo_Comentarios + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Certificado)) Mi_sql.Append(Ope_Facturas.Campo_Certificado + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_No_Certificado)) Mi_sql.Append(Ope_Facturas.Campo_No_Certificado + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_No_Autorizacion)) Mi_sql.Append(Ope_Facturas.Campo_No_Autorizacion + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Anio_Autorizacion)) Mi_sql.Append(Ope_Facturas.Campo_Anio_Autorizacion + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Genero_Informe)) Mi_sql.Append(Ope_Facturas.Campo_Genero_Informe + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Informe_Sat)) Mi_sql.Append(Ope_Facturas.Campo_Fecha_Informe_Sat + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Usuario_Informe_Sat)) Mi_sql.Append(Ope_Facturas.Campo_Usuario_Informe_Sat + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Creo_Xml)) Mi_sql.Append(Ope_Facturas.Campo_Fecha_Creo_Xml + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Usuario_Autorizo_Regenerar)) Mi_sql.Append(Ope_Facturas.Campo_Usuario_Autorizo_Regenerar + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Autorizo_Regenerar)) Mi_sql.Append(Ope_Facturas.Campo_Fecha_Autorizo_Regenerar + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Usuario_Autorizo_Informe)) Mi_sql.Append(Ope_Facturas.Campo_Usuario_Autorizo_Informe + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Autorizo_Informe)) Mi_sql.Append(Ope_Facturas.Campo_Fecha_Autorizo_Informe + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Timbre_Version)) Mi_sql.Append(Ope_Facturas.Campo_Timbre_Version + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Timbre_Uuid)) Mi_sql.Append(Ope_Facturas.Campo_Timbre_Uuid + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Timbre_Fecha_Timbrado)) Mi_sql.Append(Ope_Facturas.Campo_Timbre_Fecha_Timbrado + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Timbre_Sello_Cfd)) Mi_sql.Append(Ope_Facturas.Campo_Timbre_Sello_Cfd + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Timbre_No_Certificado_Sat)) Mi_sql.Append(Ope_Facturas.Campo_Timbre_No_Certificado_Sat + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Timbre_Sello_Sat)) Mi_sql.Append(Ope_Facturas.Campo_Timbre_Sello_Sat + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Ruta_Codigo_Bd)) Mi_sql.Append(Ope_Facturas.Campo_Ruta_Codigo_Bd + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Ruta_Codigo_Bd)) Mi_sql.Append(Ope_Facturas.Campo_Usuario_Creo + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.Curp)) Mi_sql.Append(Ope_Facturas.Campo_Curp + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.Fax)) Mi_sql.Append(Ope_Facturas.Campo_Fax + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.Telefono_1)) Mi_sql.Append(Ope_Facturas.Campo_Telefono_1 + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.Telefono_2)) Mi_sql.Append(Ope_Facturas.Campo_Telefono_2 + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.Extension)) Mi_sql.Append(Ope_Facturas.Campo_Extension + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.Nextel)) Mi_sql.Append(Ope_Facturas.Campo_Nextel + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.Sitio_Web)) Mi_sql.Append(Ope_Facturas.Campo_Sitio_Web + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Estatus)) Mi_sql.Append(Ope_Facturas.Campo_Estatus + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_No_Cuenta)) Mi_sql.Append(Ope_Facturas.Campo_No_Cuenta + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Rpu)) Mi_sql.Append(Ope_Facturas.Campo_Rpu + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Periodo_Pago)) Mi_sql.Append(Ope_Facturas.Campo_Periodo_Pago + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Tipo_Servicio)) Mi_sql.Append(Ope_Facturas.Campo_Tipo_Servicio + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Cp_Lugar_Expedicion)) Mi_sql.Append("codigo_postal_expedicion, ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Cp_Lugar_Expedicion)) Mi_sql.Append("uso_cfdi, ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Cadena_Original)) Mi_sql.Append(Ope_Facturas.Campo_cadena_Original);
                Mi_sql.Append(") VALUES (");
                if (!String.IsNullOrEmpty(P_Facturas.P_No_Factura)) Mi_sql.Append("'" + P_Facturas.P_No_Factura + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Serie)) Mi_sql.Append("'" + P_Facturas.P_Serie + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Cliente_Id)) Mi_sql.Append("'" + P_Facturas.P_Cliente_Id + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.No_Folio)) Mi_sql.Append("'" + P_Facturas.No_Folio + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Email_Facturacion)) Mi_sql.Append("'" + P_Facturas.P_Email_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Razon_Social_Facturacion)) Mi_sql.Append("'" + P_Facturas.P_Razon_Social_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Rfc_Facturacion)) Mi_sql.Append("'" + P_Facturas.P_Rfc_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Pais_Facturacion)) Mi_sql.Append("'" + P_Facturas.P_Pais_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Estado_Facturacion)) Mi_sql.Append("'" + P_Facturas.P_Estado_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Localidad_Facturacion)) Mi_sql.Append("'" + P_Facturas.P_Localidad_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Colonia_Facturacion)) Mi_sql.Append("'" + P_Facturas.P_Colonia_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Ciudad_Facturacion)) Mi_sql.Append("'" + P_Facturas.P_Ciudad_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Calle_Facturacion)) Mi_sql.Append("'" + P_Facturas.P_Calle_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Numero_Exterior_Facturacion)) Mi_sql.Append("'" + P_Facturas.P_Numero_Exterior_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Numero_Interior_Facturacion)) Mi_sql.Append("'" + P_Facturas.P_Numero_Interior_Facturacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Cp)) Mi_sql.Append("'" + P_Facturas.P_Cp + "', ");
                //if (!String.IsNullOrEmpty(P_Facturas.P_Dias_Credito)) Mi_sql.Append(P_Facturas.P_Dias_Credito + ", ");
                //if (!String.IsNullOrEmpty(P_Facturas.P_Porcentaje_Descuento)) Mi_sql.Append(P_Facturas.P_Porcentaje_Descuento + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Orden_Compra)) Mi_sql.Append("'" + P_Facturas.P_Orden_Compra + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Forma_Pago)) Mi_sql.Append("'" + P_Facturas.P_Forma_Pago + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Condiciones)) Mi_sql.Append("'" + P_Facturas.P_Condiciones + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_No_Cuenta_Pago)) Mi_sql.Append("'" + P_Facturas.P_No_Cuenta_Pago + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Metodo_Pago)) Mi_sql.Append("'" + P_Facturas.P_Metodo_Pago + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Banco_Pago)) Mi_sql.Append("'" + P_Facturas.P_Banco_Pago + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Emision)) Mi_sql.Append("'" + String.Format("{0:yyyyMMdd HH:mm:ss}", Convert.ToDateTime(P_Facturas.P_Fecha_Emision)) + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Vencimiento)) Mi_sql.Append("'" + String.Format("{0:yyyyMMdd HH:mm:ss}", Convert.ToDateTime(P_Facturas.P_Fecha_Vencimiento)) + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Pago)) Mi_sql.Append("'" + String.Format("{0:yyyyMMdd HH:mm:ss}", Convert.ToDateTime(P_Facturas.P_Fecha_Pago)) + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Tipo_Factura)) Mi_sql.Append("'" + P_Facturas.P_Tipo_Factura + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Tipo_Moneda)) Mi_sql.Append("'" + P_Facturas.P_Tipo_Moneda + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Tipo_Cambio)) Mi_sql.Append(P_Facturas.P_Tipo_Cambio + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Subtotal)) Mi_sql.Append(P_Facturas.P_Subtotal + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Subtotal_Cero)) Mi_sql.Append(P_Facturas.P_Subtotal_Cero + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Retencion_Cedular)) Mi_sql.Append(P_Facturas.P_Retencion_Cedular + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Retencion_Isr)) Mi_sql.Append(P_Facturas.P_Retencion_Isr + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Retencion_Iva)) Mi_sql.Append(P_Facturas.P_Retencion_Iva + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Retencion_Flete)) Mi_sql.Append(P_Facturas.P_Retencion_Flete + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Iva)) Mi_sql.Append(P_Facturas.P_Iva + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Descuento)) Mi_sql.Append(P_Facturas.P_Descuento + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Total)) Mi_sql.Append(P_Facturas.P_Total + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Saldo)) Mi_sql.Append(P_Facturas.P_Saldo + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Abono)) Mi_sql.Append(P_Facturas.P_Abono + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Pagada)) Mi_sql.Append("'" + P_Facturas.P_Pagada + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Cancelada)) Mi_sql.Append("'" + P_Facturas.P_Cancelada + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Usuario_Cancelacion)) Mi_sql.Append("'" + P_Facturas.P_Usuario_Cancelacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Cancelacion)) Mi_sql.Append("'" + P_Facturas.P_Fecha_Cancelacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Motivo_Cancelacion)) Mi_sql.Append("'" + P_Facturas.P_Motivo_Cancelacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Comentarios)) Mi_sql.Append("'" + P_Facturas.P_Comentarios + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Certificado)) Mi_sql.Append("'" + P_Facturas.P_Certificado + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_No_Certificado)) Mi_sql.Append("'" + P_Facturas.P_No_Certificado + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_No_Autorizacion)) Mi_sql.Append("'" + P_Facturas.P_No_Autorizacion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Anio_Autorizacion)) Mi_sql.Append(P_Facturas.P_Anio_Autorizacion + ", ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Genero_Informe)) Mi_sql.Append("'" + P_Facturas.P_Genero_Informe + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Informe_Sat)) Mi_sql.Append("'" + P_Facturas.P_Fecha_Informe_Sat + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Usuario_Informe_Sat)) Mi_sql.Append("'" + P_Facturas.P_Usuario_Informe_Sat + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Creo_Xml)) Mi_sql.Append("'" + String.Format("{0:yyyyMMdd HH:mm:ss}", Convert.ToDateTime(P_Facturas.P_Fecha_Creo_Xml)) + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Usuario_Autorizo_Regenerar)) Mi_sql.Append("'" + P_Facturas.P_Usuario_Autorizo_Regenerar + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Autorizo_Regenerar)) Mi_sql.Append("'" + P_Facturas.P_Fecha_Autorizo_Regenerar + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Usuario_Autorizo_Informe)) Mi_sql.Append("'" + P_Facturas.P_Usuario_Autorizo_Informe + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Autorizo_Informe)) Mi_sql.Append("'" + P_Facturas.P_Fecha_Autorizo_Informe + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Timbre_Version)) Mi_sql.Append("'" + P_Facturas.P_Timbre_Version + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Timbre_Uuid)) Mi_sql.Append("'" + P_Facturas.P_Timbre_Uuid + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Timbre_Fecha_Timbrado)) Mi_sql.Append("'" + String.Format("{0:yyyyMMdd HH:mm:ss}", Convert.ToDateTime(P_Facturas.P_Timbre_Fecha_Timbrado)) + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Timbre_Sello_Cfd)) Mi_sql.Append("'" + P_Facturas.P_Timbre_Sello_Cfd + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Timbre_No_Certificado_Sat)) Mi_sql.Append("'" + P_Facturas.P_Timbre_No_Certificado_Sat + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Timbre_Sello_Sat)) Mi_sql.Append("'" + P_Facturas.P_Timbre_Sello_Sat + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Ruta_Codigo_Bd)) Mi_sql.Append("'" + P_Facturas.P_Ruta_Codigo_Bd + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Ruta_Codigo_Bd)) Mi_sql.Append(string.IsNullOrEmpty(P_Facturas.P_Usuario_Creo) ? "NULL, " : ("'" + P_Facturas.P_Usuario_Creo + "', "));
                if (!String.IsNullOrEmpty(P_Facturas.Curp)) Mi_sql.Append("'" + P_Facturas.Curp + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.Fax)) Mi_sql.Append("'" + P_Facturas.Fax + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.Telefono_1)) Mi_sql.Append("'" + P_Facturas.Telefono_1 + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.Telefono_2)) Mi_sql.Append("'" + P_Facturas.Telefono_2 + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.Extension)) Mi_sql.Append("'" + P_Facturas.Extension + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.Nextel)) Mi_sql.Append("'" + P_Facturas.Nextel + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.Sitio_Web)) Mi_sql.Append("'" + P_Facturas.Sitio_Web + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Estatus)) Mi_sql.Append("'" + P_Facturas.P_Estatus + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_No_Cuenta)) Mi_sql.Append("'" + P_Facturas.P_No_Cuenta + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Rpu)) Mi_sql.Append("'" + P_Facturas.P_Rpu + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Periodo_Pago)) Mi_sql.Append("'" + P_Facturas.P_Periodo_Pago + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Tipo_Servicio)) Mi_sql.Append("'" + P_Facturas.P_Tipo_Servicio + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Cp_Lugar_Expedicion)) Mi_sql.Append("'" + P_Facturas.P_Cp_Lugar_Expedicion + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Cp_Lugar_Expedicion)) Mi_sql.Append("'" + P_Facturas.UsoCFDI + "', ");
                if (!String.IsNullOrEmpty(P_Facturas.P_Cadena_Original)) Mi_sql.Append("'" + P_Facturas.P_Cadena_Original);
                Mi_sql.Append("')");
                Obj_Comando.CommandText = Mi_sql.ToString();
                Obj_Comando.ExecuteNonQuery();

                // Da de alta los detalles de la factura.
                Cls_Ope_Facturas_Detalles_Negocio Rs_Detalle_Factura = new Cls_Ope_Facturas_Detalles_Negocio();
                Rs_Detalle_Factura.P_No_Factura = P_Facturas.P_No_Factura;
                Rs_Detalle_Factura.P_Serie = P_Facturas.P_Serie;

                foreach (DataRow Dr_Fila in Dt_Detalle_Factura.Rows)
                {
                    Rs_Detalle_Factura.P_Cantidad = Dr_Fila["Valor_Unitario"].ToString();
                    Rs_Detalle_Factura.P_Codigo = Dr_Fila["Clave_Concepto"].ToString();
                    //Rs_Detalle_Factura.P_No_Recibo = Dr_Fila["No_Recibo"].ToString();
                    Rs_Detalle_Factura.P_Descripcion = Dr_Fila[Ope_Facturas_Detalle.Campo_Descripcion].ToString();
                    Rs_Detalle_Factura.P_Unidad = Dr_Fila["Unidad_de_Medida"].ToString();
                    Rs_Detalle_Factura.P_Subtotal = Regex.Replace(Dr_Fila["Importe"].ToString(), @"[$,]", "");
                    Rs_Detalle_Factura.P_Precio = Regex.Replace(Dr_Fila["Importe"].ToString(), @"[$,]", "");
                    Rs_Detalle_Factura.P_Porcentaje = String.Format("{0:#0.0000}", Convert.ToDouble( Dr_Fila["tasa"].ToString() )* 100 );
                    Rs_Detalle_Factura.P_Iva = Regex.Replace(Dr_Fila["ivaCorrecto"].ToString(), @"[$,]", "");
                    Rs_Detalle_Factura.P_Total = Regex.Replace(Dr_Fila[Ope_Facturas_Detalle.Campo_Total].ToString(), @"[$,]", "");
                    Rs_Detalle_Factura.Alta_Detalle_Factura(ref Obj_Comando);
                }

                Mi_sql = new StringBuilder();
                Mi_sql.Append("UPDATE Ope_Cor_Caj_Recibos_Cobros set ");
                Mi_sql.Append("NO_FACTURA = '" + P_Facturas.P_No_Factura + "' ");
                Mi_sql.Append("Where CODIGO_BARRAS = '" + P_Facturas.No_Folio + "' ");

                Obj_Comando.CommandText = Mi_sql.ToString();
                Obj_Comando.ExecuteNonQuery();

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

                Alta = true;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Alta;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Factura
        ///DESCRIPCIÓN: Consulta las facturas
        ///PARAMENTROS:     
        ///             1. P_Facturas.      Instancia de la Clase de Negocio de Facturas 
        ///                                 con los datos que servirán de
        ///                                 filtro.
        ///CREO: Luis Alberto Salas García
        ///FECHA_CREO: 8 Abril 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Factura(Cls_Ope_Facturacion_Negocio P_Facturas)
        {
            DataTable Tabla = new DataTable();
            StringBuilder Mi_SQL = new StringBuilder();
            Boolean Entro_Where = false;

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;

            try
            {
                Mi_SQL.Append("SELECT " + Ope_Facturas.Campo_No_Factura + "+'-'+" + Ope_Facturas.Campo_Serie + " as NO_FACTURA_SERIE, " + Ope_Facturas.Tabla_Ope_Facturas + ".* ");
                Mi_SQL.Append(" FROM " + Ope_Facturas.Tabla_Ope_Facturas);
                if (!String.IsNullOrEmpty(P_Facturas.P_No_Factura))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Facturas.Campo_No_Factura + " = '" + P_Facturas.P_No_Factura + "'");
                }
                if (!String.IsNullOrEmpty(P_Facturas.P_Cliente_Id))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Facturas.Tabla_Ope_Facturas + "." + Ope_Facturas.Campo_Cliente_Id + " = '" + P_Facturas.P_Cliente_Id + "'");
                }
                if (!String.IsNullOrEmpty(P_Facturas.P_Orden_Compra))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Facturas.Campo_Orden_Compra + " = '" + P_Facturas.P_Orden_Compra + "'");
                }
                if (!String.IsNullOrEmpty(P_Facturas.P_Cancelada))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Facturas.Campo_Cancelada + " = '" + P_Facturas.P_Cancelada + "'");
                }
                if (!String.IsNullOrEmpty(P_Facturas.P_Pagada))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Facturas.Campo_Pagada + " = '" + P_Facturas.P_Pagada + "'");
                }
                if (!String.IsNullOrEmpty(P_Facturas.P_Serie))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Facturas.Campo_Serie + " = '" + P_Facturas.P_Serie + "'");
                }
                if (!String.IsNullOrEmpty(P_Facturas.P_Estatus))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Facturas.Campo_Estatus + " = '" + P_Facturas.P_Estatus + "'");
                }
                if (!String.IsNullOrEmpty(P_Facturas.P_Razon_Social_Facturacion))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(" (" + Ope_Facturas.Campo_Razon_Social + " LIKE '%" + P_Facturas.P_Razon_Social_Facturacion + "%'");
                    Mi_SQL.Append(" OR " + Ope_Facturas.Campo_Tipo_Factura + " LIKE '%" + P_Facturas.P_Razon_Social_Facturacion + "%'");
                    Mi_SQL.Append(" OR " + Ope_Facturas.Campo_No_Factura + " LIKE '%" + P_Facturas.P_Razon_Social_Facturacion + "%')");
                    //Mi_SQL.Append(" OR " + Ope_Facturas.Campo_Orden_Compra + " LIKE '%" + P_Facturas.P_Razon_Social_Facturacion + "%')");
                }
                //Consulta por rango de fechas
                // por Fecha de Emision
                //if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Emision_Inicial) && !String.IsNullOrEmpty(P_Facturas.P_Fecha_Emision_Final))
                //{
                //    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                //    Mi_SQL.Append(Ope_Facturas.Campo_Fecha_Emision + " BETWEEN " + Cls_Ayudante_Sintaxis.Formato_Fecha(String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(P_Facturas.P_Fecha_Emision_Inicial))) + "");
                //    Mi_SQL.Append(" AND " + Cls_Ayudante_Sintaxis.Formato_Fecha(String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(P_Facturas.P_Fecha_Emision_Final))));
                //}
                // por Fecha de Vencimiento
                //if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Vencimiento_Inicial) && !String.IsNullOrEmpty(P_Facturas.P_Fecha_Vencimiento_Final))
                //{
                //    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                //    Mi_SQL.Append(Ope_Facturas.Campo_Fecha_Vencimiento + " BETWEEN " + Cls_Ayudante_Sintaxis.Formato_Fecha(String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(P_Facturas.P_Fecha_Vencimiento_Inicial))) + "");
                //    Mi_SQL.Append(" AND " + Cls_Ayudante_Sintaxis.Formato_Fecha(String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(P_Facturas.P_Fecha_Vencimiento_Final))) + "");
                //}
                Mi_SQL.Append(" ORDER BY " + Ope_Facturas.Campo_No_Factura + ", " + Ope_Facturas.Campo_Serie + " ASC");

                // agregar filtro y orden a la consulta
                Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }
        public static DataTable Consultar_Factura(Cls_Ope_Facturacion_Negocio P_Facturas, SqlConnection objConexion)
        {
            DataTable Tabla = new DataTable();
            StringBuilder Mi_SQL = new StringBuilder();
            Boolean Entro_Where = false;
            SqlDataAdapter adapter = new SqlDataAdapter();

            try
            {
                Mi_SQL.Append("SELECT " + Ope_Facturas.Campo_No_Factura + "+'-'+" + Ope_Facturas.Campo_Serie + " as NO_FACTURA_SERIE, " + Ope_Facturas.Tabla_Ope_Facturas + ".* ");
                Mi_SQL.Append(" FROM " + Ope_Facturas.Tabla_Ope_Facturas);
                if (!String.IsNullOrEmpty(P_Facturas.P_No_Factura))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Facturas.Campo_No_Factura + " = '" + P_Facturas.P_No_Factura + "'");
                }
                if (!String.IsNullOrEmpty(P_Facturas.P_Cliente_Id))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Facturas.Tabla_Ope_Facturas + "." + Ope_Facturas.Campo_Cliente_Id + " = '" + P_Facturas.P_Cliente_Id + "'");
                }
                if (!String.IsNullOrEmpty(P_Facturas.P_Orden_Compra))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Facturas.Campo_Orden_Compra + " = '" + P_Facturas.P_Orden_Compra + "'");
                }
                if (!String.IsNullOrEmpty(P_Facturas.P_Cancelada))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Facturas.Campo_Cancelada + " = '" + P_Facturas.P_Cancelada + "'");
                }
                if (!String.IsNullOrEmpty(P_Facturas.P_Pagada))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Facturas.Campo_Pagada + " = '" + P_Facturas.P_Pagada + "'");
                }
                if (!String.IsNullOrEmpty(P_Facturas.P_Serie))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Facturas.Campo_Serie + " = '" + P_Facturas.P_Serie + "'");
                }
                if (!String.IsNullOrEmpty(P_Facturas.P_Estatus))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Facturas.Campo_Estatus + " = '" + P_Facturas.P_Estatus + "'");
                }
                if (!String.IsNullOrEmpty(P_Facturas.P_Razon_Social_Facturacion))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(" (" + Ope_Facturas.Campo_Razon_Social + " LIKE '%" + P_Facturas.P_Razon_Social_Facturacion + "%'");
                    Mi_SQL.Append(" OR " + Ope_Facturas.Campo_Tipo_Factura + " LIKE '%" + P_Facturas.P_Razon_Social_Facturacion + "%'");
                    Mi_SQL.Append(" OR " + Ope_Facturas.Campo_No_Factura + " LIKE '%" + P_Facturas.P_Razon_Social_Facturacion + "%')");
                    //Mi_SQL.Append(" OR " + Ope_Facturas.Campo_Orden_Compra + " LIKE '%" + P_Facturas.P_Razon_Social_Facturacion + "%')");
                }
                //Consulta por rango de fechas
                // por Fecha de Emision
                //if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Emision_Inicial) && !String.IsNullOrEmpty(P_Facturas.P_Fecha_Emision_Final))
                //{
                //    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                //    Mi_SQL.Append(Ope_Facturas.Campo_Fecha_Emision + " BETWEEN " + Cls_Ayudante_Sintaxis.Formato_Fecha(String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(P_Facturas.P_Fecha_Emision_Inicial))) + "");
                //    Mi_SQL.Append(" AND " + Cls_Ayudante_Sintaxis.Formato_Fecha(String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(P_Facturas.P_Fecha_Emision_Final))));
                //}
                // por Fecha de Vencimiento
                //if (!String.IsNullOrEmpty(P_Facturas.P_Fecha_Vencimiento_Inicial) && !String.IsNullOrEmpty(P_Facturas.P_Fecha_Vencimiento_Final))
                //{
                //    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                //    Mi_SQL.Append(Ope_Facturas.Campo_Fecha_Vencimiento + " BETWEEN " + Cls_Ayudante_Sintaxis.Formato_Fecha(String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(P_Facturas.P_Fecha_Vencimiento_Inicial))) + "");
                //    Mi_SQL.Append(" AND " + Cls_Ayudante_Sintaxis.Formato_Fecha(String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(P_Facturas.P_Fecha_Vencimiento_Final))) + "");
                //}
                Mi_SQL.Append(" ORDER BY " + Ope_Facturas.Campo_No_Factura + ", " + Ope_Facturas.Campo_Serie + " ASC");

                using (SqlCommand Obj_Comando = objConexion.CreateCommand())
                {
                    Obj_Comando.CommandText = Mi_SQL.ToString();
                    adapter.SelectCommand = Obj_Comando;
                    adapter.Fill(Tabla);
                }

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Ultima_Factura
        ///DESCRIPCIÓN: Consulta la ultima factura hecha
        ///PARAMENTROS:     
        ///             1. P_Facturas.      Instancia de la Clase de Negocio de Facturas 
        ///                                 con los datos que servirán de
        ///                                 filtro.
        ///CREO: Miguel Angel Alvarado Enriquez
        ///FECHA_CREO: 26/Diciembre/2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        ///

        public static DataTable Consultar_Ultima_Factura(Cls_Ope_Facturacion_Negocio P_Facturas)
        {
            DataTable Tabla = new DataTable();
            Boolean Entro_Where = false;

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            StringBuilder Mi_SQL = new StringBuilder();

            SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {
                Mi_SQL.Append("SELECT MAX(" + Ope_Facturas.Campo_No_Factura + ") AS NO_FACTURA");
                Mi_SQL.Append(" FROM " + Ope_Facturas.Tabla_Ope_Facturas);
                if (!String.IsNullOrEmpty(P_Facturas.P_Serie))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Facturas.Campo_Serie + " = '" + P_Facturas.P_Serie + "'");
                }
                // agregar filtro y orden a la consulta
                Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

                Obj_Comando.CommandText = Mi_SQL.ToString();
                adapter.SelectCommand = Obj_Comando;
                adapter.Fill(Tabla);
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }


        public static DataTable Consultar_Ultima_Factura(Cls_Ope_Facturacion_Negocio P_Facturas, SqlConnection objConexion)
        {
            DataTable Tabla = new DataTable();
            Boolean Entro_Where = false;
            StringBuilder Mi_SQL = new StringBuilder();
            SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {
                Mi_SQL.Append("SELECT MAX(" + Ope_Facturas.Campo_No_Factura + ") AS NO_FACTURA");
                Mi_SQL.Append(" FROM " + Ope_Facturas.Tabla_Ope_Facturas);
                if (!String.IsNullOrEmpty(P_Facturas.P_Serie))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Facturas.Campo_Serie + " = '" + P_Facturas.P_Serie + "'");
                }
                using (SqlCommand Obj_Comando = objConexion.CreateCommand())
                {
                    Obj_Comando.CommandText = Mi_SQL.ToString();
                    adapter.SelectCommand = Obj_Comando;
                    adapter.Fill(Tabla);
                }
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Cliente
        ///DESCRIPCIÓN          : Inserta un Cliente en la base de datos.
        ///PARAMETROS           : Parametros: Contiene el registro que sera insertado.
        ///CREO                 : Salvador Vázquez Camacho
        ///FECHA_CREO           : 12/Enero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Boolean Alta_Cliente(Cls_Ope_Facturacion_Negocio Parametros)
        {
            String Cliente_Id = "";
            Boolean Alta = false;
            StringBuilder Mi_SQL = new StringBuilder();
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            try
            {
                //Cls_Metodos_Generales.Obtener_ID_Consecutivo(Ope_Facturas.Tabla_Ope_Facturas, Ope_Facturas.Campo_No_Factura, Ope_Facturas.Campo_Serie + "= '" + Serie + "'", 10);
                Cliente_Id = Cls_Metodos_Generales.Obtener_ID_Consecutivo("CAT_ADM_CLIENTES", "CLIENTE_ID", "", 10);
                Parametros.Cliente_ID = Cliente_Id;
                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("INSERT INTO " + Cat_Adm_Clientes.Tabla_Cat_Adm_Clientes);

                Mi_SQL.Append(" (" + Cat_Adm_Clientes.Campo_Cliente_Id);
                if (!String.IsNullOrEmpty(Parametros.Nombre)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_Nombre_Corto);
                if (!String.IsNullOrEmpty(Parametros.Nombre)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_Razon_Social);
                if (!String.IsNullOrEmpty(Parametros.Rfc)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_RFC);
                if (!String.IsNullOrEmpty(Parametros.Curp)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_CURP);
                if (!String.IsNullOrEmpty(Parametros.Pais)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_Pais);
                if (!String.IsNullOrEmpty(Parametros.Estado_ID)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_Estado);
                if (!String.IsNullOrEmpty(Parametros.Localidad)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_Localidad);
                if (!String.IsNullOrEmpty(Parametros.Colonia)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_Colonia);
                if (!String.IsNullOrEmpty(Parametros.Ciudad)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_Ciudad);
                if (!String.IsNullOrEmpty(Parametros.Calle)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_Calle);
                if (!String.IsNullOrEmpty(Parametros.Numero_Exterior)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_Numero_Exterior);
                if (!String.IsNullOrEmpty(Parametros.Numero_Interior)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_Numero_Interior);
                if (!String.IsNullOrEmpty(Parametros.Codigo_Postal)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_CP);
                if (!String.IsNullOrEmpty(Parametros.Fax)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_Fax);
                if (!String.IsNullOrEmpty(Parametros.Telefono_1)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_Telefono_1);
                if (!String.IsNullOrEmpty(Parametros.Telefono_2)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_Telefono_2);
                if (!String.IsNullOrEmpty(Parametros.Extension)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_Extension);
                if (!String.IsNullOrEmpty(Parametros.Nextel)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_Nextel);
                if (!String.IsNullOrEmpty(Parametros.Email)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_Email);
                if (!String.IsNullOrEmpty(Parametros.Sitio_Web)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_Sitio_Web);
                if (!String.IsNullOrEmpty(Parametros.P_Predio_ID)) Mi_SQL.Append(", " + Cat_Adm_Clientes.Campo_Predio_Id);
                Mi_SQL.Append(") VALUES ");
                Mi_SQL.Append("( '" + Cliente_Id);
                if (!String.IsNullOrEmpty(Parametros.Nombre)) Mi_SQL.Append("','" + Parametros.Nombre);
                if (!String.IsNullOrEmpty(Parametros.Nombre)) Mi_SQL.Append("','" + Parametros.Nombre);
                if (!String.IsNullOrEmpty(Parametros.Rfc)) Mi_SQL.Append("','" + Parametros.Rfc);
                if (!String.IsNullOrEmpty(Parametros.Curp)) Mi_SQL.Append("','" + Parametros.Curp);
                if (!String.IsNullOrEmpty(Parametros.Pais)) Mi_SQL.Append("','" + Parametros.Pais);
                if (!String.IsNullOrEmpty(Parametros.Estado_ID)) Mi_SQL.Append("','" + Parametros.Estado_ID);
                if (!String.IsNullOrEmpty(Parametros.Localidad)) Mi_SQL.Append("','" + Parametros.Localidad);
                if (!String.IsNullOrEmpty(Parametros.Colonia)) Mi_SQL.Append("','" + Parametros.Colonia);
                if (!String.IsNullOrEmpty(Parametros.Ciudad)) Mi_SQL.Append("','" + Parametros.Ciudad);
                if (!String.IsNullOrEmpty(Parametros.Calle)) Mi_SQL.Append("','" + Parametros.Calle);
                if (!String.IsNullOrEmpty(Parametros.Numero_Exterior)) Mi_SQL.Append("','" + Parametros.Numero_Exterior);
                if (!String.IsNullOrEmpty(Parametros.Numero_Interior)) Mi_SQL.Append("','" + Parametros.Numero_Interior);
                if (!String.IsNullOrEmpty(Parametros.Codigo_Postal)) Mi_SQL.Append("','" + Parametros.Codigo_Postal);
                if (!String.IsNullOrEmpty(Parametros.Fax)) Mi_SQL.Append("','" + Parametros.Fax);
                if (!String.IsNullOrEmpty(Parametros.Telefono_1)) Mi_SQL.Append("','" + Parametros.Telefono_1);
                if (!String.IsNullOrEmpty(Parametros.Telefono_2)) Mi_SQL.Append("','" + Parametros.Telefono_2);
                if (!String.IsNullOrEmpty(Parametros.Extension)) Mi_SQL.Append("','" + Parametros.Extension);
                if (!String.IsNullOrEmpty(Parametros.Nextel)) Mi_SQL.Append("','" + Parametros.Nextel);
                if (!String.IsNullOrEmpty(Parametros.Email)) Mi_SQL.Append("','" + Parametros.Email);
                if (!String.IsNullOrEmpty(Parametros.Sitio_Web)) Mi_SQL.Append("','" + Parametros.Sitio_Web);
                if (!String.IsNullOrEmpty(Parametros.P_Predio_ID)) Mi_SQL.Append("', '" + Parametros.P_Predio_ID);
                Mi_SQL.Append("')");
                Obj_Comando.CommandText = Mi_SQL.ToString();
                Obj_Comando.ExecuteNonQuery();

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

                Alta = true;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Alta;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Costos_Factores
        ///DESCRIPCIÓN: Consulta la ultima factura hecha
        ///PARAMENTROS:     
        ///             1. P_Facturas.      Instancia de la Clase de Negocio de Facturas 
        ///                                 con los datos que servirán de
        ///                                 filtro.
        ///CREO: Miguel Angel Alvarado Enriquez
        ///FECHA_CREO: 20/Marzo/2014
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Costos_Factores(Cls_Ope_Facturacion_Negocio P_Facturas)
        {
            DataTable Tabla = new DataTable();

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            StringBuilder Mi_SQL = new StringBuilder();

            try
            {
                Mi_SQL.Append("select * from cat_cor_costos_factores where Costo_Factor_ID in");
                Mi_SQL.Append(" (select MAX(Costo_Factor_ID) from cat_cor_costos_factores) ");

                Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_Datos_Factura_Global()
        //DESCRIPCIÓN:      consulta los datos para la facturacion
        //PARÁMETROS:       Cls_Ope_Facturacion_Negocio: Datos para realizar la busqueda
        //CREO:             Miguel Angel Alvarado Enriquez
        //FECHA_CREO:       09/Abril/2014
        //*******************************************************************************************************
        public DataTable Consultar_Datos_Factura_Global(Cls_Ope_Facturacion_Negocio Datos)
        {
            String Mi_SQL;
            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado            
            DataTable Dt_Datos = new DataTable(); //tabla para el resultado    
            DataTable Dt_Datos_Factura = new DataTable(); //tabla para el resultado           
            SqlConnection Obj_Conexion = null;
            SqlCommand Obj_Comando = null;
            SqlDataReader Dr_Lector;
            //Asignar conexion
            Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            Obj_Comando = Obj_Conexion.CreateCommand();
            try
            {
                Mi_SQL = "select ltrim(RTRIM(isnull(NO_FACTURA,''))) AS NO_FACTURA from Ope_Cor_Caj_Recibos_Cobros ";
                Mi_SQL += "WHERE CODIGO_BARRAS = @cb";
                Mi_SQL += " AND (Estado_Recibo='ACTIVO' OR Estado_Recibo='PORAPLICAR')";
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.Parameters.Add("@cb", SqlDbType.Char, 50).Value = Datos.No_Folio;
                Obj_Comando.CommandType = CommandType.Text;
                //Ejecutar consulta
                Dr_Lector = Obj_Comando.ExecuteReader();
                Dt_Datos.Load(Dr_Lector);

                if(Dt_Datos.Rows.Count<=0)
                    throw new Exception("No existe el folio ingresado");
                //Cerrar conexion
                //Dr_Lector.Close();
                //Obj_Conexion.Close();

                //Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                if (!string.IsNullOrEmpty(Dt_Datos.Rows[0]["NO_FACTURA"].ToString()))
                {
                    //if (Dt_Datos.Rows[0]["NO_FACTURA"].ToString().ToUpper() != "NC")
                    //{
                    if (Dt_Datos.Rows[0]["NO_FACTURA"].ToString().ToUpper() != "CANCELADA" && Dt_Datos.Rows[0]["NO_FACTURA"].ToString().ToUpper() != "NC")
                    {

                        Mi_SQL = String.Empty;
                        Mi_SQL = "select * from OPE_FACTURAS ";
                        Mi_SQL += "WHERE NO_FACTURA = @no_factura";
                        Mi_SQL += " ORDER BY Fecha_Emision DESC";

                        Obj_Comando.CommandText = Mi_SQL;
                        Obj_Comando.Parameters.Add("@no_factura", SqlDbType.Char, 50).Value = Dt_Datos.Rows[0]["NO_FACTURA"];
                        Obj_Comando.CommandType = CommandType.Text;
                        //Ejecutar consulta
                        Dr_Lector = Obj_Comando.ExecuteReader();
                        Dt_Datos_Factura.Load(Dr_Lector);
                        //Dt_Datos_Factura = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                        if (Dt_Datos_Factura.Rows.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(Dt_Datos_Factura.Rows[0]["FOLIO_PAGO"].ToString()))
                            {
                                Dt_Datos.Clear();
                            }
                            else
                            {
                                if ((Dt_Datos.Rows[0]["NO_FACTURA"].ToString().Trim() != "NC") && (Dt_Datos.Rows[0]["NO_FACTURA"].ToString().Trim() != "CANCELADA"))
                                {
                                    Mi_SQL = String.Empty;
                                    Dt_Datos = new DataTable();
                                    Mi_SQL = "select distinct FOLIO_PAGO, Factura='GLOBAL' from OPE_FACTURAS_DETALLES ";
                                    Mi_SQL += "WHERE FOLIO_PAGO = '" + Datos.No_Folio + "';";
                                    Obj_Comando.CommandText = Mi_SQL;
                                    //Obj_Comando.Parameters.Add("@cb", SqlDbType.Char, 50).Value = Datos.No_Folio;
                                    Obj_Comando.CommandType = CommandType.Text;
                                    //Ejecutar consulta
                                    Dr_Lector = Obj_Comando.ExecuteReader();
                                    Dt_Datos.Load(Dr_Lector);
                                }
                                else
                                {
                                    Dt_Datos.Clear();
                                }
                            }
                        }
                        else
                        {
                            Dt_Datos.Clear();
                        }

                    } // fin del if cancelada
                    //else
                    //{
                    //    Dt_Datos.Clear();
                    //}
                }
                //}
                else
                {
                    if ((Dt_Datos.Rows[0]["NO_FACTURA"].ToString().Trim() != "NC") && (Dt_Datos.Rows[0]["NO_FACTURA"].ToString().Trim() != "CANCELADA"))
                    {
                        Mi_SQL = String.Empty;
                        Dt_Datos = new DataTable();
                        Mi_SQL = "select (select ESTATUS FROM OPE_FACTURAS WHERE OPE_FACTURAS.NO_FACTURA = OPE_FACTURAS_DETALLES.NO_FACTURA ), FOLIO_PAGO, Factura='GLOBAL' from OPE_FACTURAS_DETALLES ";
                        Mi_SQL += "WHERE FOLIO_PAGO = '" + Datos.No_Folio + "';";
                        Obj_Comando.CommandText = Mi_SQL;
                        //Obj_Comando.Parameters.Add("@cb", SqlDbType.Char, 50).Value = Datos.No_Folio;
                        Obj_Comando.CommandType = CommandType.Text;
                        //Ejecutar consulta
                        Dr_Lector = Obj_Comando.ExecuteReader();
                        Dt_Datos.Load(Dr_Lector);
                    }
                    else
                    {
                        Dt_Datos.Clear();
                    }
                }


            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Modificar_Email()
        //DESCRIPCIÓN:      modifica el Email del cliente
        //PARÁMETROS:       Cls_Ope_Facturacion_Negocio: Datos para realizar la busqueda
        //CREO:             Miguel Angel Alvarado Enriquez
        //FECHA_CREO:       12/05/2014
        //*******************************************************************************************************
        public void Modificar_Email(Cls_Ope_Facturacion_Negocio Datos)
        {
            //Declaración de las variables
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            String Mi_SQL;
            try
            {
                //Query para modificar un Banco de acuerdo a su ID
                Mi_SQL = "UPDATE OPE_FACTURAS set ";
                Mi_SQL += "Email = '" + Datos.P_Email_Facturacion + "' ";
                Mi_SQL += "Where No_Factura = '" + Datos.P_No_Factura + "' ";

                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex) { throw new Exception("Error: " + Ex.Message); }
            catch (DBConcurrencyException Ex) { throw new Exception("Error: " + Ex.Message); }
            catch (Exception Ex) { throw new Exception("Error: " + Ex.Message); }
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_Datos_Estado_Cuenta()
        //DESCRIPCIÓN:      consula el utlimo recibo de una cuenta
        //PARÁMETROS:       Cls_Ope_Facturacion_Negocio: Datos para realizar la busqueda
        //CREO:             Fernando Gonzalez B.
        //FECHA_CREO:       24/Abril/2014
        //*******************************************************************************************************
        public DataTable Consultar_Datos_Estado_Cuenta(Cls_Ope_Facturacion_Negocio Datos)
        {
            DataTable Dt_Datos = new DataTable();
            String Mi_SQL;
            try
            {
                //Mi_SQL = "SELECT TOP 1 ROW_NUMBER() OVER(ORDER BY Ope_Cor_Facturacion_Recibos.Region_ID, Ope_Cor_Facturacion_Recibos.Sector_ID,Ope_Cor_Facturacion_Recibos.No_Cuenta) as Consecutivo,"
                //    + "Cat_Cor_Predios.No_Cuenta,Cat_Cor_Tarifas.Nombre as Tarifa,NO_PADRON_USUARIOS as No_Padron,"
                //    + " Cat_Cor_Usuarios.NOMBRE + ' ' + Cat_Cor_Usuarios.APELLIDO_PATERNO + ' ' + Cat_Cor_Usuarios.APELLIDO_MATERNO  as Usuario,"
                //    + " Cat_Cor_Colonias.NOMBRE as Colonia,Cat_Cor_Calles.NOMBRE + ' No. Ext. ' + ltrim(rtrim(Cat_Cor_Predios.Numero_Exterior)) as Direccion,Direccion_Corta,Codigo_Barras, "
                //    + " (select TOP 1 case month(Fact.Fecha_Emision) WHEN 1 THEN 'BIM-01-' + CAST(YEAR(Fact.Fecha_Emision) as varchar(10)) "
                //    + " WHEN 2 THEN 'BIM-01-' + CAST(YEAR(Fact.Fecha_Emision) as varchar(10)) WHEN 3 THEN 'BIM-02-' + CAST(YEAR(Fact.Fecha_Emision) as varchar(10)) "
                //    + " WHEN 4 THEN 'BIM-02-' + CAST(YEAR(Fact.Fecha_Emision) as varchar(10)) WHEN 5 THEN 'BIM-03-' + CAST(YEAR(Fact.Fecha_Emision) as varchar(10)) "
                //    + " WHEN 6 THEN 'BIM-03-' + CAST(YEAR(Fact.Fecha_Emision) as varchar(10)) WHEN 7 THEN 'BIM-04-' + CAST(YEAR(Fact.Fecha_Emision) as varchar(10)) "
                //    + " WHEN 8 THEN 'BIM-04-' + CAST(YEAR(Fact.Fecha_Emision) as varchar(10)) WHEN 9 THEN 'BIM-05-' + CAST(YEAR(Fact.Fecha_Emision) as varchar(10)) "
                //    + " WHEN 10 THEN 'BIM-05-' + CAST(YEAR(Fact.Fecha_Emision) as varchar(10)) WHEN 11 THEN 'BIM-06-' + CAST(YEAR(Fact.Fecha_Emision) as varchar(10)) "
                //    + " WHEN 13 THEN 'BIM-06-' + CAST(YEAR(Fact.Fecha_Emision) as varchar(10)) END as ultimo_pagado"
                //    + " from Ope_Cor_Facturacion_Recibos Fact where Fact.No_Cuenta=Cat_Cor_Predios.No_Cuenta and Fact.Estatus_Recibo='PAGADO' ORDER BY Fact.Fecha_Emision DESC)  as ultimo_pagado,"
                //    + " case MONTH(Fecha_Emision) WHEN 1 THEN 'BIM-01-' + cast(YEAR(Fecha_Emision) as varchar(10))"
                //    + " WHEN 2 THEN 'BIM-01-' + cast(YEAR(Fecha_Emision) as varchar(10)) WHEN 3 THEN 'BIM-02-' + cast(YEAR(Fecha_Emision) as varchar(10))"
                //    + " WHEN 4 THEN 'BIM-02-' + cast(YEAR(Fecha_Emision) as varchar(10)) WHEN 5 THEN 'BIM-03-' + cast(YEAR(Fecha_Emision) as varchar(10))"
                //    + " WHEN 6 THEN 'BIM-03-' + cast(YEAR(Fecha_Emision) as varchar(10)) WHEN 7 THEN 'BIM-04-' + cast(YEAR(Fecha_Emision) as varchar(10))"
                //    + " WHEN 8 THEN 'BIM-04-' + cast(YEAR(Fecha_Emision) as varchar(10)) WHEN 9 THEN 'BIM-05-' + cast(YEAR(Fecha_Emision) as varchar(10))"
                //    + " WHEN 10 THEN 'BIM-05-' + cast(YEAR(Fecha_Emision) as varchar(10)) WHEN 11 THEN 'BIM-06-' + cast(YEAR(Fecha_Emision) as varchar(10))"
                //    + " WHEN 12 THEN 'BIM-06-' + cast(YEAR(Fecha_Emision) as varchar(10)) END as Fecha_Emision,cast(Ope_Cor_Facturacion_Recibos.Referencia_Bancaria as numeric(18,0)) as Referencia,"
                //    + " Fecha_Emision AS Periodo_Facturacion,Cat_Cor_Rutas_Reparto.Nombre as Ruta_Reparto,Cat_Cor_Predios_Medidores.NO_MEDIDOR,"
                //    + " Cat_Cor_Predios_Medidores.NO_CABEZA_MEDIDOR as Lectora,Ope_Cor_Facturacion_Recibos.Lectura_Anterior,Ope_Cor_Facturacion_Recibos.Lectura_Actual, Ope_Cor_Facturacion_Recibos.Consumo,"
                //    + "Ope_Cor_Facturacion_Recibos.Fecha_Limite_Pago,Ope_Cor_Facturacion_Recibos.Referencia_Bancaria,Ope_Cor_Facturacion_Recibos.No_Factura_Recibo,"
                //    + " (select isnull(sum(Total), 0) from Ope_Cor_Facturacion_Recibos_Detalles Detalles where Detalles.No_Factura_Recibo = Ope_Cor_Facturacion_Recibos.No_Factura_Recibo"
                //    + " and abs(Detalles.No_Factura_Recibo) = Detalles.No_Factura_Recibo_Origen) AS Importe_Bimestre,"
                //    + " (select isnull(sum(Total), 0) from Ope_Cor_Facturacion_Recibos_Detalles Detalles where Detalles.No_Factura_Recibo = Ope_Cor_Facturacion_Recibos.No_Factura_Recibo"
                //    + " and abs(Detalles.No_Factura_Recibo) <> Detalles.No_Factura_Recibo_Origen) AS Adeudo,"
                //    + " ((select isnull(sum(Total), 0) from Ope_Cor_Facturacion_Recibos_Detalles Detalles where Detalles.No_Factura_Recibo = Ope_Cor_Facturacion_Recibos.No_Factura_Recibo"
                //    + " and abs(Detalles.No_Factura_Recibo) = Detalles.No_Factura_Recibo_Origen) + (select isnull(sum(Total), 0) from Ope_Cor_Facturacion_Recibos_Detalles Detalles"
                //    + " where Detalles.No_Factura_Recibo = Ope_Cor_Facturacion_Recibos.No_Factura_Recibo and abs(Detalles.No_Factura_Recibo) <> Detalles.No_Factura_Recibo_Origen)) AS Total_Pagar"
                //    + ",Ope_Cor_Facturacion_Recibos.Tipo_Recibo,Ope_Cor_Facturacion_Recibos.Codigo_Barras as Numero_Codigo_Barras,case  WHEN  CONVERT(SMALLDATETIME ,CONVERT(VARCHAR(10), GETDATE(),103))>  CONVERT(SMALLDATETIME, CONVERT(varchar(10), Ope_Cor_Facturacion_Recibos.Fecha_Limite_Pago,103)) THEN 'vencido' ELSE 'vigente' end as [vigente] ,Ope_Cor_Facturacion_Recibos.Predio_ID as [predio_id]"
                //    + " FROM Ope_Cor_Facturacion_Recibos left JOIN Cat_Cor_Rutas_Reparto on Ope_Cor_Facturacion_Recibos.Ruta_Reparto_ID=Cat_Cor_Rutas_Reparto.Ruta_Reparto_ID "
                //    + " LEFT JOIN Cat_Cor_Predios_Medidores on Ope_Cor_Facturacion_Recibos.Medidor_ID=Cat_Cor_Predios_Medidores.MEDIDOR_ID ,Cat_Cor_Predios,Cat_Cor_Regiones,"
                //    + " Cat_Cor_Sectores,Cat_Cor_Tarifas,Cat_Cor_Usuarios,Cat_Cor_Colonias,Cat_Cor_Calles"
                //    + " WHERE Ope_Cor_Facturacion_Recibos.Predio_ID=Cat_Cor_Predios.Predio_ID and Ope_Cor_Facturacion_Recibos.Region_ID=Cat_Cor_Regiones.Region_ID "
                //    + " AND Ope_Cor_Facturacion_Recibos.Sector_ID=Cat_Cor_Sectores.Sector_ID AND Ope_Cor_Facturacion_Recibos.Tarifa_ID=Cat_Cor_Tarifas.Tarifa_ID"
                //    + " AND Ope_Cor_Facturacion_Recibos.Usuario_ID=Cat_Cor_Usuarios.USUARIO_ID and Cat_Cor_Predios.Colonia_ID=Cat_Cor_Colonias.COLONIA_ID"
                //    + " and Cat_Cor_Predios.Calle_ID=Cat_Cor_Calles.CALLE_ID"
                //    + " AND Ope_Cor_Facturacion_Recibos.Estatus_Recibo IN ('PENDIENTE','PARCIAL')";

                Mi_SQL = "SELECT TOP 1 ROW_NUMBER() OVER (";
                Mi_SQL += " ORDER BY Ope_Cor_Facturacion_Recibos.Region_ID";
                Mi_SQL += " ,Ope_Cor_Facturacion_Recibos.Sector_ID";
                Mi_SQL += " ,Ope_Cor_Facturacion_Recibos.No_Cuenta";
                Mi_SQL += " ) AS Consecutivo";
                Mi_SQL += " ,Cat_Cor_Predios.No_Cuenta";
                Mi_SQL += " ,Cat_Cor_Tarifas.Nombre AS Tarifa";
                Mi_SQL += " ,NO_PADRON_USUARIOS AS No_Padron";
                Mi_SQL += " ,Cat_Cor_Usuarios.NOMBRE + ' ' + Cat_Cor_Usuarios.APELLIDO_PATERNO + ' ' + Cat_Cor_Usuarios.APELLIDO_MATERNO AS Usuario";
                Mi_SQL += " ,Cat_Cor_Colonias.NOMBRE AS Colonia";
                Mi_SQL += " ,Cat_Cor_Calles.NOMBRE + ' No. Ext. ' + ltrim(rtrim(Cat_Cor_Predios.Numero_Exterior)) AS Direccion";
                Mi_SQL += " ,Direccion_Corta";
                Mi_SQL += " ,Codigo_Barras";
                Mi_SQL += " ,(";
                Mi_SQL += " SELECT TOP 1 CASE month(Fact.Fecha_Emision)";
                Mi_SQL += " WHEN 1";
                Mi_SQL += " THEN 'BIM-01-' + CAST(YEAR(Fact.Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 2";
                Mi_SQL += " THEN 'BIM-01-' + CAST(YEAR(Fact.Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 3";
                Mi_SQL += " THEN 'BIM-02-' + CAST(YEAR(Fact.Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 4";
                Mi_SQL += " THEN 'BIM-02-' + CAST(YEAR(Fact.Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 5";
                Mi_SQL += " THEN 'BIM-03-' + CAST(YEAR(Fact.Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 6";
                Mi_SQL += " THEN 'BIM-03-' + CAST(YEAR(Fact.Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 7";
                Mi_SQL += " THEN 'BIM-04-' + CAST(YEAR(Fact.Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 8";
                Mi_SQL += " THEN 'BIM-04-' + CAST(YEAR(Fact.Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 9";
                Mi_SQL += " THEN 'BIM-05-' + CAST(YEAR(Fact.Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 10";
                Mi_SQL += " THEN 'BIM-05-' + CAST(YEAR(Fact.Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 11";
                Mi_SQL += " THEN 'BIM-06-' + CAST(YEAR(Fact.Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 13";
                Mi_SQL += " THEN 'BIM-06-' + CAST(YEAR(Fact.Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " END AS ultimo_pagado";
                Mi_SQL += " FROM Ope_Cor_Facturacion_Recibos Fact";
                Mi_SQL += " WHERE Fact.No_Cuenta = Cat_Cor_Predios.No_Cuenta";
                Mi_SQL += " AND Fact.Estatus_Recibo = 'PAGADO'";
                Mi_SQL += " ORDER BY Fact.Fecha_Emision DESC";
                Mi_SQL += " ) AS ultimo_pagado";
                Mi_SQL += " ,CASE MONTH(Fecha_Emision)";
                Mi_SQL += " WHEN 1";
                Mi_SQL += " THEN 'BIM-01-' + cast(YEAR(Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 2";
                Mi_SQL += " THEN 'BIM-01-' + cast(YEAR(Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 3";
                Mi_SQL += " THEN 'BIM-02-' + cast(YEAR(Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 4";
                Mi_SQL += " THEN 'BIM-02-' + cast(YEAR(Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 5";
                Mi_SQL += " THEN 'BIM-03-' + cast(YEAR(Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 6";
                Mi_SQL += " THEN 'BIM-03-' + cast(YEAR(Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 7";
                Mi_SQL += " THEN 'BIM-04-' + cast(YEAR(Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 8";
                Mi_SQL += " THEN 'BIM-04-' + cast(YEAR(Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 9";
                Mi_SQL += " THEN 'BIM-05-' + cast(YEAR(Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 10";
                Mi_SQL += " THEN 'BIM-05-' + cast(YEAR(Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 11";
                Mi_SQL += " THEN 'BIM-06-' + cast(YEAR(Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " WHEN 12";
                Mi_SQL += " THEN 'BIM-06-' + cast(YEAR(Fecha_Emision) AS VARCHAR(10))";
                Mi_SQL += " END AS Fecha_Emision";
                Mi_SQL += " ,cast(Ope_Cor_Facturacion_Recibos.Referencia_Bancaria AS NUMERIC(18, 0)) AS Referencia";
                Mi_SQL += " ,Fecha_Emision AS Periodo_Facturacion";
                Mi_SQL += " ,Cat_Cor_Rutas_Reparto.Nombre AS Ruta_Reparto";
                Mi_SQL += " ,Cat_Cor_Predios_Medidores.MEDIDOR_ID";
                Mi_SQL += " ,Cat_Cor_Predios_Medidores.LECTURA_INICIAL AS Lectora";
                Mi_SQL += " ,Ope_Cor_Facturacion_Recibos.Lectura_Anterior";
                Mi_SQL += " ,Ope_Cor_Facturacion_Recibos.Lectura_Actual";
                Mi_SQL += " ,Ope_Cor_Facturacion_Recibos.Consumo";
                Mi_SQL += " ,Ope_Cor_Facturacion_Recibos.Fecha_Limite_Pago AS Fecha_Limite_Pago";
                Mi_SQL += " ,CONVERT(VARCHAR, Fecha_Limite_Pago, 106) AS fecha_limite_pago_formato";
                Mi_SQL += " ,Ope_Cor_Facturacion_Recibos.Referencia_Bancaria";
                Mi_SQL += " ,Ope_Cor_Facturacion_Recibos.No_Factura_Recibo";
                Mi_SQL += " ,(";
                Mi_SQL += " SELECT isnull(sum(Total), 0)";
                Mi_SQL += " FROM Ope_Cor_Facturacion_Recibos_Detalles Detalles";
                Mi_SQL += " WHERE Detalles.No_Factura_Recibo = Ope_Cor_Facturacion_Recibos.No_Factura_Recibo";
                Mi_SQL += " AND abs(Detalles.No_Factura_Recibo) =  Detalles.No_Factura_Recibo";// --Detalles.No_Factura_Recibo_Origen
                Mi_SQL += " ) AS Importe_Bimestre";
                Mi_SQL += " ,(";
                Mi_SQL += " SELECT isnull(sum(Total), 0)";
                Mi_SQL += " FROM Ope_Cor_Facturacion_Recibos_Detalles Detalles";
                Mi_SQL += " WHERE Detalles.No_Factura_Recibo = Ope_Cor_Facturacion_Recibos.No_Factura_Recibo";
                Mi_SQL += " AND abs(Detalles.No_Factura_Recibo) <> Detalles.No_Factura_Recibo";// --Detalles.No_Factura_Recibo_Origen
                Mi_SQL += " ) AS Adeudo";
                Mi_SQL += " ,(";
                Mi_SQL += " (";
                Mi_SQL += " SELECT isnull(sum(Total), 0)";
                Mi_SQL += " FROM Ope_Cor_Facturacion_Recibos_Detalles Detalles";
                Mi_SQL += " WHERE Detalles.No_Factura_Recibo = Ope_Cor_Facturacion_Recibos.No_Factura_Recibo";
                Mi_SQL += " AND abs(Detalles.No_Factura_Recibo) = Detalles.No_Factura_Recibo"; // -- Detalles.No_Factura_Recibo_Origen";
                Mi_SQL += " ) + (";
                Mi_SQL += " SELECT isnull(sum(Total), 0)";
                Mi_SQL += " FROM Ope_Cor_Facturacion_Recibos_Detalles Detalles";
                Mi_SQL += " WHERE Detalles.No_Factura_Recibo = Ope_Cor_Facturacion_Recibos.No_Factura_Recibo";
                Mi_SQL += " AND abs(Detalles.No_Factura_Recibo) <> Detalles.No_Factura_Recibo";// -- Detalles.No_Factura_Recibo_Origen
                Mi_SQL += " )";
                Mi_SQL += " ) AS Total_Pagar";
                Mi_SQL += " ,Ope_Cor_Facturacion_Recibos.Tipo_Recibo";
                Mi_SQL += " ,Ope_Cor_Facturacion_Recibos.Codigo_Barras AS Numero_Codigo_Barras";
                Mi_SQL += ",CASE ";
                Mi_SQL += "	WHEN CONVERT(SMALLDATETIME, CONVERT(VARCHAR(10), GETDATE(), 103)) > CONVERT(SMALLDATETIME, CONVERT(VARCHAR(10), Ope_Cor_Facturacion_Recibos.Fecha_Limite_Pago, 103))";
                Mi_SQL += " THEN 'vencido'";
                Mi_SQL += "	ELSE 'vigente'";
                Mi_SQL += "	END AS [vigente]";
                Mi_SQL += " ,Ope_Cor_Facturacion_Recibos.Predio_ID AS [predio_id]";

                Mi_SQL += ", Ope_Cor_Facturacion_Recibos." + Ope_Cor_Facturacion_Recibos.Campo_RPU;

                Mi_SQL += " FROM Ope_Cor_Facturacion_Recibos";
                Mi_SQL += " LEFT JOIN Cat_Cor_Rutas_Reparto ON Ope_Cor_Facturacion_Recibos.Ruta_Reparto_ID = Cat_Cor_Rutas_Reparto.Ruta_Reparto_ID";
                Mi_SQL += " LEFT JOIN Cat_Cor_Predios_Medidores ON Ope_Cor_Facturacion_Recibos.Medidor_ID = Cat_Cor_Predios_Medidores.MEDIDOR_ID";
                Mi_SQL += " ,Cat_Cor_Predios";
                Mi_SQL += " ,Cat_Cor_Regiones";
                Mi_SQL += " ,Cat_Cor_Sectores";
                Mi_SQL += " ,Cat_Cor_Tarifas";
                Mi_SQL += " ,Cat_Cor_Usuarios";
                Mi_SQL += " ,Cat_Cor_Colonias";
                Mi_SQL += " ,Cat_Cor_Calles";
                Mi_SQL += " WHERE Ope_Cor_Facturacion_Recibos.Predio_ID = Cat_Cor_Predios.Predio_ID";
                Mi_SQL += " AND Ope_Cor_Facturacion_Recibos.Region_ID = Cat_Cor_Regiones.Region_ID";
                Mi_SQL += " AND Ope_Cor_Facturacion_Recibos.Sector_ID = Cat_Cor_Sectores.Sector_ID";
                Mi_SQL += " AND Ope_Cor_Facturacion_Recibos.Tarifa_ID = Cat_Cor_Tarifas.Tarifa_ID";
                Mi_SQL += " AND Ope_Cor_Facturacion_Recibos.Usuario_ID = Cat_Cor_Usuarios.USUARIO_ID";
                Mi_SQL += " AND Cat_Cor_Predios.Colonia_ID = Cat_Cor_Colonias.COLONIA_ID";
                Mi_SQL += " AND Cat_Cor_Predios.Calle_ID = Cat_Cor_Calles.CALLE_ID";
                Mi_SQL += " AND Ope_Cor_Facturacion_Recibos.Estatus_Recibo IN (";
                Mi_SQL += "'PENDIENTE'";
                Mi_SQL += " ,'PARCIAL'";
                Mi_SQL += "         )";

                //Filtro por No Cuenta
                if (!string.IsNullOrEmpty(Datos.P_Rpu))
                    Mi_SQL += " AND Ope_Cor_Facturacion_Recibos.RPU='" + Datos.P_Rpu + "'";

                Mi_SQL += " ORDER BY Ope_Cor_Facturacion_Recibos.Anio DESC,Ope_Cor_Facturacion_Recibos.Bimestre desc";
                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }


        public static DataTable Consultar_Facturas_Clientes(Cls_Ope_Facturacion_Negocio Datos)
        {
            DataTable Dt_Datos = new DataTable();
            DataTable Dt_Datos_Factura = new DataTable();
            String Mi_SQL;
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT NO_RECIBO, NO_FACTURA, CODIGO_BARRAS, CONVERT(VARCHAR(11), FECHA, 106) as fecha ";
                Mi_SQL += ",CASE WHEN isnull(NO_FACTURA, '') = '' OR NO_FACTURA='NC' OR NO_FACTURA ='CANCELADA'";
                Mi_SQL += " THEN 'NO FACTURADO' ";
                Mi_SQL += " ELSE 'FACTURADO' ";
                Mi_SQL += " END AS ESTATUS ";

                Mi_SQL += " FROM Ope_Cor_Caj_Recibos_Cobros";
                if (!String.IsNullOrEmpty(Datos.P_Predio_ID))
                {
                    Mi_SQL += Entro_Where ? " AND " : " WHERE "; Entro_Where = true;
                    Mi_SQL += " RPU = (SELECT RPU FROM Cat_Cor_Predios WHERE Predio_ID='" + Datos.P_Predio_ID + "')";
                }
                if (!String.IsNullOrEmpty(Datos.Codigo_Barras))
                {
                    Mi_SQL += Entro_Where ? " AND " : " WHERE "; Entro_Where = true;
                    Mi_SQL += "CODIGO_BARRAS = '" + Datos.Codigo_Barras + "'";
                }
                if (!String.IsNullOrEmpty(Datos.P_Estatus))
                {
                    if (Datos.P_Estatus.Trim().ToUpper() == "NO FACTURADO")
                    {
                        Mi_SQL += Entro_Where ? " AND " : " WHERE "; Entro_Where = true;
                        Mi_SQL += "(NO_FACTURA is NULL OR NO_FACTURA IN ('NC', 'CANCELADA'))";
                    }
                    if (Datos.P_Estatus.Trim().ToUpper() == "FACTURADO")
                    {
                        Mi_SQL += Entro_Where ? " AND " : " WHERE "; Entro_Where = true;
                        Mi_SQL += "(NO_FACTURA is NOT NULL AND NO_FACTURA not in ('NC', 'CANCELADA'))";
                    }
                }
                Mi_SQL += Entro_Where ? " AND " : " WHERE ";
                Mi_SQL += " estado_recibo IN ('ACTIVO','PORAPLICAR')";

                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }

        public static DataTable Consultar_Predios(Cls_Ope_Facturacion_Negocio Datos)
        {
            DataTable Dt_Datos = new DataTable();
            DataTable Dt_Datos_Factura = new DataTable();
            String Mi_SQL;
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT Cat_Cor_Predios.Predio_ID, * FROM Ope_Cor_Caj_Recibos_Cobros INNER JOIN "
                + " Cat_Cor_Predios ON Cat_Cor_Predios.RPU = Ope_Cor_Caj_Recibos_Cobros.RPU ";
                if (!String.IsNullOrEmpty(Datos.Codigo_Barras))
                {
                    Mi_SQL += Entro_Where ? " AND " : " WHERE "; Entro_Where = true;
                    Mi_SQL += " CODIGO_BARRAS = '" + Datos.Codigo_Barras + "'";
                }
                if (!String.IsNullOrEmpty(Datos.P_Usuario_Id))
                {
                    Mi_SQL += Entro_Where ? " AND " : " WHERE "; Entro_Where = true;
                    Mi_SQL += "Cat_Cor_Predios.Usuario_Id = '" + Datos.P_Usuario_Id + "'";
                }

                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }

        public static DataTable Consultar_Facturas_Recibos(Cls_Ope_Facturacion_Negocio Datos)
        {
            DataTable Dt_Datos = new DataTable();
            DataTable Dt_Datos_Factura = new DataTable();
            String Mi_SQL;
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT ISNULL(CONVERT(VARCHAR(10), f.Fecha_Emision, 103), '') AS [fecha_emision]";
                Mi_SQL += ",f.Periodo_Facturacion AS [periodo_facturacion]";
                Mi_SQL += ",f.Anio AS [anio]";
                Mi_SQL += ",f.Bimestre AS [bimestre]";
                Mi_SQL += ",f.No_Factura_Recibo AS [no_factura_recibo]";
                Mi_SQL += ",f.Estatus_Recibo AS [estatus_recibo]";
                Mi_SQL += ",CONVERT(VARCHAR(10), f.Fecha_Limite_Pago, 103) AS fecha_limite_pago";
                Mi_SQL += ",isnull(CONVERT(VARCHAR(10), f.Fecha_Corte, 103), CONVERT(VARCHAR(10), f.Fecha_Limite_Pago, 103)) AS fecha_corte";
                Mi_SQL += ",f.Comentarios AS comentario";
                Mi_SQL += ",f.RPU AS rpu";
                Mi_SQL += ",CONVERT(VARCHAR(10), f.Fecha_Creo, 103) AS [fecha_creo]";
                Mi_SQL += ",f.Lectura_Anterior";
                Mi_SQL += ",f.Lectura_Actual";
                Mi_SQL += ",f.Consumo";
                Mi_SQL += ",f.Referencia_Bancaria referencia";
                Mi_SQL += ",f.No_Cuenta";
                Mi_SQL += ",f.Codigo_Barras";
                Mi_SQL += ",f.Total_IVA";
                Mi_SQL += ",f.No_Factura_Recibo";
                Mi_SQL += ", (";
                Mi_SQL += "SELECT ISNULL(SUM(Total_Saldo), 0)	FROM Ope_Cor_Facturacion_Recibos_Detalles Det WHERE Det.No_Factura_Recibo = f.No_Factura_Recibo ";
		        Mi_SQL += " AND Det.Concepto_ID <> (SELECT Concepto_Rezago_Saneamiento_Id FROM Cat_Cor_Parametros )	AND Det.Concepto_ID <> (SELECT Concepto_Rezago_Agua_Id FROM Cat_Cor_Parametros ) ";
                Mi_SQL += " AND Det.Concepto_ID <> (SELECT Concepto_Rezago_Drenaje_Id FROM Cat_Cor_Parametros )	and Det.Estatus in ('PENDIENTE')) As Total_Importe ";
                Mi_SQL += ",(";
                Mi_SQL += "SELECT ISNULL(SUM(Total_Saldo), 0) FROM Ope_Cor_Facturacion_Recibos_Detalles Det WHERE Det.No_Factura_Recibo = f.No_Factura_Recibo ";
                Mi_SQL += " AND (Det.Concepto_ID = (SELECT Concepto_Rezago_Saneamiento_Id FROM Cat_Cor_Parametros ) OR Det.Concepto_ID = (SELECT Concepto_Rezago_Agua_Id FROM Cat_Cor_Parametros ) ";
		        Mi_SQL += " OR Det.Concepto_ID = (SELECT Concepto_Rezago_Drenaje_Id FROM Cat_Cor_Parametros )) and Det.Estatus in ('PENDIENTE')) As Adeudo ";
                Mi_SQL += ",ISNULL(";
                Mi_SQL += "    (";
                Mi_SQL += "SELECT SUM(fd.Total) - isnull((";
                Mi_SQL += " SELECT sum(m.total)";
                Mi_SQL += " FROM Ope_Cor_Facturacion_Recibos r";
                Mi_SQL += " JOIN Ope_Cor_Facturacion_Recibos_Detalles d ON r.No_Factura_Recibo = d.No_Factura_Recibo";
                Mi_SQL += " JOIN Ope_Cor_Caj_Movimientos_Cobros_Espejo m ON m.No_Movimiento_Facturacion = d.No_Movimiento";
                Mi_SQL += " WHERE r.RPU = f.RPU";
                Mi_SQL += " AND isnull(m.estado_concepto_cobro, '') <> 'CANCELADO'";
                Mi_SQL += " ), 0)";
                Mi_SQL += " FROM Ope_Cor_Facturacion_Recibos f";
                Mi_SQL += " JOIN Ope_Cor_Facturacion_Recibos_Detalles fd ON f.No_Factura_Recibo = fd.No_Factura_Recibo";
                Mi_SQL += " WHERE f.RPU = '" + Datos.P_Rpu + "'";
                Mi_SQL += " AND f.Estatus_Recibo IN (";
                Mi_SQL += " 'PENDIENTE'";
                Mi_SQL += " ,'PARCIAL'";
                Mi_SQL += " )";
                Mi_SQL += " GROUP BY f.RPU";
                Mi_SQL += "        )";
                Mi_SQL += ",0    ) AS Total_Pagar";
                Mi_SQL += ", '' AS [vigente] ";
                Mi_SQL += " FROM Ope_Cor_Facturacion_Recibos f";
                Mi_SQL += " WHERE f.RPU = '" + Datos.P_Rpu + "'";
                Mi_SQL += " AND f.Estatus_Recibo NOT IN (";
                Mi_SQL += " 'CANCELADO'";
                Mi_SQL += " ,'PREFACTURA'";
                Mi_SQL += " ,'POR AUTORIZAR'";
                Mi_SQL += " ,'LIQUIDACION'";
                Mi_SQL += " ,'PRESUPUESTO'";
                Mi_SQL += " )";
                Mi_SQL += " ORDER BY f.Anio DESC";
                Mi_SQL += ",f.Bimestre DESC";

                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }
        public static DataTable Consultar_Medidores(Cls_Ope_Facturacion_Negocio Datos)
        {
            DataTable Dt_Datos = new DataTable();
            DataTable Dt_Datos_Factura = new DataTable();
            String Mi_SQL;
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT TOP 1 me.NO_MEDIDOR AS [no_medidor]";
                Mi_SQL += ", me.NO_CABEZA_MEDIDOR AS [no_cabeza_medidor]";
                Mi_SQL += ", me.DIAMETRO AS [diametro]";
                Mi_SQL += ", ma.NOMBRE AS [marca]";
                Mi_SQL += ", m.LECTURA_INICIAL AS [lectura_inicial]";
                Mi_SQL += ", m.FECHA_INSTALACION AS [fecha_instalacion]";
                Mi_SQL += ", (";
                Mi_SQL += "	SELECT DISTINCT TOP 1 convert(VARCHAR, FECHA, 103)";
                Mi_SQL += "	FROM Ope_Cor_Diversos";
                Mi_SQL += "	JOIN Ope_Cor_Diversos_Detalles ON Ope_Cor_Diversos.No_Diverso = Ope_Cor_Diversos_Detalles.No_Diverso";
                Mi_SQL += "	JOIN Ope_Cor_Caj_Recibos_Cobros ON Ope_Cor_Caj_Recibos_Cobros.No_Diverso = Ope_Cor_Diversos.No_Diverso";
                Mi_SQL += "	WHERE Ope_Cor_Diversos.Estatus = 'PAGADO'";
                Mi_SQL += "	AND Concepto LIKE ('%VENTA%MEDIDOR%')";
                Mi_SQL += "	AND Ope_Cor_Caj_Recibos_Cobros.RPU = '" + Datos.P_Rpu + "'";
                Mi_SQL += "	) AS fecha_pago";
                Mi_SQL += "	FROM Cat_Cor_Predios p";
                Mi_SQL += "	LEFT JOIN Cat_Cor_Predios_Medidores m ON p.PREDIO_ID = m.PREDIO_ID";
                Mi_SQL += "	LEFT JOIN Cat_Cor_Medidores me ON m.Medidor_ID = me.MEDIDOR_ID";
                Mi_SQL += "	LEFT JOIN CAT_COM_MARCAS ma ON ma.MARCA_ID = me.MARCA_ID";
                Mi_SQL += "	WHERE p.RPU = '" + Datos.P_Rpu + "'";

                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }

        public static DataTable Consultar_Usuarios(Cls_Ope_Facturacion_Negocio Datos)
        {
            DataTable Dt_Datos = new DataTable();
            DataTable Dt_Datos_Factura = new DataTable();
            String Mi_SQL;
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT p.Tipo_Conexion AS [tipo_conexion] ";
                Mi_SQL += ",p.predio_id ";
                Mi_SQL += ",p.RPU AS [no_cuenta] ";
                Mi_SQL += ",isnull(u.NOMBRE, '') + ' ' + isnull(u.APELLIDO_PATERNO, '') + ' ' + isnull(u.APELLIDO_MATERNO, '') AS [usuario] ";
                Mi_SQL += "/*,u.Razon_Social AS [usuario] */";
                Mi_SQL += ",r.NUMERO_REGION AS [no_region] ";
                Mi_SQL += ",r.NUMERO_REGION AS [no_sector] ";
                Mi_SQL += ",No_Orden_Reparto AS [no_reparto] ";
                Mi_SQL += ",z.NUMERO_ZONA AS [no_zona] ";
                Mi_SQL += ",z.DESCRIPCION AS [zona_descripcion] ";
                Mi_SQL += ",g.Nombre_Giro AS [giro] ";
                Mi_SQL += ",g.Aplica_IVA AS [iva] ";
                Mi_SQL += ",p.ESTATUS AS [estatus] ";
                Mi_SQL += ",p.NO_PADRON_USUARIOS AS [no_padron_usuarios] ";
                Mi_SQL += ",t.Nombre AS [tarifa] ";
                Mi_SQL += ",v.NOMBRE AS [tipo] ";
                Mi_SQL += ",l.NOMBRE AS [calle] ";
                Mi_SQL += ",c.NOMBRE AS [colonia] ";
                Mi_SQL += ",p.NUMERO_EXTERIOR AS [exterior] ";
                Mi_SQL += ",CASE  ";
                Mi_SQL += "WHEN ab.Estatus = 'ACTIVO' ";
                Mi_SQL += "THEN tb.Nombre ";
                Mi_SQL += "ELSE '' ";
                Mi_SQL += "END AS Tipo_Beneficio ";
                Mi_SQL += ",p.NUMERO_INTERIOR AS [interior] ";
                Mi_SQL += ",[giro_actividad] = '' ";
                Mi_SQL += ",cc.nombre [servicio] ";
                Mi_SQL += ",t.clave AS [clave_tarifa] ";
                Mi_SQL += ",convert(VARCHAR(10), isnull(p.Fecha_Inicio_Facturacion, ''), 103) AS [Fecha_Inicio_Servicio] ";
                Mi_SQL += ",isnull(convert(VARCHAR(10), u.fecha_creo, 103), '') AS [fecha_registro] ";
                Mi_SQL += ",isnull(convert(VARCHAR(10), u.fecha_modifico, 103), '') AS [fecha_actualizo] ";
                Mi_SQL += ",rr.No_Ruta ";
                Mi_SQL += ",rr.Nombre AS [Nombre_Ruta]";
                Mi_SQL += ",isnull(p.Aplica_Notificacion, 'NO') AS Aplica_Notificacion ";
                Mi_SQL += ",isnull(( ";
                Mi_SQL += "SELECT TOP 1 ";
                //Mi_SQL += " 'PER-' + CONVERT(VARCHAR, fd.Bimestre) + '-' + CONVERT(VARCHAR, fd.Anio) ";
                Mi_SQL += " CONVERT(VARCHAR, fd.Bimestre) + '/' + CONVERT(VARCHAR, fd.Anio) ";
                Mi_SQL += " FROM Ope_Cor_Facturacion_Recibos_Detalles fd";
                Mi_SQL += " INNER JOIN Ope_Cor_Facturacion_Recibos f ON ";
                Mi_SQL += " fd.No_Factura_Recibo = f.No_Factura_Recibo";
                Mi_SQL += " WHERE ";
                Mi_SQL += " f.RPU = '"+Datos.P_Rpu+"'";
                Mi_SQL += " AND ";
                Mi_SQL += " fd.Estatus = 'PAGADO'";
                Mi_SQL += " ORDER BY fd.Anio DESC, fd.Bimestre DESC ";
                Mi_SQL += "), '') AS Ultimo_Bim_Pagado ";
                Mi_SQL += ",p.Clave_Catastral ";
                Mi_SQL += ",p.Latitud AS [latitud] ";
                Mi_SQL += ",p.Longitud AS [longitud] ";
                Mi_SQL += "FROM Cat_Cor_Predios p ";
                Mi_SQL += "JOIN Cat_Cor_Usuarios u ON u.USUARIO_ID = p.Usuario_ID ";
                Mi_SQL += "JOIN Cat_Cor_Giros_Actividades ga ON ga.Actividad_Giro_ID = p.Giro_Actividad_ID ";
                Mi_SQL += "JOIN Cat_Cor_Giros g ON g.GIRO_ID = ga.GIRO_ID ";
                Mi_SQL += "JOIN Cat_Cor_Tarifas t ON t.Tarifa_ID = p.TARIFA_ID ";
                Mi_SQL += "JOIN Cat_Cor_Colonias c ON c.COLONIA_ID = p.COLONIA_ID ";
                Mi_SQL += "JOIN Cat_Cor_Calles l ON l.CALLE_ID = p.CALLE_ID ";
                Mi_SQL += "LEFT JOIN CAT_COR_REGIONES r ON r.Region_ID = p.Region_ID ";
                Mi_SQL += "LEFT JOIN CAT_COR_ZONAS z ON z.ZONA_ID = p.ZONA_ID ";
                Mi_SQL += "LEFT JOIN CAT_COR_VIALIDADES v ON v.VIALIDAD_ID = l.VIALIDAD_ID ";
                Mi_SQL += "LEFT JOIN CAT_COR_ASIGNACION_BENEFICIOS ab ON ab.RPU = p.RPU ";
                Mi_SQL += "LEFT JOIN Cat_Cor_Tipos_Beneficios tb ON tb.Tipo_Beneficio_ID = ab.TIPO_BENEFICIO_ID ";
                Mi_SQL += "LEFT JOIN Cat_Cor_Grupos_Conceptos cc ON cc.GRUPO_CONCEPTO_ID = p.Grupo_Concepto_Cobro_ID ";
                Mi_SQL += "LEFT JOIN Cat_Cor_Rutas_Reparto rr ON rr.Ruta_Reparto_ID = p.Ruta_Reparto_ID ";
                Mi_SQL += "WHERE p.RPU = '" + Datos.P_Rpu + "' ";
                Mi_SQL += "ORDER BY u.RAZON_SOCIAL ";

                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }

        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_Totales
        //DESCRIPCIÓN:      Consulta los cobros pendientes de una cuenta
        //PARÁMETROS:       Datos: Datos para realizar la busqueda
        //CREO:             José Maldonado Méndez
        //FECHA_CREO:       01/Abril/2015
        //*******************************************************************************************************
        public static DataTable Consultar_Totales(Cls_Ope_Facturacion_Negocio Parametros)
        {
            DataTable Dt_Datos = new DataTable();
            DataTable Dt_Datos_Factura = new DataTable();
            String Mi_SQL;
            try
            {
                Mi_SQL = "SELECT c.Nombre AS [concepto] " +
                     "    ,fd.Importe - ISNULL(( " +
                     "           SELECT sum(mc.IMPORTE) " +
                     "           FROM Ope_Cor_Caj_Movimientos_Cobros_Espejo mc " +
                     "           WHERE mc.No_Movimiento_Facturacion = fd.No_Movimiento and mc.estado_concepto_cobro<>'CANCELADO' " +
                     "           ), 0) AS [importe_saldo] " +
                     "   ,fd.Impuesto - ISNULL(( " +
                     "           SELECT sum(mc.impuesto) " +
                     "           FROM Ope_Cor_Caj_Movimientos_Cobros_Espejo mc " +
                     "           WHERE mc.No_Movimiento_Facturacion = fd.No_Movimiento and mc.estado_concepto_cobro<>'CANCELADO' " +
                     "           ), 0) AS [impuesto_saldo] " +
                     "   ,f.Anio AS [anio] " +
                     "   ,f.Bimestre AS [bimestre] " +
                     "   ,fd.Total - isnull(( " +
                     "           SELECT sum(mc.total) " +
                     "           FROM Ope_Cor_Caj_Movimientos_Cobros_Espejo mc " +
                     "           WHERE mc.No_Movimiento_Facturacion = fd.No_Movimiento and mc.estado_concepto_cobro<>'CANCELADO'" +
                     "           ), 0) AS [total_saldo] " +
                     "   ,f.No_Factura_Recibo AS [no_factura_recibo] " +
                     "   ,fd.Concepto_ID AS [concepto_id] " +
                    "FROM Ope_Cor_Facturacion_Recibos f " +
                    "JOIN Ope_Cor_Facturacion_Recibos_Detalles fd ON f.No_Factura_Recibo = fd.No_Factura_Recibo " +
                    "JOIN Cat_Cor_Conceptos_Cobros c ON c.Concepto_ID = fd.Concepto_ID " +
                    "WHERE RPU = '" + Parametros.P_Rpu + "' " +
                    "    AND Estatus_Recibo IN ( " +
                    "        'PENDIENTE' " +
                    "        ,'PARCIAL' " +
                    "        )         " +
                    "    AND fd.Total - isnull(( " +
                    "            SELECT sum(mc.total) " +
                    "            FROM Ope_Cor_Caj_Movimientos_Cobros_Espejo mc " +
                    "            WHERE mc.No_Movimiento_Facturacion = fd.No_Movimiento  and mc.estado_concepto_cobro<>'CANCELADO' " +
                    "            ), 0) > 0 " +
                    "ORDER BY f.Anio ASC " +
                    "    ,bimestre ASC " +
                    "    ,f.No_Factura_Recibo ASC ";

                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }

        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_Rpu()
        //DESCRIPCIÓN:      consulta los datos del recibo
        //PARÁMETROS:       Cls_Ope_Facturacion_Negocio: Datos para realizar la busqueda
        //CREO:             José Maldonado Méndez
        //FECHA_CREO:       04/Abril/2015
        //*******************************************************************************************************
        public static DataTable Consultar_Rpu(Cls_Ope_Facturacion_Negocio Datos)
        {
            DataTable Dt_Datos = new DataTable();
            String Mi_SQL;
            try
            {
                Mi_SQL = "select * from Ope_Cor_Caj_Recibos_Cobros " +
                    "WHERE CODIGO_BARRAS = '" + Datos.Codigo_Barras + "'";
                //" AND (estado_Recibo='ACTIVO' OR estado_recibo = 'PORAPLICAR')";
                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }
        
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Modificar_Datos_Usuarios
        //DESCRIPCIÓN:      Actualiza los datos de facturacion de los usuarios
        //PARÁMETROS:       Datos: Datos para realizar la busqueda
        //CREO:             José Maldonado Méndez
        //FECHA_CREO:       04/Abril/2015
        //*******************************************************************************************************
        public static void Modificar_Datos_Usuarios(Cls_Ope_Facturacion_Negocio Datos)
        {
            //Declaración de las variables
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            String Mi_SQL;
            try
            {
                //Query para modificar un Banco de acuerdo a su ID
                Mi_SQL = "UPDATE Cat_Cor_Usuarios";
                Mi_SQL += " SET	RAZON_SOCIAL = '" + Datos.P_Razon_Social_Facturacion + "'";
                Mi_SQL += " ,RFC = '" + Datos.P_Rfc_Facturacion + "'";
                Mi_SQL += " ,CORREO_ELECTRONICO = '" + Datos.P_Email_Facturacion + "'";
                Mi_SQL += " ,Domicilio_Facturacion = '" + Datos.P_Calle_Facturacion + "'";
                Mi_SQL += " ,Colonia_Facturacion = '" + Datos.P_Colonia_Facturacion + "'";
                Mi_SQL += " ,CP_Facturacion = '" + Datos.P_Cp_Facturacion + "'";
                Mi_SQL += " ,Ciudad_Facturacion = '" + Datos.P_Ciudad_Facturacion + "'";
                Mi_SQL += " ,Estado_Facturacion = '" + Datos.P_Estado_Facturacion + "'";
                Mi_SQL += " ,numero_exterior_fiscal = '" + Datos.P_Numero_Exterior_Facturacion + "'";
                Mi_SQL += " ,numero_interior_fiscal = '" + Datos.P_Numero_Interior_Facturacion + "'";
                Mi_SQL += " ,estado_fiscal_id = '" + Datos.P_Estado_Fiscal_Id + "'";
                Mi_SQL += " ,ciudad_fiscal_id = '" + Datos.P_Ciudad_Fiscal_ID + "'";

                Mi_SQL += " WHERE USUARIO_ID = '" + Datos.P_Usuario_Id + "'";

                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                //Mi_SQL += "UPDATE Ope_Cor_Diversos SET RFC='" + Datos.Rfc + "' WHERE Codigo_Barras='" + Datos.No_Folio + "'";

                //Obj_Comando.CommandText = Mi_SQL;
                //Obj_Comando.ExecuteNonQuery();

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex) { throw new Exception("Error: " + Ex.Message); }
            catch (DBConcurrencyException Ex) { throw new Exception("Error: " + Ex.Message); }
            catch (Exception Ex) { throw new Exception("Error: " + Ex.Message); }
        }

        public static DataTable Buscar_Bancos(Cls_Ope_Facturacion_Negocio Datos)
        {
            String Mi_SQL;
            DataTable Dt_Datos = new DataTable();
            try
            {
                //Query para buscar una Región de acuerdo a su número de región
                Mi_SQL = "SELECT * FROM Cat_Cor_Bancos";

                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }

        public static DataTable Consultar_Detalles_Recibos(string Rpu)
        {
            String str_Sql = "";
            DataTable Dt_Datos = new DataTable();
            str_Sql = "SELECT c.Nombre AS [concepto],fd.Total - ISNULL((SELECT ";
            str_Sql += " SUM(mc.total) FROM Ope_Cor_Caj_Movimientos_Cobros_Espejo mc WHERE mc.No_Movimiento_Facturacion = fd.No_Movimiento ";
            str_Sql += " AND mc.estado_concepto_cobro <> 'CANCELADO'), 0) AS [total_saldo]";
            str_Sql += " ,fd.Concepto_ID AS [concepto_id]";
            str_Sql += " FROM Ope_Cor_Facturacion_Recibos f";
            str_Sql += " JOIN Ope_Cor_Facturacion_Recibos_Detalles fd";
            str_Sql += " ON f.No_Factura_Recibo = fd.No_Factura_Recibo";
            str_Sql += " JOIN Cat_Cor_Conceptos_Cobros c";
            str_Sql += " ON c.Concepto_ID = fd.Concepto_ID";
            str_Sql += " WHERE RPU = '" + Rpu + "'";
            str_Sql += " AND Estatus_Recibo IN ('PENDIENTE', 'PARCIAL')";
            str_Sql += " AND fd.Total - ISNULL((SELECT";
            str_Sql += " SUM(mc.total)";
            str_Sql += " FROM Ope_Cor_Caj_Movimientos_Cobros_Espejo mc";
            str_Sql += " WHERE mc.No_Movimiento_Facturacion = fd.No_Movimiento";
            str_Sql += " AND mc.estado_concepto_cobro <> 'CANCELADO'), 0) > 0";
            //ORDER BY concepto_id";
            //str_Sql = "";

            str_Sql =  " SELECT c.Nombre AS [concepto],fd.Total_Saldo - ISNULL((SELECT ";
            str_Sql += " SUM(mc.total) FROM Ope_Cor_Caj_Movimientos_Cobros_Espejo mc WHERE mc.No_Movimiento_Facturacion = fd.No_Movimiento ";
            str_Sql += " AND mc.estado_concepto_cobro <> 'CANCELADO'), 0) AS [total_saldo]";
            str_Sql += " ,fd.Concepto_ID AS [concepto_id]";
            str_Sql += " FROM Ope_Cor_Facturacion_Recibos fc, Ope_Cor_Facturacion_Recibos_Detalles fd,	Cat_Cor_Conceptos_Cobros c ";
            str_Sql += " WHERE fc.RPU = '" + Rpu + "' and fc.No_Factura_Recibo = fd.No_Factura_Recibo AND c.Concepto_ID = fd.Concepto_ID";
            str_Sql += " and fd.Estatus <> 'PAGADO' and fd.No_Movimiento NOT IN ( ";
            str_Sql += " select mc.No_Movimiento_Facturacion ";
	        str_Sql += " from Ope_Cor_Caj_Movimientos_Cobros_Espejo mc ";
	        str_Sql += " where mc.estado_concepto_cobro = 'PORAPLICAR'  ";
	        str_Sql += " and mc.No_Movimiento = fd.No_Movimiento ";
    	    str_Sql += " and (fd.Total_Saldo - mc.total) <= 0) ";
            str_Sql += " AND fd.Total_Saldo > 0";
            str_Sql += " ORDER BY c.orden";

            Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, str_Sql).Tables[0];

            return Dt_Datos;
        }

        public static double Obtener_Periodos_Adeudos(string no_cuenta)
        {
            DataTable dt;
            double periodos = 0;


            try
            {
                //strsql = "SELECT max(Fecha_Emision) FROM Ope_Cor_Facturacion_Recibos WHERE RPU ='" + no_cuenta + "' AND Estatus_Recibo = 'PAGADO'";
                //dt = SqlHelper.ExecuteDataset(conexion, CommandType.Text, strsql).Tables[0];

                //strsql = "SELECT count(*)  as [periodos_adeudos] " +
                //"from Ope_Cor_Facturacion_Recibos WHERE Estatus_Recibo IN ('PENDIENTE','PAGO PARCIAL') AND " +
                //" RPU='" + no_cuenta + "' ";

                //if (dt.Rows.Count > 0)
                //{
                //    string fecha;
                //    fecha = String.Format("{0:dd/MM/yyyy}", dt.Rows[0][0]);

                //    if (fecha.Length > 0)
                //        strsql += "AND Fecha_Emision >= '" + String.Format("{0:dd/MM/yyyy}", dt.Rows[0][0]) + "'";

                //}


                string strsql = "SELECT Anio,Bimestre,RPU from View_Periodos_Resumen WHERE RPU='" + no_cuenta + "' " +
                        "GROUP BY Anio,Bimestre,RPU";

                dt = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, strsql).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    periodos = Convert.ToDouble(dt.Rows.Count);
                }
                else
                {
                    periodos = 0;
                }
            }
            catch (Exception ex)
            {

                periodos = 0;
            }

            return periodos;
        }
        public static double Obtener_Saldo_a_Favor(string no_cuenta)
        {
            DataTable dt;
            double saldo_favor = 0;
            double saldo_favor_aux = 0;
            string strsql;
            try
            {
                saldo_favor = 0;
                strsql = "SELECT  ISNULL( sum(Saldo),0) as [saldo_favor] " +
                       "FROM Ope_Cor_Pagos_Adelantados where RPU='" + no_cuenta + "'  ";
                dt = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, strsql).Tables[0];

                if (dt.Rows.Count > 0)
                {
                    saldo_favor_aux = Convert.ToDouble(dt.Rows[0]["saldo_favor"]);
                }

            }
            catch (Exception ex)
            {
                saldo_favor_aux = 0;
            }

            return saldo_favor_aux;

        }

        public static bool Verificar_Importe_Iva(string conceptoId,SqlConnection objConexion)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();
            string strsql;
            try
            {
                strsql = "select ci.PORCENTAJE_IMPUESTO from CAT_COM_IMPUESTOS as ci inner join Cat_Cor_Conceptos_Cobros as cc on ci.IMPUESTO_ID = cc.impuesto_id where cc.Concepto_ID = '"+conceptoId+"'";
                using (SqlCommand Obj_Comando = objConexion.CreateCommand())
                {
                    Obj_Comando.CommandText = strsql;
                    Obj_Comando.CommandType = CommandType.Text;
                    adapter.SelectCommand = Obj_Comando;
                    adapter.Fill(dt);
                }

                if (Convert.ToDouble(dt.Rows[0]["PORCENTAJE_IMPUESTO"]) > 0)
                    return true;

                return false;

                //if (dt.Rows.Count > 0)
                //{
                //    saldo_favor_aux = Convert.ToDouble(dt.Rows[0]["saldo_favor"]);
                //}

            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }

        }

        public static string Verificar_Es_Descuento(string conceptoId)
        {
            DataTable dt;

            string strsql;
            try
            {
                strsql = @"select Concepto_ID
                           from Cat_Cor_Conceptos_Cobros
                            WHERE Concepto_Descuento_Id = '{0}'";

                dt = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, string.Format(strsql, conceptoId)).Tables[0];

                if (dt.Rows.Count < 1)
                    return "00000";

                return dt.Rows[0]["Concepto_ID"].ToString();
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }

        }


        public static DataTable Verificar_Es_Sancion(string conceptoId)
        {
            DataTable dt;

            string strsql;
            try
            {
                strsql = @"SELECT con.Concepto_ID, con.Nombre, cat.nombre_categoria
                    from Cat_Cor_Conceptos_Cobros as con
	                    JOIN cat_cor_categoria_conceptos_cobros  as cat
		                    on con.concepto_categoria_id = cat.concepto_categoria_id
                    WHERE Concepto_ID = '{0}' AND cat.nombre_categoria like '%sanciones%';";

                dt = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, string.Format(strsql, conceptoId)).Tables[0];

                return dt;
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }

        }
    }
}