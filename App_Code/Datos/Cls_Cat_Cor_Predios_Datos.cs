﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SharpContent.ApplicationBlocks.Data;
using SIAC.Cat_Predios.Negocio;
using SIAC.Constantes;

/// <summary>
/// Descripción breve de Cls_Cat_Cor_Predios_Datos
/// </summary>
/// 
namespace SIAC.Cat_Predios.Datos
{
    public class Cls_Cat_Cor_Predios_Datos
    {
        ///**************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Predios
        ///DESCRIPCIÓN         : Consulta los clientes
        ///PARAMENTROS         : P_Parametros.- Instancia de la Clase de Negocio de Clientes con los datos que servirán de
        ///                                   filtro.
        ///CREO                : José Maldonado Méndez
        ///FECHA_CREO          : 30/Noviembre/2015
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        public static DataTable Consultar_Predios(Cls_Cat_Cor_Predios_Negocio P_Parametros)
        {
            DataTable Tabla = new DataTable();
            Boolean Segundo_Filtro = false;

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            String Mi_SQL = "";

            try
            {
                Mi_SQL += "SELECT * FROM  " + Cat_Cor_Predios.Tabla_Cat_Cor_Predios;
                if (!String.IsNullOrEmpty(P_Parametros.P_Predio_ID))
                {
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE ";
                    Mi_SQL += Cat_Cor_Predios.Campo_Predio_Id + " = '" + P_Parametros.P_Predio_ID + "'";
                    Segundo_Filtro = true;
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_RPU))
                {
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE ";
                    Mi_SQL += Cat_Cor_Predios.Campo_RPU + " = '" + P_Parametros.P_RPU + "'";
                    Segundo_Filtro = true;
                }
                
                Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }


        public static DataTable Consultar_Usuarios(Cls_Cat_Cor_Predios_Negocio Parametros)
        {
            DataTable Tabla = new DataTable();
            Boolean Segundo_Filtro = false;

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            String Mi_SQL = "";
            
            try
            {
                Mi_SQL = "SELECT TOP 1 u." + Cat_Cor_Usuarios.Campo_Usuario_Id + " AS usuario_id";
                Mi_SQL += ",u." + Cat_Cor_Usuarios.Campo_Rfc + " AS rfc";
                Mi_SQL += ",u." + Cat_Cor_Usuarios.Campo_Razon_Social + " AS razon_social";
                Mi_SQL += ",u." + Cat_Cor_Usuarios.Campo_Razon_Social + " AS nombre_corto";
                Mi_SQL += ",isnull(u." + Cat_Cor_Usuarios.Campo_Pais + ", 'MEXICO') AS pais";
                Mi_SQL += ",u." + Cat_Cor_Usuarios.Campo_Correo_Electronico + " AS email";
                Mi_SQL += ",u." + Cat_Cor_Usuarios.Campo_Domicilio_Facturacion + " AS calle";
                Mi_SQL += ",u." + Cat_Cor_Usuarios.Campo_Colonia_Facturacion + " AS colonia";
                Mi_SQL += ",u." + Cat_Cor_Usuarios.Campo_CP_Facturacion + " AS cp";
                Mi_SQL += ",u." + Cat_Cor_Usuarios.Campo_Ciudad_Facturacion + " AS localidad";
                Mi_SQL += ",u." + Cat_Cor_Usuarios.Campo_No_Exterior_Facturacion + " AS numero_exterior";
                Mi_SQL += ",u." + Cat_Cor_Usuarios.Campo_No_Interior_Facturacion + " AS numero_interior";
                Mi_SQL += ",u." + Cat_Cor_Usuarios.Campo_Estado_Fiscal_Id + " AS estado";
                Mi_SQL += ",u." + Cat_Cor_Usuarios.Campo_Estado_Facturacion + " AS nombre_estado";
                Mi_SQL += ",u." + Cat_Cor_Usuarios.Campo_Ciudad_Fiscal_Id + " AS ciudad_id";
                Mi_SQL += ",u." + Cat_Cor_Usuarios.Campo_Ciudad_Facturacion + " AS ciudad";
                Mi_SQL += ",p.Predio_ID AS predio_id";
                Mi_SQL += ",'' AS cliente_id";
                Mi_SQL += ",'' AS estatus,isnull((select CLAVE from CAT_FE_USO_COMPROBANTES where USO_COMPROBANTES_ID = u.USO_CFDI), '') as uso_cfdi";
                Mi_SQL += " FROM " + Cat_Cor_Usuarios.Tabla_Cat_Cor_Usuarios + " u";
                Mi_SQL += " INNER JOIN " + Cat_Cor_Predios.Tabla_Cat_Cor_Predios + " p ON u.USUARIO_ID = p.Usuario_ID";

                if (!String.IsNullOrEmpty(Parametros.P_Predio_ID))
                {
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE ";
                    Mi_SQL += "p." + Cat_Cor_Predios.Campo_Predio_Id + " = '" + Parametros.P_Predio_ID + "'";
                    Segundo_Filtro = true;
                }
                if (!String.IsNullOrEmpty(Parametros.P_RPU)) {
                    Mi_SQL += Segundo_Filtro ? " AND " : " WHERE ";
                    Mi_SQL += "p." + Cat_Cor_Predios.Campo_RPU + " = '" + Parametros.P_RPU + "'";
                    Segundo_Filtro = true;
                }

                Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                //Tabla = SqlHelper.ExecuteDataset("Data Source=187.174.176.231;Initial Catalog=Simapag;User ID=dbcajas;Password=TellerMachine01", CommandType.Text, Mi_SQL.ToString()).Tables[0];

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }

        public static DataTable Consultar_Predios_Factura(Cls_Cat_Cor_Predios_Negocio Parametros)
        {
            DataTable Tabla = new DataTable();
            Boolean Segundo_Filtro = false;

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            String Mi_SQL = "";

            try
            {
                Mi_SQL = "SELECT Predio_ID, RPU, Usuario_ID, Estatus, Facturar";
                Mi_SQL += ",ISNULL(Cortado, 'NO') AS Cortado";
                Mi_SQL += ",ISNULL(Congelado, 'NO') AS Congelado";
                Mi_SQL += ",ISNULL(Cobranza, 'NO') AS Cobranza";
                Mi_SQL += ",ISNULL(Requerido, 'NO') AS Requerido";
                Mi_SQL += ",ISNULL(Bloqueado, 'NO') AS Bloqueado";
                Mi_SQL += ",ISNULL(Sancionado, 'NO') AS Sancionado";
                Mi_SQL += " FROM Cat_Cor_Predios";
                Mi_SQL += " WHERE RPU = '" + Parametros.P_RPU + "'";

                Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }
    }
}