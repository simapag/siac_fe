﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using SIAC.Metodos_Generales;
using SIAC.Constantes;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using SIAC_Cat_Apl_Parametros_Correo.Negocio;

namespace SIAC_Cat_Apl_Parametros_Correo.Datos
{
    public class Cls_Cat_Apl_Parametros_Correos_Datos
    {
        #region MÉTODOS
        /// ***********************************************************************************
        /// Nombre de la Función: Alta_Parametros
        /// Descripción         : Da de alta en la Base de Datos un nuevo Parametro
        /// Parámetros          : 
        /// Usuario Creo        : Miguel Angel Alvarado Enriquez.
        /// Fecha Creó          : 11/Marzo/2013 10:15 a.m.
        /// Usuario Modifico    :
        /// Fecha Modifico      :
        /// ***********************************************************************************
        public static Boolean Alta_Parametros(Cls_Apl_Parametros_Correo_Negocio P_Parametros)
        {
            Boolean Alta = false;
            StringBuilder Mi_sql = new StringBuilder();
            String Parametro_ID = "";
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;

            try
            {
                Parametro_ID = Cls_Metodos_Generales.Obtener_ID_Consecutivo(Apl_Parametros_Correos.Tabla_Apl_Parametros_Correo, Apl_Parametros_Correos.Campo_Parametro_Id, "", 5);

                Mi_sql.Append("INSERT INTO " + Apl_Parametros_Correos.Tabla_Apl_Parametros_Correo + "(");
                Mi_sql.Append(Apl_Parametros_Correos.Campo_Parametro_Id + ", ");
                Mi_sql.Append(Apl_Parametros_Correos.Campo_Email + ", ");
                Mi_sql.Append(Apl_Parametros_Correos.Campo_Contrasenia + ", ");
                Mi_sql.Append(Apl_Parametros_Correos.Campo_Host + ", ");
                Mi_sql.Append(Apl_Parametros_Correos.Campo_Puerto + ", ");
                Mi_sql.Append(Apl_Parametros_Correos.Campo_Imagen_Empresa + ", ");
                Mi_sql.Append(Apl_Parametros_Correos.Campo_Mensaje_Sistema + ", ");
                Mi_sql.Append(Apl_Parametros_Correos.Campo_Tipo_Movimiento);
                Mi_sql.Append(") VALUES ('");
                Mi_sql.Append(Parametro_ID + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Email + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Password + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Host + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Puerto + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Imagen_Empresa + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Mensaje_Sistema + "', ");
                Mi_sql.Append("'" + P_Parametros.P_Tipo_Movimiento);
                Mi_sql.Append(")");

                Obj_Comando.CommandText = Mi_sql.ToString();
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

                Alta = true;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Alta;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Parametro
        ///DESCRIPCIÓN         : Modifica en la Base de Datos un nuevo Parametro
        ///PARAMENTROS         : P_Parametros.- Instancia de la Clase de Negocio de Parametros con los datos que van a ser
        ///                                   modificados.
        ///CREO                : Miguel Angel Alvarado Enriquez.
        ///FECHA_CREO          : 11/Marzo/2013 11:12 a.m. 
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        public static Boolean Modificar_Parametro(Cls_Apl_Parametros_Correo_Negocio P_Parametros)
        {
            Boolean Modificado = false;
            StringBuilder Mi_sql = new StringBuilder();

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;

            try
            {
                Mi_sql.Append("UPDATE " + Apl_Parametros_Correos.Tabla_Apl_Parametros_Correo + " SET ");
                if (!String.IsNullOrEmpty(P_Parametros.P_Email))
                {
                    Mi_sql.Append(Apl_Parametros_Correos.Campo_Email + " = '" + P_Parametros.P_Email + "', ");
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Password))
                {
                    Mi_sql.Append(Apl_Parametros_Correos.Campo_Contrasenia + " = '" + P_Parametros.P_Password + "', ");
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Host))
                {
                    Mi_sql.Append(Apl_Parametros_Correos.Campo_Host + " = '" + P_Parametros.P_Host + "', ");
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Puerto))
                {
                    Mi_sql.Append(Apl_Parametros_Correos.Campo_Puerto + " = '" + P_Parametros.P_Puerto + "', ");
                }

                if (!String.IsNullOrEmpty(P_Parametros.P_Imagen_Empresa))
                {
                    Mi_sql.Append(Apl_Parametros_Correos.Campo_Imagen_Empresa + " = '" + P_Parametros.P_Imagen_Empresa + "', ");
                }

                if (!String.IsNullOrEmpty(P_Parametros.P_Mensaje_Sistema))
                {
                    Mi_sql.Append(Apl_Parametros_Correos.Campo_Mensaje_Sistema + " = '" + P_Parametros.P_Mensaje_Sistema + "', ");
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Tipo_Movimiento))
                {
                    Mi_sql.Append(Apl_Parametros_Correos.Campo_Tipo_Movimiento + " = '" + P_Parametros.P_Tipo_Movimiento + "', ");
                }
                Mi_sql.Append(Apl_Parametros_Correos.Campo_Fecha_Modifico + " = GETDATE()");
                Mi_sql.Append(" WHERE " + Apl_Parametros_Correos.Campo_Parametro_Id + " = '" + P_Parametros.P_Parametro_Id + "'");

                Obj_Comando.CommandText = Mi_sql.ToString();
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

                Modificado = true;

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Modificado;
        }
        ///**************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Parametros
        ///DESCRIPCIÓN         : Consulta los Pametros
        ///PARAMENTROS         : P_Parametros.- Instancia de la Clase de Negocio de Empresas con los datos que servirán de
        ///                                   filtro.
        ///CREO                : Miguel Angel Alvarado Eniquez.
        ///FECHA_CREO          : 15/Febrero/2013 
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        public static DataTable Consultar_Parametros(Cls_Apl_Parametros_Correo_Negocio P_Parametros)
        {
            DataTable Tabla = new DataTable();
            StringBuilder Mi_SQL = new StringBuilder();
            String Aux_Filtros = "";
            Boolean Segundo_Filtro = false;

            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;

            try
            {
                Mi_SQL.Append("SELECT * FROM  " + Apl_Parametros_Correos.Tabla_Apl_Parametros_Correo);
                if (!String.IsNullOrEmpty(P_Parametros.P_Email))
                {
                    Mi_SQL.Append(Segundo_Filtro ? " AND " : " WHERE ");
                    Mi_SQL.Append(Apl_Parametros_Correos.Campo_Email + " LIKE '%" + P_Parametros.P_Email + "%' ");
                    Segundo_Filtro = true;
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Password))
                {
                    Mi_SQL.Append(Segundo_Filtro ? " AND " : " WHERE ");
                    Mi_SQL.Append(Apl_Parametros_Correos.Campo_Contrasenia + " = '" + P_Parametros.P_Password + "' ");
                    Segundo_Filtro = true;
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Parametro_Id))
                {
                    Mi_SQL.Append(Segundo_Filtro ? " AND " : " WHERE ");
                    Mi_SQL.Append(Apl_Parametros_Correos.Campo_Parametro_Id + " = '" + P_Parametros.P_Parametro_Id + "'  ");
                    Segundo_Filtro = true;
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Host))
                {
                    Mi_SQL.Append(Segundo_Filtro ? " AND " : " WHERE ");
                    Mi_SQL.Append(Apl_Parametros_Correos.Campo_Host + " = '" + P_Parametros.P_Host + "' ");
                    Segundo_Filtro = true;
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Puerto))
                {
                    Mi_SQL.Append(Segundo_Filtro ? " AND " : " WHERE ");
                    Mi_SQL.Append(Apl_Parametros_Correos.Campo_Puerto + " = '" + P_Parametros.P_Puerto + "' ");
                    Segundo_Filtro = true;
                }
                if (!String.IsNullOrEmpty(P_Parametros.P_Imagen_Empresa))
                {
                    Mi_SQL.Append(Segundo_Filtro ? " AND " : " WHERE ");
                    Mi_SQL.Append(Apl_Parametros_Correos.Campo_Imagen_Empresa + " = '" + P_Parametros.P_Imagen_Empresa + "' ");
                    Segundo_Filtro = true;
                }
                if (Mi_SQL.ToString().EndsWith(" AND "))
                {
                    Aux_Filtros = Mi_SQL.ToString().Substring(0, Mi_SQL.Length - 5);
                    Mi_SQL.Length = 0;
                    Mi_SQL.Append(Aux_Filtros);
                }
                if (Mi_SQL.ToString().EndsWith(" WHERE "))
                {
                    Aux_Filtros = Mi_SQL.ToString().Substring(0, Mi_SQL.Length - 7);
                    Mi_SQL.Length = 0;
                    Mi_SQL.Append(Aux_Filtros);
                }
                // agregar filtro y orden a la consulta
                Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Tabla;
        }
        #endregion
    }
}