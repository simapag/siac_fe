﻿using Cls_Cat_Parametros.Negocio;
using SharpContent.ApplicationBlocks.Data;
using SIAC.Constantes;
using SIAC.Metodos_Generales;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de Cls_Cat_Parametros_Datos
/// </summary>
/// 
namespace Cls_Cat_Parametros.Datos
{
    public class Cls_Cat_Parametros_Datos
    {
        public static List<Cls_Cat_Parametros_Negocio> consultarParametros()
        {
            List<Cls_Cat_Parametros_Negocio> lista = new List<Cls_Cat_Parametros_Negocio>();
            String sql = string.Empty;

            sql = "SELECT NO_INTENTOS_ACCESO,NO_BLOQUEOS_TEMPORALES,MINUTOS_BLOQUEO_TEMPORAL FROM Cat_Cor_Parametros";
            DataTable Dt = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, sql).Tables[0];

            lista = Cls_Metodos_Generales.ConvertDataTableToList<Cls_Cat_Parametros_Negocio>(Dt);
            return lista;
        }
    }
}