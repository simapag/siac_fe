﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIAC.Catalogo_Comercializacion_Usuarios.Negocio;
using SIAC.Constantes;
using System.Data.SqlClient;
using System.Text;
using SharpContent.ApplicationBlocks.Data;
 
namespace SIAC.Catalogo_Comercializacion_Usuarios.Datos
{
    public class Cls_Cat_Cor_Usuarios_Datos
    {
        public Cls_Cat_Cor_Usuarios_Datos(){}

        public static String Alta_Usuarios(Cls_Cat_Cor_Usuarios_Negocio Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            Object Aux; //Variable auxiliar para las consultas
            String Mensaje = String.Empty; //Variable para el mensaje de error

            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Consultas para el ID
                Mi_SQL = "SELECT ISNULL(MAX(" + Cat_Cor_Usuarios.Campo_Usuario_Id + "), '0000000000') FROM " + Cat_Cor_Usuarios.Tabla_Cat_Cor_Usuarios + " WHERE Usuario_ID < 900000 ";

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Aux = Obj_Comando.ExecuteScalar();

                //Verificar si no es nulo
                if (Convert.IsDBNull(Aux) == false)
                    Datos.P_Usuario_Id = String.Format("{0:0000000000}", Convert.ToInt32(Aux) + 1);
                else
                    Datos.P_Usuario_Id = "0000000001";

                //Asignar consulta para la insercion
                Mi_SQL = "INSERT INTO " + Cat_Cor_Usuarios.Tabla_Cat_Cor_Usuarios + " (" +
                        Cat_Cor_Usuarios.Campo_Usuario_Id + ", " +
                        Cat_Cor_Usuarios.Campo_Estado_ID + ", " +
                        Cat_Cor_Usuarios.Campo_Ciudad_ID + ", " +
                        Cat_Cor_Usuarios.Campo_Nombre + ", " +
                        Cat_Cor_Usuarios.Campo_Apellido_Paterno + ", " +
                        Cat_Cor_Usuarios.Campo_Apellido_Materno + ", " +
                        Cat_Cor_Usuarios.Campo_Rfc + ", " +
                        Cat_Cor_Usuarios.Campo_Razon_Social + ", " +
                        Cat_Cor_Usuarios.Campo_Calle + ", " +
                        Cat_Cor_Usuarios.Campo_Colonia + ", " +
                        Cat_Cor_Usuarios.Campo_Calle_Id + ", " +
                        Cat_Cor_Usuarios.Campo_Colonia_Id + ", " +
                        Cat_Cor_Usuarios.Campo_Pais + ", " +
                        Cat_Cor_Usuarios.Campo_No_Interior + ", " +
                        Cat_Cor_Usuarios.Campo_No_Exterior + ", " +
                        Cat_Cor_Usuarios.Campo_Telefono_Casa + ", " +
                        Cat_Cor_Usuarios.Campo_Telefono_Oficina + ", " +
                        Cat_Cor_Usuarios.Campo_Telefono_Celular + ", " +
                        Cat_Cor_Usuarios.Campo_Telefono_Nextel + ", " +
                        Cat_Cor_Usuarios.Campo_Correo_Electronico + ", " +
                        Cat_Cor_Usuarios.Campo_Estatus + ", " +
                        Cat_Cor_Usuarios.Campo_Folio_IFE + ", " +
                        Cat_Cor_Usuarios.Campo_Usuario_Creo + ", " +
                        Cat_Cor_Usuarios.Campo_Fecha_Creo + ")" +
                        " VALUES( " +
                        "'" + Datos.P_Usuario_Id + "', " +
                        "'" + Datos.P_Estado_ID + "', " +
                        "'" + Datos.P_Ciudad_ID + "', " +
                        "'" + Datos.P_Nombre + "', " +
                        "'" + Datos.P_Apellido_Paterno + "', " +
                        "'" + Datos.P_Apellido_Materno + "', " +
                        "'" + Datos.P_Rfc + "', " +
                        "'" + Datos.P_Razon_Social + "', " +
                        "'" + Datos.P_Calle + "', " +
                        "'" + Datos.P_Colonia + "', " +
                        "'" + Datos.P_Calle_Id + "', " +
                        "'" + Datos.P_Colonia_Id + "', " +
                        "'" + Datos.P_Pais + "', " +
                        "'" + Datos.P_No_Interior + "', " +
                        "'" + Datos.P_No_Exterior + "', " +
                        "'" + Datos.P_Telefono_Casa + "', " +
                        "'" + Datos.P_Telefono_Oficina + "', " +
                        "'" + Datos.P_Telefono_Celular + "', " +
                        "'" + Datos.P_Telefono_Nextel + "', " +
                        "'" + Datos.P_Correo_Electronico + "', " +
                        "'ACTIVO', " +
                        "'" + Datos.P_Folio_IFE + "', " +
                        "'" + Datos.P_Usuario_Creo + "', " +
                        " GETDATE() )";
                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                ///Damos de alta el nuevo usuario como cliente de facturación
                String Nombre = "";
                Nombre = Datos.P_Nombre + " " + Datos.P_Apellido_Paterno + " " + Datos.P_Apellido_Materno;
                Datos.P_Razon_Social = Nombre;
                //if (String.IsNullOrEmpty(Datos.P_Razon_Social))
                //    Nombre = Datos.P_Nombre + " " + Datos.P_Apellido_Paterno + " " + Datos.P_Apellido_Materno;
                //else
                //    Nombre = Datos.P_Razon_Social;

                Alta_Clientes_Facturacion(Datos, Nombre, ref Obj_Comando);

                //Ejecutar transaccion
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

                return Datos.P_Nombre + " " + Datos.P_Apellido_Paterno + " " + Datos.P_Apellido_Materno;
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Baja_Usuarios
        /// DESCRIPCION:            Eliminar un Usuario existente de la base de datos
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene los datos a eliminar
        /// CREO       :            Raúl Raya Ortega
        /// FECHA_CREO :            20/Julio/2011 
        /// MODIFICO          :     Diego N. Yañez Rodriguez
        /// FECHA_MODIFICO    :     02 Sept 2011
        /// CAUSA_MODIFICACION:     Se agregron campos adicionales a la tabla de usuarios
        ///*******************************************************************************/
        public static void Baja_Usuarios(Cls_Cat_Cor_Usuarios_Negocio Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            String Mensaje = String.Empty; //Variable para el mensaje de error

            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Asignar consulta para la eliminacion
                Mi_SQL = " DELETE FROM " + Cat_Cor_Usuarios.Tabla_Cat_Cor_Usuarios + " ";
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Cor_Usuarios.Campo_Usuario_Id + " = '" + Datos.P_Usuario_Id + "'";
  
                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                //Ejecutar transaccion
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Modifica_Usuarios
        /// DESCRIPCION:            Modificar un Usuario existente de la base de datos
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene los datos a modificar
        /// CREO       :            Raúl Raya Ortega
        /// FECHA_CREO :            29/Julio/2011  
        /// MODIFICO          :     Diego N. Yañez Rodriguez
        /// FECHA_MODIFICO    :     02 Sept 2011
        /// CAUSA_MODIFICACION:     Se agregron campos adicionales a la tabla de usuarios
        ///*******************************************************************************/
        public static void Modifica_Usuarios(Cls_Cat_Cor_Usuarios_Negocio Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            String Mensaje = String.Empty; //Variable para el mensaje de error

            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Asignar consulta para la modificacion
                Mi_SQL = "UPDATE " + Cat_Cor_Usuarios.Tabla_Cat_Cor_Usuarios + " SET "; 
                    if (!String.IsNullOrEmpty(Datos.P_Estado_ID))
                      Mi_SQL +=  Cat_Cor_Usuarios.Campo_Estado_ID + " = " + "'" + Datos.P_Estado_ID + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_Ciudad_ID))
                    Mi_SQL += Cat_Cor_Usuarios.Campo_Ciudad_ID + " = " + "'" + Datos.P_Ciudad_ID + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_Nombre))
                    Mi_SQL += Cat_Cor_Usuarios.Campo_Nombre + " = " + "'" + Datos.P_Nombre + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_Apellido_Paterno))
                    Mi_SQL += Cat_Cor_Usuarios.Campo_Apellido_Paterno + " = " + "'" + Datos.P_Apellido_Paterno + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_Apellido_Materno))
                    Mi_SQL += Cat_Cor_Usuarios.Campo_Apellido_Materno + " = " + "'" + Datos.P_Apellido_Materno + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_Rfc))
                    Mi_SQL += Cat_Cor_Usuarios.Campo_Rfc + " = " + "'" + Datos.P_Rfc + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_Razon_Social))
                    Mi_SQL += Cat_Cor_Usuarios.Campo_Razon_Social + " = " + "'" + Datos.P_Razon_Social + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_Calle))
                    Mi_SQL += Cat_Cor_Usuarios.Campo_Calle + " = " + "'" + Datos.P_Calle + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_Colonia))
                    Mi_SQL += Cat_Cor_Usuarios.Campo_Colonia + " = " + "'" + Datos.P_Colonia + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_Calle_Id))
                    Mi_SQL += Cat_Cor_Usuarios.Campo_Calle_Id + " = " + "'" + Datos.P_Calle_Id + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_Colonia_Id))
                    Mi_SQL += Cat_Cor_Usuarios.Campo_Colonia_Id + " = " + "'" + Datos.P_Colonia_Id + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_Pais))
                    Mi_SQL += Cat_Cor_Usuarios.Campo_Pais + " = " + "'" + Datos.P_Pais + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_No_Interior))
                    Mi_SQL += Cat_Cor_Usuarios.Campo_No_Interior + " = " + "'" + Datos.P_No_Interior + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_No_Exterior))
                    Mi_SQL += Cat_Cor_Usuarios.Campo_No_Exterior + " = " + "'" + Datos.P_No_Exterior + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_Telefono_Casa))
                    Mi_SQL += Cat_Cor_Usuarios.Campo_Telefono_Casa + " = " + "'" + Datos.P_Telefono_Casa + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_Telefono_Oficina))
                    Mi_SQL += Cat_Cor_Usuarios.Campo_Telefono_Oficina + " = " + "'" + Datos.P_Telefono_Oficina + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_Telefono_Celular))
                    Mi_SQL += Cat_Cor_Usuarios.Campo_Telefono_Celular + " = " + "'" + Datos.P_Telefono_Celular + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_Telefono_Nextel))
                    Mi_SQL += Cat_Cor_Usuarios.Campo_Telefono_Nextel + " = " + "'" + Datos.P_Telefono_Nextel + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_Correo_Electronico))
                    Mi_SQL += Cat_Cor_Usuarios.Campo_Correo_Electronico + " = " + "'" + Datos.P_Correo_Electronico + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_Estatus))
                    Mi_SQL += Cat_Cor_Usuarios.Campo_Estatus + " = " + "'" + Datos.P_Estatus + "', ";
                    Mi_SQL += Cat_Cor_Usuarios.Campo_Usuario_Modifico + " = " + "'" + Datos.P_Usuario_Modifico + "', ";
                    Mi_SQL += Cat_Cor_Usuarios.Campo_Fecha_Modifico + " = GETDATE()";
                    Mi_SQL += " WHERE " + Cat_Cor_Usuarios.Campo_Usuario_Id + " = " + "'" + Datos.P_Usuario_Id + "'";
                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                //Ejecutar transaccion
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Usuarios
        /// DESCRIPCION:            Realizar la consulta de los giros por criterio de busqueda o por un ID
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene los datos a modificar
        /// CREO       :            Raúl Raya Ortega
        /// FECHA_CREO :            29/Julio/2011
        /// MODIFICO          :     Diego N. Yañez Rodriguez
        /// FECHA_MODIFICO    :     02 Sept 2011
        /// CAUSA_MODIFICACION:     Se agregron campos adicionales a la tabla de usuarios
        ///*******************************************************************************/
        public static DataTable Consulta_Usuarios(Cls_Cat_Cor_Usuarios_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;
            //Nombre, Apellido Paterno, Apellido_Materno, , por Calle, Colonia, por Ciudad por Pais, RFC, Razon SOcial
            try
            {
                //Asignar consulta para las Usuarios
                Mi_SQL = "SELECT * FROM (SELECT row_number() OVER (ORDER BY CCU.RFC, CCU.Apellido_Paterno) AS rownum," +
                        " CCU." + Cat_Cor_Usuarios.Campo_Usuario_Id + ", " +
                        " CCU." + Cat_Cor_Usuarios.Campo_Estado_ID + ", " + "CCU." + Cat_Cor_Usuarios.Campo_Ciudad_ID + ", " +
                        " (CCU." + Cat_Cor_Usuarios.Campo_Apellido_Paterno + " +' '+" +
                        " CCU." + Cat_Cor_Usuarios.Campo_Apellido_Materno + " +' '+" +
                        " CCU." + Cat_Cor_Usuarios.Campo_Nombre + ") as Nombre_Completo," +
                        " CCU." + Cat_Cor_Usuarios.Campo_Nombre + ", " + "CCU." + Cat_Cor_Usuarios.Campo_Apellido_Paterno + ", " +
                        " CCU." + Cat_Cor_Usuarios.Campo_Apellido_Materno + ", " + "CCU." + Cat_Cor_Usuarios.Campo_Rfc + ", " +
                        " CCU." + Cat_Cor_Usuarios.Campo_Razon_Social + ", " + "CCU." + Cat_Cor_Usuarios.Campo_Calle + ", " +
                        " CCU." + Cat_Cor_Usuarios.Campo_Colonia + ", " + "CCU." + Cat_Cor_Usuarios.Campo_Calle_Id + ", " +
                        " CCU." + Cat_Cor_Usuarios.Campo_Colonia_Id + ", " + "CCU." + Cat_Cor_Usuarios.Campo_Pais + ", " +
                        " CCU." + Cat_Cor_Usuarios.Campo_No_Interior + ", " + "CCU." + Cat_Cor_Usuarios.Campo_No_Exterior + ", " +
                        " CCU." + Cat_Cor_Usuarios.Campo_Telefono_Casa + ", " + "CCU." + Cat_Cor_Usuarios.Campo_Telefono_Oficina + ", " +
                        " CCU." + Cat_Cor_Usuarios.Campo_Telefono_Celular + ", " + "CCU." + Cat_Cor_Usuarios.Campo_Telefono_Nextel + ", " +
                        " CCU." + Cat_Cor_Usuarios.Campo_Correo_Electronico + ", " + "CCU." + Cat_Cor_Usuarios.Campo_Estatus + ", " +
                        " CCU." + Cat_Cor_Usuarios.Campo_Folio_IFE + " AS FOLIO_IFE, " +
                        " ISNULL(CCE." + Cat_Cor_Estados.Campo_Nombre + ",'') as ESTADO, " +
                        " ISNULL(CCC." + Cat_Cor_Ciudades.Campo_Nombre + ",'') as CIUDAD, Col.Codigo_Postal " +
                        " FROM " + Cat_Cor_Usuarios.Tabla_Cat_Cor_Usuarios + " CCU LEFT JOIN " +
                        " Cat_Cor_Predios CCP ON  CCU.USUARIO_ID = CCP.Usuario_ID LEFT JOIN " +
                        "" + Cat_Cor_Estados.Tabla_Cat_Cor_Estados + " CCE ON CCU.ESTADO_ID = CCE.ESTADO_ID LEFT JOIN " +
                        "" + Cat_Cor_Ciudades.Tabla_Cat_Cor_Ciudades + " CCC ON CCU.CIUDAD_ID = CCC.CIUDAD_ID left join Cat_Cor_Colonias col" +
                        " on col.COLONIA_ID=CCU.Colonia_ID " +
                        " WHERE CCU.ESTATUS IS NOT NULL ";

                //Filtros
                if (!string.IsNullOrEmpty(Datos.P_Nombre_Completo))
                {
                    Mi_SQL += "AND (LTRIM(RTRIM(CCU.APELLIDO_PATERNO)) + ' ' + " +
                              "LTRIM(RTRIM(CCU.APELLIDO_MATERNO)) + ' ' + " +
                              "LTRIM(RTRIM(CCU.NOMBRE))) " +
                              "LIKE '%" + Datos.P_Nombre_Completo + "%' ";
                }
                else if (Datos.P_Busqueda != null && !Datos.P_Busqueda.Equals(""))
                {
                    Mi_SQL += " AND (CCU." + Cat_Cor_Usuarios.Campo_Nombre + " LIKE '%" + Datos.P_Busqueda + "%'" +
                              " OR CCU." + Cat_Cor_Usuarios.Campo_Apellido_Paterno + " LIKE '%" + Datos.P_Busqueda + "%'" +
                              " OR CCU." + Cat_Cor_Usuarios.Campo_Apellido_Materno + " LIKE '%" + Datos.P_Busqueda + "%'" +
                              " OR CCU." + Cat_Cor_Usuarios.Campo_Rfc + " LIKE '%" + Datos.P_Busqueda + "%'" +
                              " OR CCU." + Cat_Cor_Usuarios.Campo_Razon_Social + " LIKE '%" + Datos.P_Busqueda + "%')";
                }
                else
                {
                    if (Datos.P_Usuario_Id != null && !Datos.P_Usuario_Id.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Usuario_Id + " = '" + Datos.P_Usuario_Id + "'";
                    }
                    if (Datos.P_Nombre != null && !Datos.P_Nombre.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Nombre + " LIKE '%" + Datos.P_Nombre + "%'";
                    }
                    if (Datos.P_Apellido_Paterno != null && !Datos.P_Apellido_Paterno.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Apellido_Paterno + " LIKE '%" + Datos.P_Apellido_Paterno + "%'";
                    }
                    if (Datos.P_Apellido_Materno != null && !Datos.P_Apellido_Materno.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Apellido_Materno + " LIKE '%" + Datos.P_Apellido_Materno + "%'";
                    }
                    if (Datos.P_Rfc != null && !Datos.P_Rfc.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Rfc + " LIKE '%" + Datos.P_Rfc + "%'";
                    }
                    if (Datos.P_Razon_Social != null && !Datos.P_Razon_Social.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Razon_Social + " LIKE '%" + Datos.P_Razon_Social + "%'";
                    }
                    if (Datos.P_Estado_ID != null && !Datos.P_Estado_ID.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Estado_ID + " = '" + Datos.P_Estado_ID + "'";
                    }
                    if (Datos.P_Ciudad_ID != null && !Datos.P_Ciudad_ID.Equals(""))
                    {
                        Mi_SQL += " AND CCP.No_Cuenta = " + Datos.P_Ciudad_ID + "";
                    }
                    if (Datos.P_Colonia_Id != null && !Datos.P_Colonia_Id.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Colonia_Id + " = '" + Datos.P_Colonia_Id + "'";
                    }
                    if (Datos.P_Calle_Id != null && !Datos.P_Calle_Id.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Calle_Id + " = '" + Datos.P_Calle_Id + "'";
                    }
                    if (Datos.P_Colonia != null && !Datos.P_Colonia.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Colonia + " LIKE '%" + Datos.P_Colonia + "%'";
                    }
                    if (Datos.P_Calle != null && !Datos.P_Calle.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Calle + " LIKE '%" + Datos.P_Calle + "%'";
                    }
                }
                Mi_SQL += ") AS A WHERE A.rownum BETWEEN (" + (Datos.P_Cantidad * (Datos.P_Pagina - 1)) + ") AND (" + (Datos.P_Cantidad * (Datos.P_Pagina)) + ") ";
                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Usuarios_Cantidad
        /// DESCRIPCION:            Realiza la consulta de usuario de acuerdo a los filtros y arroja la cantidad de usuarios
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene los datos a modificar
        /// CREO       :            Diego N. Yañez Rodriguez
        /// FECHA_CREO :            02 Sept 2011
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:     
        ///*******************************************************************************/
        public static string Consulta_Usuarios_Cantidad(Cls_Cat_Cor_Usuarios_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;
            //Nombre, Apellido Paterno, Apellido_Materno, , por Calle, Colonia, por Ciudad por Pais, RFC, Razon SOcial
            try
            {
                //Asignar consulta para las Usuarios
                Mi_SQL = "SELECT COUNT(*) as Usuarios " +
                        " FROM " + Cat_Cor_Usuarios.Tabla_Cat_Cor_Usuarios + " CCU," +
                        "" + Cat_Cor_Estados.Tabla_Cat_Cor_Estados + " CCE, " +
                        "" + Cat_Cor_Ciudades.Tabla_Cat_Cor_Ciudades + " CCC " +
                        " WHERE CCU." + Cat_Cor_Usuarios.Campo_Ciudad_ID + " = CCC." + Cat_Cor_Ciudades.Campo_Ciudad_ID +
                        " AND CCU." + Cat_Cor_Usuarios.Campo_Estado_ID + " = CCE." + Cat_Cor_Estados.Campo_Estado_ID +
                        " AND CCE." + Cat_Cor_Ciudades.Campo_Estado_ID + " = CCC." + Cat_Cor_Estados.Campo_Estado_ID;
                //Filtros
                if (Datos.P_Busqueda != null && !Datos.P_Busqueda.Equals(""))
                {
                    Mi_SQL += " AND (CCU." + Cat_Cor_Usuarios.Campo_Nombre + " LIKE '%" + Datos.P_Busqueda + "%'" +
                              " OR CCU." + Cat_Cor_Usuarios.Campo_Apellido_Paterno + " LIKE '%" + Datos.P_Busqueda + "%'" +
                              " OR CCU." + Cat_Cor_Usuarios.Campo_Apellido_Materno + " LIKE '%" + Datos.P_Busqueda + "%'" +
                              " OR CCU." + Cat_Cor_Usuarios.Campo_Rfc + " LIKE '%" + Datos.P_Busqueda + "%'" +
                              " OR CCU." + Cat_Cor_Usuarios.Campo_Razon_Social + " LIKE '%" + Datos.P_Busqueda + "%')";

                }
                else
                {
                    if (Datos.P_Usuario_Id != null && !Datos.P_Usuario_Id.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Usuario_Id + " = '" + Datos.P_Usuario_Id + "'";
                    }
                    if (Datos.P_Nombre != null && !Datos.P_Nombre.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Nombre + " LIKE '%" + Datos.P_Nombre + "%'";
                    }
                    if (Datos.P_Apellido_Paterno != null && !Datos.P_Apellido_Paterno.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Apellido_Paterno + " LIKE '%" + Datos.P_Apellido_Paterno + "%'";
                    }
                    if (Datos.P_Apellido_Materno != null && !Datos.P_Apellido_Materno.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Apellido_Materno + " LIKE '%" + Datos.P_Apellido_Materno + "%'";
                    }
                    if (Datos.P_Rfc != null && !Datos.P_Rfc.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Rfc + " LIKE '%" + Datos.P_Rfc + "%'";
                    }
                    if (Datos.P_Razon_Social != null && !Datos.P_Razon_Social.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Razon_Social + " LIKE '%" + Datos.P_Razon_Social + "%'";
                    }
                    if (Datos.P_Estado_ID != null && !Datos.P_Estado_ID.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Estado_ID + " = '" + Datos.P_Estado_ID + "'";
                    }
                    if (Datos.P_Ciudad_ID != null && !Datos.P_Ciudad_ID.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Ciudad_ID + " = '" + Datos.P_Ciudad_ID + "'";
                    }
                    if (Datos.P_Colonia_Id != null && !Datos.P_Colonia_Id.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Colonia_Id + " = '" + Datos.P_Colonia_Id + "'";
                    }
                    if (Datos.P_Calle_Id != null && !Datos.P_Calle_Id.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Calle_Id + " = '" + Datos.P_Calle_Id + "'";
                    }
                    if (Datos.P_Colonia != null && !Datos.P_Colonia.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Colonia + " LIKE '%" + Datos.P_Colonia + "%'";
                    }
                    if (Datos.P_Calle != null && !Datos.P_Calle.Equals(""))
                    {
                        Mi_SQL += " AND CCU." + Cat_Cor_Usuarios.Campo_Calle + " LIKE '%" + Datos.P_Calle + "%'";
                    }
                }
                
                string Cantidad = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).ToString();
                return Cantidad;
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }


        public static DataTable obtenerColonias(string nombre_colonia, int limte) {
           
            string My_SQL="";

            My_SQL = "SELECT  * from ( " +
                  "SELECT COLONIA_ID as [colonia_id], NOMBRE as [colonia], ROW_NUMBER() OVER(ORDER BY NOMBRE) as ROW " +
                  "FROM Cat_Cor_Colonias " +
                  "WHERE NOMBRE LIKE '%" + nombre_colonia + "%' " +
                  ") as A " +
                  "WHERE A.ROW>=1 and A.ROW<=" + limte + "";

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, My_SQL).Tables[0];
            
        }

        public static DataTable obtenerCalles( string nombre_calle , int limite) {

         
            string My_SQL;

            My_SQL = "SELECT * from ( " +
                    "SELECT CALLE_ID as [calle_id], NOMBRE as [calle], ROW_NUMBER() OVER(ORDER BY NOMBRE) as ROW " +
                    "from Cat_Cor_Calles " +
                   "WHERE NOMBRE LIKE '%" + nombre_calle  + "%' " +
                   ") as A " +
                   "WHERE A.ROW>=1 and A.ROW<=" + limite + "";

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, My_SQL).Tables[0];
            

            
        }


        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_UsuariosxCampo
        /// DESCRIPCION:            Realizar la consulta de los usuarios por un campo especifico y regresa ese campo 
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene los datos a modificar
        /// CREO       :            Diego N. Yañez Rodriguez
        /// FECHA_CREO :            02 Sept 2011
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:     
        ///*******************************************************************************/
        public static DataTable Consulta_UsuariosxCampo(Cls_Cat_Cor_Usuarios_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;
            //Nombre, Apellido Paterno, Apellido_Materno, , por Calle, Colonia, por Ciudad por Pais, RFC, Razon SOcial
            try
            {
                //Asignar consulta para las Usuarios
                Mi_SQL = "SElECT DISTINCT " + Datos.P_Campo + "" +
                         " FROM Cat_Cor_Usuarios " +
                         " WHERE " + Datos.P_Campo + " like '%" + Datos.P_Busqueda + "%'" +
                         " ORDER BY " + Datos.P_Campo + "";
                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Datos">Variable de enlace con la capa de negocios. Almacena toda la informacion del usuario a registrar</param>
        /// <param name="Nombre">Almacena el nombre completo o razon social del usuario</param>
        /// <param name="Comando">Comando SQL que ejeciutara las sentencias</param>
        public static void Alta_Clientes_Facturacion(Cls_Cat_Cor_Usuarios_Negocio Datos, String Nombre, ref SqlCommand Comando)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            String Ciudad = String.Empty;
            String CP = String.Empty;
            object Max_Cliente = null;
            String Cliente_ID = String.Empty;
            try
            {
                #region(Max CLIENTE)
                Mi_Sql.Append("SELECT ISNULL(MAX(CLIENTE_ID),'0000000000') FROM CAT_ADM_CLIENTES");
                Max_Cliente = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());

                if (!Convert.IsDBNull(Max_Cliente))
                    Cliente_ID = String.Format("{0:0000000000}", Convert.ToInt32(Max_Cliente) + 1);
                else
                    Cliente_ID = String.Format("{0:0000000000}", 1);
                #endregion

                #region(Consulta CP y Ciudad)
                if (!String.IsNullOrEmpty(Datos.P_Colonia_Id))
                {
                    Mi_Sql = new StringBuilder();

                    Mi_Sql.Append("SELECT CODIGO_POSTAL FROM Cat_Cor_Colonias WHERE COLONIA_ID ='" + Datos.P_Colonia_Id + "'");

                    CP = (SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString())).ToString().Trim(); 
                }
                if (!String.IsNullOrEmpty(Datos.P_Ciudad_ID))
                {
                    Mi_Sql = new StringBuilder();

                    Mi_Sql.Append("SELECT NOMBRE FROM CAT_COR_CIUDADES WHERE CIUDAD_ID ='" + Datos.P_Ciudad_ID + "'");

                    Ciudad = (SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString())).ToString().Trim();
                }

                #endregion
                
                #region(Datos a Insertar)
                Mi_Sql = new StringBuilder();
                Mi_Sql.Append("INSERT INTO CAT_ADM_CLIENTES(");
                Mi_Sql.Append("CLIENTE_ID, ");
                Mi_Sql.Append("USUARIO_ID, ");
                Mi_Sql.Append("ESTADO_ID, ");
                Mi_Sql.Append("NOMBRE_CORTO, ");
                Mi_Sql.Append("RAZON_SOCIAL, ");
                Mi_Sql.Append("RFC, ");
                Mi_Sql.Append("CURP, ");
                Mi_Sql.Append("PAIS, ");
                Mi_Sql.Append("LOCALIDAD, ");
                Mi_Sql.Append("COLONIA, ");
                Mi_Sql.Append("CIUDAD, ");
                Mi_Sql.Append("CALLE, ");
                Mi_Sql.Append("NUMERO_EXTERIOR, ");
                Mi_Sql.Append("NUMERO_INTERIOR, ");
                Mi_Sql.Append("CP, ");
                Mi_Sql.Append("FAX, ");
                Mi_Sql.Append("TELEFONO_1, ");
                Mi_Sql.Append("TELEFONO_2, ");
                Mi_Sql.Append("EXTENSION, ");
                Mi_Sql.Append("NEXTEL, ");
                Mi_Sql.Append("EMAIL, ");
                Mi_Sql.Append("ESTATUS, ");// = 'ACTIVO'
                Mi_Sql.Append("USUARIO_CREO, ");
                Mi_Sql.Append("FECHA_CREO");
                #endregion
                
                #region(Valores a Insertar)
                Mi_Sql.Append(") VALUES(");
                Mi_Sql.Append("'" + Cliente_ID + "', "); // cliente id

                if (!String.IsNullOrEmpty(Datos.P_Usuario_Id)) // usuario_id
                    Mi_Sql.Append("'" + Datos.P_Usuario_Id.ToString().Trim() + "', ");
                else
                    Mi_Sql.Append("NULL, ");


                if (!String.IsNullOrEmpty(Datos.P_Estado_ID)) // estado_id
                    Mi_Sql.Append("'" + Datos.P_Estado_ID.ToString().Trim() + "', ");
                else
                    Mi_Sql.Append("NULL, ");


                if (!String.IsNullOrEmpty(Nombre))
                {
                    Mi_Sql.Append("'" + Nombre.Trim() + "', ");// nombre_corto
                    Mi_Sql.Append("'" + Nombre.Trim() + "', ");// razon social
                }
                else
                {
                    Mi_Sql.Append("NULL, ");
                    Mi_Sql.Append("NULL, ");
                }


                if (!String.IsNullOrEmpty(Datos.P_Rfc)) // rfc
                    Mi_Sql.Append("'" + Datos.P_Rfc.ToString().Trim() + "', ");
                else
                    Mi_Sql.Append("NULL, ");


                Mi_Sql.Append("NULL, "); // curp

                if (!String.IsNullOrEmpty(Datos.P_Pais)) // pais
                    Mi_Sql.Append("'" + Datos.P_Pais + "', ");
                else
                    Mi_Sql.Append("NULL, ");


                if (!String.IsNullOrEmpty(Datos.P_Ciudad_ID)) // localidad
                    Mi_Sql.Append("'" + Ciudad + "', ");
                else
                    Mi_Sql.Append("NULL, ");


                if (!String.IsNullOrEmpty(Datos.P_Colonia)) // colonia
                    Mi_Sql.Append("'"+Datos.P_Colonia.ToString().Trim()+"', ");
                else
                    Mi_Sql.Append("NULL, ");


                if (!String.IsNullOrEmpty(Datos.P_Ciudad_ID)) // ciudad
                    Mi_Sql.Append("'" + Ciudad + "', ");
                else
                    Mi_Sql.Append("NULL, ");


                if (!String.IsNullOrEmpty(Datos.P_Calle)) // calle
                    Mi_Sql.Append("'" + Datos.P_Calle.ToString().Trim() + "', ");
                else
                    Mi_Sql.Append("NULL, ");

                if (!String.IsNullOrEmpty(Datos.P_No_Exterior)) // numero_exterior
                    Mi_Sql.Append("'" + Datos.P_No_Exterior.ToString().Trim() + "', ");
                else
                    Mi_Sql.Append("NULL, ");

                if (!String.IsNullOrEmpty(Datos.P_No_Interior)) // numero_interior
                    Mi_Sql.Append("'" + Datos.P_No_Interior.ToString().Trim() + "', ");
                else
                    Mi_Sql.Append("NULL, ");

                if (!String.IsNullOrEmpty(CP)) // codigo_postal
                    Mi_Sql.Append("'" + CP.Trim() + "', ");
                else
                    Mi_Sql.Append("NULL, ");


                Mi_Sql.Append("NULL, ");//FAx

                //Mi_Sql.Append("NULL, ");

                if (!String.IsNullOrEmpty(Datos.P_Telefono_Casa)) // telefono1
                    Mi_Sql.Append("'" + Datos.P_Telefono_Casa.ToString().Trim() + "', ");
                else
                    Mi_Sql.Append("NULL, ");

                if (!String.IsNullOrEmpty(Datos.P_Telefono_Celular)) //telefono2
                    Mi_Sql.Append("'" + Datos.P_Telefono_Celular.ToString().Trim() + "', ");
                else
                    Mi_Sql.Append("NULL, ");

                Mi_Sql.Append("NULL, "); // extension

                if (!String.IsNullOrEmpty(Datos.P_Telefono_Nextel)) // nextel
                    Mi_Sql.Append("'" + Datos.P_Telefono_Nextel.ToString().Trim() + "', ");
                else
                    Mi_Sql.Append("NULL, ");

                if (!String.IsNullOrEmpty(Datos.P_Correo_Electronico)) // email
                    Mi_Sql.Append("'" + Datos.P_Correo_Electronico.ToString().Trim() + "', ");
                else
                    Mi_Sql.Append("NULL, ");

                Mi_Sql.Append("'ACTIVO', "); // estatus

                if (!String.IsNullOrEmpty(Datos.P_Usuario_Creo)) // usuario creo
                    Mi_Sql.Append("'" + Datos.P_Usuario_Creo.ToString().Trim() + "', ");
                else
                    Mi_Sql.Append("NULL, ");

                Mi_Sql.Append("GETDATE())");
                #endregion

                Comando.CommandText = Mi_Sql.ToString();
                Comando.ExecuteNonQuery();
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al general el alta del usuario como cliente de facturacion.[" + Ex.Message + "]");
            }
        }
    }// fin de la clase Cls_Cat_Cor_Usuarios_Datos
}// fin namespace

