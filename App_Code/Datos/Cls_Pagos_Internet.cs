﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIAC.Constantes;
using System.Data.SqlClient;
using System.Data;
using SharpContent.ApplicationBlocks.Data;
using SIAC.Pagos_Internet.Bean;

namespace SIAC.Pagos_Internet.Datos
{
    public class Cls_Pagos_Internet
    {
        string strsql;
        string conexion = Cls_Constantes.Str_Conexion;



        public void Actualizar_Correo_Electronico(string numero_cuenta,string correo_electronico) {

            strsql = "UPDATE Cat_Cor_Usuarios set CORREO_ELECTRONICO='" + correo_electronico + "' where Usuario_ID = " +
                     "(SELECT top 1 u.USUARIO_ID " +
                     "FROM Cat_Cor_Usuarios u JOIn Cat_Cor_Predios_Usuarios pu " +
                     "on u.USUARIO_ID=pu.Usuario_ID  " +
                     "WHERE pu.No_Cuenta=" + numero_cuenta + ")";
            SqlHelper.ExecuteNonQuery(conexion, CommandType.Text, strsql);

        }

        public string Buscar_Correo_Electronico(string numero_cuenta) {
            string respuesta="";

            strsql = "SELECT top 1 ISNULL( u.CORREO_ELECTRONICO,'') as [correo],pu.No_Cuenta " +
                     "FROM Cat_Cor_Usuarios u JOIn Cat_Cor_Predios_Usuarios pu " +
                     "on u.USUARIO_ID=pu.Usuario_ID  " +
                     "WHERE pu.No_Cuenta=" + numero_cuenta + "";
            respuesta = Convert.ToString( SqlHelper.ExecuteScalar(conexion, CommandType.Text, strsql));
            return respuesta;

        }

        public string Elaborar_Refacturacion(string predio_id, int year, int bimestre, string usuario_creo) {

            //Declaracion de variables
            string Resultado = string.Empty; //variable para el resultado
            DataTable Dt_Predios = new DataTable(); //tabla para los predios que se van a facturar
            SqlTransaction Obj_Transaccion;
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            SqlCommand Obj_Comando = new SqlCommand();
            object aux; //variable auxiliar para los objetos

            //Abrir la conexion
            Obj_Conexion.Open();
            Obj_Transaccion = Obj_Conexion.BeginTransaction();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;

            //Ejecutar la facturacion del predio
            Obj_Comando.CommandType = CommandType.StoredProcedure;
            Obj_Comando.CommandText = "SP_Cor_Refacturar_Predio";
            Obj_Comando.Parameters.Clear();
            Obj_Comando.Parameters.AddWithValue("Predio_ID", predio_id);
            Obj_Comando.Parameters.AddWithValue("Anio", year);
            Obj_Comando.Parameters.AddWithValue("Bimestre", bimestre);
            Obj_Comando.Parameters.AddWithValue("Usuario_Creo", usuario_creo);
            Obj_Comando.CommandTimeout = 600;
            

            aux = Obj_Comando.ExecuteScalar();

            //Verificar si hay datos
            if (string.IsNullOrEmpty(aux.ToString().Trim()) == false)
            {
                Resultado = aux.ToString().Trim();
            }

            //Ejecutar la transaccion
            Obj_Transaccion.Commit();
            Obj_Conexion.Close();

            return Resultado;
        }

        public  DataTable Obtener_Bimestre(int no_cuenta) {

            DataTable Dt_Datos = new DataTable();
            SqlConnection Obj_Conexion = null;
            SqlCommand Obj_Comando = null;
            SqlDataReader Dr_Lector;

            //Asignar conexion
            Obj_Conexion = new SqlConnection(conexion);
            Obj_Conexion.Open();
            Obj_Comando = Obj_Conexion.CreateCommand();

            //Abrir la conexion
            Obj_Comando.CommandText = "SP_Cor_Consultar_Periodos_Predios";
            Obj_Comando.CommandType = CommandType.StoredProcedure;
            Obj_Comando.Parameters.Add("@No_Cuenta", SqlDbType.Int, 10).Value = no_cuenta;
            Dr_Lector = Obj_Comando.ExecuteReader();
            Dt_Datos.Load(Dr_Lector);


            //Cerrar conexion
            Dr_Lector.Close();

            //Entregar resultado           
            return Dt_Datos;

        }


        public void Guardar_Pago_Linea( Cls_Bean_Pago_Internet Obj_Bean_Pagos_Internet) {

            DataTable Dt_Consulta;

            strsql = "SELECT convert(VARCHAR(10), Fecha_Inicio_Periodo,103) as [fecha_inicio_periodo], " + 
                     "convert(VARCHAR(10), Fecha_Termino_Periodo,103) as [fecha_Termino_periodo], " +
                     "CONVERT(varchar(10), Fecha_Limite_Pago,103) as [fecha_limite_pago], " +
                     "CONVERT(VARCHAR(10), Periodo_Facturacion,103) as [periodo_facturacion],Anio as [anio], " +
                     "Bimestre as [bimestre], isnull(Comentarios,'') as [comentario],No_Cuenta as [no_cuenta] FROM Ope_Cor_Facturacion_Recibos WHERE No_Factura_Recibo='" + Obj_Bean_Pagos_Internet.no_factura_recibo + "'";

            Dt_Consulta = SqlHelper.ExecuteDataset(conexion, CommandType.Text, strsql).Tables[0];

            if (Dt_Consulta.Rows.Count > 0) {

                Obj_Bean_Pagos_Internet.no_cuenta = Convert.ToInt32( Dt_Consulta.Rows[0]["no_cuenta"].ToString());
                Obj_Bean_Pagos_Internet.fecha_inicio_periodo = Dt_Consulta.Rows[0]["fecha_inicio_periodo"].ToString();
                Obj_Bean_Pagos_Internet.fecha_termino_periodo = Dt_Consulta.Rows[0]["fecha_Termino_periodo"].ToString();
                Obj_Bean_Pagos_Internet.fecha_limite_pago = Dt_Consulta.Rows[0]["fecha_limite_pago"].ToString();
                Obj_Bean_Pagos_Internet.periodo_facturacion = Dt_Consulta.Rows[0]["periodo_facturacion"].ToString();
                Obj_Bean_Pagos_Internet.anio = Convert.ToInt16( Dt_Consulta.Rows[0]["anio"].ToString());
                Obj_Bean_Pagos_Internet.bimestre = Convert.ToInt16(Dt_Consulta.Rows[0]["bimestre"].ToString());
                Obj_Bean_Pagos_Internet.comentario = Dt_Consulta.Rows[0]["comentario"].ToString();

            }


            strsql = "insert into Ope_Cor_Pagos_En_Internet(no_factura_recibo,no_cuenta,fecha_pago_banco,numero_autorizacion, " +
                     "numero_tarjeta,referencia_bancaria,importe_total,anio,bimestre,comentario,fecha_inicio_periodo,fecha_termino_periodo, " +
                     "fecha_limite_pago,periodo_facturacion,fecha_creo,estatus,correo_electronico) VALUES('" + Obj_Bean_Pagos_Internet.no_factura_recibo + "'," + Obj_Bean_Pagos_Internet.no_cuenta + "," +
                     "'" + Obj_Bean_Pagos_Internet.fecha_pago_banco + "','" + Obj_Bean_Pagos_Internet.numero_autorizacion + "','" + Obj_Bean_Pagos_Internet.numero_tarjeta + "'," +
                     "'" + Obj_Bean_Pagos_Internet.referencia_bancaria + "'," + Obj_Bean_Pagos_Internet.importe_total + "," + Obj_Bean_Pagos_Internet.anio + ", " + Obj_Bean_Pagos_Internet.bimestre + "," +
                     "'" + Obj_Bean_Pagos_Internet.comentario + "','" + Obj_Bean_Pagos_Internet.fecha_inicio_periodo + "','" + Obj_Bean_Pagos_Internet.fecha_termino_periodo + "'," +
                     "'" + Obj_Bean_Pagos_Internet.fecha_limite_pago + "','" + Obj_Bean_Pagos_Internet.periodo_facturacion + "',getdate(),'PENDIENTE','" + Obj_Bean_Pagos_Internet.correo_electronico + "')";

            SqlHelper.ExecuteNonQuery(conexion, CommandType.Text, strsql);
        
        }


        public string Servidor_Autorizado(string servidor_origen) {
            string respuesta = "no";
            int id=0;
            try
            {

                strsql = "SELECT top 1 servidor_pago_id  from Ope_Cor_Servidor_Pagos WHERE nombre_servidor='" + servidor_origen + "' and estatus='ACTIVO'";
                id = Convert.ToInt16(SqlHelper.ExecuteScalar(conexion, CommandType.Text, strsql));
                if (id > 0) { 
                  respuesta="si";
                }


            }
            catch (Exception ex) { 
            
            }


            return respuesta;

        }
          

    }

}