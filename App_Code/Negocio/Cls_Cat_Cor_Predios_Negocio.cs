﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using SIAC.Cat_Predios.Datos;

/// <summary>
/// Descripción breve de Cls_Cat_Cor_Predios_Negocio
/// </summary>
/// 
namespace SIAC.Cat_Predios.Negocio
{
    public class Cls_Cat_Cor_Predios_Negocio
    {
        #region Variables
        private String Predio_Id = String.Empty;
        private String RPU = String.Empty;
        #endregion
        #region Variables_Publicas
        public String P_Predio_ID {
            get { return Predio_Id; }
            set { Predio_Id = value; }
        }
        public String P_RPU {
            get { return RPU; }
            set { RPU = value; }
        }

        #endregion

        #region Metodos
        public DataTable Consultar_Predios() {
            return Cls_Cat_Cor_Predios_Datos.Consultar_Predios(this);
        }
        
        public DataTable Consulta_Usuarios()
        {
            return Cls_Cat_Cor_Predios_Datos.Consultar_Usuarios(this);
        }
        #endregion

        public DataTable Consultar_Predios_Factura()
        {
            return Cls_Cat_Cor_Predios_Datos.Consultar_Predios_Factura(this);
        }
    }
}