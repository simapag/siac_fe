﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SIAC_Ope_Facturas_Detalles.Datos;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Cat_Contactos_Negocio
/// </summary>

namespace SIAC_Ope_Facturas_Detalles.Negocio
{
    public class Cls_Ope_Facturas_Detalles_Negocio
    {
        public Cls_Ope_Facturas_Detalles_Negocio() { }

        #region Variables

        private String No_Factura = String.Empty;
        private String Producto_Id = String.Empty;
        private String Serie = String.Empty;
        private String Cantidad = String.Empty;
        private String Codigo = String.Empty;
        private String Cuenta_Predial = String.Empty;
        private String Descripcion = String.Empty;
        private String Unidad = String.Empty;
        private String Porcentaje = String.Empty;
        private String Subtotal = String.Empty;
        private String Precio = String.Empty;
        private String Iva = String.Empty;
        private String Total = String.Empty;
        private String Codigo_Barras = String.Empty;
        private String No_Recibo = String.Empty;
        #endregion

        #region Variables Públicas

        public String P_No_Factura
        {
            get { return No_Factura; }
            set { No_Factura = value; }
        }

        public String P_Producto_Id
        {
            get { return Producto_Id; }
            set { Producto_Id = value; }
        }
        public String P_Serie
        {
            get { return Serie; }
            set { Serie = value; }
        }
        public String P_Cantidad {
            get { return Cantidad; }
            set { Cantidad = value; }
        }
        public String P_Cuenta_Predial
        {
            get { return Cuenta_Predial; }
            set { Cuenta_Predial = value; }
        }
        public String P_Codigo {
            get { return Codigo; }
            set { Codigo = value; }
        }
        public String P_No_Recibo
        {
            get { return No_Recibo; }
            set { No_Recibo = value; }
        }
        public String P_Descripcion {
            get { return Descripcion; }
            set { Descripcion = value; }
        }
        public String P_Unidad {
            get { return Unidad; }
            set { Unidad = value; }
        }
        public String P_Porcentaje {
            get { return Porcentaje; }
            set { Porcentaje = value; }
        }
        public String P_Subtotal {
            get { return Subtotal; }
            set { Subtotal = value; }
        }
        public String P_Precio {
            get { return Precio; }
            set { Precio = value; }
        }
        public String P_Iva
        {
            get { return Iva; }
            set { Iva = value; }
        }
        public String P_Total
        {
            get { return Total; }
            set { Total = value; }
        }

        #endregion

        #region Métodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Detalle_Factura
        ///DESCRIPCIÓN          : Manda llamar el método de Alta_Factura de la clase de datos
        ///PARAMETROS           : 
        ///CREO                 : Miguel Angel Alvarado Enriquez
        ///FECHA_CREO           : 26/Diciembre/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public Boolean Alta_Detalle_Factura(ref System.Data.SqlClient.SqlCommand Obj_Comando)
        {
            return Cls_Ope_Facturas_Detalles_Datos.Alta_Detalle_Factura(this, ref Obj_Comando);
        }
        public Boolean Alta_Detalle_Factura(SqlConnection objConexion)
        {
            return Cls_Ope_Facturas_Detalles_Datos.Alta_Detalle_Factura(this, objConexion);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Factura
        ///DESCRIPCIÓN          : Manda llamar el método de Consultar_Factura de la clase de datos
        ///PARAMETROS           : 
        ///CREO                 : Miguel Angel Alvarado Enriquez
        ///FECHA_CREO           : 26/Diciembre/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public DataTable Consultar_Factura()
        {
            return Cls_Ope_Facturas_Detalles_Datos.Consultar_Detalle_Factura(this);
        }

        #endregion
    }
}