﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using SIAC.Ope_Facturacion_Recibos.Datos;

/// <summary>
/// Descripción breve de Cls_Ope_Cor_Facturacion_Recibos
/// </summary>
/// 
namespace SIAC.Ope_Facturacion_Recibos.Negocio
{
    public class Cls_Ope_Cor_Facturacion_Recibos_Negocio
    {
        #region Variables
        private String No_Factura_Recibo = String.Empty;
        private String No_Cuenta = String.Empty;
        private String No_Recibo = String.Empty;
        private String Medidor_Detalle_Id = String.Empty;
        private String Region_Id = String.Empty;
        private String Sector_Id = String.Empty;
        private String Predio_Id = String.Empty;
        private String Usuario_Id = String.Empty;
        private String Medidor_Id = String.Empty;
        private String Tarifa_Id = String.Empty;
        private String No_Convenio = String.Empty;
        private String Codigo_Barras = String.Empty;
        private Decimal Lectura_Anterior = Decimal.MinValue;
        private Decimal Lectura_Actual = Decimal.MinValue;
        private Decimal Consumo = Decimal.MinValue;
        private Decimal Cuota_Base = Decimal.MinValue;
        private Decimal Cuota_Consumo = Decimal.MinValue;
        private Decimal Precio_Metro_Cubico = Decimal.MinValue;
        private DateTime Fecha_Inicio_Periodo = DateTime.MinValue;
        private DateTime Fecha_Termino_Periodo = DateTime.MinValue;
        private DateTime Fecha_Limite_Pago = DateTime.MinValue;
        private DateTime Fecha_Emision = DateTime.MinValue;
        private String Periodo_Facturacion = String.Empty;
        private Decimal Tasa_IVA = Decimal.MinValue;
        private Decimal Total_Importe = Decimal.MinValue;
        private Decimal Total_IVA = Decimal.MinValue;
        private Decimal Total_Pagar = Decimal.MinValue;
        private Decimal Total_Abono = Decimal.MinValue;
        private Decimal Saldo = Decimal.MinValue;
        private String Estatus_Recibo = String.Empty;
        private String Estatus_Impresion = String.Empty;
        private String Comentarios = String.Empty;
        private String Tipo_Recibo = String.Empty;
        private int Anio = int.MinValue;
        private int Bimestre = int.MinValue;
        private String Ruta_Reparto_ID = String.Empty;
        private String Referencia_Bancaria = String.Empty;
        private String RPU = String.Empty;
        #endregion

        #region Variables Publicas
        public String P_No_Factura_Recibo
        {
            get { return No_Factura_Recibo; }
            set { No_Factura_Recibo = value; }
        }
        public String P_No_Cuenta
        {
            get { return No_Cuenta; }
            set { No_Cuenta = value; }
        }
        public String P_No_Recibo
        {
            get { return No_Recibo; }
            set { No_Recibo = value; }
        }
        public String P_Medidor_Detalle_Id
        {
            get { return P_Medidor_Detalle_Id; }
            set { P_Medidor_Detalle_Id = value; }
        }
        public String P_Region_Id
        {
            get { return Region_Id; }
            set { Region_Id = value; }
        }
        public String P_Sector_Id
        {
            get { return Sector_Id; }
            set { Sector_Id = value; }
        }
        public String P_Predio_Id
        {
            get { return Predio_Id; }
            set { Predio_Id = value; }
        }
        public String P_Usuario_Id
        {
            get { return Usuario_Id; }
            set { Usuario_Id = value; }
        }
        public String P_Medidor_Id
        {
            get { return Medidor_Id; }
            set { Medidor_Id = value; }
        }
        public String P_Tarifa_Id
        {
            get { return Tarifa_Id; }
            set { Tarifa_Id = value; }
        }
        public String P_No_Convenio
        {
            get { return No_Convenio; }
            set { No_Convenio = value; }
        }
        public String P_Codigo_Barras
        {
            get { return Codigo_Barras; }
            set { Codigo_Barras = value; }
        }
        public Decimal P_Lectura_Anterior
        {
            get { return Lectura_Anterior; }
            set { Lectura_Anterior = value; }
        }

        public Decimal P_Lectura_Actual
        {
            get { return Lectura_Actual; }
            set { Lectura_Actual = value; }
        }
        public Decimal P_Consumo
        {
            get { return Consumo; }
            set { Consumo = value; }
        }
        public Decimal P_Cuota_Base
        {
            get { return Cuota_Base; }
            set { Cuota_Base = value; }
        }

        public Decimal P_Cuota_Consumo
        {
            get { return Cuota_Consumo; }
            set { Cuota_Consumo = value; }
        }
        public Decimal P_Precio_Metro_Cubico
        {
            get { return Precio_Metro_Cubico; }
            set { Precio_Metro_Cubico = value; }
        }
        public DateTime P_Fecha_Inicio_Periodo
        {
            get { return Fecha_Inicio_Periodo; }
            set { Fecha_Inicio_Periodo = value; }
        }
        public DateTime P_Fecha_Termino_Periodo
        {
            get { return Fecha_Termino_Periodo; }
            set { Fecha_Termino_Periodo = value; }
        }
        public DateTime P_Fecha_Limite_Pago
        {
            get { return Fecha_Limite_Pago; }
            set { Fecha_Limite_Pago = value; }
        }
        public DateTime P_Fecha_Emision
        {
            get { return Fecha_Emision; }
            set { Fecha_Emision = value; }
        }
        public String P_Periodo_Facturacion
        {
            get { return Periodo_Facturacion; }
            set { Periodo_Facturacion = value; }
        }
        public Decimal P_Tasa_IVA
        {
            get { return Tasa_IVA; }
            set { Tasa_IVA = value; }
        }
        public Decimal P_Total_Importe
        {
            get { return Total_Importe; }
            set { Total_Importe = value; }
        }
        public Decimal P_Total_IVA
        {
            get { return Total_IVA; }
            set { Total_IVA = value; }
        }
        public Decimal P_Total_Pagar
        {
            get { return Total_Pagar; }
            set { Total_Pagar = value; }
        }
        public Decimal P_Total_Abono
        {
            get { return Total_Abono; }
            set { Total_Abono = value; }
        }
        public Decimal P_Saldo
        {
            get { return Saldo; }
            set { Saldo = value; }
        }
        public String P_Estatus_Recibo
        {
            get { return Estatus_Recibo; }
            set { Estatus_Recibo = value; }
        }
        public String P_Estatus_Impresion
        {
            get { return Estatus_Impresion; }
            set { Estatus_Impresion = value; }
        }
        public String P_Comentarios
        {
            get { return Comentarios; }
            set { Comentarios = value; }
        }
        public String P_Tipo_Recibo
        {
            get { return Tipo_Recibo; }
            set { Tipo_Recibo = value; }
        }
        public int P_Anio
        {
            get { return Anio; }
            set { Anio = value; }
        }
        public int P_Bimestre
        {
            get { return Bimestre; }
            set { Bimestre = value; }
        }
        public String P_Ruta_Reparto_ID
        {
            get { return Ruta_Reparto_ID; }
            set { Ruta_Reparto_ID = value; }
        }
        public String P_Referencia_Bancaria
        {
            get { return Referencia_Bancaria; }
            set { Referencia_Bancaria = value; }
        }
        public String P_RPU
        {
            get { return RPU; }
            set { RPU = value; }
        }
        #endregion

        public DataTable Consultar_Factura()
        {
            return Cls_Ope_Cor_Facturacion_Recibos_Datos.Consultar_Facturacion_Recibos(this);
        }
    }
}