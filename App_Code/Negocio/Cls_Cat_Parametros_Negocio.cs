﻿using Cls_Cat_Parametros.Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de Cls_Cat_Parametros_Negocio
/// </summary>
/// 
namespace Cls_Cat_Parametros.Negocio
{
    public class Cls_Cat_Parametros_Negocio
    {
        public int NO_INTENTOS_ACCESO { get; set; }
        public int NO_BLOQUEOS_TEMPORALES { get; set; }
        public int MINUTOS_BLOQUEO_TEMPORAL { get; set; }

        public List<Cls_Cat_Parametros_Negocio> consultarParametros()
        {
            return Cls_Cat_Parametros_Datos.consultarParametros();
        }
    }
}