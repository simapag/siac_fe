﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using SIAC.Registro_Usuarios.Datos;
/// <summary>
/// Descripción breve de Cls_Cat_Cliente_Temporal_Negocio
/// </summary>
/// 
namespace SIAC.Registro_Usuarios.Negocio
{
    public class Cls_Cat_Registro_Usuarios_Negocio
    {
        #region Variables
        private String Registro_Id = String.Empty;
        private String Email = String.Empty;
        private String No_Cuenta = String.Empty;
        private String RPU = String.Empty;
        private DateTime Fecha_Emision = DateTime.MinValue;
        private String Total_Pagar = String.Empty;
        private String Estatus = String.Empty;
        private String Predio_ID = String.Empty;

        #endregion

        #region Propiedades

        public String P_Registro_Id {
            get { return Registro_Id; }
            set { Registro_Id = value; }
        }
        public String P_Email
        {
            get { return Email; }
            set { Email = value; }
        }
        public String P_No_Cuenta
        {
            get { return No_Cuenta; }
            set { No_Cuenta = value; }
        }

        public String P_Rpu
        {
            get { return RPU; }
            set { RPU = value; }
        }
        public DateTime P_Fecha_Emision
        {
            get { return Fecha_Emision; }
            set { Fecha_Emision = value; }
        }
        public String P_Total_Pagar
        {
            get { return Total_Pagar; }
            set { Total_Pagar = value; }
        }
        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }

        public String P_Predio_Id {
            get { return Predio_ID; }
            set { Predio_ID = value; }
            }
        #endregion

        public bool Alta_Registro_Usuarios()
        {
            return Cls_Cat_Registro_Usuarios_Datos.Alta_Registro_Usuario(this);
        }

        public bool Modificar_Registro_Usuarios()
        {
            return Cls_Cat_Registro_Usuarios_Datos.Modificar_Registro_Usuarios(this);
        }

        public DataTable Consultar_Registro_Usuario()
        {
            return Cls_Cat_Registro_Usuarios_Datos.Consultar_Registro_Usuarios(this);
        }

        public void Eliminar_Registro()
        {
            Cls_Cat_Registro_Usuarios_Datos.Eliminar_Registro(this);
        }
    }
}