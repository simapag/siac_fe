﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIAC.Catalogo_Comercializacion_Usuarios.Datos;
 

/// <summary>
/// Summary description for Cls_Cat_Cor_Usuarios_Negocio
/// </summary>
namespace SIAC.Catalogo_Comercializacion_Usuarios.Negocio
{
    public class Cls_Cat_Cor_Usuarios_Negocio
    {
        public Cls_Cat_Cor_Usuarios_Negocio() { }

        #region (Variables Publicas)
            public String P_Usuario_Id { get; set; }
            public String P_Estado_ID { get; set; }
            public String P_Ciudad_ID { get; set; }
            public String P_Nombre { get; set; }
            public String P_Apellido_Paterno { get; set; }
            public String P_Apellido_Materno { get; set; }
            public String P_Rfc { get; set; }
            public String P_Razon_Social { get; set; }
            public String P_Calle { get; set; }
            public String P_Colonia { get; set; }
            public String P_Calle_Id { get; set; }
            public String P_Colonia_Id { get; set; }
            public String P_Pais { get; set; }
            public String P_No_Interior { get; set; }
            public String P_No_Exterior { get; set; }
            public String P_Telefono_Casa { get; set; }
            public String P_Telefono_Oficina { get; set; }
            public String P_Telefono_Celular { get; set; }
            public String P_Telefono_Nextel { get; set; }
            public String P_Correo_Electronico { get; set; }
            public String P_Busqueda { get; set; }
            public String P_Campo { get; set; }
            public String P_Estatus { get; set; }
            public String P_Usuario_Creo { get; set; }
            public String P_Fecha_Creo { get; set; }
            public String P_Usuario_Modifico { get; set; }
            public String P_Fecha_Modifico { get; set; }
            public String P_Folio_IFE { get; set; }
            public String P_Nombre_Completo { get; set; }

        //Paginacion
            public int P_Pagina { get; set; }
            public int P_Cantidad  { get; set; }
        #endregion

        #region (Metodos)

           public String Alta_Usuarios(){return Cls_Cat_Cor_Usuarios_Datos.Alta_Usuarios(this);}
            public void Baja_Usuarios(){Cls_Cat_Cor_Usuarios_Datos.Baja_Usuarios(this);}
            public void Modifica_Usuarios(){Cls_Cat_Cor_Usuarios_Datos.Modifica_Usuarios(this);}
            public DataTable Consulta_Usuarios(){return Cls_Cat_Cor_Usuarios_Datos.Consulta_Usuarios(this);}
            public string Consulta_Usuarios_Cantidad() { return Cls_Cat_Cor_Usuarios_Datos.Consulta_Usuarios_Cantidad(this); }
            public DataTable Consulta_UsuariosxCampo() { return Cls_Cat_Cor_Usuarios_Datos.Consulta_UsuariosxCampo(this); }
            public DataTable obtenerCalles(string nombre_calle, int limite) { return Cls_Cat_Cor_Usuarios_Datos.obtenerCalles(nombre_calle, limite); }
            public DataTable obtenerColonias(string nombre_colonia, int limte) { return Cls_Cat_Cor_Usuarios_Datos.obtenerColonias(nombre_colonia, limte); }
           //public DataTable Alta_Usuarios_Web()
            //{
            //    return Cls_Cat_Cor_Usuarios_Datos.Alta_Usuarios_Web(this);
            //}
            //public DataTable Cambio_Password_Web()
            //{
            //    return Cls_Cat_Cor_Usuarios_Datos.Cambio_Password_Web(this);
            //}
        #endregion
    }
}
