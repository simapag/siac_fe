﻿using System;
using SIAC.Cat_Clientes.Datos;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;

namespace SIAC.Cat_Clientes.Negocio
{
    public class Cls_Cat_Clientes_Negocio
    {
        #region Variables Internas

        private String Cliente_Id = String.Empty;
        private String Nombre_Corto = String.Empty;
        private String Razon_Social = String.Empty;
        private String Rfc = String.Empty;
        private String Curp = String.Empty;
        private String Pais = String.Empty;
        private String Estado = String.Empty;
        private String Localidad = String.Empty;
        private String Colonia = String.Empty;
        private String Ciudad = String.Empty;
        private String Calle = String.Empty;
        private String Numero_Exterior = String.Empty;
        private String Numero_Interior = String.Empty;
        private String Cp = String.Empty;
        private String Email = String.Empty;
        private String Estatus = String.Empty;
        private String Contrasena = String.Empty;
        private String Predio_Id = String.Empty;

        #endregion

        #region Variables Publicas

        public String P_Cliente_Id
        {
            get { return Cliente_Id; }
            set { Cliente_Id = value; }
        }
        public String P_Nombre_Corto
        {
            get { return Nombre_Corto; }
            set { Nombre_Corto = value; }
        }
        public String P_Razon_Social
        {
            get { return Razon_Social; }
            set { Razon_Social = value; }
        }
        public String P_Rfc
        {
            get { return Rfc; }
            set { Rfc = value; }
        }
        public String P_Curp
        {
            get { return Curp; }
            set { Curp = value; }
        }
        public String P_Pais
        {
            get { return Pais; }
            set { Pais = value; }
        }
        public String P_Estado
        {
            get { return Estado; }
            set { Estado = value; }
        }
        public String P_Localidad
        {
            get { return Localidad; }
            set { Localidad = value; }
        }
        public String P_Colonia
        {
            get { return Colonia; }
            set { Colonia = value; }
        }
        public String P_Ciudad
        {
            get { return Ciudad; }
            set { Ciudad = value; }
        }
        public String P_Calle
        {
            get { return Calle; }
            set { Calle = value; }
        }
        public String P_Numero_Exterior
        {
            get { return Numero_Exterior; }
            set { Numero_Exterior = value; }
        }
        public String P_Numero_Interior
        {
            get { return Numero_Interior; }
            set { Numero_Interior = value; }
        }
        public String P_Cp
        {
            get { return Cp; }
            set { Cp = value; }
        }
        public String P_Email
        {
            get { return Email; }
            set { Email = value; }
        }
        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
        public String P_Contrasena {
            get { return Contrasena; }
            set { Contrasena = value; }
        }
        public String P_Predio_Id {
            get { return Predio_Id; }
            set { Predio_Id = value; }
        }



        #endregion

        #region Metodos

        public Boolean Alta_Cliente()
        {
            return Cls_Cat_Clientes_Datos.Alta_Clientes(this);
        }

        public DataTable Consultar_Clientes()
        {
            return Cls_Cat_Clientes_Datos.Consultar_Clientes(this);
        }

        #endregion

        public Boolean Modificar_Cliente()
        {
            return Cls_Cat_Clientes_Datos.Modificar_Cliente(this);
        }
        public Boolean Modificar_Cliente_Por_Predio()
        {
            return Cls_Cat_Clientes_Datos.Modificar_Cliente_Por_Predio(this);
        }

        public DataTable Consultar_Predios()
        {
            return Cls_Cat_Clientes_Datos.Consultar_Predios(this);
        }

        public DataTable Verificar_Email()
        {
            return Cls_Cat_Clientes_Datos.Verificar_Email(this);
        }

        public DataTable VerificarEmail()
        {
            return Cls_Cat_Clientes_Datos.VerificarEmail(this);
        }

        public List<Cls_Bloqueo> ConsultarDatosBloqueos(string cliente_id) {
            return Cls_Cat_Clientes_Datos.ConsultarDatosBloqueos(cliente_id);
        }

        public void Modificar_Datos_Bloqueos(int numero_intento, int no_bloqueos, string cliente, string estatus)
        {
            Cls_Cat_Clientes_Datos.Modificar_Datos_Bloqueos(numero_intento, no_bloqueos, cliente, estatus);
        }
    }
   public  class Cls_Bloqueo {
        public string cliente { get; set; }
        public string estatus { get; set; }
        public int No_Intentos_Temporal { get; set; }
        public int No_Bloqueo_Temporal { get; set; }
        public string Fecha_Bloqueo_Temporal { get; set; }
    }
}
