﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SIAC.Ope_Facturas_Series.Datos;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Ope_Facturas_Series_Negocio
/// </summary>

namespace SIAC.Ope_Facturas_Series.Negocio
{
    public class Cls_Ope_Facturas_Series_Negocio
    {
        public Cls_Ope_Facturas_Series_Negocio() { }

        #region Variables

        private String Serie_Id;
        private String Serie;
        private String Descripcion;
        private String Tipo_Serie;
        private String Total_Timbres;
        private String Timbre_Inicial;
        private String Timbre_Final;
        private String Cancelaciones;
        private String Estatus;
        private String Tipo_Default;

        #endregion

        #region Variables Públicas

        public String P_Serie_Id {
            get { return Serie_Id; }
            set { Serie_Id = value; }
        }
        public String P_Serie {
            get { return Serie; }
            set { Serie = value; }
        }
        public String P_Descripcion {
            get { return Descripcion; }
            set { Descripcion = value; }
        }
        public String P_Tipo_Serie
        {
            get { return Tipo_Serie; }
            set { Tipo_Serie = value; }
        }
        public String P_Total_Timbres
        {
            get { return Total_Timbres; }
            set { Total_Timbres = value; }
        }
        public String P_Timbre_Inicial
        {
            get { return Timbre_Inicial; }
            set { Timbre_Inicial = value; }
        }
        public String P_Timbre_Final
        {
            get { return Timbre_Final; }
            set { Timbre_Final = value; }
        }
        public String P_Cancelaciones
        {
            get { return Cancelaciones; }
            set { Cancelaciones = value; }
        }
        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
        public String P_Tipo_Default
        {
            get { return Tipo_Default; }
            set { Tipo_Default = value; }
        }

        #endregion

        #region Métodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Serie
        ///DESCRIPCIÓN          : Manda llamar el método de Alta_Serie de la clase de datos
        ///PARAMETROS           : 
        ///CREO                 : Miguel Angel Alvarado Enriquez
        ///FECHA_CREO           : 26/Diciembre/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public Boolean Alta_Serie()
        {
            return Cls_Ope_Facturas_Series_Datos.Alta_Serie(this);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Serie
        ///DESCRIPCIÓN          : Manda llamar el método de Modificar_Serie de la clase de datos
        ///PARAMETROS           : 
        ///CREO                 : Miguel Angel Alvarado Enriquez
        ///FECHA_CREO           : 26/Diciembre/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public Boolean Modificar_Serie()
        {
            return Cls_Ope_Facturas_Series_Datos.Modificar_Serie(this);
        }
        public Boolean Modificar_Serie(SqlConnection objConexion)
        {
            return Cls_Ope_Facturas_Series_Datos.Modificar_Serie(this, objConexion);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Eliminar_Serie
        ///DESCRIPCIÓN          : Manda llamar el método de Eliminar_Serie de la clase de datos
        ///PARAMETROS           : 
        ///CREO                 : Miguel Angel Alvarado Enriquez
        ///FECHA_CREO           : 26/Diciembre/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public Boolean Eliminar_Serie()
        {
            return Cls_Ope_Facturas_Series_Datos.Eliminar_Serie(this);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Serie
        ///DESCRIPCIÓN          : Manda llamar el método de Consultar_Serie de la clase de datos
        ///PARAMETROS           : 
        ///CREO                 : Miguel Angel Alvarado Enriquez
        ///FECHA_CREO           : 26/Diciembre/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public DataTable Consultar_Serie()
        {
            return Cls_Ope_Facturas_Series_Datos.Consultar_Serie(this);
        }
        public DataTable Consultar_Serie(SqlConnection objConexion)
        {
            return Cls_Ope_Facturas_Series_Datos.Consultar_Serie(this, objConexion);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Serie
        ///DESCRIPCIÓN          : Manda llamar el método de Consultar_Serie de la clase de datos
        ///PARAMETROS           : 
        ///CREO                 : Miguel Angel Alvarado Enriquez
        ///FECHA_CREO           : 26/Diciembre/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public DataTable Consultar_Serie_Activa_Pendiente()
        {
            return Cls_Ope_Facturas_Series_Datos.Consultar_Serie_Activa_Pendiente(this);
        }
        public DataTable Consultar_Serie_Activa_Pendiente(SqlConnection sqlConnection)
        {
            return Cls_Ope_Facturas_Series_Datos.Consultar_Serie_Activa_Pendiente(this, sqlConnection);
        }

        #endregion
    }
}