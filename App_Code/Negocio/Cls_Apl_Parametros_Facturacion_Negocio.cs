﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIAC_Apl_Parametros_Facturacion.Datos;
using System.Data;

/// <summary>
/// Summary description for Cls_Apl_Parametros_Facturacion_Negocios
/// </summary>
/// 
namespace SIAC_Apl_Parametros_Facturacion.Negocio
{
    public class Cls_Apl_Parametros_Facturacion_Negocio
    {

    #region Variables

    private String Parametro_Factura_Id = String.Empty;
    private String Interes_Pagare = String.Empty;
    private String IVA = String.Empty;
    private String Retencion_Fletes = String.Empty;
    private String Retencion_Iva = String.Empty;
    private String Retencion_Cedular = String.Empty;
    private String Retencion_ISR = String.Empty;
    private String Email_Origen_Correos = String.Empty;
    private String Mensaje_Factura = String.Empty;
    private String Regimen_Fiscal = String.Empty;
    private String Version = String.Empty;
    private String Sucursal_Id = String.Empty;
    private String Codigo_Usuario = String.Empty;
    private String Codigo_Usuario_Proveedor = String.Empty;
    private String Ambiente_Timbrado = String.Empty;

    private String Expira_Vigencia = String.Empty;
    private String Expira_Folio = String.Empty;
    private String Vigencia_Inicial = String.Empty;
    private String Vigencia_Final = String.Empty;
    private String Password_Llave = String.Empty;
    private String Envio_Automatico_Correo = String.Empty;


    private String Ruta_Certificado = String.Empty;
    private String Ruta_Llave = String.Empty;
    private String Ruta_Pdf = String.Empty;
    private String Ruta_Xmls = String.Empty;
    private String Imagen_Factura = String.Empty;
    //private String Ip_Creo = String.Empty;
    //private String Equipo_Creo = String.Empty;
    //private String Fecha_Creo = String.Empty;
    //private String Usuario_Modifico = String.Empty;
    //private String Ip_Modifico = String.Empty;
    //private String Equipo_Modifico = String.Empty;
    //private String Fecha_Modifico = String.Empty;
    #endregion

    #region Variables Publicas

    public String P_Parametro_Factura_Id
    {
        get { return Parametro_Factura_Id; }
        set { Parametro_Factura_Id = value; }
    }
    public String P_Interes_Pagare
    {
        get { return Interes_Pagare; }
        set { Interes_Pagare = value; }
    }
    public String P_IVA
    {
        get { return IVA; }
        set { IVA = value; }
    }
    public String P_Retencion_Fletes
    {
        get { return Retencion_Fletes; }
        set { Retencion_Fletes = value; }
    }
    public String P_Retencion_Iva
    {
        get { return Retencion_Iva; }
        set { Retencion_Iva = value; }
    }
    public String P_Retencion_Cedular
    {
        get { return Retencion_Cedular; }
        set { Retencion_Cedular = value; }
    }
    public String P_Retencion_ISR
    {
        get { return Retencion_ISR; }
        set { Retencion_ISR = value; }
    }
    public String P_Email_Origen_Correos
    {
        get { return Email_Origen_Correos; }
        set { Email_Origen_Correos = value; }
    }
    public String P_Mensaje_Factura
    {
        get { return Mensaje_Factura; }
        set { Mensaje_Factura = value; }
    }
    public String P_Regimen_Fiscal
    {
        get { return Regimen_Fiscal; }
        set { Regimen_Fiscal = value; }
    }
    public String P_Version
    {
        get { return Version; }
        set { Version = value; }
    }
    public String P_Sucursal_Id
    {
        get { return Sucursal_Id; }
        set { Sucursal_Id = value; }
    }
    public String P_Codigo_Usuario
    {
        get { return Codigo_Usuario; }
        set { Codigo_Usuario = value; }
    }
    public String P_Codigo_Usuario_Proveedor
    {
        get { return Codigo_Usuario_Proveedor; }
        set { Codigo_Usuario_Proveedor = value; }
    }
    public String P_Ambiente_Timbrado
    {
        get { return Ambiente_Timbrado; }
        set { Ambiente_Timbrado = value; }
    }


    public String P_Ruta_Certificado
    {
        get { return Ruta_Certificado; }
        set { Ruta_Certificado = value; }
    }
    public String P_Ruta_Llave
    {
        get { return Ruta_Llave; }
        set { Ruta_Llave = value; }
    }
    public String P_Ruta_Pdf
    {
        get { return Ruta_Pdf; }
        set { Ruta_Pdf = value; }
    }
    public String P_Ruta_Xmls
    {
        get { return Ruta_Xmls; }
        set { Ruta_Xmls = value; }
    }
    public String P_Imagen_Factura
    {
        get { return Imagen_Factura; }
        set { Imagen_Factura = value; }
    }
    
    //
    public String P_Expira_Vigencia
    {
        get { return Expira_Vigencia; }
        set { Expira_Vigencia = value; }
    }
    public String P_Expira_Folio
    {
        get { return Expira_Folio; }
        set { Expira_Folio = value; }
    }
    public String P_Vigencia_Inicial
    {
        get { return Vigencia_Inicial; }
        set { Vigencia_Inicial = value; }
    }
    public String P_Vigencia_Final
    {
        get { return Vigencia_Final; }
        set { Vigencia_Final = value; }
    }
    public String P_Password_Llave
    {
        get { return Password_Llave; }
        set { Password_Llave = value; }
    }


    public String P_Envio_Automatico_Correo
    {
        get { return Envio_Automatico_Correo; }
        set { Envio_Automatico_Correo = value; }
    }
    //public String P_Equipo_Creo
    //{
    //    get { return Equipo_Creo; }
    //    set { Equipo_Creo = value; }
    //}
    //public String P_Fecha_Creo
    //{
    //    get { return Fecha_Creo; }
    //    set { Fecha_Creo = value; }
    //}
    //public String P_Usuario_Modifico
    //{
    //    get { return Usuario_Modifico; }
    //    set { Usuario_Modifico = value; }
    //}
    //public String P_Ip_Modifico
    //{
    //    get { return Ip_Modifico; }
    //    set { Ip_Modifico = value; }
    //}
    //public String P_Equipo_Modifico
    //{
    //    get { return Equipo_Modifico; }
    //    set { Equipo_Modifico = value; }
    //}
    //public String P_Fecha_Modifico
    //{
    //    get { return Fecha_Modifico; }
    //    set { Fecha_Modifico = value; }
    //}
    //public String P_Mensaje_Sistema
    //{
    //    get { return Mensaje_Sistema; }
    //    set { Mensaje_Sistema = value; }
    //}

    #endregion

    #region Metodos

    /////*******************************************************************************
    /////NOMBRE DE LA FUNCIÓN : Alta_Parametros
    /////DESCRIPCIÓN          : Ingresa al metodo que realizara el alta de registros.
    /////PARAMETROS           : 
    /////CREO                 : Miguel Angel Alvarado Enriquez
    /////FECHA_CREO           : 15/Febrero/2013 01:40 p.m.
    /////MODIFICO             :
    /////FECHA_MODIFICO       :
    /////CAUSA_MODIFICACIÓN   :
    /////*******************************************************************************
    public void Alta_Parametros_Facturacion()
    {
        Cls_Apl_Parametros_Facturacion_Datos.Alta_Parametros_Facturacion(this);
    }
    /////*******************************************************************************
    /////NOMBRE DE LA FUNCIÓN : Modificar_Parametro
    /////DESCRIPCIÓN          : Ingresa al metodo que realizara la modificación de registros.
    /////PARAMETROS           : 
    /////CREO                 : Miguel Angel Alvarado Enriquez
    /////FECHA_CREO           : 11/Marzo/2013 09:45 a.m.
    /////MODIFICO             :
    /////FECHA_MODIFICO       :
    /////CAUSA_MODIFICACIÓN   :
    /////*******************************************************************************
    public void Modificar_Parametro()
    {
        Cls_Apl_Parametros_Facturacion_Datos.Modificar_Parametro_Facturacion(this);
    }
    /////*******************************************************************************
    /////NOMBRE DE LA FUNCIÓN : Consultar_Parametros
    /////DESCRIPCIÓN          : Regresa un DataTable con los Parametros encontrados.
    /////PARAMETROS           : 
    /////CREO                 : Miguel Angel Alvarado Enriquez
    /////FECHA_CREO           : 11/Marzo/2013 09:45 a.m.
    /////MODIFICO             :
    /////FECHA_MODIFICO       :
    /////CAUSA_MODIFICACIÓN   :
    /////*******************************************************************************
    public DataTable Consultar_Parametros()
    {
        return Cls_Apl_Parametros_Facturacion_Datos.Consultar_Parametros_Facturacion(this);
    }

    public DataTable ObtenerUnidadConceptoSat(string concepto)
    {
        return Cls_Apl_Parametros_Facturacion_Datos.ObtenerUnidadConceptoSat(concepto);
    }

        #endregion

    }
}
