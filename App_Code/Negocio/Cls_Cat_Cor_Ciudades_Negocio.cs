﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIAC.Ciudades.Datos;

/// <summary>
/// Summary description for Cls_Cat_Cor_Regiones_Negocio
/// </summary>
/// 
namespace SIAC.Ciudades.Negocios
{
    public class Cls_Cat_Cor_Ciudades_Negocio
    {
        public Cls_Cat_Cor_Ciudades_Negocio() { }

        #region Variables Locales
            private String Var_Loc_Ciudad_ID;
            private String Var_Loc_Estado_ID;
            private String Var_Loc_Clave;
            private String Var_Loc_Estatus;
            private String Var_Loc_Nombre;
            private String Var_Loc_Usuario_Creo;
            private String Var_Loc_Usuario_Modifico;
        #endregion

        #region Variable Públicas
            public String P_Ciudad_ID
            {
                get { return Var_Loc_Ciudad_ID; }
                set { Var_Loc_Ciudad_ID = value; }
            }
            public String P_Estado_ID { get; set; }
            public String P_Clave { get; set; }
            public String P_Estatus { get; set; }
            public String P_Nombre { get; set; }
            public String P_Usuario_Creo { get; set; }
            public String P_Usuario_Modifico { get; set; }
        #endregion

        #region Métodos

            ///*******************************************************************************
            ///NOMBRE_FUNCION : Insertar_Ciudad
            ///DESCRIPCION    : Método que manda llamar el metodo Insertar_Ciudad para agregar un registro en la BD
            ///PARAMETROS     :
            ///CREO           : Sergio Ulises Durán Hernández
            ///FECHA_CREO     : 18-Agosto-2011
            ///MODIFICO       :
            ///FECHA_MODIFICO :
            ///CAUSA_MODIFICO :
            ///*******************************************************************************
            ///
            public void Insertar_Ciudad(){
                Cls_Cat_Cor_Ciudades_Datos.Insertar_Ciudad(this);
            }

            ///*******************************************************************************
            ///NOMBRE_FUNCION : Modificar_Ciudad
            ///DESCRIPCION    : Método que manda llamar el metodo Modificar_Ciudad para actualizar un registro en la BD
            ///PARAMETROS     :
            ///CREO           : Sergio Ulises Durán Hernández
            ///FECHA_CREO     : 18-Agosto-2011
            ///MODIFICO       :
            ///FECHA_MODIFICO :
            ///CAUSA_MODIFICO :
            ///*******************************************************************************
            ///
            public void Modificar_Ciudad(){
                Cls_Cat_Cor_Ciudades_Datos.Modificar_Ciudad(this);
            }

            ///*******************************************************************************
            ///NOMBRE_FUNCION : Eliminar_Ciudad
            ///DESCRIPCION    : Método que manda llamar el metodo Eliminar_Ciudad para eliminar un registro en la BD
            ///PARAMETROS     :
            ///CREO           : Sergio Ulises Durán Hernández
            ///FECHA_CREO     : 18-Agosto-2011
            ///MODIFICO       :
            ///FECHA_MODIFICO :
            ///CAUSA_MODIFICO :
            ///*******************************************************************************
            ///
            public void Eliminar_Ciudad(){
                Cls_Cat_Cor_Ciudades_Datos.Eliminar_Ciudad(this);
            }

            ///*******************************************************************************
            ///NOMBRE_FUNCION : Buscar_Ciudad
            ///DESCRIPCION    : Método que manda llamar el metodo Buscar_Ciudad para buscar un registro en la BD
            ///PARAMETROS     :
            ///CREO           : Sergio Ulises Durán Hernández
            ///FECHA_CREO     : 18-Agosto-2011
            ///MODIFICO       :
            ///FECHA_MODIFICO :
            ///CAUSA_MODIFICO :
            ///*******************************************************************************
            ///
            public DataTable Buscar_Ciudad(){
                return Cls_Cat_Cor_Ciudades_Datos.Buscar_Ciudad(this);
            }

            public DataTable obtenerCiudad(string estado_id) {
                return Cls_Cat_Cor_Ciudades_Datos.obtenerCiudad(estado_id);
            }

            ///*******************************************************************************
            ///NOMBRE_FUNCION : Llenar_Grid_Ciudad
            ///DESCRIPCION    : Método que manda llamar el metodo Llenar_Grid_Ciudad para llenar el grid del formulario 
            ///PARAMETROS     :
            ///CREO           : Sergio Ulises Durán Hernández
            ///FECHA_CREO     : 18-Agosto-2011
            ///MODIFICO       :
            ///FECHA_MODIFICO :
            ///CAUSA_MODIFICO :
            ///*******************************************************************************
            ///
            public DataTable Llenar_Grid_Ciudad(){
                return Cls_Cat_Cor_Ciudades_Datos.Llenar_Grid_Ciudad(this);
            }

            ///*******************************************************************************
            /// NOMBRE_FUNCION : Consulta_Estados
            /// DESCRIPCION    : Consulta los estados
            /// PARAMETROS     :
            /// CREO           : Sergio Ulises Durán Hernández
            /// FECHA_CREO     : 18-Agosto-2011
            /// MODIFICO       :
            /// FECHA_MODIFICO :
            /// CAUSA_MODIFICO :
            ///*******************************************************************************/
            public DataTable Consulta_Estados(){
                return Cls_Cat_Cor_Ciudades_Datos.Consulta_Estados(this);
            }

            ///*******************************************************************************
            ///NOMBRE_FUNCION : Valida_Ciudades
            ///DESCRIPCION    : Método que manda llamar el metodo Valida_Ciudades para saber si ya existe la ciudad 
            ///PARAMETROS     :
            ///CREO           : David Herrera Rincon
            ///FECHA_CREO     : 29-Junio-2012
            ///MODIFICO       :
            ///FECHA_MODIFICO :
            ///CAUSA_MODIFICO :
            ///*******************************************************************************
            ///
            public DataTable Valida_Ciudades(){
                return Cls_Cat_Cor_Ciudades_Datos.Valida_Ciudades(this);
            }
        #endregion
    }
}