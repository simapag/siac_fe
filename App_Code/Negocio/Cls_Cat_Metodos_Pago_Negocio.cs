﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SIAC.Cat_Metodos_Pago.Datos;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Ope_Facturas_Series_Negocio
/// </summary>

namespace SIAC.Cat_Metodos_Pago.Negocio
{
    public class Cls_Cat_Metodos_Pago_Negocio
    {
        public Cls_Cat_Metodos_Pago_Negocio() { }

        #region Variables

        private String Metodo_Pago_Id;
        private String Clave;
        private String Nombre;
        private String Estatus;
        private String Tipo_Default;

        #endregion

        #region Variables Públicas

        public String P_Metodo_Pago_Id {
            get { return Metodo_Pago_Id; }
            set { Metodo_Pago_Id = value; }
        }
        public String P_Clave {
            get { return Clave; }
            set { Clave = value; }
        }
        public String P_Nombre {
            get { return Nombre; }
            set { Nombre = value; }
        }
        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
        public String P_Tipo_Default
        {
            get { return Tipo_Default; }
            set { Tipo_Default = value; }
        }

        public String P_No_Recibo { get; set; }

        #endregion

        #region Métodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Metodo_Pago
        ///DESCRIPCIÓN          : Manda llamar el método de Alta_Metodo_Pago de la clase de datos
        ///PARAMETROS           : 
        ///CREO                 : Ana Laura Huichapa Ramírez
        ///FECHA_CREO           : 19/Septiembre/2016
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        //public Boolean Alta_Metodo_Pago()
        //{
        //    return Cls_Cat_Metodos_Pago_Datos.Alta_Metodo_Pago(this);
        //}

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Metodos_Pago
        ///DESCRIPCIÓN          : Manda llamar el método de Modificar_Metodos_Pago de la clase de datos
        ///PARAMETROS           : 
        ///CREO                 : Ana Laura Huichapa Ramírez
        ///FECHA_CREO           : 19/Septiembre/2016
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        //public Boolean Modificar_Metodos_Pago()
        //{
        //    return Cls_Cat_Metodos_Pago_Datos.Modificar_Metodos_Pago(this);
        //}

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Eliminar_Metodo_Pago
        ///DESCRIPCIÓN          : Manda llamar el método de Eliminar_Metodo_Pago de la clase de datos
        ///PARAMETROS           : 
        ///CREO                 : Ana Laura Huichapa Ramírez
        ///FECHA_CREO           : 19/Septiembre/2016
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        //public Boolean Eliminar_Metodo_Pago()
        //{

        //    return Cls_Cat_Metodos_Pago_Datos.Eliminar_Metodo_Pago(this);
        //}

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Metodos_Pago
        ///DESCRIPCIÓN          : Manda llamar el método de Consultar_Metodos_Pago de la clase de datos
        ///PARAMETROS           : 
        ///CREO                 : Ana Laura Huichapa Ramírez
        ///FECHA_CREO           : 19/Setiembre/2016
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public DataTable Consultar_Metodos_Pago()
        {
            return Cls_Cat_Metodos_Pago_Datos.Consultar_Metodos_Pago(this);
        }
        public DataTable Consultar_Metodos_Pago(SqlConnection objConexion)
        {
            return Cls_Cat_Metodos_Pago_Datos.Consultar_Metodos_Pago(this, objConexion);
        }

        public DataTable Consultar_Metodos_Pago_Tarjetas()
        {
            return Cls_Cat_Metodos_Pago_Datos.Consultar_Metodos_Pago_Tarjetas(this);
        }
        public DataTable Consultar_Metodos_Pago_Tarjetas(SqlConnection objConexion)
        {
            return Cls_Cat_Metodos_Pago_Datos.Consultar_Metodos_Pago_Tarjetas(this, objConexion);
        }

        #endregion
    }
}