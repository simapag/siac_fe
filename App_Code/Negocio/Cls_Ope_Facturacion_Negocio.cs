﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SIAC.Facturacion.Datos;
using SIISR3_Impresion_Facturacion.Clases.Beans;
using Newtonsoft.Json;
using System.Data.SqlClient;

namespace SIAC.Facturacion.Negocio
{
    public class Cls_Ope_Facturacion_Negocio
    {

        #region Variables Usuario
        public String No_Folio { get; set; }
	    public String Empresa_ID { get; set; }
        public String Cliente_ID { get; set; }
        public String Estado_ID { get; set; }
        public String No_Cuenta { get; set; }
        public String Codigo_Barras { get; set; }
        public String Nombre { get; set; }
        public String Rfc { get; set; }
        public String Curp { get; set; }
        public String Pais { get; set; }
        public String Estado { get; set; }
        public String Ciudad { get; set; }
        public String Localidad { get; set; }
        public String Colonia { get; set; }
        public String Calle { get; set; }
        public String Numero_Exterior { get; set; }
        public String Numero_Interior { get; set; }
        public String Codigo_Postal { get; set; }
        public String Fax { get; set; }
        public String Telefono_1 { get; set; }
        public String Telefono_2 { get; set; }
        public String Extension { get; set; }
        public String Nextel { get; set; }
        public String Email { get; set; }
        public String Sitio_Web { get; set; }
        public DataTable Facturacion_Detalles { get; set; }

        #endregion

        #region Facturacion Electronica

        #region Variables Facturacion

        private String No_Factura = String.Empty;
        private String No_Cotizacion = String.Empty;
        private String Serie = String.Empty;
        private String Cliente_Id = String.Empty;
        private String Email_Facturacion = String.Empty;
        private String Razon_Social_Facturacion = String.Empty;
        private String Rfc_Facturacion = String.Empty;
        private String Pais_Facturacion = String.Empty;
        private String Estado_Facturacion = String.Empty;
        private String Localidad_Facturacion = String.Empty;
        private String Colonia_Facturacion = String.Empty;
        private String Ciudad_Facturacion = String.Empty;
        private String Calle_Facturacion = String.Empty;
        private String Numero_Exterior_Facturacion = String.Empty;
        private String Numero_Interior_Facturacion = String.Empty;
        private String Cp = String.Empty;
        private String Dias_Credito = String.Empty;
        private String Porcentaje_Descuento = String.Empty;
        private String Orden_Compra = String.Empty;
        private String Forma_Pago = String.Empty;
        private String Tipo_Comprobante = String.Empty;
        private String Condiciones = String.Empty;
        private String No_Cuenta_Pago = String.Empty;
        private String Metodo_Pago = String.Empty;
        private String Banco_Pago = String.Empty;
        private String Fecha_Emision = String.Empty;
        private String Fecha_Vencimiento = String.Empty;
        private String Fecha_Pago = String.Empty;
        private String Tipo_Factura = String.Empty;
        private String Tipo_Moneda = String.Empty;
        private String Tipo_Cambio = String.Empty;
        private String Subtotal = String.Empty;
        private String Subtotal_Cero = String.Empty;
        private String Retencion_Cedular = String.Empty;
        private String Retencion_Isr = String.Empty;
        private String Retencion_Iva = String.Empty;
        private String Retencion_Flete = String.Empty;
        private String Iva = String.Empty;
        private String Descuento = String.Empty;
        private String Total = String.Empty;
        private String Saldo = String.Empty;
        private String Abono = String.Empty;
        private String Pagada = String.Empty;
        private String Cancelada = String.Empty;
        private String Usuario_Cancelacion = String.Empty;
        private String Fecha_Cancelacion = String.Empty;
        private String Motivo_Cancelacion = String.Empty;
        private String Comentarios = String.Empty;
        private String Certificado = String.Empty;
        private String No_Certificado = String.Empty;
        private String No_Autorizacion = String.Empty;
        private String Anio_Autorizacion = String.Empty;
        private String Genero_Informe = String.Empty;
        private String Fecha_Informe_Sat = String.Empty;
        private String Usuario_Informe_Sat = String.Empty;
        private String Fecha_Creo_Xml = String.Empty;
        private DateTime Fecha_Creo_Xml_Factura= DateTime.MinValue;
        private String Usuario_Autorizo_Regenerar = String.Empty;
        private String Fecha_Autorizo_Regenerar = String.Empty;
        private String Usuario_Autorizo_Informe = String.Empty;
        private String Fecha_Autorizo_Informe = String.Empty;
        private String Timbre_Version = String.Empty;
        private String Timbre_Uuid = String.Empty;
        private String Timbre_Fecha_Timbrado = String.Empty;
        private String Timbre_Sello_Cfd = String.Empty;
        private String Timbre_No_Certificado_Sat = String.Empty;
        private String Timbre_Sello_Sat = String.Empty;
        private String Ruta_Codigo_Bd = String.Empty;
        private String Estatus = String.Empty;
        private String Tipo_Pago = String.Empty;
        private string Cadena_Original = string.Empty;
        private string Periodo_Pago = string.Empty;
        private string Tipo_servicio = string.Empty;
        private string Rpu = string.Empty;
        private string Predio = string.Empty;
        private String Cp_Facturacion = String.Empty;
        private String Estado_Fiscal_Id = String.Empty;
        private String Ciudad_Fiscal_ID = String.Empty;
        private String Usuario_ID = String.Empty;
        private String Usuario_Creo = String.Empty;
        #endregion

        #region Variables Públicas Facturacion
        public String P_Cp_Facturacion { get { return Cp_Facturacion; } set { Cp_Facturacion = value; } }
        public String P_Estado_Fiscal_Id { get { return Estado_Fiscal_Id; } set { Estado_Fiscal_Id = value; } }
        public String P_Ciudad_Fiscal_ID { get { return Ciudad_Fiscal_ID; } set { Ciudad_Fiscal_ID = value; } }
        public String P_Usuario_Id { get { return Usuario_ID; } set { Usuario_ID = value; } }
        public String P_No_Factura { get { return No_Factura; } set { No_Factura = value; } }
        public String P_No_Cotizacion { get { return No_Cotizacion; } set { No_Cotizacion = value; } }
        public String P_Serie { get { return Serie; } set { Serie = value; } }
        public String P_Cliente_Id { get { return Cliente_Id; } set { Cliente_Id = value; } }
        public String P_Email_Facturacion { get { return Email_Facturacion; } set { Email_Facturacion = value; } }
        public String P_Razon_Social_Facturacion { get { return Razon_Social_Facturacion; } set { Razon_Social_Facturacion = value; } }
        public String P_Rfc_Facturacion { get { return Rfc_Facturacion; } set { Rfc_Facturacion = value; } }
        public String P_Pais_Facturacion { get { return Pais_Facturacion; } set { Pais_Facturacion = value; } }
        public String P_Estado_Facturacion { get { return Estado_Facturacion; } set { Estado_Facturacion = value; } }
        public String P_Localidad_Facturacion { get { return Localidad_Facturacion; } set { Localidad_Facturacion = value; } }
        public String P_Colonia_Facturacion { get { return Colonia_Facturacion; } set { Colonia_Facturacion = value; } }
        public String P_Ciudad_Facturacion { get { return Ciudad_Facturacion; } set { Ciudad_Facturacion = value; } }
        public String P_Calle_Facturacion { get { return Calle_Facturacion; } set { Calle_Facturacion = value; } }
        public String P_Numero_Exterior_Facturacion { get { return Numero_Exterior_Facturacion; } set { Numero_Exterior_Facturacion = value; } }
        public String P_Numero_Interior_Facturacion { get { return Numero_Interior_Facturacion; } set { Numero_Interior_Facturacion = value; } }
        public String P_Cp { get { return Cp; } set { Cp = value; } }
        public String P_Dias_Credito { get { return Dias_Credito; } set { Dias_Credito = value; } }
        public String P_Porcentaje_Descuento { get { return Porcentaje_Descuento; } set { Porcentaje_Descuento = value; } }
        public String P_Orden_Compra { get { return Orden_Compra; } set { Orden_Compra = value; } }
        public String P_Forma_Pago { get { return Forma_Pago; } set { Forma_Pago = value; } }
        public String P_Tipo_Comprobante { get { return Tipo_Comprobante; } set { Tipo_Comprobante = value; } }
        public String P_Condiciones { get { return Condiciones; } set { Condiciones = value; } }
        public String P_No_Cuenta_Pago { get { return No_Cuenta_Pago; } set { No_Cuenta_Pago = value; } }
        public String P_Metodo_Pago { get { return Metodo_Pago; } set { Metodo_Pago = value; } }
        public String P_Banco_Pago { get { return Banco_Pago; } set { Banco_Pago = value; } }
        public String P_Fecha_Emision { get { return Fecha_Emision; } set { Fecha_Emision = value; } }
        public String P_Fecha_Vencimiento { get { return Fecha_Vencimiento; } set { Fecha_Vencimiento = value; } }
        public String P_Fecha_Pago { get { return Fecha_Pago; } set { Fecha_Pago = value; } }
        public String P_Tipo_Factura { get { return Tipo_Factura; } set { Tipo_Factura = value; } }
        public String P_Tipo_Moneda { get { return Tipo_Moneda; } set { Tipo_Moneda = value; } }
        public String P_Tipo_Cambio { get { return Tipo_Cambio; } set { Tipo_Cambio = value; } }
        public String P_Subtotal { get { return Subtotal; } set { Subtotal = value; } }
        public String P_Subtotal_Cero { get { return Subtotal_Cero; } set { Subtotal_Cero = value; } }
        public String P_Retencion_Cedular { get { return Retencion_Cedular; } set { Retencion_Cedular = value; } }
        public String P_Retencion_Isr { get { return Retencion_Isr; } set { Retencion_Isr = value; } }
        public String P_Retencion_Iva { get { return Retencion_Iva; } set { Retencion_Iva = value; } }
        public String P_Retencion_Flete { get { return Retencion_Flete; } set { Retencion_Flete = value; } }
        public String P_Iva { get { return Iva; } set { Iva = value; } }
        public String P_Descuento { get { return Descuento; } set { Descuento = value; } }
        public String P_Total { get { return Total; } set { Total = value; } }
        public String P_Saldo { get { return Saldo; } set { Saldo = value; } }
        public String P_Abono { get { return Abono; } set { Abono = value; } }
        public String P_Pagada { get { return Pagada; } set { Pagada = value; } }
        public String P_Cancelada { get { return Cancelada; } set { Cancelada = value; } }
        public String P_Usuario_Cancelacion { get { return Usuario_Cancelacion; } set { Usuario_Cancelacion = value; } }
        public String P_Fecha_Cancelacion { get { return Fecha_Cancelacion; } set { Fecha_Cancelacion = value; } }
        public String P_Motivo_Cancelacion { get { return Motivo_Cancelacion; } set { Motivo_Cancelacion = value; } }
        public String P_Comentarios { get { return Comentarios; } set { Comentarios = value; } }
        public String P_Certificado { get { return Certificado; } set { Certificado = value; } }
        public String P_No_Certificado { get { return No_Certificado; } set { No_Certificado = value; } }
        public String P_No_Autorizacion { get { return No_Autorizacion; } set { No_Autorizacion = value; } }
        public String P_Anio_Autorizacion { get { return Anio_Autorizacion; } set { Anio_Autorizacion = value; } }
        public String P_Genero_Informe { get { return Genero_Informe; } set { Genero_Informe = value; } }
        public String P_Fecha_Informe_Sat { get { return Fecha_Informe_Sat; } set { Fecha_Informe_Sat = value; } }
        public String P_Usuario_Informe_Sat { get { return Usuario_Informe_Sat; } set { Usuario_Informe_Sat = value; } }
        public String P_Fecha_Creo_Xml { get { return Fecha_Creo_Xml; } set { Fecha_Creo_Xml = value; } }
        public DateTime P_Fecha_Creo_Xml_Factura { get { return Fecha_Creo_Xml_Factura; } set { Fecha_Creo_Xml_Factura = value; } }
        public String P_Usuario_Autorizo_Regenerar { get { return Usuario_Autorizo_Regenerar; } set { Usuario_Autorizo_Regenerar = value; } }
        public String P_Fecha_Autorizo_Regenerar { get { return Fecha_Autorizo_Regenerar; } set { Fecha_Autorizo_Regenerar = value; } }
        public String P_Usuario_Autorizo_Informe { get { return Usuario_Autorizo_Informe; } set { Usuario_Autorizo_Informe = value; } }
        public String P_Fecha_Autorizo_Informe { get { return Fecha_Autorizo_Informe; } set { Fecha_Autorizo_Informe = value; } }
        public String P_Timbre_Version { get { return Timbre_Version; } set { Timbre_Version = value; } }
        public String P_Timbre_Uuid { get { return Timbre_Uuid; } set { Timbre_Uuid = value; } }
        public String P_Timbre_Fecha_Timbrado { get { return Timbre_Fecha_Timbrado; } set { Timbre_Fecha_Timbrado = value; } }
        public String P_Timbre_Sello_Cfd { get { return Timbre_Sello_Cfd; } set { Timbre_Sello_Cfd = value; } }
        public String P_Timbre_No_Certificado_Sat { get { return Timbre_No_Certificado_Sat; } set { Timbre_No_Certificado_Sat = value; } }
        public String P_Timbre_Sello_Sat { get { return Timbre_Sello_Sat; } set { Timbre_Sello_Sat = value; } }
        public String P_Ruta_Codigo_Bd { get { return Ruta_Codigo_Bd; } set { Ruta_Codigo_Bd = value; } }
        public String P_Estatus { get { return Estatus; } set { Estatus = value; } }
        public String P_Tipo_Pago { get { return Tipo_Pago; } set { Tipo_Pago = value; } }
        public string P_Cadena_Original { get { return Cadena_Original; } set { Cadena_Original = value; } }
        public string P_Periodo_Pago { get { return Periodo_Pago; } set { Periodo_Pago = value; } }
        public string P_No_Cuenta { get { return No_Cuenta; } set { No_Cuenta= value; } }
        public string P_Tipo_Servicio { get { return Tipo_servicio; } set { Tipo_servicio = value; } }
        public string P_Rpu { get { return Rpu; } set { Rpu = value; } }
        public string P_Predio_ID { get { return Predio; } set { Predio = value; } }
        public string P_Usuario_Creo { get { return Usuario_Creo; } set { Usuario_Creo = value; } }

        public string UsoCFDI { get; set; }
        public string P_Leyenda_Fiscal { get; set; }
        public double P_Total_Descuento { get; set; }

        public string P_SubTotal_No_Descuentos { get; set; }
        public string P_Cp_Lugar_Expedicion { get; set; }
        #endregion

        #endregion

        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_Datos_Factura()
        //DESCRIPCIÓN:      consulta los datos para facturacion
        //PARÁMETROS:       
        //CREO:             Ramón Baeza Yépez
        //FECHA_CREO:       05/12/2013
        //*******************************************************************************************************
        public DataTable Consultar_Datos_Factura()
        {
            return new Cls_Ope_Facturacion_Datos().Consultar_Datos_Factura(this);
        }



        public DataTable Consultar_Datos_Cliente(string rfc) {
            return new Cls_Ope_Facturacion_Datos().Consultar_Datos_Cliente_Por_RFC(rfc);
        }


        public DataTable Consultar_Datos_Usuarios_No_Registrados(string rfc)
        {
            return new Cls_Ope_Facturacion_Datos().Consultar_Datos_Usuarios_No_Registrados(rfc);
        }

        public DataTable Consultar_Diverso_Codigo_Barras(string codigo_barras)
        {
            return new Cls_Ope_Facturacion_Datos().Consultar_Datos_Diversos_Por_Codigo_Barras(codigo_barras);
        }

        public DataTable Consultar_Sancion_Codigo_Barras(string codigo_barras)
        {
            return new Cls_Ope_Facturacion_Datos().Consultar_Datos_Sanciones_Por_Codigo_Barras(codigo_barras);
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_Datos_Cliente()
        //DESCRIPCIÓN:      consulta los datos para la facturacion
        //PARÁMETROS:       
        //CREO:             Ramón Baeza Yépez
        //FECHA_CREO:       05/12/2013
        //*******************************************************************************************************
        public DataTable Consultar_Datos_Cliente()
        {
            return new Cls_Ope_Facturacion_Datos().Consultar_Datos_Cliente(this);
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_Tabla_Cliente()
        //DESCRIPCIÓN:      consulta los datos para la facturacion
        //PARÁMETROS:       
        //CREO:             Miguel Angel Alvarado Enriquez
        //FECHA_CREO:       19/Feb/2014
        //*******************************************************************************************************
        public DataTable Consultar_Tabla_Cliente()
        {
            return new Cls_Ope_Facturacion_Datos().Consultar_Tabla_Cliente(this);
        }
        public DataTable Consultar_Existe_Rezago()
        {
            return new Cls_Ope_Facturacion_Datos().Consultar_Existe_Rezago(this);
        }

        public DataTable Consultar_Datos_Pago()
        {
            return new Cls_Ope_Facturacion_Datos().Consultar_Datos_Pago(this);
        }


        public string Consultar_Diverso_RFC(string codigo_barras) {

            return new Cls_Ope_Facturacion_Datos().Consultar_Diverso_RFC(codigo_barras);
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_No_Cuenta()
        //DESCRIPCIÓN:      consulta los datos para la facturacion
        //PARÁMETROS:       
        //CREO:             Miguel Angel Alvarado Enriquez
        //FECHA_CREO:       04/01/2014
        //*******************************************************************************************************
        public DataTable Consultar_No_Cuenta()
        {
            return new Cls_Ope_Facturacion_Datos().Consultar_No_Cuenta(this);
        }

        public string Consultar_Lugar_Expedicion(string noRecibo)
        {
            return new Cls_Ope_Facturacion_Datos().Consultar_Lugar_Expedicion(noRecibo);
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_Facturacion_Detalles()
        //DESCRIPCIÓN:      consulta los detalles de la facturacion
        //PARÁMETROS:       
        //CREO:             Ramón Baeza Yépez
        //FECHA_CREO:       05/12/2013
        //*******************************************************************************************************
        public DataTable Consultar_Detalles_Pago()
        {
            return new Cls_Ope_Facturacion_Datos().Consultar_Detalles_Pago(this);
        }
        public DataTable Consultar_Detalles_Pago_PDF()
        {
            return new Cls_Ope_Facturacion_Datos().Consultar_Detalles_Pago_PDF(this);
        }

        public DataTable Consultar_Detalles_Pago_Facturado()
        {
            return new Cls_Ope_Facturacion_Datos().Consultar_Detalles_Pago_Facturado(this);
        }
        
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Modificar_Datos_Cliente()
        //DESCRIPCIÓN:      modifica los datos de los clientes
        //PARÁMETROS:       
        //CREO:             Ramón Baeza Yépez
        //FECHA_CREO:       05/12/2013
        //*******************************************************************************************************
        public void Modificar_Datos_Cliente()
        {
            new Cls_Ope_Facturacion_Datos().Modificar_Datos_Cliente(this);
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Modificar_Email()
        //DESCRIPCIÓN:      modifica los datos de los clientes
        //PARÁMETROS:       
        //CREO:             mIGUEL aNGEL aLVARADO eNRIQUEZ
        //FECHA_CREO:       12/05/2014
        //*******************************************************************************************************
        public void Modificar_Email()
        {
            new Cls_Ope_Facturacion_Datos().Modificar_Email(this);
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_Estados()
        //DESCRIPCIÓN:      consulta los estados de la facturacion
        //PARÁMETROS:       
        //CREO:             Ramón Baeza Yépez
        //FECHA_CREO:       05/12/2013
        //*******************************************************************************************************
        public DataTable Consultar_Estados()
        {
            return new Cls_Ope_Facturacion_Datos().Consultar_Estados(this);
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_Datos_Fiscales()
        //DESCRIPCIÓN:      consulta los estados de la facturacion
        //PARÁMETROS:       
        //CREO:             Ramón Baeza Yépez
        //FECHA_CREO:       05/12/2013
        //*******************************************************************************************************
        public DataTable Consultar_Datos_Fiscales()
        {
            return new Cls_Ope_Facturacion_Datos().Consultar_Datos_Fiscales(this);
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Factura
        ///DESCRIPCIÓN          : Manda llamar el método de Alta_Factura de la clase de datos
        ///PARAMETROS           : 
        ///CREO                 : Miguel Angel Alvarado Enriquez
        ///FECHA_CREO           : 20 Diciembre 2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public Boolean Alta_Factura(DataTable Dt_Detalle_Factura,SqlConnection objConexion)
        {
            return Cls_Ope_Facturacion_Datos.Alta_Factura(this, Dt_Detalle_Factura, objConexion);
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Ultima_Factura
        ///DESCRIPCIÓN          : Manda llamar el método de Consultar_Ultima_Factura de la clase de datos
        ///PARAMETROS           : 
        ///CREO                 : Miguel Angel Alvarado enriquez
        ///FECHA_CREO           : 03 Octubre 2013 01:43 pm
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public DataTable Consultar_Ultima_Factura()
        {
            return Cls_Ope_Facturacion_Datos.Consultar_Ultima_Factura(this);
        }
        public DataTable Consultar_Ultima_Factura(SqlConnection objConexion)
        {
            return Cls_Ope_Facturacion_Datos.Consultar_Ultima_Factura(this, objConexion);
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Factura
        ///DESCRIPCIÓN          : Manda llamar el método de Consultar_Factura de la clase de datos
        ///PARAMETROS           : 
        ///CREO                 : Miguel Angel Alvarado Enriquez
        ///FECHA_CREO           : 28/Diciembre/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public DataTable Consultar_Factura()
        {
            return Cls_Ope_Facturacion_Datos.Consultar_Factura(this);
        }
        public DataTable Consultar_Factura(SqlConnection objConexion)
        {
            return Cls_Ope_Facturacion_Datos.Consultar_Factura(this, objConexion);
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Serie
        ///DESCRIPCIÓN          : Manda llamar el método de Alta_Serie de la clase de datos
        ///PARAMETROS           : 
        ///CREO                 : Miguel Angel Alvarado Enriquez
        ///FECHA_CREO           : 26/Diciembre/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public Boolean Alta_Cliente()
        {
            return Cls_Ope_Facturacion_Datos.Alta_Cliente(this);
        }
       ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Costos_Factores
        ///DESCRIPCIÓN          : Manda llamar el método de Consultar_Costos_Factores de la clase de datos
        ///PARAMETROS           : 
        ///CREO                 : Miguel Angel Alvarado enriquez
        ///FECHA_CREO           : 20 Marzo 2014 01:43 pm
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public DataTable Consultar_Costos_Factores()
        {
            return Cls_Ope_Facturacion_Datos.Consultar_Costos_Factores(this);
        }

        public DataTable Consultar_Datos_Estado_Cuenta()
        {
            return new Cls_Ope_Facturacion_Datos().Consultar_Datos_Estado_Cuenta(this);
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN:   Consultar_Datos_Factura_Global()
        //DESCRIPCIÓN:      consulta los datos de la facturación Global
        //PARÁMETROS:       
        //CREO:             Ramón Baeza Yépez
        //FECHA_CREO:       05/12/2013
        //*******************************************************************************************************
        public DataTable Consultar_Datos_Factura_Global()
        {
            return new Cls_Ope_Facturacion_Datos().Consultar_Datos_Factura_Global(this);
        }

        public DataTable Consultar_Recibo()
        {
            return new Cls_Ope_Facturacion_Datos().Consultar_Recibo(this);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION: Obtener_Conceptos_Facturacion_Clase
        ///DESCRIPCION         : Obtiene la datos de facturacion electronica
        ///PARAMETROS          : 1.codigoBarras     
        ///CREO                : Jesus Toledo Rodriguez
        ///FECHA_CREO          : 12/Diciembre/2013
        ///MODIFICO            :                
        ///FECHA_MODIFICO      :          
        ///CAUSA_MODIFICACION  :      
        ///*******************************************************************************
        public Cls_Bean_Conceptos_Facturacion_Electronica Obtener_Conceptos_Facturacion_Clase()
        {
            List<Cls_Bean_Conceptos_Facturacion_Electronica> Lista_Objetos = new List<Cls_Bean_Conceptos_Facturacion_Electronica>();
            Cls_Bean_Conceptos_Facturacion_Electronica Respuesta = new Cls_Bean_Conceptos_Facturacion_Electronica();
            DataTable Dt_Datos;
            String Datos;

            try
            {
                Dt_Datos = new Cls_Ope_Facturacion_Datos().Consultar_Detalles_Pago(this);
                Datos = Cls_Util.dataTableToJSONsintotalrenglones(Dt_Datos);

                if (Datos.Length > 2)
                    Lista_Objetos = JsonConvert.DeserializeObject<List<Cls_Bean_Conceptos_Facturacion_Electronica>>(Datos);
                if (Lista_Objetos.Count > 0)
                {
                    Respuesta = Lista_Objetos[0];
                }

            }
            catch (Exception ex)
            {
            }
            return Respuesta;
        }

        public DataTable Consultar_Facturas_Clientes()
        {
            return Cls_Ope_Facturacion_Datos.Consultar_Facturas_Clientes(this);
        }

        public DataTable Consultar_Predios()
        {
            return Cls_Ope_Facturacion_Datos.Consultar_Predios(this);
        }

        public DataTable Consultar_Facturas_Recibos()
        {
            return Cls_Ope_Facturacion_Datos.Consultar_Facturas_Recibos(this);
        }

        public DataTable Consultar_Medidores()
        {
            return Cls_Ope_Facturacion_Datos.Consultar_Medidores(this);
        }

        public DataTable Consultar_Usuarios()
        {
            return Cls_Ope_Facturacion_Datos.Consultar_Usuarios(this);
        }

        public DataTable Consultar_Totales()
        {
            return Cls_Ope_Facturacion_Datos.Consultar_Totales(this);
        }

        public DataTable Consultar_Rpu()
        {
            return Cls_Ope_Facturacion_Datos.Consultar_Rpu(this);
        }

        public void Modificar_Datos_Usuarios()
        {
            Cls_Ope_Facturacion_Datos.Modificar_Datos_Usuarios(this);
        }

        public DataTable Consultar_Bancos()
        {
           return Cls_Ope_Facturacion_Datos.Buscar_Bancos(this);
        }

        public DataTable Consultar_Detalles_Recibo(string No_Cuenta)
        {
            return Cls_Ope_Facturacion_Datos.Consultar_Detalles_Recibos(No_Cuenta);
        }

        public double consultar_periodos_Adeudos(string p)
        {
            return Cls_Ope_Facturacion_Datos.Obtener_Periodos_Adeudos(p);
        }

        public double Obtener_saldo_favor(string rpu)
        {
            return Cls_Ope_Facturacion_Datos.Obtener_Saldo_a_Favor(rpu);
        }

        public bool Verificar_Importe_Iva(string conceptoId,SqlConnection objConexion)
        {
            return Cls_Ope_Facturacion_Datos.Verificar_Importe_Iva(conceptoId, objConexion);
        }

        public string Verificar_Es_Descuento(string conceptoId)
        {
            return Cls_Ope_Facturacion_Datos.Verificar_Es_Descuento(conceptoId);
        }

        public DataTable Verificar_Es_Sancion(string conceptoId)
        {
            return Cls_Ope_Facturacion_Datos.Verificar_Es_Sancion(conceptoId);
        }

        public Int64 InsertarFacturaParaObtenerElConsecutivo(Cls_Ope_Facturacion_Negocio negocio, SqlConnection conexion)
        {
            return Cls_Ope_Facturacion_Datos.InsertarFacturaParaObtenerElConsecutivo(negocio, conexion);
        }
    }
}