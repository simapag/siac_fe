﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SIAC_Cat_Apl_Parametros_Correo.Datos;

namespace SIAC_Cat_Apl_Parametros_Correo.Negocio
{
    public class Cls_Apl_Parametros_Correo_Negocio
    {
        #region Variables

        private String Parametro_Id = String.Empty;
        private String Email = String.Empty;
        private String Password = String.Empty;
        private String Host = String.Empty;
        private String Puerto = String.Empty;
        private String Imagen_Empresa = String.Empty;
        private String Tipo_Movimiento = String.Empty;
        private String Mensaje_Sistema = String.Empty;
        private String Usuario_Creo = String.Empty;
        private String Ip_Creo = String.Empty;
        private String Equipo_Creo = String.Empty;
        private String Fecha_Creo = String.Empty;
        private String Usuario_Modifico = String.Empty;
        private String Ip_Modifico = String.Empty;
        private String Equipo_Modifico = String.Empty;
        private String Fecha_Modifico = String.Empty;
        #endregion

        #region Variables Publicas

        public String P_Parametro_Id
        {
            get { return Parametro_Id; }
            set { Parametro_Id = value; }
        }
        public String P_Email
        {
            get { return Email; }
            set { Email = value; }
        }
        public String P_Password
        {
            get { return Password; }
            set { Password = value; }
        }
        public String P_Host
        {
            get { return Host; }
            set { Host = value; }
        }
        public String P_Puerto
        {
            get { return Puerto; }
            set { Puerto = value; }
        }
        public String P_Imagen_Empresa
        {
            get { return Imagen_Empresa; }
            set { Imagen_Empresa = value; }
        }
        public String P_Usuario_Creo
        {
            get { return Usuario_Creo; }
            set { Usuario_Creo = value; }
        }
        public String P_Ip_Creo
        {
            get { return Ip_Creo; }
            set { Ip_Creo = value; }
        }
        public String P_Equipo_Creo
        {
            get { return Equipo_Creo; }
            set { Equipo_Creo = value; }
        }
        public String P_Fecha_Creo
        {
            get { return Fecha_Creo; }
            set { Fecha_Creo = value; }
        }
        public String P_Usuario_Modifico
        {
            get { return Usuario_Modifico; }
            set { Usuario_Modifico = value; }
        }
        public String P_Ip_Modifico
        {
            get { return Ip_Modifico; }
            set { Ip_Modifico = value; }
        }
        public String P_Equipo_Modifico
        {
            get { return Equipo_Modifico; }
            set { Equipo_Modifico = value; }
        }
        public String P_Fecha_Modifico
        {
            get { return Fecha_Modifico; }
            set { Fecha_Modifico = value; }
        }
        public String P_Mensaje_Sistema
        {
            get { return Mensaje_Sistema; }
            set { Mensaje_Sistema = value; }
        }
        public String P_Tipo_Movimiento
        {
            get { return Tipo_Movimiento; }
            set { Tipo_Movimiento = value; }
        }
        
        
        #endregion

        #region Metodos

        ///////*******************************************************************************
        ///////NOMBRE DE LA FUNCIÓN : Alta_Parametros
        ///////DESCRIPCIÓN          : Ingresa al metodo que realizara el alta de registros.
        ///////PARAMETROS           : 
        ///////CREO                 : Miguel Angel Alvarado Enriquez
        ///////FECHA_CREO           : 15/Febrero/2013 01:40 p.m.
        ///////MODIFICO             :
        ///////FECHA_MODIFICO       :
        ///////CAUSA_MODIFICACIÓN   :
        ///////*******************************************************************************
        public void Alta_Parametros()
        {
            Cls_Cat_Apl_Parametros_Correos_Datos.Alta_Parametros(this);
        }
        ///////*******************************************************************************
        ///////NOMBRE DE LA FUNCIÓN : Modificar_Parametro
        ///////DESCRIPCIÓN          : Ingresa al metodo que realizara la modificación de registros.
        ///////PARAMETROS           : 
        ///////CREO                 : Miguel Angel Alvarado Enriquez
        ///////FECHA_CREO           : 11/Marzo/2013 09:45 a.m.
        ///////MODIFICO             :
        ///////FECHA_MODIFICO       :
        ///////CAUSA_MODIFICACIÓN   :
        ///////*******************************************************************************
        public void Modificar_Parametro()
        {
            Cls_Cat_Apl_Parametros_Correos_Datos.Modificar_Parametro(this);
        }
        ///////*******************************************************************************
        ///////NOMBRE DE LA FUNCIÓN : Consultar_Parametros
        ///////DESCRIPCIÓN          : Regresa un DataTable con los Parametros encontrados.
        ///////PARAMETROS           : 
        ///////CREO                 : Miguel Angel Alvarado Enriquez
        ///////FECHA_CREO           : 11/Marzo/2013 09:45 a.m.
        ///////MODIFICO             :
        ///////FECHA_MODIFICO       :
        ///////CAUSA_MODIFICACIÓN   :
        ///////*******************************************************************************
        public DataTable Consultar_Parametros()
        {
            return Cls_Cat_Apl_Parametros_Correos_Datos.Consultar_Parametros(this);
        }

        #endregion
    }
}