﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.VisualBasic;
using System.Net.Mail;
using System.Net.Mime;

//using SIAC_Cat_Apl_Parametros_Correo.Negocio;
using SIAC.Constantes;
using SIAC.Seguridad;
using Erp.Sesiones;
using SIAC_Cat_Apl_Parametros_Correo.Negocio;

namespace SIAC.Envio_Email
{
    public class Cls_Enviar_Correo
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Envia_Correo
        ///DESCRIPCIÓN  : Envia un correo a un usuario
        ///PARAMENTROS  :
        ///CREO         : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO   : 25/Feb/2013
        ///MODIFICO     :
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static bool Envia_Correo(String Mensaje, String Correo_Destinatario, String Asunto)
        {
            try
            {
                Cls_Apl_Parametros_Correo_Negocio Consulta_Parametros = new Cls_Apl_Parametros_Correo_Negocio();
                Consulta_Parametros.P_Parametro_Id = "00001";
                DataTable Dt_Consulta = Consulta_Parametros.Consultar_Parametros();


                if (Dt_Consulta != null)
                {
                    if (Dt_Consulta.Rows.Count >= 1)
                    {
                        DataTable Dt_Usuarios = new DataTable();
                        System.Net.Mail.MailMessage Mensaje_Correo = new System.Net.Mail.MailMessage();
                        Mensaje_Correo.To.Add(Correo_Destinatario);
                        Mensaje_Correo.From = new MailAddress(Dt_Consulta.Rows[0][Apl_Parametros_Correos.Campo_Email].ToString().Trim(), "SIMAPAG", System.Text.Encoding.UTF8);
                        Mensaje_Correo.Subject = Asunto;
                        Mensaje_Correo.SubjectEncoding = System.Text.Encoding.UTF8;
                        Mensaje_Correo.Body = Mensaje;
                        Mensaje_Correo.BodyEncoding = System.Text.Encoding.UTF8;
                        Mensaje_Correo.IsBodyHtml = true;

                        SmtpClient Servidor = new SmtpClient();
                        Servidor.Credentials = new System.Net.NetworkCredential(Dt_Consulta.Rows[0][Apl_Parametros_Correos.Campo_Email].ToString().Trim(), Cls_Seguridad.Desencriptar(Dt_Consulta.Rows[0][Apl_Parametros_Correos.Campo_Contrasenia].ToString().Trim()));
                        Servidor.Port = Int16.Parse(Dt_Consulta.Rows[0][Apl_Parametros_Correos.Campo_Puerto].ToString().Trim());
                        Servidor.Host = Dt_Consulta.Rows[0][Apl_Parametros_Correos.Campo_Host].ToString().Trim();
                        Servidor.EnableSsl = true;
                        Servidor.Timeout = 200000;
                        Servidor.Send(Mensaje_Correo);
                        Mensaje_Correo.Dispose();
                    }
                }
            }
            catch (Exception E)
            {
                throw new Exception("Enviar_Correo: " + E.Message);
            }
            return false;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Enviar_Correo_Archivos
        ///DESCRIPCIÓN  : Envia un correo a un usuario con archivos adjuntos
        ///PARAMENTROS  : 
        ///               Archivos, Arreglo con las rutas de los archvos.
        ///                 sino se quiere mandar archivos adjuntos, enviar null.
        ///               Mensjae, Cuerpo del mensaje.
        ///               Correo_Destinario, Email al que va dirigido el mensaje.
        ///               Correo_Origen, Email de quien manda el correo.
        ///               Asunto, Asunto del mensaje.
        ///CREO         : Luis Alberto Salas
        ///FECHA_CREO   : 01/Julio/2013
        ///MODIFICO     :
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static bool Enviar_Correo_Archivos(String[] Archivos, String Mensaje, String Correo_Destinatario, String Correo_Origen, String Asunto)
        {
            try
            {
                Cls_Apl_Parametros_Correo_Negocio Consulta_Parametros = new Cls_Apl_Parametros_Correo_Negocio();
                Consulta_Parametros.P_Parametro_Id = "00001";
                DataTable Dt_Consulta = Consulta_Parametros.Consultar_Parametros();

                if (Dt_Consulta != null)
                {
                    if (Dt_Consulta.Rows.Count >= 1)
                    {
                        DataTable Dt_Usuarios = new DataTable();
                        MailMessage Mensaje_Correo = new MailMessage();
                        //Separar_Cadena_Enviar
                        //Correo_Destinatario += ";";
                        string[] correos = Correo_Destinatario.Split(';', ',');
                        foreach (string correo in correos)
                        {
                            if (!correo.Trim().Equals(""))
                            {
                                Mensaje_Correo.To.Add(correo);
                            }
                        }
                        //Mensaje_Correo.To.Add(Correo_Destinatario);
                        Mensaje_Correo.From = new MailAddress(Dt_Consulta.Rows[0]["Email"].ToString(), "SIMAPAG", System.Text.Encoding.UTF8);
                        Mensaje_Correo.Subject = Asunto;
                        Mensaje_Correo.SubjectEncoding = System.Text.Encoding.UTF8;
                        Mensaje_Correo.Body = Mensaje;
                        Mensaje_Correo.BodyEncoding = System.Text.Encoding.UTF8;
                        Mensaje_Correo.IsBodyHtml = true;

                        // Adjunta los archivos  
                        if (Archivos != null)
                        {
                            foreach (String Ruta_Archivo in Archivos)
                            {
                                Attachment Data = new Attachment(Ruta_Archivo, MediaTypeNames.Application.Octet);

                                // Agrega la informacion del tiempo del archivo.
                                ContentDisposition disposition = Data.ContentDisposition;
                                disposition.DispositionType = DispositionTypeNames.Attachment;

                                // Agrega los archivos adjuntos al mensaje
                                Mensaje_Correo.Attachments.Add(Data);
                            }
                        }
                       
                        SmtpClient Servidor = new SmtpClient();
                        Servidor.Credentials = new System.Net.NetworkCredential(Dt_Consulta.Rows[0][Apl_Parametros_Correos.Campo_Email].ToString(), Cls_Seguridad.Desencriptar(Dt_Consulta.Rows[0][Apl_Parametros_Correos.Campo_Contrasenia].ToString()));
                        Servidor.Port = Int16.Parse(Dt_Consulta.Rows[0][Apl_Parametros_Correos.Campo_Puerto].ToString());
                        Servidor.Host = Dt_Consulta.Rows[0][Apl_Parametros_Correos.Campo_Host].ToString();
                        Servidor.EnableSsl = true;
                        Servidor.Send(Mensaje_Correo);

                        return true;
                    }
                }
            }
            catch (Exception E)
            {
                throw new Exception("Enviar_Correo_Archivos: " + E.Message);
            }
            return false;
        }
    }
}
