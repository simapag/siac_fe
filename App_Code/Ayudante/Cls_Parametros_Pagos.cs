﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace SIAC.Parametros_Pagos
{

    public class Cls_Parametros_Pagos
    {

        public static string RequstForm(string name)
        {

            return (HttpContext.Current.Request.Form[name] == null ? string.Empty : HttpContext.Current.Request.Form[name].ToString().Trim());

        }

        public static string RequstString(string sParam)
        {

            return (HttpContext.Current.Request[sParam] == null ? string.Empty : HttpContext.Current.Request[sParam].ToString().Trim());

        }

        public static string s_transm
        {
            get { return RequstString("s_transm"); }
        }

        public static string c_referencia
        {
            get { return RequstString("c_referencia"); }
        }


        public static string t_servicio
        {
            get { return RequstString("t_servicio"); }
        }

        public static string t_importe
        {
            get { return RequstString("t_importe"); }
        }

        public static string sha1
        {
            get { return RequstString("val_13"); }
        }

        public static string autorizacion {
            get { return RequstString("n_autoriz"); }
        }


        public static string fecha_pago {
            get { return RequstString("val_10"); }
        }

        public static string numero_tarjeta_credito {
            get { return RequstString("val_9"); }
        }

        public static string correo_electronico {
            get { return RequstString("val_11"); }
        }

        public static string tipo_tarejeta
        {
            get { return RequstString("t_pago"); }
        }

        public static string fecha_paga_american_express
        {
            get { return RequstString("val_2"); }
        }

        public static string numero_tarjeta_america_express
        {
            get { return RequstString("val_3"); }
        }
        public static string banco
        {
            get { return RequstString("banco"); }
        }
        public static string banco_id
        {
            get { return RequstString("n_banco"); }
        }

    } // fin de la clase
}