﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIAC.Constantes;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
    public class Cls_Util
    {
        public Cls_Util() { }

        #region Métodos
        public static string dataTableToJSONsintotalrenglones(DataTable dt)
        {
            StringBuilder jsonStringBuilder = new StringBuilder();
            StringWriter jsonStringWriter = new StringWriter(jsonStringBuilder);
            String cadenaFinal, valor = "";
            JsonWriter jsonWriter;
            int i, j;

            jsonWriter = new JsonTextWriter(jsonStringWriter);

            if (dt != null && dt.Rows.Count > 0)
            {
                jsonWriter.Formatting = Formatting.None;
                jsonWriter.WriteStartArray();

                for (i = 0; i < dt.Rows.Count; i++)
                {
                    jsonWriter.WriteStartObject();
                    for (j = 0; j < dt.Columns.Count; j++)
                    {
                        jsonWriter.WritePropertyName(dt.Columns[j].ColumnName.ToString().ToLower());
                        valor = dt.Rows[i][j].ToString();
                        jsonWriter.WriteValue(valor.Trim());
                    }
                    jsonWriter.WriteEndObject();

                }
                jsonWriter.WriteEndArray();
                cadenaFinal = jsonStringBuilder.ToString();
                return cadenaFinal;
            }

            return "{}";

        }

        #endregion
    }
