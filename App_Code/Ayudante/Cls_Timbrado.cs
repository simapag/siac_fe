﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Facturas;
using System.Xml;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing;
using System.Data;

using SIAC.Facturacion.Negocio;
using SIAC.Constantes;

using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using CryptoSysPKI;
using System.Text;
using System.Text.RegularExpressions;
//using Erp_Ope_Notas_Credito.Negocio;
using SIAC.Metodos_Generales;
using System.Data.SqlClient;

namespace SIAC.Timbrado
{
    public class Cls_Timbrado
    {
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Generar_Timbrado
        ///DESCRIPCIÓN: Genera el timbrado de las facturas
        ///PARAMETROS:  
        ///             Timbre_Produccion, Determina si el timbre es para produccion o de prueba
        ///             Version, Timbre version
        ///             Codigo_Usuario_Proveedor, Codigo de usuario de proveedor
        ///             Codigo_Usuario, Codigo del usuario
        ///             Id_Sucursal, Id de la sucursal
        ///             Texto_Xml, Cadena que contiene la informacion de la factura
        ///CREO:        Luis Alberto Salas Garcia
        ///FECHA_CREO:  30/Mayo/2013
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static String Generar_Timbrado(Boolean Timbre_Produccion, String Version, String Codigo_Usuario_Proveedor, String Codigo_Usuario, String Id_Sucursal, String Texto_Xml)
        {
            TimbreFiscalDigitalSoapClient Timbrado = new TimbreFiscalDigitalSoapClient();
            String Respuesta = String.Empty;
            String Parametros = String.Empty;
            XmlDocument Xml_Documento = new XmlDocument();
            XmlDocument Xml_Parametro = new XmlDocument();
            XmlElement Raiz;
            XmlDeclaration Declaracion;

            try
            {
                if (!String.IsNullOrEmpty(Version) && !String.IsNullOrEmpty(Codigo_Usuario_Proveedor) && !String.IsNullOrEmpty(Codigo_Usuario)
                    && !String.IsNullOrEmpty(Id_Sucursal) && !String.IsNullOrEmpty(Texto_Xml))
                {
                    if (Xml_Parametro.ChildNodes.Count == 0)
                    {
                        Declaracion = Xml_Parametro.CreateXmlDeclaration("1.0", "UTF-8", String.Empty);
                        Xml_Parametro.AppendChild(Declaracion);
                        Raiz = Xml_Parametro.CreateElement("Parametros");
                    }
                    else
                    {
                        Raiz = Xml_Parametro.DocumentElement;
                        Raiz.RemoveAll();
                    }

                    Raiz.SetAttribute("Version", Version);
                    Raiz.SetAttribute("CodigoUsuarioProveedor", Codigo_Usuario_Proveedor);
                    Raiz.SetAttribute("CodigoUsuario", Codigo_Usuario);
                    Raiz.SetAttribute("IdSucursal", Id_Sucursal);
                    Raiz.SetAttribute("TextoXml", Texto_Xml);

                    Xml_Parametro.AppendChild(Raiz);

                    Parametros = Xml_Parametro.InnerXml;

                    // Ejecuta el web service con los parametros
                    if (Timbre_Produccion) // Si es de produccion
                    {
                        Timbrado.GenerarTimbre(Parametros, out Respuesta);
                    }
                    else // Si es para prueba
                    {
                        Timbrado.GenerarTimbrePrueba(Parametros, out Respuesta);
                    }
                }
                else
                {
                    Respuesta = "Error: ";
                    if (String.IsNullOrEmpty(Version))
                    {
                        Respuesta += "Proporcione la versi&oacute;n";
                    }
                    if (String.IsNullOrEmpty(Codigo_Usuario_Proveedor))
                    {
                        Respuesta = (Respuesta.Length > 10 ? ", el codigo de usuario proveedor" : "Proporcione el codigo de usuario proveedor");
                    }
                    if (String.IsNullOrEmpty(Codigo_Usuario))
                    {
                        Respuesta = (Respuesta.Length > 10 ? ", el codigo de usuario" : "Proporcione el codigo de usuario");
                    }
                    if (String.IsNullOrEmpty(Id_Sucursal))
                    {
                        Respuesta = (Respuesta.Length > 10 ? ", el codigo del id de la sucursal" : "Proporcione el codigo del id de la sucursal");
                    }
                    if (String.IsNullOrEmpty(Texto_Xml))
                    {
                        Respuesta = (Respuesta.Length > 10 ? ", el XML en texto" : "Proporcione el XML en texto");
                    }
                    Respuesta += " del Timbrado.";
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error Timbrado: " + Ex.Message);
            }

            return Respuesta;
        }

        public static String Generar_Timbrado_v3(Boolean Timbre_Produccion, String Version, String Codigo_Usuario_Proveedor, String Codigo_Usuario, String Id_Sucursal, String Texto_Xml)
        {

            ws_timbrado.WSTimbradoClient Timbrado33 = null;
            ws_timbrado.ParametrosEntradaWSTimbrado Parametros_Entrada;
            ws_timbrado.ParametrosSalidaWSTimbrado Parametros_Salida;
            //Pruebas
            ws_timbradopruebas.WSTimbradoClient Timbrado_Prueba33 = null;
            ws_timbradopruebas.ParametrosEntradaWSTimbrado Parametros_Prueba_Entrada;
            ws_timbradopruebas.ParametrosSalidaWSTimbrado Parametros_Prueba_Salida;

            String Respuesta = string.Empty;

           if (Timbre_Produccion) // Si es de produccion
            {
                Parametros_Entrada = new ws_timbrado.ParametrosEntradaWSTimbrado();
                Parametros_Entrada.Usuario = "CONECTIVIDAD";
                Parametros_Entrada.Contrasenia = "c0n3ct1v1d4D#Pr0dUc";
                Parametros_Entrada.XMLPreCFDI = Texto_Xml.ToString();
                Parametros_Entrada.XMLOpcionales = "";
                Parametros_Entrada.IdAddenda = 0;
                Parametros_Entrada.NodoAddenda = "";
                Parametros_Entrada.MailPara = "";
                Parametros_Entrada.MailCC = "";
                Parametros_Entrada.UUIDSucursal = "";
                Parametros_Entrada.IdLlaveUnico = "";
                Timbrado33 = new ws_timbrado.WSTimbradoClient();
                Parametros_Salida = Timbrado33.TimbrarObjeto(Parametros_Entrada);
                Timbrado33.Close();

                if (!string.IsNullOrEmpty(Parametros_Salida.IdValidacion) || !string.IsNullOrEmpty(Parametros_Salida.MensajeValidacion))
                    throw new Exception(string.Format("Error Timbrado: {0}-{1}", Parametros_Salida.IdValidacion, Parametros_Salida.MensajeValidacion));

                Respuesta = Parametros_Salida.XMLTimbrado;
                //Respuesta = JsonConvert.SerializeObject(Parametros_Salida, Formatting.Indented, configuracionJson);
            }
            else // Si es para prueba
            {
                Parametros_Prueba_Entrada = new ws_timbradopruebas.ParametrosEntradaWSTimbrado();
                Parametros_Prueba_Entrada.Usuario = "demo";
                Parametros_Prueba_Entrada.Contrasenia = "123456";
                Parametros_Prueba_Entrada.XMLPreCFDI = Texto_Xml.ToString();
                Parametros_Prueba_Entrada.XMLOpcionales = "";
                Parametros_Prueba_Entrada.IdAddenda = 0;
                Parametros_Prueba_Entrada.NodoAddenda = "";
                Parametros_Prueba_Entrada.MailPara = "";
                Parametros_Prueba_Entrada.MailCC = "";
                Parametros_Prueba_Entrada.UUIDSucursal = "";
                Parametros_Prueba_Entrada.IdLlaveUnico = "";
                Timbrado_Prueba33 = new ws_timbradopruebas.WSTimbradoClient();
                Parametros_Prueba_Salida = Timbrado_Prueba33.TimbrarObjeto(Parametros_Prueba_Entrada);
                Timbrado_Prueba33.Close();

                if(!string.IsNullOrEmpty(Parametros_Prueba_Salida.IdValidacion) || !string.IsNullOrEmpty(Parametros_Prueba_Salida.MensajeValidacion))
                    throw new Exception(string.Format("Error Timbrado: {0}-{1}", Parametros_Prueba_Salida.IdValidacion, Parametros_Prueba_Salida.MensajeValidacion));

                Respuesta = Parametros_Prueba_Salida.XMLTimbrado;

                //Respuesta = JsonConvert.SerializeObject(Parametros_Prueba_Salida, Formatting.Indented, configuracionJson);
            }

            return Respuesta;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Cancelar_Factura
        ///DESCRIPCIÓN: Cancela la factura ante el SAT.
        ///PARAMETROS:  
        ///             Timbre_Produccion, Determina si el timbre es para produccion o de prueba
        ///             Texto_Xml, Cadena que contiene la informacion de la factura
        ///CREO:        Luis Alberto Salas Garcia
        ///FECHA_CREO:  24/Junio/2013
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static String Cancelar_Factura(Boolean Timbre_Produccion, String RFC_Emisor, String Codigo_Usuario_Proveedor, String Codigo_Usuario, String Codigo_UUID)
        {
            TimbreFiscalDigitalSoapClient Timbrado = new TimbreFiscalDigitalSoapClient();
            String Respuesta = String.Empty;
            String Parametros = String.Empty;
            XmlDocument Xml_Parametro = new XmlDocument();
            XmlNode Raiz;
            XmlNode xmlFolios;
            XmlNode xmlUUID;
            XmlNode Declaracion;
            XmlAttribute Atributo;

            try
            {
                if (!String.IsNullOrEmpty(Codigo_Usuario_Proveedor) && !String.IsNullOrEmpty(Codigo_Usuario)
                    && !String.IsNullOrEmpty(RFC_Emisor) && !String.IsNullOrEmpty(Codigo_UUID)) 
                {
                    //Forma los parametros de entrada
                    Declaracion = Xml_Parametro.CreateXmlDeclaration("1.0", "UTF-8", String.Empty);
                    Xml_Parametro.AppendChild(Declaracion);
                    Raiz = Xml_Parametro.CreateNode(XmlNodeType.Element, "Parametros", "");
                    Xml_Parametro.AppendChild(Raiz);
                    
                    Atributo = Raiz.OwnerDocument.CreateAttribute("CodigoUsuario");
                    Atributo.Value = Codigo_Usuario;
                    Raiz.Attributes.Append(Atributo);

                    Atributo = Raiz.OwnerDocument.CreateAttribute("CodigoUsuarioProveedor");
                    Atributo.Value = Codigo_Usuario_Proveedor;
                    Raiz.Attributes.Append(Atributo);

                    Atributo = Raiz.OwnerDocument.CreateAttribute("RFCEmisor");
                    Atributo.Value = RFC_Emisor;
                    Raiz.Attributes.Append(Atributo);

                    xmlFolios = Xml_Parametro.CreateNode(XmlNodeType.Element, "Folios", "");

                    xmlUUID = Xml_Parametro.CreateNode(XmlNodeType.Element, "UUID", "");
                    xmlUUID.InnerText = Codigo_UUID;

                    xmlFolios.AppendChild(xmlUUID);

                    Raiz.AppendChild(xmlFolios);

                    Parametros = Xml_Parametro.InnerXml;

                    // Ejecuta el web service con los parametros
                    if (Timbre_Produccion) // Si es de produccion
                    {
                        Timbrado.ReportarCancelacion(Parametros, out Respuesta);
                    }
                    else // Si es para prueba
                    {
                        Timbrado.ReportarCancelacionPrueba(Parametros, out Respuesta);
                    }
                }
                else
                {
                    Respuesta = "Error: ";
                    if (String.IsNullOrEmpty(RFC_Emisor))
                    {
                        Respuesta = (Respuesta.Length > 10 ? ", el RFC del emisor" : "Proporcione el RFC del emisor");
                    }
                    if (String.IsNullOrEmpty(Codigo_Usuario_Proveedor))
                    {
                        Respuesta = (Respuesta.Length > 10 ? ", el codigo de usuario proveedor" : "Proporcione el codigo de usuario proveedor");
                    }
                    if (String.IsNullOrEmpty(Codigo_Usuario))
                    {
                        Respuesta = (Respuesta.Length > 10 ? ", el codigo de usuario" : "Proporcione el codigo de usuario");
                    }
                    if (String.IsNullOrEmpty(Codigo_UUID))
                    {
                        Respuesta = (Respuesta.Length > 10 ? ", el codigo UUID" : "Proporcione el codigo UUID de timbrado");
                    }
                    Respuesta += " del Timbrado.";
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error Timbrado: " + Ex.Message);
            }

            return Respuesta;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Genera_Cadena_Original
        ///DESCRIPCIÓN: Genera la cadena original de la factura
        ///PARAMETROS:  
        ///             Rs_Factura, Datos de la factura
        ///             Expedida_En, Cadena con al informacion de donde se expedio la factura
        ///             Regimen_Fiscal, Regimen fiscal
        ///             Regimen_Fiscal_2, Regimen fiscal opcional
        ///             Dt_Empresa, Datos de la empresa
        ///             Dt_Factura_Detalles, Detalles de la factura
        ///             Dt_Impuestos, Informacion de los impuestos
        ///             Dt_Impuestos_Locales, Informacion de los impuestos locales
        ///CREO:        Luis Alberto Salas Garcia
        ///FECHA_CREO:  10/Junio/2013
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        ///
        ///ESTE ES EL METODO CHIDO
        ///
        public static String Genera_Cadena_Original(Cls_Ope_Facturacion_Negocio Rs_Factura, String Expedida_En, String Regimen_Fiscal, DataTable Dt_Empresa,
            DataTable Dt_Factura_Detalles, DataTable Dt_Impuestos, SqlConnection objConexion)
        {
            String Cadena_Original = String.Empty;
            Double Sumatoria_Iva_Recalculado = 0;
            //DateTime fecha_xml;
            //DateTime.TryParseExact(Rs_Factura.P_Fecha_Creo_Xml, "", null, System.Globalization.DateTimeStyles.None, out fecha_xml);
            try
            {
                // Datos Generales
                Cadena_Original += "||" + Rs_Factura.P_Timbre_Version;
                if (!String.IsNullOrEmpty(Rs_Factura.P_Serie))
                    Cadena_Original += "|" + Rs_Factura.P_Serie;
                Cadena_Original += "|" + Convert.ToInt32(Rs_Factura.P_No_Factura).ToString();

                Cadena_Original += "|" + String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(Rs_Factura.P_Fecha_Creo_Xml));
                Cadena_Original += "T" + String.Format("{0:HH:mm:ss}", Convert.ToDateTime(Rs_Factura.P_Fecha_Creo_Xml));

                Cadena_Original += "|" + Rs_Factura.P_Metodo_Pago;

                Cadena_Original += "|" + Rs_Factura.P_No_Certificado;
                Cadena_Original += "|" + Rs_Factura.P_Condiciones;

                //Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Rs_Factura.P_Subtotal));
                Cadena_Original += "|" + Rs_Factura.P_SubTotal_No_Descuentos;

                //if (Double.Parse((String.IsNullOrEmpty(Rs_Factura.P_Descuento) ? "0" : Rs_Factura.P_Descuento)) > 0)
                //{
                //    Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Rs_Factura.P_Descuento));
                //}
                if (Rs_Factura.P_Total_Descuento > 0)
                    Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Rs_Factura.P_Total_Descuento));

                if (Rs_Factura.P_Tipo_Moneda.Equals("DOLARES"))
                {
                    Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Rs_Factura.P_Tipo_Cambio));
                }
                Cadena_Original += "|" + Rs_Factura.P_Tipo_Moneda;
                Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Rs_Factura.P_Total));
                
                Cadena_Original += "|" + Rs_Factura.P_Tipo_Comprobante;

                if (!String.IsNullOrEmpty(Rs_Factura.P_Forma_Pago))
                {
                    Cadena_Original += "|" + Rs_Factura.P_Forma_Pago;
                }



                Cadena_Original += "|" + Expedida_En;
                if (!String.IsNullOrEmpty(Rs_Factura.P_No_Cuenta_Pago))
                {
                    Cadena_Original += "|" + Rs_Factura.P_No_Cuenta_Pago;
                }

                // Datos del Emisor
                Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["RFC"].ToString());
                //Cadena_Original += "|" + "AAA010101AAA";
                Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Razon_Social"].ToString());

                //Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Calle"].ToString());
                //if (!String.IsNullOrEmpty(Dt_Empresa.Rows[0]["Numero_Exterior"].ToString()))
                //{
                //    Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Numero_Exterior"].ToString());
                //}
                //if (!String.IsNullOrEmpty(Dt_Empresa.Rows[0]["Numero_Interior"].ToString()))
                //{
                //    Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Numero_Interior"].ToString());
                //}
                //if (!String.IsNullOrEmpty(Dt_Empresa.Rows[0]["Colonia"].ToString()))
                //{
                //    Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Colonia"].ToString());
                //}
                //Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Localidad"].ToString());
                //Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Ciudad"].ToString());
                //Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Estado"].ToString());
                //Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Pais"].ToString());
                //Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["CP"].ToString());

                Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Regimen_Fiscal);

                // Datos del receptor
                Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Factura.P_Rfc_Facturacion);
                Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Factura.P_Razon_Social_Facturacion);
                //Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Factura.P_Calle_Facturacion);
                //if (!String.IsNullOrEmpty(Rs_Factura.P_Numero_Exterior_Facturacion))
                //{
                //    Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Factura.P_Numero_Exterior_Facturacion);
                //}
                //if (!String.IsNullOrEmpty(Rs_Factura.P_Numero_Interior_Facturacion))
                //{
                //    Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Factura.P_Numero_Interior_Facturacion);
                //}
                //if (!String.IsNullOrEmpty(Rs_Factura.P_Colonia_Facturacion))
                //{
                //    Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Factura.P_Colonia_Facturacion);
                //}
                ////Cadena_Original += "|" + Rs_Factura.P_Localidad;
                //Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Factura.P_Ciudad_Facturacion);
                //Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Factura.P_Estado_Facturacion);
                //Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Factura.P_Pais_Facturacion);
                //Cadena_Original += "|" + Rs_Factura.P_Cp;
                Cadena_Original += "|" + Rs_Factura.UsoCFDI;

                bool seRedondeoUnIva = false;
                Sumatoria_Iva_Recalculado = 0;
                // Datos de los Conceptos
                foreach (DataRow Fila_Tabla  in Dt_Factura_Detalles.Rows)
                {
                    if (Math.Round( Convert.ToDouble(Fila_Tabla["Importe"]),2 )== 0)
                        continue;
                    //Cadena_Original += "|" + "01010101";
                    Cadena_Original += "|" + Fila_Tabla["concepto_sat"].ToString();
                    if (!String.IsNullOrEmpty(Fila_Tabla["Clave_Concepto"].ToString()))
                    {
                        Cadena_Original += "|" + Fila_Tabla["Clave_Concepto"].ToString().Trim();
                    }

                    Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Fila_Tabla["Cantidad"]));
                    //Cadena_Original += "|" + "E48";
                    Cadena_Original += "|" + Fila_Tabla["unidad_sat"].ToString();

                    if (!String.IsNullOrEmpty(Fila_Tabla["Unidad_de_Medida"].ToString()))
                    {
                        Cadena_Original += "|" + Fila_Tabla["Unidad_de_Medida"].ToString().Trim();
                    }

                    Cadena_Original += "|" + Fila_Tabla["Descripcion"].ToString().Trim();
                    Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Fila_Tabla["Importe"]));
                    Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Fila_Tabla["Importe"]));

                    if (Convert.ToDouble(Fila_Tabla["descuento"]) > 0)
                    {
                        Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Fila_Tabla["descuento"]));
                        //Cadena_Original += "|" + Fila_Tabla["descuento"];
                    }

                    //if (Convert.ToDouble(Fila_Tabla["iva"].ToString()) > 0)
                    if (Rs_Factura.Verificar_Importe_Iva(Fila_Tabla["Clave_Concepto"].ToString().Trim(), objConexion))
                    {
                        Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Fila_Tabla["Importe"]));
                        Cadena_Original += "|" + "002";
                        Cadena_Original += "|" + "Tasa";
                        double iva;
                        var importe = Convert.ToDouble(Fila_Tabla["ivaCorrecto"]);

                        var baseImporte = Math.Round(Convert.ToDouble(Fila_Tabla["importe"].ToString()), 4);
                        var tasa = Double.Parse(Fila_Tabla["tasa"].ToString());
                        //var tasa = Double.Parse(Dt_Impuestos.Rows[0]["TASA"].ToString()) / 100;

                        //var limiteInferior = (baseImporte - Math.Pow(10, -2) / 2) * tasa;
                        //var limiteTruncado = Math.Truncate(limiteInferior * 100) / 100;

                        //var limiteSuperior = (baseImporte - Math.Pow(10, -2) / 2 - Math.Pow(10, -12)) * tasa;
                        //var limiteSuperiorRedondeaco = Math.Round(limiteSuperior, 2);

                        if (!seRedondeoUnIva)
                        {
                            var iva1 = Math.Truncate(importe * 100) / 100;
                            var iva2 = Convert.ToDouble(String.Format("{0:#0.00}", importe));


                            if (iva1 != iva2)
                            {
                                seRedondeoUnIva = true;
                                importe = iva2;
                            } else
                                importe = Math.Truncate(importe * 100) / 100;
                        } else
                            importe = Math.Truncate(importe * 100) / 100;

                        //if (importe >= limiteTruncado && importe <= limiteSuperiorRedondeaco)
                        //    iva = importe;
                        //else if (Math.Round(Math.Abs(importe - limiteTruncado), 2) == 0.01 || Math.Round(Math.Abs(importe - limiteSuperiorRedondeaco), 2) == 0.01)
                        //    iva = importe;
                        //else
                        //{
                        //    //iva = Math.Truncate(limiteInferior * 100) / 100;
                        //    //seRedondeoUnIva = false;
                        //    if (!seRedondeoUnIva & limiteTruncado != limiteSuperiorRedondeaco)
                        //    {
                        //        iva = limiteSuperiorRedondeaco;
                        //        seRedondeoUnIva = true;
                        //    }
                        //    else
                        //        iva = limiteTruncado;
                        //}

                        Cadena_Original += "|" + String.Format("{0:#0.000000}", tasa);

                        //if (Dt_Factura_Detalles.Rows.IndexOf(Fila_Tabla) != 0)
                        //{
                        //    Cadena_Original += "|" + String.Format("{0:#0.00}", String.Format("{0:#0.00}", iva));
                        //} else
                        //{
                        //    var ivaTmp = Math.Truncate(iva * 100) / 100;
                        //    Cadena_Original += "|" + String.Format("{0:#0.00}", String.Format("{0:#0.00}", ivaTmp));
                        //}
                        iva = Math.Round(baseImporte,2) * tasa;
                        iva = Math.Round(iva, 4);
                        Cadena_Original += "|" +  String.Format("{0:#0.0000}", Math.Round(iva, 4));
                        Sumatoria_Iva_Recalculado += iva;

                    }

                    // Si tiene predial
                    //if (!String.IsNullOrEmpty(Fila_Tabla["Cuenta_Predial"].ToString()))
                    //{
                    //    Cadena_Original += "|" + Fila_Tabla["Cuenta_Predial"].ToString();
                    //}
                }


                // Datos Impuestos
                if (Dt_Impuestos.Rows.Count > 0 && Double.Parse(Dt_Impuestos.Rows[0]["IMPORTE"].ToString()) > 0)
                {
                    foreach (DataRow Fila_Tabla in Dt_Impuestos.Rows)
                    {
                        Cadena_Original += "|" + "002";
                        Cadena_Original += "|" + "Tasa";
                        //Cadena_Original += "|" + Fila_Tabla["IMPUESTO"].ToString();
                        Cadena_Original += "|" + String.Format("{0:#0.000000}", Double.Parse(Fila_Tabla["TASA"].ToString()) );
                        Cadena_Original += "|" + String.Format("{0:#0.00}", Sumatoria_Iva_Recalculado);
                        Cadena_Original += "|" + String.Format("{0:#0.00}", Sumatoria_Iva_Recalculado);
                    }
                }

                Cadena_Original += "||";

                // Elimina dobles espacios
                Cadena_Original = Regex.Replace(Cadena_Original, @"\s+", " ");
            }
            catch (Exception Ex)
            {
                throw new Exception("Genera_Cadena_Original Error: " + Ex.Message);
            }

            return Cadena_Original;
        }

        public static String Genera_Cadena_Original_Original(Cls_Ope_Facturacion_Negocio Rs_Factura, String Expedida_En, String Regimen_Fiscal, DataTable Dt_Empresa,
    DataTable Dt_Factura_Detalles, DataTable Dt_Impuestos)
        {
            String Cadena_Original = String.Empty;
            //DateTime fecha_xml;
            //DateTime.TryParseExact(Rs_Factura.P_Fecha_Creo_Xml, "", null, System.Globalization.DateTimeStyles.None, out fecha_xml);
            try
            {
                // Datos Generales
                Cadena_Original += "||" + Rs_Factura.P_Timbre_Version;
                Cadena_Original += "|" + String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(Rs_Factura.P_Fecha_Creo_Xml));
                Cadena_Original += "T" + String.Format("{0:HH:mm:ss}", Convert.ToDateTime(Rs_Factura.P_Fecha_Creo_Xml));
                Cadena_Original += "|" + Rs_Factura.P_Tipo_Comprobante.ToLower();
                if (!String.IsNullOrEmpty(Rs_Factura.P_Forma_Pago))
                {
                    Cadena_Original += "|" + Rs_Factura.P_Forma_Pago;
                }
                //if (!String.IsNullOrEmpty(Rs_Factura.P_Condiciones))
                //{
                Cadena_Original += "|" + Rs_Factura.P_Condiciones;
                //}

                Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Rs_Factura.P_Subtotal));
                if (Double.Parse((String.IsNullOrEmpty(Rs_Factura.P_Descuento) ? "0" : Rs_Factura.P_Descuento)) > 0)
                {
                    Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Rs_Factura.P_Descuento));
                }
                if (Rs_Factura.P_Tipo_Moneda.Equals("DOLARES"))
                {
                    Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Rs_Factura.P_Tipo_Cambio));
                }
                Cadena_Original += "|" + Rs_Factura.P_Tipo_Moneda;
                Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Rs_Factura.P_Total));
                Cadena_Original += "|" + Rs_Factura.P_Metodo_Pago;
                Cadena_Original += "|" + Expedida_En;
                if (!String.IsNullOrEmpty(Rs_Factura.P_No_Cuenta_Pago))
                {
                    Cadena_Original += "|" + Rs_Factura.P_No_Cuenta_Pago;
                }

                // Datos del Emisor
                Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["RFC"].ToString());
                //Cadena_Original += "|" + "AAA010101AAA";
                Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Razon_Social"].ToString());
                Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Calle"].ToString());
                if (!String.IsNullOrEmpty(Dt_Empresa.Rows[0]["Numero_Exterior"].ToString()))
                {
                    Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Numero_Exterior"].ToString());
                }
                if (!String.IsNullOrEmpty(Dt_Empresa.Rows[0]["Numero_Interior"].ToString()))
                {
                    Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Numero_Interior"].ToString());
                }
                if (!String.IsNullOrEmpty(Dt_Empresa.Rows[0]["Colonia"].ToString()))
                {
                    Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Colonia"].ToString());
                }
                Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Localidad"].ToString());
                Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Ciudad"].ToString());
                Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Estado"].ToString());
                Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Pais"].ToString());
                Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["CP"].ToString());

                Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Regimen_Fiscal);

                // Datos del receptor
                Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Factura.P_Rfc_Facturacion);
                Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Factura.P_Razon_Social_Facturacion);
                Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Factura.P_Calle_Facturacion);
                if (!String.IsNullOrEmpty(Rs_Factura.P_Numero_Exterior_Facturacion))
                {
                    Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Factura.P_Numero_Exterior_Facturacion);
                }
                if (!String.IsNullOrEmpty(Rs_Factura.P_Numero_Interior_Facturacion))
                {
                    Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Factura.P_Numero_Interior_Facturacion);
                }
                if (!String.IsNullOrEmpty(Rs_Factura.P_Colonia_Facturacion))
                {
                    Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Factura.P_Colonia_Facturacion);
                }
                //Cadena_Original += "|" + Rs_Factura.P_Localidad;
                Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Factura.P_Ciudad_Facturacion);
                Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Factura.P_Estado_Facturacion);
                Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Factura.P_Pais_Facturacion);
                Cadena_Original += "|" + Rs_Factura.P_Cp;

                // Datos de los Conceptos
                foreach (DataRow Fila_Tabla in Dt_Factura_Detalles.Rows)
                {
                    Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Fila_Tabla["Valor_Unitario"]));
                    if (!String.IsNullOrEmpty(Fila_Tabla["Unidad_de_Medida"].ToString()))
                    {
                        Cadena_Original += "|" + Fila_Tabla["Unidad_de_Medida"].ToString().Trim();
                    }
                    if (!String.IsNullOrEmpty(Fila_Tabla["Clave_Concepto"].ToString()))
                    {
                        Cadena_Original += "|" + Fila_Tabla["Clave_Concepto"].ToString().Trim();
                    }
                    Cadena_Original += "|" + Fila_Tabla["Descripcion"].ToString().Trim();
                    Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Fila_Tabla["Importe"]));
                    Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Fila_Tabla["Total"]));

                    // Si tiene predial
                    //if (!String.IsNullOrEmpty(Fila_Tabla["Cuenta_Predial"].ToString()))
                    //{
                    //    Cadena_Original += "|" + Fila_Tabla["Cuenta_Predial"].ToString();
                    //}
                }

                // Datos Impuestos
                if (Dt_Impuestos.Rows.Count > 0)
                {
                    foreach (DataRow Fila_Tabla in Dt_Impuestos.Rows)
                    {
                        Cadena_Original += "|" + Fila_Tabla["IMPUESTO"].ToString();
                        Cadena_Original += "|" + String.Format("{0:#0.00}", Double.Parse(Fila_Tabla["TASA"].ToString()));
                        Cadena_Original += "|" + String.Format("{0:#0.00}", Double.Parse(Fila_Tabla["IMPORTE"].ToString()));
                        Cadena_Original += "|" + String.Format("{0:#0.00}", Double.Parse(Fila_Tabla["IMPORTE"].ToString()));
                    }
                }

                Cadena_Original += "||";

                // Elimina dobles espacios
                Cadena_Original = Regex.Replace(Cadena_Original, @"\s+", " ");
            }
            catch (Exception Ex)
            {
                throw new Exception("Genera_Cadena_Original Error: " + Ex.Message);
            }

            return Cadena_Original;
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Genera_Cadena_Original
        ///DESCRIPCIÓN: Genera la cadena original de la factura
        ///PARAMETROS:  
        ///             Rs_Factura, Datos de la factura
        ///             Expedida_En, Cadena con al informacion de donde se expedio la factura
        ///             Regimen_Fiscal, Regimen fiscal
        ///             Regimen_Fiscal_2, Regimen fiscal opcional
        ///             Dt_Empresa, Datos de la empresa
        ///             Dt_Factura_Detalles, Detalles de la factura
        ///             Dt_Impuestos, Informacion de los impuestos
        ///             Dt_Impuestos_Locales, Informacion de los impuestos locales
        ///CREO:        Luis Alberto Salas Garcia
        ///FECHA_CREO:  10/Junio/2013
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        //public static String Genera_Cadena_Original(Cls_Ope_Notas_Credito_Negocio Rs_Nota_Credito, String Expedida_En, String Regimen_Fiscal, DataTable Dt_Empresa,
        //    DataTable Dt_Notas_Credito_Detalles, DataTable Dt_Impuestos)
        //{
        //    String Cadena_Original = String.Empty;
        //    try
        //    {
        //        //DateTime Fecha = DateTime.ParseExact(Rs_Nota_Credito.P_Fecha_Emision, "dd/MM/yyyy HH:mm:ss", null);
        //        // Datos Generales
        //        Cadena_Original += "||" + Rs_Nota_Credito.P_Timbre_Version;
        //        Cadena_Original += "|" + String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(Rs_Nota_Credito.P_Fecha_Xml));
        //        Cadena_Original += "T" + String.Format("{0:HH:mm:ss}", Convert.ToDateTime(Rs_Nota_Credito.P_Fecha_Xml));
        //        Cadena_Original += "|" + Rs_Nota_Credito.P_Tipo_Comprobante.ToLower();
        //        Cadena_Original += "|PAGO EN UNA SOLA EXHIBICION";
        //        if (!String.IsNullOrEmpty(Rs_Nota_Credito.P_Forma_Pago))
        //        {
        //            Cadena_Original += "|" + Rs_Nota_Credito.P_Forma_Pago;
        //        }
        //        Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Rs_Nota_Credito.P_Subtotal));
        //        if (Rs_Nota_Credito.P_Tipo_Moneda.Equals("DOLARES"))
        //        {
        //            Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Rs_Nota_Credito.P_Tipo_Cambio));
        //        }
        //        Cadena_Original += "|" + Rs_Nota_Credito.P_Tipo_Moneda;
        //        Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Rs_Nota_Credito.P_Total));
        //        Cadena_Original += "|" + Rs_Nota_Credito.P_Metodo_Pago;
        //        Cadena_Original += "|" + Expedida_En;

        //        // Datos del Emisor
        //        Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["RFC"].ToString());
        //        //Cadena_Original += "|" + "AAA010101AAA";
        //        Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Razon_Social"].ToString());
        //        Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Calle"].ToString());
        //        if (!String.IsNullOrEmpty(Dt_Empresa.Rows[0]["Numero_Exterior"].ToString()))
        //        {
        //            Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Numero_Exterior"].ToString());
        //        }
        //        if (!String.IsNullOrEmpty(Dt_Empresa.Rows[0]["Numero_Interior"].ToString()))
        //        {
        //            Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Numero_Interior"].ToString());
        //        }
        //        if (!String.IsNullOrEmpty(Dt_Empresa.Rows[0]["Colonia"].ToString()))
        //        {
        //            Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Colonia"].ToString());
        //        }
        //        Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Localidad"].ToString());
        //        Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Ciudad"].ToString());
        //        Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Estado"].ToString());
        //        Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Pais"].ToString());
        //        Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["CP"].ToString());

        //        Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Regimen_Fiscal);

        //        // Datos del receptor
        //        Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Rfc_Facturacion);
        //        Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Razon_Social);
        //        Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Calle);
        //        if (!String.IsNullOrEmpty(Rs_Nota_Credito.P_Numero_Exterior))
        //        {
        //            Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Numero_Exterior);
        //        }
        //        if (!String.IsNullOrEmpty(Rs_Nota_Credito.P_Numero_Interior))
        //        {
        //            Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Numero_Interior);
        //        }
        //        if (!String.IsNullOrEmpty(Rs_Nota_Credito.P_Colonia))
        //        {
        //            Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Colonia);
        //        }
        //        //Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Localidad);
        //        Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Ciudad);
        //        Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Estado);
        //        Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Pais);
        //        Cadena_Original += "|" + Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Cp);

        //        // Datos de los Conceptos
        //        foreach (DataRow Fila_Tabla in Dt_Notas_Credito_Detalles.Rows)
        //        {
        //            Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Fila_Tabla["Cantidad"]));
        //            if (!String.IsNullOrEmpty(Fila_Tabla["Unidad"].ToString()))
        //            {
        //                Cadena_Original += "|" + Fila_Tabla["Unidad"].ToString();
        //            }
        //            if (!String.IsNullOrEmpty(Fila_Tabla["Codigo"].ToString()))
        //            {
        //                Cadena_Original += "|" + Fila_Tabla["Codigo"].ToString();
        //            }
        //            Cadena_Original += "|" + Fila_Tabla["Descripcion"].ToString();
        //            Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Fila_Tabla["Precio_Unitario"]));
        //            Cadena_Original += "|" + String.Format("{0:#0.00}", Convert.ToDouble(Fila_Tabla["Total_Detalle"]));

        //            // Si tiene predial
        //            if (!String.IsNullOrEmpty(Fila_Tabla["Cuenta_Predial"].ToString()))
        //            {
        //                Cadena_Original += "|" + Fila_Tabla["Cuenta_Predial"].ToString();
        //            }
        //        }

        //        // Datos Impuestos
        //        if (Dt_Impuestos.Rows.Count > 0)
        //        {
        //            foreach (DataRow Fila_Tabla in Dt_Impuestos.Rows)
        //            {
        //                Cadena_Original += "|" + Fila_Tabla["IMPUESTO"].ToString();
        //                Cadena_Original += "|" + String.Format("{0:#0.00}", Double.Parse(Fila_Tabla["TASA"].ToString()));
        //                Cadena_Original += "|" + String.Format("{0:#0.00}", Double.Parse(Fila_Tabla["IMPORTE"].ToString()));
        //                Cadena_Original += "|" + String.Format("{0:#0.00}", Double.Parse(Fila_Tabla["IMPORTE"].ToString()));
        //            }
        //        }

        //        Cadena_Original += "||";
        //        // Elimina dobles espacios
        //        Cadena_Original = Regex.Replace(Cadena_Original, @"\s+", " ");
        //    }
        //    catch (Exception Ex)
        //    {
        //        throw new Exception("Genera_Cadena_Original Error: " + Ex.Message);
        //    }

        //    return Cadena_Original;
        //}
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Generar_Codigo_QR
        ///DESCRIPCIÓN: Crea y guarda el codigo bidimensional
        ///PARAMETROS:  
        ///             Codigo_Bidimensional, Cadena con la informacion general de la
        ///                         factura, que se reflejara en el codigo QR.
        ///             Ruta, Ruta donde se guardara la imagen
        ///CREO:        Luis Alberto Salas Garcia
        ///FECHA_CREO:  10/Junio/2013
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Generar_Codigo_QR(String Codigo_Bidimensional, String Ruta, String Nombre_Codigo_Bidimensional, String Ruta_Externa_Xml)
        {
            try
            {
                QrEncoder Qr_Encoder = new QrEncoder(ErrorCorrectionLevel.H);
                QrCode Qr_Code = new QrCode();
                MemoryStream Ms = new MemoryStream();
                Qr_Encoder.TryEncode(Codigo_Bidimensional, out Qr_Code);

                GraphicsRenderer G_Render = new GraphicsRenderer(new FixedModuleSize(2, QuietZoneModules.Two), Brushes.Black, Brushes.White);
                G_Render.WriteToStream(Qr_Code.Matrix, ImageFormat.Png, Ms);

                Bitmap ImageTemp = new Bitmap(Ms);
                Bitmap Image = new Bitmap(ImageTemp, new Size(new Point(200, 200)));

                Image.Save(Ruta, ImageFormat.Png);

                if (!String.IsNullOrEmpty(Ruta_Externa_Xml))
                {
                    //Guarda una copia del codigo bidimensional en el proyecto de factura a usuarios
                    Image.Save((Ruta_Externa_Xml + Nombre_Codigo_Bidimensional), ImageFormat.Png);
                    //Image.Save(("C:/Users/Public/Documents/SIAC_VERSION/Codigo/SIISR3/Facturacion/Facturacion/XML/" + Nombre_Codigo_Bidimensional), ImageFormat.Png);
                }

                //Limpia las variables
                Qr_Encoder = null;
                Qr_Code = null;
                Ms = null;
                G_Render = null;
                ImageTemp = null;
                Image = null;
            }
            catch (Exception Ex)
            {
                throw new Exception("Generar_Codigo_QR Error: " + Ex.Message);
            }
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Valida_Caracteres_UTF
        ///DESCRIPCIÓN: Convierte la cadena original a UTF8
        ///PARAMETROS:  
        ///             Cadena_Original, Cadena con la informacion general de la factura
        ///CREO:        Luis Alberto Salas Garcia
        ///FECHA_CREO:  10/Junio/2013
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static String Valida_Caracteres_UTF(String Cadena_Original)
        {
            String Cadena_Copia = String.Empty;
            String Cadena_UTF = String.Empty;
            byte[] Bytes;
            long Tamanio;

            try
            {
                Cadena_Copia = Cadena_Original;

                Tamanio = Cnv.CheckUTF8(Cadena_Copia);
                if (Tamanio < 0)
                {
                    throw new Exception("Valida_Caracteres_UTF Error: No se puede convertir la cadena a UTF8.");
                }
                else
                {
                    Bytes = System.Text.Encoding.UTF8.GetBytes(Cadena_Copia);
                    Cadena_UTF = System.Text.Encoding.UTF8.GetString(Bytes);
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Valida_Caracteres_UTF Error: " + Ex.Message);
            }

            return Cadena_UTF;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Genera_Sello
        ///DESCRIPCIÓN: Genera el sello de la factura.
        ///PARAMETROS:  
        ///             Cadena_UTF8, Cadena orginal en formato UTF8
        ///             Archivo_Llave, Archivo que contiene la llave
        ///             Password, Contraseña del archivo llave
        ///             Anio, Año de expedicion de la factura
        ///CREO:        Luis Alberto Salas Garcia
        ///FECHA_CREO:  10/Junio/2013
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static String Genera_Sello(String Cadena_UTF8, String Archivo_Llave, String Password)
        {
            String Sello = String.Empty;
            StringBuilder Llave_Privada;
            //Encoding Codificador_Base64= new 64
            byte[] Mensaje;
            byte[] Respuesta;
            int nBlock;

            try
            {
                Llave_Privada = Rsa.ReadEncPrivateKey(Archivo_Llave, Password);

                if (Llave_Privada.Length == 0)
                {
                    throw new Exception("Genera_Sello Error: No se puede leer la llave privada.");
                }
                else
                {
                    if (Cnv.CheckUTF8(Cadena_UTF8) >= 0)
                    {
                        Mensaje = System.Text.Encoding.UTF8.GetBytes(Cadena_UTF8);
                        nBlock = Rsa.KeyBytes(Llave_Privada.ToString().ToString());
                        //nBlock = 64;
                        //Respuesta = Rsa.EncodeMsgForSignature(nBlock, Mensaje, HashAlgorithm.Sha1);
                        Respuesta = Rsa.EncodeMsgForSignature(nBlock, Mensaje, HashAlgorithm.Sha256);

                        Respuesta = Rsa.RawPrivate(Respuesta, Llave_Privada.ToString().ToString());
                        Sello = Convert.ToBase64String(Respuesta);
                        Wipe.String(Llave_Privada);
                    }
                    else
                    {
                        throw new Exception("Genera_Sello Error: La Cadena UTF no es v&aacute;lida.");
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Genera_Sello Error: " + Ex.Message);
            }

            return Sello;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Consulta_Serie_Certificado
        ///DESCRIPCIÓN: Consulta la serie del certificado.
        ///PARAMETROS:  
        ///             Ruta_Certificado, Cadena con la ruta del certificado
        ///CREO:        Luis Alberto Salas Garcia
        ///FECHA_CREO:  10/Junio/2013
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static String Consulta_Serie_Certificado(String Ruta_Certificado)
        {
            String Serie = String.Empty;
            String Serie_Sat = String.Empty;

            try
            {
                // Obtiene el numero de serie
                Serie = X509.CertSerialNumber(Ruta_Certificado);

                // Convierte la serie original de hex a cadena
                Serie_Sat = System.Text.Encoding.Default.GetString(Cnv.FromHex(Serie));
            }
            catch (Exception Ex)
            {
                throw new Exception("Consulta_Serie_Certificado Error: " + Ex.Message);
            }

            return Serie_Sat;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Consulta_Certificado
        ///DESCRIPCIÓN: Consulta el certificado.
        ///PARAMETROS:  
        ///             Ruta_Certificado, Cadena con la ruta del certificado
        ///CREO:        Luis Alberto Salas Garcia
        ///FECHA_CREO:  10/Junio/2013
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static String Consulta_Certificado(String Ruta_Certificado)
        {
            String Certificado = String.Empty;
            try
            {
                Certificado = X509.ReadStringFromFile(Ruta_Certificado);
            }
            catch (Exception Ex)
            {
                throw new Exception("Consulta_Certificado Error: " + Ex.Message);
            }

            return Certificado;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Obtener_Fecha_Inicio_Certificado
        ///DESCRIPCIÓN: Obtiene la fecha de inicio del certificado
        ///PARAMETROS:  
        ///             Ruta, Cadena con la ruta del certificado
        ///CREO:        Luis Alberto Salas Garcia
        ///FECHA_CREO:  10/Junio/2013
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static String Obtener_Fecha_Inicio_Certificado(String Ruta)
        {
            String Fecha = String.Empty;

            try
            {
                Fecha = X509.CertIssuedOn(Ruta);
            }
            catch (Exception Ex)
            {
                throw new Exception("Obtener_Fecha_Inicio_Certificado Error: " + Ex.Message);
            }

            return Fecha;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Obtener_Fecha_Fin_Certificado
        ///DESCRIPCIÓN: Obtiene la fecha de fin del certificado
        ///PARAMETROS:  
        ///             Ruta, Cadena con la ruta del certificado
        ///CREO:        Luis Alberto Salas Garcia
        ///FECHA_CREO:  10/Junio/2013
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static String Obtener_Fecha_Fin_Certificado(String Ruta)
        {
            String Fecha = String.Empty;

            try
            {
                Fecha = X509.CertExpiresOn(Ruta);
            }
            catch (Exception Ex)
            {
                throw new Exception("Obtener_Fecha_Fin_Certificado Error: " + Ex.Message);
            }

            return Fecha;
        }
    }
}