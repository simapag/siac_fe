﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;
using System.Web;
using System.Web.UI;
using System.Drawing;
using System.Configuration;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using SharpContent.ApplicationBlocks.Data;
using System.Collections.Generic;
using CrystalDecisions.Shared;
using SIAC.Constantes;
using Erp.Numeros_Letras;
//using SIAC.Timbrado;
using System.Data.SqlClient;
using SIAC.Timbrado;
using System.Reflection;

namespace SIAC.Metodos_Generales
{
    public class Cls_Metodos_Generales
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
        ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : Antonio Salvador Benavides Guardado
        ///FECHA_MODIFICO       : 26/Octubre/2010
        ///CAUSA_MODIFICACIÓN   : Estandarizar variables usadas
        ///*******************************************************************************
        public static String Obtener_ID_Consecutivo(String Tabla, String Campo, String Filtro, Int32 Longitud_ID)
        {

            //Declaración de las variables
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            String Mi_SQL;

            String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;

            try
            {
                Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;

                if (Filtro != "" && Filtro != null)
                {
                    Mi_SQL += " WHERE " + Filtro;
                }

                IDbCommand com = Obj_Conexion.CreateCommand();
                com.CommandType = CommandType.Text;
                com.Transaction = Obj_Transaccion;
                com.CommandText = Mi_SQL;

                Object Obj_Temp = com.ExecuteScalar();

                if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                {
                    Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                }

                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            { 
                throw new Exception("Error: " + Ex.Message); 
            }
            catch (DBConcurrencyException Ex) 
            { 
                throw new Exception("Error: " + Ex.Message); 
            }
            catch (Exception Ex) 
            { 
                throw new Exception("Error: " + Ex.Message); 
            }
            return Id;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
        ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : Antonio Salvador Benavides Guardado
        ///FECHA_MODIFICO       : 26/Octubre/2010
        ///CAUSA_MODIFICACIÓN   : Estandarizar variables usadas
        ///*******************************************************************************
        public static String Obtener_ID_Consecutivo_Tabla()
        {

            //Declaración de las variables
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Conexion.Open();
            SqlTransaction Obj_Transaccion = Obj_Conexion.BeginTransaction();
            SqlCommand Obj_Comando = new SqlCommand();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            String Mi_SQL;

            String Id = Convertir_A_Formato_ID(1, 10);

            try
            {
                Mi_SQL = "INSERT INTO OPE_FACTURAS_CONSECUTIVOS DEFAULT VALUES; SELECT MAX(ID)FROM OPE_FACTURAS_CONSECUTIVOS;";

                IDbCommand com = Obj_Conexion.CreateCommand();
                com.CommandType = CommandType.Text;
                com.Transaction = Obj_Transaccion;
                com.CommandText = Mi_SQL;

                Object Obj_Temp = com.ExecuteScalar();

                if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                {
                    Id = Convertir_A_Formato_ID(Convert.ToInt32(Obj_Temp), 10);
                }

                //Obj_Comando.CommandText = Mi_SQL;
                //Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            return Id;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
        ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
        ///PARAMETROS:     
        ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
        ///             2. Longitud_ID. Longitud que tendra el ID. 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : Antonio Salvador Benavides Guardado
        ///FECHA_MODIFICO       : 26/Octubre/2010
        ///CAUSA_MODIFICACIÓN   : Estandarizar variables usadas
        ///*******************************************************************************
        public static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID)
        {
            String Retornar = "";
            String Dato = "" + Dato_ID;
            for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++)
            {
                Retornar = Retornar + "0";
            }
            Retornar = Retornar + Dato;
            return Retornar;
        }

        /// *************************************************************************************
        /// NOMBRE:             Generar_Reporte
        /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
        ///              
        /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
        ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
        ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
        ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
        /// USUARIO CREO:       Juan Alberto Hernández Negrete.
        /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
        /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
        /// FECHA MODIFICO:     16/Mayo/2011
        /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
        ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
        /// *************************************************************************************
        public static void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Nombre_PDF, String Ruta_Reporte, String Nombre_Reporte, String Ruta_Archivo, String Titulo_Ventana, Page Server, Type Tipo, String Ruta_Externa_Pdf)
        {
            ReportDocument Reporte = new ReportDocument();
            String Ruta = String.Empty;

            try
            {
                Ruta = Server.MapPath(Ruta_Reporte + Nombre_Reporte);
                Reporte.Load(Ruta);

                if (Ds_Reporte_Crystal is DataSet)
                {
                    if (Ds_Reporte_Crystal.Tables.Count > 0)
                    {
                        Reporte.SetDataSource(Ds_Reporte_Crystal);
                        Exportar_Reporte_PDF(Reporte, Nombre_PDF, Ruta_Archivo, Server, Ruta_Externa_Pdf);
                        //Mostrar_Archivo(Ruta_Archivo, Titulo_Ventana, Server, Tipo);
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Generar_Reporte Error: [" + Ex.Message + "]");
            }
            finally
            {
                if(Reporte != null)
                {
                    Reporte.Close();
                    Reporte.Dispose();
                    Reporte = null;
                }
            }
        }
        
        //*******************************************************************************
        //NOMBRE DE LA FUNCION:    Generar_Reporte
        //DESCRIPCION:             funcion que exporta el reporte de crystal reports a PDF
        //PARAMETROS:              Ds_Reporte: Ds_Ope_Cor_Validacion_Lecuras_1_5 contiene las tablas de los reportes,
        //                         Nombre_Reporte: string que contiene el nombre del reporte
        //CREO:                    Fernando Gonzalez B.
        //FECHA_CREO:              04/Septiembre/2012 17:45 
        //MODIFICO:                
        //FECHA_MODIFICO:          
        //CAUSA_MODIFICACION:      
        //*******************************************************************************
        public static void Generar_Reporte(DataSet Ds_Reporte, String Nombre_Reporte,HttpResponse Response)
        {
            try
            {
                ReportDocument Obj_Reporte = new ReportDocument();
                DiskFileDestinationOptions diskFileDestinationOptions = new DiskFileDestinationOptions();
                string Ruta_Reporte = HttpContext.Current.Server.MapPath("~");
                //string Nombre_Archivo = "";
                //ExportOptions Export_Options = null;
                //ExportFormatOptions Formato_Exportacion = null;

                if (Ruta_Reporte.EndsWith("\\"))
                {
                    Ruta_Reporte = Ruta_Reporte.Remove(Ruta_Reporte.LastIndexOf("\\"));
                }

                Ruta_Reporte += "\\Paginas\\Rpt\\Facturacion\\" + Nombre_Reporte + ".rpt";
                Obj_Reporte.Load(Ruta_Reporte);
                Obj_Reporte.SetDataSource(Ds_Reporte);
                Obj_Reporte.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, Nombre_Reporte);
                //Export_Options = Obj_Reporte.ExportOptions;
                //{
                //    Formato_Exportacion = new PdfRtfWordFormatOptions();
                //    Nombre_Archivo = Nombre_Reporte + "_.pdf";
                //    //diskFileDestinationOptions.DiskFileName = @Server.MapPath("..\\..\\..\\Reportes\\" + Nombre_Archivo);
                //    //Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
                //    Obj_Reporte.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "Validacion_Lecturas");
                //    //Export_Options.ExportFormatOptions = Formato_Exportacion;
                //    //Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
                //    //Export_Options.ExportDestinationOptions = diskFileDestinationOptions;
                //    //Obj_Reporte.Export(Export_Options);
                //}
                //ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + "../../../Reportes/" + Nombre_Archivo + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            catch (Exception Ex)
            {
                //Div_Mostrar_Mensaje_Error.Visible = true;
                //Img_Error.Visible = true;
                //Lbl_Mensaje_Error.Visible = true;
                //Lbl_Mensaje_Error.Text = "Error: " + Ex.Message;
            }
        }
                
        /// *************************************************************************************
        /// NOMBRE:             Generar_Reporte
        /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
        ///              
        /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
        ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
        ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
        ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
        ///                     Parametros, arreglo de n x 2, donde la primera posicion es el nombre del parametro
        ///                             y en la segunda posicion se guarda el valor de ese parametro.
        ///                             ejemplo. new String[1, 2] {{"NOMBRE_PARAMETRO","VALOR"}}
        /// USUARIO CREO:       Juan Alberto Hernández Negrete.
        /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
        /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
        /// FECHA MODIFICO:     16/Mayo/2011
        /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
        ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
        /// *************************************************************************************
        public static void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte, String Nombre_Reporte, String[,] Parametros, String Ruta_Archivo, String Titulo_Ventana, Page Server, Type Tipo)
        {
            ReportDocument Reporte = new ReportDocument();
            String Ruta = String.Empty;

            try
            {
                Ruta = Server.MapPath(Ruta_Reporte + Nombre_Reporte);
                Reporte.Load(Ruta);

                if (Ds_Reporte_Crystal is DataSet)
                {
                    if (Ds_Reporte_Crystal.Tables.Count > 0)
                    {
                        Reporte.SetDataSource(Ds_Reporte_Crystal);
                        if (Parametros != null)
                        {
                            for (int i = 0; i < Parametros.GetLength(0); i++)
                            {
                                Reporte.SetParameterValue(Parametros[i, 0], Parametros[i, 1]);
                            }
                        }
                        Exportar_Reporte_PDF(Reporte, "", Ruta_Archivo, Server, "");
                        Mostrar_Archivo(Ruta_Archivo, Titulo_Ventana, Server, Tipo);
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Generar_Reporte Error: [" + Ex.Message + "]");
            }
        }
        
        /// *************************************************************************************
        /// NOMBRE:             Generar_Reporte
        /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
        ///              
        /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
        ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
        ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
        ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
        ///                     Parametros, arreglo de n x 2, donde la primera posicion es el nombre del parametro
        ///                             y en la segunda posicion se guarda el valor de ese parametro.
        ///                             ejemplo. new String[1, 2] {{"NOMBRE_PARAMETRO","VALOR"}}
        /// USUARIO CREO:       Juan Alberto Hernández Negrete.
        /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
        /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
        /// FECHA MODIFICO:     16/Mayo/2011
        /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
        ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
        /// *************************************************************************************
        public static void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, ref DataSet Ds_Empresa, String Ruta_Reporte, String Nombre_Reporte, String[,] Parametros, String Ruta_Archivo, String Titulo_Ventana, Page Server, Type Tipo)
        {
            ReportDocument Reporte = new ReportDocument();
            String Ruta = String.Empty;

            try
            {
                Ruta = Server.MapPath(Ruta_Reporte + Nombre_Reporte);
                Reporte.Load(Ruta);

                if (Ds_Reporte_Crystal is DataSet)
                {
                    if (Ds_Reporte_Crystal.Tables.Count > 0)
                    {
                        Reporte.SetDataSource(Ds_Reporte_Crystal);
                        Reporte.Subreports[0].DataSourceConnections.Clear();
                        Reporte.Subreports[0].SetDataSource(Ds_Empresa.Tables[0]);

                        if (Parametros != null)
                        {
                            for (int i = 0; i < Parametros.GetLength(0); i++)
                            {
                                Reporte.SetParameterValue(Parametros[i, 0], Parametros[i, 1]);
                            }
                        }

                        Exportar_Reporte_PDF(Reporte, "", Ruta_Archivo, Server, "");
                        Mostrar_Archivo(Ruta_Archivo, Titulo_Ventana, Server, Tipo);
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Generar_Reporte Error: [" + Ex.Message + "]");
            }
        }
        
        /// *************************************************************************************
        /// NOMBRE:             Generar_Reporte
        /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
        ///              
        /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
        ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
        ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
        ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
        /// USUARIO CREO:       Juan Alberto Hernández Negrete.
        /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
        /// USUARIO MODIFICO:   Armando Zavala Moreno
        /// FECHA MODIFICO:     05/Julio/2013 01:00 p.m
        /// CAUSA MODIFICACIÓN: Se agrego el dataset de la empresa para llenar el encabezado del reporte
        /// *************************************************************************************
        public static void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, ref DataSet Ds_Empresa, String Ruta_Reporte, String Nombre_Reporte, String Ruta_Archivo, String Titulo_Ventana, Page Server, Type Tipo)
        {
            ReportDocument Reporte = new ReportDocument();
            String Ruta = String.Empty;

            try
            {
                Ruta = Server.MapPath(Ruta_Reporte + Nombre_Reporte);
                Reporte.Load(Ruta);

                if (Ds_Reporte_Crystal is DataSet)
                {
                    if (Ds_Reporte_Crystal.Tables.Count > 0)
                    {
                        Reporte.SetDataSource(Ds_Reporte_Crystal);
                        Reporte.Subreports[0].DataSourceConnections.Clear();
                        Reporte.Subreports[0].SetDataSource(Ds_Empresa.Tables[0]);
                        Exportar_Reporte_PDF(Reporte, "", Ruta_Archivo, Server, "");
                        //Mostrar_Archivo(Ruta_Archivo, Titulo_Ventana, Server, Tipo);
                    }
                }
                Reporte = null;
            }
            catch (Exception Ex)
            {
                throw new Exception("Generar_Reporte Error: [" + Ex.Message + "]");
            }
        }
        
        /// ************************************************************************************
        /// NOMBRE:             Exportar_Reporte_PDF
        /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
        ///                     especificada.
        /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
        ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
        /// USUARIO CREO:       Juan Alberto Hernández Negrete.
        /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACIÓN:
        /// *************************************************************************************
        public static void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_PDF, String Nombre_Reporte_Generar, Page Server, String Ruta_Externa_Pdf)
        {
            ExportOptions Opciones_Exportacion = new ExportOptions();
            DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
            PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

            try
            {
                if (Reporte is ReportDocument)
                {
                    //Direccion_Guardar_Disco.DiskFileName = Nombre_Reporte_Generar;
                    Direccion_Guardar_Disco.DiskFileName = Server.MapPath(Nombre_Reporte_Generar);
                    Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);

                    if (!String.IsNullOrEmpty(Ruta_Externa_Pdf))
                    {
                        //Guarda una copia de la factura en el proyecto de factura Global
                        Direccion_Guardar_Disco.DiskFileName = (Ruta_Externa_Pdf + Nombre_PDF);
                        //Direccion_Guardar_Disco.DiskFileName = ("C:/Users/Public/Documents/SIAC_VERSION/Codigo/SIISR3/Facturacion/Facturacion/PDF/" + Nombre_PDF);
                        Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                        Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                        Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                        Reporte.Export(Opciones_Exportacion);
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Exportar_Reporte_PDF. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Mostrar_Archivo
        ///DESCRIPCIÓN:          Muestra una archivo en una ventana.
        ///PARAMETROS:           Ruta,  Ruta del Archivo.
        ///                      Titulo_Ventana, Titulo que se muestra en al ventana
        ///                      Server, Página padre donde se mostrará
        ///                      Tipo, Tipo de la página padre
        ///CREO:                 Luis Alberto Salas
        ///FECHA_CREO:           01/Junio/2013
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************  
        public static void Mostrar_Archivo(String Ruta, String Titulo_Ventana, Page Server, Type Tipo)
        {
            try
            {
                if (System.IO.File.Exists(Server.MapPath(HttpUtility.HtmlDecode(Ruta))))
                {
                    ScriptManager.RegisterStartupScript(Server, Tipo, "Window_Archivo_Archivos", "window.open('" + Ruta + "','" + Titulo_Ventana + "','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=800')", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Server, Tipo, "Mostrar Archivo", "$.messager.alert('" + "OPDAPAS Metepec" + "','El archivo no existe.', 'warning');", true);
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Mostrar_Archivo. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Convertir_Imagen_Bytes
        ///DESCRIPCIÓN:          Convierte una imagen a bytes
        ///PARAMETROS:           Ruta,  Ruta de la imagen.
        ///                      Porcentaje_Reducion,  Porcentaje que reducira el tamaño de
        ///                         la imagen, tiene que ser double.
        ///                      Resolucion, Resolusion de la imagen dpi, entre mayor, mejor
        ///                         la calidad de la imagen.
        ///CREO:                 Luis Alberto Salas
        ///FECHA_CREO:           13/Junio/2013
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************  
        public static byte[] Convertir_Imagen_Bytes(String Ruta, Double Porcentaje_Reducion, int Resolucion)
        {
            try
            {
                System.Drawing.Image Imagen = System.Drawing.Image.FromFile(Ruta);
                MemoryStream Ms = new MemoryStream();

                int Nuevo_Ancho = Convert.ToInt32((Imagen.Width * Resolucion) / Imagen.HorizontalResolution * Porcentaje_Reducion);
                int Nuevo_Alto = Convert.ToInt32((Imagen.Height * Resolucion) / Imagen.VerticalResolution * Porcentaje_Reducion);
                Bitmap Nueva_Imagen = new Bitmap(Imagen, Nuevo_Ancho, Nuevo_Alto);
                Nueva_Imagen.SetResolution(Resolucion, Resolucion);
                Nueva_Imagen.Save(Ms, Imagen.RawFormat);

                return Ms.ToArray();
            }
            catch
            {
                return new byte[0];
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Convertir_Imagen_Bytes
        ///DESCRIPCIÓN:          Convierte una imagen a bytes
        ///PARAMETROS:           Ruta,  Ruta de la imagen.
        ///                      Nuevo_Ancho, Ancho de la imagen, con esto calcula el nuevo
        ///                         alto de la imagen.
        ///                      Resolucion, Resolusion de la imagen dpi, entre mayor, mejor
        ///                         la calidad de la imagen.
        ///CREO:                 Luis Alberto Salas
        ///FECHA_CREO:           13/Junio/2013
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************  
        public static byte[] Convertir_Imagen_Bytes(String Ruta, int Nuevo_Ancho, int Resolucion)
        {
            try
            {
                System.Drawing.Image Imagen = System.Drawing.Image.FromFile(Ruta);
                MemoryStream Ms = new MemoryStream();
                int Old_Ancho = Imagen.Width;
                int Old_Alto = Imagen.Height;
                Double Aux_Alto = ((Convert.ToDouble(Nuevo_Ancho) / Convert.ToDouble(Old_Ancho)) * Convert.ToDouble(Old_Alto));
                int Nuevo_Alto = Convert.ToInt32(Aux_Alto);
                Bitmap Nueva_Imagen = new Bitmap(Imagen, Nuevo_Ancho, Nuevo_Alto);
                Nueva_Imagen.SetResolution(Resolucion, Resolucion);
                Nueva_Imagen.Save(Ms, Imagen.RawFormat);

                return Ms.ToArray();
            }
            catch
            {
                return new byte[0];
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Convertir_Cantidad_Letras
        ///DESCRIPCIÓN:          Convierte una cantidad a letras
        ///PARAMETROS:           Cantidad_Numero, Cantidad a convertir a letras
        ///                      Moneda, Tipo de moneda Pesos o Dolares
        ///CREO:                 Luis Alberto Salas
        ///FECHA_CREO:           13/Junio/2013
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        public static String Convertir_Cantidad_Letras(Decimal Cantidad_Numero, String Moneda)
        {
            Cls_Numeros_Letras Letras = new Cls_Numeros_Letras();
            String Cantidad_Letra = String.Empty;

            try
            {
                if (Moneda.ToUpper().Equals("MXN"))
                {
                    Letras.MascaraSalidaDecimal = "00/100 M.N.";
                    Letras.SeparadorDecimalSalida = "PESOS CON";
                }
                else
                {
                    Letras.MascaraSalidaDecimal = "00/100 DLLS";
                    Letras.SeparadorDecimalSalida = "DOLARES CON";
                }
                Letras.ApocoparUnoParteEntera = true;
                Cantidad_Letra = Letras.ToCustomCardinal(Cantidad_Numero).Trim().ToUpper();
            }
            catch (Exception Ex)
            {
                throw new Exception("Convertir_Cantidad_Letras. Error:[" + Ex.Message + "]");
            }
            return Cantidad_Letra;
        }

        ///'*******************************************************************************
        ///'NOMBRE DE LA FUNCIÓN: CFD_Elimina_Espacios
        ///'DESCRIPCIÓN: Eliminas los n espacios entre palabras
        ///'PARÁMETROS:
        ///'               1. Cadena, cadena a eliminar los espacios
        ///'CREO: Miguel Angel Alvarado Enriquez
        ///'FECHA_CREO: 07/Agosto/2013 10:18 pm
        ///'MODIFICO:
        ///'FECHA_MODIFICO
       ///'CAUSA_MODIFICACIÓN
       ///'*******************************************************************************
        public static String CFD_Elimina_Espacios(String Cadena)
        {
            String[] Grupo_Cadena;
            Int32 Cuenta_Cadena;
            String Cadena_Nueva;
            String CFD_Elimina_Espacios_Blanco;


            try
            {
                //Divide la cadena por los espacios
                Grupo_Cadena = Cadena.Split(new Char[] { ' ' });

                //Valida la longitud
                if (Grupo_Cadena.GetUpperBound(0) > 0)
                {
                    Cadena_Nueva = "";

                    for (Cuenta_Cadena = 0; Cuenta_Cadena <= Grupo_Cadena.GetUpperBound(0); Cuenta_Cadena++) //Grupo_Cadena.Length gives the same error
                    {
                        if (Grupo_Cadena[Cuenta_Cadena].Trim() != "")
                        {
                            Cadena_Nueva = Cadena_Nueva + " " + Grupo_Cadena[Cuenta_Cadena].Trim();
                        }
                    }
                    if (Cadena_Nueva.Trim() != "")
                    {
                        CFD_Elimina_Espacios_Blanco = Cls_Timbrado.Valida_Caracteres_UTF(Cadena_Nueva.Trim());
                    }
                    else
                    {
                        CFD_Elimina_Espacios_Blanco = Cadena_Nueva.Trim();
                    }

                }
                else
                {
                    if (Cadena.Trim() != "")
                    {
                        CFD_Elimina_Espacios_Blanco = Cls_Timbrado.Valida_Caracteres_UTF(Cadena.Trim());
                    }
                    else
                    {
                        CFD_Elimina_Espacios_Blanco = Cadena.Trim();
                    }
                }
            }

            catch (Exception Ex)
            {
                throw new Exception("CFD_Elimina_Espacios. Error:[" + Ex.Message + "]");
            }
            return CFD_Elimina_Espacios_Blanco;
        }


        public static List<T> ConvertDataTableToList<T>(DataTable datatable) where T : new()
        {
            List<T> Temp = new List<T>();
            try
            {
                List<string> columnsNames = new List<string>();
                foreach (DataColumn DataColumn in datatable.Columns)
                    columnsNames.Add(DataColumn.ColumnName);
                Temp = datatable.AsEnumerable().ToList().ConvertAll<T>(row => getObject<T>(row, columnsNames));
                return Temp;
            }
            catch
            {
                return Temp;
            }

        }
        public static T getObject<T>(DataRow row, List<string> columnsName) where T : new()
        {
            T obj = new T();
            try
            {
                string columnname = "";
                string value = "";
                PropertyInfo[] Properties;
                Properties = typeof(T).GetProperties();
                foreach (PropertyInfo objProperty in Properties)
                {
                    columnname = columnsName.Find(name => name.ToLower() == objProperty.Name.ToLower());
                    if (!string.IsNullOrEmpty(columnname))
                    {
                        value = row[columnname].ToString();
                        if (!string.IsNullOrEmpty(value))
                        {
                            if (Nullable.GetUnderlyingType(objProperty.PropertyType) != null)
                            {
                                value = row[columnname].ToString().Replace("$", "").Replace(",", "");
                                objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(Nullable.GetUnderlyingType(objProperty.PropertyType).ToString())), null);
                            }
                            else
                            {
                                value = row[columnname].ToString().Replace("%", "");
                                objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(objProperty.PropertyType.ToString())), null);
                            }
                        }
                    }
                }
                return obj;
            }
            catch
            {
                return obj;
            }
        }
    }
}