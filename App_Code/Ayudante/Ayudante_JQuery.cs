﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Data;

namespace SIAC.Ayudante_JQuery{
    public class Ayudante_JQuery
    {

        public static string dataTableToJSONsintotalrenglones(DataTable dt)
        {
            StringBuilder jsonStringBuilder = new StringBuilder();
            StringWriter jsonStringWriter = new StringWriter(jsonStringBuilder);
            String cadenaFinal, valor, tipo;
            JsonWriter jsonWriter;
            int i, j;

            jsonWriter = new JsonTextWriter(jsonStringWriter);

            if (dt != null && dt.Rows.Count > 0)
            {
                jsonWriter.Formatting = Formatting.None;
                jsonWriter.WriteStartArray();

                for (i = 0; i < dt.Rows.Count; i++)
                {
                    jsonWriter.WriteStartObject();
                    for (j = 0; j < dt.Columns.Count; j++)
                    {
                        jsonWriter.WritePropertyName(dt.Columns[j].ColumnName.ToString().ToLower());
                        valor = dt.Rows[i][j].ToString();
                        jsonWriter.WriteValue(valor.Trim());
                    }
                    jsonWriter.WriteEndObject();

                }
                jsonWriter.WriteEndArray();
                cadenaFinal = jsonStringBuilder.ToString();
                return cadenaFinal;
            }

            return "{}";

        }




        public static string dataTableToJSON(DataTable dt)
        {
            StringBuilder jsonStringBuilder = new StringBuilder();
            StringWriter jsonStringWriter = new StringWriter(jsonStringBuilder);
            String cadenaFinal, valor, tipo;
            JsonWriter jsonWriter;
            int i, j;

            jsonWriter = new JsonTextWriter(jsonStringWriter);

            if (dt != null && dt.Rows.Count > 0)
            {
                jsonWriter.Formatting = Formatting.None;
                jsonWriter.WriteStartArray();

                for (i = 0; i < dt.Rows.Count; i++)
                {
                    jsonWriter.WriteStartObject();
                    for (j = 0; j < dt.Columns.Count; j++)
                    {
                        jsonWriter.WritePropertyName(dt.Columns[j].ColumnName.ToString().ToLower());
                        valor = dt.Rows[i][j].ToString();
                        jsonWriter.WriteValue(valor.Trim());
                    }
                    jsonWriter.WriteEndObject();

                }
                jsonWriter.WriteEndArray();
                cadenaFinal = "{\"total\":" + dt.Rows.Count + ",\"rows\":" + jsonStringBuilder.ToString() + "}";
                return cadenaFinal;
            }

            return "{\"total\":0,\"rows\":[]}";

        }
        public static String Crear_Tabla_Formato_JSON_ComboBox(DataTable Dt_Datos)
        {
            StringBuilder Buffer = new StringBuilder();
            StringWriter Escritor = new StringWriter(Buffer);
            JsonWriter Escribir_Formato_JSON = new JsonTextWriter(Escritor);
            String Cadena_Resultado = String.Empty;

            try
            {
                Escribir_Formato_JSON.Formatting = Formatting.None;
                Escribir_Formato_JSON.WriteStartArray();

                if (Dt_Datos is DataTable)
                {
                    if (Dt_Datos.Rows.Count > 0)
                    {
                        foreach (DataRow FILA in Dt_Datos.Rows)
                        {
                            Escribir_Formato_JSON.WriteStartObject();
                            foreach (DataColumn COLUMNA in Dt_Datos.Columns)
                            {
                                if (!String.IsNullOrEmpty(FILA[COLUMNA.ColumnName].ToString()))
                                {
                                    Escribir_Formato_JSON.WritePropertyName(COLUMNA.ColumnName);
                                    Escribir_Formato_JSON.WriteValue(FILA[COLUMNA.ColumnName].ToString());
                                }
                            }
                            Escribir_Formato_JSON.WriteEndObject();
                        }
                    }
                }

                Escribir_Formato_JSON.WriteEndArray();
                Cadena_Resultado = Buffer.ToString();
            }
            catch (Exception)
            {
                throw new Exception("Error al crear la cadena json para cargar un combo.");
            }
            return Cadena_Resultado;
        }
        public static String Crear_Tabla_Formato_JSON_DataGrid(DataTable Dt_Datos, Int32 Total_Registros)
        {
            StringBuilder Buffer = new StringBuilder();
            StringWriter Escritor = new StringWriter(Buffer);
            JsonWriter Escribir_Formato_JSON = new JsonTextWriter(Escritor);
            String Cadena_Resultado = String.Empty;

            try
            {
                Escribir_Formato_JSON.Formatting = Formatting.None;
                Escribir_Formato_JSON.WriteStartObject();
                Escribir_Formato_JSON.WritePropertyName("total");
                Escribir_Formato_JSON.WriteValue(Total_Registros.ToString());
                Escribir_Formato_JSON.WritePropertyName(Dt_Datos.TableName);

                if (Dt_Datos is DataTable)
                {
                    if (Dt_Datos.Rows.Count > 0)
                    {
                        Escribir_Formato_JSON.WriteStartArray();
                        foreach (DataRow FILA in Dt_Datos.Rows)
                        {
                            Escribir_Formato_JSON.WriteStartObject();
                            foreach (DataColumn COLUMNA in Dt_Datos.Columns)
                            {
                                if (!String.IsNullOrEmpty(FILA[COLUMNA.ColumnName].ToString()))
                                {
                                    Escribir_Formato_JSON.WritePropertyName(COLUMNA.ColumnName);
                                    Escribir_Formato_JSON.WriteValue(FILA[COLUMNA.ColumnName].ToString());
                                }
                            }
                            Escribir_Formato_JSON.WriteEndObject();
                        }

                        Escribir_Formato_JSON.WriteEndArray();
                        Escribir_Formato_JSON.WriteEndObject();
                        Cadena_Resultado = Buffer.ToString();
                    }
                    else Cadena_Resultado = "[]";
                }
                else Cadena_Resultado = "[]";
            }
            catch (Exception)
            {
                throw new Exception("Error al crear la cadena json para cargar un combo.");
            }
            return Cadena_Resultado;
        }
        public static String Crear_Tabla_Formato_JSON(DataTable Dt_Datos)
        {
            StringBuilder Buffer = new StringBuilder();
            StringWriter Escritor = new StringWriter(Buffer);
            JsonWriter Escribir_Formato_JSON = new JsonTextWriter(Escritor);
            String Cadena_Resultado = String.Empty;

            try
            {
                Escribir_Formato_JSON.Formatting = Formatting.None;
                Escribir_Formato_JSON.WriteStartObject();
                Escribir_Formato_JSON.WritePropertyName("TOTAL");
                Escribir_Formato_JSON.WriteValue(Dt_Datos.Rows.Count.ToString());
                Escribir_Formato_JSON.WritePropertyName(Dt_Datos.TableName);

                if (Dt_Datos is DataTable)
                {
                    if (Dt_Datos.Rows.Count > 0)
                    {
                        Escribir_Formato_JSON.WriteStartArray();
                        foreach (DataRow FILA in Dt_Datos.Rows)
                        {
                            Escribir_Formato_JSON.WriteStartObject();
                            foreach (DataColumn COLUMNA in Dt_Datos.Columns)
                            {
                                if (!String.IsNullOrEmpty(FILA[COLUMNA.ColumnName].ToString()))
                                {
                                    Escribir_Formato_JSON.WritePropertyName(COLUMNA.ColumnName);
                                    Escribir_Formato_JSON.WriteValue(FILA[COLUMNA.ColumnName].ToString());
                                }
                            }
                            Escribir_Formato_JSON.WriteEndObject();
                        }

                        Escribir_Formato_JSON.WriteEndArray();
                        Escribir_Formato_JSON.WriteEndObject();
                        Cadena_Resultado = Buffer.ToString();
                    }
                    else Cadena_Resultado = "[]";
                }
                else Cadena_Resultado = "[]";
            }
            catch (Exception)
            {
                throw new Exception("Error al crear la cadena json para cargar un combo.");
            }
            return Cadena_Resultado;
        }
        public static string onDataGrid(DataTable dt, int page, int rows)
        {

            string dato;
            page = (page == 0) ? 1 : page;
            rows = (rows == 0) ? 10 : rows;
            int start = (page - 1) * rows;
            int end = page * rows;
            end = (end > dt.Rows.Count) ? dt.Rows.Count : end;
            StringBuilder jsonBuilder = new StringBuilder();
            jsonBuilder.Append("{\"total\":" + dt.Rows.Count + ",\"rows\":[");
            for (int i = start; i < end; i++)
            {
                jsonBuilder.Append("{");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    jsonBuilder.Append("\"");
                    jsonBuilder.Append(dt.Columns[j].ColumnName);
                    jsonBuilder.Append("\":\"");
                    dato = dt.Rows[i][j].ToString().Trim();
                    dato = dato.Replace("\r\n", " ").Replace("\n", " ").Replace("\r", " ").Replace(Environment.NewLine, " ");

                    jsonBuilder.Append(dato);

                    jsonBuilder.Append("\",");
                }
                jsonBuilder.Remove(jsonBuilder.Length - 1, 1);
                jsonBuilder.Append("},");
            }
            jsonBuilder.Remove(jsonBuilder.Length - 1, 1);
            jsonBuilder.Append("]}");
            return jsonBuilder.ToString();
        }
        ///*********************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Json_A_DataTable
        ///DESCRIPCIÓN          : Crea un la cadena json para el treegrid de facturacion
        ///PARAMETROS           :
        ///CREO                 : Ramón Baeza Yépez
        ///FECHA_CREO           : 19/11/2013
        ///************************************************************************************************   
        public static DataTable Json_A_DataTable(String Json)
        {
            DataTable Dt_Resultado = new DataTable();
            String[] Json_Objetos;
            String[] Json_Atributos;
            String[] Json_Valores;
            String Columna;
            String Valor;
            DataRow Nueva_Linea;
            String Ultimo_Valor;
            try
            {
                //Quitamos los caracteres de inicio y fin de la cadena json
                Json.Remove(0, 2);
                Json.Remove(Json.Length - 3, 2);
                Json_Objetos = Json.Split('{');
                for (int Indice_Objetos = 1; Indice_Objetos < Json_Objetos.Length; Indice_Objetos++)
                {
                    Json_Atributos = Json_Objetos[Indice_Objetos].Split(';');
                    for (int Indice_Atributos = 0; Indice_Atributos < Json_Atributos.Length; Indice_Atributos++)
                    {
                        Json_Valores = Json_Atributos[Indice_Atributos].Split(':');
                        Columna = Json_Valores[0].Replace("\"", "").Replace("[","").Trim();
                        Valor = Json_Valores[1].Replace("\"", "").Trim();
                        if (!Dt_Resultado.Columns.Contains(Columna))
                        {
                            Dt_Resultado.Columns.Add(Columna, typeof(System.String));
                        }
                    }
                }
                for (int Indice_Objetos = 1; Indice_Objetos < Json_Objetos.Length; Indice_Objetos++)
                {
                    Json_Atributos = Json_Objetos[Indice_Objetos].Split(';');
                    Nueva_Linea = Dt_Resultado.NewRow();
                    for (int Indice_Atributos = 0; Indice_Atributos < Json_Atributos.Length; Indice_Atributos++)
                    {
                        Json_Valores = Json_Atributos[Indice_Atributos].Split(':');
                        Columna = Json_Valores[0].Replace("\"", "").Replace("[", "").Trim();
                        Valor = Json_Valores[1].Replace("\"", "").Trim();
                        Nueva_Linea[Columna] = Valor;
                    }
                    Dt_Resultado.Rows.Add(Nueva_Linea);
                }
                Ultimo_Valor = Dt_Resultado.Rows[Dt_Resultado.Rows.Count - 1][Dt_Resultado.Columns.Count - 1].ToString();
                Dt_Resultado.Rows[Dt_Resultado.Rows.Count - 1][Dt_Resultado.Columns.Count - 1] = Ultimo_Valor.Remove(Ultimo_Valor.Length - 2, 2);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al crear la tabla desde la cadena json." + Ex.Message);
            }
            return Dt_Resultado;
        }
    }
}