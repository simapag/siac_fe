﻿using System;
using System.Web;
using System.Data;
using System.Text;

namespace Erp.Sesiones
{
    public static class Cls_Sessiones
    {
        private static String S_Empleado_ID = "Empleado_ID";
        private static String S_Empleado_Nombre = "Empleado_Nombre";
        private static String S_Nombre_Empleado = "Empleado_Nombre";
        private static String S_Usuario_ID = "Usuario_ID";
        private static String S_Rol_ID = "Rol_ID";
        private static String S_Area_ID = "Area_ID";
        private static String S_Centro_Costo_ID = "Centro_Costo_ID";
        private static String S_Puesto_ID = "Puesto_ID";
        private static String S_Ip = "Ip";
        private static String S_Equipo = "Equipo";
        private static String S_Gestor_Base_Datos = "Gbd";
        private static String S_Mostrar_Menu = "Mostrar_Menu";
        private static String S_Menus_Control_Acceso = "MENUS_CONTROL_ACCESO";
        private static String S_Mensaje_Sistema = "Mensaje_Sistema";
        private static String S_Imagen_Sistema = "Imagen_Sistema";
        private static String S_Empresa_ID = "Empresa_ID";
        private static String S_Nombre_Empresa = "Nombre_Empresa";
        private static String S_Conexion_Servidor = "Servidor";
        private static String S_Conexion_Base = "Base";
        private static String S_Conexion_Usuario = "Usuario";
        private static String S_Conexion_Password = "Password";
        private static String S_Conexion_Gestor = "Gestor";
        private static String S_Historial_Nomina_Generada = "Nomina_Generada";
        private static String S_Totales = "Totales_Percepciones_Deducciones";
        private static String S_Dependencia_ID_Empleado = "Dependencia_ID_Empleado";
        private static String S_Datos_Empleado = "Datos_Empleado";
        private static String S_Contrato_ID= "Contrato_ID";
        private static String S_Cliente_ID = "Cliente_ID";
        private static String S_Nombre_Corto = "Nombre_Corto";
        private static String S_Razon_Social = "Razon_Social";
        private static String S_Predio_Id = "Predio_Id";
        private static String S_Email = "Email";
        private static String S_Rpu = "Rpu";
        public static DataTable Datos_Empleado
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Datos_Empleado] == null)
                    return null;
                else
                    return (DataTable)HttpContext.Current.Session[Cls_Sessiones.S_Datos_Empleado];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Datos_Empleado] = value;
            }
        }

        public static String Dependencia_ID_Empleado
        {
            get
            {
                return String.Empty;
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Dependencia_ID_Empleado] = value;
            }
        }

        public static String Nombre_Empresa
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Nombre_Empresa] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Nombre_Empresa].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Nombre_Empresa] = value;
            }
        }

        public static StringBuilder Historial_Nomina_Generada
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Historial_Nomina_Generada] == null)
                    return null;
                else
                    return (StringBuilder)HttpContext.Current.Session[Cls_Sessiones.S_Historial_Nomina_Generada];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Historial_Nomina_Generada] = value;
            }
        }

        public static DataTable Totales_Percepciones_Deducciones
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Totales] == null)
                    return null;
                else
                    return (DataTable)HttpContext.Current.Session[Cls_Sessiones.S_Totales];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Totales] = value;
            }
        }

        public static String Empleado_ID
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Empleado_ID] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Empleado_ID].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Empleado_ID] = value;
            }
        }
        public static String Empleado_Nombre
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Empleado_Nombre] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Empleado_Nombre].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Empleado_Nombre] = value;
            }
        }

        public static String Nombre_Empleado
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Empleado_Nombre] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Empleado_Nombre].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Empleado_Nombre] = value;
            }
        }

        public static String Usuario_ID
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Usuario_ID] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Usuario_ID].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Usuario_ID] = value;
            }
        }
        public static String Rol_ID
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Rol_ID] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Rol_ID].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Rol_ID] = value;
            }
        }
        public static String Area_ID
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Area_ID] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Area_ID].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Area_ID] = value;
            }
        }
        public static String Centro_Costo_ID
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Centro_Costo_ID] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Centro_Costo_ID].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Centro_Costo_ID] = value;
            }
        }
        public static String Puesto_ID
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Puesto_ID] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Puesto_ID].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Puesto_ID] = value;
            }
        }
        public static String Ip
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Ip] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Ip].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Ip] = value;
            }
        }
        public static String Equipo
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Equipo] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Equipo].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Equipo] = value;
            }
        }
        public static String Gestor_Base_Datos
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Gestor_Base_Datos] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Gestor_Base_Datos].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Gestor_Base_Datos] = value;
            }
        }
        public static Boolean Mostrar_Menu
        {
            get
            {
                bool dato = Convert.ToBoolean(HttpContext.Current.Session[Cls_Sessiones.S_Mostrar_Menu]);
                return dato;
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Mostrar_Menu] = value;
            }
        }
        public static DataTable Menu_Control_Acceso
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Menus_Control_Acceso] == null)
                    return null;
                else
                    return (DataTable)HttpContext.Current.Session[Cls_Sessiones.S_Menus_Control_Acceso];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Menus_Control_Acceso] = value;
            }
        }
        public static String Mensaje_Sistema
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Mensaje_Sistema] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Mensaje_Sistema].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Mensaje_Sistema] = value;
            }
        }
        public static String Imagen_Sistema
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Imagen_Sistema] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Imagen_Sistema].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Imagen_Sistema] = value;
            }
        }
        public static String Empresa_ID
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Empresa_ID] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Empresa_ID].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Empresa_ID] = value;
            }
        }
        public static String Conexion_Servidor
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Conexion_Servidor] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Conexion_Servidor].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Conexion_Servidor] = value;
            }
        }
        public static String Conexion_Base
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Conexion_Base] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Conexion_Base].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Conexion_Base] = value;
            }
        }
        public static String Conexion_Usuario
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Conexion_Usuario] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Conexion_Usuario].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Conexion_Usuario] = value;
            }
        }
        public static String Conexion_Password
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Conexion_Password] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Conexion_Password].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Conexion_Password] = value;
            }
        }
        public static String Conexion_Gestor
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Conexion_Gestor] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Conexion_Gestor].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Conexion_Gestor] = value;
            }
        }

        public static String Contrato_ID
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Contrato_ID] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Contrato_ID].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Contrato_ID] = value;
            }
        }

        public static String Cliente_ID
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Cliente_ID] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Cliente_ID].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Cliente_ID] = value;
            }
        }

        public static String Nombre_Corto
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Nombre_Corto] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Nombre_Corto].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Nombre_Corto] = value;
            }
        }
        public static String Razon_Social
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Razon_Social] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Razon_Social].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Razon_Social] = value;
            }
        }
        public static String Predio_ID
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Predio_Id] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Predio_Id].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Predio_Id] = value;
            }
        }
        public static String Correo_Electronico {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Email] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Email].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Email] = value;
            }
        
        }
        public static String Rpu
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Rpu] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Rpu].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Rpu] = value;
            }

        }
    }
}
