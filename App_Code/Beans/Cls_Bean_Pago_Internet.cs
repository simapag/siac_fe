﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIAC.Pagos_Internet.Bean
{
    public class Cls_Bean_Pago_Internet
    {
        public string no_factura_recibo { get; set; }
        public int no_cuenta { get; set; }
        public string fecha_pago_banco { get; set; }
        public string numero_autorizacion { get; set; }
        public string numero_tarjeta { get; set; }
        public string referencia_bancaria { get; set; }
        public double importe_total { get; set; }
        public int anio { get; set; }
        public int bimestre { get; set; }
        public string comentario { get; set; }
        public string fecha_inicio_periodo { get; set; }
        public string fecha_termino_periodo { get; set; }
        public string fecha_limite_pago { get; set; }
        public string periodo_facturacion { get; set; }
        public string fecha_creo { get; set; }
        public string estatus { get; set; }
        public string correo_electronico { get; set; }
    }
}