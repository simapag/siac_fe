﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIISR3_Impresion_Facturacion.Clases.Beans
{
    public class Cls_Bean_Conceptos_Facturacion_Electronica
    {
        public string no_cuenta { get; set; }
        public string codigo_barras { get; set; }
        public double iva { get; set; }
        public double importe { get; set; }
        public double total { get; set; }
        public string clave_concepto { get; set; }
        public string descripcion { get; set; }
        public string valor_unitario { get; set; }
    }
}
