﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIAC_Apl_Parametros_Facturacion.Datos;
using System.Data;
using SIAC.Facturacion.Reportes.Entidades;

/// <summary>
/// Summary description for Cls_Apl_Parametros_Facturacion_Negocios
/// </summary>
/// 
namespace SIAC.Facturacion.Reportes
{
    public class Cls_Factura_Pdf
    {
        public byte[] Imagen_Qr { get; set; }
        public string Razon_Social_Empresa { get; set; }
        public string Rfc_Empresa { get; set; }
        public string Direccion_Empresa { get; set; }
        public string Codigo_Postal_Empresa { get; set; }
        public string Colonia_Empresa { get; set; }
        public string Ciudad_Empresa { get; set; }
        public string Estado_Empresa { get; set; }
        public string Pais_Empresa { get; set; }
        public string Telefono_Empresa { get; set; }
        public string Regimen_Fiscal { get; set; }
        public string Lugar_Expedicion { get; set; }
        public string Tipo_CFDI { get; set; }
        public string No_certificado { get; set; }
        public string No_certificado_Sat { get; set; }
        public string Fecha_Timbrado { get; set; }
        public string Fecha_Emision { get; set; }
        public string Fecha_Vencimiento { get; set; }
        public string Fecha_Creo_Xml { get; set; }
        public string Uuid { get; set; }
        public string Folio { get; set; }
        public string Serie { get; set; }
        public string Moneda { get; set; }
        public string Tipo_Cambio { get; set; }
        public string Forma_Pago { get; set; }
        public string Metodo_Pago { get; set; }
        public string Condiciones_Pago { get; set; }
        public string Cuenta_Predial { get; set; }
        public string Importe_Letra { get; set; }
        public string Descuento { get; set; }
        public string Total { get; set; }
        public string SubTotal { get; set; }
        public string Version_Cfdi { get; set; }
        public string Dias_Credito { get; set; }
        public string Porcentaje_Descuento { get; set; }
        public string Comentario { get; set; }
        public string Certificado { get; set; }
        public string No_Autorizacion { get; set; }
        public string Cadena_Original { get; set; }
        public string Timbre_Sello_CFD { get; set; }
        public string Timbre_Sello_SAT { get; set; }
        public string Leyenda_Fiscal { get; set; }
        public string Impuesto_Retenido { get; set; }
        public string Impuesto_Traslado { get; set; }
        public string Recibos_Incluidos { get; set; }

        public string Logo { get; set; }

        public string RPU { get; set; }
        public string Periodo_Pago { get; set; }
        public string Tipo_Servicio { get; set; }
        public string No_Recibo { get; set; }
        public string Codigo_Barras { get; set; }
        public string Uso_Cfdi { get; set; }

        public List<Cls_Cfdi_Relacionados> Lst_Cfdi_Relacionados { get; set; }
        public Cls_Datos_Clientes Datos_Clientes { get; set; }
        public List<Cls_Datos_Conceptos> Lst_Conceptos { get; set; }
        public List<Cls_Datos_Impuestos> Lst_Impuestos { get; set; }
    }
}
