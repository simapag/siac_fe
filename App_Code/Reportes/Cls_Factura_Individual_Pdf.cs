﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

using SIAC.Facturacion.Reportes.Entidades;

namespace SIAC.Facturacion.Reportes
{
    public class Cls_Factura_Individual_Pdf
    {
        Document documento = new Document();
        Cls_Factura_Pdf Reporte;
        String Ruta_Pdf;
        #region Fuentes
        private Font arial = FontFactory.GetFont("arial", 8);
        private Font arialChica = FontFactory.GetFont("arial", 6);
        private Font arialChicaNegrita = FontFactory.GetFont("arial", 8, Font.BOLD);
        private Font arialGrandeNegrita = FontFactory.GetFont("arial", 11f, Font.BOLD);
        private Font _standardFont = new iTextSharp.text.Font(Font.HELVETICA, 8, Font.NORMAL, Color.BLACK);
        private Font _BigFont = new iTextSharp.text.Font(Font.HELVETICA, 9, Font.NORMAL, Color.BLACK);
        private Font _standardFontWhite = new iTextSharp.text.Font(Font.HELVETICA, 8, Font.NORMAL, Color.WHITE);
        #endregion
        public Cls_Factura_Individual_Pdf(string Ruta_Pdf, Cls_Factura_Pdf Reporte)
        {
            this.Reporte = Reporte;
            this.Ruta_Pdf = Ruta_Pdf;
        }
        public void Iniciar_Reporte()
        {
            try
            {
                documento.SetPageSize(iTextSharp.text.PageSize.A4);

                PdfWriter writer = PdfWriter.GetInstance(documento, new FileStream(Ruta_Pdf, FileMode.Create));
                documento.Open();
                //PdfContentByte cb = writer.DirectContent;
                documento.AddTitle("Factura_Electronica");
                documento.AddCreator("Conectividad y Telecomunicacion S.A de C.V.");
                documento.Add(Crear_Titulo());
                documento.Add(Crear_Tabla_Datos());
                documento.Add(Crear_Tabla_Uuid());
                documento.Add(Crear_Tabla_Conceptos());
                documento.Add(Crear_Tabla_Totales());
                if (!string.IsNullOrEmpty(Reporte.Recibos_Incluidos))
                    documento.Add(Crear_Recibos_Relacionados());
                if (!string.IsNullOrEmpty(Reporte.Comentario))
                    documento.Add(Crear_Comentarios());
                if (Reporte.Imagen_Qr.Length > 0)
                    documento.Add(Crear_Tabla_Sellos());
                //documento.Close();
            }
            catch (Exception Ex)
            {
                throw new Exception("Generar_Reporte Error: [" + Ex.Message + "]");
            }
            finally
            {
                if (documento != null && documento.IsOpen())
                    documento.Close();
            }
        }

        private PdfPTable Crear_Recibos_Relacionados()
        {
            PdfPTable tabla;
            tabla = new PdfPTable(1) { WidthPercentage = 100 };
            tabla.TotalWidth = 400f;
            PdfPCell clTituloRecibosIncliudos = new PdfPCell(new Paragraph("Recibos_Incluidos", arialChicaNegrita));
            clTituloRecibosIncliudos.BorderWidthTop = 0;
            clTituloRecibosIncliudos.BorderWidthRight = 0;
            clTituloRecibosIncliudos.BorderWidthBottom = 0;
            clTituloRecibosIncliudos.BorderWidthLeft = 0;
            clTituloRecibosIncliudos.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell clRecibosIncliudos = new PdfPCell(new Paragraph(Reporte.Recibos_Incluidos, arialChica));
            clRecibosIncliudos.BorderWidthTop = 0.5f;
            clRecibosIncliudos.BorderWidthRight = 0;
            clRecibosIncliudos.BorderWidthBottom = 0.5f;
            clRecibosIncliudos.BorderWidthLeft = 0;
            clRecibosIncliudos.HorizontalAlignment = Element.ALIGN_LEFT;
            tabla.AddCell(clTituloRecibosIncliudos);
            tabla.AddCell(clRecibosIncliudos);
            return tabla;
        }

        private PdfPTable Crear_Comentarios()
        {
            PdfPTable tabla;
            tabla = new PdfPTable(1) { WidthPercentage = 100 };
            tabla.TotalWidth = 400f;
            PdfPCell clComentarios = new PdfPCell(new Paragraph(Reporte.Comentario, arial));
            clComentarios.BorderWidthTop = 0.5f;
            clComentarios.BorderWidthRight = 0;
            clComentarios.BorderWidthBottom = 0.5f;
            clComentarios.BorderWidthLeft = 0;
            clComentarios.HorizontalAlignment = Element.ALIGN_LEFT;
            tabla.AddCell(clComentarios);
            return tabla;
        }

        private PdfPTable Crear_Tabla_Sellos()
        {
            PdfPTable tabla;
            PdfPCell clImagenQr;
            Image imagenQr = null;

            tabla = new PdfPTable(2) { WidthPercentage = 100 };
            tabla.TotalWidth = 400f;
            float[] widths = new float[] { 1f, 3f };
            tabla.SetWidths(widths);

            imagenQr = Image.GetInstance(Reporte.Imagen_Qr);
            imagenQr.ScaleToFit(100, 100f);
            if (imagenQr != null)
                clImagenQr = new PdfPCell(imagenQr);
            else
                clImagenQr = new PdfPCell();
            clImagenQr.Rowspan = 10;
            clImagenQr.Border = 0;
            clImagenQr.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell clTitulo = new PdfPCell(new Paragraph("Este documento es una representación impresa de un CFDI", arialChicaNegrita));
            clTitulo.Border = 0;
            clTitulo.Colspan = 2;
            clTitulo.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell clTituloVersionCfdi = new PdfPCell(new Paragraph("Version CFDI", arialChicaNegrita));
            clTituloVersionCfdi.Border = 0;
            clTituloVersionCfdi.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clVersionCfdi = new PdfPCell(new Paragraph(Reporte.Version_Cfdi, arial));
            clVersionCfdi.Border = 0;
            clVersionCfdi.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTituloCadenaOriginal = new PdfPCell(new Paragraph("Cadena Original de Complemento de Certificado Digital del SAT", arialChicaNegrita));
            clTituloCadenaOriginal.Border = 0;
            clTituloCadenaOriginal.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clCadenaOriginal = new PdfPCell(new Paragraph(Reporte.Cadena_Original, arial));
            clCadenaOriginal.Border = 0;
            clCadenaOriginal.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clvacio = new PdfPCell(new Paragraph("", arial));
            clvacio.Border = 0;

            PdfPCell clTituloSelloEmisor = new PdfPCell(new Paragraph("Sello Digital del Emisor", arialChicaNegrita));
            clTituloSelloEmisor.Border = 0;
            clTituloSelloEmisor.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clSelloEmisor = new PdfPCell(new Paragraph(Reporte.Timbre_Sello_CFD, arial));
            clSelloEmisor.Border = 0;
            clSelloEmisor.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTituloSelloSat = new PdfPCell(new Paragraph("Sello Digital del SAT", arialChicaNegrita));
            clTituloSelloSat.Border = 0;
            clTituloSelloSat.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clSelloSat = new PdfPCell(new Paragraph(Reporte.Timbre_Sello_SAT, arial));
            clSelloSat.Border = 0;
            clSelloSat.HorizontalAlignment = Element.ALIGN_LEFT;


            tabla.AddCell(clTitulo);
            tabla.AddCell(clImagenQr);
            tabla.AddCell(clTituloVersionCfdi);
            tabla.AddCell(clVersionCfdi);
            tabla.AddCell(clTituloCadenaOriginal);
            tabla.AddCell(clCadenaOriginal);
            tabla.AddCell(clvacio);
            tabla.AddCell(clTituloSelloEmisor);
            tabla.AddCell(clSelloEmisor);
            tabla.AddCell(clvacio);
            tabla.AddCell(clTituloSelloSat);
            tabla.AddCell(clSelloSat);

            return tabla;
        }

        private PdfPTable Crear_Tabla_Totales()
        {
            PdfPTable tabla;
            tabla = new PdfPTable(3) { WidthPercentage = 100 };
            tabla.TotalWidth = 400f;
            float[] widths = new float[] { 4f, 1f, 1f };
            tabla.SetWidths(widths);

            PdfPCell clTituloImporteLetra = new PdfPCell(new Paragraph("Importe Total con Letra", arial));
            clTituloImporteLetra.BorderWidthTop = 0.5f;
            clTituloImporteLetra.BorderWidthRight = 0.5f;
            clTituloImporteLetra.BorderWidthBottom = 0;
            clTituloImporteLetra.BorderWidthLeft = 0.5f;
            clTituloImporteLetra.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTituloSubtotal = new PdfPCell(new Paragraph("Subtotal", arial));
            clTituloSubtotal.BorderWidthTop = 0.5f;
            clTituloSubtotal.BorderWidthRight = 0;
            clTituloSubtotal.BorderWidthBottom = 0;
            clTituloSubtotal.BorderWidthLeft = 0;
            clTituloSubtotal.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clSubtotal = new PdfPCell(new Paragraph(Reporte.SubTotal, arial));
            clSubtotal.BorderWidthTop = 0.5f;
            clSubtotal.BorderWidthRight = 0.5f;
            clSubtotal.BorderWidthBottom = 0;
            clSubtotal.BorderWidthLeft = 0;
            clSubtotal.HorizontalAlignment = Element.ALIGN_RIGHT;

            PdfPCell clImporteLetra = new PdfPCell(new Paragraph(Reporte.Importe_Letra, arial));
            clImporteLetra.BorderWidthTop = 0;
            clImporteLetra.BorderWidthRight = 0.5f;
            clImporteLetra.BorderWidthBottom = 0.5f;
            clImporteLetra.BorderWidthLeft = 0.5f;
            clImporteLetra.Rowspan = 4;
            clImporteLetra.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTasa = new PdfPCell(new Paragraph("", arial));
            clTasa.Border = 0;
            clTasa.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clImpuestoTraslado = new PdfPCell(new Paragraph(Reporte.Impuesto_Traslado, arial));
            clImpuestoTraslado.BorderWidthTop = 0;
            clImpuestoTraslado.BorderWidthRight = 0.5f;
            clImpuestoTraslado.BorderWidthBottom = 0;
            clImpuestoTraslado.BorderWidthLeft = 0;
            clImpuestoTraslado.HorizontalAlignment = Element.ALIGN_RIGHT;

            PdfPCell clTituloDescuento = new PdfPCell(new Paragraph("", arial));
            clTituloDescuento.Border = 0;
            clTituloDescuento.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clDescuento = new PdfPCell(new Paragraph("", arial));
            clDescuento.BorderWidthTop = 0;
            clDescuento.BorderWidthRight = 0.5f;
            clDescuento.BorderWidthBottom = 0;
            clDescuento.BorderWidthLeft = 0;
            clDescuento.HorizontalAlignment = Element.ALIGN_RIGHT;

            //string iva = "IVA"  + (Reporte.Lst_Impuestos[0].TasaOcuota > 0 ? string.Format(" {0}%", Reporte.Lst_Impuestos[0].TasaOcuota) : "");

            PdfPCell clTituloImpuestoTraslado = new PdfPCell(new Paragraph("IVA", arial));
            clTituloImpuestoTraslado.Border = 0;
            clTituloImpuestoTraslado.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clretencion = new PdfPCell(new Paragraph(Reporte.Impuesto_Retenido, arial));
            clretencion.BorderWidthTop = 0;
            clretencion.BorderWidthRight = 0.5f;
            clretencion.BorderWidthBottom = 0;
            clretencion.BorderWidthLeft = 0;
            clretencion.HorizontalAlignment = Element.ALIGN_RIGHT;

            PdfPCell clTituloImpuestoRetenidos = new PdfPCell(new Paragraph("Impuestos Retenidos", arial));
            clTituloImpuestoRetenidos.BorderWidthTop = 0;
            clTituloImpuestoRetenidos.BorderWidthRight = 0;
            clTituloImpuestoRetenidos.BorderWidthBottom = 0;
            clTituloImpuestoRetenidos.BorderWidthLeft = 0;
            clTituloImpuestoRetenidos.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTituloTotal = new PdfPCell(new Paragraph("Total", arial));
            clTituloTotal.BorderWidthTop = 0;
            clTituloTotal.BorderWidthRight = 0;
            clTituloTotal.BorderWidthBottom = 0.5f;
            clTituloTotal.BorderWidthLeft = 0;
            clTituloTotal.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTotal = new PdfPCell(new Paragraph(Reporte.Total, arial));
            clTotal.BorderWidthTop = 0;
            clTotal.BorderWidthRight = 0.5f;
            clTotal.BorderWidthBottom = 0.5f;
            clTotal.BorderWidthLeft = 0;
            clTotal.HorizontalAlignment = Element.ALIGN_RIGHT;

            PdfPCell clvacio = new PdfPCell(new Paragraph("", arial));
            clvacio.Border = 0;
            
            
            

            tabla.AddCell(clTituloImporteLetra);
            tabla.AddCell(clTituloSubtotal);
            tabla.AddCell(clSubtotal);
            tabla.AddCell(clImporteLetra);
            tabla.AddCell(clTituloImpuestoTraslado);
            tabla.AddCell(clImpuestoTraslado);
            tabla.AddCell(clTituloDescuento);
            tabla.AddCell(clDescuento);
            tabla.AddCell(clvacio);
            clvacio.BorderWidthRight = 0.5f;
            tabla.AddCell(clvacio);
            tabla.AddCell(clTituloTotal);
            tabla.AddCell(clTotal);

            clvacio.Border = 0;
            tabla.AddCell(clvacio);
            tabla.AddCell(clvacio);
            tabla.AddCell(clvacio);



            return tabla;
        }

        private PdfPTable Crear_Tabla_Conceptos()
        {
            PdfPTable tabla;
            tabla = new PdfPTable(6) { WidthPercentage = 100 };
            tabla.TotalWidth = 400f;
            float[] widths = new float[] { 1f, 2f, 4f, 2f, 2f, 2f };
            tabla.SetWidths(widths);
            PdfPCell clTituloCantidad = new PdfPCell(new Paragraph("Cantidad", arialChicaNegrita));
            clTituloCantidad.BorderWidthLeft = 0.5f;
            clTituloCantidad.BorderWidthRight = 0;
            clTituloCantidad.BorderWidthBottom = 0.5f;
            clTituloCantidad.BorderWidthTop = 0.5f;
            clTituloCantidad.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTituloUnidad = new PdfPCell(new Paragraph("Clave", arialChicaNegrita));
            clTituloUnidad.BorderWidthLeft = 0.5f;
            clTituloUnidad.BorderWidthRight = 0;
            clTituloUnidad.BorderWidthBottom = 0.5f;
            clTituloUnidad.BorderWidthTop = 0.5f;
            clTituloUnidad.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell ClsTituloDescripcion = new PdfPCell(new Paragraph("Descripción", arialChicaNegrita));
            ClsTituloDescripcion.BorderWidthLeft = 0.5f;
            ClsTituloDescripcion.BorderWidthRight = 0;
            ClsTituloDescripcion.BorderWidthBottom = 0.5f;
            ClsTituloDescripcion.BorderWidthTop = 0.5f;
            ClsTituloDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell clTituloPrecioUnitario = new PdfPCell(new Paragraph("Precio Unitario", arialChicaNegrita));
            clTituloPrecioUnitario.BorderWidthLeft = 0.5f;
            clTituloPrecioUnitario.BorderWidthRight = 0;
            clTituloPrecioUnitario.BorderWidthBottom = 0.5f;
            clTituloPrecioUnitario.BorderWidthTop = 0.5f;
            clTituloPrecioUnitario.HorizontalAlignment = Element.ALIGN_RIGHT;

            PdfPCell clTituloUnidadAgregada = new PdfPCell(new Paragraph("Unidad", arialChicaNegrita));
            clTituloUnidadAgregada.BorderWidthLeft = 0.5f;
            clTituloUnidadAgregada.BorderWidthRight = 0;
            clTituloUnidadAgregada.BorderWidthBottom = 0.5f;
            clTituloUnidadAgregada.BorderWidthTop = 0.5f;
            clTituloUnidadAgregada.HorizontalAlignment = Element.ALIGN_RIGHT;

            PdfPCell clTituloImporte = new PdfPCell(new Paragraph("Importe", arialChicaNegrita));
            clTituloImporte.BorderWidthLeft = 0.5f;
            clTituloImporte.BorderWidthRight = 0.5f;
            clTituloImporte.BorderWidthBottom = 0.5f;
            clTituloImporte.BorderWidthTop = 0.5f;
            clTituloImporte.HorizontalAlignment = Element.ALIGN_RIGHT;

            tabla.AddCell(clTituloCantidad);
            tabla.AddCell(clTituloUnidad);
            tabla.AddCell(ClsTituloDescripcion);
            tabla.AddCell(clTituloUnidadAgregada);
            tabla.AddCell(clTituloPrecioUnitario);
            tabla.AddCell(clTituloImporte);
            foreach (Cls_Datos_Conceptos bean in Reporte.Lst_Conceptos)
            {
                clTituloCantidad = new PdfPCell(new Paragraph(bean.Cantidad, arial));
                clTituloCantidad.BorderWidthLeft = 0.5f;
                clTituloCantidad.BorderWidthRight = 0;
                clTituloCantidad.BorderWidthBottom = 0.5f;
                clTituloCantidad.BorderWidthTop = 0f;
                clTituloCantidad.HorizontalAlignment = Element.ALIGN_LEFT;

                clTituloUnidad = new PdfPCell(new Paragraph(bean.Unidad_Desc, arial));
                clTituloUnidad.BorderWidthLeft = 0;
                clTituloUnidad.BorderWidthRight = 0;
                clTituloUnidad.BorderWidthBottom = 0.5f;
                clTituloUnidad.BorderWidthTop = 0;
                clTituloUnidad.HorizontalAlignment = Element.ALIGN_LEFT;

                ClsTituloDescripcion = new PdfPCell(new Paragraph(bean.Descripcion, arial));
                ClsTituloDescripcion.BorderWidthLeft = 0;
                ClsTituloDescripcion.BorderWidthRight = 0;
                ClsTituloDescripcion.BorderWidthBottom = 0.5f;
                ClsTituloDescripcion.BorderWidthTop = 0;
                ClsTituloDescripcion.HorizontalAlignment = Element.ALIGN_LEFT;

                clTituloUnidadAgregada = new PdfPCell(new Paragraph("SERVICIOS", arial));
                clTituloUnidadAgregada.BorderWidthLeft = 0;
                clTituloUnidadAgregada.BorderWidthRight = 0;
                clTituloUnidadAgregada.BorderWidthBottom = 0.5f;
                clTituloUnidadAgregada.BorderWidthTop = 0;
                clTituloUnidadAgregada.HorizontalAlignment = Element.ALIGN_RIGHT;

                clTituloPrecioUnitario = new PdfPCell(new Paragraph(bean.valorUnitario, arial));
                clTituloPrecioUnitario.BorderWidthLeft = 0;
                clTituloPrecioUnitario.BorderWidthRight = 0;
                clTituloPrecioUnitario.BorderWidthBottom = 0.5f;
                clTituloPrecioUnitario.BorderWidthTop = 0;
                clTituloPrecioUnitario.HorizontalAlignment = Element.ALIGN_RIGHT;

                clTituloImporte = new PdfPCell(new Paragraph(String.Format("{0:#0.00}", bean.Subtotal), arial));
                clTituloImporte.BorderWidthLeft = 0;
                clTituloImporte.BorderWidthRight = 0.5f;
                clTituloImporte.BorderWidthBottom = 0.5f;
                clTituloImporte.BorderWidthTop = 0;
                clTituloImporte.HorizontalAlignment = Element.ALIGN_RIGHT;

                tabla.AddCell(clTituloCantidad);
                tabla.AddCell(clTituloUnidad);
                tabla.AddCell(ClsTituloDescripcion);
                tabla.AddCell(clTituloUnidadAgregada);
                tabla.AddCell(clTituloPrecioUnitario);
                tabla.AddCell(clTituloImporte);
            }

            PdfPCell clvacio = new PdfPCell(new Paragraph("", arial));
            clvacio.Border = 0;
            clvacio.Colspan = 5;
            tabla.AddCell(clvacio);
            tabla.AddCell(clvacio);
            tabla.AddCell(clvacio);

            return tabla;
        }

        private PdfPTable Crear_Tabla_Datos()
        {
            PdfPTable tabla;
            tabla = new PdfPTable(6) { WidthPercentage = 100 };
            tabla.TotalWidth = 400f;
            float[] widths = new float[] { 1f, 3f, 2f, 1f, 1f, 1f };
            tabla.SetWidths(widths);

            PdfPCell clFacturado = new PdfPCell(new Paragraph("Facturado A:", arialChicaNegrita));
            clFacturado.Colspan = 2;
            clFacturado.BorderWidthLeft = 0.5f;
            clFacturado.BorderWidthRight = 0;
            clFacturado.BorderWidthBottom = 0;
            clFacturado.BorderWidthTop = 0.5f;
            clFacturado.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell clTituloSerie = new PdfPCell(new Paragraph("Serie", arialChicaNegrita));
            clTituloSerie.BorderWidthLeft = 0.5f;
            clTituloSerie.BorderWidthRight = 0;
            clTituloSerie.BorderWidthBottom = 0;
            clTituloSerie.BorderWidthTop = 0.5f;
            clTituloSerie.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clSerie = new PdfPCell(new Paragraph(Reporte.Serie, arial));
            clSerie.BorderWidthLeft = 0;
            clSerie.BorderWidthRight = 0;
            clSerie.BorderWidthBottom = 0;
            clSerie.BorderWidthTop = 0.5f;
            clSerie.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTituloFolio = new PdfPCell(new Paragraph("Folio", arialChicaNegrita));
            clTituloFolio.BorderWidthLeft = 0.5f;
            clTituloFolio.BorderWidthRight = 0;
            clTituloFolio.BorderWidthBottom = 0;
            clTituloFolio.BorderWidthTop = 0.5f;
            clTituloFolio.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clFolio = new PdfPCell(new Paragraph(Reporte.Folio, arial));
            clFolio.BorderWidthLeft = 0;
            clFolio.BorderWidthRight = 0;
            clFolio.BorderWidthBottom = 0;
            clFolio.BorderWidthTop = 0.5f;
            clFolio.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTituloUsoCfdi = new PdfPCell(new Paragraph("Uso CFDI", arialChicaNegrita));
            clTituloUsoCfdi.BorderWidthLeft = 0;
            clTituloUsoCfdi.BorderWidthRight = 0;
            clTituloUsoCfdi.BorderWidthBottom = 0;
            clTituloUsoCfdi.BorderWidthTop = 0.5f;
            clTituloUsoCfdi.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clUsoCfdi = new PdfPCell(new Paragraph(Reporte.Uso_Cfdi, arial));
            clUsoCfdi.BorderWidthLeft = 0;
            clUsoCfdi.BorderWidthRight = 0.5f;
            clUsoCfdi.BorderWidthBottom = 0;
            clUsoCfdi.BorderWidthTop = 0.5f;
            clUsoCfdi.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clRazonSocial = new PdfPCell(new Paragraph(Reporte.Datos_Clientes.Razon_Social, arial));
            clRazonSocial.Colspan = 2;
            clRazonSocial.BorderWidthLeft = 0.5f;
            clRazonSocial.BorderWidthRight = 0;
            clRazonSocial.BorderWidthBottom = 0;
            clRazonSocial.BorderWidthTop = 0;
            clRazonSocial.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTituloMoneda = new PdfPCell(new Paragraph("Moneda", arialChicaNegrita));
            clTituloMoneda.BorderWidthLeft = 0.5f;
            clTituloMoneda.BorderWidthRight = 0;
            clTituloMoneda.BorderWidthBottom = 0;
            clTituloMoneda.BorderWidthTop = 0;
            clTituloMoneda.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clMoneda = new PdfPCell(new Paragraph(Reporte.Moneda, arial));
            clMoneda.BorderWidthLeft = 0;
            clMoneda.BorderWidthRight = 0;
            clMoneda.BorderWidthBottom = 0;
            clMoneda.BorderWidthTop = 0;
            clMoneda.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTituloTipoCambio = new PdfPCell(new Paragraph("Tipo Cambio", arialChicaNegrita));
            clTituloTipoCambio.BorderWidthLeft = 0;
            clTituloTipoCambio.BorderWidthRight = 0;
            clTituloTipoCambio.BorderWidthBottom = 0;
            clTituloTipoCambio.BorderWidthTop = 0;
            clTituloTipoCambio.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTipoCambio = new PdfPCell(new Paragraph(Reporte.Tipo_Cambio, arial));
            clTipoCambio.BorderWidthLeft = 0;
            clTipoCambio.BorderWidthRight = 0.5f;
            clTipoCambio.BorderWidthBottom = 0;
            clTipoCambio.BorderWidthTop = 0;
            clTipoCambio.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTituloDireccion = new PdfPCell(new Paragraph("Dirección", arialChicaNegrita));
            clTituloDireccion.BorderWidthLeft = 0.5f;
            clTituloDireccion.BorderWidthRight = 0;
            clTituloDireccion.BorderWidthBottom = 0;
            clTituloDireccion.BorderWidthTop = 0;
            clTituloDireccion.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clDireccion = new PdfPCell(new Paragraph(Reporte.Datos_Clientes.Calle + " " + Reporte.Datos_Clientes.Numero_Exterior, arial));
            clDireccion.BorderWidthLeft = 0;
            clDireccion.BorderWidthRight = 0;
            clDireccion.BorderWidthBottom = 0;
            clDireccion.BorderWidthTop = 0;
            clDireccion.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTituloFecha = new PdfPCell(new Paragraph("Fecha y Hora", arialChicaNegrita));
            clTituloFecha.BorderWidthLeft = 0.5f;
            clTituloFecha.BorderWidthRight = 0;
            clTituloFecha.BorderWidthBottom = 0;
            clTituloFecha.BorderWidthTop = 0;
            clTituloFecha.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clFecha = new PdfPCell(new Paragraph(Reporte.Fecha_Emision, arial));
            clFecha.Colspan = 3;
            clFecha.BorderWidthLeft = 0;
            clFecha.BorderWidthRight = 0.5f;
            clFecha.BorderWidthBottom = 0;
            clFecha.BorderWidthTop = 0;
            clFecha.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clvacio = new PdfPCell(new Paragraph("", arial));
            clvacio.BorderWidthLeft = 0.5f;
            clvacio.BorderWidthRight = 0;
            clvacio.BorderWidthBottom = 0;
            clvacio.BorderWidthTop = 0;
            clvacio.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clColonia = new PdfPCell(new Paragraph(Reporte.Datos_Clientes.Colonia + " " + Reporte.Datos_Clientes.CP, arial));
            clColonia.BorderWidthLeft = 0;
            clColonia.BorderWidthRight = 0;
            clColonia.BorderWidthBottom = 0;
            clColonia.BorderWidthTop = 0;
            clColonia.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTituloLugarExpedicion = new PdfPCell(new Paragraph("Lugar de Expedición", arialChicaNegrita));
            clTituloLugarExpedicion.BorderWidthLeft = 0.5f;
            clTituloLugarExpedicion.BorderWidthRight = 0;
            clTituloLugarExpedicion.BorderWidthBottom = 0;
            clTituloLugarExpedicion.BorderWidthTop = 0;
            clTituloLugarExpedicion.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clLugarExpedicion = new PdfPCell(new Paragraph(Reporte.Lugar_Expedicion, arial));
            clLugarExpedicion.Colspan = 3;
            clLugarExpedicion.BorderWidthLeft = 0;
            clLugarExpedicion.BorderWidthRight = 0.5f;
            clLugarExpedicion.BorderWidthBottom = 0;
            clLugarExpedicion.BorderWidthTop = 0;
            clLugarExpedicion.HorizontalAlignment = Element.ALIGN_LEFT;


            PdfPCell clTituloCiudad = new PdfPCell(new Paragraph("Ciudad", arialChicaNegrita));
            clTituloCiudad.BorderWidthLeft = 0.5f;
            clTituloCiudad.BorderWidthRight = 0;
            clTituloCiudad.BorderWidthBottom = 0;
            clTituloCiudad.BorderWidthTop = 0;
            clTituloCiudad.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clCiudad = new PdfPCell(new Paragraph(Reporte.Datos_Clientes.Ciudad + ", " + Reporte.Datos_Clientes.Estado + ", " + Reporte.Datos_Clientes.Pais, arial));
            clCiudad.BorderWidthLeft = 0;
            clCiudad.BorderWidthRight = 0;
            clCiudad.BorderWidthBottom = 0;
            clCiudad.BorderWidthTop = 0;
            clCiudad.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTituloFormaPago = new PdfPCell(new Paragraph("Forma de Pago", arialChicaNegrita));
            clTituloFormaPago.BorderWidthLeft = 0.5f;
            clTituloFormaPago.BorderWidthRight = 0;
            clTituloFormaPago.BorderWidthBottom = 0;
            clTituloFormaPago.BorderWidthTop = 0;
            clTituloFormaPago.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clFormaPago = new PdfPCell(new Paragraph(Reporte.Metodo_Pago, arial));
            clFormaPago.Colspan = 3;
            clFormaPago.BorderWidthLeft = 0;
            clFormaPago.BorderWidthRight = 0.5f;
            clFormaPago.BorderWidthBottom = 0;
            clFormaPago.BorderWidthTop = 0;
            clFormaPago.HorizontalAlignment = Element.ALIGN_LEFT;


            PdfPCell clTituloRfc = new PdfPCell(new Paragraph("RFC", arialChicaNegrita));
            clTituloRfc.BorderWidthLeft = 0.5f;
            clTituloRfc.BorderWidthRight = 0;
            clTituloRfc.BorderWidthBottom = 0;
            clTituloRfc.BorderWidthTop = 0;
            clTituloRfc.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clRfc = new PdfPCell(new Paragraph(Reporte.Datos_Clientes.RFC, arial));
            clRfc.BorderWidthLeft = 0;
            clRfc.BorderWidthRight = 0;
            clRfc.BorderWidthBottom = 0;
            clRfc.BorderWidthTop = 0;
            clRfc.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTituloCuentaPredial = new PdfPCell(new Paragraph("Cuenta Predial", arialChicaNegrita));
            clTituloCuentaPredial.BorderWidthLeft = 0.5f;
            clTituloCuentaPredial.BorderWidthRight = 0;
            clTituloCuentaPredial.BorderWidthBottom = 0;
            clTituloCuentaPredial.BorderWidthTop = 0;
            clTituloCuentaPredial.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clCuentaPredial = new PdfPCell(new Paragraph(Reporte.Cuenta_Predial, arial));
            clCuentaPredial.BorderWidthLeft = 0;
            clCuentaPredial.BorderWidthRight = 0;
            clCuentaPredial.BorderWidthBottom = 0;
            clCuentaPredial.BorderWidthTop = 0;
            clCuentaPredial.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTituloMetodoPago = new PdfPCell(new Paragraph("Método de Pago", arialChicaNegrita));
            clTituloMetodoPago.BorderWidthLeft = 0.5f;
            clTituloMetodoPago.BorderWidthRight = 0;
            clTituloMetodoPago.BorderWidthBottom = 0;
            clTituloMetodoPago.BorderWidthTop = 0;
            clTituloMetodoPago.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clMetodoPago = new PdfPCell(new Paragraph(Reporte.Forma_Pago, arial));
            clMetodoPago.Colspan = 3;
            clMetodoPago.BorderWidthLeft = 0;
            clMetodoPago.BorderWidthRight = 0.5f;
            clMetodoPago.BorderWidthBottom = 0;
            clMetodoPago.BorderWidthTop = 0;
            clMetodoPago.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clvacio2 = new PdfPCell(new Paragraph("", arial));
            clvacio2.Colspan = 2;
            clvacio2.BorderWidthLeft = 0.5f;
            clvacio2.BorderWidthRight = 0;
            clvacio2.BorderWidthBottom = 0;
            clvacio2.BorderWidthTop = 0;

            PdfPCell clTituloCondicionesPago = new PdfPCell(new Paragraph("Condiciones de Pago", arialChicaNegrita));
            clTituloCondicionesPago.BorderWidthLeft = 0.5f;
            clTituloCondicionesPago.BorderWidthRight = 0;
            clTituloCondicionesPago.BorderWidthBottom = 0;
            clTituloCondicionesPago.BorderWidthTop = 0;
            clTituloCondicionesPago.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clCondicionesPago = new PdfPCell(new Paragraph(Reporte.Condiciones_Pago, arial));
            clCondicionesPago.Colspan = 3;
            clCondicionesPago.BorderWidthLeft = 0;
            clCondicionesPago.BorderWidthRight = 0.5f;
            clCondicionesPago.BorderWidthBottom = 0;
            clCondicionesPago.BorderWidthTop = 0;
            clCondicionesPago.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clvacio3 = new PdfPCell(new Paragraph("", arial));
            clvacio3.Colspan = 2;
            clvacio3.BorderWidthLeft = 0.5f;
            clvacio3.BorderWidthRight = 0;
            clvacio3.BorderWidthBottom = 0.5f;
            clvacio3.BorderWidthTop = 0;

            PdfPCell clTituloCuentaPago = new PdfPCell(new Paragraph("Número Cuenta Pago", arialChicaNegrita));
            clTituloCuentaPago.BorderWidthLeft = 0.5f;
            clTituloCuentaPago.BorderWidthRight = 0;
            clTituloCuentaPago.BorderWidthBottom = 0.5f;
            clTituloCuentaPago.BorderWidthTop = 0;
            clTituloCuentaPago.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clCuentaPago = new PdfPCell(new Paragraph(Reporte.Datos_Clientes.Cuenta_Pago, arial));
            clCuentaPago.Colspan = 3;
            clCuentaPago.BorderWidthLeft = 0;
            clCuentaPago.BorderWidthRight = 0.5f;
            clCuentaPago.BorderWidthBottom = 0.5f;
            clCuentaPago.BorderWidthTop = 0;
            clCuentaPago.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clvacio4 = new PdfPCell(new Paragraph("", arial));
            clvacio4.Border = 0;
            clvacio4.Colspan = 6;


            PdfPCell clTituloRPU = new PdfPCell(new Paragraph("RPU", arialChicaNegrita));
            clTituloRPU.BorderWidthLeft = 0.5f;
            clTituloRPU.BorderWidthRight = 0;
            clTituloRPU.BorderWidthBottom = 0;
            clTituloRPU.BorderWidthTop = 0;
            clTituloRPU.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clRPU = new PdfPCell(new Paragraph(Reporte.RPU, arial));
            clRPU.BorderWidthLeft = 0;
            clRPU.BorderWidthRight = 0;
            clRPU.BorderWidthBottom = 0;
            clRPU.BorderWidthTop = 0;
            clRPU.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTituloPeriodoPago = new PdfPCell(new Paragraph("Periodo de Pago", arialChicaNegrita));
            clTituloPeriodoPago.BorderWidthLeft = 0.5f;
            clTituloPeriodoPago.BorderWidthRight = 0;
            clTituloPeriodoPago.BorderWidthBottom = 0;
            clTituloPeriodoPago.BorderWidthTop = 0;
            clTituloPeriodoPago.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clPeriodoPago = new PdfPCell(new Paragraph(Reporte.Periodo_Pago, arial));
            clPeriodoPago.Colspan = 3;
            clPeriodoPago.BorderWidthLeft = 0;
            clPeriodoPago.BorderWidthRight = 0.5f;
            clPeriodoPago.BorderWidthBottom = 0;
            clPeriodoPago.BorderWidthTop = 0;
            clPeriodoPago.HorizontalAlignment = Element.ALIGN_LEFT;


            PdfPCell clTituloTipoServicio = new PdfPCell(new Paragraph("Tipo de Servicio", arialChicaNegrita));
            clTituloTipoServicio.BorderWidthLeft = 0.5f;
            clTituloTipoServicio.BorderWidthRight = 0;
            clTituloTipoServicio.BorderWidthBottom = 0;
            clTituloTipoServicio.BorderWidthTop = 0;
            clTituloTipoServicio.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clPTipoServicio = new PdfPCell(new Paragraph(Reporte.Tipo_Servicio, arial));
            clPTipoServicio.Colspan = 3;
            clPTipoServicio.BorderWidthLeft = 0;
            clCondicionesPago.BorderWidthRight = 0.5f;
            clPTipoServicio.BorderWidthBottom = 0;
            clPTipoServicio.BorderWidthTop = 0;
            clPTipoServicio.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTituloNoRecibo = new PdfPCell(new Paragraph("No. Recibo", arialChicaNegrita));
            clTituloNoRecibo.BorderWidthLeft = 0.5f;
            clTituloNoRecibo.BorderWidthRight = 0;
            clTituloNoRecibo.BorderWidthBottom = 0;
            clTituloNoRecibo.BorderWidthTop = 0;
            clTituloNoRecibo.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clNoRecibo = new PdfPCell(new Paragraph(Reporte.No_Recibo, arial));
            clNoRecibo.Colspan = 3;
            clNoRecibo.BorderWidthLeft = 0;
            clNoRecibo.BorderWidthRight = 0.5f;
            clNoRecibo.BorderWidthBottom = 0;
            clNoRecibo.BorderWidthTop = 0;
            clNoRecibo.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clTituloCodigoBarras = new PdfPCell(new Paragraph("Código Barras", arialChicaNegrita));
            clTituloCodigoBarras.BorderWidthLeft = 0.5f;
            clTituloCodigoBarras.BorderWidthRight = 0;
            clTituloCodigoBarras.BorderWidthBottom = 0.5f;
            clTituloCodigoBarras.BorderWidthTop = 0;
            clTituloCodigoBarras.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell clCodigoBarras = new PdfPCell(new Paragraph(Reporte.Codigo_Barras, arial));
            clCodigoBarras.Colspan = 3;
            clCodigoBarras.BorderWidthLeft = 0;
            clCodigoBarras.BorderWidthRight = 0.5f;
            clCodigoBarras.BorderWidthBottom = 0.5f; 
            clCodigoBarras.BorderWidthTop = 0;
            clCodigoBarras.HorizontalAlignment = Element.ALIGN_LEFT;

            // Añadimos las celdas a la tabla
            tabla.AddCell(clFacturado);

            //tabla.AddCell(clTituloSerie);
            //tabla.AddCell(clSerie);
            tabla.AddCell(clTituloFolio);
            tabla.AddCell(clFolio);

            tabla.AddCell(clTituloUsoCfdi);
            tabla.AddCell(clUsoCfdi);

            tabla.AddCell(clTituloRPU);
            tabla.AddCell(clRPU);

            //tabla.AddCell(clTituloMoneda);
            //tabla.AddCell(clMoneda);
            //tabla.AddCell(clTituloTipoCambio);
            //tabla.AddCell(clTipoCambio);

            tabla.AddCell(clTituloFecha);
            tabla.AddCell(clFecha);

            tabla.AddCell(clRazonSocial);
            tabla.AddCell(clTituloLugarExpedicion);
            tabla.AddCell(clLugarExpedicion);

            tabla.AddCell(clTituloDireccion);

            tabla.AddCell(clDireccion);
            tabla.AddCell(clTituloFormaPago);
            tabla.AddCell(clFormaPago);

            tabla.AddCell(clvacio);
            tabla.AddCell(clColonia);
            tabla.AddCell(clTituloMetodoPago);
            tabla.AddCell(clMetodoPago);

            tabla.AddCell(clTituloCiudad);
            tabla.AddCell(clCiudad);

            tabla.AddCell(clTituloPeriodoPago);
            tabla.AddCell(clPeriodoPago);

            tabla.AddCell(clTituloRfc);
            tabla.AddCell(clRfc);

            tabla.AddCell(clTituloTipoServicio);
            tabla.AddCell(clPTipoServicio);

            //if (!string.IsNullOrEmpty(Reporte.Cuenta_Predial))
            //{
            //    tabla.AddCell(clTituloCuentaPredial);
            //    tabla.AddCell(clCuentaPredial);
            //}
            //else
            //    tabla.AddCell(clvacio2);

            tabla.AddCell(clvacio2);

            //tabla.AddCell(clTituloCuentaPago);
            //tabla.AddCell(clCuentaPago);
            tabla.AddCell(clTituloNoRecibo);
            tabla.AddCell(clNoRecibo);

            tabla.AddCell(clvacio3);
            tabla.AddCell(clTituloCodigoBarras);
            tabla.AddCell(clCodigoBarras);
            




            tabla.AddCell(clvacio4);
            tabla.AddCell(clvacio4);
            tabla.AddCell(clvacio4);

            return tabla;
        }

        private PdfPTable Crear_Tabla_Uuid()
        {
            PdfPTable tabla;
            tabla = new PdfPTable(4) { WidthPercentage = 100 };
            tabla.TotalWidth = 400f;
            Paragraph pTituloNoCertificado = new Paragraph("No. Certificado", arialChicaNegrita);
            Paragraph pNoCertificado = new Paragraph(Reporte.No_certificado, arial);
            Paragraph pTituloNoCertificadoSat = new Paragraph("No. Certificado SAT", arialChicaNegrita);
            Paragraph pNoCertificadoSat = new Paragraph(Reporte.No_certificado_Sat, arial);
            Paragraph pTituloFechaTimbrado = new Paragraph("Fecha Timbrado", arialChicaNegrita);
            Paragraph pFechaTimbrado = new Paragraph(Reporte.Fecha_Timbrado, arial);
            Paragraph pTituloFolioFiscal = new Paragraph("Folio Fiscal", arialChicaNegrita);
            Paragraph pFolioFiscal = new Paragraph(Reporte.Uuid, arial);

            PdfPCell clTituloNoCertificado = new PdfPCell(pTituloNoCertificado);
            clTituloNoCertificado.BorderWidthLeft = 0.5f;
            clTituloNoCertificado.BorderWidthRight = 0;
            clTituloNoCertificado.BorderWidthBottom = 0.5f;
            clTituloNoCertificado.BorderWidthTop = 0.5f;
            clTituloNoCertificado.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell clNoCertificado = new PdfPCell(pNoCertificado);
            clNoCertificado.BorderWidthLeft = 0.5f;
            clNoCertificado.BorderWidthRight = 0;
            clNoCertificado.BorderWidthBottom = 0.5f;
            clNoCertificado.BorderWidthTop = 0;
            clNoCertificado.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell clTituloNoCertificadoSat = new PdfPCell(pTituloNoCertificadoSat);
            clTituloNoCertificadoSat.BorderWidthLeft = 0.5f;
            clTituloNoCertificadoSat.BorderWidthRight = 0;
            clTituloNoCertificadoSat.BorderWidthBottom = 0.5f;
            clTituloNoCertificadoSat.BorderWidthTop = 0.5f;
            clTituloNoCertificadoSat.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell clNoCertificadoSat = new PdfPCell(pNoCertificadoSat);
            clNoCertificadoSat.BorderWidthLeft = 0.5f;
            clNoCertificadoSat.BorderWidthRight = 0;
            clNoCertificadoSat.BorderWidthBottom = 0.5f;
            clNoCertificadoSat.BorderWidthTop = 0;
            clNoCertificadoSat.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell clTituloFechaTimbrado = new PdfPCell(pTituloFechaTimbrado);
            clTituloFechaTimbrado.BorderWidthLeft = 0.5f;
            clTituloFechaTimbrado.BorderWidthRight = 0;
            clTituloFechaTimbrado.BorderWidthBottom = 0.5f;
            clTituloFechaTimbrado.BorderWidthTop = 0.5f;
            clTituloFechaTimbrado.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell clFechaTimbrado = new PdfPCell(pFechaTimbrado);
            clFechaTimbrado.BorderWidthLeft = 0.5f;
            clFechaTimbrado.BorderWidthRight = 0;
            clFechaTimbrado.BorderWidthBottom = 0.5f;
            clFechaTimbrado.BorderWidthTop = 0;
            clFechaTimbrado.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell clTituloFolioFiscal = new PdfPCell(pTituloFolioFiscal);
            clTituloFolioFiscal.BorderWidthLeft = 0.5f;
            clTituloFolioFiscal.BorderWidthRight = 0.5f;
            clTituloFolioFiscal.BorderWidthBottom = 0.5f;
            clTituloFolioFiscal.BorderWidthTop = 0.5f;
            clTituloFolioFiscal.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell clFolioFiscal = new PdfPCell(pFolioFiscal);
            clFolioFiscal.BorderWidthLeft = 0.5f;
            clFolioFiscal.BorderWidthRight = 0.5f;
            clFolioFiscal.BorderWidthBottom = 0.5f;
            clFolioFiscal.BorderWidthTop = 0;
            clFolioFiscal.DisableBorderSide(1);
            clFolioFiscal.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell clVacio = new PdfPCell(new Paragraph("", arial));
            clVacio.BorderWidthLeft = 0;
            clVacio.BorderWidthRight = 0;
            clVacio.BorderWidthBottom = 0;
            clVacio.BorderWidthTop = 0;

            // Añadimos las celdas a la tabla
            tabla.AddCell(clTituloNoCertificado);
            tabla.AddCell(clTituloNoCertificadoSat);
            tabla.AddCell(clTituloFechaTimbrado);
            tabla.AddCell(clTituloFolioFiscal);
            tabla.AddCell(clNoCertificado);
            tabla.AddCell(clNoCertificadoSat);
            tabla.AddCell(clFechaTimbrado);
            tabla.AddCell(clFolioFiscal);

            tabla.AddCell(clVacio);
            tabla.AddCell(clVacio);
            tabla.AddCell(clVacio);
            tabla.AddCell(clVacio);
            tabla.AddCell(clVacio);
            tabla.AddCell(clVacio);

            return tabla;
        }

        /// <summary>
        /// Crears the encabezado.
        /// </summary>
        /// <returns>PdfPTable.</returns>
        private PdfPTable Crear_Titulo()
        {
            PdfPCell clImagenIz;
            PdfPCell clImagenDr;


            PdfPTable tabla_titulos;
            tabla_titulos = new PdfPTable(3) { WidthPercentage = 100 };
            tabla_titulos.TotalWidth = 400f;

            float[] widths = new float[] { 1f, 2f, 1f };
            tabla_titulos.SetWidths(widths);

            Image imagenIzq = null;
            Image imagenDer = null;
            //float[] ancho = new float[] { convertir_a_puntos(2), convertir_a_puntos(16), convertir_a_puntos(4) };

            string imagenIzqUrl = this.Reporte.Logo;//@HttpContext.Current.Server.MapPath("~") + "Imagenes\\Logo_Izquierda.jpg";
            string imagenDerUrl = @HttpContext.Current.Server.MapPath("~") + "Imagenes\\Logo_Derecha.jpg";
            //Logo Izquierda
            if (File.Exists(imagenIzqUrl))
            {
                imagenIzq = iTextSharp.text.Image.GetInstance(imagenIzqUrl);
                imagenIzq.ScaleToFit(125f, 75f);
            }
            if (imagenIzq != null)
                clImagenIz = new PdfPCell(imagenIzq);
            else
                clImagenIz = new PdfPCell();
            clImagenIz.Rowspan = 7;
            //Logo Derecha
            if (File.Exists(imagenDerUrl))
            {
                imagenDer = iTextSharp.text.Image.GetInstance(imagenDerUrl);
                imagenDer.ScaleToFit(100f, 50);
            }
            if (imagenDer != null)
                clImagenDr = new PdfPCell(imagenDer);
            else
                clImagenDr = new PdfPCell();
            clImagenDr.Rowspan = 7;

            clImagenIz.BorderWidth = 0;
            clImagenIz.HorizontalAlignment = Element.ALIGN_LEFT;
            clImagenDr.BorderWidth = 0;
            clImagenDr.HorizontalAlignment = Element.ALIGN_RIGHT;

            Paragraph pRazonSocial = new Paragraph(Reporte.Razon_Social_Empresa, arialChicaNegrita);
            Paragraph pRfc = new Paragraph(Reporte.Rfc_Empresa, arial);
            Paragraph pDireccionEmpresa = new Paragraph(Reporte.Direccion_Empresa, arial);
            Paragraph pColonia = new Paragraph(Reporte.Colonia_Empresa, arial);
            Paragraph pCiudadEstadoPais = new Paragraph(Reporte.Ciudad_Empresa + " " + Reporte.Estado_Empresa + " " + Reporte.Pais_Empresa, arial);
            Paragraph pTelefonoEmpresa = new Paragraph(Reporte.Telefono_Empresa, arial);
            Paragraph pRegimenFiscal = new Paragraph("Régimen Fiscal " + Reporte.Regimen_Fiscal, arial);
            Paragraph pTitulo = new Paragraph("Comprobante Fiscal Digital", arialChicaNegrita);
            Paragraph pTipoCfdi = new Paragraph("Tipo Documento: " + Reporte.Tipo_CFDI, arialChicaNegrita);
            Paragraph pVacio = new Paragraph("", arial);

            PdfPCell clRazonSocial = new PdfPCell(pRazonSocial);
            clRazonSocial.BorderWidth = 0;
            clRazonSocial.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell clRfc = new PdfPCell(pRfc);
            clRfc.BorderWidth = 0;
            clRfc.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell clpDireccionEmpresa = new PdfPCell(pDireccionEmpresa);
            clpDireccionEmpresa.BorderWidth = 0;
            clpDireccionEmpresa.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell clpColonia = new PdfPCell(pColonia);
            clpColonia.BorderWidth = 0;
            clpColonia.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell clCiudadEstadoPais = new PdfPCell(pCiudadEstadoPais);
            clCiudadEstadoPais.BorderWidth = 0;
            clCiudadEstadoPais.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell clTelefonoEmpresa = new PdfPCell(pTelefonoEmpresa);
            clTelefonoEmpresa.BorderWidth = 0;
            clTelefonoEmpresa.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell clRegimenFiscal = new PdfPCell(pRegimenFiscal);
            clRegimenFiscal.BorderWidth = 0;
            clRegimenFiscal.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell clTitulo = new PdfPCell(pTitulo);
            clTitulo.BorderWidth = 0;
            clTitulo.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell clTipoCfdi = new PdfPCell(pTipoCfdi);
            clTipoCfdi.BorderWidth = 0;
            clTipoCfdi.HorizontalAlignment = Element.ALIGN_RIGHT;

            PdfPCell clVacio = new PdfPCell(pVacio);
            clVacio.BorderWidth = 0;
            clVacio.HorizontalAlignment = Element.ALIGN_RIGHT;

            // Añadimos las celdas a la tabla
            tabla_titulos.AddCell(clImagenIz);
            tabla_titulos.AddCell(clRazonSocial);
            tabla_titulos.AddCell(clImagenDr);
            tabla_titulos.AddCell(clRfc);
            tabla_titulos.AddCell(clpDireccionEmpresa);
            tabla_titulos.AddCell(clpColonia);
            tabla_titulos.AddCell(clCiudadEstadoPais);
            tabla_titulos.AddCell(clTelefonoEmpresa);
            tabla_titulos.AddCell(clRegimenFiscal);
            tabla_titulos.AddCell(clVacio);
            tabla_titulos.AddCell(clTitulo);
            tabla_titulos.AddCell(clTipoCfdi);
            tabla_titulos.AddCell(clVacio);
            tabla_titulos.AddCell(clVacio);
            tabla_titulos.AddCell(clVacio);
            tabla_titulos.AddCell(clVacio);
            tabla_titulos.AddCell(clVacio);
            tabla_titulos.AddCell(clVacio);

            return tabla_titulos;

        }
        /// <summary>
        /// Convertirs a puntos.
        /// </summary>
        /// <param name="medida_cm">The medida cm.</param>
        /// <returns>System.Single.</returns>
        public float convertir_a_puntos(float medida_cm)
        {
            float respuesta = 0;

            respuesta = (medida_cm / 2.54f) * 72;
            return respuesta;

        }
    }
}