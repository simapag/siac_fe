﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIAC.Facturacion.Reportes.Entidades
{
    public class Cls_Metodos_Pago
    {
        public string FormaPago { get; set; }
        public string MetodoPago { get; set; }
        public string CondicionesDePago { get; set; }
    }
}