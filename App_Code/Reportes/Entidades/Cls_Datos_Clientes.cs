﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIAC.Facturacion.Reportes.Entidades
{
    public class Cls_Datos_Clientes
    {
        public string Razon_Social { get; set; }
        public string Nombre { get; set; }
        public string RFC { get; set; }
        public string Calle { get; set; }
        public string Numero_Interior { get; set; }
        public string Numero_Exterior { get; set; }
        public string Colonia { get; set; }
        public string CP { get; set; }
        public string Localidad { get; set; }
        public string Ciudad { get; set; }
        public string Estado { get; set; }
        public string Pais { get; set; }
        public string E_Mail { get; set; }
        public bool Enviar_E_Mail { get; set; }
        public string Forma_Pago { get; set; }
        public string Metodo_Pago { get; set; }
        public string Condiciones_Pago { get; set; }
        public string UsoCFDI { get; set; }
        //public string Leyenda_Fiscal { get; set; }
        //public string Tipo_Ingreso { get; set; }
        public string Cuenta_Pago { get; set; }
        public string Comentarios { get; set; }
    }
}