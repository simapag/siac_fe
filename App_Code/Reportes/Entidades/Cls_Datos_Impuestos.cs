﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIAC.Facturacion.Reportes.Entidades
{
    public class Cls_Datos_Impuestos
    {
        public double Base { get; set; }
        public string Impuesto { get; set; }
        public string Nodo { get; set; }
        public double TasaOcuota { get; set; }
        public string TipoFactor { get; set; }
        public double Importe { get; set; }
    }
}