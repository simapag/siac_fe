﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIAC.Facturacion.Reportes.Entidades
{
    public class Cls_Datos_Conceptos
    {
        public string Cantidad { get; set; }
        public string valorUnitario { get; set; }
        public string Unidad_Desc { get; set; }
        public string Unidad { get; set; }
        public string Codigo_Barras { get; set; }
        public string ClaveProdServ { get; set; }
        public string NoIdentificacion { get; set; }
        public string No_Recibo { get; set; }
        public string No_Pago { get; set; }
        public string Folio_Pago { get; set; }
        public string Fecha_Recibo { get; set; }
        public string Descripcion_Cat { get; set; }
        public string Descripcion { get; set; }
        public string ClaveUnidad { get; set; }
        public double Importe { get; set; }
        public double Subtotal { get; set; }
        public double Total { get; set; }
        public double Descuento { get; set; }
        public string Impuesto { get; set; }
        public double Base { get; set; }
        public string TipoFactor { get; set; }
        public string TasaOCuota { get; set; }
        public string Nodo { get; set; }
        public double Impuesto_Importe { get; set; }

        //public string Forma_Pago { get; set; }
        //public string Metodo_Pago { get; set; }
        //public string Numero_Cuenta_Pago { get; set; }
        //public string Condiciones_Pago { get; set; }
        //public string Leyenda_Fiscal { get; set; }
    }
}