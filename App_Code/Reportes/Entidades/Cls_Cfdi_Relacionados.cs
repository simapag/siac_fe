﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIAC.Facturacion.Reportes.Entidades
{
    public class Cls_Cfdi_Relacionados
    {
        public string TipoRelacion { get; set; }
        public string FolioFactura { get; set; }
    }
}